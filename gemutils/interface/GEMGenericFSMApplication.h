/// @file

#ifndef GEM_UTILS_GEMGENERICFSMAPPLICATION_H
#define GEM_UTILS_GEMGENERICFSMAPPLICATION_H

#include <gem/utils/GEMApplication.h>
#include <gem/utils/GEMState.h>

#include <nlohmann/json.hpp>
#include <toolbox/fsm/AsynchronousFiniteStateMachine.h>
#include <toolbox/fsm/FiniteStateMachine.h>
#include <toolbox/task/exception/Exception.h>
#include <xgi/framework/Method.h>

#include <map>
#include <unordered_map>
#include <unordered_set>

namespace toolbox::task {
class WorkLoop;
class ActionSignature;
}

namespace gem::utils {

/// @brief Holder for info of the transition function
struct CommandInfo {
    std::string transition_name; ///< State to transition to after the command
    std::function<void()> command; ///< function to be called for a transtion
};

class GEMGenericFSMApplication : public GEMApplication {
public:
    GEMGenericFSMApplication(xdaq::ApplicationStub* stub);
    virtual ~GEMGenericFSMApplication();

    /// @brief SOAP version of function to change the state of the FSM
    ///
    /// @param SOAP message on what state to change to
    xoap::MessageReference changeState(xoap::MessageReference msg);

    /// @brief event triggered whenever fsm state changes.
    ///
    /// States change from terminus states to transistion states and
    /// transition states to terminus states. Whenever one changes to a transistion
    /// state, the workloopDriver is called and function corresponding to
    /// the transition is called. Otherwise, the state simply changes
    ///
    /// @param
    void stateChanged(toolbox::fsm::FiniteStateMachine& fsm);

    /// @brief xgi Function used to chang the state of the FSM
    ///
    /// @param State to change to, in form of json as {"action": <state>}
    /// @param unused
    void xgiChangeState(xgi::Input* in, xgi::Output* out);

    /// @brief xgi Function for getting the current state of the FSM
    ///
    /// @param Unused in param
    /// @param Stream where state input is put into
    void stateUpdate(xgi::Input* in, xgi::Output* out);

    /// @brief Create control for this FSM in the out stream
    ///
    /// @param Unused in param
    /// @param Stream where html for control panel is put into
    void createFSMControlPanel(xgi::Input* in, xgi::Output* out);

protected:
    /// @brief Wrapper for firing event on the FSM, e.g. change the state.
    ///
    /// This function is used to avoid firing toolbox::Event objects which
    /// are harder to read
    ///
    /// @param name of the state to change to/event to fire
    void fireEvent(std::string event);

    /// @brief Wrapper for the workloop create and error handling associated with it
    void workloopDriver();

    /// @brief Function run by the workloop created by workloopDriver
    ///
    /// Workloop grabs the lock and tries to run the command associated with the
    /// transition state the FSM is in. If it passes, it makes the FSM tranisition
    /// to the terminus state else if changes the FSM to the Fail state
    ///
    /// @param unused workloop object
    bool workloopFunction(toolbox::task::WorkLoop* wl);

    /// @brief Add state to contained_states set
    ///
    /// @param state to be added
    void addState(States state);

    /// @brief Function for building the FSM nodes and transitions
    ///
    /// Function takes the predefined states in GEMStates.h and creates the edges and
    /// nodes for the given state transition from one terminus state to another.
    /// The idea is a function should be called (anonymously) in a transition state.
    /// The last three parameters are for the FSM control panel, the transition and terminus
    /// names being what the FSM reports are the current state and the button class
    /// for giving color information to the button used for state transition
    ///
    /// @param State the transition starts in
    /// @param State the transition goes through
    /// @param State the transition starts ends
    /// @param function called in the transition period
    /// @param Name associated with the transition
    /// @param Name associated with the final state of the transtion
    void addTransitionFunction(States start_state, States middle_state, States end_state,
        std::function<void()> transitionFunc, std::string transitionName, std::string terminusName);

    /// @brief Returns current state of the fsm
    ///
    /// @returns @c std::string current state of the fsm
    std::string currentStateName() { return p_gemfsm->getStateName(p_gemfsm->getCurrentState()); };

    std::string workLoopName; // Name of workloop for this instance
    std::unique_ptr<toolbox::fsm::FiniteStateMachine> p_gemfsm;
    std::unordered_set<States> contained_states; ///< Set of states currently in the FSM (for ease)
    /// Map of transition name to the command to be run for the transition
    std::unordered_map<std::string, CommandInfo> transitionInfo_by_name;
    nlohmann::json buttonInfo_json;
    toolbox::task::ActionSignature* m_wlSig;
    toolbox::BSem m_wl_semaphore;
};

} // namespace gem::utils

#endif // GEM_UTILS_GEMGENERICFSMAPPLICATION_H
