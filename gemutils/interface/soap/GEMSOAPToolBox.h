/// @file

#ifndef GEM_UTILS_SOAP_GEMSOAPTOOLBOX_H
#define GEM_UTILS_SOAP_GEMSOAPTOOLBOX_H

#include <gem/utils/GEMLogging.h>
#include <gem/utils/exception/Exception.h>

#include <xdaq/ApplicationContext.h>
#include <xdaq/ApplicationDescriptor.h>
#include <xdaq/NamespaceURI.h>
#include <xdata/Bag.h>
#include <xdata/Boolean.h>
#include <xdata/Double.h>
#include <xdata/Float.h>
#include <xdata/Integer.h>
#include <xdata/Integer32.h>
#include <xdata/Integer64.h>
#include <xdata/Serializable.h>
#include <xdata/String.h>
#include <xdata/UnsignedInteger32.h>
#include <xdata/UnsignedInteger64.h>
#include <xdata/UnsignedLong.h>
#include <xdata/UnsignedShort.h>
#include <xdata/Vector.h>
#include <xdata/soap/Serializer.h>
#include <xercesc/util/XercesDefs.hpp>
#include <xoap/MessageFactory.h>
#include <xoap/MessageReference.h>
#include <xoap/SOAPBody.h>
#include <xoap/SOAPEnvelope.h>
#include <xoap/SOAPPart.h>
#include <xoap/SOAPSerializer.h>
#include <xoap/domutils.h>

#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

namespace gem::utils::soap {

/// @brief Creates a SOAP fault message
///
/// See the SOAP specifications for more details.
xoap::MessageReference makeSOAPFaultReply(
    const std::string& faultString,
    const std::string& faultCode = "Server",
    const std::string& faultActor = "",
    const std::string& detail = "");

/// @brief Creates a SOAP reply message to the command @c command
xoap::MessageReference makeSOAPReply(const std::string& command, const std::string& response);

/// @brief Creates a SOAP reply to FSM transition command
xoap::MessageReference makeFSMSOAPReply(
    const std::string& event,
    const std::string& state);

/// @brief Sends a command without parameters to the destination application
///
/// @param command Name of the command to send to the  destination application
/// @param appCxt Context in which the source/receiver applications are running
/// @param srcDsc Source application descriptor
/// @param destDsc Destination application descriptor
void sendCommand(
    const std::string& command,
    xdaq::ApplicationContext* appCxt,
    const xdaq::ApplicationDescriptor* srcDsc,
    const xdaq::ApplicationDescriptor* destDsc);

/// @brief Sends a command with parameters to the destination application
///
/// @param command Name of the command to send to the destination application
/// @param parameters Map of values (keyed by string) to send with the SOAP message
/// @param appCxt Context in which the source/receiver applications are running
/// @param srcDsc Source application descriptor
/// @param destDsc Destination application descriptor
void sendCommandWithParameters(
    const std::string& command,
    const std::unordered_map<std::string, xdata::Serializable*>& parameters,
    xdaq::ApplicationContext* appCxt,
    const xdaq::ApplicationDescriptor* srcDsc,
    const xdaq::ApplicationDescriptor* destDsc);

/// @brief Extract the command name from a SOAP message
std::string extractCommandName(const xoap::MessageReference& msg);

/// @brief Returns the XOAP element that corresponds to a given parameter
xoap::SOAPElement extractSOAPCommandParameterElement(
    const xoap::MessageReference& msg,
    const std::string& parameterName);

/// @brief Extracts a parameter value from a SOAP message
///
/// @param msg is the SOAP message from which to extract the parameter value
/// @param parameterName is the name of the parameter of interest
///
/// @returns The corresponding parameter value
template <typename T>
T extractSOAPCommandParameter(
    const xoap::MessageReference& msg,
    const std::string& parameterName)
{
    log4cplus::Logger m_gemLogger(log4cplus::Logger::getInstance("GEMSOAPToolBoxLogger"));

    xdata::soap::Serializer serializer;
    T tmpVal;
    xoap::SOAPElement element = gem::utils::soap::extractSOAPCommandParameterElement(msg, parameterName);
    try {
        serializer.import(&tmpVal, element.getDOM());
    } catch (xcept::Exception& err) {
        const std::string msg = toolbox::toString("Failed to import SOAP command parameter 'xdaq:%s': '%s'.",
            parameterName.c_str(),
            err.message().c_str());

        XCEPT_RAISE(gem::utils::exception::SOAPException, msg);
    }
    return tmpVal;
}

/// @brief Modifies the parameters from a remote application configuration InfoSpace
///
/// @param parameters Map of values (keyed by string) to send with the SOAP message
/// @param appCxt Context in which the source/receiver applications are running
/// @param srcDsc Source application descriptor
/// @param destDsc Destination application descriptor
void sendApplicationParameters(
    const std::unordered_map<std::string, xdata::Serializable*>& parameters,
    xdaq::ApplicationContext* appCxt,
    const xdaq::ApplicationDescriptor* srcDsc,
    const xdaq::ApplicationDescriptor* destDsc);

/// @brief Creates a SOAP message requesting informtion about an application FSM state
///
/// @param nstag Namespace tag to append to the parameter request
/// @param appURN URN of the application to send the request to
/// @param isGEMApp whether to query additional parameters that are only in GEM applications
///
/// returns xoap::MessageReference to calling application
xoap::MessageReference createStateRequestMessage(
    const std::string& nstag,
    const std::string& appURN,
    const bool isGEMApp);

/// @brief Returns the FSM state from the given application
///
/// @param appCxt Context in which the source/receiver applications are running
/// @param srcDsc Source application descriptor
/// @param destDsc Destination application descriptor
///
/// returns xoap::MessageReference to calling application
std::string getApplicationState(
    xdaq::ApplicationContext* appCxt,
    const xdaq::ApplicationDescriptor* srcDsc,
    const xdaq::ApplicationDescriptor* destDsc);

} // namespace gem::utils::soap

#endif // GEM_UTILS_SOAP_GEMSOAPTOOLBOX_H
