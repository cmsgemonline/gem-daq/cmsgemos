/// @file

#ifndef GEM_UTILS_GEMSTATES_H
#define GEM_UTILS_GEMSTATES_H

#include <toolbox/fsm/FiniteStateMachine.h>

#include <unordered_map>

namespace gem::utils {

static const toolbox::fsm::State STATE_UNINIT = 'U'; ///< Uninitialized state (power on, reset, and recovery state)
static const toolbox::fsm::State STATE_COLD = 'B'; ///< Cold initialization state (firmware reload)
static const toolbox::fsm::State STATE_INITIAL = 'I'; ///< Initial state
static const toolbox::fsm::State STATE_HALTED = 'H'; ///< Halted state
static const toolbox::fsm::State STATE_CONFIGURED = 'C'; ///< Configured state
static const toolbox::fsm::State STATE_RUNNING = 'E'; ///< Running (enabled, active) state
static const toolbox::fsm::State STATE_PAUSED = 'P'; ///< Paused state
static const toolbox::fsm::State STATE_FAILED = 'F'; ///< Failed/Error state

// transitional states, TCDS way seems more elegant than HCAL, but both use a similar idea
static const toolbox::fsm::State STATE_INITIALIZING = 'i'; ///< Initializing transitional state
static const toolbox::fsm::State STATE_CONFIGURING = 'c'; ///< Configuring transitional state
static const toolbox::fsm::State STATE_HALTING = 'h'; ///< Halting transitional state
static const toolbox::fsm::State STATE_PAUSING = 'p'; ///< Pausing transitional state
static const toolbox::fsm::State STATE_STOPPING = 's'; ///< Stopping transitional state
static const toolbox::fsm::State STATE_STARTING = 'e'; ///< Starting transitional state
static const toolbox::fsm::State STATE_RESUMING = 'r'; ///< Resuming transitional state
static const toolbox::fsm::State STATE_RESETTING = 't'; ///< Resetting transitional state
static const toolbox::fsm::State STATE_FIXING = 'X'; ///< Fixing transitional state

static const toolbox::fsm::State STATE_NULL = 0; ///< Null state

enum class States : char {
    UNINIT = 'U', ///< Uninitialized state (power on, reset, and recovery state)
    COLD = 'B', ///< Cold initialization state (firmware reload)
    INITIAL = 'I', ///< Initial state
    HALTED = 'H', ///< Halted state
    CONFIGURED = 'C', ///< Configured state
    RUNNING = 'E', ///< Running (enabled, active) state
    PAUSED = 'P', ///< Paused state
    FAILED = 'F', ///< Failed/Error state

    // transitional states, TCDS way seems more elegant than HCAL, but both use a similar idea
    INITIALIZING = 'i', ///< Initializing transitional state
    CONFIGURING = 'c', ///< Configuring transitional state
    HALTING = 'h', ///< Halting transitional state
    PAUSING = 'p', ///< Pausing transitional state
    STOPPING = 's', ///< Stopping transitional state
    STARTING = 'e', ///< Starting transitional state
    RESUMING = 'r', ///< Resuming transitional state
    RESETTING = 't', ///< Resetting transitional state
    FIXING = 'X', ///< Fixing transitional state

    NULLED = 0, ///< Null state
};

static const std::unordered_map<States, std::string> name_by_state = {
    { States::UNINIT, "Uninitalized" },
    { States::COLD, "Cold" },
    { States::HALTED, "Halted" },
    { States::INITIAL, "Initial" },
    { States::CONFIGURED, "Configured" },
    { States::RUNNING, "Running" },
    { States::PAUSED, "Paused" },
    { States::FAILED, "Failed" },
    { States::INITIALIZING, "Initializing" },
    { States::CONFIGURING, "Configuring" },
    { States::HALTING, "Halting" },
    { States::PAUSING, "Pausing" },
    { States::STOPPING, "Stopping" },
    { States::STARTING, "Starting" },
    { States::RESUMING, "Resuming" },
    { States::RESETTING, "Resetting" },
    { States::FIXING, "Fixing" },
    { States::NULLED, "Nulled" },
};

} // namespace gem::utils

#endif // GEM_UTILS_GEMSTATES_H
