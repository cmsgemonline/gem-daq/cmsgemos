/// @file

#ifndef GEM_UTILS_LOCK_H
#define GEM_UTILS_LOCK_H

#include <toolbox/BSem.h>

namespace gem::utils {

class Lock {
public:
    Lock(toolbox::BSem::State state = toolbox::BSem::EMPTY, bool recursive = true);
    ~Lock();

    void lock();
    void unlock();

private:
    toolbox::BSem m_semaphore;

    // Prevent copying.
    Lock(Lock const&);
    Lock& operator=(Lock const&);
};

} // namespace gem::utils

#endif // GEM_UTILS_LOCK_H
