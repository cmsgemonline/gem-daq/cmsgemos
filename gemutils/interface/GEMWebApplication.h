/// @file

#ifndef GEM_UTILS_GEMWEBAPPLICATION_H
#define GEM_UTILS_GEMWEBAPPLICATION_H

#include <gem/utils/GEMLogging.h>

#include <xdaq/WebApplication.h>

#include <functional>
#include <string>
#include <vector>

// Forward declarations
namespace xgi {
class Input;
class Output;
}

namespace gem::utils {

// Forward declarations
class GEMFSM;
class GEMApplication;
class GEMFSMApplication;

/// @brief This class helps creating tabbed Web interfaces
class tab_creator {
protected:
    /// @brief Adds a new tab to be rendered
    ///
    /// Tabs will be rendered in order they are added.
    ///
    /// @param @c title Tab title (also used to derive the HTML ID)
    /// @param @c f Tab rendering function
    template <class Class>
    void add_tab(const std::string& title, void (Class::*f)(xgi::Input*, xgi::Output*))
    {
        std::string html_id = title;
        html_id.erase(std::remove_if(html_id.begin(), html_id.end(),
                          [](char c) { return !std::isalnum(c); }),
            html_id.end());
        html_id += "-tab";
        while (m_tabs.end() != std::find_if(m_tabs.begin(), m_tabs.end(), [html_id](auto tab) { return tab.html_id == html_id; })) {
            html_id += "_dup";
        }
        auto function = [this, f](xgi::Input* in, xgi::Output* out) { (static_cast<Class*>(this)->*f)(in, out); };

        m_tabs.push_back({ title, std::move(html_id), std::move(function) });
    }

    /// @brief Renders the tabs as HTML in the output stream
    ///
    /// @param @c Input stream
    /// @param @c Output stream
    void render_tabs(xgi::Input* in, xgi::Output* out);

private:
    struct tab_info {
        std::string title; ///< Tab title
        std::string html_id; ///< Unique HTML ID
        std::function<void(xgi::Input*, xgi::Output*)> function; ///< Custom rendering function
    };

    std::vector<tab_info> m_tabs; ///< List of tabs to be rendered
};

class GEMWebApplication : public tab_creator {
    friend class GEMFSM;
    friend class GEMApplication;
    friend class GEMFSMApplication;
    friend class GEMGenericFSMApplication;

public:
    GEMWebApplication(GEMApplication* application);
    GEMWebApplication(GEMFSMApplication* application);
    GEMWebApplication(GEMWebApplication const&) = delete;
    virtual ~GEMWebApplication();

protected:
    virtual void webDefault(xgi::Input* in, xgi::Output* out);
    void webFooterGEM(xgi::Input* in, xgi::Output* out);

    virtual void jsonStateUpdate(xgi::Input* in, xgi::Output* out);
    virtual void jsonUpdate(xgi::Input* in, xgi::Output* out);

    // FIXME: FSM only, to be moved
    void controlPanel(xgi::Input* in, xgi::Output* out);

    log4cplus::Logger m_gemLogger;
    GEMApplication* p_gemApp;
    GEMFSMApplication* p_gemFSMApp;
};

} // namespace gem::utils

#endif // GEM_UTILS_GEMWEBAPPLICATION_H
