/// @file

#ifndef GEM_UTILS_GEMAPPLICATION_H
#define GEM_UTILS_GEMAPPLICATION_H

#include <gem/core/layout-tree/addressing.h>
#include <gem/core/layout-tree/nodes.h>
#include <gem/utils/GEMLogging.h>
#include <gem/utils/exception/Exception.h>
#include <gem/utils/soap/GEMSOAPToolBox.h>

#include <log4cplus/logger.h>
#include <toolbox/TimeVal.h>
#include <toolbox/string.h>
#include <xcept/Exception.h>
#include <xcept/tools.h>
#include <xdaq/Application.h>
#include <xdaq/ApplicationContext.h>
#include <xdaq/ApplicationDescriptorImpl.h>
#include <xdaq/ApplicationGroup.h>
#include <xdaq/ApplicationStub.h>
#include <xdaq/NamespaceURI.h>
#include <xdaq/WebApplication.h>
#include <xdaq/XceptSerializer.h>
#include <xdaq/exception/Exception.h>
#include <xdata/Bag.h>
#include <xdata/Boolean.h>
#include <xdata/Double.h>
#include <xdata/Float.h>
#include <xdata/Integer.h>
#include <xdata/Integer32.h>
#include <xdata/Integer64.h>
#include <xdata/String.h>
#include <xdata/UnsignedInteger32.h>
#include <xdata/UnsignedInteger64.h>
#include <xdata/UnsignedLong.h>
#include <xdata/UnsignedShort.h>
#include <xdata/Vector.h>
#include <xgi/Input.h>
#include <xgi/Method.h>
#include <xgi/Output.h>
#include <xgi/framework/UIManager.h>
#include <xoap/Method.h>

#include <cstdlib>
#include <deque>
#include <limits>
#include <map>
#include <memory>
#include <string>

// Forward declarations
namespace xdaq {
class ApplicationStub;
}

namespace xgi {
class Input;
class Output;
}

namespace gem::utils {

class GEMWebApplication;

class GEMApplication : public xdaq::WebApplication, public xdata::ActionListener {
    friend class GEMWebApplication;

public:
    static constexpr size_t MAX_AMCS_PER_CRATE = 12; ///< I SHOULD PROBABLY NOT BE HARD CODED
    static constexpr size_t MAX_OPTOHYBRIDS_PER_AMC = 16; ///< I SHOULD PROBABLY NOT BE HARD CODED

    GEMApplication(xdaq::ApplicationStub* stub);

    virtual ~GEMApplication();

    void actionPerformed(xdata::Event& event) override;

    /// @brief
    std::string getFullURL();

    /// @brief
    void xgiDefault(xgi::Input* in, xgi::Output* out);

    /// @brief
    void jsonUpdate(xgi::Input* in, xgi::Output* out);

protected:
    /// @brief
    virtual GEMWebApplication* getWebApp() const { return p_gemWebInterface; };

    log4cplus::Logger m_gemLogger /* = member-init-list */;

    xdata::InfoSpace* p_appInfoSpace /* = member-init-list */;

    GEMWebApplication* p_gemWebInterface = nullptr;

    const std::unique_ptr<core::layout_tree::root_node>& layout_tree() const;

public:
    struct dacScanInfoParam {
        xdata::String name = "";
        xdata::UnsignedInteger32 min = 0;
        xdata::UnsignedInteger32 max = 0;
        xdata::Boolean doScan = 0;

        void registerFields(xdata::Bag<dacScanInfoParam>* bag);
        std::string toString() const;
    };

    struct ScanInfo {
        xdata::UnsignedInteger32 adcExtType = 0;
        xdata::Boolean applyChannelMask = true;
        xdata::Boolean calDataFormat = false;
        xdata::UnsignedInteger32 calPulseAmplitude = 0;
        xdata::UnsignedInteger32 calPulseDuration = 0;
        xdata::UnsignedInteger32 calPulseFS = 0;
        xdata::UnsignedInteger32 calPulsePhase = 0;
        xdata::UnsignedInteger32 comparatorMode = 0;
        xdata::Boolean cyclic = false;
        xdata::Vector<xdata::Bag<dacScanInfoParam>> dacScanInfo = { /* empty */ };
        xdata::Boolean doVFATPhaseScan = false;
        xdata::Boolean doSingleChannel = false;
        xdata::UnsignedInteger32 eomSamplesBX = 0;
        xdata::UnsignedInteger32 iterNum = 0;
        xdata::UnsignedInteger32 l1aInterval = 0;
        xdata::UnsignedInteger32 latency = 0;
        xdata::UnsignedInteger32 lpgbtEqAttnGain = 0;
        xdata::UnsignedInteger64 nTransactions = 0;
        xdata::UnsignedInteger64 nTriggers = 0;
        xdata::UnsignedInteger32 pulseDelay = 0;
        xdata::UnsignedInteger32 pulseStretch = 0;
        xdata::UnsignedInteger32 scanMax = 0;
        xdata::UnsignedInteger32 scanMin = 0;
        xdata::UnsignedInteger32 scanType = 1; // Defaults to physics
        xdata::UnsignedInteger32 signalSourceType = 0;
        xdata::UnsignedInteger32 stepSize = 0;
        xdata::UnsignedInteger32 threshold = 0;
        xdata::UnsignedInteger32 thresholdSourceType = 0;
        xdata::UnsignedInteger32 thresholdType = 0;
        xdata::UnsignedInteger32 timeInterval = 0;
        xdata::Boolean toggleRunMode = 0;
        xdata::UnsignedInteger32 trigType = 0;
        xdata::Integer32 trimming = 0;
        xdata::UnsignedInteger32 trimmingSourceType = 0;
        xdata::UnsignedInteger32 vfatCh = 0;
        xdata::UnsignedInteger32 vfatChMax = 0;
        xdata::UnsignedInteger32 vfatChMin = 0;

        void registerFields(xdata::Bag<ScanInfo>* bag);
        std::string toString() const;
    };

    /// various application properties
    const xdaq::ApplicationDescriptor* p_appDescriptor /* = member-init-list */;
    const std::string m_xmlClass /* = member-init-list */;
    uint32_t m_instance /* = member-init-list */;
    const std::string m_urn /* = member-init-list */;

    xdaq::ApplicationContext* p_appContext /* = member-init-list */;
    const xdaq::Zone* p_appZone /* = member-init-list */;
    const xdaq::ApplicationGroup* p_appGroup /* = member-init-list */;

    xoap::MessageReference calibParamRetrieve(xoap::MessageReference mns);

    /// @brief Converts a run number into string
    ///
    /// @param @c runNumber Run number
    /// @param @c isLocalRunNumber Whether the run number is locally or globally assigned
    static std::string runNumberToString(uint32_t runNumber, bool isLocalRunNumber);

    /// @brief Converts the run current number into string
    std::string runNumberToString();

protected:
    // Configuration parameters
    xdata::UnsignedInteger32 m_runNumber = 0;
    xdata::Boolean m_isLocalRunNumber = true; ///< Set when the run number is locally assigned

    // Scan parameters
    xdata::Bag<ScanInfo> m_scanInfo;
    bool m_scanInfoNew = true; ///< Set to true when the scan parameters bag as been updated and a (re-)configuration is needed
};

} // namespace gem::utils

#endif // GEM_UTILS_GEMAPPLICATION_H
