/// @brief default called function. Sets up the state updating
///
/// @param variable with <URN>/jsonUpdate in it. Parsed for the urn
function startUpdate( statejson )
{
    // Done because generic stateUpdate calls jsonUpdate function
    var urn = statejson.replace("/jsonUpdate", "");

    setInterval('staterequest("'+urn+'")', 1000);
}

/// @brief function that updates the state of the xdaq app
///
/// Updates the state displayed in the control panel. If the state has change
/// the buttons are updated as well
///
/// @param urn for the xdaq app
function staterequest( urn )
{
    $.getJSON(urn+"/stateUpdate")
        .done(function(data) {
            newState = data["fsm-state"];
            if ($("#fsm-state").attr("state") != newState) {
                /// Clear old buttons and remake the buttons
                $('#Buttons').html("");
                if ("transitions" in data) {
                    data["transitions"].forEach(function(buttonId) {
                        $("<button>", {
                            id: buttonId,
                            type: "button",
                            html: buttonId,
                            class: "btn btn-lg btn-primary",
                            onClick: "changeState(\""+buttonId+"\", \""+urn + "\")"
                        }).appendTo("#Buttons");
                    });
                }
            }
            $("#fsm-state").html("MONITORING STATE: " + newState)
                .attr("state", newState);
        })
        .fail(function(data, textStatus, error) {
            console.error("staterequest: get failed, status: " + textStatus + ", error: "+error);
        });
}

/// @brief Changes state of the FSM application and updates the HTML
///
/// @param command to be send to the xdaq application
/// @param urn corresponding to the xdaq application
function changeState(command, urn) {
    indata = {"action": command};
    $.post(urn+"/xgiChangeState", indata);

    staterequest(urn);
}
