cmsgemos_add_module(
    utils
    SOURCES
    version.cpp
    GEMApplication.cpp
    GEMFSM.cpp
    GEMFSMApplication.cpp
    GEMGenericFSMApplication.cpp
    GEMRegisterUtils.cpp
    GEMWebApplication.cpp
    Lock.cpp
    soap/GEMSOAPToolBox.cpp
)

target_link_libraries(gemutils PUBLIC gemcore_layout-tree)

target_link_libraries(gemutils PUBLIC xDAQ::cgicc)
target_link_libraries(gemutils PUBLIC xDAQ::log4cplus)
target_link_libraries(gemutils PUBLIC xDAQ::toolbox)
target_link_libraries(gemutils PUBLIC xDAQ::xcept)
target_link_libraries(gemutils PUBLIC xDAQ::xdaq)
target_link_libraries(gemutils PUBLIC xDAQ::xdata)
target_link_libraries(gemutils PUBLIC xDAQ::xerces-c)
target_link_libraries(gemutils PUBLIC xDAQ::xgi)
target_link_libraries(gemutils PUBLIC xDAQ::xoap)

target_link_libraries(gemutils PUBLIC Boost::boost) # for Boost.Assignment

target_link_libraries(gemutils PRIVATE xDAQ::config)

target_link_libraries(gemutils PRIVATE fmt::fmt)
