/// @file

#include <gem/utils/soap/GEMSOAPToolBox.h>

xoap::MessageReference gem::utils::soap::makeSOAPFaultReply(
    const std::string& faultString,
    const std::string& faultCode,
    const std::string& detail,
    const std::string& faultActor)
{
    log4cplus::Logger m_gemLogger(log4cplus::Logger::getInstance("GEMSOAPToolBoxLogger"));
    xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName faultName = envelope.createName("Fault", "xdaq", XDAQ_NS_URI);
    xoap::SOAPElement bodyElement = envelope.getBody().addBodyElement(faultName);

    xoap::SOAPName faultStringName = envelope.createName("faultstring", "xdaq", XDAQ_NS_URI);
    xoap::SOAPElement faultStringElement = bodyElement.addChildElement(faultStringName);
    faultStringElement.addTextNode(faultString);

    xoap::SOAPName faultCodeName = envelope.createName("faultcode", "xdaq", XDAQ_NS_URI);
    xoap::SOAPElement faultCodeElement = bodyElement.addChildElement(faultCodeName);
    faultCodeElement.addTextNode(faultCode);

    if (detail.size() > 0) {
        xoap::SOAPName detailName = envelope.createName("detail", "xdaq", XDAQ_NS_URI);
        xoap::SOAPElement detailElement = bodyElement.addChildElement(detailName);
        detailElement.addTextNode(detail);
    }

    if (faultActor.size() > 0) {
        xoap::SOAPName faultActorName = envelope.createName("faultactor", "xdaq", XDAQ_NS_URI);
        xoap::SOAPElement faultActorElement = bodyElement.addChildElement(faultActorName);
        faultActorElement.addTextNode(faultActor);
    }

    return reply;
}

xoap::MessageReference gem::utils::soap::makeSOAPReply(
    const std::string& command,
    const std::string& response)
{
    log4cplus::Logger m_gemLogger(log4cplus::Logger::getInstance("GEMSOAPToolBoxLogger"));
    xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName responseName = envelope.createName(command, "xdaq", XDAQ_NS_URI);
    xoap::SOAPElement bodyElement = envelope.getBody().addBodyElement(responseName);
    bodyElement.addTextNode(response);
    return reply;
}

xoap::MessageReference gem::utils::soap::makeFSMSOAPReply(
    const std::string& event,
    const std::string& state)
{
    log4cplus::Logger m_gemLogger(log4cplus::Logger::getInstance("GEMSOAPToolBoxLogger"));

    xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPBody body = envelope.getBody();
    std::string responseString = event + "Response";
    xoap::SOAPName responseName = envelope.createName(responseString, "xdaq", XDAQ_NS_URI);
    xoap::SOAPBodyElement responseElement = body.addBodyElement(responseName);
    xoap::SOAPName stateName = envelope.createName("state", "xdaq", XDAQ_NS_URI);
    xoap::SOAPElement stateElement = responseElement.addChildElement(stateName);
    xoap::SOAPName attributeName = envelope.createName("stateName", "xdaq", XDAQ_NS_URI);
    stateElement.addAttribute(attributeName, state);
    CMSGEMOS_DEBUG("makeFSMSOAPReply reply ");
    std::string tool;
    reply->writeTo(tool);
    CMSGEMOS_DEBUG(tool);
    return reply;
}

void gem::utils::soap::sendCommand(
    const std::string& command,
    xdaq::ApplicationContext* appCxt,
    const xdaq::ApplicationDescriptor* srcDsc,
    const xdaq::ApplicationDescriptor* destDsc)
{
    log4cplus::Logger m_gemLogger(log4cplus::Logger::getInstance("GEMSOAPToolBoxLogger"));

    try {
        xoap::MessageReference message = xoap::createMessage();
        xoap::SOAPEnvelope soap_envelope = message->getSOAPPart().getEnvelope();
        xoap::SOAPName soap_command = soap_envelope.createName(command, "xdaq", XDAQ_NS_URI);
        xoap::SOAPBodyElement soap_container = soap_envelope.getBody().addBodyElement(soap_command);

        if (destDsc->getClassName().find("tcds") != std::string::npos) {
            const std::string requestor_id = srcDsc->getContextDescriptor()->getURL() + "?id=" + std::to_string(srcDsc->getLocalId());
            xoap::SOAPName soap_origin = soap_envelope.createName("actionRequestorId", "xdaq", XDAQ_NS_URI);
            soap_container.addAttribute(soap_origin, requestor_id);
        }

        {
            std::string tool;
            message->writeTo(tool);
            CMSGEMOS_DEBUG("sendCommand: SOAP message is: " << tool);
        }

        auto reply = appCxt->postSOAP(message, *srcDsc, *destDsc);

        {
            std::string tool;
            reply->writeTo(tool);
            CMSGEMOS_DEBUG("sendCommand: SOAP reply is: " << tool);
        }
    } catch (const std::exception& e) {
        std::string errMsg = toolbox::toString("Send command failed: %s", e.what());
        XCEPT_RAISE(gem::utils::exception::SOAPException, errMsg);
    } catch (...) {
        std::string errMsg = toolbox::toString("Send command failed");
        XCEPT_RAISE(gem::utils::exception::SOAPException, errMsg);
    }
}

void gem::utils::soap::sendCommandWithParameters(
    const std::string& command,
    const std::unordered_map<std::string, xdata::Serializable*>& parameters,
    xdaq::ApplicationContext* appCxt,
    const xdaq::ApplicationDescriptor* srcDsc,
    const xdaq::ApplicationDescriptor* destDsc)
{
    log4cplus::Logger m_gemLogger(log4cplus::Logger::getInstance("GEMSOAPToolBoxLogger"));

    try {
        xoap::MessageReference message = xoap::createMessage();
        xoap::SOAPEnvelope soap_envelope = message->getSOAPPart().getEnvelope();
        xoap::SOAPName soap_command = soap_envelope.createName(command, "xdaq", XDAQ_NS_URI);
        xoap::SOAPBodyElement soap_container = soap_envelope.getBody().addBodyElement(soap_command);

        if (destDsc->getClassName().find("tcds") != std::string::npos) {
            const std::string requestor_id = srcDsc->getContextDescriptor()->getURL() + "?id=" + std::to_string(srcDsc->getLocalId());
            xoap::SOAPName soap_origin = soap_envelope.createName("actionRequestorId", "xdaq", XDAQ_NS_URI);
            soap_container.addAttribute(soap_origin, requestor_id);
        }

        xdata::soap::Serializer serializer;
        for (const auto& parameter : parameters) {
            const std::string& name = parameter.first;
            xdata::Serializable* serializable = parameter.second;

            xoap::SOAPName soap_name = soap_envelope.createName(name, "xdaq", XDAQ_NS_URI);
            xoap::SOAPElement soap_element = soap_container.addChildElement(soap_name);

            serializer.exportAll(serializable, dynamic_cast<DOMElement*>(soap_element.getDOM()), true);
        }

        {
            std::string tool;
            message->writeTo(tool);
            CMSGEMOS_DEBUG("sendCommandWithParameters: SOAP message is: " << tool);
        }

        auto reply = appCxt->postSOAP(message, *srcDsc, *destDsc);

        {
            std::string tool;
            reply->writeTo(tool);
            CMSGEMOS_DEBUG("sendCommandWithParameters: SOAP reply is: " << tool);
        }
    } catch (const std::exception& e) {
        std::string errMsg = toolbox::toString("Send command with parameters failed: %s", e.what());
        XCEPT_RAISE(gem::utils::exception::SOAPException, errMsg);
    } catch (...) {
        std::string errMsg = toolbox::toString("Send command with parameters failed");
        XCEPT_RAISE(gem::utils::exception::SOAPException, errMsg);
    }
}

std::string gem::utils::soap::extractCommandName(const xoap::MessageReference& msg)
{
    log4cplus::Logger m_gemLogger(log4cplus::Logger::getInstance("GEMSOAPToolBoxLogger"));
    xoap::SOAPPart part = msg->getSOAPPart();
    xoap::SOAPEnvelope env = part.getEnvelope();
    xoap::SOAPBody body = env.getBody();

    DOMNode* node = body.getDOMNode();
    DOMNodeList* bodyList = node->getChildNodes();

    // The body should contain a single node with the name of the FSM
    // command to execute.
    if (bodyList->getLength() != 1) {
        XCEPT_RAISE(xoap::exception::Exception,
            toolbox::toString("Expected exactly one element "
                              "in GEMFSM command SOAP message, "
                              "but found %d.",
                bodyList->getLength()));
    }
    return xoap::XMLCh2String((bodyList->item(0))->getLocalName());
}

xoap::SOAPElement gem::utils::soap::extractSOAPCommandParameterElement(
    const xoap::MessageReference& msg,
    const std::string& parameterName)
{
    log4cplus::Logger m_gemLogger(log4cplus::Logger::getInstance("GEMSOAPToolBoxLogger"));

    for (auto li : msg->getSOAPPart().getEnvelope().getBody().getChildElements()) {
        xoap::SOAPName name = (li).getElementName();
        std::vector<xoap::SOAPElement> children = (li).getChildElements();
        std::vector<xoap::SOAPElement>::iterator vi;

        for (vi = children.begin(); vi != children.end(); ++vi) {
            xoap::SOAPName childName = (*vi).getElementName();

            if (childName.getLocalName() == parameterName) {
                CMSGEMOS_DEBUG("extractSOAPCommandParameterElementc loading parameter  : " << childName.getLocalName());
                return *vi;
            }
        }
    }
    const std::string msgError = toolbox::toString("Failed to import SOAP command parameter with name 'xdaq:%s'.",
        parameterName.c_str());
    XCEPT_RAISE(gem::utils::exception::SOAPException, msgError);
}

void gem::utils::soap::sendApplicationParameters(
    const std::unordered_map<std::string, xdata::Serializable*>& parameters,
    xdaq::ApplicationContext* appCxt,
    const xdaq::ApplicationDescriptor* srcDsc,
    const xdaq::ApplicationDescriptor* destDsc)
{
    log4cplus::Logger m_gemLogger(log4cplus::Logger::getInstance("GEMSOAPToolBoxLogger"));

    const std::string application_urn = "urn:xdaq-application:" + destDsc->getClassName();

    try {
        xoap::MessageReference message = xoap::createMessage();

        xoap::SOAPEnvelope soap_envelope = message->getSOAPPart().getEnvelope();
        soap_envelope.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
        soap_envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
        soap_envelope.addNamespaceDeclaration("soapenc", "http://schemas.xmlsoap.org/soap/encoding/");

        xoap::SOAPName soap_command = soap_envelope.createName("ParameterSet", "xdaq", XDAQ_NS_URI);
        xoap::SOAPBodyElement soap_container = soap_envelope.getBody().addBodyElement(soap_command);

        xoap::SOAPName soap_properties_name = soap_envelope.createName("Properties", "props", application_urn);
        xoap::SOAPName soap_type_name = soap_envelope.createName("type", "xsi", "http://www.w3.org/2001/XMLSchema-instance");
        xoap::SOAPElement soap_pbox = soap_container.addChildElement(soap_properties_name);
        soap_pbox.addAttribute(soap_type_name, "soapenc:Struct");

        xdata::soap::Serializer serializer;
        for (const auto& parameter : parameters) {
            const std::string& name = parameter.first;
            xdata::Serializable* serializable = parameter.second;

            xoap::SOAPName soap_name = soap_envelope.createName(name, "props", application_urn);
            xoap::SOAPElement soap_element = soap_pbox.addChildElement(soap_name);

            serializer.exportAll(serializable, dynamic_cast<DOMElement*>(soap_element.getDOM()), false);
        }

        {
            std::string tool;
            message->writeTo(tool);
            CMSGEMOS_INFO("sendApplicationParameter message: " << tool);
        }

        auto reply = appCxt->postSOAP(message, *srcDsc, *destDsc);

        {
            std::string tool;
            reply->writeTo(tool);
            CMSGEMOS_INFO("sendApplicationParameter reply: " << tool);
        }
    } catch (const std::exception& e) {
        std::string errMsg = toolbox::toString("Send application parameters failed: %s", e.what());
        XCEPT_RAISE(gem::utils::exception::SOAPException, errMsg);
    } catch (...) {
        std::string errMsg = toolbox::toString("Send application parameters failed");
        XCEPT_RAISE(gem::utils::exception::SOAPException, errMsg);
    }
}

xoap::MessageReference gem::utils::soap::createStateRequestMessage(
    const std::string& nstag,
    const std::string& appURN,
    const bool isGEMApp)
{
    log4cplus::Logger m_gemLogger(log4cplus::Logger::getInstance("GEMSOAPToolBoxLogger"));
    xoap::MessageReference msg = xoap::createMessage();

    xoap::SOAPEnvelope env = msg->getSOAPPart().getEnvelope();
    xoap::SOAPName soapcmd = env.createName("ParameterGet", "xdaq", XDAQ_NS_URI);
    xoap::SOAPName tname = env.createName("type", "xsi", "http://www.w3.org/2001/XMLSchema-instance");
    xoap::SOAPElement container = env.getBody().addBodyElement(soapcmd);
    env.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
    env.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
    env.addNamespaceDeclaration("soapenc", "http://schemas.xmlsoap.org/soap/encoding/");
    xoap::SOAPName pboxname = env.createName("properties", nstag, appURN);
    xoap::SOAPElement prop = container.addChildElement(pboxname);
    prop.addAttribute(tname, "soapenc:Struct");

    if (isGEMApp) {
        xoap::SOAPName msgN = env.createName("StateMessage", nstag, appURN);
        xoap::SOAPElement msgE = prop.addChildElement(msgN);
        xoap::SOAPName stateN = env.createName("StateName", nstag, appURN);
        xoap::SOAPElement stateE = prop.addChildElement(stateN);
        msgE.addAttribute(tname, "xsd:string");
        stateE.addAttribute(tname, "xsd:string");
    } else {
        xoap::SOAPName stateN = env.createName("stateName", nstag, appURN);
        xoap::SOAPElement stateE = prop.addChildElement(stateN);
        stateE.addAttribute(tname, "xsd:string");
    }

    return msg;
}

std::string gem::utils::soap::getApplicationState(
    xdaq::ApplicationContext* appCxt,
    const xdaq::ApplicationDescriptor* srcDsc,
    const xdaq::ApplicationDescriptor* destDsc)
{
    log4cplus::Logger m_gemLogger(log4cplus::Logger::getInstance("GEMSOAPToolBoxLogger"));
    try {
        const std::string appClass = destDsc->getClassName();
        const std::string appURN = "urn:xdaq-application:" + appClass;

        std::string responseName = "stateName";
        bool isGEMApp = false;
        if (appClass.find("gem::") != std::string::npos) {
            responseName = "StateName";
            isGEMApp = true;
        }

        xoap::MessageReference msg = xoap::createMessage(gem::utils::soap::createStateRequestMessage("app", appURN, isGEMApp));
        std::string nstag = "gemapp";

        std::stringstream debugstream;
        msg->writeTo(debugstream);

        xoap::MessageReference answer = appCxt->postSOAP(msg, *srcDsc, *destDsc);

        xoap::SOAPName stateReply(responseName, nstag, appURN);
        xoap::SOAPElement props = answer->getSOAPPart().getEnvelope().getBody().getChildElements()[0].getChildElements()[0];
        std::vector<xoap::SOAPElement> basic = props.getChildElements(stateReply);

        if (basic.size() == 1) {
            std::string stateString = basic[0].getValue();
            CMSGEMOS_DEBUG("createStateRequestMessage application " << destDsc->getClassName()
                                                                    << " returned state " << stateString);
            return stateString;
        } else {
            std::string toolInput;
            xoap::dumpTree(msg->getSOAPPart().getEnvelope().getDOMNode(), toolInput);
            std::string toolOutput;
            xoap::dumpTree(answer->getSOAPPart().getEnvelope().getDOMNode(), toolOutput);

            if (answer->getSOAPPart().getEnvelope().getBody().hasFault()) {
                std::stringstream errMsg;
                errMsg << "SOAP fault getting state: " << std::endl
                       << "SOAP request:" << std::endl
                       << toolInput << std::endl
                       << "SOAP reply:" << std::endl
                       << toolOutput << std::endl
                       << "SOAP fault string:" << std::endl
                       << answer->getSOAPPart().getEnvelope().getBody().getFault().getFaultString();
                XCEPT_RAISE(gem::utils::exception::SOAPException, errMsg.str());
            } else {
                XCEPT_RAISE(gem::utils::exception::SOAPException, "Unexpected SOAP message structure");
            }
        }
    } catch (const std::exception& e) {
        std::string errMsg = toolbox::toString("Get application state parameter failed: %s", e.what());
        XCEPT_RAISE(gem::utils::exception::SOAPException, errMsg);
    } catch (...) {
        std::string errMsg = toolbox::toString("Get application state parameter failed");
        XCEPT_RAISE(gem::utils::exception::SOAPException, errMsg);
    }
}
