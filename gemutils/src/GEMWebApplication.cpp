/// @file

#include <gem/utils/GEMWebApplication.h>

#include <gem/utils/GEMApplication.h>
#include <gem/utils/GEMFSM.h>
#include <gem/utils/GEMFSMApplication.h>
#include <gem/utils/soap/GEMSOAPToolBox.h>

#include <nlohmann/json.hpp>
#include <xcept/tools.h>
#include <xgi/Input.h>
#include <xgi/Method.h>
#include <xgi/Output.h>
#include <xgi/framework/UIManager.h>

void gem::utils::tab_creator::render_tabs(xgi::Input* in, xgi::Output* out)
{
    // Setup tabs
    *out << "<ul class=\"nav nav-tabs mb-3\" style=\"font-size:16px\" role=\"tablist\">";
    for (size_t i = 0; i < m_tabs.size(); ++i) {
        *out << "  <li class=\"nav-item\">"
             << "    <a class=\"nav-link" << (i == 0 ? " active" : "")
             << "\" data-toggle=\"tab\" href=\"#" << m_tabs.at(i).html_id << "\">"
             << m_tabs.at(i).title << "</a></li>";
    }
    *out << "</ul>" << std::endl;

    // Setup tab content
    *out << "<div class=\"tab-content\">" << std::endl;
    for (size_t i = 0; i < m_tabs.size(); ++i) {
        *out << "  <div id=\"" << m_tabs.at(i).html_id << "\" class=\"tab-pane"
             << (i == 0 ? " show active" : "") << "\">" << std::endl;
        m_tabs.at(i).function(in, out);
        *out << "  </div>" << std::endl;
    }
    *out << "</div>" << std::endl;
}

gem::utils::GEMWebApplication::GEMWebApplication(gem::utils::GEMApplication* application)
    : m_gemLogger(application->getApplicationLogger())
    , p_gemFSMApp(nullptr)
    , p_gemApp(application)
{
}

gem::utils::GEMWebApplication::GEMWebApplication(gem::utils::GEMFSMApplication* application)
    : m_gemLogger(application->getApplicationLogger())
    , p_gemFSMApp(application)
    , p_gemApp(application)
{
}

gem::utils::GEMWebApplication::~GEMWebApplication()
{
}

void gem::utils::GEMWebApplication::webDefault(xgi::Input* in, xgi::Output* out)
{
    *out << "<link href=\"/cmsgemos/gemutils/html/css/bootstrap.css\" rel=\"stylesheet\">" << std::endl;
    *out << "<script src=\"/cmsgemos/gemutils/html/scripts/bootstrap.bundle.js\"></script>" << std::endl;
    *out << "<script src=\"/cmsgemos/gemutils/html/scripts/gemwebapp.js\"></script>" << std::endl;

    render_tabs(in, out);

    *out << " <div class=\"gem-push\"></div>" << std::endl;

    webFooterGEM(in, out);

    std::string updateLink = "/" + p_gemApp->m_urn + "/jsonUpdate";
    *out << "<script type=\"text/javascript\">" << std::endl
         << "    startUpdate( \"" << updateLink << "\" );" << std::endl
         << "</script>" << std::endl;
}

void gem::utils::GEMWebApplication::webFooterGEM(xgi::Input* in, xgi::Output* out)
{
    *out << "<div class=\"gem-footer\" id=\"xdaq-footer\">" << std::endl;
    *out << "GEM DAQ GIT_VERSION:" << GIT_VERSION
         << " -- developer:" << GEMDEVELOPER << std::endl
         << "</div>" << std::endl;
}

/*To be filled in with the control page code (only for FSM derived classes?*/
void gem::utils::GEMWebApplication::controlPanel(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_DEBUG("GEMWebApplication::controlPanel");
    // maybe the control part should only be displayed if the application is not supervised?
    if (p_gemFSMApp) {
        *out << "<script src=\"/cmsgemos/gemutils/html/scripts/gemfsmwebcontrol.js\"></script>" << std::endl;

        std::string updateLink = "/" + p_gemApp->m_urn + "/stateUpdate";
        *out << "<script type=\"text/javascript\">" << std::endl
             << "    updateStateTable( \"" << updateLink << "\" );" << std::endl
             << "</script>" << std::endl;

        try {
            std::string state = dynamic_cast<gem::utils::GEMFSMApplication*>(p_gemFSMApp)->getCurrentState();
            CMSGEMOS_DEBUG("GEMWebApplication::controlPanel:: current state " << state);

            *out << "<p class=\"h4\">" << std::endl
                 << "<span class=\"text-muted\">Run number  </span>" << std::endl
                 << "<span class=\"text-body\" id=\"run-number\"></span>" << std::endl
                 << "</p>" << std::endl;

            *out << "<table class=\"xdaq-table\">" << std::endl
                 << "<thead><tr>   <th>Control</th>   <th>State</th>   </tr></thead>" << std::endl
                 << "<tbody>" << std::endl
                 << "<tr>" << std::endl
                 << "<td>" << std::endl;

            *out << "<table class=\"xdaq-table\">" << std::endl;
            // Buttons                     | Initial             | Halted               | Configured           | Running            | Paused              |
            // Initialize Configure        | Configure invisible | Initialize invisible | Initialize invisible | All invisible      | All invisible       |
            // (Start Stop) (Pause Resume) | All invisible       | All invisible        | Start visible        | Stop/Pause visible | Stop/Resume visible |
            // Halt                        | All invisible       | All visible          | All visible          | All visible        | All visible         |

            *out << "  <tr class=\"hide\" id=\"initconf\">" << std::endl
                 << "    <td>" << std::endl
                 << "      <button class=\"hide\" id=\"init\" onclick=\"gemFSMWebCommand(\'Initialize\',\'/"
                 << p_gemApp->m_urn << "\')\">Initialize</button>"
                 << std::endl
                 << "    </td>" << std::endl
                 << "    <td>" << std::endl
                 << "<button class=\"hide\" id=\"conf\" onclick=\"gemFSMWebCommand(\'Configure\',\'/"
                 << p_gemApp->m_urn << "\')\">Configure</button>" << std::endl
                 << "    </td>" << std::endl
                 << "  </tr>" << std::endl;

            *out << "  <tr class=\"hide\" id=\"startstop\">" << std::endl
                 << "    <td>" << std::endl
                 << "      <button class=\"hide\" id=\"start\" onclick=\"gemFSMWebCommand(\'Start\',\'/"
                 << p_gemApp->m_urn << "\')\">Start</button>"
                 << "      <button class=\"hide\" id=\"stop\" onclick=\"gemFSMWebCommand(\'Stop\',\'/"
                 << p_gemApp->m_urn << "\')\">Stop</button>" << std::endl
                 << "    </td>" << std::endl
                 << "    <td>" << std::endl
                 << "      <button class=\"hide\" id=\"pause\" onclick=\"gemFSMWebCommand(\'Pause\',\'/"
                 << p_gemApp->m_urn << "\')\">Pause</button>"
                 << "      <button class=\"hide\" id=\"resume\" onclick=\"gemFSMWebCommand(\'Resume\',\'/"
                 << p_gemApp->m_urn << "\')\">Resume</button>" << std::endl
                 << "    </td>" << std::endl
                 << "  </tr>" << std::endl;

            *out << "  <tr class=\"hide\" id=\"haltrow\">" << std::endl
                 << "    <td>" << std::endl
                 << "      <button class=\"hide\" id=\"halt\" onclick=\"gemFSMWebCommand(\'Halt\',\'/"
                 << p_gemApp->m_urn << "\')\">Halt</button>" << std::endl
                 << "    </td>" << std::endl
                 << "    <td>" << std::endl
                 << "    </td>" << std::endl
                 << "  </tr>" << std::endl;

            *out << "</table>" << std::endl
                 << "<br />" << std::endl
                 << "Last command was: " << std::endl
                 << "<div id=\"fsmdebug\"></div>" << std::endl
                 << "</td>" << std::endl
                 << "<td>" << std::endl
                 << "<h3 id=\"fsm-state\">"
                 // change the colour to red if failed maybe
                 << dynamic_cast<gem::utils::GEMFSMApplication*>(p_gemFSMApp)->getCurrentState()
                 << "</h3>" << std::endl
                 << "</td>" << std::endl
                 << "</tr>" << std::endl
                 << "</tbody>" << std::endl
                 << "</table>" << std::endl;
        } catch (xgi::exception::Exception const& e) {
            CMSGEMOS_ERROR("GEMWebApplication::Something went wrong displaying web control panel(xgi): " << e.what());
            XCEPT_RAISE(xgi::exception::Exception, e.what());
        } catch (std::exception const& e) {
            CMSGEMOS_ERROR("GEMWebApplication::Something went wrong displaying web control panel(std): " << e.what());
            XCEPT_RAISE(xgi::exception::Exception, e.what());
        }
    } // only when the GEMFSM has been created
}

void gem::utils::GEMWebApplication::jsonStateUpdate(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_DEBUG("GEMWebApplication::jsonStateUpdate");

    nlohmann::json jsonFile;
    jsonFile["fsm-state"] = dynamic_cast<gem::utils::GEMFSMApplication*>(p_gemFSMApp)->getCurrentState();
    jsonFile["run-number"] = p_gemFSMApp->runNumberToString();

    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
    *out << jsonFile.dump(4) << std::endl;
}

void gem::utils::GEMWebApplication::jsonUpdate(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_DEBUG("GEMWebApplication::jsonUpdate");
    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
    *out << "{}" << std::endl;
}
