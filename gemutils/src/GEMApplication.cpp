/// @file

#include <gem/utils/GEMApplication.h>

#include <gem/utils/GEMWebApplication.h>

#include <xgi/framework/Method.h>

#include <fmt/format.h>

void gem::utils::GEMApplication::dacScanInfoParam::registerFields(xdata::Bag<dacScanInfoParam>* bag)
{
    bag->addField("doScan", &doScan);
    bag->addField("max", &max);
    bag->addField("min", &min);
    bag->addField("name", &name);
}

std::string gem::utils::GEMApplication::dacScanInfoParam::toString() const
{
    std::stringstream os;
    os << "doScan: " << doScan.value_ << std::endl
       << "max: " << max.value_ << std::endl
       << "min: " << min.value_ << std::endl
       << "name: " << name.value_ << std::endl;
    return os.str();
}

void gem::utils::GEMApplication::ScanInfo::registerFields(xdata::Bag<ScanInfo>* bag)
{
    bag->addField("adcExtType", &adcExtType);
    bag->addField("applyChannelMask", &applyChannelMask);
    bag->addField("calDataFormat", &calDataFormat);
    bag->addField("calPulseAmplitude", &calPulseAmplitude);
    bag->addField("calPulseDuration", &calPulseDuration);
    bag->addField("calPulseFS", &calPulseFS);
    bag->addField("calPulsePhase", &calPulsePhase);
    bag->addField("comparatorMode", &comparatorMode);
    bag->addField("eomSamplesBX", &eomSamplesBX);
    bag->addField("cyclic", &cyclic);
    bag->addField("dacScanInfo", &dacScanInfo);
    bag->addField("doSingleChannel", &doSingleChannel);
    bag->addField("doVFATPhaseScan", &doVFATPhaseScan);
    bag->addField("lpgbtEqAttnGain", &lpgbtEqAttnGain);
    bag->addField("iterNum", &iterNum);
    bag->addField("l1aInterval", &l1aInterval);
    bag->addField("latency", &latency);
    bag->addField("nTransactions", &nTransactions);
    bag->addField("nTriggers", &nTriggers);
    bag->addField("pulseDelay", &pulseDelay);
    bag->addField("pulseStretch", &pulseStretch);
    bag->addField("scanMax", &scanMax);
    bag->addField("scanMin", &scanMin);
    bag->addField("scanType", &scanType);
    bag->addField("signalSourceType", &signalSourceType);
    bag->addField("stepSize", &stepSize);
    bag->addField("threshold", &threshold);
    bag->addField("thresholdSourceType", &thresholdSourceType);
    bag->addField("thresholdType", &thresholdType);
    bag->addField("timeInterval", &timeInterval);
    bag->addField("toggleRunMode", &toggleRunMode);
    bag->addField("trigType", &trigType);
    bag->addField("trimming", &trimming);
    bag->addField("trimmingSourceType", &trimmingSourceType);
    bag->addField("vfatCh", &vfatCh);
    bag->addField("vfatChMax", &vfatChMax);
    bag->addField("vfatChMin", &vfatChMin);
}

std::string gem::utils::GEMApplication::ScanInfo::toString() const
{
    std::stringstream os;
    os << "adcExtType: " << adcExtType.value_ << std::endl
       << "applyChannelMask: " << applyChannelMask.value_ << std::endl
       << "calDataFormat: " << calDataFormat.value_ << std::endl
       << "calPulseAmplitude: " << calPulseAmplitude.value_ << std::endl
       << "calPulseDuration: " << calPulseDuration.value_ << std::endl
       << "calPulseFS: " << calPulseFS.value_ << std::endl
       << "calPulsePhase: " << calPulsePhase.value_ << std::endl
       << "comparatorMode: " << comparatorMode.value_ << std::endl
       << "cyclic: " << cyclic.value_ << std::endl
       << "dacScanInfo: " << std::endl;
    for (const auto& i : dacScanInfo)
        os << "    " << i.bag.toString() << std::endl;

    os << "doSingleChannel: " << doSingleChannel.value_ << std::endl
       << "doVFATPhaseScan: " << doVFATPhaseScan.value_ << std::endl
       << "eomSamplesBX: " << eomSamplesBX.value_ << std::endl
       << "iterNum: " << iterNum.value_ << std::endl
       << "l1aInterval: " << l1aInterval.value_ << std::endl
       << "latency: " << latency.value_ << std::endl
       << "lpgbtEqAttnGain: " << lpgbtEqAttnGain.value_ << std::endl
       << "nTransactions: " << nTransactions.value_ << std::endl
       << "nTrigger: " << nTriggers.value_ << std::endl
       << "pulseDelay: " << pulseDelay.value_ << std::endl
       << "pulseStretch: " << pulseStretch.value_ << std::endl
       << "scanMax: " << scanMax.toString() << std::endl
       << "scanMin: " << scanMin.toString() << std::endl
       << "scanType: " << scanType.toString() << std::endl
       << "signalSourceType: " << signalSourceType.value_ << std::endl
       << "stepSize: " << stepSize.toString() << std::endl
       << "threshold: " << threshold.value_ << std::endl
       << "thresholdSourceType: " << thresholdSourceType.value_ << std::endl
       << "thresholdType: " << thresholdType.value_ << std::endl
       << "timeInterval: " << timeInterval.value_ << std::endl
       << "toggleRunMode: " << toggleRunMode.value_ << std::endl
       << "trigType: " << trigType.value_ << std::endl
       << "trimming: " << trimming.value_ << std::endl
       << "trimmingSourceType: " << trimmingSourceType.value_ << std::endl
       << "vfatCh: " << vfatCh.value_ << std::endl
       << "vfatChMax: " << vfatChMax.value_ << std::endl
       << "vfatChMin: " << vfatChMin.value_ << std::endl;
    return os.str();
};

gem::utils::GEMApplication::GEMApplication(xdaq::ApplicationStub* stub)
    : xdaq::WebApplication(stub)
    , m_gemLogger(getApplicationLogger())
    , p_appInfoSpace(getApplicationInfoSpace())
    , p_appDescriptor(getApplicationDescriptor())
    , m_xmlClass(p_appDescriptor->getClassName())
    , m_instance(p_appDescriptor->getInstance())
    , m_urn(p_appDescriptor->getURN())
    , p_appContext(getApplicationContext())
    , p_appZone(p_appContext->getDefaultZone())
    , p_appGroup(p_appZone->getApplicationGroup("default"))
{
    CMSGEMOS_INFO("GEMApplication version:" << GIT_VERSION);
    CMSGEMOS_INFO("GEMApplication developer:" << GEMDEVELOPER);

    // Configuration parameters
    p_appInfoSpace->addListener(this, "urn:xdaq-event:setDefaultValues");

    p_appInfoSpace->fireItemAvailable("runNumber", &m_runNumber);
    p_appInfoSpace->fireItemAvailable("isLocalRunNumber", &m_isLocalRunNumber);

    // Web UI
    xgi::framework::deferredbind(this, this, &GEMApplication::xgiDefault, "Default");

    // JSON API
    xgi::deferredbind(this, this, &GEMApplication::jsonUpdate, "jsonUpdate");

    // SOAP API
    xoap::deferredbind(this, this, &GEMApplication::calibParamRetrieve, "calibParamRetrieve", XDAQ_NS_URI);

    // Pre-load the layout tree to catch errors early on
    layout_tree();
}

gem::utils::GEMApplication::~GEMApplication()
{
    CMSGEMOS_DEBUG("GEMApplication::gem::utils::GEMApplication destructor called");
}

std::string gem::utils::GEMApplication::getFullURL()
{
    std::string url = getApplicationDescriptor()->getContextDescriptor()->getURL();
    std::string urn = getApplicationDescriptor()->getURN();
    std::string fullURL = toolbox::toString("%s/%s", url.c_str(), urn.c_str());
    return fullURL;
}

void gem::utils::GEMApplication::actionPerformed(xdata::Event& event)
{
    if (event.type() == "setDefaultValues" || event.type() == "urn:xdaq-event:setDefaultValues") {
    }
}

void gem::utils::GEMApplication::xgiDefault(xgi::Input* in, xgi::Output* out)
{
    p_gemWebInterface->webDefault(in, out);
}

void gem::utils::GEMApplication::jsonUpdate(xgi::Input* in, xgi::Output* out)
{
    p_gemWebInterface->jsonUpdate(in, out);
}

const std::unique_ptr<gem::core::layout_tree::root_node>& gem::utils::GEMApplication::layout_tree() const
{
    static auto layout_tree = core::layout_tree::load();
    return layout_tree;
}

xoap::MessageReference gem::utils::GEMApplication::calibParamRetrieve(xoap::MessageReference msg)
{
    const std::string commandName = "calibParamRetrieve";

    if (msg.isNull()) {
        CMSGEMOS_INFO("GEMApplication::calibParamRetrieve Null message received!");
        XCEPT_RAISE(xoap::exception::Exception, "Null message received!");
    }

    m_scanInfo = gem::utils::soap::extractSOAPCommandParameter<xdata::Bag<ScanInfo>>(msg, "scanInfo");
    m_scanInfoNew = true;

    CMSGEMOS_INFO("GEMApplication::calibParamRetrieve message received - new parameters are: " << std::endl
                                                                                               << m_scanInfo.bag.toString());

    return gem::utils::soap::makeSOAPReply(commandName, "CalibrationParametersRetrieved");
}

std::string gem::utils::GEMApplication::runNumberToString(uint32_t runNumber, const bool isLocalRunNumber)
{
    if (!isLocalRunNumber) {
        return fmt::format(FMT_STRING("{:06d}"), runNumber);
    } else {
        const auto time = runNumber % 100'000;
        runNumber /= 100'000;
        const auto day = runNumber % 10'000;
        runNumber /= 10'000;
        const auto era = runNumber;
        return fmt::format(FMT_STRING("{:01d}.{:04d}.{:05d}L"), era, day, time);
    }
}

std::string gem::utils::GEMApplication::runNumberToString()
{
    return runNumberToString(m_runNumber, m_isLocalRunNumber);
}
