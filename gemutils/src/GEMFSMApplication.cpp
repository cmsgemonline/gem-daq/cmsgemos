/// @file

#include <gem/utils/GEMFSMApplication.h>

#include <gem/utils/GEMFSM.h>
#include <gem/utils/GEMWebApplication.h>
#include <gem/utils/exception/Exception.h>
#include <gem/utils/soap/GEMSOAPToolBox.h>

#include <toolbox/BSem.h>
#include <toolbox/fsm/AsynchronousFiniteStateMachine.h>
#include <toolbox/fsm/FailedEvent.h>
#include <toolbox/fsm/InvalidInputEvent.h>
#include <toolbox/string.h>
#include <toolbox/task/WorkLoopFactory.h>
#include <xcept/Exception.h>
#include <xdaq/ApplicationStub.h>
#include <xdaq/NamespaceURI.h>
#include <xgi/Input.h>
#include <xgi/Method.h>
#include <xgi/Output.h>
#include <xgi/framework/Method.h>
#include <xgi/framework/UIManager.h>
#include <xoap/Method.h>

gem::utils::GEMFSMApplication::GEMFSMApplication(xdaq::ApplicationStub* stub)
    : GEMApplication(stub)
    , m_gemfsm(this)
    , b_accept_web_commands(true)
    , m_wl_semaphore(toolbox::BSem::FULL)
    , m_db_semaphore(toolbox::BSem::FULL)
    , m_cfg_semaphore(toolbox::BSem::FULL)
    , m_web_semaphore(toolbox::BSem::FULL)
    , m_infspc_semaphore(toolbox::BSem::FULL)
{
    // Configuration parameters
    p_appInfoSpace->fireItemAvailable("mustReconfigure", &m_mustReconfigure);

    // Web UI

    // These bindings expose the state machine to the hyperdaq world. The
    // xgi<Action> callback simply creates a SOAP message that then triggers the usual
    // state transition
    xgi::framework::deferredbind(this, this, &GEMFSMApplication::xgiInitialize, "Initialize");
    xgi::framework::deferredbind(this, this, &GEMFSMApplication::xgiConfigure, "Configure");
    xgi::framework::deferredbind(this, this, &GEMFSMApplication::xgiStart, "Start");
    xgi::framework::deferredbind(this, this, &GEMFSMApplication::xgiStop, "Stop");
    xgi::framework::deferredbind(this, this, &GEMFSMApplication::xgiPause, "Pause");
    xgi::framework::deferredbind(this, this, &GEMFSMApplication::xgiResume, "Resume");
    xgi::framework::deferredbind(this, this, &GEMFSMApplication::xgiHalt, "Halt");

    // JSON API
    xgi::deferredbind(this, this, &GEMFSMApplication::jsonStateUpdate, "stateUpdate");

    // SOAP API

    // These bindings expose the state machine to the outside world. The
    // changeState() method simply forwards the calls to the GEMFSM
    // object.
    xoap::deferredbind(this, this, &GEMFSMApplication::changeState, "Initialize", XDAQ_NS_URI);
    xoap::deferredbind(this, this, &GEMFSMApplication::changeState, "Configure", XDAQ_NS_URI);
    xoap::deferredbind(this, this, &GEMFSMApplication::changeState, "Start", XDAQ_NS_URI);
    xoap::deferredbind(this, this, &GEMFSMApplication::changeState, "Stop", XDAQ_NS_URI);
    xoap::deferredbind(this, this, &GEMFSMApplication::changeState, "Pause", XDAQ_NS_URI);
    xoap::deferredbind(this, this, &GEMFSMApplication::changeState, "Resume", XDAQ_NS_URI);
    xoap::deferredbind(this, this, &GEMFSMApplication::changeState, "Halt", XDAQ_NS_URI);
    CMSGEMOS_DEBUG("GEMFSMApplication::Created xoap bindings");

    // benefit or disadvantage to setting up the workloop signatures this way?
    // hcal has done a forwarding which may be a clever solution, but to what problem?
    m_initSig = toolbox::task::bind(this, &GEMFSMApplication::initialize, "initialize");
    m_confSig = toolbox::task::bind(this, &GEMFSMApplication::configure, "configure");
    m_startSig = toolbox::task::bind(this, &GEMFSMApplication::start, "start");
    m_stopSig = toolbox::task::bind(this, &GEMFSMApplication::stop, "stop");
    m_pauseSig = toolbox::task::bind(this, &GEMFSMApplication::pause, "pause");
    m_resumeSig = toolbox::task::bind(this, &GEMFSMApplication::resume, "resume");
    m_haltSig = toolbox::task::bind(this, &GEMFSMApplication::halt, "halt");
    CMSGEMOS_DEBUG("GEMFSMApplication::Created task bindings");

    // Export status in the application InfoSpace
    // lpetre: Can we do better?
    p_appInfoSpace->fireItemAvailable("StateName", &m_stateName);
    p_appInfoSpace->fireItemAvailable("StateMessage", &m_stateMessage);

    // Workloop name
    const std::string class_name = getApplicationDescriptor()->getClassName();
    const uint32_t instance_number = getApplicationDescriptor()->getInstance();

    std::stringstream tmp_workloop_name;
    tmp_workloop_name << class_name << ":" << instance_number << ":fsmTransition";

    workLoopName = tmp_workloop_name.str();
    CMSGEMOS_DEBUG("GEMFSMApplication::Created workloop name " << workLoopName);

    updateState();
}

gem::utils::GEMFSMApplication::~GEMFSMApplication()
{
}

void gem::utils::GEMFSMApplication::xgiInitialize(xgi::Input* in, xgi::Output* out)
{
    std::string msgBase = "[GEMFSMApplication::xgiInitialize] ";
    CMSGEMOS_DEBUG(msgBase << "Begin");
    if (b_accept_web_commands) {
        try {
            CMSGEMOS_DEBUG("GEMFSMApplication::xgiInitialize::Sending SOAP command to application");
            gem::utils::soap::sendCommand("Initialize", p_appContext, p_appDescriptor, p_appDescriptor);
        } catch (toolbox::fsm::exception::Exception const& e) {
            XCEPT_RETHROW(xgi::exception::Exception, "Initialize failed", e);
        }
    }
    CMSGEMOS_DEBUG("GEMFSMApplication::xgiInitialize end");
}

void gem::utils::GEMFSMApplication::xgiConfigure(xgi::Input* in, xgi::Output* out)
{
    std::string msgBase = "[GEMFSMApplication::xgiConfigure] ";
    if (b_accept_web_commands) {
        try {
            CMSGEMOS_DEBUG(msgBase << "Sending SOAP command to application");
            gem::utils::soap::sendCommand("Configure", p_appContext, p_appDescriptor, p_appDescriptor);
        } catch (toolbox::fsm::exception::Exception const& e) {
            XCEPT_RETHROW(xgi::exception::Exception, "Configure failed", e);
        }
    }
}

void gem::utils::GEMFSMApplication::xgiStart(xgi::Input* in, xgi::Output* out)
{
    std::string msgBase = "[GEMFSMApplication::xgiStart] ";
    if (b_accept_web_commands) {
        try {
            CMSGEMOS_DEBUG(msgBase << "Sending SOAP command to application");
            gem::utils::soap::sendCommand("Start", p_appContext, p_appDescriptor, p_appDescriptor);
        } catch (toolbox::fsm::exception::Exception const& e) {
            XCEPT_RETHROW(xgi::exception::Exception, "Start failed", e);
        }
    }
}

void gem::utils::GEMFSMApplication::xgiStop(xgi::Input* in, xgi::Output* out)
{
    std::string msgBase = "[GEMFSMApplication::xgiStop] ";
    if (b_accept_web_commands) {
        try {
            CMSGEMOS_DEBUG(msgBase << "Sending SOAP command to application");
            gem::utils::soap::sendCommand("Stop", p_appContext, p_appDescriptor, p_appDescriptor);
        } catch (toolbox::fsm::exception::Exception const& e) {
            XCEPT_RETHROW(xgi::exception::Exception, "Stop failed", e);
        }
    }
}

void gem::utils::GEMFSMApplication::xgiPause(xgi::Input* in, xgi::Output* out)
{
    std::string msgBase = "[GEMFSMApplication::xgiPause] ";
    if (b_accept_web_commands) {
        try {
            CMSGEMOS_DEBUG(msgBase << "Sending SOAP command to application");
            gem::utils::soap::sendCommand("Pause", p_appContext, p_appDescriptor, p_appDescriptor);
        } catch (toolbox::fsm::exception::Exception const& e) {
            XCEPT_RETHROW(xgi::exception::Exception, "Pause failed", e);
        }
    }
}

void gem::utils::GEMFSMApplication::xgiResume(xgi::Input* in, xgi::Output* out)
{
    std::string msgBase = "[GEMFSMApplication::xgiResume] ";
    if (b_accept_web_commands) {
        try {
            CMSGEMOS_DEBUG(msgBase << "Sending SOAP command to application");
            gem::utils::soap::sendCommand("Resume", p_appContext, p_appDescriptor, p_appDescriptor);
        } catch (toolbox::fsm::exception::Exception const& e) {
            XCEPT_RETHROW(xgi::exception::Exception, "Resume failed", e);
        }
    }
}

void gem::utils::GEMFSMApplication::xgiHalt(xgi::Input* in, xgi::Output* out)
{
    std::string msgBase = "[GEMFSMApplication::xgiHalt] ";
    if (b_accept_web_commands) {
        try {
            CMSGEMOS_DEBUG(msgBase << "Sending SOAP command to application");
            gem::utils::soap::sendCommand("Halt", p_appContext, p_appDescriptor, p_appDescriptor);
        } catch (toolbox::fsm::exception::Exception const& e) {
            XCEPT_RETHROW(xgi::exception::Exception, "Halt failed", e);
        }
    }
}

void gem::utils::GEMFSMApplication::jsonStateUpdate(xgi::Input* in, xgi::Output* out)
{
    p_gemWebInterface->jsonStateUpdate(in, out);
}

/// state transitions
void gem::utils::GEMFSMApplication::transitionDriver(toolbox::Event::Reference event)
{
    std::string msgBase = "[GEMFSMApplication::transitionDriver] ";
    // set a transition message to ""
    CMSGEMOS_DEBUG(msgBase << "(" << event->type() << ")");
    try {
        if (event->type() == "Initialize" || event->type() == "Configure" || event->type() == "Start" || event->type() == "Stop" || event->type() == "Pause" || event->type() == "Resume" || event->type() == "Halt") {
            CMSGEMOS_DEBUG(msgBase << "submitting workloopDriver(" << event->type() << ")");
            workloopDriver(event->type());
            // does this preclude the future "success" message at the end of the catch block?
            return;
        } else if (event->type() == "IsInitial" || event->type() == "IsConfigured" || event->type() == "IsRunning" || event->type() == "IsPaused" || event->type() == "IsHalted") {
            // report success
            CMSGEMOS_DEBUG(msgBase << "Recieved confirmation that state changed to " << event->type());
        } else if (event->type() == "Fail" || event->type() == "fail") {
            // do nothing for the fail action
            CMSGEMOS_DEBUG(msgBase << "Recieved fail event type");
        } else {
            CMSGEMOS_DEBUG(msgBase << "Unknown transition command");
            XCEPT_RAISE(toolbox::fsm::exception::Exception, "Unknown transition command");
        }
    } catch (gem::utils::exception::Exception const& ex) {
        CMSGEMOS_ERROR(msgBase << "Caught gem::utils::exception::Exception");
        fireEvent("Fail");
        // set a transition message to ex.what()
        XCEPT_RETHROW(toolbox::fsm::exception::Exception, "State Transition Failed", ex);
    } /*catch (std::exception& ex) {
    fireEvent("Fail");
    // set a transition message to ex.what()
    XCEPT_RETHROW(toolbox::fsm::exception::Exception, "State Transition Failed", ex);
  } catch (...) {
    fireEvent("Fail");
    // set a transition message to ex.what()
    XCEPT_RETHROW(toolbox::fsm::exception::Exception, "State Transition Failed", "...");
    }*/
    // set a transition message to "Success"
    updateState();
}

void gem::utils::GEMFSMApplication::workloopDriver(std::string const& command)
{
    std::string msgBase = "[GEMFSMApplication::workloopDriver] ";
    CMSGEMOS_DEBUG(msgBase << "workloopDriver begin");
    try {
        toolbox::task::WorkLoopFactory* wlf = toolbox::task::WorkLoopFactory::getInstance();
        CMSGEMOS_DEBUG(msgBase << "Trying to access the workloop with name " << workLoopName);
        toolbox::task::WorkLoop* loop = wlf->getWorkLoop(workLoopName, "waiting");
        if (!loop->isActive())
            loop->activate();
        CMSGEMOS_DEBUG(msgBase << "Workloop should now be active");

        if (command == "Initialize")
            loop->submit(m_initSig);
        else if (command == "Configure")
            loop->submit(m_confSig);
        else if (command == "Start")
            loop->submit(m_startSig);
        else if (command == "Stop")
            loop->submit(m_stopSig);
        else if (command == "Pause")
            loop->submit(m_pauseSig);
        else if (command == "Resume")
            loop->submit(m_resumeSig);
        else if (command == "Halt")
            loop->submit(m_haltSig);
        CMSGEMOS_DEBUG(msgBase << "Workloop should now be submitted");
    } catch (gem::utils::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMFSMApplication::workloopDriver Workloop failure (gem::utils::exception)";
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RETHROW(gem::utils::exception::Exception, msg.str(), e);
    } catch (toolbox::task::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMFSMApplication::workloopDriver Workloop failure (toolbox::task::exception)";
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RETHROW(gem::utils::exception::Exception, msg.str(), e);
    }
    updateState();
    CMSGEMOS_DEBUG(msgBase << "workloopDriver end");
}

void gem::utils::GEMFSMApplication::stateChanged(toolbox::fsm::FiniteStateMachine& fsm)
{
    std::string msgBase = "[GEMFSMApplication::stateChanged] ";
    CMSGEMOS_INFO(msgBase << "stateChanged");
    updateState();
}

void gem::utils::GEMFSMApplication::transitionFailed(toolbox::Event::Reference event)
{
    std::string msgBase = "[GEMFSMApplication::transitionFailed] ";
    CMSGEMOS_WARN(msgBase << "transitionFailed(" << event->type() << ")");
    updateState();
}

void gem::utils::GEMFSMApplication::fireEvent(std::string event)
{
    std::string msgBase = "[GEMFSMApplication::fireEvent] ";
    CMSGEMOS_INFO(msgBase << "fireEvent(" << event << ")");
    try {
        toolbox::Event::Reference e(new toolbox::Event(event, this));
        m_gemfsm.fireEvent(e);
    } catch (toolbox::fsm::exception::Exception const& e) {
        XCEPT_RETHROW(xoap::exception::Exception, "invalid command", e);
    }
    updateState();
}

bool gem::utils::GEMFSMApplication::isStartAllowed()
{
    return !(m_scanInfoNew & m_mustReconfigure);
}

/// SOAP callback
// This simply forwards the message to the GEMFSM object, since it is
// technically not possible to bind directly to anything but an
// xdaq::Application.
xoap::MessageReference gem::utils::GEMFSMApplication::changeState(xoap::MessageReference msg)
{
    std::string msgBase = "[GEMFSMApplication::changeState] ";
    CMSGEMOS_DEBUG(msgBase << "changeState");
    updateState();
    return m_gemfsm.changeState(msg);
}

/// workloop driven transitions
bool gem::utils::GEMFSMApplication::initialize(toolbox::task::WorkLoop* wl)
{
    std::string msgBase = "[GEMFSMApplication::initialize] ";
    m_wl_semaphore.take();
    CMSGEMOS_INFO(msgBase << "initialize called, current state: " << m_gemfsm.getCurrentState());
    // while ((m_gemfsm.getCurrentState()) != m_gemfsm.getStateName(STATE_INITIALIZING)) {  // deal with possible race condition
    while ((m_gemfsm.getCurrentFSMState()) != STATE_INITIALIZING) { // deal with possible race condition
        CMSGEMOS_DEBUG(msgBase << "not in " << STATE_INITIALIZING << " sleeping (" << m_gemfsm.getCurrentState() << ")");
        usleep(10);
    }
    CMSGEMOS_DEBUG(msgBase << "initialize called, current state: " << m_gemfsm.getCurrentState());

    try {
        CMSGEMOS_DEBUG(msgBase << "Calling initializeAction");
        initializeAction();
        CMSGEMOS_DEBUG(msgBase << "Finished initializeAction");
    } catch (gem::utils::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "Caught gem::utils::exception: " << ex.what();
        CMSGEMOS_ERROR(msgBase << msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::task::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "Caught toolbox::task::exception: " << ex.what();
        CMSGEMOS_ERROR(msgBase << msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::net::exception::MalformedURN const& ex) {
        std::stringstream msg;
        msg << "Malformed URN: " << ex.what();
        CMSGEMOS_ERROR(msgBase << msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (std::exception const& ex) {
        std::stringstream msg;
        msg << "Caught std::exception: " << ex.what();
        CMSGEMOS_ERROR(msgBase << msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (...) {
        std::stringstream msg;
        msg << "Caught other exception";
        CMSGEMOS_ERROR(msgBase << msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    }

    std::stringstream msg;
    msg << "GEMFSMApplication::initialize Firing 'IsHalted' into the FSM";
    CMSGEMOS_INFO(msg.str());
    fireEvent("IsHalted");
    m_wl_semaphore.give();
    return false;
}

bool gem::utils::GEMFSMApplication::configure(toolbox::task::WorkLoop* wl)
{
    std::string msgBase = "[GEMFSMApplication::configure] ";
    m_wl_semaphore.take();
    CMSGEMOS_INFO(msgBase << "Called, current state: " << m_gemfsm.getCurrentState());

    while ((m_gemfsm.getCurrentState()) != m_gemfsm.getStateName(STATE_CONFIGURING)) { // deal with possible race condition
        CMSGEMOS_DEBUG(msgBase << "Not in " << STATE_CONFIGURING << " sleeping (" << m_gemfsm.getCurrentState() << ")");
        usleep(10);
    }
    CMSGEMOS_DEBUG(msgBase << "Called, current state: " << m_gemfsm.getCurrentState());

    try {
        configureAction();
        m_scanInfoNew = false;
    } catch (gem::utils::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "Caught gem::utils::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::task::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "Caught toolbox::task::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::net::exception::MalformedURN const& ex) {
        std::stringstream msg;
        msg << "malformed URN: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (std::exception const& ex) {
        std::stringstream msg;
        msg << "caught std::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (...) {
        std::stringstream msg;
        msg << "caught unknown exception";
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    }

    std::stringstream msg;
    msg << "Firing 'IsConfigured' into the FSM";
    CMSGEMOS_INFO(msg.str());
    fireEvent("IsConfigured");
    m_wl_semaphore.give();
    return false;
}

bool gem::utils::GEMFSMApplication::start(toolbox::task::WorkLoop* wl)
{
    std::string msgBase = "[GEMFSMApplication::start] ";
    m_wl_semaphore.take();
    CMSGEMOS_INFO(msgBase << "start called, current state: " << m_gemfsm.getCurrentState());

    while ((m_gemfsm.getCurrentState()) != m_gemfsm.getStateName(STATE_STARTING)) { // deal with possible race condition
        usleep(10);
    }
    CMSGEMOS_DEBUG(msgBase << "start called, current state: " << m_gemfsm.getCurrentState());

    try {
        if (!isStartAllowed())
            throw std::runtime_error("Starting a run was not allowed (did you re-configure the system?)");
        startAction();
    } catch (gem::utils::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::start caught gem::utils::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::task::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::start caught toolbox::task::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::net::exception::MalformedURN const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::start malformed URN: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (std::exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::start caught std::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (...) {
        std::stringstream msg;
        msg << "GEMFSMApplication::start caught unknown exception";
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    }

    std::stringstream msg;
    msg << "GEMFSMApplication::start Firing 'IsRunning' into the FSM";
    CMSGEMOS_INFO(msg.str());
    fireEvent("IsRunning");
    m_wl_semaphore.give();
    return false;
}

bool gem::utils::GEMFSMApplication::pause(toolbox::task::WorkLoop* wl)
{
    std::string msgBase = "[GEMFSMApplication::pause] ";
    m_wl_semaphore.take();
    CMSGEMOS_INFO(msgBase << "pause called, current state: " << m_gemfsm.getCurrentState());

    while ((m_gemfsm.getCurrentState()) != m_gemfsm.getStateName(STATE_PAUSING)) { // deal with possible race condition
        usleep(10);
    }
    CMSGEMOS_DEBUG(msgBase << "pause called, current state: " << m_gemfsm.getCurrentState());

    try {
        pauseAction();
    } catch (gem::utils::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::pause gem::utils::exception " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::task::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::pause caught toolbox::task::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::net::exception::MalformedURN const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::pause malformed URN: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (std::exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::pause caught std::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (...) {
        std::stringstream msg;
        msg << "GEMFSMApplication::pause caught unknown exception";
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    }

    std::stringstream msg;
    msg << "GEMFSMApplication::pause Firing 'IsPaused' into the FSM";
    CMSGEMOS_INFO(msg.str());
    fireEvent("IsPaused");
    m_wl_semaphore.give();
    return false;
}

bool gem::utils::GEMFSMApplication::resume(toolbox::task::WorkLoop* wl)
{
    std::string msgBase = "[GEMFSMApplication::resume] ";
    m_wl_semaphore.take();
    CMSGEMOS_INFO(msgBase << "resume called, current state: " << m_gemfsm.getCurrentState());

    while ((m_gemfsm.getCurrentState()) != m_gemfsm.getStateName(STATE_RESUMING)) { // deal with possible race condition
        usleep(10);
    }
    CMSGEMOS_DEBUG(msgBase << "resume called, current state: " << m_gemfsm.getCurrentState());

    try {
        resumeAction();
    } catch (gem::utils::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::resume gem::utils::exception " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::task::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::resume caught toolbox::task::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::net::exception::MalformedURN const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::resume malformed URN: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (std::exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::resume caught std::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (...) {
        std::stringstream msg;
        msg << "GEMFSMApplication::resume caught unknown exception";
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    }

    std::stringstream msg;
    msg << "GEMFSMApplication::resume Firing 'IsRunning' into the FSM";
    CMSGEMOS_INFO(msg.str());
    fireEvent("IsRunning");
    m_wl_semaphore.give();
    return false;
}

bool gem::utils::GEMFSMApplication::stop(toolbox::task::WorkLoop* wl)
{
    std::string msgBase = "[GEMFSMApplication::stop] ";
    m_wl_semaphore.take();
    CMSGEMOS_INFO(msgBase << "stop called, current state: " << m_gemfsm.getCurrentState());

    while ((m_gemfsm.getCurrentState()) != m_gemfsm.getStateName(STATE_STOPPING)) { // deal with possible race condition
        usleep(10);
    }
    CMSGEMOS_DEBUG(msgBase << "stop called, current state: " << m_gemfsm.getCurrentState());

    try {
        stopAction();
    } catch (gem::utils::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::stop gem::utils::exception " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::task::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::stop caught toolbox::task::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::net::exception::MalformedURN const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::stop malformed URN: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (std::exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::stop caught std::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (...) {
        std::stringstream msg;
        msg << "GEMFSMApplication::stop caught unknown exception";
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    }

    std::stringstream msg;
    msg << "GEMFSMApplication::stop Firing 'IsConfigured' into the FSM";
    CMSGEMOS_INFO(msg.str());
    fireEvent("IsConfigured");
    m_wl_semaphore.give();
    return false;
}

bool gem::utils::GEMFSMApplication::halt(toolbox::task::WorkLoop* wl)
{
    std::string msgBase = "[GEMFSMApplication::halt] ";
    m_wl_semaphore.take();
    CMSGEMOS_INFO(msgBase << "halt called, current state: " << m_gemfsm.getCurrentState());

    while ((m_gemfsm.getCurrentState()) != m_gemfsm.getStateName(STATE_HALTING)) { // deal with possible race condition
        usleep(10);
    }
    CMSGEMOS_DEBUG(msgBase << "halt called, current state: " << m_gemfsm.getCurrentState());

    try {
        haltAction();
    } catch (gem::utils::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::halt gem::utils::exception " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::task::exception::Exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::halt caught toolbox::task::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (toolbox::net::exception::MalformedURN const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::halt malformed URN: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (std::exception const& ex) {
        std::stringstream msg;
        msg << "GEMFSMApplication::halt caught std::exception: " << ex.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    } catch (...) {
        std::stringstream msg;
        msg << "GEMFSMApplication::halt caught unknown exception";
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        m_wl_semaphore.give();
        return false;
    }

    std::stringstream msg;
    msg << "GEMFSMApplication::halt Firing 'IsHalted' into the FSM";
    CMSGEMOS_INFO(msg.str());
    fireEvent("IsHalted");
    m_wl_semaphore.give();
    return false;
}
