/// @file

#include <gem/utils/GEMFSM.h>

#include <gem/utils/GEMFSMApplication.h>
#include <gem/utils/exception/Exception.h>
#include <gem/utils/soap/GEMSOAPToolBox.h>

#include <toolbox/fsm/AsynchronousFiniteStateMachine.h>
#include <toolbox/fsm/InvalidInputEvent.h>
#include <toolbox/string.h>
#include <xcept/tools.h>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMNodeList.hpp>
#include <xercesc/util/XercesDefs.hpp>

gem::utils::GEMFSM::GEMFSM(GEMFSMApplication* const gemAppP)
    : p_gemfsm(0)
    , m_gemFSMState("Undefined")
    , m_reasonForFailure("")
    , p_gemApp(gemAppP)
    , m_gemLogger(gemAppP->getApplicationLogger())
{
    CMSGEMOS_DEBUG("GEMFSM::ctor begin");

    // Create the underlying Finite State Machine itself.
    const std::string class_name = p_gemApp->getApplicationDescriptor()->getClassName();
    const uint32_t instance_number = p_gemApp->getApplicationDescriptor()->getInstance();

    std::stringstream tmp_workloop_name;
    tmp_workloop_name << class_name << ":" << instance_number << ":asyncFSM";

    p_gemfsm = new toolbox::fsm::AsynchronousFiniteStateMachine(tmp_workloop_name.str());

    // A map to look up the names of the 'intermediate' state transitions.
    // TCDS does things this way, is it the right way for GEMs?
    m_lookupMap["Initializing"] = "Halted"; // Halted
    m_lookupMap["Configuring"] = "Configured"; // Configured
    m_lookupMap["Halting"] = "Halted"; // Halted
    m_lookupMap["Starting"] = "Running"; // Running
    m_lookupMap["Pausing"] = "Paused"; // Paused
    m_lookupMap["Resuming"] = "Running"; // Running
    m_lookupMap["Stopping"] = "Configured"; // Configured

    // Define all states and transitions.
    /* intermediate states (states entered when a transition is requested*/
    p_gemfsm->addState(STATE_INITIALIZING, "Initializing", this, &gem::utils::GEMFSM::stateChanged);
    p_gemfsm->addState(STATE_HALTING, "Halting", this, &gem::utils::GEMFSM::stateChanged);
    p_gemfsm->addState(STATE_CONFIGURING, "Configuring", this, &gem::utils::GEMFSM::stateChanged);
    p_gemfsm->addState(STATE_STARTING, "Starting", this, &gem::utils::GEMFSM::stateChanged);
    p_gemfsm->addState(STATE_PAUSING, "Pausing", this, &gem::utils::GEMFSM::stateChanged);
    p_gemfsm->addState(STATE_RESUMING, "Resuming", this, &gem::utils::GEMFSM::stateChanged);
    p_gemfsm->addState(STATE_STOPPING, "Stopping", this, &gem::utils::GEMFSM::stateChanged);

    /*terminal states*/
    p_gemfsm->addState(STATE_INITIAL, "Initial", this, &gem::utils::GEMFSM::stateChanged);
    p_gemfsm->addState(STATE_HALTED, "Halted", this, &gem::utils::GEMFSM::stateChanged);
    p_gemfsm->addState(STATE_CONFIGURED, "Configured", this, &gem::utils::GEMFSM::stateChanged);
    p_gemfsm->addState(STATE_RUNNING, "Running", this, &gem::utils::GEMFSM::stateChanged);
    p_gemfsm->addState(STATE_PAUSED, "Paused", this, &gem::utils::GEMFSM::stateChanged);

    /*State transitions*/
    // Initialize: I -> h., connect hardware, perform basic checks, (load firware?)
    p_gemfsm->addStateTransition(STATE_INITIAL, STATE_INITIALIZING, "Initialize", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);

    // Configure: H/C/E/P -> c., configure hardware, set default parameters, probably don't allow from running
    p_gemfsm->addStateTransition(STATE_HALTED, STATE_CONFIGURING, "Configure", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_CONFIGURED, STATE_CONFIGURING, "Configure", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    /*p_gemfsm->addStateTransition(STATE_RUNNING,    STATE_CONFIGURING, "Configure", p_gemApp,
    &gem::utils::GEMFSMApplication::transitionDriver);*/
    // FIXME do we really allow Configure to be called from Paused?
    /*p_gemfsm->addStateTransition(STATE_PAUSED,     STATE_CONFIGURING, "Configure", p_gemApp,
    &gem::utils::GEMFSMApplication::transitionDriver);*/

    // Start: C -> e., enable links for data to flow from front ends to back ends
    p_gemfsm->addStateTransition(STATE_CONFIGURED, STATE_STARTING, "Start", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);

    // Pause: E -> p. pause data flow, links stay alive, TTC/TTS counters stay active
    p_gemfsm->addStateTransition(STATE_RUNNING, STATE_PAUSING, "Pause", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);

    // Resume: P -> e., resume data flow
    p_gemfsm->addStateTransition(STATE_PAUSED, STATE_RESUMING, "Resume", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);

    // Stop: C/E/P -> s., stop data flow, disable links
    // FIXME: STOP from CONFIGURED??? Should ignore or throw error
    // p_gemfsm->addStateTransition(STATE_CONFIGURED, STATE_STOPPING, "Stop", p_gemApp,
    //                              &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_RUNNING, STATE_STOPPING, "Stop", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_PAUSED, STATE_STOPPING, "Stop", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);

    // Halt: C/E/F/H/P/ -> h., halt hardware state to pre-configured state
    p_gemfsm->addStateTransition(STATE_CONFIGURED, STATE_HALTING, "Halt", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_RUNNING, STATE_HALTING, "Halt", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_FAILED, STATE_HALTING, "Halt", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_HALTED, STATE_HALTING, "Halt", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_PAUSED, STATE_HALTING, "Halt", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);

    // intermediate to terminal transitions:
    // i/h -> H
    p_gemfsm->addStateTransition(STATE_INITIALIZING, STATE_HALTED, "IsHalted", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_HALTING, STATE_HALTED, "IsHalted", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    // c/s -> C
    p_gemfsm->addStateTransition(STATE_CONFIGURING, STATE_CONFIGURED, "IsConfigured", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_STOPPING, STATE_CONFIGURED, "IsConfigured", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    // e/r -> E
    p_gemfsm->addStateTransition(STATE_STARTING, STATE_RUNNING, "IsRunning", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_RESUMING, STATE_RUNNING, "IsRunning", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    // p -> P
    p_gemfsm->addStateTransition(STATE_PAUSING, STATE_PAUSED, "IsPaused", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);

    // State transition failures: i/c/h/p/s/e/r/t -> F., go to failed
    p_gemfsm->addStateTransition(STATE_INITIALIZING, STATE_FAILED, "Fail", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_CONFIGURING, STATE_FAILED, "Fail", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_HALTING, STATE_FAILED, "Fail", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_PAUSING, STATE_FAILED, "Fail", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_STOPPING, STATE_FAILED, "Fail", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_STARTING, STATE_FAILED, "Fail", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->addStateTransition(STATE_RESUMING, STATE_FAILED, "Fail", p_gemApp,
        &gem::utils::GEMFSMApplication::transitionDriver);

    p_gemfsm->setStateName(STATE_FAILED, "Error");
    p_gemfsm->setFailedStateTransitionAction(p_gemApp, &gem::utils::GEMFSMApplication::transitionDriver);
    p_gemfsm->setFailedStateTransitionChanged(this, &gem::utils::GEMFSM::stateChanged);
    p_gemfsm->setInvalidInputStateTransitionAction(this, &gem::utils::GEMFSM::invalidAction);

    // Start out with the FSM in its initial state: Initial.
    p_gemfsm->setInitialState(STATE_INITIAL);
    p_gemfsm->reset();

    m_gemFSMState = p_gemfsm->getStateName(p_gemfsm->getCurrentState());
    CMSGEMOS_DEBUG("GEMFSM::GEMFSM current state is " << m_gemFSMState.toString());

    p_gemApp->getApplicationInfoSpace()->fireItemAvailable("FSMState", &m_gemFSMState);
    p_gemApp->getApplicationInfoSpace()->fireItemAvailable("ReasonForFailure", &m_reasonForFailure);

    p_gemApp->getApplicationInfoSpace()->fireItemValueRetrieve("FSMState");
    p_gemApp->getApplicationInfoSpace()->fireItemValueRetrieve("ReasonForFailure");
}

gem::utils::GEMFSM::~GEMFSM()
{
    if (p_gemfsm)
        delete p_gemfsm;
    p_gemfsm = 0;
}

void gem::utils::GEMFSM::fireEvent(toolbox::Event::Reference const& event)
{
    CMSGEMOS_INFO("GEMFSM::fireEvent(" << event->type() << ")");
    try {
        p_gemfsm->fireEvent(event);
    } catch (toolbox::fsm::exception::Exception const& e) {
        XCEPT_RETHROW(xoap::exception::Exception, "invalid command", e);
    }
};

xoap::MessageReference gem::utils::GEMFSM::changeState(xoap::MessageReference msg)
{
    CMSGEMOS_INFO("GEMFSM::changeState()");
    if (msg.isNull()) {
        XCEPT_RAISE(xoap::exception::Exception, "Null message received!");
    }

    std::string commandName = "undefined";
    try {
        commandName = gem::utils::soap::extractCommandName(msg);
        CMSGEMOS_INFO("GEMFSM::received command " << commandName);
    } catch (xoap::exception::Exception& e) {
        const std::string errmsg = "Unable to extract command from GEMFSM SOAP message";
        CMSGEMOS_ERROR(errmsg << ": " << xcept::stdformat_exception_history(e));
        XCEPT_DECLARE_NESTED(gem::utils::exception::SOAPTransitionProblem, top, errmsg, e);
        p_gemApp->notifyQualified("error", top);
        const std::string faultString = commandName + " failed";
        const std::string faultCode = "Client";
        const std::string detail = errmsg + ": " + e.message();
        const std::string faultActor = p_gemApp->getFullURL();
        xoap::MessageReference reply = gem::utils::soap::makeSOAPFaultReply(faultString, faultCode, detail, faultActor);
        return reply;
    }

    CMSGEMOS_DEBUG("GEMFSM::changeState() received command '" << commandName << "'.");

    try {
        toolbox::Event::Reference event(new toolbox::Event(commandName, this));
        CMSGEMOS_INFO("Firing GEMFSM for event " << commandName);
        CMSGEMOS_INFO("initial state is: " << p_gemfsm->getStateName(p_gemfsm->getCurrentState()));
        p_gemfsm->fireEvent(event);
        CMSGEMOS_INFO("new state is: " << p_gemfsm->getStateName(p_gemfsm->getCurrentState()));
    } catch (toolbox::fsm::exception::Exception& err) {
        const std::string errmsg = "Problem executing the GEMFSM '" + commandName + "' command";
        CMSGEMOS_ERROR(errmsg << ": " << xcept::stdformat_exception_history(err));
        XCEPT_DECLARE_NESTED(gem::utils::exception::SOAPTransitionProblem, top, errmsg, err);
        p_gemApp->notifyQualified("error", top);
        const std::string faultString = commandName + " failed";
        const std::string faultCode = "Server";
        const std::string detail = errmsg + ": " + err.message();
        const std::string faultActor = p_gemApp->getFullURL();

        gotoFailedAsynchronously(err);
        return gem::utils::soap::makeSOAPFaultReply(faultString, faultCode, detail, faultActor);
    }

    // the HCAL way
    std::string newStateName = p_gemfsm->getStateName(p_gemfsm->getCurrentState());
    // Once we get here, the state transition has been triggered.
    // Notify the requestor of the new state.
    try {
        CMSGEMOS_INFO("changeState::sending command " << commandName << " newStateName " << newStateName);
        return gem::utils::soap::makeFSMSOAPReply(commandName,
            p_gemfsm->getStateName(p_gemfsm->getCurrentState()));
    } catch (xcept::Exception& err) {
        const std::string errmsg = "Failed to create GEMFSM SOAP reply for command '" + commandName + "'";
        CMSGEMOS_ERROR(errmsg << ": " << xcept::stdformat_exception_history(err));
        XCEPT_DECLARE_NESTED(gem::utils::exception::SoftwareProblem, top, errmsg, err);
        p_gemApp->notifyQualified("error", top);

        gotoFailedAsynchronously(err);
        XCEPT_RETHROW(xoap::exception::Exception, errmsg, err);
    }

    XCEPT_RAISE(xoap::exception::Exception, "command not found");
}

std::string gem::utils::GEMFSM::getCurrentState() const
{
    CMSGEMOS_DEBUG("GEMFSM::getCurrentState()");
    return p_gemfsm->getStateName(p_gemfsm->getCurrentState());
}

toolbox::fsm::State gem::utils::GEMFSM::getCurrentFSMState() const
{
    CMSGEMOS_DEBUG("GEMFSM::getCurrentFSMState()");
    return p_gemfsm->getCurrentState();
}

std::string gem::utils::GEMFSM::getStateName(toolbox::fsm::State const& state) const
{
    CMSGEMOS_DEBUG("GEMFSM::getStateName()");
    return p_gemfsm->getStateName(state);
}

/* moved into GEMSupervisor, as only the supervisor global state should be reported to RCMS
void gem::utils::GEMFSM::notifyRCMS(toolbox::fsm::FiniteStateMachine &fsm, std::string const msg)
{
  CMSGEMOS_INFO("GEMFSM::notifyRCMS()");
  // Notify RCMS of a state change.
  // NOTE: Should only be used for state _changes_.

  // toolbox::fsm::State currentState = fsm.getCurrentState();
  // std::string stateName            = fsm.getStateName(currentState);
  std::string stateName = fsm.getStateName(fsm.getCurrentState());
  CMSGEMOS_DEBUG("notifyRCMS() called with msg = " << msg);
  try {
    m_gemRCMSNotifier.stateChanged(stateName, msg);
  } catch (xcept::Exception& err) {
    CMSGEMOS_ERROR("GEMFSM::Failed to notify RCMS of state change: "
          << xcept::stdformat_exception_history(err));
    XCEPT_DECLARE_NESTED(gem::utils::exception::RCMSNotificationError, top,
                         "Failed to notify RCMS of state change.", err);
    p_gemApp->notifyQualified("error", top);
  }
}
*/

void gem::utils::GEMFSM::stateChanged(toolbox::fsm::FiniteStateMachine& fsm)
{
    CMSGEMOS_DEBUG("GEMFSM::stateChanged() begin");
    try {
        m_gemFSMState = fsm.getStateName(fsm.getCurrentState());
        *dynamic_cast<xdata::String*>(p_gemApp->getApplicationInfoSpace()->find("StateName")) = m_gemFSMState.toString();
    } catch (toolbox::fsm::exception::Exception const& e) {
        std::stringstream msg;
        msg << "Problem updating state after stateChanged " << e.what();
        CMSGEMOS_FATAL(msg.str());
        XCEPT_DECLARE_NESTED(gem::utils::exception::SoftwareProblem, top, msg.str(), e);
    } catch (xcept::Exception const& e) {
        std::stringstream msg;
        msg << "Problem updating state after stateChanged " << e.what();
        CMSGEMOS_FATAL(msg.str());
        XCEPT_DECLARE_NESTED(gem::utils::exception::SoftwareProblem, top, msg.str(), e);
        p_gemApp->notifyQualified("fatal", top);
    } catch (std::exception const& e) {
        std::stringstream msg;
        msg << "Problem updating state after stateChanged " << e.what();
        CMSGEMOS_FATAL(msg.str());
        XCEPT_RAISE(gem::utils::exception::SoftwareProblem, msg.str());
    } catch (...) {
        std::stringstream msg;
        msg << "Problem updating state after stateChanged";
        CMSGEMOS_FATAL(msg.str());
        XCEPT_RAISE(gem::utils::exception::SoftwareProblem, msg.str());
    }
    CMSGEMOS_INFO("GEMFSM::stateChanged:Current state is: [" << m_gemFSMState.toString() << "]");
    CMSGEMOS_DEBUG("GEMFSM::stateChanged:stateChanged() end");
}

void gem::utils::GEMFSM::invalidAction(toolbox::Event::Reference event)
{
    /* what's the point of this action?
     * should we go to failed or try to ensure no action is taken and the initial state is preserved?
     */
    CMSGEMOS_INFO("GEMFSM::invalidAction(" << event->type() << ")");
    toolbox::fsm::InvalidInputEvent& invalidInputEvent = dynamic_cast<toolbox::fsm::InvalidInputEvent&>(*event);
    const std::string initialState = p_gemfsm->getStateName(invalidInputEvent.getFromState());
    const std::string requestedState = invalidInputEvent.getInput();

    const std::string message = "An invalid state transition has been received: requested transition to '" + requestedState + "' from '" + initialState + "'.";
    CMSGEMOS_ERROR("GEMFSM::" << message);
    gotoFailed(message);
}

void gem::utils::GEMFSM::gotoFailed(std::string const reason)
{
    CMSGEMOS_DEBUG("GEMFSM::gotoFailed(std::string)");
    CMSGEMOS_ERROR("GEMFSM::Going to 'Failed' state. Reason: '" << reason << "'.");
    XCEPT_RAISE(toolbox::fsm::exception::Exception, reason);
}

void gem::utils::GEMFSM::gotoFailed(xcept::Exception& err)
{
    CMSGEMOS_DEBUG("GEMFSM::gotoFailed(xcept::Exception&)");
    std::string reason = err.message();
    gotoFailed(reason);
}

void gem::utils::GEMFSM::gotoFailedAsynchronously(xcept::Exception& err)
{
    CMSGEMOS_DEBUG("GEMFSM::gotoFailedAsynchronously(xcept::Exception&)");
    std::string reason = err.message();
    CMSGEMOS_ERROR("GEMFSM::Going to 'Failed' state. Reason: " << reason);
    try {
        toolbox::Event::Reference event(new toolbox::Event("Fail", this));
        p_gemfsm->fireEvent(event);
    } catch (xcept::Exception const& e) {
        std::stringstream msg;
        msg << "Cannot initiate asynchronous 'Fail' transition.";
        CMSGEMOS_FATAL(msg.str());
        XCEPT_DECLARE_NESTED(gem::utils::exception::SoftwareProblem, top, msg.str(), e);
        p_gemApp->notifyQualified("fatal", top);
    }
}
