/// @file

#include <gem/utils/GEMGenericFSMApplication.h>

#include <gem/utils/exception/Exception.h>
#include <gem/utils/soap/GEMSOAPToolBox.h>

#include <toolbox/task/WorkLoopFactory.h>
#include <toolbox/task/exception/Exception.h>

gem::utils::GEMGenericFSMApplication::GEMGenericFSMApplication(xdaq::ApplicationStub* stub)
    : GEMApplication(stub)
    , m_wl_semaphore(toolbox::BSem::FULL)
{
    CMSGEMOS_DEBUG("GEMGenericFSMApplication::ctor: Starting");
    // Workloop name
    const std::string class_name = this->getApplicationDescriptor()->getClassName();
    const uint32_t instance_number = this->getApplicationDescriptor()->getInstance();

    std::stringstream tmp_workloop_name;
    tmp_workloop_name << class_name << ":" << instance_number << ":fsmTransition";
    workLoopName = tmp_workloop_name.str();
    CMSGEMOS_DEBUG("GEMGenericFSMApplication::ctr: Created workloop name " << workLoopName);

    p_gemfsm = std::make_unique<toolbox::fsm::FiniteStateMachine>();
    // All FSMs will have an initial state and an error state.
    addState(States::INITIAL);
    p_gemfsm->setStateName(static_cast<char>(States::FAILED),
        name_by_state.at(States::FAILED));
    contained_states.insert(States::FAILED);
    p_gemfsm->setInitialState(static_cast<char>(States::INITIAL));
    p_gemfsm->reset();

    m_wlSig = toolbox::task::bind(this, &GEMGenericFSMApplication::workloopFunction, "workloopFunction");
    xgi::bind(this, &GEMGenericFSMApplication::stateUpdate, "stateUpdate");
    xgi::bind(this, &GEMGenericFSMApplication::xgiChangeState, "xgiChangeState");
}

gem::utils::GEMGenericFSMApplication::~GEMGenericFSMApplication()
{
}

void gem::utils::GEMGenericFSMApplication::stateChanged(toolbox::fsm::FiniteStateMachine& fsm)
{
    std::string msgBase = "[GEMGenericFSMApplication::stateChanged] ";
    CMSGEMOS_DEBUG(msgBase << "In the state: " << currentStateName());
    std::string currentState = currentStateName();
    // set a transition message to ""
    try {
        if (transitionInfo_by_name.find(currentState) != transitionInfo_by_name.end()) {
            CMSGEMOS_DEBUG(msgBase << "submitting workloopDriver()");
            workloopDriver();
        } else {
            // report success
            CMSGEMOS_DEBUG(msgBase << "Recieved confirmation that state changed to " << currentStateName());
        }
    } catch (gem::utils::exception::Exception const& ex) {
        CMSGEMOS_ERROR(msgBase << "Caught gem::utils::exception::Exception");
        fireEvent("Fail");
        // set a transition message to ex.what()
        XCEPT_RETHROW(toolbox::fsm::exception::Exception, "State Transition Failed", ex);
    }
    CMSGEMOS_DEBUG(msgBase << "Finished stateChanged: " << currentState << " | " << currentStateName());
}

void gem::utils::GEMGenericFSMApplication::workloopDriver()
{
    std::string msgBase = "[GEMGenericFSMApplication::workloopDriver] ";
    try {
        toolbox::task::WorkLoopFactory* wlf = toolbox::task::WorkLoopFactory::getInstance();
        CMSGEMOS_DEBUG(msgBase << "Trying to access the workloop with name " << workLoopName);
        toolbox::task::WorkLoop* loop = wlf->getWorkLoop(workLoopName, "waiting");
        if (!loop->isActive())
            loop->activate();
        CMSGEMOS_DEBUG(msgBase << "Workloop should now be active");
        loop->submit(m_wlSig);
        CMSGEMOS_DEBUG(msgBase << "Workloop should now be submitted");
    } catch (gem::utils::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMGenericFSMApp::workloopDriver Workloop failure (gem::utils::exception)";
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RETHROW(gem::utils::exception::Exception, msg.str(), e);
    } catch (toolbox::task::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMGenericFSMApp::workloopDriver Workloop failure (toolbox::task::exception)";
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RETHROW(gem::utils::exception::Exception, msg.str(), e);
    }
    CMSGEMOS_DEBUG(msgBase << "workloopDriver end");
}

xoap::MessageReference gem::utils::GEMGenericFSMApplication::changeState(xoap::MessageReference msg)
{
    CMSGEMOS_DEBUG("GEMGenericFSMApplication::changeState: Start");
    if (msg.isNull()) {
        XCEPT_RAISE(xoap::exception::Exception, "Null message received!");
    }

    std::string commandName = "undefined";
    try {
        commandName = gem::utils::soap::extractCommandName(msg);
        CMSGEMOS_DEBUG("GEMGenericFSMApplication::changeState: command is " << commandName);
        fireEvent(commandName);
        return gem::utils::soap::makeFSMSOAPReply(commandName, currentStateName());
    } catch (xoap::exception::Exception& e) {
        const std::string errmsg = "Unable to extract command from GEMFSM SOAP message";
        CMSGEMOS_ERROR(errmsg << ": " << xcept::stdformat_exception_history(e));
        XCEPT_DECLARE_NESTED(gem::utils::exception::SOAPTransitionProblem, top, errmsg, e);
        this->notifyQualified("error", top);
        const std::string faultString = commandName + " failed";
        const std::string faultCode = "Client";
        const std::string detail = errmsg + ": " + e.message();
        const std::string faultActor = this->getFullURL();
        return gem::utils::soap::makeSOAPFaultReply(faultString, faultCode, detail, faultActor);
    } catch (toolbox::fsm::exception::Exception& err) {
        const std::string errmsg = "Problem executing the GEMFSM '" + commandName + "' command";
        CMSGEMOS_ERROR(errmsg << ": " << xcept::stdformat_exception_history(err));
        XCEPT_DECLARE_NESTED(gem::utils::exception::SOAPTransitionProblem, top, errmsg, err);
        this->notifyQualified("error", top);
        const std::string faultString = commandName + " failed";
        const std::string faultCode = "Server";
        const std::string detail = errmsg + ": " + err.message();
        const std::string faultActor = this->getFullURL();
        return gem::utils::soap::makeSOAPFaultReply(faultString, faultCode, detail, faultActor);
    } catch (xcept::Exception& err) {
        const std::string errmsg = "Failed to create GEMFSM SOAP reply for command '" + commandName + "'";
        CMSGEMOS_ERROR(errmsg << ": " << xcept::stdformat_exception_history(err));
        XCEPT_DECLARE_NESTED(gem::utils::exception::SoftwareProblem, top, errmsg, err);
        this->notifyQualified("error", top);

        XCEPT_RETHROW(xoap::exception::Exception, errmsg, err);
    }
}

void gem::utils::GEMGenericFSMApplication::xgiChangeState(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_DEBUG("GEMGenericFSMApplication::xgiChangeState: Start");
    try {
        cgicc::Cgicc cgi(in);
        std::string action = cgi["action"][0].getValue();
        CMSGEMOS_DEBUG("GEMGenericFSMApplication::xgiChangeState: Action is " << action);
        fireEvent(action);
    } catch (const xgi::exception::Exception& e) {
        CMSGEMOS_WARN("GEMGenericFSMApplication::Caught xgi::exception " << e.what());
        // XCEPT_RAISE(xgi::exception::Exception, e.what());
    } catch (const std::exception& e) {
        CMSGEMOS_WARN("GEMGenericFSMApplication::Caught std::exception " << e.what());
        // XCEPT_RAISE(xgi::exception::Exception, e.what());
    } catch (...) {
        CMSGEMOS_WARN("GEMGenericFSMApplication::Caught unknown exception");
        // XCEPT_RAISE(xgi::exception::Exception, e.what());
    }
}

void gem::utils::GEMGenericFSMApplication::stateUpdate(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_DEBUG("GEMGenericFSMApplication::stateUpdate: Current stats is " << currentStateName());
    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
    nlohmann::json json;
    json["fsm-state"] = currentStateName();
    if (buttonInfo_json.find(currentStateName()) != buttonInfo_json.end()) {
        json["transitions"] = buttonInfo_json[currentStateName()];
    }
    *out << json.dump(4) << std::endl;
}

void gem::utils::GEMGenericFSMApplication::fireEvent(std::string event)
{
    CMSGEMOS_DEBUG("GEMGenericFSMApplication::fireEvent: event is (" << event << ") with curent state is (" << currentStateName() << ")");

    try {
        toolbox::Event::Reference e(new toolbox::Event(event, this));
        p_gemfsm->fireEvent(e);
    } catch (toolbox::fsm::exception::Exception const& e) {
        CMSGEMOS_WARN("GEMGenericFSMApplication::Caught toolbox::fsm::exception" << e.what());
        // XCEPT_RETHROW(xoap::exception::Exception, "invalid command", e);
    }
    CMSGEMOS_DEBUG("GEMGenericFSMApplication::fireEvent: New state is (" << currentStateName() << ")");
}

void gem::utils::GEMGenericFSMApplication::addState(gem::utils::States state)
{
    CMSGEMOS_DEBUG("GEMGenericFSMApplication::addState: Start");
    if (contained_states.find(state) != contained_states.end())
        return;
    contained_states.insert(state);
    p_gemfsm->addState(static_cast<char>(state), name_by_state.at(state), this,
        &gem::utils::GEMGenericFSMApplication::stateChanged);
}

void gem::utils::GEMGenericFSMApplication::addTransitionFunction(gem::utils::States start_state, gem::utils::States middle_state, gem::utils::States end_state,
    std::function<void()> transitionFunc, std::string transitionName, std::string terminusName)
{
    CMSGEMOS_DEBUG("GEMGenericFSMApplication::addTransitionFunction: Start");
    addState(start_state);
    addState(middle_state);
    addState(end_state);
    p_gemfsm->addStateTransition(static_cast<char>(start_state),
        static_cast<char>(middle_state), transitionName);
    p_gemfsm->addStateTransition(static_cast<char>(middle_state),
        static_cast<char>(end_state), terminusName);
    p_gemfsm->addStateTransition(static_cast<char>(middle_state),
        static_cast<char>(States::FAILED), "Fail");
    transitionInfo_by_name[name_by_state.at(middle_state)] = { terminusName, transitionFunc };

    std::string start_name = name_by_state.at(start_state);
    buttonInfo_json[start_name].push_back(transitionName);

    xoap::bind(this, &GEMGenericFSMApplication::changeState, transitionName, XDAQ_NS_URI);
}

bool gem::utils::GEMGenericFSMApplication::workloopFunction(toolbox::task::WorkLoop* wl)
{
    m_wl_semaphore.take();
    auto info = transitionInfo_by_name[currentStateName()];
    try {
        info.command();
        fireEvent(info.transition_name);
    } catch (gem::utils::exception::Exception const& ex) {
        CMSGEMOS_ERROR("In workloopFunction--Caught gem::utils::exception: " << ex.what());
        fireEvent("Fail");
    } catch (toolbox::task::exception::Exception const& ex) {
        CMSGEMOS_ERROR("In workloopFunction--Caught toolbox::task::exception: " << ex.what());
        fireEvent("Fail");
    } catch (toolbox::net::exception::MalformedURN const& ex) {
        CMSGEMOS_ERROR("In workloopFunction--Caught Malformed URN: " << ex.what());
        fireEvent("Fail");
    } catch (std::exception const& ex) {
        CMSGEMOS_ERROR("In workloopFunction--Caught std::exception: " << ex.what());
        fireEvent("Fail");
    } catch (...) {
        CMSGEMOS_ERROR("In workloopFunction--Caught other exception");
        fireEvent("Fail");
    }
    m_wl_semaphore.give();
    return false;
}

void gem::utils::GEMGenericFSMApplication::createFSMControlPanel(xgi::Input* in, xgi::Output* out)
{
    *out << "<script src=\"/cmsgemos/gemutils/html/scripts/genericFSMactions.js\"></script>" << std::endl;

    *out << "<div align=\"center\">" << std::endl;
    *out << "<h1><span class=\"label label-info\" id=\"fsm-state\" state=\"\">MONITORING STATE: "
         << "</span></h1>" << std::endl;
    *out << "</div>" << std::endl;
    *out << "<div id=\"Buttons\" align=\"center\">" << std::endl;
    *out << "</div>" << std::endl;
}
