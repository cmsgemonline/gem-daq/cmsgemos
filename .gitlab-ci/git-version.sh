#!/bin/bash

# This script can be used to extract the version and release numbers
# from Git tags. Additionally, archive, PEP440, and RPM compatible
# versions are produced.
#
# The conventions used by this script are the following:
#
# Tag: <name>-<version>
# Name: <name>
# Archive: <version>[-dev<commits-since-parent>-git<hash>]
# PEP440/RPM: <version>[.dev<commits-since-parent>+git<hash>]
#
# The following variables are defined if sourced:
#
# * GITVER_NAME
# * GITVER_VERSION
# * GITVER_COMMITS
# * GITVER_HASH
# * GITVER_ARCHIVE
# * GITVER_PEP440
# * GITVER_RPM

# We use the function trick to get local variables in case the script is sourced
func () {
    # This is easy and not ambiguous to define
    GITVER_NAME=$1
    GITVER_HASH=$(git rev-parse --short=8 HEAD)

    # Find the closest tag in the current branch
    local TAG TAG_EXIST
    TAG=$(git describe --tags --first-parent --abbrev=0 --match=$1-\* 2>/dev/null)
    TAG_EXIST=$?

    # Use default values if no tag matches
    if [[ "${TAG_EXIST}" -ne 0 ]]; then
        GITVER_VERSION=0
        GITVER_COMMITS=$(git rev-list HEAD --count)
    else
        GITVER_VERSION=$(echo ${TAG} | awk '{n=split($0,a,"-"); print a[n]}')
        GITVER_COMMITS=$(git rev-list --count ${TAG}..HEAD)
    fi

    # Produce the versions
    GITVER_ARCHIVE=${GITVER_VERSION}
    GITVER_PEP440=${GITVER_VERSION}

    # If we are not on a tagged release we need to disambiguate the version
    if [[ ${GITVER_COMMITS} -ne 0 ]]; then
        GITVER_ARCHIVE=${GITVER_VERSION}-dev${GITVER_COMMITS}-git${GITVER_HASH}
        GITVER_PEP440=${GITVER_VERSION}.dev${GITVER_COMMITS}+git${GITVER_HASH}
    fi

    # We use the PEP440 version also for the RPM
    GITVER_RPM=${GITVER_PEP440}
}

if [[ "$#" -ne 1 ]]; then
    echo "Error: the package name must be provided"
    if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
        return 1
    else
        exit 1
    fi
fi

func $1

# Print the produced variables
echo "Name: ${GITVER_NAME}"
echo "Version: ${GITVER_VERSION}"
echo "Additional commits: ${GITVER_COMMITS}"
echo "Git hash: ${GITVER_HASH}"
echo "Archive version: ${GITVER_ARCHIVE}"
echo "PEP440 version: ${GITVER_PEP440}"
echo "RPM version: ${GITVER_RPM}"
