# Require Python 3
%global __python %__python3

Name:    gem-root
Version: 6.28.00
Release: 0%{?dist}
Summary: An open-source data analysis framework used by high energy physics and others
License: LGPLv2.1+
URL:     https://root.cern/
Source0: root-%{version}.tar.gz

# Updated toolchain
BuildRequires: cmake3
BuildRequires: devtoolset-8-gcc-c++
Requires:      devtoolset-8-gcc-c++

# Direct ROOT dependencies
BuildRequires: freetype-devel
BuildRequires: libuuid-devel
BuildRequires: libzstd-devel
BuildRequires: lz4-devel
BuildRequires: pcre-devel
BuildRequires: xxhash-devel
BuildRequires: xz-devel
BuildRequires: zlib-devel

# Indirect LLVM dependencies
BuildRequires: libxml2-devel
BuildRequires: ncurses-devel

%description
The ROOT system provides a set of OO frameworks with all the functionality
needed to handle and analyze large amounts of data in a very efficient way.
Having the data defined as a set of objects, specialized storage methods are
used to get direct access to the separate attributes of the selected objects,
without having to touch the bulk of the data. Included are histograming methods
in an arbitrary number of dimensions, curve fitting, function evaluation,
minimization, graphics and visualization classes to allow the easy setup of an
analysis system that can query and process the data interactively or in batch
mode, as well as a general parallel processing framework, PROOF, that can
considerably speed up an analysis.

Thanks to the built-in C++ interpreter cling, the command, the scripting and
the programming language are all C++. The interpreter allows for fast
prototyping of the macros since it removes the time consuming compile/link
cycle. It also provides a good environment to learn C++. If more performance is
needed the interactively developed macros can be compiled using a C++ compiler
via a machine independent transparent compiler interface called ACliC.

The system has been designed in such a way that it can query its databases in
parallel on clusters of workstations or many-core machines. ROOT is an open
system that can be dynamically extended by linking external libraries. This
makes ROOT a premier platform on which to build data acquisition, simulation
and data analysis systems.

%prep
%setup -q -n root-%{version}

%build
source /opt/rh/devtoolset-8/enable
%__cmake3 -S . -B build \
         -DCMAKE_INSTALL_PREFIX=/opt/gem-root \
         -Dgnuinstall=ON \
         -Drpath=ON \
         -Dminimal=ON \
         -Dbuiltin_nlohmannjson=ON \
         -Dfail-on-missing=ON \
         -DCMAKE_CXX_STANDARD=17 \
         -Dtesting=ON \
         -Dbuiltin_gtest=ON
%__cmake3 --build build %{?_smp_mflags} --verbose

%install
source /opt/rh/devtoolset-8/enable
DESTDIR=%{buildroot} %__cmake3 --build build --target install

# Remove some junk
rm %{buildroot}/opt/gem-root/bin/setxrd*
rm %{buildroot}/opt/gem-root/bin/thisroot*

%check
source /opt/rh/devtoolset-8/enable

# The gdml option is disabled, skip its tests
cd build && %__ctest3 %{?_smp_mflags} --output-on-failure -E '.*gdml.*'

%files
/opt/gem-root/

%changelog
* Tue Feb 07 2023 Laurent Pétré <laurent.petre@cern.ch> - 6.28.00-0
- Initial release
