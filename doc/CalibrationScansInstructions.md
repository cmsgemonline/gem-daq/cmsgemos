# Calibration Scan Instructions

## Sbit rate vs THR_ARM_DAC scan
To take a scan these steps need to be followed in order, **precisely**:
- Launch the xdaq application on the terminal
- Initialize the system
- Load the calibration suite in a new tab, selecting the S-bit rate vs THR_ARM_DAC scan
- Check the parameters
- Apply the parameters
- Select the icon of the `gemsupervisor` in the App tab
- Configure
- Go back to the Calibration Web interface tab for the S-bit rate vs THR_ARM_DAC scan
- Click on the "RUN" button

A .txt file should be produced at the location specified in the XML file with the parameter `outputPath`. At the moment nothing is displayed when the scan is completed so make your calculations assuming 4.5 minutes per scan iteration (0-255 dac units).

## DAC scans
To take DAC scan these steps need to be followed in order, **precisely**:
- Launch the xdaq application on the terminal
- Initialize the system
- Load the calibration suite in a new tab, selecting DAC scan on VFAT
- Click SELECT DEFAULT button
- Check the parameters in the scroll down window
- Apply the parameters
- Select the icon of the `gemsupervisor` in the App tab
- Configure
- Go back to the Calibration Web interface tab for the DAC scan
- Click on the "RUN" button

A .txt file should be produced at the location specified in the XML file with the parameter `outputPath`. At the moment nothing is displayed when the scan is completed, but a message appears in the terminal when a new register is scanned.


## Latency scan
To take a scan these steps need to be followed in order:
- Launch the xdaq application on the terminal
- If done at the time of joining global: ask the shift leader to initialize and then stop and wait to configure
- If in local: initialize the system
- Load the calibration suite, selecting the Latency scan
- Select the particle source or calibration
- Check and modify the parameters to the desired ones, making sure that the trigger rate and number of second in each point are commensurate
- Apply the parameters
- If in local: select the icon of the `gemsupervisor` in the App tab
- Configure or ask the shift leader to configure
- Start the run or let the shift leader start the run
