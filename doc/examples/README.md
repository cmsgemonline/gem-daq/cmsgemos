## Running local installation

**These instructions are written to easily start a run on the GEM 904 GE1/1 integration development setup and should only be used as a reference in other cases!**

After producing a local installation of `cmsgemos`, one can start a xDAQ executive and test the software.
The full sequence starting from compilation, assuming that the build system is configured, is below.
It will launch a xDAQ executive process with a default XML configuration, written for GEM 904 GE1/1 integration setup running on `gem904daq04`, and a user-specific generated port.

``` shell
cd </path/to/cmsgemos/>
cmake3 --build _build --target install
_build/_install/bin/gem-start-xdaq
```

It is possible to launch the xDAQ executive in `gdb` by setting the `DEBUG` environment variable before executing the `gem-start-xdaq` tool.
Similarly, it is possible to launch the xDAQ executive with alternative options whose usage should be obvious to the user in need:

* `CMSGEMOS_TREE_NAME` to run with an alternate layout tree (from `doc/config/layout-tree`)
* `CMSGEMOS_TREE_PATH` (overrides `CMSGEMOS_TREE_NAME`) to run with an out-of-tree layout tree
* `CMSGEMOS_CONFIG_NAME` to run with an alternate xDAQ configuration (from `doc/examples/xml`)
* `CMSGEMOS_CONFIG_PATH` (overrides `CMSGEMOS_CONFIG_NAME`) to run with an out-of-tree xDAQ configuration
* `CMSGEMOS_PORT` to define a fixed alternate port for the xDAQ executive

In order to start a run, one must make sure that the backend board is properly configured and recovered.
This requires to first build `gemhardware` and copy the binaries to the backend board, and second recover the board.
The full sequence starting from compilation, assuming that the build system is configured, is below.

**Since the backend board is touched, the following actions require to reserve the setup!**

``` shell
cd </path/to/cmsgemos/>
(cd gemhardware && DESTDIR=_install cmake3 --build _build --target install)
rsync -v -a --delete --include='/bin/***' --include='/lib/***' --exclude='*' gemhardware/_build/_install/mnt/persistent/gempro/ gempro@amc-e1a16-13-02:
# Needed only if memhub or gemrpc have been modified
ssh gempro@amc-e1a16-13-02 'bash -l -c "su -c \"killall -9 memhub-server; LD_LIBRARY_PATH=/mnt/persistent/lib nohup memhub-server /dev/mem >/dev/null 2>&1 & disown\""'
ssh gempro@amc-e1a16-13-02 'bash -l -c "killall -9 gemrpc; gemrpc"'
```

### FedKit runs

In some situations, it may be useful to start a run with the FedKit in order increase the AMC13 readout bandwidth or emulate better the CMS readout.
A FedKit has been installed and configured for data taking on the `gem904daq04` machine.

From that machine, a run can be started by instantiating 2 xDAQ executives, in two different terminal:

``` shell
CMSGEMOS_PORT=20200 CMSGEMOS_CONFIG_NAME=b904int-ge11-fedkit _build/_install/bin/gem-start-xdaq # GEM/cmsgemos applications
CMSGEMOS_PORT=20201 CMSGEMOS_CONFIG_NAME=b904int-ge11-fedkit _build/_install/bin/gem-start-xdaq # cDAQ/FedKit applications
```

### memhub

In order for the software to run, one must make sure that the `memhub` server is running as `root` on the backend board.
The following commands can be used to start the server from the installation directory:

``` shell
bin/memhub-server /dev/mem # CTP7/APEX/X2O
bin/memhub-server "/sys/bus/pci/devices/<cvp13-bdf>/resource2" # CVP13
```

The standard `nohup` and `disown` tools can be used to daemonize the process as shown above in the GE1/1 integration setup example.
