# Strongly inspired by the CentOS 8 and Fedora SPEC files of DPDK:
#
# - https://git.centos.org/rpms/dpdk/blob/0ac13dcfc44286f3ba535c95e4175704f9b2c385/f/SPECS/dpdk.spec
# - https://src.fedoraproject.org/rpms/dpdk/blob/56ad86b8d9e4cf92be9efdb5cf78f71f94073076/f/dpdk.spec

Name: gem-dpdk
Version: 20.11.5
Release: 2%{?dist}
Epoch: 2
URL: http://dpdk.org
Source: https://fast.dpdk.org/rel/dpdk-%{version}.tar.xz

Summary: Set of libraries and drivers for fast packet processing

#
# Note that, while this is dual licensed, all code that is included with this
# Package are BSD licensed. The only files that aren't licensed via BSD is the
# kni kernel module which is dual LGPLv2/BSD, and thats not built for fedora.
#
License: BSD and LGPLv2 and GPLv2

#
# The DPDK is designed to optimize througput of network traffic using, among
# other techniques, carefully crafted assembly instructions. As such it
# needs extensive work to port it to other architectures.
#
ExclusiveArch: x86_64 aarch64 ppc64le

BuildRequires: gcc, meson, python(abi) = 3.6
BuildRequires: kernel-headers, numactl-devel, rdma-core-devel, libpcap-devel, zlib-devel

Conflicts: dpdk dpdk-doc

%description
The Data Plane Development Kit is a set of libraries and drivers for
fast packet processing in the user space.

%package devel
Summary: Data Plane Development Kit development files
Requires: %{name} = %{?epoch:%{epoch}:}%{version}-%{release}
Requires: make, python(abi) = 3.6, rdma-core-devel
Conflicts: dpdk-devel

%description devel
This package contains the headers and other files needed for developing
applications with the Data Plane Development Kit.

%package tools
Summary: Tools for setting up Data Plane Development Kit environment
Requires: %{name} = %{?epoch:%{epoch}:}%{version}-%{release}
Requires: pciutils, python(abi) = 3.6
Conflicts: dpdk-tools

%description tools
%{summary}

#
# DPDK variables
#
%define sdkdir %{_datadir}/dpdk
%define docdir %{_docdir}/dpdk
%define incdir %{_includedir}/dpdk
%define pmddir %{_libdir}/dpdk-pmds

#
# To be used by meson
#
%global _vpath_srcdir .
%global _vpath_builddir %{_target_platform}
%global _smp_build_ncpus 0

%global set_build_flags \
    export CFLAGS="${CFLAGS:-%__global_cflags}" \
    export CXXFLAGS="${CXXFLAGS:-%__global_cxxflags}" \
    export FFLAGS="${FFLAGS:-%__global_fflags}" \
    export FCFLAGS="${FCFLAGS:-%__global_fcflags}" \
    export LDFLAGS="${LDFLAGS:-%__global_ldflags}" \

%prep
%setup -q -n dpdk-stable-%{version}

%build
%meson --includedir=include/dpdk \
       --default-library=shared \
       -Ddrivers_install_subdir=dpdk-pmds \
       -Dmachine=default \
       -Dplatform=generic

%meson_build

%install
%meson_install

%files
%doc README MAINTAINERS
%exclude %{docdir}
%exclude %{_bindir}/dpdk-*.py
%{_bindir}/dpdk-*
%{_libdir}/*.so.*
%{pmddir}/*.so.*

%files devel
%{incdir}/
%{sdkdir}/
%{_libdir}/*.a
%{_libdir}/*.so
%{pmddir}/*.so
%{_libdir}/pkgconfig/libdpdk.pc
%{_libdir}/pkgconfig/libdpdk-libs.pc

%files tools
%exclude %{_bindir}/dpdk-pmdinfo.py
%{_bindir}/dpdk-*.py

%changelog
