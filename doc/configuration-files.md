# Configuration files

## Fimware artifacts

The artifacts for a given firmware are stored in an unique folder with well-known names in order for the software to be able to find them.
The files must respect the following naming convention.

**Backend**

| Filename            | Description                           |
|---------------------|---------------------------------------|
| `bitstream.bit`     | Firmware bitfile                      |
| `address-table.xml` | XML address table of the firmware     |
| `clocks.cfg`        | Clock configuration file (CTP7-only)  |
| `<dash-case>`       | Additional ressources (if needed)     |

**OptoHybrid**

| Filename            | Description                                       |
|---------------------|---------------------------------------------------|
| `bitstream.bin`     | Firmware bitfile (if applicable)                  |
| `address-table.xml` | XML address table of the firmware (if applicable) |
| `gbt<n>.txt`        | Default configuration of the GBTx or lpGBT        |
| `<dash-case>`       | Additional ressources (if needed)                 |

The firmware versions published on the [official cmsgemos repository](https://cmsgemos.web.cern.ch/repos/firmware/) respect this convention.
It is recommended to store the firmware artifacts in `${INSTALL_PREFIX}/share/0xbefe`, but any other directory can be used, particularly in the case of multiple parallel installations.

By default, the software will pick up the artifacts from `${INSTALL_PREFIX}/etc/cmsgemos/backend-firmware` and `${INSTALL_PREFIX}/etc/cmsgemos/optohybrid-firmware`.
This behavior can be overwritten by setting the environment variables `GEM_BACKEND_FIRMWARE` (`GEM_OPTOHYBRID_FIRMWARE`) to configure for a different backend (frontend) firmware.

## Backend and frontend configuration

After a proper installation of the `cmsgemos` software and the firmware artifacts as well as the creation of a xDAQ configuration, data taking is possible.
The backend and frontend will be configured in the following order:

1. Default parameters originates from the hardware or firmware with surgical modification from the software.
   These default parameters should allow to start a run, without any regards on the data quality.

2. Every component can be individually configured through files stored on the backend board.

| Component      | Path                                                           | Registers                       |
|----------------|----------------------------------------------------------------|---------------------------------|
| Back-end board | `${INSTALL_PREFIX}/etc/cmsgemos/board/config.cfg`              | `BEFE` [^1]                     |
| AMC            | `${INSTALL_PREFIX}/etc/cmsgemos/amc/config.cfg`                | `BEFE.GEM`                      |
| GBT            | `${INSTALL_PREFIX}/etc/cmsgemos/gbt/config-oh<n>-gbt.cfg`      | Virtual registers               |
| OptoHybrid     | `${INSTALL_PREFIX}/etc/cmsgemos/oh/config-oh<n>.cfg`           | `BEFE.GEM.OH.OH<n>.FPGA`        |
| VFAT           | `${INSTALL_PREFIX}/etc/cmsgemos/vfat/config-oh<n>-vfat<m>.cfg` | `BEFE.GEM.OH.OH<n>.GEB.VFAT<m>` |

3. Any additional change required to successfully take data.
   This includes, the zero-suppression mode configuration in the VFAT or the modification of the run parameters during a scan.

[^1]: It is strongly recommended against modifying registers under the `BEFE.GEM` node; using the AMC configuration files should be used instead.
      The rationale behind this advice is that not all AMC are mapped at the same time, only the first AMC would be configured while other are unreachable.
