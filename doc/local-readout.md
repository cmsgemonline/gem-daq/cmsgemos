# Local readout

The local readout is a feature of the GEM DAQ allowing for a fast Ethernet readout straight from the backend board.

## Dependencies

Its dependencies have been carefully selected to fulfill the following requirements:

* Compatibility with CentOS 7.9
* Support of the Mellanox ConnectX-3 NIC and above

The drivers and low-level libraries come from Mellanox OFED v4.9 because:

* The CentOS 7.9 kernel is too old to use the upstream RDMA core libraries
* This is the last LTS version supporting the ConnectX-3 NIC

The readout application is based on the DPDK 20.11 framework chosen for its CentOS 7.9 compatibility.

## Installation

Since the selected DPDK version is not available in the CentOS 7 repositories a custom build is required.
The `gem-dpdk`, `gem-dpdk-devel`, and `gem-dpdk-tools` RPM packages are available in [the cmsgemos extras repository](https://cmsgemos.web.cern.ch/repos/extras/).
As reference, the SPEC file used for the RPM generation is stored next to this file.

The required Mellanox drivers for use with DPDK are pulled by the `mlnx-ofed-dpdk` package.
