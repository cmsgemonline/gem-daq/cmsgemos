# DIM interface

The DAQ online software is able to communicate with the DCS software to fulfill
multiple purposes:

* Send commands to the DCS
* Retrieve the OptoHybrid powering status
* Publish monitoring values

A simple, but functional, protocol has been developed based on the CERN DIM
system. Each feature is associated with a DIM service whose description is
below.

## Service naming convention

For each kind of service, one DIM service exists per OptoHybrid. The naming
convention for the DIM service is as follow:

```
<service-name> = <service-type-prefix>-<optohybrid-name>
```

with the `optohybrid-name` field being one of:

* `GE11-<M|P>-<00-36>L<1|2>`
* `GE21-<M|P>-<00-36>M<1-8>`

Note that no guarantee is given for a service to exist at all times. Also note
that the `optohybrid-name` is fully configurable in the DAQ software through
the usage of the layout tree.

## Services list

Currently, the following services are defined:

| Service                         | Prefix     | DAQ DIM type     | DCS `fwDim` type |
|---------------------------------|------------|------------------|------------------|
| Command channel                 | `COMMAND1` | `DimRpcInfo`     | Server RPC       |
| Low-voltage status              | `LV2`      | `DimStampedInfo` | Server Service   |
| Frontend temperature monitoring | `LV1`      | `DimService`     | Client Service   |
| Frontend low-voltage monitoring | `TEMP1`    | `DimService`     | Client Service   |
| High-voltage status             | `HV2`      | `DimStampedInfo` | Server Service   |

The details of the services can be found in the source code while waiting for a
more thorough documentation.
