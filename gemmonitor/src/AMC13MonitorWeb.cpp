/// @file

#include <gem/monitor/AMC13MonitorWeb.h>

#include <gem/monitor/AMC13Monitor.h>
#include <gem/utils/GEMGenericFSMApplication.h>

gem::monitor::AMC13MonitorWeb::AMC13MonitorWeb(gem::monitor::AMC13Monitor* app)
    : gem::utils::GEMWebApplication(app)
{
    add_tab("Expert Page", &AMC13MonitorWeb::expertPage);
}

gem::monitor::AMC13MonitorWeb::~AMC13MonitorWeb()
{
}

void gem::monitor::AMC13MonitorWeb::expertPage(xgi::Input* in, xgi::Output* out)
{
    static_cast<gem::utils::GEMGenericFSMApplication*>(p_gemApp)->createFSMControlPanel(in, out);
}
