/// @file

#include <gem/monitor/AMCMonitor.h>

#include <gem/hardware/amc.h>
#include <gem/hardware/monitor.h>
#include <gem/monitor/AMCMonitorWeb.h>
#include <xhal/client/call.h>

#include <chrono>
#include <memory>
#include <stdexcept>
#include <variant>

/* We need to import std::variant<T> variables into the monitoring JSON
 * objects. Make them importable in nlohmann::json via ADL serialization.
 */
namespace nlohmann { // update to NLOHMANN_JSON_NAMESPACE_BEGIN when available
template <typename... Args>
struct adl_serializer<std::variant<Args...>> {
    static void to_json(json& j, std::variant<Args...> const& v)
    {
        std::visit([&](auto&& value) {
            j = std::forward<decltype(value)>(value);
        },
            v);
    }
};
} // update to NLOHMANN_JSON_NAMESPACE_END when available

void gem::monitor::AMCMonitor::monitorable_group_config::registerFields(xdata::Bag<monitorable_group_config>* bag)
{
    bag->addField("name", &name);
    bag->addField("period", &period);
}

std::string gem::monitor::AMCMonitor::monitorable_group_config::toString() const
{
    std::stringstream os;
    os << "name: " << name.value_ << std::endl
       << "period: " << period.value_ << std::endl;
    return os.str();
}

void gem::monitor::AMCMonitor::dcs_export_config::registerFields(xdata::Bag<dcs_export_config>* bag)
{
    bag->addField("nSamples", &nSamples);
    bag->addField("voltageDeadBand", &voltageDeadBand);
    bag->addField("temperatureDeadBand", &temperatureDeadBand);
}

std::string gem::monitor::AMCMonitor::dcs_export_config::toString() const
{
    std::stringstream os;
    os << "nSamples: " << nSamples.value_ << std::endl
       << "voltageDeadBand: " << voltageDeadBand.value_ << std::endl
       << "temperatureDeadBand: " << temperatureDeadBand.value_ << std::endl;
    return os.str();
}
XDAQ_INSTANTIATOR_IMPL(gem::monitor::AMCMonitor);

gem::monitor::AMCMonitor::AMCMonitor(xdaq::ApplicationStub* stub)
    : gem::utils::GEMGenericFSMApplication(stub)
{
    using gem::utils::States;
    addTransitionFunction(States::INITIAL, States::CONFIGURING, States::CONFIGURED,
        std::bind(&AMCMonitor::configureAction, this), "Configure", "isConfigured");
    addTransitionFunction(States::CONFIGURED, States::STARTING, States::RUNNING,
        std::bind(&AMCMonitor::startAction, this), "Start", "isStarted");
    addTransitionFunction(States::RUNNING, States::STOPPING, States::CONFIGURED,
        std::bind(&AMCMonitor::stopAction, this), "Stop", "isStopped");
    // Failed recovery
    addTransitionFunction(States::FAILED, States::CONFIGURING, States::CONFIGURED,
        std::bind(&AMCMonitor::configureAction, this), "Configure", "isConfigured");

    // Configuration parameters
    p_appInfoSpace->fireItemAvailable("fedID", &m_fed_id);
    p_appInfoSpace->fireItemAvailable("slot", &m_slot);
    p_appInfoSpace->fireItemAvailable("monitorablesGroupConfigs", &m_monitorable_group_configs);
    p_appInfoSpace->fireItemAvailable("dcsExportConfig", &m_dcs_export_config);

    // Web UI
    p_gemWebInterface = new gem::monitor::AMCMonitorWeb(this);

    // Direct JSON interface
    xgi::deferredbind(this, this, &AMCMonitor::jsonGetLayout, "layout");

    // Default (empty) layout
    m_layout = nlohmann::json::object();
    m_layout["num-oh"] = MAX_OPTOHYBRIDS_PER_AMC;
    m_layout["num-gbt"] = 8;
    m_layout["num-vfat"] = 24;
    m_layout["oh"] = nlohmann::json::object();
}

gem::monitor::AMCMonitor::~AMCMonitor()
{
}

void gem::monitor::AMCMonitor::configureAction()
{
    namespace hmon = gem::hardware::monitor;

    /*
     * Connect to the backend board
     */
    const auto layout_tree_address = core::layout_tree::address<core::layout_tree::backend_board_node> { m_fed_id.value_, m_slot.value_ };
    const auto hostname = (layout_tree() % layout_tree_address).hostname();

    m_amc = std::make_unique<xhal::client::XHALInterface>(hostname, m_gemLogger);

    /*
     * Fetch the parameters needed for the Web UI layout
     */
    m_layout = nlohmann::json::object();

    const auto amcFacts = m_amc->call<gem::hardware::amc::getFacts>();
    m_layout["num-oh"] = amcFacts.number_oh;
    m_layout["num-gbt"] = amcFacts.number_gbt;
    m_layout["num-vfat"] = amcFacts.number_vfat;

    m_layout["oh"] = nlohmann::json::object();
    for (const auto& link : (layout_tree() % layout_tree_address).recursive_children() | core::layout_tree::filter_nodes<core::layout_tree::link_node>) {
        if (link.number() >= amcFacts.number_oh)
            continue; // Don't expose OH that cannot be used

        const auto& number = std::to_string(link.number());
        m_layout["oh"][number]["display"] = link.display_name();
        m_layout["oh"][number]["wire"] = link.wire_name();
        m_layout["oh"][number]["uid"] = link.uid();
    }

    /*
     * Setup timer
     */
    const std::string class_name = this->getApplicationDescriptor()->getClassName();
    const uint32_t instance_number = this->getApplicationDescriptor()->getInstance();
    std::stringstream t_monitoring_timer_name;
    t_monitoring_timer_name << class_name << ":" << instance_number << ":monitoring-timer";
    if (!m_monitoring_timer)
        m_monitoring_timer = toolbox::task::getTimerFactory()->createTimer(t_monitoring_timer_name.str());

    /*
     * Setup RPC-based monitoring
     */

    // Front-end masks
    add_monitorable_group("SCA.MASK", 1, [&]() {
        m_sca_mask = m_amc->call<hmon::getSCAMask>();
        return nlohmann::json::object({ { "SCA_MASK", m_sca_mask } });
    });
    add_monitorable_group("OH.MASK", 1, [&]() {
        m_optohybrid_mask = m_amc->call<hmon::getOHMask>();
        return nlohmann::json::object({ { "OH_MASK", m_optohybrid_mask } });
    });

    // Monitorable groups
    add_monitorable_group("DAQ.MAIN", 1, [&]() { return m_amc->call<hmon::getDAQMain>(); });
    add_monitorable_group("DAQ.OH", 1, [&]() { return m_amc->call<hmon::getDAQOH>(); });
    add_monitorable_group("FE.STATUS", 1, [&]() { return m_amc->call<hmon::getFEStatus>(m_sca_mask); });
    add_monitorable_group("GBT.LINK", 1, [&]() { return m_amc->call<hmon::getGBTLink>(); });
    add_monitorable_group("GBT.MAIN", 5, [&]() { return m_amc->call<hmon::getGBTMain>(); });
    add_monitorable_group("OH.MAIN", 1, [&]() { return m_amc->call<hmon::getOHMain>(m_optohybrid_mask); });
    add_monitorable_group("OH.SEU", 1, [&]() { return m_amc->call<hmon::getOHSEU>(m_optohybrid_mask); });
    add_monitorable_group("OH.SYSMON", 1, [&]() { return m_amc->call<hmon::getOHSysmon>(m_optohybrid_mask); });
    add_monitorable_group("SCA.CURRENT", 1, [&]() { return m_amc->call<hmon::getSCACurrent>(m_sca_mask); });
    add_monitorable_group("SCA.MAIN", 1, [&]() { return m_amc->call<hmon::getSCAMain>(); });
    add_monitorable_group("SCA.RSSI", 1, [&]() { return m_amc->call<hmon::getSCARSSI>(m_sca_mask); });
    add_monitorable_group("SCA.TEMP", 1, [&]() { return m_amc->call<hmon::getSCATemperature>(m_sca_mask); });
    add_monitorable_group("SCA.VOLTAGE", 1, [&]() { return m_amc->call<hmon::getSCAVoltage>(m_sca_mask); });
    add_monitorable_group("TRIGGER.MAIN", 1, [&]() { return m_amc->call<hmon::getTriggerMain>(); });
    add_monitorable_group("TRIGGER.OH_BE", 1, [&]() { return m_amc->call<hmon::getTriggerBE>(); });
    add_monitorable_group("TRIGGER.OH_FE", 1, [&]() { return m_amc->call<hmon::getTriggerFE>(m_optohybrid_mask); });
    add_monitorable_group("TTC.MAIN", 1, [&]() { return m_amc->call<hmon::getTTCMain>(); });
    add_monitorable_group("VFAT.LINK", 1, [&]() { return m_amc->call<hmon::getVFATLink>(); });

    /*
     * Setup the DCS interface
     */
    for (const auto& link : (layout_tree() % layout_tree_address).recursive_children() | core::layout_tree::filter_nodes<core::layout_tree::link_node>) {
        const auto& display_name = link.display_name();
        const auto& number = link.number();
        const auto& wire_name = link.wire_name();

        if (wire_name.empty()) {
            // Do not expose the links that do not have an wire_name
            CMSGEMOS_WARN("OptoHybrid '" << display_name << "' does not have a wire name - not creaing a DIM service...");
            continue;
        }

        m_oh_dcs_export_services.push_back(OptoHybridDCSExportAdapter { wire_name, number,
            m_dcs_export_config.bag.nSamples, m_dcs_export_config.bag.voltageDeadBand, m_dcs_export_config.bag.temperatureDeadBand });
        m_oh_dcs_import_services.push_back(OptoHybridDCSImportAdapter { wire_name, number });
    }

    // Fill DCS export history for each new sample
    add_callback("OH.SYSMON", [this](auto group, auto value) {
        for (auto& services : m_oh_dcs_export_services)
            services.pushNewSamples(group, value);
    });
    add_callback("SCA.TEMP", [this](auto group, auto value) {
        for (auto& services : m_oh_dcs_export_services)
            services.pushNewSamples(group, value);
    });
    add_callback("SCA.VOLTAGE", [this](auto group, auto value) {
        for (auto& services : m_oh_dcs_export_services)
            services.pushNewSamples(group, value);
    });

    // Periodically export monitorables
    add_monitorable_group("DCS.EXPORT", 15, [&]() {
        auto values = nlohmann::json::object();
        for (auto& services : m_oh_dcs_export_services)
            services.update(values);
        return values;
    });

    // Periodically import monitorables
    add_monitorable_group("DCS.IMPORT", 1, [&]() {
        auto values = nlohmann::json::object();
        for (const auto& services : m_oh_dcs_import_services)
            services.update(values);
        return values;
    });
}

void gem::monitor::AMCMonitor::startAction()
{
    start_monitoring();
}

void gem::monitor::AMCMonitor::stopAction()
{
    CMSGEMOS_DEBUG("AMCMonitor::Entering AMCMonitor::stopMonitoring()");

    stop_monitoring();

    // Invalidate the DCS service values
    for (auto& services : m_oh_dcs_export_services)
        services.reset();
}

void gem::monitor::AMCMonitor::add_monitorable_group(const std::string& name, double period, const std::function<nlohmann::json()>& function)
{
    // Rerieves the update period from configuration
    for (const auto& c : m_monitorable_group_configs) {
        if (c.bag.name == name)
            period = c.bag.period.value_;
    }

    m_monitorable_group_infos[name] = { function, period };
}

void gem::monitor::AMCMonitor::add_callback(const std::string& name, callback_t callback)
{
    const auto it = m_monitorable_group_infos.find(name);
    if (it == m_monitorable_group_infos.end())
        throw std::logic_error("trying to add a callback to non-existing group " + name);

    it->second.callbacks.push_back(callback);
}

void gem::monitor::AMCMonitor::start_monitoring()
{
    try {
        m_monitoring_timer->stop();
    } catch (toolbox::task::exception::NotActive const& ex) {
        CMSGEMOS_WARN("AMCMonitor::start_monitoring: could not stop timer " << ex.what());
    }
    m_monitoring_timer->start();

    toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
    for (const auto& [name, info] : m_monitorable_group_infos) {
        // Disable the monitoring if no period
        if (info.period == 0)
            continue;

        toolbox::TimeInterval period(info.period);
        m_monitoring_timer->scheduleAtFixedRate(start, this, period, 0, name);
    }
}

void gem::monitor::AMCMonitor::stop_monitoring()
{
    m_monitoring_timer->stop();
}

void gem::monitor::AMCMonitor::timeExpired(toolbox::task::TimerEvent& event)
{
    update_monitorable_group(event.getTimerTask()->name);
}

void gem::monitor::AMCMonitor::update_monitorable_group(const std::string& name)
{
    CMSGEMOS_DEBUG("AMCMonitor::update_monitorable_group: Updating group " << name);

    const auto& info = m_monitorable_group_infos.find(name);
    if (info == m_monitorable_group_infos.end()) {
        CMSGEMOS_WARN("AMCMonitor::update_monitorable_group: Unknown group " << name);
        return;
    }

    // Update group
    nlohmann::json response = nlohmann::json::object();
    try {
        const auto begin = std::chrono::system_clock::now();

        response = info->second.function();
        if (!response.is_object())
            throw std::runtime_error(name + "did not return a JSON object");

        std::scoped_lock values_lock(m_values_mutex);
        m_values_cache.clear();
        m_values[name]["values"] = response;
        m_values[name]["last-update"] = std::chrono::duration<double>(begin.time_since_epoch()).count();

        const auto end = std::chrono::system_clock::now();
        m_values[name]["last-update-duration"] = std::chrono::duration<double>(end - begin).count();
    } catch (const std::exception& e) {
        CMSGEMOS_WARN("AMCMonitor::update_monitorable_group: Failed to update group " << name << " with exception: " << e.what());
    } catch (...) {
        CMSGEMOS_WARN("AMCMonitor::update_monitorable_group: Failed to update group " << name);
    }

    // Always trigger all callbacks
    for (const auto& callback : info->second.callbacks) {
        try {
            callback(name, response);
        } catch (const std::exception& e) {
            CMSGEMOS_WARN("AMCMonitor::update_monitorable_group: Error while calling callback with exception: " << e.what());
        } catch (...) {
            CMSGEMOS_WARN("AMCMonitor::update_monitorable_group: Error while calling callback");
        }
    }
}

void gem::monitor::AMCMonitor::jsonGetLayout(xgi::Input* in, xgi::Output* out)
{
    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
    *out << m_layout.dump(4) << std::endl;
}
