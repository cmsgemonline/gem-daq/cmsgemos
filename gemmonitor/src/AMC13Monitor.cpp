/// @file
/// @defines Class used for connecting to an AMC13 and printing monitorable information about it

#include <gem/monitor/AMC13Monitor.h>

#include <gem/monitor/AMC13MonitorWeb.h>
#include <gem/monitor/exception/Exception.h>

#include <amc13/AMC13.hh>
#include <amc13/Status.hh>
#include <uhal/log/exception.hpp>

#include <stdexcept>
#include <string>

XDAQ_INSTANTIATOR_IMPL(gem::monitor::AMC13Monitor);

typedef gem::utils::States States;

gem::monitor::AMC13Monitor::AMC13Monitor(xdaq::ApplicationStub* stub)
    : gem::utils::GEMGenericFSMApplication(stub)
    , m_amc13Lock(toolbox::BSem::FULL, true)
{
    // Configuration parameters
    p_appInfoSpace->fireItemAvailable("fedID", &m_fed_id);

    // Web UI
    p_gemWebInterface = new gem::monitor::AMC13MonitorWeb(this);

    xgi::deferredbind(this, this, &AMC13Monitor::updateStatus, "updateStatus");

    // FSM
    addTransitionFunction(States::INITIAL, States::CONFIGURING, States::CONFIGURED,
        std::bind(&AMC13Monitor::configureAction, this), "Configure", "isConfigured");

    // Failed recovery
    addTransitionFunction(States::FAILED, States::CONFIGURING, States::CONFIGURED,
        std::bind(&AMC13Monitor::configureAction, this), "Resetting", "isReset");
}

gem::monitor::AMC13Monitor::~AMC13Monitor()
{
}

void gem::monitor::AMC13Monitor::configureAction()
{
    try {
        gem::utils::LockGuard<gem::utils::Lock> guardedLock(m_amc13Lock);
        const auto layout_tree_address = core::layout_tree::address<core::layout_tree::amc13_node> { m_fed_id.value_ };
        const auto t1_uri = (layout_tree() % layout_tree_address).t1_uri();
        const auto t2_uri = (layout_tree() % layout_tree_address).t2_uri();
        p_amc13 = std::make_shared<::amc13::AMC13>(t1_uri, "${AMC13_ADDRESS_TABLE_PATH}/AMC13XG_T1.xml", t2_uri, "${AMC13_ADDRESS_TABLE_PATH}/AMC13XG_T2.xml");
    } catch (::amc13::Exception::exBase const& e) {
        std::stringstream msg;
        msg << "AMC13Monitor::AMC13::AMC13() failed, caught amc13::Exception: " << e.what();
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::monitor::exception::HardwareProblem, msg.str());
    } catch (uhal::exception::exception const& e) {
        std::stringstream msg;
        msg << "AMC13Monitor::AMC13::AMC13() failed, caught uhal::exception: " << e.what();
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::monitor::exception::HardwareProblem, msg.str());
    } catch (std::exception const& e) {
        std::stringstream msg;
        msg << "AMC13Monitor::AMC13::AMC13() failed, caught std::exception: " << e.what();
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::monitor::exception::HardwareProblem, msg.str());
    } catch (...) {
        std::stringstream msg;
        msg << "AMC13Monitor::AMC13::AMC13() failed (unknown exception)";
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::monitor::exception::HardwareProblem, msg.str());
    }
    CMSGEMOS_DEBUG("AMC13Monitor::configureAction finished with AMC13::AMC13()");
}

void gem::monitor::AMC13Monitor::updateStatus(xgi::Input* in, xgi::Output* out)
{
    using namespace std::string_literals;

    cgicc::Cgicc cgi(in);
    out->getHTTPResponseHeader().addHeader("Content-Type", "plain/text");

    if (!p_amc13) {
        CMSGEMOS_DEBUG("AMC13Monitor::updateStatus: no connected to board");
        return;
    }

    // Get verbosity from input form
    int level = 2;
    try {
        level = std::stoi(cgi("amc13statuslevel"));
        CMSGEMOS_DEBUG("AMC13Monitor::updateStatus: setting AMC13 status verbosity to " << level);
    } catch (const std::exception& e) {
        CMSGEMOS_WARN("AMC13Monitor::updateStatus: caught std::exception: " << e.what());
    } catch (...) {
        CMSGEMOS_WARN("AMC13Monitor::updateStatus: caught unknown exception");
    }

    try {
        ::amc13::Status* s(p_amc13->getStatus());
        s->SetHTML();
        s->ReportBody(level, *out);
    } catch (const std::exception& e) {
        const std::string errMsg = "unable to display the AMC13 status page: "s + e.what();
        CMSGEMOS_WARN("AMC13Monitor::updateStatus: " << errMsg);
        *out << errMsg;
    } catch (...) {
        const std::string errMsg = "unable to display the AMC13 status page";
        CMSGEMOS_WARN("AMC13Monitor::updateStatus: " << errMsg);
        *out << errMsg;
    }
}
