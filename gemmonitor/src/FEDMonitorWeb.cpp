/// @file

#include <gem/monitor/FEDMonitorWeb.h>

#include <gem/monitor/FEDMonitor.h>
#include <gem/monitor/exception/Exception.h>

#include <xcept/tools.h>
#include <xdata/InfoSpaceFactory.h>

#include <iomanip>

gem::monitor::FEDMonitorWeb::FEDMonitorWeb(gem::monitor::FEDMonitor* application)
    : gem::utils::GEMWebApplication(application)
{
    add_tab("Monitoring Page", &FEDMonitorWeb::monitorPage);
    add_tab("Expert Page", &FEDMonitorWeb::expertPage);
}

gem::monitor::FEDMonitorWeb::~FEDMonitorWeb()
{
}

void gem::monitor::FEDMonitorWeb::monitorPage(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_DEBUG("FEDMonitorWeb::monitorPage");
    auto fed = dynamic_cast<gem::monitor::FEDMonitor*>(p_gemApp);
    auto amc_apps = fed->v_amc_apps;

    *out << "<script src=\"/cmsgemos/gemmonitor/html/scripts/fedMonitor.js\"></script>" << std::endl;

    *out << "<ul class=\"nav nav-tabs mb-3\" style=\"font-size:16px\" role=\"tablist\">";
    for (auto app = amc_apps.begin(); app != amc_apps.end(); ++app) {
        uint32_t slot = fed->getSlot(*app);
        std::string urnId = "amc-monitor-tab-" + std::to_string((*app)->getLocalId());
        *out << "<li class=\"nav-item\">"
             << "<a class=\"nav-link" << (app == amc_apps.begin() ? " active" : "")
             << "\" data-toggle=\"tab\" href=\"#" << urnId << "\">"
             << "AMC" << std::setfill('0') << std::setw(2) << slot
             << "</a></li>" << std::endl;
    }
    *out << "</ul>" << std::endl;

    *out << "<div class=\"tab-content\">" << std::endl;
    for (auto app = amc_apps.begin(); app != amc_apps.end(); ++app) {
        std::string urnId = "amc-monitor-tab-" + std::to_string((*app)->getLocalId());
        *out << "<div id=\"" << urnId << "\" class=\"tab-pane"
             << (app == amc_apps.begin() ? " show active" : "") << "\">" << std::endl;
        *out << "<script type=\"text/javascript\">" << std::endl
             << "    setupGenericAMCTab( \"" << (*app)->getURN() << "\"," << fed->getSlot(*app) << " );" << std::endl
             << "</script>" << std::endl;
        *out << "</div>" << std::endl;
    }
    *out << "</div>" << std::endl;
}

void gem::monitor::FEDMonitorWeb::expertPage(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_DEBUG("FEDMonitoringWeb::expertPage");

    std::string urn = p_gemApp->p_appDescriptor->getURN();
    *out << "<div align=\"center\">" << std::endl;
    *out << "<button type=\"button\" class=\"btn btn-info btn-lg\" onClick=\"$.getJSON(\'/" << urn << "/configureAction\')\">Configure AMCs</button>"
         << "<button type=\"button\" class=\"btn btn-success btn-lg\" onClick=\"$.getJSON(\'/" << urn << "/startAction\')\">Start AMCs Monitoring</button>"
         << "<button type=\"button\" class=\"btn btn-danger btn-lg\" onClick=\"$.getJSON(\'/" << urn << "/stopAction\')\">Stop AMCs Monitoring</button>" << std::endl;
    *out << "</div>" << std::endl;

    *out << "<div>" << std::endl;
    *out << "<table class=\"table table-lg\">"
         << "  <thead><tr>"
         << "    <th scope=\"col\">AMC</th>   <th scope=\"col\">State</th>"
         << "  </tr></thead>"
         << "  <tbody>" << std::endl;

    auto fed = dynamic_cast<gem::monitor::FEDMonitor*>(p_gemApp);
    auto amc_apps = fed->v_amc_apps;
    for (auto& app : amc_apps) {
        xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get(app->getURN());
        auto slot = fed->getSlot(app);
        std::string status_id = "amc-status-" + std::to_string(slot);
        *out << "<tr><th scope=\"row\">" << slot << "</th>";
        *out << "<td id=\"" << status_id << "\">"
             << "<script>setInterval('get_amc_status(\"/" << app->getURN() << "\", \"" << status_id << "\")', 1000)</script></td>"
             << "</tr>" << std::endl;
    }
    *out << "  </tbody>"
         << "</table>" << std::endl;
    *out << "</div>" << std::endl;
}
