/// @file

#include <gem/monitor/AMCMonitorWeb.h>

#include <gem/monitor/AMCMonitor.h>
#include <gem/monitor/exception/Exception.h>
#include <gem/utils/GEMGenericFSMApplication.h>

#include <nlohmann/json.hpp>
#include <xcept/tools.h>

#include <boost/algorithm/string.hpp>

#include <iomanip>

gem::monitor::AMCMonitorWeb::AMCMonitorWeb(gem::monitor::AMCMonitor* application)
    : gem::utils::GEMWebApplication(application)
{
    add_tab("Expert Page", &AMCMonitorWeb::expertPage);
}

gem::monitor::AMCMonitorWeb::~AMCMonitorWeb()
{
}

void gem::monitor::AMCMonitorWeb::expertPage(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_DEBUG("AMCMonitoringWeb::expertPage");
    static_cast<gem::utils::GEMGenericFSMApplication*>(p_gemApp)->createFSMControlPanel(in, out);
}

void gem::monitor::AMCMonitorWeb::jsonUpdate(xgi::Input* in, xgi::Output* out)
{
    const auto* amcApp = dynamic_cast<AMCMonitor*>(p_gemApp);

    // Update stringified JSON object cache if needed
    std::scoped_lock values_lock(amcApp->m_values_mutex);
    if (amcApp->m_values_cache.empty())
        amcApp->m_values_cache = amcApp->m_values.dump();

    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
    *out << amcApp->m_values_cache << std::endl;
}
