/// @file

#include <gem/monitor/FEDMonitor.h>

#include <gem/monitor/FEDMonitorWeb.h>
#include <gem/utils/exception/Exception.h>

#include <xdata/InfoSpaceFactory.h>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include <algorithm>
#include <cstdlib>
#include <iomanip>
#include <map>
#include <set>
#include <vector>

XDAQ_INSTANTIATOR_IMPL(gem::monitor::FEDMonitor);

gem::monitor::FEDMonitor::FEDMonitor(xdaq::ApplicationStub* stub)
    : gem::utils::GEMApplication(stub)
{
    // Configuration parameters
    p_appInfoSpace->fireItemAvailable("fedID", &m_fed_id);

    // Web UI
    p_gemWebInterface = new gem::monitor::FEDMonitorWeb(this);

    xgi::bind(this, &FEDMonitor::configureAction, "configureAction");
    xgi::bind(this, &FEDMonitor::startAction, "startAction");
    xgi::bind(this, &FEDMonitor::stopAction, "stopAction");
}

gem::monitor::FEDMonitor::~FEDMonitor()
{
    CMSGEMOS_DEBUG("gem::monitor::FEDMonitor : Destructor called");
}

void gem::monitor::FEDMonitor::actionPerformed(xdata::Event& event)
{
    gem::utils::GEMApplication::actionPerformed(event);

    if (event.type() == "urn:xdaq-event:setDefaultValues") {
        init();
    }
}

uint32_t gem::monitor::FEDMonitor::getSlot(const xdaq::ApplicationDescriptor* app)
{
    if (app->getClassName().find("gem::monitor::AMC13Monitor") != std::string::npos) {
        return 13;
    }
    xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get(app->getURN());
    return dynamic_cast<xdata::UnsignedInteger32*>(is->find("slot"))->value_;
}

void gem::monitor::FEDMonitor::init()
{
    CMSGEMOS_DEBUG("FEDMonitor::init:: looping over " << p_appZone->getGroupNames().size() << " groups");
    std::set<const xdaq::ApplicationDescriptor*> used;
    std::set<std::string> groups = p_appZone->getGroupNames();
    for (auto& group : groups) {
        CMSGEMOS_DEBUG("FEDMonitor::init::xDAQ group: " << group
                                                        << "getApplicationGroup() " << p_appZone->getApplicationGroup(group)->getName());

        const xdaq::ApplicationGroup* ag = p_appZone->getApplicationGroup(group);
        std::set<const xdaq::ApplicationDescriptor*> allApps = ag->getApplicationDescriptors();
        CMSGEMOS_DEBUG("FEDMonitor::init::getApplicationDescriptors() " << allApps.size());
        for (auto& app : allApps) {
            if (used.find(app) != used.end())
                continue; // no duplicates
            if (app == p_appDescriptor)
                continue; // don't fire the command into the FEDMonitor again
            if (isAMCInFED(app)) {
                v_amc_apps.push_back(app);
            }
        } // done iterating over applications in group
        CMSGEMOS_DEBUG("FEDMonitor::init::done iterating over applications in group");
    } // done iterating over groups in zone
    CMSGEMOS_DEBUG("FEDMonitor::init::done iterating over groups in zone");

    CMSGEMOS_DEBUG("FEDMonitor::init sorting amc tabs");
    sort(v_amc_apps.begin(), v_amc_apps.end(), [&](const xdaq::ApplicationDescriptor* app1, const xdaq::ApplicationDescriptor* app2) -> bool { return getSlot(app1) < getSlot(app2); });

    CMSGEMOS_DEBUG("FEDMonitor::init::starting the monitoring");
}

bool gem::monitor::FEDMonitor::isAMCInFED(const xdaq::ApplicationDescriptor* app) const
{
    std::string classname = app->getClassName();
    if (classname.find("gem::monitor::AMCMonitor") != std::string::npos) {
        xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get(app->getURN());
        xdata::Serializable* s = is->find("fedID");
        xdata::UnsignedInteger32* i = dynamic_cast<xdata::UnsignedInteger32*>(s);
        return i->value_ == m_fed_id.value_;
    } else if (classname.find("gem::monitor::AMC13Monitor") != std::string::npos) {
        xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get(app->getURN());
        auto fed_id = dynamic_cast<xdata::UnsignedInteger32*>(is->find("fedID"))->value_;
        return fed_id == m_fed_id;
    }
    return false;
}

void gem::monitor::FEDMonitor::configureAction(xgi::Input* in, xgi::Output* out)
{
    for (auto app : v_amc_apps) {
        gem::utils::soap::sendCommand("Configure", p_appContext, p_appDescriptor, app);
    }
}

void gem::monitor::FEDMonitor::startAction(xgi::Input* in, xgi::Output* out)
{
    for (auto app : v_amc_apps) {
        gem::utils::soap::sendCommand("Start", p_appContext, p_appDescriptor, app);
    }
}

void gem::monitor::FEDMonitor::stopAction(xgi::Input* in, xgi::Output* out)
{
    for (auto app : v_amc_apps) {
        gem::utils::soap::sendCommand("Stop", p_appContext, p_appDescriptor, app);
    }
}
