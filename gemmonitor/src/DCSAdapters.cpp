/// @file

#include <gem/monitor/AMCMonitor.h>

#include <gem/hardware/amc.h>
#include <gem/hardware/monitor.h>
#include <gem/monitor/AMCMonitorWeb.h>
#include <xhal/client/call.h>

#include <algorithm>
#include <chrono>
#include <cmath>
#include <memory>
#include <numeric>
#include <stdexcept>
#include <variant>

gem::monitor::OptoHybridDCSExportAdapter::OptoHybridDCSExportAdapter(const std::string& wire_name,
    const size_t oh_index, const unsigned n_samples,
    const double voltage_deadband, const double temperature_deadband)
    : m_oh_index(oh_index)
    , m_voltage_deadband(voltage_deadband)
    , m_temperature_deadband(temperature_deadband)
{
    // Configure number of samples to be recorded in the history
    for (auto& h : m_lv1_hist)
        h.set_capacity(n_samples);
    for (auto& h : m_temp1_hist)
        h.set_capacity(n_samples);

    // Create DIM services
    m_dcs_lv1_service = std::make_unique<core::dim::dcs_service_export<core::dim::lv1_dim_broker>>(wire_name);
    m_dcs_temp1_service = std::make_unique<core::dim::dcs_service_export<core::dim::temp1_dim_broker>>(wire_name);

    reset();
}

gem::monitor::OptoHybridDCSExportAdapter::~OptoHybridDCSExportAdapter()
{
}

void gem::monitor::OptoHybridDCSExportAdapter::reset()
{
    // Clear history
    for (auto& h : m_lv1_hist)
        h.clear();
    for (auto& h : m_temp1_hist)
        h.clear();

    // Publish invalid values
    m_dcs_lv1_service->update(core::dim::lv1_dim_broker {});
    m_dcs_temp1_service->update(core::dim::temp1_dim_broker {});
}

void gem::monitor::OptoHybridDCSExportAdapter::pushNewSamples(const std::string& group, const nlohmann::json& values)
{
    // Helper function to lookup values within the monitoring JSON objects
    const auto lookup = [&values](const std::string& key) -> std::optional<double> {
        const auto& it = values.find(key);
        if (it == values.end())
            return std::nullopt; // Not found

        const auto numericValue = it->get<double>();
        if (std::isnan(numericValue))
            return std::nullopt; // Not a valid reading

        return numericValue; // All good!
    };

    // Extract the values from the monitoring objects
    using gem::core::dim::lv1_dim_indexes;
    using gem::core::dim::temp1_dim_indexes;

    if (group == "SCA.VOLTAGE") {
        m_lv1_hist[lv1_dim_indexes::voltage_1p0_fpga_core].push_back(lookup("OH" + std::to_string(m_oh_index) + ".V1P0_FPGA_CORE"));
        m_lv1_hist[lv1_dim_indexes::voltage_1p0_fpga_mgt].push_back(lookup("OH" + std::to_string(m_oh_index) + ".V1P0_FPGA_MGT"));
        m_lv1_hist[lv1_dim_indexes::voltage_1p2_fpga_mgt].push_back(lookup("OH" + std::to_string(m_oh_index) + ".V1P2_FPGA_MGT"));
        m_lv1_hist[lv1_dim_indexes::voltage_1p5].push_back(lookup("OH" + std::to_string(m_oh_index) + ".V1P5"));
        m_lv1_hist[lv1_dim_indexes::voltage_2p5].push_back(lookup("OH" + std::to_string(m_oh_index) + ".V2P5"));
    } else if (group == "OH.SYSMON") {
        m_temp1_hist[temp1_dim_indexes::fpga_core].push_back(lookup("OH" + std::to_string(m_oh_index) + ".FPGA_CORE_TEMP"));
    } else if (group == "SCA.TEMP") {
        m_temp1_hist[temp1_dim_indexes::fpga_pt].push_back(lookup("OH" + std::to_string(m_oh_index) + ".FPGA_PT"));
        m_temp1_hist[temp1_dim_indexes::gbt0_pt].push_back(lookup("OH" + std::to_string(m_oh_index) + ".GBT0_PT"));
        m_temp1_hist[temp1_dim_indexes::sca_core].push_back(lookup("OH" + std::to_string(m_oh_index) + ".SCA_TEMP"));
        m_temp1_hist[temp1_dim_indexes::vttx_csc_pt].push_back(lookup("OH" + std::to_string(m_oh_index) + ".VTTX_CSC_PT"));
        m_temp1_hist[temp1_dim_indexes::vttx_gem_pt].push_back(lookup("OH" + std::to_string(m_oh_index) + ".VTTX_GEM_PT"));
    }
}

void gem::monitor::OptoHybridDCSExportAdapter::update(nlohmann::json& values) const
{
    // Helper function to compute a "smart" mean, i.e.
    // 1. Ignores the non-present/invalid readings
    // 2. Applies a configurable dead band
    const auto smart_mean = [](const boost::circular_buffer<std::optional<double>>& samples, float value_current, double deadband) -> float {
        unsigned n_valid_samples = std::count_if(samples.begin(), samples.end(), [](const auto& i) { return i.has_value(); });
        if (n_valid_samples == 0)
            return core::dim::invalid_dim_v<float>; // No valid data

        // Compute average
        const double sum = std::accumulate(samples.begin(), samples.end(), .0, [](double a, std::optional<double> b) { return a + (b ? *b : .0); });
        const double mean = sum / n_valid_samples;

        // Update value only if outside of the dead band
        const double new_value = ((mean < (value_current - deadband)) || (mean > (value_current + deadband))) ? mean : value_current;
        return new_value;
    };

    const auto lv1_broker_current = m_dcs_lv1_service->get();
    const auto temp1_broker_current = m_dcs_temp1_service->get();

    // Compute the new values
    core::dim::lv1_dim_broker lv1_broker {};
    core::dim::temp1_dim_broker temp1_broker {};

    for (size_t idx = 0; idx < m_lv1_hist.size(); ++idx)
        lv1_broker.values[idx] = smart_mean(m_lv1_hist[idx], lv1_broker_current.values[idx], m_voltage_deadband);
    for (size_t idx = 0; idx < m_temp1_hist.size(); ++idx)
        temp1_broker.values[idx] = smart_mean(m_temp1_hist[idx], temp1_broker_current.values[idx], m_voltage_deadband);

    // Record the preprocessed values back into the DAQ monitoring JSON object
    // for crosscheck
    using gem::core::dim::lv1_dim_indexes;
    using gem::core::dim::temp1_dim_indexes;

    values["OH" + std::to_string(m_oh_index) + ".DCS.V1P0_FPGA_CORE"] = lv1_broker.values[lv1_dim_indexes::voltage_1p0_fpga_core];
    values["OH" + std::to_string(m_oh_index) + ".DCS.V1P0_FPGA_MGT"] = lv1_broker.values[lv1_dim_indexes::voltage_1p0_fpga_mgt];
    values["OH" + std::to_string(m_oh_index) + ".DCS.V1P2_FPGA_MGT"] = lv1_broker.values[lv1_dim_indexes::voltage_1p2_fpga_mgt];
    values["OH" + std::to_string(m_oh_index) + ".DCS.V1P5"] = lv1_broker.values[lv1_dim_indexes::voltage_1p5];
    values["OH" + std::to_string(m_oh_index) + ".DCS.V2P5"] = lv1_broker.values[lv1_dim_indexes::voltage_2p5];

    values["OH" + std::to_string(m_oh_index) + ".DCS.FPGA_CORE_TEMP"] = temp1_broker.values[temp1_dim_indexes::fpga_core];
    values["OH" + std::to_string(m_oh_index) + ".DCS.FPGA_PT"] = temp1_broker.values[temp1_dim_indexes::fpga_pt];
    values["OH" + std::to_string(m_oh_index) + ".DCS.GBT0_PT"] = temp1_broker.values[temp1_dim_indexes::gbt0_pt];
    values["OH" + std::to_string(m_oh_index) + ".DCS.SCA_TEMP"] = temp1_broker.values[temp1_dim_indexes::sca_core];
    values["OH" + std::to_string(m_oh_index) + ".DCS.VTTX_CSC_PT"] = temp1_broker.values[temp1_dim_indexes::vttx_csc_pt];
    values["OH" + std::to_string(m_oh_index) + ".DCS.VTTX_GEM_PT"] = temp1_broker.values[temp1_dim_indexes::vttx_gem_pt];

    // Push the values to the DCS
    m_dcs_lv1_service->update(lv1_broker);
    m_dcs_temp1_service->update(temp1_broker);
}

gem::monitor::OptoHybridDCSImportAdapter::OptoHybridDCSImportAdapter(const std::string& wire_name, const size_t oh_index)
    : m_oh_index(oh_index)
{
    m_dcs_hv2_service = std::make_unique<core::dim::dcs_service_import<core::dim::hv2_dim_broker>>(wire_name);
    m_dcs_lv2_service = std::make_unique<core::dim::dcs_service_import<core::dim::lv2_dim_broker>>(wire_name);
}

gem::monitor::OptoHybridDCSImportAdapter::~OptoHybridDCSImportAdapter()
{
}

void gem::monitor::OptoHybridDCSImportAdapter::update(nlohmann::json& values) const
{
    const auto [hv_timestamp, hv_broker] = m_dcs_hv2_service->get();
    values["OH" + std::to_string(m_oh_index) + ".HV_STATUS.DRIFT"] = hv_broker.status[0];
    values["OH" + std::to_string(m_oh_index) + ".HV_STATUS.G1TOP"] = hv_broker.status[1];
    values["OH" + std::to_string(m_oh_index) + ".HV_STATUS.G1BOT"] = hv_broker.status[2];
    values["OH" + std::to_string(m_oh_index) + ".HV_STATUS.G2TOP"] = hv_broker.status[3];
    values["OH" + std::to_string(m_oh_index) + ".HV_STATUS.G2BOT"] = hv_broker.status[4];
    values["OH" + std::to_string(m_oh_index) + ".HV_STATUS.G3TOP"] = hv_broker.status[5];
    values["OH" + std::to_string(m_oh_index) + ".HV_STATUS.G3BOT"] = hv_broker.status[6];

    const auto [lv_timestamp, lv_broker] = m_dcs_lv2_service->get();
    values["OH" + std::to_string(m_oh_index) + ".LV_IMON"] = lv_broker.current_mon;
    values["OH" + std::to_string(m_oh_index) + ".LV_VMON"] = lv_broker.voltage_mon;
    values["OH" + std::to_string(m_oh_index) + ".LV_VCON"] = lv_broker.voltage_con;
    values["OH" + std::to_string(m_oh_index) + ".LV_STATUS"] = lv_broker.status;
}
