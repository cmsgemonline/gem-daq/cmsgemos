var actionURL = "";
var labelJson = null;
var level_dict = {0: "version",
                  1: "minimum",
                  2: "default",
                  3: "advanced",
                  4: "maximum",
                  99: "expert",
                 }

// Setting of label file. Timing shouldn't matter for reading in
$.getJSON("/cmsgemos/gemmonitor/html/json/labels.json")
  .done(function(data) {
    return labelJson = data;})
  .fail(showError);

function setupGenericAMCTab(urn, amcN) {
  if (amcN == 13)
    setupAMC13Tab(urn);
  else
    setupAMCTab(urn);
}

/**
 * Function that sets up tab and its update schedule. Called by xdaq process
 *     in gemmonitor by default
 *
 * @param {String} urn Name of the urn (used to identify tab)
 */
function setupAMCTab( urn)
{
  var interval = 1000; // update every 1 second
  const jsonurl = '/'+urn+'/jsonUpdate';
  var urnId = get_amc_tab_id(urn);
  var urnTab  = document.getElementById(urnId);

  // Return if no id which only happens for the shelf
  //    All AMC's have tabs with urns associated with them
  if (urnTab == null) {
    return;
  }

  var monitorables, layout;
  $.when(
    $.getJSON("/cmsgemos/gemmonitor/html/json/monitorables.json", function(data) {
        monitorables = data;
    }),
    $.getJSON("/" + urn + "/layout", function(data) {
        layout = data;
    })
  ).done( function() {
    var metadata = monitorables["metadata"];

    // Create non-OH blocks
    var topRow = makeGenericElement("div", urnTab, {"id": urnId+"_col-topRow", "class":"row"});
    $.each(metadata["top-row-headers"], function(tableId, tableTitle) {
      createTable(urnTab, tableId, tableTitle, monitorables, layout);
    });

    // Create OH blocks
    $.each(metadata["oh-rows-headers"], function(tableId, tableTitle) {
      createTableOH(urnTab, tableId, tableTitle, monitorables, layout);
    });

    // Setup tooltips after all done
    $('[data-toggle="popover"]').popover({"trigger": "hover"})
  }
  ).fail(showError);

  interval = setInterval('sendrequest("'+urn+'")', interval);
}

function setupAMC13Tab(urn) {
  tab = $("#" + get_amc_tab_id(urn));

  form = $('<form>').prop({
    name: "amc13statusform",
  }).appendTo(tab);

  $.each(level_dict, function(level, input_name) {
    $('<input>').prop({
      style: "display: inline-block;",
      type: "radio",
      id: "level"+level,
      class: "amc13statuslevel",
      name: "amc13statuslevel",
      value: level,
      checked: (level == 2)
    }).appendTo(form);

    $('<label>').prop({
      style: `padding-left: 0.3em;
                    display: inline-block;
                    padding-right: 2em;
                    font-size: 130%;`,
      for: "level"+level,
      innerHTML: input_name
    }).appendTo(form);
  });

  $("<span>").prop({
    style: "display:block; float: left",
    name: "amc13status",
    id: "amc13status",
    updateFunc: "/"+urn+"/updateStatus",
    innerHTML: ""
  }).appendTo(tab);

  $( ".amc13statuslevel" ).change(function() {
    updateAMC13Status("/"+urn+"/updateStatus");
  });

  setInterval('updateAMC13Status()', 1000);
}

function updateAMC13Status() {
  $.get($("#amc13status")[0].updateFunc, $('input:checked'))
    .done(function(data) {
      $("#amc13status").html(data);

      // CSS fixes to the AMC13 table
      $("#amc13status table").addClass("table table-bordered")
      $("#amc13status th.name").css("font-size: 20px;");
      $("#amc13status td").addClass("table-active")
      $("#amc13status td.nonerror").removeClass("table-active").addClass("table-success");
      $("#amc13status td.error").removeClass("table-active").addClass("table-danger");
      $("#amc13status td.null").removeClass("table-active").addClass("table-secondary");
    })
    .fail(function(data, textStatus, error) {
      console.error("sendrequest: get failed, status: " + textStatus + ", error: "+error);
      $("#amc13status").html(data);
    });
}

/**
 * Take a variable and "expand" certain tokens to all the possible values
 *     e.g. ${OH} -> [0, 1, ... 11]
 *
 * @param {String} variable Variable to be expanded
 * @param {Array} replaceInfo An array where each entry is an array with the
 *                            token to be replaced and maxValue the token will
 *                            be replaced by, e.g ["${OH}", 12]
 * @return {Array} list of variable expanded in all tokens
 */
function fullExpand(variable, replaceInfo) {
  varList = [variable];
  $.each(replaceInfo, function(key, maxVal) {
    if (variable.includes(key)) {
      var newList = [];
      for (let item of varList) {
        for (var i = 0; i < maxVal; i++) {
          newList.push(item.replace(key, i.toString()));
        }
      }
      varList = newList;
    }
  });

  return varList;
}


function store_actionURL(t_str)
{
  actionURL = t_str;
}

/**
 * Converts value from int to String version of Hex value
 *
 * @param {String} d UnsignedInt32 data as a string
 * @return {String}
 */
function toHex(d) {
  return "0x" + Number(d).toString(16).toLowerCase();
}

var showError = function(data, textStatus, error) {
  console.error("Failed to update JSON data, status: " + textStatus + ", error: "+error);
};

function sendrequest(urn)
{
  const jsonurl = '/'+urn+'/jsonUpdate';

  $.getJSON(jsonurl)
    .done(function(data) {
      updateMonitorables( data, urn );
    })
    .fail(showError);
}

/**
 * Function run every update. Iterates recieved data and inserts into document
 *
 * @param {Object} data Dictionary of data recieved
 * @param {String} urn String of the urn, which is the indicator for the tab/AMC
 */
function updateMonitorables( data, urn )
{
  var urnId = get_amc_tab_id(urn);

  // Clear all cells
  $("td[id^='" + urnId + "']" ).each( function() {
    this.className = "";
    this.innerHTML = "";
  });

  // Update cells with the most recent data
  $.each ( data, function(groupName, group) {
    $.each ( group["values"], function(varName, value) {
      var docVar = document.getElementById(urnId + "_" + varName);
      if (docVar == null) {
        // console.error(varName);
      } else {
        var labelInfo = labelJson[docVar.getAttribute("lblGroup")];
        setTableValue(value, labelInfo, docVar);
      }
    });
  });
};

/**
 * Wrapper for filling of a cell with the value as well as formatting
 *     the data. Might expand as add in more features (eg truncated bit length)
 *
 * @param {String} value String representation of value for cell
 * @param {Object} labelInfo Dictionary of label information used for formatting
 * @param {HTMLElement} tableHTML Element of cell where info is to be put
 */
function setTableValue(value, labelInfo, tableHTML) {
  var hexVal = toHex(value);
  var info = (labelInfo.hasOwnProperty(hexVal) ? labelInfo[hexVal] : labelInfo["default"]);

  tableHTML.className = info["class"]
  if (info.hasOwnProperty("value")) {
    tableHTML.innerHTML = info["value"];
  } else if (info["useHex"]) {
    tableHTML.innerHTML = hexVal;
  } else if (!Number.isInteger(value)) {
    tableHTML.innerHTML = Number.parseFloat(value).toFixed(2);
  } else {
    tableHTML.innerHTML = value;
  }
}

/**
 * Wrapper for creating a generic Element in the document. Used to clean up the
 *     code some
 *
 * @param {String} elName Type name for the element to be created
 * @param {HTMLElement} parent Element where the new element will be placed
 * @param {Object} attrs Dictionary of attributes to be put in the element
 * @param {String} innerHTML String to be put in the content of the element
 */
function makeGenericElement(elName, parent, attrs={}, innerHTML="") {
  var el = document.createElement(elName);
  $.each(attrs, function(key, attr) {
    el.setAttribute(key, attr);
  });
  if (innerHTML != "") {
    el.innerHTML = innerHTML;
  }
  parent.appendChild(el);
  return el;
}

/**
 * Create a tablefor a group of variables. Defaults to a size of 1/3 the screen
 *
 * @param {HTMLElement} tab Tab where the table will be put
 * @param {String} tableId Name used to identify the table and monitorables in it
 * @param {String} tableName Name used in the header of the HTML for the table
 * @param {Object} json JSON object containing the list of monitorables
 * @param {Object} layout JSON object containing the AMC layout
 */
function createTable(tab, tableId, tableName, json, layout) {
  var replaceVars = {
      "${OH}": layout["num-oh"],
      "${GBT}": layout["num-gbt"],
      "${VFAT}": layout["num-vfat"]
  }

  var row = $('[id="' + tab.id + '_col-topRow"]').get(0);
  var col = makeGenericElement("div", row, {"class":"col-lg-4",
                                            "style": "font-size: 140%"});
  makeGenericElement("h1", col, {"align": "center"}, tableName);
  var table = makeGenericElement("table", col, {"name": tableId, "class": "table table-bordered"});

  // Header
  var thead = makeGenericElement("thead", table, {}, '<tr> <th scope="col"> name</th> <th scope="col">value</th> </tr>');

  // Body
  var tbody = makeGenericElement("tbody", table);
  $.each(json[tableId], function(monName, monGroup) {
    // Expand ${} tokens then fill table
    $.each(fullExpand(monName, replaceVars), function(idx, fullName) {
      var tr = makeGenericElement("tr", tbody);
      var varCell = makeGenericElement("th", tr, {"scope": "col",
                                                  "style": "width: 70%"});
      makeGenericElement("span", varCell,
                         {"data-toggle":"popover",
                          "title": monGroup["register-name"],
                          "data-content":monGroup["description"]},
                         fullName);
      makeGenericElement("td", tr,
                         {"id": tab.id + "_" + fullName,
                          "style": "text-align: center;",
                          "lblGroup": monGroup["label"]} ,
                         0);
    });
  });
}

/**
 * Create a table specifically for a group that splits on the OH
 *     i.e. a column for each OH
 *
 * @param {HTMLElement} tab Tab where the table will be put
 * @param {String} tableId Name used to identify the table and monitorables in it
 * @param {String} tableName Name used in the header of the HTML for the table
 * @param {Object} json JSON object containing the list of monitorables
 * @param {Object} layout JSON object containing the AMC layout
 */
function createTableOH(tab, tableId, tableName, json, layout) {
  var numOHs = layout["num-oh"];
  var replaceVars = {
      "${GBT}": layout["num-gbt"],
      "${VFAT}": layout["num-vfat"]
  }

  // Special OH stuff
  var col = makeGenericElement("div", tab, {"class":"col-lg-12",
                                            "style": "font-size: 140%"});
  var card = makeGenericElement("div", col, {"class": "card"});
  var cardHeader = makeGenericElement("div", card, {"class": "card-header", "align": "center"});
  var header = makeGenericElement("h2", cardHeader, {"align": "center"});
  makeGenericElement("a", header, {"data-toggle": "collapse", "href":"#collapse-"+tableId},
                     tableName);
  var collapseGroup = makeGenericElement("div", card, {"class": "collapse",
                                                       "id": "collapse-"+tableId});
  var table = makeGenericElement("table", collapseGroup,
                                 {"name":tableId, "class": "table table-bordered"});

  // Header
  var thead = makeGenericElement("thead", table);
  var tr = makeGenericElement("tr", thead);
  var th = makeGenericElement("th", tr, {"scope": "col"},  'REGISTER|OH');
  for (i = 0; i < numOHs; i++) {
    var oh_info = "Unknown OptoHybrid (not found in layout tree)";
    if (String(i) in layout["oh"]) {
      oh_info = "<b>Display</b>: " + layout["oh"][String(i)]["display"];
      oh_info += "<br><b>UID</b>: " + layout["oh"][String(i)]["uid"];
    }
    var th = makeGenericElement("th", tr, {"scope": "col",
                                           "data-toggle": "popover",
                                           "title": "OH "+i,
                                           "data-content": oh_info,
                                           "data-html": "true",
                                          },
                                String(i));
  }

  // Body
  var tbody = makeGenericElement("tbody", table);
  $.each( json[tableId], function(monName, monGroup) {
    // Expand ${} tokens then fill table
    $.each(fullExpand(monName, replaceVars), function(idx, fullName) {
      var tr = makeGenericElement("tr", tbody);
      var varCell = makeGenericElement("th", tr, {"scope": "col",
                                                  "style": "width: 20%"});
      var colName = fullName.replace("OH${OH}.", "").replace("_OH${OH}", "");
      makeGenericElement("span", varCell,
                         {"data-toggle":"popover",
                          "title": monGroup["register-name"],
                          "data-content":monGroup["description"]},
                         colName);
      for (i = 0; i < numOHs; i++) {
        var ohID = tab.id + "_" + fullName.replace("${OH}", i.toString());
        makeGenericElement("td", tr,
                           {"id": ohID,
                            "style": "text-align: center;",
                            "lblGroup": monGroup["label"]
                           }, 0);
      }
    });
  });
}

function expert_action(id)
{
  $.getJSON(actionURL+id+"Action")
    .done(function(data) {
      document.getElementById( "mon_state" ).innerHTML = data["mon_state"];
      console.info("expert action call succeeded");
    })
    .fail(showError);
}

function get_amc_tab_id(urn)
{
  var instance = urn.substring(urn.indexOf("=")+1);
  return "amc-monitor-tab-" + instance
}

function get_amc_status(urn, id)
{
  $.getJSON(urn+"/stateUpdate")
    .done(function(data) {
      $("#"+id).html(data["fsm-state"])
    })

}
