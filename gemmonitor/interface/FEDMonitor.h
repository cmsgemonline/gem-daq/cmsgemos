/// @file

#ifndef GEM_MONITOR_FEDMONITOR_H
#define GEM_MONITOR_FEDMONITOR_H

#include <gem/utils/GEMApplication.h>

#include <string>
#include <vector>

namespace gem::monitor {

class FEDMonitor : public gem::utils::GEMApplication {
public:
    XDAQ_INSTANTIATOR();

    FEDMonitor(xdaq::ApplicationStub* s);

    virtual ~FEDMonitor();

    void actionPerformed(xdata::Event& event) override;

    /// @brief Takes an AMC xDAQ app and returns the AMC number for it
    ///
    /// @params xDAQ app used to find the AMC number
    ///
    /// @returns number of the slot for the AMC app
    uint32_t getSlot(const xdaq::ApplicationDescriptor* app);

    std::vector<const xdaq::ApplicationDescriptor*> v_amc_apps; ///< vector of amc's in this fed

private:
    void configureAction(xgi::Input* in, xgi::Output* out);

    void startAction(xgi::Input* in, xgi::Output* out);

    void stopAction(xgi::Input* in, xgi::Output* out);

    /// @brief setup variables to be used in the fedmonitor class
    ///
    /// Iterates through the different xdaq apps and connects the respective
    /// xdaq apps that are in the same fed. The apps that do match
    /// this fed are set to the apps variables
    void init();

    /// @brief Check if an app is an AMC in the same FED
    ///
    /// @param app to be checked
    bool isAMCInFED(const xdaq::ApplicationDescriptor* app) const;

    xdata::UnsignedInteger32 m_fed_id = -1; ///< Specifies the FED ID to monitor
};

} // namespace gem::monitor

#endif // GEM_MONITOR_FEDMONITOR_H
