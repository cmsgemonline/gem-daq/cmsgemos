/// @file

#ifndef GEM_MONITOR_DCSADAPTERS_H
#define GEM_MONITOR_DCSADAPTERS_H

#include <gem/core/dim/dcs.h>

#include <boost/circular_buffer.hpp>
#include <nlohmann/json.hpp>

#include <memory>

namespace gem::monitor {

class AMCMonitorWeb;

/// @brief Helper class to export all DIM services related to a given OptoHybrid to the DCS
class OptoHybridDCSExportAdapter {
public:
    OptoHybridDCSExportAdapter(const std::string& wire_name,
        size_t oh_index, unsigned n_samples = 20,
        double voltage_deadband = 0.05, double temperature_deadband = 0.5);
    virtual ~OptoHybridDCSExportAdapter();

    // Move-only policy
    OptoHybridDCSExportAdapter(OptoHybridDCSExportAdapter&&) = default;
    OptoHybridDCSExportAdapter& operator=(OptoHybridDCSExportAdapter&&) = default;

    /// @brief Reset the values history and DIM services
    void reset();

    // @brief Push new samples into the values history
    void pushNewSamples(const std::string& group, const nlohmann::json& values);

    /// @brief Update the DIM services
    void update(nlohmann::json& values) const;

protected:
    size_t m_oh_index = -1;
    double m_voltage_deadband;
    double m_temperature_deadband;

    std::array<boost::circular_buffer<std::optional<double>>, 5> m_lv1_hist;
    std::array<boost::circular_buffer<std::optional<double>>, 6> m_temp1_hist;

    std::unique_ptr<core::dim::dcs_service_export<core::dim::lv1_dim_broker>> m_dcs_lv1_service;
    std::unique_ptr<core::dim::dcs_service_export<core::dim::temp1_dim_broker>> m_dcs_temp1_service;
};

/// @brief Helper class to import from the DCS all DIM services related to a given OptoHybrid
class OptoHybridDCSImportAdapter {
public:
    OptoHybridDCSImportAdapter(const std::string& wire_name, size_t oh_index);
    virtual ~OptoHybridDCSImportAdapter();

    // Move-only policy
    OptoHybridDCSImportAdapter(OptoHybridDCSImportAdapter&&) = default;
    OptoHybridDCSImportAdapter& operator=(OptoHybridDCSImportAdapter&&) = default;

    void update(nlohmann::json& values) const;

protected:
    size_t m_oh_index = -1;
    std::unique_ptr<core::dim::dcs_service_import<core::dim::hv2_dim_broker>> m_dcs_hv2_service;
    std::unique_ptr<core::dim::dcs_service_import<core::dim::lv2_dim_broker>> m_dcs_lv2_service;
};

} // namespace gem::monitor

#endif // GEM_MONITOR_DCSADAPTERS_H
