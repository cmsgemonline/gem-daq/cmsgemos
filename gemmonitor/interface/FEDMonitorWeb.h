/// @file

#ifndef GEM_MONITOR_FEDMONITORWEB_H
#define GEM_MONITOR_FEDMONITORWEB_H

#include <gem/utils/GEMWebApplication.h>

#include <memory>

namespace gem::monitor {

// Forward declaration
class FEDMonitor;

class FEDMonitorWeb : public gem::utils::GEMWebApplication {
public:
    FEDMonitorWeb(FEDMonitor* application);
    virtual ~FEDMonitorWeb();

protected:
    void monitorPage(xgi::Input* in, xgi::Output* out);

    void expertPage(xgi::Input* in, xgi::Output* out);
};

} // namespace gem::monitor

#endif // GEM_MONITOR_FEDMONITORWEB_H
