/// @file

#ifndef GEM_MONITOR_AMCMONITORWEB_H
#define GEM_MONITOR_AMCMONITORWEB_H

#include <gem/utils/GEMWebApplication.h>

#include <memory>

namespace gem::monitor {

// Forward declaration
class AMCMonitor;

class AMCMonitorWeb : public gem::utils::GEMWebApplication {
public:
    AMCMonitorWeb(AMCMonitor* application);
    virtual ~AMCMonitorWeb();

protected:
    void jsonUpdate(xgi::Input* in, xgi::Output* out) override;
    void expertPage(xgi::Input* in, xgi::Output* out);
};

} // namespace gem::monitor

#endif // GEM_MONITOR_AMCMONITORWEB_H
