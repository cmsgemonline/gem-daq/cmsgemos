/// @file

#ifndef GEM_MONITOR_AMCMONITOR_H
#define GEM_MONITOR_AMCMONITOR_H

#include <gem/utils/GEMGenericFSMApplication.h>

#include <gem/monitor/DCSAdapters.h>
#include <xhal/client/XHALInterface.h>

#include <nlohmann/json.hpp>
#include <toolbox/task/TimerFactory.h>
#include <toolbox/task/TimerListener.h>
#include <xdata/Bag.h>
#include <xdata/Double.h>
#include <xdata/String.h>
#include <xdata/UnsignedInteger32.h>
#include <xdata/Vector.h>

#include <memory>
#include <mutex>

namespace gem::monitor {

class AMCMonitorWeb;

/// @brief
class AMCMonitor : public gem::utils::GEMGenericFSMApplication,
                   public toolbox::task::TimerListener {

    friend class AMCMonitorWeb;

public:
    XDAQ_INSTANTIATOR();

    /// @brief Constructor
    AMCMonitor(xdaq::ApplicationStub* s);

    /// @brief Destructor
    virtual ~AMCMonitor();

    // State transitions
    void configureAction();
    void startAction();
    void stopAction();

private:
    // Hardware description
    xdata::UnsignedInteger32 m_fed_id = -1; ///< Specifies the FED ID of which the AMC is part of
    xdata::UnsignedInteger32 m_slot = -1; ///< Specifies the slot in which the AMC is located

    // @brief Store the configuration for the DCS exporter
    struct dcs_export_config {
        xdata::UnsignedInteger32 nSamples = 20; ///< Number of samples to use in the sliding average
        xdata::Double voltageDeadBand = 0.05; ///< Dead band for the voltage metrics
        xdata::Double temperatureDeadBand = 0.5; ///< Dead band for the temperature metrics

        void registerFields(xdata::Bag<dcs_export_config>* bag);
        std::string toString() const;
    };

    xdata::Bag<dcs_export_config> m_dcs_export_config;

    std::unique_ptr<xhal::client::XHALInterface> m_amc; ///< RPC connection

    uint32_t m_sca_mask = 0; ///< Specifies the SCA to monitor
    uint32_t m_optohybrid_mask = 0; ///< Specifies the OptoHybrids to monitor

    std::vector<OptoHybridDCSExportAdapter> m_oh_dcs_export_services; ///< Stores the exported DCS services for all monitored OptoHybrids
    std::vector<OptoHybridDCSImportAdapter> m_oh_dcs_import_services; ///< Stores the imported DCS services for all monitored OptoHybrids

    // Web UI
    void jsonGetLayout(xgi::Input* in, xgi::Output* out);

    nlohmann::json m_layout; ///< Stores the layout parameters used for Web UI rendering

protected:
    /// @brief Adds a new monitorable group
    ///
    /// While the basic monitoring features are taken care of by the framework,
    /// the update function can be used to implement additional behaviors, e.g.
    /// cache value updates, interface with another sub-system,...
    ///
    /// @param @c name Monitorable group name
    /// @param @c period Default update period of the group (in seconds)
    /// @param @c function Update function to be called
    void add_monitorable_group(const std::string& name, double period, const std::function<nlohmann::json()>& function);

    /// @brief Group callback function signature
    ///
    /// The first argument is the group name; the second argument is the group
    /// JSON object, containing key-value pairs.
    using callback_t = std::function<void(const std::string&, const nlohmann::json&)>;

    /// @brief Adds a callback function to be called after each update of the group
    ///
    /// Note that the callback is _always_ executed, even in case the group
    /// update fails. In such situations, the JSON document is an empty object.
    ///
    /// @param @c name Monitorable group name
    /// @param @c callback Callback function to be added
    void add_callback(const std::string& name, callback_t callback);

    /// @brief Start the monitoring polling
    void start_monitoring();

    /// @brief Stops the monitoring polling
    void stop_monitoring();

    /// @brief JSON object containing all monitorables from all groups
    ///
    /// The object structure is as follow:
    /// {
    ///   "<group-name>": {
    ///     "last-update": <last-update-timestamp [s]>
    ///     "last-update-duration": <last-update-duration [s]>
    ///     "values": {
    ///       "<monitorable-name>": <monitorable-value>
    ///       ...
    ///     }
    ///   }
    ///   ...
    /// }
    nlohmann::json m_values;

    mutable std::string m_values_cache; ///< Stringified version of the @c m_values JSON object to be used as a cache
    mutable std::mutex m_values_mutex; ///< Mutex on @c m_values and its cache @c m_values_cache

private:
    toolbox::task::Timer* m_monitoring_timer = nullptr; ///< xDAQ-based monitoring timer

    /// @brief xDAQ-based monitoring timer callback function
    void timeExpired(toolbox::task::TimerEvent& event) override;

    /// @brief Updates the @c name monitorable group based on the associated @c monitorable_group_info description structure
    void update_monitorable_group(const std::string& name);

    /// @brief Stores the information regarding a specific monitoring group
    struct monitorable_group_info {
        std::function<nlohmann::json()> function; ///< Update function
        double period; ///< Update period
        std::vector<callback_t> callbacks; ///< List of callbacks to be executed after a group update, successful or not
    };

    std::unordered_map<std::string, monitorable_group_info> m_monitorable_group_infos; ///< Stores the information about all monitorable groups

    // @brief Store the configuration for a specific monitorable group
    struct monitorable_group_config {
        xdata::String name; ///< Group name
        xdata::Double period; ///< Update period

        void registerFields(xdata::Bag<monitorable_group_config>* bag);
        std::string toString() const;
    };

    xdata::Vector<xdata::Bag<monitorable_group_config>> m_monitorable_group_configs; ///< Specifies the monitorble group configurations
};

} // namespace gem::monitor

#endif // GEM_MONITOR_AMCMONITOR_H
