/// @file

#ifndef GEM_MONITOR_AMC13MONITORWEB_H
#define GEM_MONITOR_AMC13MONITORWEB_H

#include <gem/utils/GEMWebApplication.h>

#include <memory>

namespace gem::monitor {

class AMC13Monitor;

class AMC13MonitorWeb : public gem::utils::GEMWebApplication {
public:
    AMC13MonitorWeb(AMC13Monitor* app);

    virtual ~AMC13MonitorWeb();

protected:
    void expertPage(xgi::Input* in, xgi::Output* out);
};

} // namespace gem::monitor

#endif // GEM_MONITOR_AMC13MONITORWEB_H
