/// @file

#ifndef GEM_MONITOR_EXCEPTION_EXCEPTION_H
#define GEM_MONITOR_EXCEPTION_EXCEPTION_H

#include <gem/utils/exception/Exception.h>

#include <string>

#define GEM_MONITOR_DEFINE_EXCEPTION(EXCEPTION_NAME) GEM_DEFINE_EXCEPTION(EXCEPTION_NAME, monitor)
GEM_MONITOR_DEFINE_EXCEPTION(Exception)
GEM_MONITOR_DEFINE_EXCEPTION(SoftwareProblem)
GEM_MONITOR_DEFINE_EXCEPTION(HardwareProblem)
GEM_MONITOR_DEFINE_EXCEPTION(ValueError)

#define GEM_MONITOR_DEFINE_ALARM(ALARM_NAME) GEM_MONITOR_DEFINE_EXCEPTION(ALARM_NAME)
GEM_MONITOR_DEFINE_ALARM(MonitoringFailureAlarm)

#endif // GEM_MONITOR_EXCEPTION_EXCEPTION_H
