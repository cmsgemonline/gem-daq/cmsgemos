/// @file

#ifndef GEM_MONITOR_AMC13MONITOR_H
#define GEM_MONITOR_AMC13MONITOR_H

#include <gem/utils/GEMGenericFSMApplication.h>
#include <gem/utils/Lock.h>
#include <gem/utils/LockGuard.h>

#include <xdata/String.h>

namespace amc13 {
class AMC13;
class Status;
}

namespace gem::monitor {

typedef std::shared_ptr<::amc13::AMC13> amc13_ptr;

class AMC13MonitorWeb;

class AMC13Monitor : public gem::utils::GEMGenericFSMApplication {
    friend class AMC13MonitorWeb;

public:
    XDAQ_INSTANTIATOR();

    AMC13Monitor(xdaq::ApplicationStub* s);
    virtual ~AMC13Monitor();

    /// @brief Grab the pointer to the amc13
    ///
    /// To monitor, the `getStatus` function needs to be called on the
    /// amc13 itself. This fsm command grabs that pointer and changes the
    /// state from INITIAL to all the code to get the monitor page (`updateStatus`
    /// only grabs the status if not in the INITIAL state)
    void configureAction();

    /// @brief Update monitor page for the AMC13
    ///
    /// Takes an input monitor level and uses that to change the AMC13
    /// monitor html that is put into the output stream
    ///
    /// @param input stream where monitor level is retrieved
    /// @param output stream where monitor page is put
    void updateStatus(xgi::Input* in, xgi::Output* out);

private:
    amc13_ptr p_amc13; ///< AMC13 monitor object
    mutable gem::utils::Lock m_amc13Lock;
    log4cplus::Logger m_logger;

    xdata::UnsignedInteger32 m_fed_id;
};

} // namespace gem::monitor

#endif // GEM_MONITOR_AMC13MONITOR_H
