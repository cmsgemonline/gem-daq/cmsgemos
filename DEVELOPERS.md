# Developing cmsgemos

* [Development Setup](#setup)
* [Running Tests](#tests)
* [Code Style Guidelines](#rules)
* [Writing Documentation](#documentation)

## <a name="setup"> Development Setup
### Development requirements

A successful complete build requires multiple dependencies and tools to be installed. Please refer to the `Dockerfile` for the most recent and complete version.

Some of the tools require to be activated. On CERN CentOS 7:

* `devtoolset-8`
* `rh-git218`
* `clang+llvm-14.0.6` (custom)

On AlmaLinux 9, only our custom `clang+llvm-14.0.6` package requires activation.

Developers using the `gem904daq04` machine (currently running CC7), can source this script before starting their developments:

``` bash
# Software releases
source scl_source enable rh-git218
source scl_source enable devtoolset-8
export PATH=/opt/clang+llvm/14.0.6-1/bin:$PATH
```

### Build instructions

* Clone the repository `git clone https://gitlab.cern.ch/cmsgemonline/cmsgemos.git && cd cmsgemos`

To build the DAQ machine library and applications, use the following instructions:

* Configure the build with `cmake3 -G "Ninja" -DCMAKE_BUILD_TYPE=RelWithDebInfo -S . -B _build`
  (To do only once, or when configuration parameters change.)
* Build and install locally the software with `cmake3 --build _build --target install`
  The local installation is available under `_build/_install`
* You can trigger the tests with `cmake3 --build _build --target test`

To build the backend board libraries and applications, use the following instructions:

* Go to the `gemhardware` sub-directory `cd gemhardware`
* Configure the build with `cmake3 -G "Ninja" -DCMAKE_BUILD_TYPE=RelWithDebInfo -DGEM_BACKEND=<gem-backend> -DGEM_STATION=<gem-station> -S . -B _build`
  * The GEM backend board is one of `ctp7`, `cvp13`, `apex`, `x2o`.
    **In the case of the CTP7, the following parameter must be added**: `-DCMAKE_TOOLCHAIN_FILE=../cmake/ToolchainCTP7.cmake`.
    **In the case of the APEX, the following parameter must be added**: `-DCMAKE_TOOLCHAIN_FILE=../cmake/ToolchainAPEX.cmake`.
    **In the case of the X2O, the following parameter must be added**: `-DCMAKE_TOOLCHAIN_FILE=../cmake/ToolchainX2O.cmake`.
  * The GEM station is one of `me0`, `ge11` or `ge21`.
* Build and install in a local tree backend software:
  * For the CTP7: `DESTDIR=_install cmake3 --build _build --target install`
  * For the CVP13: `cmake3 --build _build --target install`
  The local installation is available under `_build/_install`

As specified in the commands above, the recommended build type is `RelWithDebInfo`.
It generates an optimized version of the binaries as well as debugging symbols.
If you don't need the debugging symbols, the variable `CMAKE_BUILD_TYPE` can be set to `Release`.
If you want to disable the optimizations to easier debug the code, you can set `CMAKE_BUILD_TYPE` to `Debug`.

The software is deployed on the machines as RPM packages built in the CI.
Developers are encouraged to test the RPM package build when introducing changes in the build system or the SPEC file.

## <a name="tests"> Running Tests
For the moment autotests are not supported (but will be added in the near future).
Thus you should manually test your code before opening a merge request.
In case you need a hardware setup, reserve it via SuperSaaS as [described in the user documentation](https://cmsgemonline.web.cern.ch/doc/setups-description/#integration-setup-usage).
You can reserve the `B904 E1A16-13 (GE11)` setup to test your code.

Example configuration and instructions on manual running are described [here](doc/examples/README.md).

## <a name="rules"> Code Style Guidelines
We are following WebKit [Code Style Guideline][webkit] for `C++` and [PEP8][pep8] standard for python code.
We also find very useful [C++ Core Guidelines][cppcore] by Bjarne Stroustrup and Herb Sutter and strongly encourage you to read them too.

The Git pre-commit hook can be used to automatically detect and fix the formatting issues before committing your changes.
Any formatting issue will prevent the CI to pass and prevent any MR to be merged.
Thus, we encourage developers to configure the hook as follow.

```shell
ln -s ../../.pre-commit.sh .git/hooks/pre-commit
```

## <a name="documentation"> Writing Documentation
We use the [Doxygen][doxygen] for `C++` API documentation and [sphinx][sphinx] for `python` code documentation.
The documentation can be generated using the following command:

```bash
cmake3 --build _build --target doc
```

Generated files are placed in `_build/html`.

This section will be completed later with precise documentation style guide, for the moment follow the examples from the code itself.

[supersaas]:https://www.supersaas.com/schedule/GEM_904_Infrastructure
[webkit]:https://webkit.org/code-style-guidelines/
[cppcore]:https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md
[pep8]:https://www.python.org/dev/peps/pep-0008/
[doxygen]:http://www.doxygen.nl/manual/starting.html
[sphinx]:https://www.sphinx-doc.org/en/master/usage/quickstart.html
