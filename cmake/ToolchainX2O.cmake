#
# Defaults
#

# CMake uses toolchain files multiple times, but cannot always read cache
# variables. Cache variables are always accessible on the first run, so push
# the variables that need to persist and an intermediary environment variable.
if(NOT DEFINED GEM_SYSROOT)
    if(NOT DEFINED ENV{GEM_SYSROOT})
        set(GEM_SYSROOT "/opt/gem-sysroot/x2o-2023101101/")
    else()
        set(GEM_SYSROOT "$ENV{GEM_SYSROOT}")
    endif()
endif()
set(ENV{GEM_SYSROOT} "${GEM_SYSROOT}")

#
# Toolchain configuration
#
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(CMAKE_C_COMPILER clang)
set(CMAKE_CXX_COMPILER clang++)

# CMake incorrect determines the Compiler Identifier
# * The CMAKE_*_COMPILER_TARGET flags are not considered while detecting the C/C++ compilers
#   => Add the flags manually in CMAKE_*_FLAGS
# * The toolchain prefix incorrectly detected as "aarch64-redhat-linux-"
#   => Keep the CMAKE_*_COMPILER_TARGET variables undefined
# Bug present in 3.17 (EPEL7) -- Fixed in 3.20 (Alma 9)
#
# set(CMAKE_C_COMPILER_TARGET "aarch64-redhat-linux"
# set(CMAKE_CXX_COMPILER_TARGET "aarch64-redhat-linux"
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} --target=aarch64-redhat-linux")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --target=aarch64-redhat-linux")

set(CMAKE_EXE_LINKER_FLAGS "-fuse-ld=lld")
set(CMAKE_MODULE_LINKER_FLAGS "-fuse-ld=lld")
set(CMAKE_SHARED_LINKER_FLAGS "-fuse-ld=lld")

set(CMAKE_SYSROOT ${GEM_SYSROOT})

# CMake does not prioritize LLVM over the system defaults, even with using with Clang
# Bug present in 3.17 (EPEL7) and 3.20 (Alma 9) -- Fixed in 3.21
find_program(CMAKE_AR NAMES llvm-ar)
find_program(CMAKE_RANLIB NAMES llvm-ranlib)
find_program(CMAKE_STRIP NAMES llvm-strip)
find_program(CMAKE_LINKER NAMES ld.lld)
find_program(CMAKE_NM NAMES llvm-nm)
find_program(CMAKE_OBJDUMP NAMES llvm-objdump)
find_program(CMAKE_OBJCOPY NAMES llvm-objcopy)
find_program(CMAKE_READELF NAMES llvm-readelf)
find_program(CMAKE_DLLTOOL NAMES llvm-dlltool)
find_program(CMAKE_ADDR2LINE NAMES llvm-addr2line)

#
# CMake configuration
#
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
