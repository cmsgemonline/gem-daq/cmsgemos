# This file makes {fmt} available to the build system

include(FetchContent)

function(_BuildFmt)
    # Enable linking into shared libraries
    set(CMAKE_POSITION_INDEPENDENT_CODE ON)

    FetchContent_Declare(
        fmt
        URL ${CMAKE_CURRENT_LIST_DIR}/../extern/fmt-9.1.0.tar.gz
    )
    FetchContent_MakeAvailable(fmt)

    # Build only the targets we depend on
    if(IS_DIRECTORY "${fmt_SOURCE_DIR}")
        set_property(DIRECTORY "${fmt_SOURCE_DIR}" PROPERTY EXCLUDE_FROM_ALL YES)
    endif()
endfunction()

_BuildFmt()
