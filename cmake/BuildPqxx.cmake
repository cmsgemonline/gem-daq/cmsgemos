# This files compiles libpqxx and makes it available as the "pqxx" target.

include(FetchContent)

function(_BuildPqxx)
    # We require PostgreSQL 10, but it is not available in the standard CentOS
    # 7 repositories. If the SCL folder is around, help CMake find it.
    set(toolset_prefix /opt/rh/rh-postgresql10/root/usr/)
    set(toolset_include_dir ${toolset_prefix}/include/)
    set(toolset_library ${toolset_prefix}/lib64/libpq.so)
    set(toolset_type_include_dir ${toolset_include_dir})

    if (NOT PostgreSQL_ROOT # Don't overwrite user-specified settings
        AND IS_DIRECTORY ${toolset_include_dir}
        AND EXISTS ${toolset_library}
        AND IS_DIRECTORY ${toolset_type_include_dir})
        message(STATUS "Setting PostgreSQL_ROOT to ${toolset_prefix}")
        set(PostgreSQL_ROOT ${toolset_prefix})
    endif()

    # Pre-find PostgreSQL
    cmake_policy(SET CMP0074 NEW) # Use PostgreSQL_ROOT
    find_package(PostgreSQL 10 REQUIRED)

    # Always enable IPO/LTO if INTERPROCEDURAL_OPTIMIZATION is set
    set(CMAKE_POLICY_DEFAULT_CMP0069 NEW)

    # Prevent option() to override a previously set normal variable
    set(CMAKE_POLICY_DEFAULT_CMP0077 NEW)

    # We need to unset BUILD_TEST because libpqxx tests depend on the availability
    # of a database, and since we use a stable release they should all pass.
    set(BUILD_TEST OFF)

    # Now get pqxx and make sure it's available
    FetchContent_Declare(
        pqxx
        URL ${CMAKE_CURRENT_LIST_DIR}/../extern/libpqxx-7.7.4.tar.gz
    )
    FetchContent_MakeAvailable(pqxx)

    # Since we build a static library but use it from shared libs, make sure that
    # PIE is used.
    set_property(TARGET pqxx PROPERTY POSITION_INDEPENDENT_CODE TRUE)

    # Build only the targets we depend on
    if(IS_DIRECTORY "${pqxx_SOURCE_DIR}")
        set_property(DIRECTORY "${pqxx_SOURCE_DIR}" PROPERTY EXCLUDE_FROM_ALL YES)
    endif()
endfunction()

_BuildPqxx()
