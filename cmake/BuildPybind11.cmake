# This file makes pybind11 available to the build system

function(_BuildPybind11)
    include(FetchContent)
    FetchContent_Declare(
        pybind11
        URL ${CMAKE_CURRENT_LIST_DIR}/../extern/pybind11-2.7.1.tar.gz
    )
    FetchContent_MakeAvailable(pybind11)

    # Build only the targets we depend on
    if(IS_DIRECTORY "${pybind11_SOURCE_DIR}")
        set_property(DIRECTORY "${pybind11_SOURCE_DIR}" PROPERTY EXCLUDE_FROM_ALL YES)
    endif()
endfunction()

_BuildPybind11()
