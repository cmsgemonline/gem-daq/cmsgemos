# This files compiles log4cplus and makes it available to the build system

include(FetchContent)

function(_BuildLog4cplus)
    FetchContent_Declare(
        log4cplus
        URL ${CMAKE_CURRENT_LIST_DIR}/../extern/log4cplus-2.0.8.tar.xz
    )
    FetchContent_MakeAvailable(log4cplus)

    # Build only the targets we depend on
    if(IS_DIRECTORY "${log4cplus_SOURCE_DIR}")
        set_property(DIRECTORY "${log4cplus_SOURCE_DIR}" PROPERTY EXCLUDE_FROM_ALL YES)
    endif()
endfunction()

_BuildLog4cplus()
