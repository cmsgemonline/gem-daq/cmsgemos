#.rst:
#
# CheckForUndefinedSymbols
# ------------------------
#
# This module attempts to enable link-time checks for undefined symbols
# for all the binary objects that can be linked, particularly the shared
# libraries for which no link-time check for undefined symbols is performed.
#
# If the appropriate compilation flag cannot be found, a warning message is
# displayed, but the build will not be prevented.
#
# The allow_undefined_symbols function can be used to disable the link-time
# check on a given target if needed (e.g. Python module or application plugin).
#

include(CheckCXXSourceCompiles)

function(_check_no_undefined_flag _var)
    set(CMAKE_REQUIRED_FLAGS "-Wl,--no-undefined")
    check_cxx_source_compiles("int main() { return 0; }" ${_var})
    set(${_var} "${${_var}}" PARENT_SCOPE)
endfunction()

_check_no_undefined_flag(NO_UNDEFINED_EXIST)

if (NO_UNDEFINED_EXIST)
    add_link_options("-Wl,--no-undefined")
else()
    message(WARNING "The linker cannot check for undefined symbols in the shared objects.")
endif()

function(allow_undefined_symbols target)
    get_target_property(TARGET_LINK_OPTIONS ${target} LINK_OPTIONS)
    if (TARGET_LINK_OPTIONS)
        list(REMOVE_ITEM TARGET_LINK_OPTIONS "-Wl,--no-undefined")
        set_target_properties(${target} PROPERTIES LINK_OPTIONS "${TARGET_LINK_OPTIONS}")
    endif()
endfunction()
