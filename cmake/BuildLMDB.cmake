# This file build the LMDB library and makes it available to the build system

include(FetchContent)

function(_BuildLMDB)
    FetchContent_Declare(
        lmdb
        URL ${CMAKE_CURRENT_LIST_DIR}/../extern/lmdb-0.9.29.tar.xz
    )

    # The LMDB library does use CMake as build system
    # Integrate our custom CMakeLists.txt
    FetchContent_GetProperties(lmdb)
    if(NOT lmdb_POPULATED)
        FetchContent_Populate(lmdb)

        configure_file(${CMAKE_CURRENT_LIST_DIR}/../extern/lmdb-0.9.29.cmake ${lmdb_SOURCE_DIR}/CMakeLists.txt COPYONLY)

        add_subdirectory(${lmdb_SOURCE_DIR} ${lmdb_BINARY_DIR} EXCLUDE_FROM_ALL)
    endif()

    # Build only the targets we depend on
    if(IS_DIRECTORY "${lmdb_SOURCE_DIR}")
        set_property(DIRECTORY "${lmdb_SOURCE_DIR}" PROPERTY EXCLUDE_FROM_ALL YES)
    endif()
endfunction()

_BuildLMDB()
