# This file makes CLI11 available to the build system

include(FetchContent)

function(_BuildCLI11)
    FetchContent_Declare(
        CLI11
        URL ${CMAKE_CURRENT_LIST_DIR}/../extern/CLI11-2.2.0.tar.gz
    )
    FetchContent_MakeAvailable(CLI11)

    # Build only the targets we depend on
    if(IS_DIRECTORY "${CLI11_SOURCE_DIR}")
        set_property(DIRECTORY "${CLI11_SOURCE_DIR}" PROPERTY EXCLUDE_FROM_ALL YES)
    endif()
endfunction()

_BuildCLI11()
