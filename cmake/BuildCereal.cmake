# This file makes cereal available to the build system

# Use target_link_libraries() from a different directory
if(POLICY CMP0079)
    cmake_policy(SET CMP0079 NEW)
endif()

include(FetchContent)

function(_BuildCereal)
    find_package(Threads REQUIRED)

    # Do not build any executable or documentation
    set(JUST_INSTALL_CEREAL ON)

    # Enable multi-threading support
    set(THREAD_SAFE ON)

    FetchContent_Declare(
        cereal
        URL ${CMAKE_CURRENT_LIST_DIR}/../extern/cereal-1.3.2.tar.gz
    )
    FetchContent_MakeAvailable(cereal)

    # Fix multi-threading support from upstream CMakeLists.txt
    target_compile_definitions(cereal INTERFACE CEREAL_THREAD_SAFE=1)
    target_link_libraries(cereal INTERFACE Threads::Threads)

    # Build only the targets we depend on
    if(IS_DIRECTORY "${cereal_SOURCE_DIR}")
        set_property(DIRECTORY "${cereal_SOURCE_DIR}" PROPERTY EXCLUDE_FROM_ALL YES)
    endif()
endfunction()

_BuildCereal()
