#.rst:
#
# FindxDAQ
# --------
#
# This module can be used to find libraries provided by the xDAQ framework. It can
# be used using the standard
# `find_package <https://cmake.org/cmake/help/latest/command/find_package.html>`_
# function. It is possible to specify the list of required libraries using the
# `COMPONENTS` and `OPTIONAL_COMPONENTS` arguments of `find_package` (all
# components are required if nothing is specified). The following libraries are
# supported:
#
# * `asyncresolv`
# * `cgicc`
# * `clntsh` (OCI)
# * `config`
# * `dim`
# * `jansson`
# * `log4cplus`
# * `logudpappender`
# * `logxmlappender`
# * `mimetic`
# * `nlohmann` (JSON)
# * `occi`
# * `peer`
# * `toolbox`
# * `tstoreclient`
# * `tstoreutils`
# * `tstore`
# * `xalan-c`
# * `xcept`
# * `xdata`
# * `xdaq`
# * `xdaq2rc`
# * `xerces-c`
# * `xgi`
# * `xoap`
#
# A target of the form `xDAQ::<lib>` is created for every library. Dependencies
# are handled automatically.
#
# The following variables are set by this module:
#
# ::
#
#     xDAQ_FOUND          -- True if the required xDAQ components were found
#     xDAQ_INCLUDE_DIRS   -- xDAQ include directories
#     xDAQ_LIBRARIES      -- xDAQ component libraries to be linked
#     xDAQ_HTML_DIR       -- Location of xDAQ HTML documents
#
#     xDAQ_VERSION        -- For compatibility only, always 0.0.0
#     xDAQ_VERSION_MAJOR  -- For compatibility only, always 0
#     xDAQ_VERSION_MINOR  -- For compatibility only, always 0
#     xDAQ_VERSION_PATCH  -- For compatibility only, always 0
#
#     xDAQ_<lib>_FOUND       -- True if the component 'lib' was found
#     xDAQ_<lib>_INCLUDE_DIR -- Per-component xDAQ include directories
#     xDAQ_<lib>_LIBARRY     -- Location of the 'lib' library
#

include(CMakeParseArguments)
include(FindPackageHandleStandardArgs)

# Exported variables
set(xDAQ_FOUND TRUE)

# List of all supported libs
set(xdaq_all_libs "")

# Declares a library that can be required using the COMPONENTS argument
macro(_xdaq_library name)
    cmake_parse_arguments(ARG "HEADER_ONLY;THREADS;NO_SONAME" "HEADER" "DEPENDS" ${ARGN})

    set(xdaq_${name}_threads ${ARG_THREADS})
    set(xdaq_${name}_header ${ARG_HEADER})
    set(xdaq_${name}_header_only ${ARG_HEADER_ONLY})
    set(xdaq_${name}_depends ${ARG_DEPENDS})
    set(xdaq_${name}_nosoname ${ARG_NO_SONAME})
    list(APPEND xdaq_all_libs ${name})
endmacro()

# List all supported libs and their dependencies
_xdaq_library(asyncresolv HEADER "asyncresolv/config.h")
_xdaq_library(cgicc HEADER "cgicc/Cgicc.h")
_xdaq_library(clntsh HEADER "oci.h")
_xdaq_library(config HEADER "config/PackageInfo.h" DEPENDS xcept NO_SONAME)
_xdaq_library(dim HEADER "dim/dim.hxx" NO_SONAME)
_xdaq_library(jansson HEADER "jansson.h")
_xdaq_library(log4cplus HEADER "log4cplus/config.hxx")
_xdaq_library(logudpappender HEADER "log4cplus/log4judpappender.h"
                             DEPENDS config log4cplus NO_SONAME)
_xdaq_library(logxmlappender HEADER "log/xmlappender/version.h"
                             DEPENDS config log4cplus NO_SONAME)
_xdaq_library(mimetic HEADER "mimetic/version.h")
_xdaq_library(nlohmann HEADER "nlohmann/json.hpp" HEADER_ONLY)
_xdaq_library(occi HEADER "occi.h" DEPENDS clntsh)
_xdaq_library(peer HEADER "pt/version.h" DEPENDS config toolbox xcept xoap
                   THREADS NO_SONAME)
_xdaq_library(toolbox HEADER "toolbox/version.h"
                      DEPENDS asyncresolv cgicc log4cplus THREADS NO_SONAME)
_xdaq_library(tstoreclient HEADER "tstore/client/version.h" NO_SONAME)
_xdaq_library(tstoreutils HEADER "tstore/utils/version.h" DEPENDS occi NO_SONAME)
_xdaq_library(tstore HEADER "tstore/version.h"
                     DEPENDS tstoreclient tstoreutils xalan-c
                     NO_SONAME)
_xdaq_library(xalan-c HEADER "xalanc/Include/XalanVersion.hpp")
_xdaq_library(xcept HEADER "xcept/version.h" DEPENDS config toolbox NO_SONAME)
_xdaq_library(xdata HEADER "xdata/version.h"
                    DEPENDS config mimetic toolbox xcept xerces-c xoap THREADS
                    NO_SONAME)
_xdaq_library(xdaq HEADER "xdaq/version.h"
                   DEPENDS config log4cplus logudpappender logxmlappender peer
                           toolbox xcept xdata xerces-c xgi xoap
                   NO_SONAME)
_xdaq_library(xdaq2rc HEADER "xdaq2rc/version.h"
                      DEPENDS config log4cplus toolbox xdaq xdata xerces-c xoap
                      THREADS NO_SONAME)
_xdaq_library(xerces-c HEADER "xercesc/util/XercesVersion.hpp")
_xdaq_library(xgi HEADER "xgi/version.h"
                  DEPENDS cgicc config toolbox xcept xerces-c THREADS NO_SONAME)
_xdaq_library(xoap HEADER "xoap/version.h"
                   DEPENDS config toolbox xcept xerces-c
                   NO_SONAME)

# Build recursive dep lists
macro(_xdaq_build_recursive_depends lib)
    if(NOT DEFINED xdaq_${lib}_recursive_depends) # Prevent infinite recursion
        set(xdaq_${lib}_recursive_depends "${xdaq_${lib}_depends}")
        foreach(dep ${xdaq_${lib}_depends})
            _xdaq_build_recursive_depends(${dep})
            foreach(recdep ${xdaq_${dep}_recursive_depends})
                list(APPEND xdaq_${lib}_recursive_depends ${recdep})
            endforeach()
        endforeach()
        list(REMOVE_DUPLICATES xdaq_${lib}_recursive_depends)
    endif()
endmacro()

foreach(lib ${xdaq_all_libs})
    _xdaq_build_recursive_depends(${lib})
endforeach()

# If the list of libs isn't specified, assume all of them are needed
if(NOT DEFINED xDAQ_FIND_COMPONENTS)
    set(xDAQ_FIND_COMPONENTS "${xdaq_all_libs}")
endif()

# Check that all requested libs are known
foreach(lib ${xDAQ_FIND_COMPONENTS})
    list(FIND xdaq_all_libs ${lib} found)
    if(found EQUAL -1)
        set(xDAQ_FOUND FALSE)
        list(REMOVE_ITEM xDAQ_FIND_COMPONENTS ${lib})

        # Notify user
        set(msg_type STATUS)
        if(xDAQ_FIND_REQUIRED)
            set(msg_type SEND_ERROR)
        endif()
        if(NOT xDAQ_FIND_QUIETLY)
            message(${msg_type} "Unknown xDAQ library ${lib} was requested. This is probably due to a programming error.")
        endif()
        unset(msg_type)
    endif()
    unset(found)
endforeach()

# Build a list of all requested libraries with dependencies included
set(xdaq_requested_libs ${xDAQ_FIND_COMPONENTS})
foreach(lib ${xDAQ_FIND_COMPONENTS})
    list(APPEND xdaq_requested_libs ${xdaq_${lib}_recursive_depends})
endforeach()
list(REMOVE_DUPLICATES xdaq_requested_libs)

# Turn the list of required libs into a list of required variables
foreach(lib ${xdaq_requested_libs})
    # Headers
    list(APPEND xdaq_required_variables xDAQ_${lib}_INCLUDE_DIR)

    # Library
    if(NOT xdaq_${lib}_header_only)
        list(APPEND xdaq_required_variables xDAQ_${lib}_LIBRARY)
    else()
        message(STATUS "xDAQ::${lib} is header only")
    endif()

    # Threads
    if(xdaq_${lib}_threads)
        list(APPEND xdaq_required_variables Threads_FOUND)
    endif()
endforeach()
list(REMOVE_DUPLICATES xdaq_required_variables)

# Are threads required?
foreach(lib ${xdaq_requested_libs})
    if(xdaq_${lib}_threads)
        set(xdaq_need_threads TRUE)
        break()
    endif()
endforeach()

# If so, find threads
if(xdaq_need_threads)
    find_package(Threads QUIET)
endif()

# Creates an imported target for the given lib
macro(_xdaq_import_lib name)

    # Do nothing if already found
    if(NOT TARGET xDAQ::${name})

        if(NOT xdaq_${name}_header_only)
            # Try to find the library
            find_library(
                xDAQ_${name}_LIBRARY
                ${name}
                NO_CMAKE_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH
                HINTS ENV XDAQ_ROOT
                PATHS /opt/xdaq/
                PATH_SUFFIXES lib lib64
                DOC "Path of xDAQ library ${name}")

            mark_as_advanced(xDAQ_${name}_LIBRARY)
        endif()

        # Try to find the headers
        find_path(
            xDAQ_${name}_INCLUDE_DIR
            ${xdaq_${name}_header}
            NO_CMAKE_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH
            HINTS ENV XDAQ_ROOT
            PATHS /opt/xdaq/
            PATH_SUFFIXES include
            DOC "xDAQ include directory")

        mark_as_advanced(xDAQ_${name}_INCLUDE_DIR)

        if(xdaq_${name}_header_only AND xDAQ_${name}_INCLUDE_DIR)
            # Found!
            set(xDAQ_${name}_FOUND TRUE)

            # Create the target
            add_library(xDAQ::${name} INTERFACE IMPORTED)

            # Set include path
            set_target_properties(
                xDAQ::${name}
                PROPERTIES
                INTERFACE_INCLUDE_DIRECTORIES
                "${xDAQ_${name}_INCLUDE_DIR};${xDAQ_${name}_INCLUDE_DIR}/linux"
                INTERFACE_SYSTEM_INCLUDE_DIRECTORIES
                "${xDAQ_${name}_INCLUDE_DIR};${xDAQ_${name}_INCLUDE_DIR}/linux")
        elseif(xDAQ_${name}_LIBRARY AND xDAQ_${name}_INCLUDE_DIR)
            # Found!
            set(xDAQ_${name}_FOUND TRUE)

            # Create the target
            add_library(xDAQ::${name} SHARED IMPORTED)

            # Set location
            set_property(
                TARGET xDAQ::${name}
                PROPERTY IMPORTED_LOCATION
                ${xDAQ_${name}_LIBRARY})

            # Handle NO_SONAME
            if(xdaq_${name}_nosoname)
                set_property(TARGET xDAQ::${name} PROPERTY IMPORTED_NO_SONAME TRUE)
            endif()

            # Set include path
            set_target_properties(
                xDAQ::${name}
                PROPERTIES
                INTERFACE_INCLUDE_DIRECTORIES
                "${xDAQ_${name}_INCLUDE_DIR};${xDAQ_${name}_INCLUDE_DIR}/linux"
                INTERFACE_SYSTEM_INCLUDE_DIRECTORIES
                "${xDAQ_${name}_INCLUDE_DIR};${xDAQ_${name}_INCLUDE_DIR}/linux")

            # Dependencies aren't written into .so as they should be, so we need to
            # link explicitely
            foreach(dep ${xdaq_${name}_depends})
                set_property(
                    TARGET xDAQ::${name}
                    APPEND PROPERTY INTERFACE_LINK_LIBRARIES
                    xDAQ::${dep})
            endforeach()

            # Some libs need threading support
            if(${xdaq_${name}_threads})
                target_link_libraries(xDAQ::${name}
                    INTERFACE Threads::Threads)
            endif()
        endif()
    endif()
endmacro()

# Import all libs
foreach(lib ${xdaq_requested_libs})
    _xdaq_import_lib(${lib})
endforeach()

# toolbox requires libuuid from the system
if(TARGET xDAQ::toolbox)
    list(APPEND xdaq_required_variables PKG_CONFIG_FOUND uuid_FOUND)
    find_package(PkgConfig)
    if (PKG_CONFIG_FOUND)
        pkg_check_modules(uuid IMPORTED_TARGET uuid)
        target_link_libraries(xDAQ::toolbox
            INTERFACE PkgConfig::uuid)
    endif()
endif()

# xdata requires libtirpc from the system
if(TARGET xDAQ::xdata)
    list(APPEND xdaq_required_variables xDAQ_EXE_PROGRAM)
    find_program(
        xDAQ_EXE_PROGRAM
        xdaq.exe
        NO_CMAKE_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH
        HINTS ENV XDAQ_ROOT
        PATHS /opt/xdaq/
        PATH_SUFFIXES bin
        DOC "xdaq.exe executable path")
    mark_as_advanced(xDAQ_EXE_PROGRAM)

    if (xDAQ_EXE_PROGRAM AND NOT DEFINED xDAQ_EXE_PROGRAM_NEED_LIBTIRPC)
        execute_process(COMMAND readelf -d "${xDAQ_EXE_PROGRAM}"
                        OUTPUT_VARIABLE xdaq_exe_program_dynamic)
        if (xdaq_exe_program_dynamic MATCHES ".*libtirpc.*")
            set(xDAQ_EXE_PROGRAM_NEED_LIBTIRPC ON CACHE BOOL "xDAQ libtirpc requirement")
        else()
            set(xDAQ_EXE_PROGRAM_NEED_LIBTIRPC OFF CACHE BOOL "xDAQ libtirpc requirement")
        endif()
        mark_as_advanced(xDAQ_EXE_PROGRAM_NEED_LIBTIRPC)
        unset(xdaq_exe_program_dynamic)
    endif()

    if (DEFINED xDAQ_EXE_PROGRAM_NEED_LIBTIRPC AND xDAQ_EXE_PROGRAM_NEED_LIBTIRPC)
        list(APPEND xdaq_required_variables PKG_CONFIG_FOUND libtirpc_FOUND)
        find_package(PkgConfig)
        if (PKG_CONFIG_FOUND)
            pkg_check_modules(libtirpc IMPORTED_TARGET libtirpc)
            target_link_libraries(xDAQ::xdata
                INTERFACE PkgConfig::libtirpc)
        endif()
    endif()
endif()

# Wrap things up
unset(xDAQ_LIBRARIES)
unset(xDAQ_INCLUDE_DIRS)

foreach(lib ${xDAQ_FIND_COMPONENTS})
    if(NOT xdaq_${lib}_header_only)
        list(APPEND xDAQ_LIBRARIES ${xDAQ_${lib}_LIBRARY})
    endif()
    list(APPEND xDAQ_INCLUDE_DIRS ${xDAQ_${lib}_INCLUDE_DIR})
endforeach()

list(REMOVE_DUPLICATES xDAQ_LIBRARIES)
list(REMOVE_DUPLICATES xDAQ_INCLUDE_DIRS)

# Set the HTML directory
foreach(dir ${xDAQ_INCLUDE_DIRS})
    get_filename_component(xDAQ_HTML_DIR "${dir}/../htdocs" ABSOLUTE CACHE)
    mark_as_advanced(xDAQ_HTML_DIR)
    if(IS_DIRECTORY "${xDAQ_HTML_DIR}")
        break()
    else()
        set(xDAQ_HTML_DIR xDAQ_HTML_DIR-NOTFOUND)
    endif()
endforeach()

# Set version information -- always 0.0.0
set(xDAQ_VERSION_MAJOR 0)
set(xDAQ_VERSION_MINOR 0)
set(xDAQ_VERSION_PATCH 0)
set(xDAQ_VERSION ${xDAQ_VERSION_MAJOR}.${xDAQ_VERSION_MINOR}.${xDAQ_VERSION_PATCH})

find_package_handle_standard_args(
    xDAQ
    REQUIRED_VARS xDAQ_INCLUDE_DIRS xDAQ_LIBRARIES xDAQ_HTML_DIR ${xdaq_required_variables}
    VERSION_VAR xDAQ_VERSION
    HANDLE_COMPONENTS)

# Cleanup
foreach(name ${xdaq_all_libs})
    unset(xdaq_${name}_threads)
    unset(xdaq_${name}_header)
    unset(xdaq_${name}_header_only)
    unset(xdaq_${name}_depends)
    unset(xdaq_${name}_nosoname)
    unset(xdaq_${name}_recursive_depends)
endforeach()

unset(xdaq_need_threads)
unset(xdaq_all_libs)
unset(xdaq_requested_libs)
unset(xdaq_required_variables)
