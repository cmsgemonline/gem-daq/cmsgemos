/// @file

#include <gem/readout/AMC13Readout.h>

#include <gem/readout/exception/Exception.h>
#include <gem/utils/soap/GEMSOAPToolBox.h>

#include <amc13/AMC13.hh>
#include <amc13/Exception.hh>
#include <uhal/log/exception.hpp>

#include <fmt/format.h>

typedef ::amc13::Exception::exBase amc13Exception;

XDAQ_INSTANTIATOR_IMPL(gem::readout::AMC13Readout);

gem::readout::AMC13Readout::AMC13Readout(xdaq::ApplicationStub* stub)
    : gem::readout::GEMReadoutApplication(stub)
{
    // Configuration parameters
    p_appInfoSpace->fireItemAvailable("fedID", &m_fed_id);
}

gem::readout::AMC13Readout::~AMC13Readout()
{
}

void gem::readout::AMC13Readout::initializeAction()
{
    CMSGEMOS_INFO("AMC13Readout::initializeAction begin");

    try {
        const auto layout_tree_address = core::layout_tree::address<core::layout_tree::amc13_node> { m_fed_id.value_ };
        const auto t1_uri = (layout_tree() % layout_tree_address).t1_uri();
        const auto t2_uri = (layout_tree() % layout_tree_address).t2_uri();
        p_amc13 = std::make_unique<::amc13::AMC13>(t1_uri, "${AMC13_ADDRESS_TABLE_PATH}/AMC13XG_T1.xml", t2_uri, "${AMC13_ADDRESS_TABLE_PATH}/AMC13XG_T2.xml");
    } catch (uhal::exception::exception const& e) {
        std::stringstream msg;
        msg << "AMC13Readout::initializeAction  failed, caught uhal::exception: "
            << e.what();
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::readout::exception::HardwareProblem, msg.str());
    } catch (std::exception const& e) {
        std::stringstream msg;
        msg << "AMC13Readout::initializeAction  failed, caught std::exception: "
            << e.what();
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::readout::exception::HardwareProblem, msg.str());
    } catch (...) {
        std::stringstream msg;
        msg << "AMC13Readout::initializeAction  failed (unknown exception)";
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::readout::exception::HardwareProblem, msg.str());
    }
    CMSGEMOS_DEBUG("AMC13Readout::initializeAction connected");

    gem::readout::GEMReadoutApplication::initializeAction();
}

bool gem::readout::AMC13Readout::readout(std::vector<uint64_t>& event)
{
    try {
        // Try to read one event from the AMC13
        size_t size;
        int rc;
        uint64_t* data = p_amc13->readEvent(size, rc);

        // No or bad data
        if (data == nullptr || size == 0 || rc != 0)
            return false;

        // We have 1 event, copy it into the event vector
        event.reserve(size);
        event.assign(data, data + size);

        return true;
    } catch (amc13Exception const& e) {
        std::stringstream msg;
        msg << "AMC13Readout::readout error " << e.what();
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::readout::exception::ReadoutProblem, msg.str());
    } catch (std::exception const& e) {
        std::stringstream msg;
        msg << "AMC13Readout::readout error" << e.what();
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::readout::exception::ReadoutProblem, msg.str());
    } catch (...) {
        std::stringstream msg;
        msg << "AMC13Readout::readout error (unknown exception)";
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::readout::exception::ReadoutProblem, msg.str());
    }
}
