/// @file

#include <gem/readout/GEMReadoutApplication.h>

#include <gem/readout/GEMReadoutWebApplication.h>

#include <toolbox/Runtime.h>
#include <toolbox/mem/CommittedHeapAllocator.h>
#include <toolbox/mem/MemoryPoolFactory.h>
#include <toolbox/mem/Pool.h>

#include <fmt/format.h>

#include <filesystem>
#include <iomanip>

gem::readout::GEMReadoutApplication::GEMReadoutApplication(xdaq::ApplicationStub* stub)
    : gem::utils::GEMFSMApplication(stub)
{
    // Configuration parameters
    p_appInfoSpace->fireItemAvailable("maxEventsPerFile", &m_max_events_per_file);
    p_appInfoSpace->fireItemAvailable("outputPath", &m_outputPath);

    // Web UI
    p_gemWebInterface = new gem::readout::GEMReadoutWebApplication(this);
}

gem::readout::GEMReadoutApplication::~GEMReadoutApplication()
{
}

void gem::readout::GEMReadoutApplication::actionPerformed(xdata::Event& event)
{
    gem::utils::GEMApplication::actionPerformed(event);

    if (event.type() == "urn:xdaq-event:setDefaultValues") {
        // Replace the environment variables in the output data path
        m_outputPath = toolbox::getRuntime()->expandPathName(std::string(m_outputPath.value_)).at(0);
    }
}

void gem::readout::GEMReadoutApplication::initializeAction()
{
    CMSGEMOS_DEBUG("gem::readout::GEMReadoutApplication::initializeAction begin");
    if (!m_task) {
        m_task = std::make_shared<gem::readout::GEMReadoutTask>(this);
        m_task->activate();
    } else {
        m_cmdQueue.push(ReadoutCommands::CMD_STOP);
    }
}

void gem::readout::GEMReadoutApplication::configureAction()
{
    CMSGEMOS_DEBUG("gem::readout::GEMReadoutApplication::configureAction begin");
}

void gem::readout::GEMReadoutApplication::startAction()
{
    CMSGEMOS_DEBUG("gem::readout::GEMReadoutApplication::startAction begin");

    // Create the output directory
    m_filename_base = fmt::format(FMT_STRING("{:s}/run{:s}/"), m_outputPath.value_, runNumberToString());
    std::filesystem::create_directories(m_filename_base);

    // Make sure no old file is open
    if (m_output_file)
        m_output_file.close();

    // Reset the counters
    m_file_index = 0;
    m_eventsReadout = 0;
    m_usecUsed = 0;
    m_usecPerEvent = 0;

    m_cmdQueue.push(ReadoutCommands::CMD_START);
}

void gem::readout::GEMReadoutApplication::pauseAction()
{
    CMSGEMOS_DEBUG("gem::readout::GEMReadoutApplication::pauseAction begin");
    m_cmdQueue.push(ReadoutCommands::CMD_PAUSE);
}

void gem::readout::GEMReadoutApplication::resumeAction()
{
    CMSGEMOS_DEBUG("gem::readout::GEMReadoutApplication::resumeAction begin");
    m_cmdQueue.push(ReadoutCommands::CMD_RESUME);
}

void gem::readout::GEMReadoutApplication::stopAction()
{
    CMSGEMOS_DEBUG("gem::readout::GEMReadoutApplication::stopAction begin");
    m_cmdQueue.push(ReadoutCommands::CMD_STOP);
}

void gem::readout::GEMReadoutApplication::haltAction()
{
    CMSGEMOS_DEBUG("gem::readout::GEMReadoutApplication::haltAction begin");
    if (m_task)
        m_cmdQueue.push(ReadoutCommands::CMD_STOP);
}

int gem::readout::GEMReadoutApplication::readoutTask()
{
    std::vector<uint64_t> event;

    bool isDone(false);
    bool isRunning(false);

    while (!isDone) {
        if (!isRunning || m_cmdQueue.size() > 0) {
            int cmd = static_cast<int>(m_cmdQueue.pop());
            switch (cmd) {
            case (static_cast<int>(ReadoutCommands::CMD_PAUSE)):
                isRunning = false;
                break;
            case (static_cast<int>(ReadoutCommands::CMD_STOP)):
                isRunning = false;
                break;
            case (static_cast<int>(ReadoutCommands::CMD_START)):
                isRunning = true;
                break;
            case (static_cast<int>(ReadoutCommands::CMD_RESUME)):
                isRunning = true;
                break;
            case (static_cast<int>(ReadoutCommands::CMD_EXIT)):
                isDone = true;
                isRunning = false;
                break;
            }
        }

        if (isRunning) {
            struct timeval start, stop;
            gettimeofday(&start, 0);

            bool valid;

            try {
                valid = readout(event);
            } catch (gem::utils::exception::Exception& e) {
                std::stringstream msg;
                msg << "GEMReadoutApplication::readoutTask error "
                    << xcept::stdformat_exception_history(e);
                CMSGEMOS_ERROR(msg.str());
            } catch (xcept::Exception& e) {
                std::stringstream msg;
                msg << "GEMReadoutApplication::readoutTask error "
                    << xcept::stdformat_exception_history(e);
                CMSGEMOS_ERROR(msg.str());
            } catch (std::exception const& e) {
                std::stringstream msg;
                msg << "GEMReadoutApplication::readoutTask error "
                    << e.what();
                CMSGEMOS_ERROR(msg.str());
            } catch (...) {
                std::stringstream msg;
                msg << "GEMReadoutApplication::readoutTask error (unknown exception)";
                CMSGEMOS_ERROR(msg.str());
            }

            // We only take into consideration the valid events
            if (!valid)
                continue;

            ++m_eventsReadout;
            save_event(event);

            // Time accounting
            gettimeofday(&stop, 0);
            double deltaU = (stop.tv_sec - start.tv_sec) * 1e6 + (stop.tv_usec - start.tv_usec);
            m_usecUsed += deltaU;
            m_usecPerEvent = m_usecUsed / m_eventsReadout;
        }
    }

    return 0;
}

void gem::readout::GEMReadoutApplication::save_event(const std::vector<uint64_t>& event)
{
    // TODO: throw and catch (!) on errors

    // Close the current file if it contains more than configured number of events
    if (m_output_file.is_open() && ((m_eventsReadout - 1) / m_max_events_per_file.value_ > m_file_index)) {
        m_output_file.close();
        ++m_file_index;
    }

    // An closed file is the signal we should open a new one... make sense, no?
    if (!m_output_file.is_open()) {
        const std::string t_output_filename = fmt::format(FMT_STRING("{:s}/run{:06d}_ls0001_index{:06d}.raw"), m_filename_base, m_runNumber.value_, m_file_index);
        m_output_file.open(t_output_filename, std::ios_base::trunc | std::ios::binary);
    }

    // Write the EventInfo header
    const uint16_t version = 5;
    const uint16_t flags = 0;
    const uint32_t run_number = m_runNumber;
    const uint32_t lumisection = 1;
    const uint32_t event_number = m_eventsReadout;
    const uint32_t event_size = event.size() * sizeof(uint64_t);
    const uint32_t crc32c = 0; // FIXME: Should be computed... Boost.CRC is not available...
    m_output_file.write(reinterpret_cast<const char*>(&version), sizeof(uint16_t));
    m_output_file.write(reinterpret_cast<const char*>(&flags), sizeof(uint16_t));
    m_output_file.write(reinterpret_cast<const char*>(&run_number), sizeof(uint32_t));
    m_output_file.write(reinterpret_cast<const char*>(&lumisection), sizeof(uint32_t));
    m_output_file.write(reinterpret_cast<const char*>(&event_number), sizeof(uint32_t));
    m_output_file.write(reinterpret_cast<const char*>(&event_size), sizeof(uint32_t));
    m_output_file.write(reinterpret_cast<const char*>(&crc32c), sizeof(uint32_t));

    // Write the FED data
    m_output_file.write(reinterpret_cast<const char*>(event.data()), event.size() * sizeof(uint64_t));
}
