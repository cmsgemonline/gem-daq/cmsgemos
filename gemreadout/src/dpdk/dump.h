/// @file

#ifndef SRC_GEM_READOUT_DUMP_H
#define SRC_GEM_READOUT_DUMP_H

int consumer_dump_loop(void* args);

#endif // SRC_GEM_READOUT_DUMP_H
