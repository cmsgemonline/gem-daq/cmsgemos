/// @file

#include "stats.h"

#include "application.h"

#include <rte_cycles.h>
#include <rte_ethdev.h>
#include <rte_mempool.h>

uint32_t stats_routine(void*)
{
    for (;;) {
        if (app_data.do_quit.load(std::memory_order_relaxed))
            break;

        int i;
        struct rte_eth_stats pstats;
        uint64_t tx_pkt = 0, good_pkt = 0, miss_pkt = 0;
        queue_stats_t qstats;

        printf("\n");
        printf("-------------------------------------------------\n");

        /* Print global stats*/
        printf("\n");
        printf("Available buffers: %u / %u\n", rte_mempool_avail_count(app_data.pkt_mempool), app_config.nb_pkt_mempool_buffers);

        /* Print per port stats */
        printf("\n");
        for (i = 0; i < app_config.nb_ports; i++) {
            rte_eth_stats_get(i, &pstats);
            tx_pkt += pstats.opackets;
            good_pkt += pstats.ipackets;
            miss_pkt += pstats.imissed;
            printf("Port: %2d  Tx: %ld Rx: %ld Miss: %ld Tot: %ld Perc: %.3f%%\n", i, pstats.opackets, pstats.ipackets, pstats.imissed, pstats.ipackets + pstats.imissed, (float)pstats.imissed / (pstats.ipackets + pstats.imissed) * 100);
        }
        printf("          ---------------------------------------\n");
        printf("Tot:      Tx: %ld Rx: %ld Miss: %ld Tot: %ld Perc: %.3f%%\n", tx_pkt, good_pkt, miss_pkt, good_pkt + miss_pkt, (float)miss_pkt / (good_pkt + miss_pkt) * 100);

        /* Print per queue stats */
        printf("\n");
        for (i = 0; i < app_config.nb_queues; i++) {
            qstats = queue_stats[i];
            printf("Queue: %2d Rx: %ld Enq: %ld EnqFail: %ld Perc: %.3f%%\n", i, qstats.rx_pkts, qstats.enqueued_pkts, qstats.enqueued_failed_pkts, (float)qstats.enqueued_failed_pkts / (qstats.enqueued_pkts + qstats.enqueued_failed_pkts) * 100);
        }

        printf("\n");
        printf("-------------------------------------------------\n");

        rte_delay_us_sleep(1000000);
    }

    return 0;
}
