/// @file

#include "send_addresses.h"

#include "application.h"

#include <rte_cycles.h>
#include <rte_ethdev.h>
#include <rte_mbuf.h>
#include <rte_version.h>

uint32_t send_addresses_routine(void*)
{
    struct rte_mempool* tx_mempool = rte_pktmbuf_pool_create("tx-mempool", GEM_MAX_QUEUES * app_config.nb_tx_queue_desc, 0 /* no cache */, 0 /* no private data */, 60 + RTE_PKTMBUF_HEADROOM, SOCKET_ID_ANY);

    for (;;) {
        if (app_data.do_quit.load(std::memory_order_relaxed))
            break;

        for (queue_data_t* iqueue = queues; iqueue->interface_name[0] != '\0'; ++iqueue) {
            /* Forge a new, empty, packet to update the switches FIB
             *
             * Destination - Broadcast MAC address (ff:ff:ff:ff:ff:ff)
             * Source      - Local queue MAC address
             * EtherType   - Experimental 2 (0x88b6)
             */
            struct rte_mbuf* pkt = rte_pktmbuf_alloc(tx_mempool);
            if (pkt == nullptr) {
                printf("Warning: could not get a buffer from the tx-mempool\n");
            }

            struct rte_ether_hdr* eth = rte_pktmbuf_mtod(pkt, struct rte_ether_hdr*);
#if RTE_VERSION < RTE_VERSION_NUM(21, 11, 0, 0)
            memset(eth->d_addr.addr_bytes, 0xff, RTE_ETHER_ADDR_LEN);
            memcpy(eth->s_addr.addr_bytes, iqueue->queue_address.addr_bytes, RTE_ETHER_ADDR_LEN);
#else
            memset(eth->dst_addr.addr_bytes, 0xff, RTE_ETHER_ADDR_LEN);
            memcpy(eth->src_addr.addr_bytes, iqueue->queue_address.addr_bytes, RTE_ETHER_ADDR_LEN);
#endif

            eth->ether_type = RTE_BE16(0x88b6);

            pkt->pkt_len = 60; // Smallest valid Ethernet packet (60 bytes)
            pkt->data_len = 60;

            /* Push the packet to the TX queue */
            rte_eth_tx_burst(iqueue->port_id, 0, &pkt, 1);
        }

        rte_delay_us_sleep(1000000);
    }

    return 0;
}
