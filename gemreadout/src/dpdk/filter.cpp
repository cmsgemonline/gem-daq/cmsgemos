/// @file

#include "filter.h"

#include "utils.h"

#include <rte_flow.h>

#include <stdint.h>

#define MAX_PATTERN_NUM 2
#define MAX_ACTION_NUM 2

struct rte_flow* add_filter(uint16_t port_id, uint16_t rx_queue, struct rte_ether_addr* address, struct rte_flow_error* error)
{
    int res;

    /*
     * Rule attribute
     * Only ingress packets will be checked.
     */
    struct rte_flow_attr attr;
    memset(&attr, 0, sizeof(struct rte_flow_attr));
    attr.ingress = 1;

    /*
     * Action sequence
     * One action only, move the packet to queue.
     */
    struct rte_flow_action action[MAX_ACTION_NUM];
    memset(action, 0, sizeof(action));

    struct rte_flow_action_queue queue = { .index = rx_queue };
    action[0].type = RTE_FLOW_ACTION_TYPE_QUEUE;
    action[0].conf = &queue;

    action[1].type = RTE_FLOW_ACTION_TYPE_END;

    /*
     * Pattern sequence
     */
    struct rte_flow_item pattern[MAX_PATTERN_NUM];
    memset(pattern, 0, sizeof(pattern));

    pattern[0].type = RTE_FLOW_ITEM_TYPE_ETH;

    struct rte_flow_item_eth eth_spec;
    memset(&eth_spec, 0, sizeof(struct rte_flow_item_eth));
    // Only destination address supported with mlx4
    memcpy(eth_spec.dst.addr_bytes, address, RTE_ETHER_ADDR_LEN);

    struct rte_flow_item_eth eth_mask;
    memset(&eth_mask, 0, sizeof(struct rte_flow_item_eth));
    // Only destination address supported with mlx4
    memcpy(eth_mask.dst.addr_bytes, "\xff\xff\xff\xff\xff\xff", RTE_ETHER_ADDR_LEN);

    pattern[0].spec = &eth_spec;
    pattern[0].mask = &eth_mask;

    // The final level must be always type end
    pattern[1].type = RTE_FLOW_ITEM_TYPE_END;

    // Validate the rule and create it
    struct rte_flow* flow = NULL;
    struct rte_flow_error err;
    res = rte_flow_validate(port_id, &attr, pattern, action, &err);
    if (!res)
        flow = rte_flow_create(port_id, &attr, pattern, action, error);
    else
        FATAL_ERROR("Cannot validate flow rule with %s", err.message);

    return flow;
}
