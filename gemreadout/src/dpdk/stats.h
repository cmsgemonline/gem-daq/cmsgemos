/// @file

#ifndef SRC_GEM_READOUT_STATS_H
#define SRC_GEM_READOUT_STATS_H

#include <stdint.h>

uint32_t stats_routine(void*);

#endif // SRC_GEM_READOUT_STATS_H
