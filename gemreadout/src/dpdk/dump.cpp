/// @file

#include "dump.h"

#include "application.h"
#include "utils.h"

#include <rte_malloc.h>
#include <rte_mbuf.h>
#include <rte_ring.h>
#include <zstd.h>

#include <stdexcept>
#include <string.h> // strerror_r

struct EventHeader {
    uint16_t version;
    uint16_t flags;
    uint32_t run_number;
    uint32_t lumisection;
    uint32_t event_number;
    uint32_t event_size;
    uint32_t crc32c;
};

/// @brief Converts a libc error number into a describing string
static std::string get_errno_string(int errnum)
{
    char t_errbuf[512] = { 0 };
    const char* r_errbuf = strerror_r(errnum, t_errbuf, 511);
#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && !_GNU_SOURCE
    return t_errbuf;
#else
    return r_errbuf;
#endif
}

/// @brief Class used to write events to a ZSTD-compressed RAW file
class raw_file_writer {
public:
    /// @brief Contructor
    raw_file_writer()
    {
        if (app_config.enable_compression) {
            // Prepare the ZSTD structures
            m_zcontext = ZSTD_createCCtx();
            ZSTD_CCtx_setParameter(m_zcontext, ZSTD_c_compressionLevel, app_config.compression_level);
            ZSTD_CCtx_setParameter(m_zcontext, ZSTD_c_checksumFlag, 1);

            m_buffOutSize = ZSTD_CStreamOutSize();
            m_buffOut = rte_malloc(nullptr, m_buffOutSize, 0);
        }
    }

    /// @brief Destructor
    virtual ~raw_file_writer()
    {
        this->close();

        if (m_zcontext) {
            rte_free(m_buffOut);
            ZSTD_freeCCtx(m_zcontext);
        }
    }

    /// @brief Returns whether or not a file is currently open
    operator bool()
    {
        return (m_file != nullptr);
    }

    /// @brief Open a new output file, closing any previously open file if needed
    ///
    /// @param @c filename File to write the events to
    void open(const std::string& filename)
    {
        this->close();
        m_file = fopen(filename.data(), "wb");
        if (m_file == nullptr)
            throw std::runtime_error("Could not open file '" + filename + "' for writing: " + get_errno_string(errno));
    }

    /// @brief Write data to file
    ///
    /// @param @c data Buffer containing the data to write
    /// @param @c size Number of bytes to write
    ///
    /// @throw @c std::runtime_error when there is no file open
    void write(const void* data, const size_t size)
    {
        if (unlikely(!m_file))
            throw std::runtime_error("Trying to save data while no file is open");

        if (m_zcontext) {
            ZSTD_inBuffer input = { data, size, 0 };
            bool finished;
            do {
                ZSTD_outBuffer output = { m_buffOut, m_buffOutSize, 0 };
                size_t const remaining = ZSTD_compressStream2(m_zcontext, &output, &input, ZSTD_e_continue);
                fwrite(m_buffOut, 1, output.pos, m_file);
                finished = (input.pos == input.size);
            } while (!finished);
        } else {
            fwrite(data, 1, size, m_file);
        }
    }

    /// @brief Close the currently open file, if any
    void close()
    {
        if (!m_file)
            return;

        if (m_zcontext) {
            ZSTD_inBuffer input = { nullptr, 0, 0 };
            bool finished;
            do {
                ZSTD_outBuffer output = { m_buffOut, m_buffOutSize, 0 };
                size_t const remaining = ZSTD_compressStream2(m_zcontext, &output, &input, ZSTD_e_end);
                fwrite(m_buffOut, 1, output.pos, m_file);
                finished = (remaining == 0);
            } while (!finished);
        }

        fclose(m_file);
        m_file = nullptr;
    }

protected:
    std::FILE* m_file = nullptr;

    // For ZSTD compression
    ZSTD_CCtx* m_zcontext = nullptr;
    size_t m_buffOutSize = 0;
    void* m_buffOut = nullptr;
};

int consumer_dump_loop(void* args)
{
    queue_data_t* const queue = (queue_data_t*)args;

    struct rte_mbuf* m;
    int ret;

    /* Prepare the output stream */
    raw_file_writer raw_file {};
    unsigned file_index = 0;
    char filename[1000];

    /* Infinite loop for consumer thread */
    for (;;) {
        if (unlikely(app_data.do_quit.load(std::memory_order_relaxed)))
            break;

        /* Dequeue packet */
        ret = rte_ring_dequeue(queue->ring, (void**)&m);

        /* Continue polling if no packet available */
        if (unlikely(ret != 0))
            continue;

        /* Parse the local DAQ packet */
        uint16_t ldaq_header = rte_le_to_cpu_16(*rte_pktmbuf_mtod_offset(m, uint16_t*, 14));
        bool first = ldaq_header & 0x8000;
        uint8_t pkt_number = (ldaq_header >> 8) & 0xf;
        uint8_t evt_number = ldaq_header & 0xf;

        uint16_t ldaq_trailer = rte_le_to_cpu_16(*rte_pktmbuf_mtod_offset(m, uint16_t*, rte_pktmbuf_data_len(m) - 2));
        bool last = ldaq_trailer & 0x1;
        uint16_t pkt_size = ((ldaq_trailer >> 4) & 0xfff) * 2;

        /* Debugging */
        /* printf("-----\n"); */
        /* const struct rte_ether_hdr* eth = rte_pktmbuf_mtod(m, struct rte_ether_hdr*); */
        /* printf("DADDR - " RTE_ETHER_ADDR_FMT "\n", RTE_ETHER_ADDR_BYTES(&eth->d_addr)); */
        /* printf("SADDR - " RTE_ETHER_ADDR_FMT "\n", RTE_ETHER_ADDR_BYTES(&eth->s_addr)); */
        /* printf("ETYPE - 0x%04hx\n",rte_be_to_cpu_16(eth->ether_type)); */
        /* printf("LDHDR - 0x%04hx\n", ldaq_header); */
        /* printf("  EvtNumber - %u\n", evt_number); */
        /* printf("  PktNumber - %u\n", pkt_number); */
        /* printf("  First - %u\n", first); */
        /* for (size_t j = 16; j < rte_pktmbuf_pkt_len(m) - 2; j += 8) */
        /*     printf("0x%016lx\n", *rte_pktmbuf_mtod_offset(m, uint64_t*, j)); */
        /* printf("LDTRL - 0x%04hx\n", ldaq_trailer); */
        /* printf("  PktSize - %u\n", pkt_size); */
        /* printf("  Last - %u\n", last); */
        /* printf("-----\n"); */

        /* Sanity checks */
        if (pkt_size > rte_pktmbuf_pkt_len(m) - 14 - 2 - 2) {
            printf("Error - Payload size larger than maximum payload size based on Ethernet frame!\n");
            rte_pktmbuf_free((struct rte_mbuf*)m);
            continue;
        }

        if (first != 1 && last != 1) {
            printf("Warning - Split events unsupported, skipping...\n");
            rte_pktmbuf_free((struct rte_mbuf*)m);
            continue;
        }

        queue->event_number++;

        /* Make sure there is an open output file */
        if (unlikely(!raw_file)) {
            snprintf(filename, 1000, "%s-" RTE_ETHER_ADDR_FMT "-index%06u.raw%s",
                app_config.output_prefix.data(),
                RTE_ETHER_ADDR_BYTES(&queue->source_address),
                file_index,
                app_config.enable_compression ? ".zst" : "");
            raw_file.open(filename);
        }

        /* Write the event */
        {
            struct EventHeader hdr;
            hdr.version = 5;
            hdr.flags = 0;
            hdr.run_number = 0;
            hdr.lumisection = 1;
            hdr.event_number = queue->event_number;
            hdr.event_size = pkt_size;
            hdr.crc32c = 0;

            raw_file.write(&hdr, sizeof(hdr));
            raw_file.write(rte_pktmbuf_mtod_offset(m, void*, 14 + 2), pkt_size);
        }

        /* Free the buffer */
        rte_pktmbuf_free((struct rte_mbuf*)m);

        /* Handle file splitting */
        if (unlikely(app_config.max_events_per_file && (queue->event_number >= (file_index + 1) * app_config.max_events_per_file))) {
            raw_file.close();
            file_index++;
        }
    }

    /* Close the output stream */
    raw_file.close();

    return 0;
}
