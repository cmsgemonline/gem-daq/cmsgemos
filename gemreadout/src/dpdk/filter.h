/// @file

#ifndef SRC_GEM_READOUT_FILTER_H
#define SRC_GEM_READOUT_FILTER_H

#include <rte_ether.h>
#include <rte_flow.h>

#include <stdint.h>

struct rte_flow* add_filter(uint16_t port_id, uint16_t rx_queue, struct rte_ether_addr* address, struct rte_flow_error* error);

#endif // SRC_GEM_READOUT_FILTER_H
