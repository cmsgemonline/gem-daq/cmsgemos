/// @file

#ifndef SRC_GEM_READOUT_UTILS_H
#define SRC_GEM_READOUT_UTILS_H

#include <rte_common.h>
#include <stdlib.h>

// Helpers
#define FATAL_ERROR(fmt, args...) rte_exit(EXIT_FAILURE, fmt "\n", ##args)

// Taken from DPDK 21.11 sources
#define RTE_ETHER_ADDR_FMT "%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx"
#define RTE_ETHER_ADDR_BYTES(mac_addrs) ((mac_addrs)->addr_bytes[0]), \
                                        ((mac_addrs)->addr_bytes[1]), \
                                        ((mac_addrs)->addr_bytes[2]), \
                                        ((mac_addrs)->addr_bytes[3]), \
                                        ((mac_addrs)->addr_bytes[4]), \
                                        ((mac_addrs)->addr_bytes[5])
#define RTE_ETHER_ADDR_BYTES_PTR(mac_addrs) ((mac_addrs)->addr_bytes + 0), \
                                            ((mac_addrs)->addr_bytes + 1), \
                                            ((mac_addrs)->addr_bytes + 2), \
                                            ((mac_addrs)->addr_bytes + 3), \
                                            ((mac_addrs)->addr_bytes + 4), \
                                            ((mac_addrs)->addr_bytes + 5)

#endif // SRC_GEM_READOUT_UTILS_H
