/// @file

#include "application.h"
#include "dump.h"
#include "filter.h"
#include "rx_pkt.h"
#include "send_addresses.h"
#include "stats.h"
#include "utils.h"

#include <rte_eal.h>
#include <rte_errno.h>
#include <rte_ethdev.h>
#include <rte_flow.h>
#include <rte_lcore.h>
#include <rte_mbuf.h>
#include <rte_version.h>

#include <net/if.h>
#include <signal.h>
#include <sys/time.h>
#include <unistd.h>
#include <vector>

/* Pre 23.11 compatibility shim */
#if RTE_VERSION < RTE_VERSION_NUM(23, 11, 0, 0)
typedef pthread_t shim_rte_thread_t;
#define shim_rte_thread_create_control(t, name, func, args) rte_ctrl_thread_create(t, name, nullptr, (void* (*)(void*))func, args)
#define shim_rte_thread_join(t, rv) pthread_join(t, rv)
#else
typedef rte_thread_t shim_rte_thread_t;
#define shim_rte_thread_create_control(t, name, func, args) rte_thread_create_control(t, name, func, args)
#define shim_rte_thread_join(r, rv) rte_thread_join(t, rv)
#endif

/* Functions */
uint16_t init_port(uint16_t port_id);
void signal_handler(int signo);

/* Main function */
int main(int argc, char** argv)
{
    int ret;
    struct rte_flow_error error;
    std::vector<shim_rte_thread_t> control_threads;

    /* Register the signal handler */
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);

    /* Parse command line */
    parse_cmdline(argc, argv);

    /* Initialize DPDK */
    std::vector<char*> dpdk_fake_args;
    dpdk_fake_args.push_back(argv[0]); // Insert the command name
    for (auto& dpdk_arg : app_config.dpdk_args)
        dpdk_fake_args.push_back(dpdk_arg.data()); // Add all args meant for DPDK
    ret = rte_eal_init(dpdk_fake_args.size(), dpdk_fake_args.data());
    if (ret < 0)
        FATAL_ERROR("Cannot init EAL\n");

    /* Get number of ethernet devices */
    app_config.nb_ports = rte_eth_dev_count_avail();
    if (app_config.nb_ports <= 0)
        FATAL_ERROR("Cannot find ETH devices\n");

    /* Create a mempool with per-core cache, initializing every element for be used as mbuf, and allocating on the current NUMA node */
    app_data.pkt_mempool = rte_mempool_create("pkt-mempool", app_config.nb_pkt_mempool_buffers, pkt_mempool_buffers_size, app_config.nb_pkt_mempool_buffers_cache, sizeof(struct rte_pktmbuf_pool_private), rte_pktmbuf_pool_init, NULL, rte_pktmbuf_init, NULL, rte_socket_id(), 0);
    if (app_data.pkt_mempool == nullptr)
        FATAL_ERROR("Cannot create pkt-mempool\n");

    /* Initialize all ports */
    for (uint16_t iport = 0; iport < app_config.nb_ports; ++iport)
        app_config.nb_queues += init_port(iport);

    /* Check the number of lcores */
    ret = rte_lcore_count();
    if (ret < (app_config.nb_queues + app_config.nb_ports))
        FATAL_ERROR("Please specify at least one lcore for each queue and each port\n");

    /* Start stats */
    for (int i = 0; i < app_config.nb_ports; i++)
        rte_eth_stats_reset(i);

    /* Start the control plane threads */
    control_threads.push_back(shim_rte_thread_t {});
    ret = shim_rte_thread_create_control(&control_threads.back(), "stats", &stats_routine, nullptr);
    if (ret < 0)
        FATAL_ERROR("Cannot create 'stats' control thread\n");

    control_threads.push_back(shim_rte_thread_t {});
    ret = shim_rte_thread_create_control(&control_threads.back(), "send-addresses", &send_addresses_routine, nullptr);
    if (ret < 0)
        FATAL_ERROR("Cannot create 'send-addresses' control thread\n");

    /* Start the data plane threads */
    unsigned int lcore_id = rte_get_next_lcore(-1, 1, 0);

    uint16_t iqueue = 0;
    for (; iqueue < app_config.nb_queues && lcore_id < RTE_MAX_LCORE; iqueue++) {
        ret = rte_eal_remote_launch(consumer_dump_loop, &queues[iqueue], lcore_id);
        if (ret != 0)
            FATAL_ERROR("Cannot start consumer thread\n");

        lcore_id = rte_get_next_lcore(lcore_id, 1, 0);
    }

    uint16_t iport = 0;
    for (; iport < (app_config.nb_ports - 1) && lcore_id < RTE_MAX_LCORE; iport++) {
        ret = rte_eal_remote_launch(producer_read_loop, &ports[iport], lcore_id);
        if (ret != 0)
            FATAL_ERROR("Cannot start producer thread\n");

        lcore_id = rte_get_next_lcore(lcore_id, 1, 0);
    }

    // Always start a producer thread on the main core
    producer_read_loop(&ports[iport]);

    /* Cleaning up... */
    rte_eal_mp_wait_lcore();
    for (auto& t : control_threads)
        shim_rte_thread_join(t, nullptr);

    return 0;
}

uint16_t init_port(uint16_t port_id)
{
    int ret;

    /* Retrieve device information */
    struct rte_eth_dev_info dev_info;
    ret = rte_eth_dev_info_get(port_id, &dev_info);
    if (ret < 0)
        FATAL_ERROR("Impossible to retrieve the device information");

    char if_name[IFNAMSIZ];
    if_indextoname(dev_info.if_index, if_name);

    /* Find the queues connected to this port */
    uint16_t number_queues = 0;
    uint16_t queue_gid = 0;
    for (queue_data_t* iqueue = queues; iqueue->interface_name[0] != '\0'; ++iqueue, ++queue_gid) {
        if (strcmp(iqueue->interface_name, if_name))
            continue;

        /* Finalize the queue initialization */
        iqueue->port_id = port_id;
        iqueue->queue_id = number_queues;

        char ring_name[256];
        snprintf(ring_name, 256, "ring-port%hu-queue%hu", iqueue->port_id, iqueue->queue_id);
        iqueue->ring = rte_ring_create(ring_name, app_config.nb_elem_queue_ring, rte_socket_id(), RING_F_SP_ENQ | RING_F_SC_DEQ);
        if (iqueue->ring == NULL)
            FATAL_ERROR("Cannot create ring %s", ring_name);

        ports[port_id].queues_gid[number_queues] = queue_gid;
        ++number_queues;
    }
    ports[port_id].port_id = port_id;
    ports[port_id].nb_queues = number_queues;

    /* Use isolated mode */
    ret = rte_flow_isolate(port_id, 1 /* set */, NULL);
    if (ret < 0)
        FATAL_ERROR("Cannot enable isoalted mode");

    /* Configure the device */
    struct rte_eth_conf port_conf = {
#if RTE_VERSION < RTE_VERSION_NUM(21, 11, 0, 0)
        .rxmode = {
            .mq_mode = ETH_MQ_RX_NONE,
            .max_rx_pkt_len = 9000,
            .split_hdr_size = 0,
            .offloads = (DEV_RX_OFFLOAD_JUMBO_FRAME),
        },
        .txmode = {
            .mq_mode = ETH_MQ_TX_NONE,
        },
#else
        .rxmode = {
            .mq_mode = RTE_ETH_MQ_RX_NONE,
            .mtu = 9000,
        },
        .txmode = { .mq_mode = RTE_ETH_MQ_TX_NONE },
#endif
    };

    ret = rte_eth_dev_configure(port_id, number_queues, 1, &port_conf);
    if (ret < 0)
        FATAL_ERROR("Error configuring the port");

    /* Enable Jumbo frames */
    ret = rte_eth_dev_set_mtu(port_id, 9000);
    if (ret < 0)
        FATAL_ERROR("Impossible to set the MTU");

    /* Configure all queues */
    struct rte_eth_rxconf rxq_conf;
    rxq_conf = dev_info.default_rxconf;
#if RTE_VERSION < RTE_VERSION_NUM(21, 11, 0, 0)
    rxq_conf.offloads = port_conf.rxmode.offloads;
#endif
    for (uint16_t iqueue = 0; iqueue < number_queues; ++iqueue) {
        ret = rte_eth_rx_queue_setup(port_id, iqueue, app_config.nb_rx_queue_desc, rte_socket_id(), &rxq_conf, app_data.pkt_mempool);
        if (ret < 0)
            FATAL_ERROR("Error configuring RX queue %hu", iqueue);

        /* ret = rte_eth_dev_set_rx_queue_stats_mapping 	(i, 0, 0); */
        /* if (ret < 0) FATAL_ERROR("Error configuring receiving queue stats\n"); */
    }

    struct rte_eth_txconf txq_conf;
    txq_conf = dev_info.default_txconf;
    ret = rte_eth_tx_queue_setup(port_id, 0, app_config.nb_tx_queue_desc, rte_socket_id(), &txq_conf);
    if (ret < 0)
        FATAL_ERROR("Error configuring TX queue %hu", (uint16_t)(0));

    /* Start the device */
    ret = rte_eth_dev_start(port_id);
    if (ret < 0)
        FATAL_ERROR("Cannot start port\n");

    /* Add the filters */
    for (queue_data_t* iqueue = queues; iqueue->interface_name[0] != '\0'; ++iqueue) {
        if (strcmp(iqueue->interface_name, if_name))
            continue;

        add_filter(port_id, iqueue->queue_id, &iqueue->queue_address, NULL);
    }

    /* Print link status */
    /* struct rte_eth_link link; */
    /* rte_eth_link_get_nowait(i, &link); */
    /* if (link.link_status) */
    /*     printf("\tPort %d Link Up - speed %u Mbps - %s\n", (uint8_t)i, (unsigned)link.link_speed, (link.link_duplex == ETH_LINK_FULL_DUPLEX) ? ("full-duplex") : ("half-duplex\n")); */
    /* else */
    /*     printf("\tPort %d Link Down\n", (uint8_t)i); */

    return number_queues;
}

/* Signal handling function */
void signal_handler(int signo)
{
    if ((signo == SIGINT) || (signo == SIGTERM)) {
        /* Signal the shutdown */
        static_assert(decltype(app_data.do_quit)::is_always_lock_free, "The app_data.do_quit atomic must be lock free!");
        app_data.do_quit.store(true, std::memory_order_relaxed);
    }
}
