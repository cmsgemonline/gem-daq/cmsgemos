/// @file

#ifndef SRC_GEM_READOUT_APPLICATION_H
#define SRC_GEM_READOUT_APPLICATION_H

#include <net/if.h>
#include <rte_ether.h>

#include <atomic>
#include <string>
#include <vector>

// Forward declarations
struct rte_mempool;
struct rte_ring;

/// @brief Define the maximum number of queues the application can support
#define GEM_MAX_QUEUES 32

/// @brief Size of the packet buffers
///
/// @warning Must be enough to hold a complete Ethernet frame (segmented mbuf not supported)
constexpr unsigned pkt_mempool_buffers_size = 10240; // 10 KiB

/// @brief Maximum value of @c app_config_t.nb_rx_burst_pkts
constexpr uint16_t nb_rx_burst_pkts_max = 4096;

/// @brief Structure meant to hold the application configuration parameters
struct app_config_t {
    /// @brief Stores the path prefix in which to dump the events
    std::string output_prefix /* = required option */;

    /// @brief Whether or not to enable ZSTD compression of the output file
    bool enable_compression = true;

    /// @brief Compression level to be used by ZSTD
    int compression_level /* = default in parse_cmdline */;

    /// @brief Maximum number of events to store in a single file
    ///
    /// A value of 0 disables file splitting.
    uint32_t max_events_per_file = 0;

    /// @brief Stores the number of ports used by the application
    uint16_t nb_ports /* = init routines */;

    /// @brief Stores the number of queues managed by the application
    uint16_t nb_queues /* = init routines */;

    /// @brief Number of packet buffers in the main mempool
    ///
    /// In order to always keep buffers available in the mempool, even
    /// during heavily load, a typical good value is the following:
    ///
    /// 1.1*(
    ///     nb_lcore*nb_pkt_mempool_buffers_cache +
    ///     nb_queues*(nb_rx_queue_desc+nb_rx_burst_pkts+nb_elem_queue_ring)
    /// )
    ///
    /// Please note that an undersized mempool will lead to packets losses
    /// at the NIC level instead of losses at the ring enqueuing level.
    ///
    /// Optimal size in terms of memory usage is achieved when n = (q² - 1)
    unsigned nb_pkt_mempool_buffers = 4095;

    /// @brief Size of (number of elements in) the per-core mempool cache
    unsigned nb_pkt_mempool_buffers_cache = 512;

    /// @brief Number of descriptors for each RX queue (i.e. number of packets the hardware queue)
    uint16_t nb_rx_queue_desc = 1024;

    /// @brief Maximum number of packets to retrieve from a RX queue in one burst
    uint16_t nb_rx_burst_pkts = 64;

    /// @brief Number of elements any RX queue can hold
    ///
    /// @warning Must be a power of two
    unsigned int nb_elem_queue_ring = 2048;

    /// @brief Number of descriptors for each TX queue (i.e. number of packets the hardware queue)
    ///
    /// @note This is the send queure, it can be kept small
    uint16_t nb_tx_queue_desc = 16;

    /// @brief List of arguments to be given to the DPDK EAL
    std::vector<std::string> dpdk_args;
} __rte_cache_aligned;

extern app_config_t app_config;

/// @brief Structure meant to hold all global application variables
struct app_data_t {
    /// @brief Signals that all threads should gracefully terminate
    std::atomic<bool> do_quit = false;

    /// @brief Main packet mempool
    struct rte_mempool* pkt_mempool = nullptr;
} __rte_cache_aligned;

extern app_data_t app_data;

/// @brief Stores the per-queue configuration and data
struct queue_data_t {
    char interface_name[IFNAMSIZ];
    struct rte_ether_addr queue_address;
    struct rte_ether_addr source_address;
    uint16_t port_id;
    uint16_t queue_id;
    struct rte_ring* ring;
    uint32_t event_number;
} __rte_cache_aligned;

extern struct queue_data_t queues[GEM_MAX_QUEUES];

/// @brief Store the per-queue stats
struct queue_stats_t {
    uint64_t rx_pkts = 0; ///< Number of packets received in the queue
    uint64_t enqueued_pkts = 0; ///< Number of packets enqueued into the ring
    uint64_t enqueued_failed_pkts = 0; ///< Number of packets that could not be enqueued into the ring
} __rte_cache_aligned;

extern struct queue_stats_t queue_stats[GEM_MAX_QUEUES];

/// @brief Stores the per-port configuration and data
struct port_data_t {
    uint16_t port_id;
    uint16_t nb_queues;
    uint16_t queues_gid[GEM_MAX_QUEUES];
} __rte_cache_aligned;

extern struct port_data_t ports[RTE_MAX_ETHPORTS];

/// @brief Parse the command line and fill the application structures
void parse_cmdline(int argc, char** argv);

#endif // SRC_GEM_READOUT_APPLICATION_H
