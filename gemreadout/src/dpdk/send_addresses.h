/// @file

#ifndef SRC_GEM_READOUT_SEND_ADDRESSES_H
#define SRC_GEM_READOUT_SEND_ADDRESSES_H

#include <stdint.h>

uint32_t send_addresses_routine(void*);

#endif // SRC_GEM_READOUT_SEND_ADDRESSES_H
