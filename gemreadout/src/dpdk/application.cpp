/// @file

#include "application.h"

#include <CLI/CLI.hpp>
#include <rte_common.h>
#include <zstd.h>

#include <regex>
#include <tuple>
#include <vector>

namespace /* anonymous */ {

/// @brief Parse a MAC address from a string to structured variable
///
/// @param @c dst Destination variable
/// @param @c src Source string
///
/// @return @c true is the address was parsed correctly, @c false otherwise
bool parse_mac_address(struct rte_ether_addr& dst, const std::string& src)
{
    std::regex re(R"(([[:xdigit:]]{2})([-:])([[:xdigit:]]{2})\2([[:xdigit:]]{2})\2([[:xdigit:]]{2})\2([[:xdigit:]]{2})\2([[:xdigit:]]{2}))");
    std::smatch m;

    if (!std::regex_match(src, m, re))
        return false;

    dst.addr_bytes[0] = std::stoi(m[1].str(), 0, 16);
    // Skip the m[2] capture group that matches the delimiter character
    dst.addr_bytes[1] = std::stoi(m[3].str(), 0, 16);
    dst.addr_bytes[2] = std::stoi(m[4].str(), 0, 16);
    dst.addr_bytes[3] = std::stoi(m[5].str(), 0, 16);
    dst.addr_bytes[4] = std::stoi(m[6].str(), 0, 16);
    dst.addr_bytes[5] = std::stoi(m[7].str(), 0, 16);

    return true;
}

/// @brief Ensure the validity of an Ethernet interface name
struct interface_name_validator : public CLI::Validator {
    interface_name_validator()
    {
        name_ = "Interface name";
        func_ = [](const std::string& str) {
            return str.length() > IFNAMSIZ ? "Interface name too long: " + str : "";
        };
    }
};

const static interface_name_validator valid_interface_name;

/// @brief Ensure the validity of a MAC address
struct mac_validator : public CLI::Validator {
    mac_validator()
    {
        name_ = "MAC Address";
        func_ = [](const std::string& str) {
            struct rte_ether_addr tmp;
            if (!parse_mac_address(tmp, str))
                return "Invalid MAC address: " + str;
            else
                return std::string();
        };
    }
};

const static mac_validator valid_mac;

} // anonymous namespace

app_config_t app_config;

app_data_t app_data;

queue_data_t queues[GEM_MAX_QUEUES] = {
    /* { .interface_name =, .queue_address = , .source_address = }, */

    // GE1/1 integration setup - gem904daq04
    { "p2p1", { u'\xca', u'\xfe', u'\x00', u'\x00', u'\x25', u'\xc6' }, { u'\xbe', u'\xfe', u'\x00', u'\x00', u'\x25', u'\xc6' } },

    { 0 }
};

queue_stats_t queue_stats[GEM_MAX_QUEUES];

port_data_t ports[RTE_MAX_ETHPORTS];

void parse_cmdline(int argc, char** argv)
{
    CLI::App app { "\nDPDK-based GEM local readout daemon\n" };
    app.option_defaults()->always_capture_default();

    app.add_option("--output-prefix,-o", app_config.output_prefix, "Output path prefix where to dump the events")
        ->required();
    app.add_option("--enable-compression", app_config.enable_compression, "Enables ZSTD compression of the output file");
    app.add_option("--compression-level,-l", app_config.compression_level, "ZSTD compression level")
        ->default_val(ZSTD_defaultCLevel())
        ->check(CLI::Range(ZSTD_minCLevel(), ZSTD_maxCLevel()));
    app.add_option("--max-events-per-file", app_config.max_events_per_file, "Maximum number of events to store in a single file (0 disables file splitting)");

    app.add_option("--nb-pkt-mempool-buffers", app_config.nb_pkt_mempool_buffers, "Number of packet buffers in the main mempool");
    app.add_option("--nb-pkt-mempool-buffers-cache", app_config.nb_pkt_mempool_buffers_cache, "Number of elements in the per-core mempool cache");
    app.add_option("--nb-rx-queue-desc", app_config.nb_rx_queue_desc, "Number of descriptors for each RX queue");
    app.add_option("--nb-rx-burst-pkts", app_config.nb_rx_burst_pkts, "Maximum number of packets to retrieve from a RX queue in one burst")
        ->check(CLI::Range(uint16_t(0), nb_rx_burst_pkts_max));
    app.add_option("--nb-elem-queue-ring", app_config.nb_elem_queue_ring, "RX queue ring size (must be a power of two)")
        ->check([](const std::string& str) {
            if (!rte_is_power_of_2(std::stoi(str)))
                return std::string("Ring size must be a power of 2");
            else
                return std::string();
        });
    app.add_option("--nb-tx-queue-desc", app_config.nb_tx_queue_desc, "Number of descriptor for each TX queue");

    std::vector<std::tuple<std::string, std::string, std::string>> queue_params;
    app.add_option("--queue,-q", queue_params, "Queues to be readout in the form <interface name>,<queue MAC address>,<source MAC address>")
        ->delimiter(',')
        ->check(valid_interface_name.application_index(0))
        ->check(valid_mac.application_index(1))
        ->check(valid_mac.application_index(2));

    app.add_option("dpdk-args", app_config.dpdk_args, "Arguments given to the DPDK framework");

    app.set_config("--config");

    // Parse the command line
    try {
        app.parse((argc), (argv));
    } catch (const CLI::ParseError& e) {
        if (app.exit(e))
            exit(EXIT_FAILURE);

        exit(EXIT_SUCCESS);
    }

    // Parse queue list
    if (queue_params.size() > GEM_MAX_QUEUES) {
        std::cout << "Too many queues provied (" << queue_params.size() << ") "
                  << "- maximum supported is " << GEM_MAX_QUEUES << std::endl;
        exit(EXIT_FAILURE);
    }
    for (size_t i = 0; i < queue_params.size(); i++) {
        strcpy(queues[i].interface_name, std::get<0>(queue_params[i]).data());
        parse_mac_address(queues[i].queue_address, std::get<1>(queue_params[i]));
        parse_mac_address(queues[i].source_address, std::get<2>(queue_params[i]));
    }

    std::cout << "Will use configuration:" << std::endl
              << std::endl
              << app.config_to_str(true, true) << std::endl;
}
