/// @file

#ifndef SRC_GEM_READOUT_RX_PKT_H
#define SRC_GEM_READOUT_RX_PKT_H

int producer_read_loop(void* args);

#endif // SRC_GEM_READOUT_RX_PKT_H
