/// @file

#include "rx_pkt.h"

#include "application.h"

#include <rte_ethdev.h>
#include <rte_mbuf.h>
#include <rte_ring.h>

int producer_read_loop(void* args)
{
    port_data_t* const port = (port_data_t*)args;

    int nqueue = 0;
    uint16_t queue_gid = 0;
    int nb_enq_pkts, nb_rx_pkts;
    struct rte_mbuf* pkts[nb_rx_burst_pkts_max];

    if (!port->nb_queues) {
        // Nothing to do!
        printf("Nothing to do on port %u (%u queues), terminating lcore\n", port->port_id, port->nb_queues);
        return 0;
    }

    /* Infinite loop */
    for (;;) {
        if (unlikely(app_data.do_quit.load(std::memory_order_relaxed)))
            break;

        queue_gid = port->queues_gid[nqueue];

        /* Read packets from the RX queue */
        nb_rx_pkts = rte_eth_rx_burst(port->port_id, queues[queue_gid].queue_id, pkts, app_config.nb_rx_burst_pkts);
        queue_stats[queue_gid].rx_pkts += nb_rx_pkts;

        /* Enqueue packets to the queue ring */
        nb_enq_pkts = rte_ring_enqueue_burst(queues[queue_gid].ring, (void**)pkts, nb_rx_pkts, NULL);
        queue_stats[queue_gid].enqueued_pkts += nb_enq_pkts;
        if (unlikely(nb_enq_pkts < nb_rx_pkts)) {
            rte_pktmbuf_free_bulk(&pkts[nb_enq_pkts], nb_rx_pkts - nb_enq_pkts);
            queue_stats[queue_gid].enqueued_failed_pkts += nb_rx_pkts - nb_enq_pkts;
        }

        /* Move to the next queue in a round-robin logic */
        nqueue = (nqueue + 1) % port->nb_queues;
    }

    return 0;
}
