/// @file

#include <gem/readout/GEMReadoutWebApplication.h>

#include <gem/readout/GEMReadoutApplication.h>
#include <gem/readout/exception/Exception.h>

#include <xcept/tools.h>

#include <memory>

gem::readout::GEMReadoutWebApplication::GEMReadoutWebApplication(gem::readout::GEMReadoutApplication* application)
    : gem::utils::GEMWebApplication(application)
{
    add_tab("Control Panel", &GEMReadoutWebApplication::controlPanel);
}

gem::readout::GEMReadoutWebApplication::~GEMReadoutWebApplication()
{
}

void gem::readout::GEMReadoutWebApplication::webDefault(xgi::Input* in, xgi::Output* out)
{
    *out << "<script src=\"/cmsgemos/gemreadout/html/scripts/readout.js\"></script>" << std::endl;

    GEMWebApplication::webDefault(in, out);
}
