/// @file

#ifndef GEM_READOUT_GEMREADOUTAPPLICATION_H
#define GEM_READOUT_GEMREADOUTAPPLICATION_H

#include <gem/utils/GEMFSMApplication.h>
#include <gem/utils/GEMLogging.h>
#include <gem/utils/Lock.h>
#include <gem/utils/LockGuard.h>

#include <toolbox/SyncQueue.h>
#include <toolbox/Task.h>
#include <toolbox/mem/Pool.h>
#include <xoap/MessageReference.h>
#include <xoap/Method.h>

#include <fstream>
#include <queue>
#include <string>

namespace gem::readout {

class GEMReadoutTask;

class GEMReadoutApplication : public gem::utils::GEMFSMApplication {
public:
    enum struct ReadoutCommands : int {
        CMD_STOP = 1,
        CMD_START = 2,
        CMD_PAUSE = 3,
        CMD_RESUME = 4,
        CMD_EXIT = 5
    };

    GEMReadoutApplication(xdaq::ApplicationStub* stub);
    virtual ~GEMReadoutApplication();

    int readoutTask();

protected:
    void actionPerformed(xdata::Event& event) override;

    // State transitions
    void initializeAction() override;
    void configureAction() override;
    void startAction() override;
    void pauseAction() override;
    void resumeAction() override;
    void stopAction() override;
    void haltAction() override;

    std::shared_ptr<toolbox::Task> m_task;
    toolbox::SyncQueue<ReadoutCommands> m_cmdQueue;

    /// @brief Reads up to one event
    ///
    /// @param @event contains the event data, including the CDF header and trailer
    ///
    /// @return @c true if the event if valid, @c false otherwise
    virtual bool readout(std::vector<uint64_t>& event) = 0;
    void save_event(const std::vector<uint64_t>& event);

    // Configuration parameters
    xdata::UnsignedInteger64 m_max_events_per_file = 10000; ///< Specifies the maximum number of events to store in a single index file
    xdata::String m_outputPath = ""; ///< Specifies the path for the output data files

    std::string m_filename_base = ""; ///< Contains the directory under which the data files are stored (valid only after the start transition)
    uint32_t m_file_index = 0; ///< Index of the file in which the events are currently saved
    std::ofstream m_output_file; ///< File into which the events are currently saved

    // Accounting
    uint64_t m_eventsReadout = 0;
    double m_usecUsed = 0;
    double m_usecPerEvent = 0;
};

class GEMReadoutTask : public toolbox::Task {
public:
    GEMReadoutTask(GEMReadoutApplication* app)
        : toolbox::Task("GEMReadoutTask")
    {
        p_readoutApp = app;
    }
    virtual int svc() { return p_readoutApp->readoutTask(); }

private:
    GEMReadoutApplication* p_readoutApp;
};

} // namespace gem::readout

#endif // GEM_READOUT_GEMREADOUTAPPLICATION_H
