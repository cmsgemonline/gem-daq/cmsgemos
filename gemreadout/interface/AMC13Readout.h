/// @file

#ifndef GEM_READOUT_AMC13READOUT_H
#define GEM_READOUT_AMC13READOUT_H

#include <gem/readout/GEMReadoutApplication.h>

#include <memory>

namespace amc13 {
class AMC13;
}

namespace gem::readout {

class AMC13Readout : public gem::readout::GEMReadoutApplication {
public:
    XDAQ_INSTANTIATOR();

    AMC13Readout(xdaq::ApplicationStub* s);
    virtual ~AMC13Readout();

protected:
    // State transitions
    void initializeAction() override;

    bool readout(std::vector<uint64_t>& event) override;

private:
    std::unique_ptr<::amc13::AMC13> p_amc13;

    // Configuration parameters
    xdata::UnsignedInteger32 m_fed_id = -1; ///< Specifies the FED ID of which the AMC13 is part of
};

} // namespace gem::readout

#endif // GEM_READOUT_AMC13READOUT_H
