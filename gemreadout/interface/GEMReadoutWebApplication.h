/// @file

#ifndef GEM_READOUT_GEMREADOUTWEBAPPLICATION_H
#define GEM_READOUT_GEMREADOUTWEBAPPLICATION_H

#include <gem/utils/GEMWebApplication.h>

namespace gem::readout {

// Forward declaration
class GEMReadoutApplication;

class GEMReadoutWebApplication : public gem::utils::GEMWebApplication {
    friend class GEMReadoutApplication;

public:
    GEMReadoutWebApplication(GEMReadoutApplication* application);
    virtual ~GEMReadoutWebApplication();

protected:
    void webDefault(xgi::Input* in, xgi::Output* out) override;
};

} // namespace gem::readout

#endif // GEM_READOUT_GEMREADOUTWEBAPPLICATION_H
