/// @file
/// @brief XHAL exception base class

#ifndef SRC_GEM_HARDWARE_EXCEPTION_H
#define SRC_GEM_HARDWARE_EXCEPTION_H

#include <exception>
#include <string>

#define GEM_HARDWARE_DEFINE_EXCEPTION(EXCEPTION_NAME)                \
    namespace gem {                                                  \
    namespace hardware {                                             \
        class EXCEPTION_NAME : public std::exception {               \
        public:                                                      \
            EXCEPTION_NAME(std::string message)                      \
                : msg(message)                                       \
            {                                                        \
            }                                                        \
                                                                     \
            virtual ~EXCEPTION_NAME() {}                             \
                                                                     \
            virtual const char* what() const noexcept(true) override \
            {                                                        \
                return msg.c_str();                                  \
            }                                                        \
                                                                     \
            std::string msg;                                         \
                                                                     \
        private:                                                     \
            EXCEPTION_NAME();                                        \
        };                                                           \
    } /* namespace gem::hardware */                                  \
    } /* namespace gem */

GEM_HARDWARE_DEFINE_EXCEPTION(XHALException)
GEM_HARDWARE_DEFINE_EXCEPTION(XHALXMLParserException)
GEM_HARDWARE_DEFINE_EXCEPTION(XHALRPCException)
GEM_HARDWARE_DEFINE_EXCEPTION(XHALRPCNotConnectedException)

#undef GEM_HARDWARE_DEFINE_EXCEPTION

#endif // SRC_GEM_HARDWARE_EXCEPTION_H
