/// @file

#include "utils.h"

#include "board/board.h"
#include "config.h"
#include "exception.h"
#include "lmdb++.h"
#include <xhal/memhub/memhub.h>

#include <fmt/format.h>
#include <log4cplus/configurator.h>
#include <log4cplus/hierarchy.h>

#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <string.h>
#include <sys/wait.h>
#include <thread>
#include <unistd.h>

// local functions

log4cplus::Logger gem::hardware::logger;

std::unique_ptr<xhal::memhub::base_memhub> gem::hardware::memhub { nullptr };

struct lmdb_handles_t {
    lmdb::env env;
    lmdb::txn rtxn;
    lmdb::dbi rdbi;
};
std::unique_ptr<lmdb_handles_t> lmdb_handles { nullptr };

std::vector<std::string> gem::hardware::utils::split(const std::string& s, char delim)
{
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

void gem::hardware::utils::init_logging(std::istream& configuration)
{
    log4cplus::initialize();

    // Loading the same configuration twice seems to create issues
    // Prefer to start from scratch
    log4cplus::Logger::getDefaultHierarchy().resetConfiguration();

    log4cplus::PropertyConfigurator configurator(configuration);
    configurator.configure();

    // FIXME: Force a copy since move construtors are disabled in the compiler library
    auto t_logger = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("logger"));
    logger = t_logger;
}

std::string gem::hardware::utils::get_errno_string(int errnum)
{
    char t_errbuf[512] = { 0 };
    const char* r_errbuf = strerror_r(errnum, t_errbuf, 511);
#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && !_GNU_SOURCE
    return t_errbuf;
#else
    return r_errbuf;
#endif
}

void gem::hardware::utils::execute_command(const std::string& command)
{
    const int rc = std::system(command.data());

    if (rc) {
        std::stringstream errmsg;
        errmsg << "Could not execute '" << command.data() << "' with ";

        if (WIFEXITED(rc))
            errmsg << "EXITSTATUS = " << WEXITSTATUS(rc);
        if (WIFSIGNALED(rc))
            errmsg << "TERMSIG = " << WTERMSIG(rc);
        if (WIFSTOPPED(rc))
            errmsg << "STOPSIG = " << WSTOPSIG(rc);
        if (WIFCONTINUED(rc))
            errmsg << "CONTINUED";

        LOG4CPLUS_INFO(logger, errmsg.str());

        throw std::runtime_error(errmsg.str());
    }
}

std::map<std::string, uint32_t> gem::hardware::utils::read_configuration_file(const std::string& filename)
{
    std::map<std::string, uint32_t> data;

    const std::string path = config_dir + "/" + filename;

    std::ifstream infile(path);
    if (!infile.is_open()) {
        LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Could not open config file '{:s}' - using empty configuration"), path));
        return {};
    }

    std::string line, key;
    uint32_t line_number = 1, value = 0x0;

    while (std::getline(infile, line)) {
        // Remove comments, i.e. characters after #
        const auto commentIndex = line.find("#");
        if (commentIndex != std::string::npos) {
            line.erase(commentIndex);
        }

        // Skip blank lines
        const auto firstChar = line.find_first_not_of(" \f\n\r\t\v");
        if (firstChar == std::string::npos) {
            continue;
        }

        // Other lines must contain the register name followed by its numerical value
        std::stringstream iss(line);
        if (!(iss >> key >> value)) {
            throw std::runtime_error(fmt::format(FMT_STRING("Error while reading configuration file '{:s}' on line {:d}"), path, line_number));
        } else {
            data[key] = value;
        }

        ++line_number;
    }

    return data;
}

std::string gem::hardware::utils::get_clock_configuration()
{
    const std::string backend_firmware_dir = get_backend_firmware_dir();
    const std::string path = backend_firmware_dir + "/clocks.cfg";

    std::ifstream infile(path);
    if (!infile.is_open()) {
        std::stringstream errmsg;
        throw std::runtime_error(fmt::format(FMT_STRING("Could not open clock config file '{:s}'"), path));
    }

    std::string line;

    // The clock configuration is contained in the first non-empty line
    while (std::getline(infile, line)) {
        // Remove comments, i.e. characters after #
        const auto commentIndex = line.find("#");
        if (commentIndex != std::string::npos) {
            line.erase(commentIndex);
        }

        // Skip blank lines
        const auto firstChar = line.find_first_not_of(" \f\n\r\t\v");
        if (firstChar == std::string::npos) {
            continue;
        }

        break;
    }

    // Split the clock configuration in its arguments
    auto arguments = split(line, ' ');
    if (arguments.size() != 6)
        throw std::runtime_error("A clock configuration must have 6 parameters");

    // Validate the PLL configuration values
    for (int i = 0; i < 2; ++i) {
        auto& arg = arguments.at(i);

        // Accept pre-configured values
        if ((arg == "320_160") || (arg == "128_192") || (arg == "250"))
            continue;

        // Do not modify absolute paths
        if (arg.at(0) == '/')
            continue;

        // Relative paths are relative w.r.t. the backend firmware
        arg.insert(0, backend_firmware_dir + "/");
    }

    // Validate X-point switches values
    for (int i = 2; i < 6; ++i) {
        auto& arg = arguments.at(i);
        if (arg != "A0" && arg != "A1" && arg != "B0" && arg != "B1")
            throw std::runtime_error("The X-point switches values must be one of: A0, A1, B0, B1");
    }

    return fmt::format("{}", fmt::join(arguments, " "));
}

std::string gem::hardware::utils::get_backend_firmware_dir()
{
    // Elect path
    const auto environment_path = std::getenv("GEM_BACKEND_FIRMWARE");
    const auto default_path = config_dir + "/backend-firmware";
    const char* t_path = environment_path ? environment_path : default_path.data();

    // Resolve path
    char* t_real_path = realpath(t_path, nullptr);
    if (!t_real_path)
        throw std::runtime_error(fmt::format(FMT_STRING("Cannot resolve GEM backend firmware path ({:s}) : {:s}"), t_path, get_errno_string(errno)));

    const std::string real_path(t_real_path);
    free(t_real_path);

    return real_path;
}

std::string gem::hardware::utils::get_optohybrid_firmware_dir()
{
    // Elect path
    const auto environment_path = std::getenv("GEM_OPTOHYBRID_FIRMWARE");
    const auto default_path = config_dir + "/optohybrid-firmware";
    const char* t_path = environment_path ? environment_path : default_path.data();

    // Resolve path
    char* t_real_path = realpath(t_path, nullptr);
    if (!t_real_path)
        throw std::runtime_error(fmt::format(FMT_STRING("Cannot resolve GEM OptoHybrid firmware path ({:s}) : {:s}"), t_path, get_errno_string(errno)));

    const std::string real_path(t_real_path);
    free(t_real_path);

    return real_path;
}

void gem::hardware::utils::update_address_table()
{
    // The LMDB database files
    const std::string lmdb_data_file = localstate_dir + "/address-tables.mdb";
    const std::string lmdb_data_file_tmp = localstate_dir + "/address-tables.mdb.tmp";

    // The original XML address tables
    const std::string backend_address_table = get_backend_firmware_dir() + "/address-table.xml";
    const std::string optohybrid_address_table = get_optohybrid_firmware_dir() + "/address-table.xml";

    // Symlink the XML files to match the names expected by the address tables
    const std::string backend_xml_file = localstate_dir + "/backend_registers.xml";
    std::remove(backend_xml_file.data());
    if (symlink(backend_address_table.data(), backend_xml_file.data()))
        throw std::runtime_error(fmt::format(FMT_STRING("Cannot create symlink ({:s}): {:s}"), backend_xml_file, get_errno_string(errno)));

    const std::string optohybrid_xml_file = localstate_dir + "/optohybrid_registers.xml";
    std::remove(optohybrid_xml_file.data());
    if (symlink(optohybrid_address_table.data(), optohybrid_xml_file.data()))
        throw std::runtime_error(fmt::format(FMT_STRING("Cannot create symlink ({:s}): {:s}"), optohybrid_xml_file, get_errno_string(errno)));

    LOG4CPLUS_INFO(logger, "Parsing XML files...");
    auto parser = std::make_unique<utils::XHALXMLParser>(backend_xml_file.data());
    try {
        parser->setLogLevel(0);
        parser->parseXML();
    } catch (...) {
        LOG4CPLUS_ERROR(logger, "XML parsing failed");
        throw std::runtime_error("XML parsing failed");
    }
    auto parsed_at = parser->getAllNodes();

    LOG4CPLUS_INFO(logger, "Creating new LMDB database...");

    // Remove leftovers from a previous failed update, if any
    std::remove(lmdb_data_file_tmp.data());
    std::remove((lmdb_data_file_tmp + "-lock").data());

    auto env = lmdb::env::create();
    env.set_mapsize(LMDB_SIZE);
    env.open(lmdb_data_file_tmp.data(), MDB_NOSUBDIR, 0644);

    auto wtxn = lmdb::txn::begin(env);
    auto wdbi = lmdb::dbi::open(wtxn, nullptr);

    LOG4CPLUS_INFO(logger, "Adding registers to LMDB...");

    lmdb::val key, value;
    for (auto const& it : parsed_at) {
        const std::string& t_key = it.first;

        const Node& t_node = it.second;
        RegInfo t_value = {
            .address = t_node.real_address,
            .mask = t_node.mask,
            .size = t_node.size,
            .permissions = 0,
            .mode = 0,
        };

        if (t_node.permission.find("w") != std::string::npos)
            t_value.permissions |= RegInfo::is_writable;
        if (t_node.permission.find("r") != std::string::npos)
            t_value.permissions |= RegInfo::is_readable;

        if (t_node.mode == "single")
            t_value.mode = RegInfo::is_single;
        else if (t_node.mode == "block")
            t_value.mode = RegInfo::is_block;
        else
            throw std::runtime_error(fmt::format(FMT_STRING("Unsuppoted mode '{:s}' for register '{:s}'"), t_node.mode, t_key));

        key.assign(t_key);
        value.assign(&t_value, sizeof(t_value));
        wdbi.put(wtxn, key, value);
    }

    LOG4CPLUS_INFO(logger, "Saving LMDB database...");

    // Commit the transaction and close the DB
    wtxn.commit();
    env.close();

    // If everything went well, replace the old LMDB file
    std::rename(lmdb_data_file_tmp.data(), lmdb_data_file.data());
}

uint32_t gem::hardware::utils::bitCheck(uint32_t word, int bit)
{
    if (bit > 31)
        throw std::invalid_argument("Invalid request to shift 32-bit word by more than 31 bits");
    return (word >> bit) & 0x1;
}

uint32_t gem::hardware::utils::getNumNonzeroBits(uint32_t value)
{
    // https://stackoverflow.com/questions/4244274/how-do-i-count-the-number-of-zero-bits-in-an-integer
    uint32_t numNonzeroBits = 0;
    for (size_t i = 0; i < CHAR_BIT * sizeof value; ++i) {
        if ((value & (1 << i)) == 1) {
            ++numNonzeroBits;
        }
    }

    return numNonzeroBits;
}

void gem::hardware::utils::init_lmdb()
{
    // Don't do anything if the database is already opened
    if (lmdb_handles)
        return;

    const std::string lmdb_data_file = localstate_dir + "/address-tables.mdb";

    auto env = lmdb::env::create();
    env.set_mapsize(LMDB_SIZE);
    env.open(lmdb_data_file.data(), MDB_NOSUBDIR | MDB_NOLOCK | MDB_RDONLY, 0644);

    auto rtxn = lmdb::txn::begin(env, nullptr, MDB_RDONLY);
    auto rdbi = lmdb::dbi::open(rtxn, nullptr);

    lmdb_handles.reset(new lmdb_handles_t { .env = std::move(env),
        .rtxn = std::move(rtxn),
        .rdbi = std::move(rdbi) });
}

void gem::hardware::utils::init_memhub()
{
    // Don't do anything if the memory service already exists
    if (memhub)
        return;

    memhub = std::make_unique<board::memory_type>("memhub-card0-fpga0", board::memory_base, board::memory_size);
}

void gem::hardware::utils::assert_n(const size_t trials, std::function<bool()> condition, const std::string& message)
{
    for (size_t i = 0; i < trials; ++i) {
        if (condition())
            break;

        // Throw after the last attempt
        if (i == trials - 1)
            throw std::runtime_error(message);

        LOG4CPLUS_WARN(logger, message << " (" << i + 1 << " out of " << trials << ")");
    }
}

bool gem::hardware::utils::regExists(const std::string& regName)
{
    if (!lmdb_handles)
        throw std::runtime_error("LMDB connection closed (must be open before any register lookup)");

    lmdb::val key, db_res;
    key.assign(regName);
    if (lmdb_handles->rdbi.get(lmdb_handles->rtxn, key, db_res))
        return true;

    return false;
}

gem::hardware::utils::RegInfo gem::hardware::utils::getReg(const std::string& regName)
{
    if (!lmdb_handles)
        throw std::runtime_error("LMDB connection closed (must be open before any register lookup)");

    lmdb::val key, db_res;
    key.assign(regName);
    if (!lmdb_handles->rdbi.get(lmdb_handles->rtxn, key, db_res))
        throw std::runtime_error(fmt::format(FMT_STRING("Register {:s} not found in LMDB"), regName));

    // Note how we use memcpy to copy the RegInfo structure instead of
    // dereferencing the pointer. LMDB does not guaranteed any alignement on
    // the key and value so that dereferencing can (and does) end up in very
    // slow unaligned memory access, particularly on ARM where the kernel traps
    // and fixes up the load. The mempy function is equivalent but should be
    // optimized out by the compiler and/or linker is alignement safe.
    RegInfo res;
    memcpy(&res, db_res.data(), sizeof(RegInfo));

    return res;
}

std::vector<std::string> gem::hardware::utils::find_registers(const std::vector<std::string>& parts)
{
    if (!lmdb_handles)
        throw std::runtime_error("LMDB connection closed (must be open before any register lookup)");

    std::vector<std::string> result;

    auto cur = lmdb::cursor::open(lmdb_handles->rtxn, lmdb_handles->rdbi);

    // Loop over all keys stored in the DB
    lmdb::val key;
    while (cur.get(key, nullptr, MDB_NEXT)) {
        std::string register_name(key.data(), key.size());
        std::size_t pos = 0;

        for (const auto& ipart : parts) {
            pos = register_name.find(ipart, pos);
            if (pos == std::string::npos)
                break;
            // Continue the search from the end of the last part found
            pos += ipart.size();
        }

        // Store the register name only if all parts were found
        if (pos != std::string::npos)
            result.push_back(std::move(register_name));
    }

    return result;
}

void gem::hardware::utils::writeRawAddress(const uint32_t address, const uint32_t value, const uint32_t mask)
{
    memhub->write(address, value, mask);
}

uint32_t gem::hardware::utils::readRawAddress(const uint32_t address, const uint32_t mask)
{
    return memhub->read(address, mask);
}

void gem::hardware::utils::writeReg(const RegInfo& reg, const uint32_t value)
{
    if (!(reg.permissions & RegInfo::is_writable))
        throw std::runtime_error(fmt::format(FMT_STRING("No write permission on {:#08x}"), reg.address));

    memhub->write(reg.address, value, reg.mask);
}

uint32_t gem::hardware::utils::readReg(const RegInfo& reg)
{
    if (!(reg.permissions & RegInfo::is_readable))
        throw std::runtime_error(fmt::format(FMT_STRING("No read permission on {:#08x}"), reg.address));

    return memhub->read(reg.address, reg.mask);
}

void gem::hardware::utils::writeReg(const std::string& regName, const uint32_t value)
{
    try {
        const auto reg = getReg(regName);

        if (!(reg.permissions & RegInfo::is_writable))
            throw std::runtime_error(fmt::format(FMT_STRING("No write permission on {:s}"), regName));

        return memhub->write(reg.address, value, reg.mask);
    } catch (const xhal::memhub::memory_access_error& e) {
        throw register_acces_error(regName, e);
    }
}

uint32_t gem::hardware::utils::readReg(const std::string& regName)
{
    try {
        const auto reg = getReg(regName);

        if (!(reg.permissions & RegInfo::is_readable))
            throw std::runtime_error(fmt::format(FMT_STRING("No read permission on {:s}"), regName));

        return memhub->read(reg.address, reg.mask);
    } catch (const xhal::memhub::memory_access_error& e) {
        throw register_acces_error(regName, e);
    }
}

void gem::hardware::utils::writeBlock(const std::string& regName, const uint32_t* values, const uint32_t& size, const uint32_t& offset)
{
    const auto db_res = getReg(regName);

    if (db_res.mask != 0xFFFFFFFF) {
        // deny block write on masked register
        std::stringstream errmsg;
        errmsg << "Block write attempted on masked register";
        LOG4CPLUS_ERROR(logger, errmsg.str());
        throw std::runtime_error(errmsg.str());
    } else if (!(db_res.mode & RegInfo::is_block)) {
        // only allow block write of size 1 on single registers
        std::stringstream errmsg;
        errmsg << "Block write attempted on single register with size greater than 1";
        LOG4CPLUS_ERROR(logger, errmsg.str());
        throw std::runtime_error(errmsg.str());
    } else if ((offset + size) > db_res.size) {
        // don't allow the write to go beyond the block range
        std::stringstream errmsg;
        errmsg << "Block write attempted would go beyond the size of the RAM: "
               << "raddr: 0x" << std::hex << db_res.address
               << ", offset: 0x" << std::hex << offset
               << ", size: 0x" << std::hex << size
               << ", rsize: 0x" << std::hex << db_res.size;
        LOG4CPLUS_ERROR(logger, errmsg.str());
        throw std::runtime_error(errmsg.str());
    } else {
        for (uint32_t i = 0; i < size; ++i) {
            memhub->write(db_res.address + offset + i, values[i]);
        }
    }
}

uint32_t gem::hardware::utils::readBlock(const std::string& regName, uint32_t* result, const uint32_t& size, const uint32_t& offset)
{
    const auto db_res = getReg(regName);

    if (db_res.mask != 0xFFFFFFFF) {
        // FIXME
        // deny block read on masked register, but what if mask is None?
        // Adopt the rule that mask None equals to mask == 0xFFFFFFFF?
        std::stringstream errmsg;
        errmsg << "Block read attempted on masked register";
        LOG4CPLUS_ERROR(logger, errmsg.str());
        throw std::range_error(errmsg.str());
    } else if (!(db_res.mode & RegInfo::is_block)) {
        // only allow block read of size 1 on single registers?
        std::stringstream errmsg;
        errmsg << "Block read attempted on single register with size greater than 1";
        LOG4CPLUS_ERROR(logger, errmsg.str());
        throw std::range_error(errmsg.str());
    } else if ((offset + size) > db_res.size) {
        // don't allow the read to go beyond the range
        std::stringstream errmsg;
        errmsg << "Block read attempted would go beyond the size of the RAM: "
               << "raddr: 0x" << std::hex << db_res.address
               << ", offset: 0x" << std::hex << offset
               << ", size: 0x" << std::hex << size
               << ", rsize: 0x" << std::hex << db_res.size;
        LOG4CPLUS_ERROR(logger, errmsg.str());
        throw std::range_error(errmsg.str());
    } else {
        for (uint32_t i = 0; i < size; ++i) {
            result[i] = memhub->read(db_res.address + offset + i);
        }
    }

    return size;
}

// remote functions

uint32_t gem::hardware::utils::readRemoteReg::operator()(const std::string& regName) const
{
    return readReg(regName);
}

void gem::hardware::utils::writeRemoteReg::operator()(const std::string& regName, const uint32_t& value) const
{
    return writeReg(regName, value);
}
