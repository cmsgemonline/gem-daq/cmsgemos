/// @file GBTx/lpGBT common features
///
/// If the GEM firmware abstracts everything related to the communication,
/// high speed links, locking, slow-control transaction,... between the GBTx,
/// lpGBT v0, and lpGBT v1, the same is not (and cannot be) done for the register
/// map. This includes, among others, the configuration after power-up or the
/// phase scan.
///
/// The various functions implemented here aim at abstracting those
/// differences. For reference, the register addresses spaces are the
/// following:
///
/// * GBTx
///   *   0 - 365 - Read/Write/Fuse
///   * 366 - 368 - Read/Fuse
///   * 369 - 435 - Read
///
/// * lpGBT v0
///   *   0 - 239 - Read/Write/Fuse
///   * 240 - 319 - Read/Write
///   * 320 - 462 - Read
///
/// * lpGBT v1
///   *   0 - 255 - Read/Write/Fuse
///   * 256 - 335 - Read/Write
///   * 336 - 493 - Read
///
/// The fuses can include critical information or read-only information such as
/// the lock parameters and the fuse-to-register transfer selection or the chip
/// ID. Additionally, the lpGBT v1 can also boot from ROM, i.e. the chip gets
/// the ability to lock without any fusing or external configuration.
///
/// It is by choice that only writable fuse registers are part of a
/// configuration and programmed by default. The other writable registers
/// should allow using more block in the chip but are not required for basic
/// operations (I2C masters, eye diagram,...) Would the operations require it,
/// more functions could be added (e.g. CRC, watchdog,...)
///
/// Considering the reduced number of lpGBT v0 used in the GEM project and
/// their usage, only the GBTx and lpGBT v1 are currently supported.

#ifndef SRC_GEM_HARDWARE_GBT_H
#define SRC_GEM_HARDWARE_GBT_H

#include <gem/hardware/gbt.h>

#include "hw_constants.h"

#include <array>
#include <stdint.h>
#include <utility> // std::pair

namespace gem {
namespace hardware {
    namespace gbt {

        /// @brief Exception representing an error in the IC communication with a GBT
        class gbt_ic_error : public std::exception {
        public:
            /// @brief Representing the direction of an IC transaction
            enum class direction_t {
                read, ///< Read transaction
                unknown, ///< Undetermined transaction direction, useful as default
                write, ///< Write transaction
            };

            /// @brief Converts a direction into a human readable string
            static const char* to_string(const direction_t& direction);

            /// @brief Creates an error message based on the IC controller status registers
            ///
            /// @details The error message is extract from the status registers available
            ///          under @c BEFE.GEM.SLOW_CONTROL.IC.READ_STATUS. Note that it is
            ///          recommended to call this function atomically with the IC transaction
            ///          itself. Failing to do so could result in corrupted/invalid error
            ///          message.
            static std::string create_message_from_ic_status();

            /// @brief Constructor
            ///
            /// @param @c ohN OptoHybrid index number for which the transaction failed
            /// @param @c gbtN GBT index number for which the transaction failed
            /// @param @c address on which the transaction failed
            /// @param @c direction of the transaction
            /// @param @c message additional informational message
            explicit gbt_ic_error(uint32_t ohN, uint32_t gbtN, uint16_t address, direction_t direction = direction_t::unknown, const std::string& message = "");

            /// @brief Returns the OptoHybrid index number for which the transaction failed
            uint32_t ohN() const noexcept { return m_ohN; }

            /// @brief Returns the GBT index number for which the transaction failed
            uint32_t gbtN() const noexcept { return m_gbtN; }

            /// @brief Returns the address for which the error occurred
            uint32_t address() const noexcept { return m_address; }

            /// @brief Returns @c true if the operation was a read access
            bool is_read() const noexcept { return m_direction == direction_t::read ? true : false; }

            /// @brief Returns @c true if the operation was a write access
            bool is_write() const noexcept { return m_direction == direction_t::write ? true : false; }

            /// @brief Description of the error
            const char* what() const noexcept override { return m_what.data(); }

        protected:
            uint32_t m_ohN = 0; ///< Address for which the access failed
            uint32_t m_gbtN = 0; ///< Address for which the access failed
            uint16_t m_address = 0; ///< Address for which the access failed
            direction_t m_direction = direction_t::unknown; ///< Direction of the transaction
            std::string m_message = ""; ///< Original text message

            std::string m_what = ""; ///< Complete error message
        };

        /// @brief This type defines a generic GBT configuration blob
        using config_t = std::array<uint8_t, GBT_CONFIG_SIZE>;

        /// @brief Returns the default GBT configuration from files
        std::array<config_t, GBTS_PER_OH> getDefaultGBTConfigurations();

        /// @brief Returns the reachable front-end components for a given GBT mask
        ///
        /// @param @c gbtMask GBT mask
        ///
        /// @return An @c std::pair with the SCA and OptoHybrid FPGA (if applicable) availability as first argument and the VFAT mask as second argument
        std::pair<bool, uint32_t> getReachableFrontend(uint8_t gbtMask);

        /// @brief Reads a single register in the given GBT of the given OptoHybrid
        ///
        /// @param @c ohN OptoHybrid index number
        /// @param @c gbtN Index of the GBT to write
        /// @param @c address GBT register address to write
        /// @param @c mask Mask of the register
        /// @param @c unsafe Whether a blind/unsafe transaction should be performed (log the error and return 0 instead of throwing)
        uint8_t readGBTReg(uint32_t ohN, uint32_t gbtN, uint16_t address, uint8_t mask = 0xff, bool unsafe = false);

        /// @brief Writes a single register in the given GBT of the given OptoHybrid
        ///
        /// @param @c ohN OptoHybrid index number
        /// @param @c gbtN Index of the GBT to write
        /// @param @c address GBT register address to write
        /// @param @c value Value to write to the GBT register
        /// @param @c mask Mask of the register
        /// @param @c unsafe Whether a blind/unsafe transaction should be performed (log the error instead of throwing)
        void writeGBTReg(uint32_t ohN, uint32_t gbtN, uint16_t address, uint8_t value, uint8_t mask = 0xff, bool unsafe = false);

        /// @brief Resets the GBT specified in the mask via register reset
        ///
        /// @note Currently implemented only for ME0
        ///
        /// @warning Due to the particularities of the ASIAGO, this reset is
        ///          implemented as a sequence of unsafe/blind operations. The
        ///          effectiveness of the reset should be tested by external
        ///          means depending on the situation.
        ///
        /// @param @c ohN The OptoHybrid index number
        /// @param @c gbtMask The GBT mask
        void reset_gbt(uint32_t ohN, uint8_t gbtMask);

        /// @brief Resets the VTRx+ connected to the master GBT specified in the mask
        ///
        /// @note Currently implemented only for ME0
        ///
        /// @warning Due to the particularities of the ASIAGO, this reset is
        ///          implemented as a sequence of unsafe/blind operations. The
        ///          effectiveness of the reset should be tested by external
        ///          means depending on the situation.
        ///
        /// @param @c ohN The OptoHybrid index number
        /// @param @c gbtMask The GBT mask
        void reset_vtrx_lasers(uint32_t ohN, uint8_t gbtMask);

        /// @brief Enables the lasers on the VTRx connected to the specified GBT
        ///
        /// @note Currently implemented only for ME0
        ///
        /// @warning Due to the particularities of the ASIAGO, this action is
        ///          implemented as a sequence of unsafe/blind operations. The
        ///          effectiveness of the action should be tested by external
        ///          means depending on the situation.
        ///
        /// @param @c ohN The OptoHybrid index number
        /// @param @c gbtMask The GBT mask
        void enable_vtrx_lasers(uint32_t ohN, uint8_t gbtMask);

        /// @brief Resets the slave GBT connected to the master GBT specified in the mask via external reset
        ///
        /// @note Implemented only for ME0
        ///
        /// @param @c ohN The OptoHybrid index number
        /// @param @c gbtMask The GBT mask
        ///
        /// @returns The GBT mask for which the reset was successful
        uint8_t reset_slave_gbt(uint32_t ohN, uint8_t gbtMask);

        /// @brief Defines the various existing eLink RX phase tracking modes
        enum class eLinkRXPhaseTrackingMode : uint8_t {
            FIXED = 0, ///< Fixed phase
            TRAINING = 1, ///< Training mode
            CONTINOUS = 2, ///< Continuous phase tracking
            CONTINOUS_INITIAL = 3, ///< Continuous phase tracking with initial phase
        };

        /// @brief Configures the lpGBT eLink RX phase tracking mode
        ///
        /// @note Currently implemented only for ME0
        ///
        /// @param @c ohN OptoHybrid index number
        /// @param @c gbtN Index of the GBT to configure
        /// @param @c mode RX phase tracking mode to apply
        void set_elink_rx_phase_tracking_mode(uint32_t ohN, uint32_t gbtN, eLinkRXPhaseTrackingMode mode);

    } // namespace gem::hardware::gbt
} // namespace gem::hardware
} // namespace gem

#endif // SRC_GEM_HARDWARE_GBT_H
