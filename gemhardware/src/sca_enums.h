/// @file

#ifndef SRC_GEM_HARDWARE_SCA_ENUMS_H
#define SRC_GEM_HARDWARE_SCA_ENUMS_H

#include <stdint.h>

namespace gem {
namespace hardware {
    namespace sca {

        /// @brief SCA Error Flags
        ///
        /// List of error flags that can be set in the reply
        enum struct SCAErrorFlags : uint8_t {
            Generic = 0x01, ///< Generic error flag
            InvChReq = 0x02, ///< Invalid channel requested
            InvCmdReq = 0x04, ///< Invalid command requested
            InvTranReqN = 0x08, ///< Invalid transaction request number
            InvLen = 0x10, ///< Invalid length
            ChNotEn = 0x20, ///< Channel not enabled
            ChBusy = 0x40, ///< Channel busy
            CmdInTreat = 0x80, ///< Command in treatment
        };

        /// @brief SCA channels
        ///
        /// List of channels with which the SCA can be controlled
        enum struct SCAChannel : uint8_t {
            CTRL = 0x00, ///< SCA configuration registers
            SPI = 0x01, ///< Serial peripheral master interface
            GPIO = 0x02, ///< General purpose parallel I/O interface
            I2C00 = 0x03, ///< I2C serial interface - master 0
            I2C01 = 0x04, ///< I2C serial interface - master 1
            I2C02 = 0x05, ///< I2C serial interface - master 2
            I2C03 = 0x06, ///< I2C serial interface - master 3
            I2C04 = 0x07, ///< I2C serial interface - master 4
            I2C05 = 0x08, ///< I2C serial interface - master 5
            I2C06 = 0x09, ///< I2C serial interface - master 6
            I2C07 = 0x0a, ///< I2C serial interface - master 7
            I2C08 = 0x0b, ///< I2C serial interface - master 8
            I2C09 = 0x0c, ///< I2C serial interface - master 9
            I2C10 = 0x0d, ///< I2C serial interface - master 10
            I2C11 = 0x0e, ///< I2C serial interface - master 11
            I2C12 = 0x0f, ///< I2C serial interface - master 12
            I2C13 = 0x10, ///< I2C serial interface - master 13
            I2C14 = 0x11, ///< I2C serial interface - master 14
            I2C15 = 0x12, ///< I2C serial interface - master 15
            JTAG = 0x13, ///< JTAG serial master interface
            ADC = 0x14, ///< Analog-to-digital converter
            DAC = 0x15, ///< Digital-to-analog converter
        };

        /// @brief SCA control commands
        ///
        /// Command aliases that can be used when controlling the SCA generic control interface
        enum struct CTRLCommand : uint8_t {
            CTRL_W_CRB = 0x02, ///< Write to CTRL register B
            CTRL_R_CRB = 0x03, ///< Read from CTRL register B
            CTRL_W_CRC = 0x04, ///< Write to CTRL register C
            CTRL_R_CRC = 0x05, ///< Read from CTRL register C
            CTRL_W_CRD = 0x06, ///< Write to CTRL register D
            CTRL_R_CRD = 0x07, ///< Read from CTRL register D
            CTRL_R_ID = 0xD1, ///< Read the SCA ChipID, on channel 0x14 (ADC)
            CTRL_R_SEU = 0xF1, ///< Read from the SEU monitor, on channel 0x13 (JTAG)
            CTRL_C_SEU = 0xF0, ///< Reset the SEU monitor, on channel 0x13 (JTAG)
        };

        /// @brief SCA GPIO commands
        ///
        /// Command aliases that can be used when controlling the GPIO interface of the SCA
        enum struct GPIOCommand : uint8_t {
            GPIO_W_DATAOUT = 0x10, ///< Write GPIO DATAOUT register, length 4, read length 2
            GPIO_R_DATAOUT = 0x11, ///< Read GPIO DATAOUT register, length 1, read length 4
            GPIO_R_DATAIN = 0x01, ///< Read GPIO DATAIN register, length 1, read length 4
            GPIO_W_DIRECTION = 0x20, ///< Write GPIO direction register, length 4, read length 2
            GPIO_R_DIRECTION = 0x21, ///< Read GPIO direction register, length 1, read length 4
            GPIO_W_INTENABLE = 0x60, ///< Write GPIO interrupt enable register, length 4, read length 2
            GPIO_R_INTENABLE = 0x61, ///< Read GPIO interrupt enable register, length 4, read length 2
            GPIO_W_INTSEL = 0x30, ///< Write GPIO interrupt line select register, length 4, read length 2
            GPIO_R_INTSEL = 0x31, ///< Read GPIO interrupt line select register, length 4, read length 2
            GPIO_W_INTTRIG = 0x40, ///< Write GPIO edge select for interrupt register, length 4, read length 2
            GPIO_R_INTTRIG = 0x41, ///< Read GPIO edge select for interrupt register, length 4, read length 2
            GPIO_W_INTS = 0x70, ///< Write GPIO interrupt vector register, length 4, read length 2
            GPIO_R_INTS = 0x71, ///< Read GPIO interrupt vector register, length 4, read length 2
            GPIO_W_CLKSEL = 0x80, ///< Write GPIO clock select register, length 4, read length 2
            GPIO_R_CLKSEL = 0x81, ///< Read GPIO clock select register, length 4, read length 2
            GPIO_W_EDGESEL = 0x90, ///< Write GPIO clock edge select register, length 4, read length 2
            GPIO_R_EDGESEL = 0x91, ///< Read GPIO clock edge select register, length 4, read length 2
        };

        /// @brief SCA ADC commands
        ///
        /// Command aliases that can be used when controlling the ADC interface of the SCA
        enum struct ADCCommand : uint8_t {
            ADC_GO = 0x02, ///< Start of ADC conversion
            ADC_W_MUX = 0x50, ///< Write ADC register INSEL
            ADC_R_MUX = 0x51, ///< Read ADC register INSEL
            ADC_W_CURR = 0x60, ///< Write ADC register
            ADC_R_CURR = 0x61, ///< Read ADC register
            ADC_W_GAIN = 0x10, ///< Write ADC register output A
            ADC_R_GAIN = 0x11, ///< Read ADC register output A
            ADC_R_DATA = 0x21, ///< Read ADC converted value
            ADC_R_RAW = 0x31, ///< Read ADC raw value
            ADC_R_OFS = 0x41, ///< Read ADC offset
        };

        /// @brief SCA ADC channel
        ///
        /// List of ADC channels on the SCA
        enum struct ADCChannel : uint8_t {
            ADC_CH00 = 0x00, ///< ADC channel 00
            ADC_CH01 = 0x01, ///< ADC channel 01
            ADC_CH02 = 0x02, ///< ADC channel 02
            ADC_CH03 = 0x03, ///< ADC channel 03
            ADC_CH04 = 0x04, ///< ADC channel 04
            ADC_CH05 = 0x05, ///< ADC channel 05
            ADC_CH06 = 0x06, ///< ADC channel 06
            ADC_CH07 = 0x07, ///< ADC channel 07
            ADC_CH08 = 0x08, ///< ADC channel 08
            ADC_CH09 = 0x09, ///< ADC channel 09
            ADC_CH10 = 0x0A, ///< ADC channel 10
            ADC_CH11 = 0x0B, ///< ADC channel 11
            ADC_CH12 = 0x0c, ///< ADC channel 12
            ADC_CH13 = 0x0d, ///< ADC channel 13
            ADC_CH14 = 0x0e, ///< ADC channel 14
            ADC_CH15 = 0x0f, ///< ADC channel 15
            ADC_CH16 = 0x10, ///< ADC channel 16
            ADC_CH17 = 0x11, ///< ADC channel 17
            ADC_CH18 = 0x12, ///< ADC channel 18
            ADC_CH19 = 0x13, ///< ADC channel 19
            ADC_CH20 = 0x14, ///< ADC channel 20
            ADC_CH21 = 0x15, ///< ADC channel 21
            ADC_CH22 = 0x16, ///< ADC channel 22
            ADC_CH23 = 0x17, ///< ADC channel 23
            ADC_CH24 = 0x18, ///< ADC channel 24
            ADC_CH25 = 0x19, ///< ADC channel 24
            ADC_CH26 = 0x1a, ///< ADC channel 26
            ADC_CH27 = 0x1b, ///< ADC channel 27
            ADC_CH28 = 0x1c, ///< ADC channel 28
            ADC_CH29 = 0x1d, ///< ADC channel 29
            ADC_CH30 = 0x1e, ///< ADC channel 30
            ADC_CH31 = 0x1f, ///< ADC channel 31

            SCA_TEMP = 0x1f, ///< Internal SCA temperature sensor
        };

    } // namespace gem::hardware::sca
} // namespace gem::hardware
} // namespace gem

#endif // SRC_GEM_HARDWARE_SCA_ENUMS_H
