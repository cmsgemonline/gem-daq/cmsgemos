/// @file

#include "optohybrid.h"

#include "exception.h"
#include "hw_constants.h"
#include "sca.h"
#include "utils.h"

#include <fmt/format.h>

#include <chrono>
#include <fstream>
#include <thread>

namespace /* anonymous */ {

constexpr size_t MAX_TRIGGER_INIT_RETRIES = 10; ///< Maximum number of retries to initialize the trigger links

} // anonymous namespace

// local functions

bool gem::hardware::oh::initialize_trigger_links(const uint32_t ohN)
{
#if defined(GEM_IS_GE11)
    try {
        // Enable the MGT RX in loopback mode
        utils::writeReg(fmt::format(FMT_STRING("BEFE.GEM.OH.OH{}.FPGA.MGT.RX_POWERDOWN"), ohN), 0);
        for (size_t mgtN = 0; mgtN < 4; ++mgtN)
            utils::writeReg(fmt::format(FMT_STRING("BEFE.GEM.OH.OH{}.FPGA.MGT.CONTROL{}.LOOPBACK_MODE"), ohN, mgtN), 2);

        for (size_t retry = 0;; ++retry) {
            // Reset all receivers
            utils::writeReg(fmt::format(FMT_STRING("BEFE.GEM.OH.OH{}.FPGA.MGT.RX_GTX_RESET"), ohN), 1);
            std::this_thread::sleep_for(std::chrono::milliseconds(1)); // Takes at most 120µs (~10x safety factor)

            // Wait for at least 1 orbit (3564 BX) to get the 8b10b comma
            std::this_thread::sleep_for(std::chrono::nanoseconds(25) * 4000);

            // Reset the counters and wait for error to build up
            utils::writeReg(fmt::format(FMT_STRING("BEFE.GEM.OH.OH{}.FPGA.MGT.NOT_IN_TABLE_CNT_RESET"), ohN), 1);
            std::this_thread::sleep_for(std::chrono::milliseconds(200));

            // Check that the loopback stream is valid
            uint8_t valid_mgt = utils::readReg(fmt::format(FMT_STRING("BEFE.GEM.OH.OH{}.FPGA.MGT.RX_VALID"), ohN));
            for (size_t mgtN = 0; mgtN < 4; ++mgtN)
                if (utils::readReg(fmt::format(FMT_STRING("BEFE.GEM.OH.OH{}.FPGA.MGT.NOT_IN_TABLE_CNT{}"), ohN, mgtN)) != 0)
                    valid_mgt &= ~(1UL << mgtN);

            // We are good!
            if (valid_mgt == 0xf) {
                LOG4CPLUS_INFO(logger, fmt::format(FMT_STRING("FPGA trigger links initialized on OH{:d} after {:d} retries"), ohN, retry));
                break;
            }

            // Out of retries
            if (retry >= MAX_TRIGGER_INIT_RETRIES) {
                LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("FPGA trigger links not initialized on OH{:d} after {:d} retries"), ohN, MAX_TRIGGER_INIT_RETRIES));
                break;
            }

            // Else, try again -- reset all transmitters
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("FPGA trigger links not initialized on OH{:d} ({:d} out of {:d} retries)"), ohN, retry, MAX_TRIGGER_INIT_RETRIES));

            for (size_t mgtN = 0; mgtN < 4; ++mgtN)
                utils::writeReg(fmt::format(FMT_STRING("BEFE.GEM.OH.OH{}.FPGA.MGT.CONTROL{}.GTTXRESET"), ohN, mgtN), 1);
            std::this_thread::sleep_for(std::chrono::milliseconds(1)); // Takes at most 120µs (~10x safety factor)
        }
    } catch (const xhal::memhub::memory_access_error& e) {
        LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Failed to initialize the FPGA trigger links on OH{:d} with {:s}"), ohN, e.what()));
        return false;
    }
#endif

    return true;
}

// remote functions

bool gem::hardware::oh::configure_fpga::operator()(const uint32_t ohN) const
{
    const std::string config_filename = fmt::format(FMT_STRING("oh/config-oh{}.cfg"), ohN);
    const std::string register_base = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".FPGA.";

    try {
        const auto data = utils::read_configuration_file(config_filename);
        for (const auto& i : data)
            utils::writeReg(register_base + i.first, i.second);
    } catch (const xhal::memhub::memory_access_error& e) {
        LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Failed to configure the FPGA on OH{:d} with {:s}"), ohN, e.what()));
        return false;
    }

    return true;
}

std::pair<uint32_t, uint32_t> gem::hardware::oh::getProgrammedFPGA::operator()(const uint32_t optohybridMask) const
{
    const auto reply = sca::scaGPIOCommand(optohybridMask, sca::GPIOCommand::GPIO_R_DATAIN, 0x1 /* length */, 0x0 /* no input data*/);

    uint32_t done_mask = 0, valid_mask = 0;
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (reply.at(ohN)) {
            const auto done_bit = (*reply.at(ohN) >> sca::GPIO_FPGA_DONE_INPUT) & 1;
            done_mask |= (done_bit << ohN);
            valid_mask |= (1ULL << ohN);
        }
    }

    return { done_mask, valid_mask };
}

uint32_t gem::hardware::oh::resetFPGA::operator()(const uint32_t optohybridMask) const
{
    const auto initialReply = sca::scaGPIOCommand(optohybridMask, sca::GPIOCommand::GPIO_R_DATAOUT, 0x1 /* length */, 0x0 /* no input data*/);

    uint32_t successMask = 0;
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!initialReply.at(ohN))
            continue;

#if defined(GEM_IS_GE11)
        // GE1/1 -- GPIO #31 active low reset
        const uint32_t resetValue = *initialReply.at(ohN) & ~(1ULL << 31);
#else
        // GE2/1 -- GPIO #31 active high reset
        const uint32_t resetValue = *initialReply.at(ohN) | (1ULL << 31);
#endif

        // Pull PROGRAM_B low
        const auto resetReply = sca::scaGPIOCommand(1ULL << ohN, sca::GPIOCommand::GPIO_W_DATAOUT, 0x4 /* length */, resetValue);
        if (!resetReply.at(ohN))
            continue;

        // Restore PROGRAM_B high
        const auto restoreReply = sca::scaGPIOCommand(1ULL << ohN, sca::GPIOCommand::GPIO_W_DATAOUT, 0x4 /* length */, *initialReply.at(ohN));
        if (!restoreReply.at(ohN))
            continue;

        successMask |= (1ULL << ohN);
    }

    return successMask;
}

std::vector<std::optional<uint64_t>> gem::hardware::oh::getOHfpgaDNA::operator()(const uint32_t ohMask) const
{
    std::vector<std::optional<uint64_t>> fpgaDNA(amc::number_oh(), std::nullopt);

#if !defined(GEM_IS_ME0) // no OH FPGA on ME0
    const std::array<std::string, 2> regs = { "FPGA.CONTROL.DNA.DNA_MSBS", "FPGA.CONTROL.DNA.DNA_LSBS" };
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!((ohMask >> ohN) & 0x1))
            continue;

        try {
            const std::string regBase = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".";
            const uint64_t dna = static_cast<uint64_t>(utils::readReg(regBase + regs[0])) << 32 | utils::readReg(regBase + regs[1]);
            fpgaDNA[ohN] = dna;
        } catch (const xhal::memhub::memory_access_error& e) {
            LOG4CPLUS_WARN(logger, e.what());
            // fpgaDNA[ohN] is still std::nullopt, i.e. the expectation for a failed read
        }
    }
#endif

    return fpgaDNA;
}
