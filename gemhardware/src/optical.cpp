/// @file

#include "optical.h"

#include <memory>

#include <iostream>

namespace /* anonymous */ {

/// @brief Number of points for the optical power measurement
constexpr size_t optical_power_points = 100;

/// @brief Return the optical transceivers management object based for a given link
///
/// @warning The MiniPOD TX in charge of fibers 48-59 is returned also for fibers
///          fibers 36-47 and 60-71. This folded configuration is used because of
///          the asymmetry between the number of TX and RX fibers.
///
/// @param @c link_number Link number according to the board/firmware convention
std::unique_ptr<gem::hardware::optical::base_transceiver> get_transceiver(const uint16_t link_number)
{
    // I2C base addresses
    constexpr uint8_t cxp_slave_address_rx = 0x54;
    constexpr uint8_t cxp_slave_address_tx = 0x50;

    constexpr uint8_t mp_slave_address_rx = 0x30;
    constexpr uint8_t mp_slave_address_tx = 0x2c;

    if (link_number < 36) { // CXP transceivers
        const uint8_t cxp_idx = link_number / 12;
        gem::hardware::i2c_device rx_device("/dev/i2c-" + std::to_string(cxp_idx + 2), cxp_slave_address_rx);
        gem::hardware::i2c_device tx_device("/dev/i2c-" + std::to_string(cxp_idx + 2), cxp_slave_address_tx);
        return std::make_unique<gem::hardware::optical::cxp_transceiver>(std::move(rx_device), std::move(tx_device));
    } else { // MiniPOD transceivers
        const uint8_t mp_idx = (link_number - 36) / 12;
        gem::hardware::i2c_device rx_device("/dev/i2c-1", mp_slave_address_rx + mp_idx);
        gem::hardware::i2c_device tx_device("/dev/i2c-1", mp_slave_address_tx); // We have only one TX, "wrap" the fibers
        return std::make_unique<gem::hardware::optical::mp_transceiver>(std::move(rx_device), std::move(tx_device));
    }
}

uint8_t get_transceiver_channel(const uint16_t link_number)
{
    if (link_number < 36) { // CXP transceivers
        return link_number % 12;
    } else { // MiniPOD transceivers
        return 11 - ((link_number - 36) % 12); // The link number and the transceiver channels are "inverted"
    }
}

} // namespace anonymous

// cxp_transceiver

gem::hardware::optical::cxp_transceiver::cxp_transceiver(i2c_device&& rx_device, i2c_device&& tx_device)
    : m_rx_device(std::move(rx_device))
    , m_tx_device(std::move(tx_device))
{
}

gem::hardware::optical::cxp_transceiver::~cxp_transceiver()
{
}

uint16_t gem::hardware::optical::cxp_transceiver::measure_rx_power(const uint8_t channel)
{
    // From the CXP specifications
    //
    // Address: 206-229 (Upper Page 01h)
    // Name: Input Optical Power Monitor Tx11... Input Optical Power Monitor Tx00
    // Description: Per-channel Rx Light Input Monitor in 0.1μW units coded as 16 bit
    //     unsigned integer, Low byte within each pair is MSB. Tolerance is +/- 3 dB

    // Select Upper Page 01h
    m_rx_device.write(127, 1);

    uint32_t sum = 0;

    for (size_t i = 0; i < optical_power_points; ++i) {
        const uint16_t msb = m_rx_device.read(206 + (11 - channel) * 2);
        const uint16_t lsb = m_rx_device.read(207 + (11 - channel) * 2);
        sum += (msb << 8) + lsb;
    }

    return sum / optical_power_points;
}

uint16_t gem::hardware::optical::cxp_transceiver::measure_tx_power(const uint8_t channel)
{
    // From the CXP specifications
    //
    // Address: 206-229 (Upper Page 01h)
    // Name: Output Optical Power Monitor Tx11... Output Optical Power Monitor Tx00
    // Description: Per-channel Tx Light Output Monitor in 0.1μW units coded as 16 bit
    //     unsigned integer, Low byte within each pair is MSB. Tolerance is +/- 3 dB

    // Select Upper Page 01h
    m_tx_device.write(127, 1);

    uint32_t sum = 0;

    for (size_t i = 0; i < optical_power_points; ++i) {
        const uint16_t msb = m_tx_device.read(206 + (11 - channel) * 2);
        const uint16_t lsb = m_tx_device.read(207 + (11 - channel) * 2);
        sum += (msb << 8) + lsb;
    }

    return sum / optical_power_points;
}

// mp_transceiver

gem::hardware::optical::mp_transceiver::mp_transceiver(i2c_device&& rx_device, i2c_device&& tx_device)
    : m_rx_device(std::move(rx_device))
    , m_tx_device(std::move(tx_device))
{
}

gem::hardware::optical::mp_transceiver::~mp_transceiver()
{
}

uint16_t gem::hardware::optical::mp_transceiver::measure_rx_power(const uint8_t channel)
{
    // From the MiniPOD Datasheet
    //
    // Address: 64-87
    // Name: RX Optical Input, PAVE, Monitor Channel 11... RX Optical Input, PAVE, Monitor Channel 00
    // Description: Optical power in 0.1 mW units coded as 16 bit unsigned integer, Low byte within
    //     in pair is MSB. Tolerance is ±3 dB for -10 to +2.4 dBm range.

    uint32_t sum = 0;

    for (size_t i = 0; i < optical_power_points; ++i) {
        const uint16_t msb = m_rx_device.read(64 + (11 - channel) * 2);
        const uint16_t lsb = m_rx_device.read(65 + (11 - channel) * 2);
        sum += (msb << 8) + lsb;
    }

    return sum / optical_power_points;
}

uint16_t gem::hardware::optical::mp_transceiver::measure_tx_power(const uint8_t channel)
{
    // From the MiniPOD Datasheet
    //
    // Address: 64-87
    // Name: TX Light Output Monitor Channel 11... TX Light Output Monitor Channel 00
    // Description: Optical power in 0.1 mW units coded as 16 bit unsigned integer, Low byte within
    //     in pair is MSB. Tolerance is ±3 dB.

    uint32_t sum = 0;

    for (size_t i = 0; i < optical_power_points; ++i) {
        const uint16_t msb = m_tx_device.read(64 + (11 - channel) * 2);
        const uint16_t lsb = m_tx_device.read(65 + (11 - channel) * 2);
        sum += (msb << 8) + lsb;
    }

    return sum / optical_power_points;
}

// helper functions

uint16_t gem::hardware::optical::measure_rx_power(const uint16_t link_number)
{
    auto transceiver = get_transceiver(link_number);
    const auto channel = get_transceiver_channel(link_number);
    return transceiver->measure_rx_power(channel);
}

uint16_t gem::hardware::optical::measure_tx_power(const uint16_t link_number)
{
    auto transceiver = get_transceiver(link_number);
    const auto channel = get_transceiver_channel(link_number);
    return transceiver->measure_tx_power(channel);
}
