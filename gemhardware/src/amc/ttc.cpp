/// @file
/// @brief AMC TTC methods for RPC modules

#include <gem/hardware/amc/ttc.h>

#include "../utils.h"

void gem::hardware::amc::ttc::enableTTCGenerator::operator()(const bool enable) const
{
    if (enable) {
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.ENABLE", 0x1);
        utils::writeReg("BEFE.GEM.TTC.CTRL.CMD_ENABLE", 0x0);
    } else {
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.ENABLE", 0x0);
        utils::writeReg("BEFE.GEM.TTC.CTRL.CMD_ENABLE", 0x1);
    }
}

void gem::hardware::amc::ttc::sendSingleHardReset::operator()() const
{
    if (utils::readReg("BEFE.GEM.TTC.GENERATOR.ENABLE")) {
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.SINGLE_HARD_RESET", 0x1);
    } else {
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.ENABLE", 0x1);
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.SINGLE_HARD_RESET", 0x1);
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.ENABLE", 0x0);
    }
}

void gem::hardware::amc::ttc::sendSingleReSync::operator()() const
{
    if (utils::readReg("BEFE.GEM.TTC.GENERATOR.ENABLE")) {
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.SINGLE_RESYNC", 0x1);
    } else {
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.ENABLE", 0x1);
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.SINGLE_RESYNC", 0x1);
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.ENABLE", 0x0);
    }
}

void gem::hardware::amc::ttc::sendReSyncSequence::operator()() const
{
    if (utils::readReg("BEFE.GEM.TTC.GENERATOR.ENABLE")) {
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.SINGLE_RESYNC", 0x1);
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.SINGLE_EC0", 0x1);
    } else {
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.ENABLE", 0x1);
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.SINGLE_RESYNC", 0x1);
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.SINGLE_EC0", 0x1);
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.ENABLE", 0x0);
    }
}

void gem::hardware::amc::ttc::ttcModuleReset::operator()() const
{
    utils::writeReg("BEFE.GEM.TTC.CTRL.MODULE_RESET", 0x1);
}

void gem::hardware::amc::ttc::ttcMMCMReset::operator()() const
{
    utils::writeReg("BEFE.GEM.TTC.CTRL.MMCM_RESET", 0x1);
}

void gem::hardware::amc::ttc::ttcCounterReset::operator()() const
{
    utils::writeReg("BEFE.GEM.TTC.CTRL.CNT_RESET", 0x1);
}

bool gem::hardware::amc::ttc::getL1AEnable::operator()() const
{
    return utils::readReg("BEFE.GEM.TTC.CTRL.L1A_ENABLE");
}

void gem::hardware::amc::ttc::setL1AEnable::operator()(const bool enable) const
{
    utils::writeReg("BEFE.GEM.TTC.CTRL.L1A_ENABLE", enable);
}

uint32_t gem::hardware::amc::ttc::getL1AID::operator()() const
{
    return utils::readReg("BEFE.GEM.TTC.L1A_ID");
}

uint32_t gem::hardware::amc::ttc::getL1ARate::operator()() const
{
    return utils::readReg("BEFE.GEM.TTC.L1A_RATE");
}
