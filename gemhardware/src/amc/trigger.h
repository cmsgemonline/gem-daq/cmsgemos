/// @file

#ifndef SRC_GEM_HARDWARE_AMC_TRIGGER_H
#define SRC_GEM_HARDWARE_AMC_TRIGGER_H

#include <gem/hardware/amc/trigger.h>

#include <vector>

namespace gem {
namespace hardware {
    namespace amc {
        namespace trigger {

            /// @brief Decodes a raw S-bit cluster into a @c clusterInfo structure
            clusterInfo decodeCluster(uint16_t rawCluster);

            /// @brief Configures and acquires S-bit clusters via the cluster DAQ
            ///
            /// @param @c ohN OptoHybrid number
            /// @param @c windowWidth Width of the readout window (the trigger position can freely be configured via @c triggerPosition)
            /// @param @c triggerPosition Position of the trigger within the readout window
            /// @param @c nTriggers Number of triggers to acquire
            /// @param @c triggerOnCluster Whether or not clusters should trigger the cluster DAQ
            /// @param @c triggerOnL1A Whether or not L1A should trigger the cluster DAQ
            ///
            /// @return The valid clusters in the readout window for all triggers.
            ///         The first (second) index encodes the trigger (cluster) number
            std::vector<std::vector<clusterInfo>> readoutClusterDAQ(
                uint8_t ohN,
                uint16_t windowWidth,
                uint16_t triggerPosition,
                unsigned nTriggers = 1,
                bool triggerOnCluster = true,
                bool triggerOnL1A = false);

        } // namespace gem::hardware::amc::trigger
    } // namespace gem::hardware::amc
} // namespace gem::hardware
} // namespace gem

#endif // SRC_GEM_HARDWARE_AMC_TRIGGER_H
