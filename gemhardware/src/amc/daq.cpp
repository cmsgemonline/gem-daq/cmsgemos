/// @file
/// @brief AMC DAQ methods for RPC modules

#include <gem/hardware/amc/daq.h>

#include "../hw_constants.h"
#include "../utils.h"

#include <fmt/format.h>

void gem::hardware::amc::daq::configureFedID::operator()(uint32_t fedID, const uint8_t slot) const
{
#if defined(GEM_IS_CTP7)
    if ((fedID >> 12))
        throw std::runtime_error(fmt::format(FMT_STRING("Invalid FED number ({:d}) for µTCA-based system. It must be a 12-bits number."), fedID));

    fedID = (uint32_t(slot) << 12) | (fedID & 0xfff);
#else
    if (slot)
        throw std::runtime_error(fmt::format(FMT_STRING("The slot number is expected to be 0 on the non µTCA systems (currently set to {:d})."), slot));
#endif

    utils::writeReg("BEFE.GEM.DAQ.CONTROL.FED_ID", fedID);
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.SPY.SPY_DEST_MAC_2", fedID >> 16);
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.SPY.SPY_DEST_MAC_3", fedID & 0xffff);
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.SPY.SPY_SOURCE_MAC_2", fedID >> 16);
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.SPY.SPY_SOURCE_MAC_3", fedID & 0xffff);
}

void gem::hardware::amc::daq::enableDAQ::operator()(const bool enable) const
{
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.DAQ_ENABLE", enable);
}

void gem::hardware::amc::daq::setZS::operator()(const bool enable) const
{
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.ZERO_SUPPRESSION_EN", enable);
}

void gem::hardware::amc::daq::disableZS::operator()() const
{
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.ZERO_SUPPRESSION_EN", 0x0);
}

void gem::hardware::amc::daq::resetDAQ::operator()(const bool wait_for_resync) const
{
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.RESET_TILL_RESYNC", wait_for_resync);

    // Notice, we perform a level reset
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.RESET", 0x1);
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.RESET", 0x0);
}

uint32_t gem::hardware::amc::daq::getDAQLinkControl::operator()() const
{
    LOG4CPLUS_WARN(logger, "getDAQLinkControl not implemented");
    return 0x0;
}

uint32_t gem::hardware::amc::daq::getDAQLinkStatus::operator()() const
{
    LOG4CPLUS_WARN(logger, "getDAQLinkStatus not implemented");
    return 0x0;
}

bool gem::hardware::amc::daq::daqLinkReady::operator()() const
{
    return static_cast<bool>(utils::readReg("BEFE.GEM.DAQ.STATUS.DAQ_LINK_RDY"));
}

bool gem::hardware::amc::daq::daqClockLocked::operator()() const
{
    return static_cast<bool>(utils::readReg("BEFE.GEM.DAQ.STATUS.DAQ_CLK_LOCKED"));
}

bool gem::hardware::amc::daq::daqTTCReady::operator()() const
{
    return static_cast<bool>(utils::readReg("BEFE.GEM.DAQ.STATUS.TTC_RDY"));
}

uint8_t gem::hardware::amc::daq::daqTTSState::operator()() const
{
    return static_cast<uint8_t>(utils::readReg("BEFE.GEM.DAQ.STATUS.TTS_STATE"));
}

bool gem::hardware::amc::daq::daqAlmostFull::operator()() const
{
    return static_cast<bool>(utils::readReg("BEFE.GEM.DAQ.STATUS.DAQ_AFULL"));
}

bool gem::hardware::amc::daq::l1aFIFOIsEmpty::operator()() const
{
    return static_cast<bool>(utils::readReg("BEFE.GEM.DAQ.STATUS.L1A_FIFO_IS_EMPTY"));
}

bool gem::hardware::amc::daq::l1aFIFOIsAlmostFull::operator()() const
{
    return static_cast<bool>(utils::readReg("BEFE.GEM.DAQ.STATUS.L1A_FIFO_IS_NEAR_FULL"));
}

bool gem::hardware::amc::daq::l1aFIFOIsFull::operator()() const
{
    return static_cast<bool>(utils::readReg("BEFE.GEM.DAQ.STATUS.L1A_FIFO_IS_FULL"));
}

bool gem::hardware::amc::daq::l1aFIFOIsUnderflow::operator()() const
{
    return static_cast<bool>(utils::readReg("BEFE.GEM.DAQ.STATUS.L1A_FIFO_IS_UNDERFLOW"));
}

uint32_t gem::hardware::amc::daq::getDAQLinkEventsSent::operator()() const
{
    return utils::readReg("BEFE.GEM.DAQ.STATUS.EVT_SENT");
}

uint32_t gem::hardware::amc::daq::getDAQLinkL1AID::operator()() const
{
    return utils::readReg("BEFE.GEM.DAQ.STATUS.L1AID");
}

uint32_t gem::hardware::amc::daq::getDAQLinkL1ARate::operator()() const
{
    LOG4CPLUS_WARN(logger, "getDAQLinkL1ARate not implemented");
    return 0x0;
}

uint32_t gem::hardware::amc::daq::getDAQLinkDisperErrors::operator()() const
{
    return utils::readReg("BEFE.GEM.DAQ.STATUS.DISPER_ERR");
}

uint32_t gem::hardware::amc::daq::getDAQLinkNonidentifiableErrors::operator()() const
{
    return utils::readReg("BEFE.GEM.DAQ.STATUS.NOTINTABLE_ERR");
}

uint32_t gem::hardware::amc::daq::getDAQLinkInputMask::operator()() const
{
    return utils::readReg("BEFE.GEM.DAQ.CONTROL.INPUT_ENABLE_MASK") & full_oh_mask();
}

void gem::hardware::amc::daq::setDAQLinkInputMask::operator()(const uint32_t optohybridMask) const
{
    return utils::writeReg("BEFE.GEM.DAQ.CONTROL.INPUT_ENABLE_MASK", optohybridMask & full_oh_mask());
}

uint32_t gem::hardware::amc::daq::getDAQLinkDAVTimeout::operator()() const
{
    return utils::readReg("BEFE.GEM.DAQ.CONTROL.DAV_TIMEOUT");
}

uint32_t gem::hardware::amc::daq::getDAQLinkDAVTimer::operator()(bool const& max) const
{
    uint32_t maxtimer = utils::readReg("BEFE.GEM.DAQ.STATUS.MAX_DAV_TIMER");
    uint32_t lasttimer = utils::readReg("BEFE.GEM.DAQ.STATUS.LAST_DAV_TIMER");

    if (max)
        return maxtimer;
    else
        return lasttimer;
}

uint32_t gem::hardware::amc::daq::getLinkDAQStatus::operator()(const uint8_t& gtx) const
{
    LOG4CPLUS_WARN(logger, "getLinkDAQStatus not implemented");
    return 0x0;
}

uint32_t gem::hardware::amc::daq::getLinkDAQCounters::operator()(const uint8_t& gtx, const uint8_t& mode) const
{
    LOG4CPLUS_WARN(logger, "getLinkDAQCounters not implemented");
    return 0x0;
}

uint32_t gem::hardware::amc::daq::getLinkLastDAQBlock::operator()(const uint8_t& gtx) const
{
    LOG4CPLUS_WARN(logger, "getLinkLastDAQBlock not implemented");
    return 0x0;
}

uint32_t gem::hardware::amc::daq::getDAQLinkInputTimeout::operator()() const
{
    return utils::readReg("BEFE.GEM.DAQ.CONTROL.INPUT_TIMEOUT");
}

uint32_t gem::hardware::amc::daq::getRunType::operator()() const
{
    return utils::readReg("BEFE.GEM.DAQ.CONTROL.RUN_TYPE");
}

uint32_t gem::hardware::amc::daq::getRunParameters::operator()() const
{
    return utils::readReg("BEFE.GEM.DAQ.CONTROL.RUN_PARAMS");
}

void gem::hardware::amc::daq::setDAQLinkInputTimeout::operator()(const uint32_t& inputTO) const
{
    LOG4CPLUS_WARN(logger, "setDAQLinkInputTimeout not implemented");
}

void gem::hardware::amc::daq::setRunType::operator()(const uint32_t& rtype) const
{
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.RUN_TYPE", rtype);
}

void gem::hardware::amc::daq::setRunParameters::operator()(const uint32_t& rparams) const
{
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.RUN_PARAMS", rparams);
}

void gem::hardware::amc::daq::configureDAQModule::operator()(const bool enable_daqlink, const uint32_t local_readout_prescale, const uint32_t inputEnableMask, const bool enableZS) const
{
    amc::daq::enableDAQ {}(false);
    amc::daq::setZS {}(enableZS);

    utils::writeReg("BEFE.GEM.DAQ.CONTROL.IGNORE_DAQLINK", !enable_daqlink);
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.INPUT_ENABLE_MASK", inputEnableMask);

    if (local_readout_prescale == DISABLE_LOCAL_READOUT)
        utils::writeReg("BEFE.GEM.DAQ.CONTROL.SPY.SPY_PRESCALE", 0);
    else if (local_readout_prescale != ENABLE_LOCAL_READOUT)
        utils::writeReg("BEFE.GEM.DAQ.CONTROL.SPY.SPY_PRESCALE", local_readout_prescale);

    // Clear the run type and run parameters
    // I.e. "invalid" events
    amc::daq::setRunType {}(0);
    amc::daq::setRunParameters {}(0);

    amc::daq::resetDAQ {}(false);
}

void gem::hardware::amc::daq::configureAMCCalDataFormat::operator()(const bool enable, const uint8_t channel) const
{
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.CALIBRATION_MODE_CHAN", channel);
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.CALIBRATION_MODE_EN", static_cast<uint32_t>(enable));
}
