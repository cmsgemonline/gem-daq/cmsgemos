/// @file

#include "trigger.h"

#include "../hw_constants.h"
#include "../utils.h"

#include <fmt/format.h>

#include <chrono>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>

namespace /* anonymous */ {

// FIXME: Take me from the firmware
const std::map<uint8_t, std::pair<uint8_t, uint8_t>> ctp7_ge11_trigger_links_map = {
    { 0, { 62, 63 } },
    { 1, { 61, 60 } },
    { 2, { 59, 58 } },
    { 3, { 57, 56 } },
    { 4, { 54, 55 } },
    { 5, { 52, 53 } },
    { 6, { 50, 51 } },
    { 7, { 49, 48 } },
    { 8, { 47, 46 } },
    { 9, { 45, 44 } },
    { 10, { 41, 37 } },
    { 11, { 38, 39 } },
};

/// @brief Maximum number of clusters produced in one BX
constexpr size_t CLUSTERS_PER_BX = 8;

/// @brief Specifies how many BX are stored in the cluster DAQ FIFO
constexpr uint16_t CLUSTER_DAQ_DEPTH = 512;

/// @brief Specifies the cluster DAQ FIFO latency (in BX)
///
/// In other terms, how many BX are written into the FIFO after it triggers.
constexpr uint16_t CLUSTER_DAQ_LATENCY = 3;

} // anonymous namespace

// local functions

gem::hardware::amc::trigger::clusterInfo gem::hardware::amc::trigger::decodeCluster(const uint16_t rawCluster)
{
    clusterInfo cluster { 0 };
    cluster.pad = rawCluster & 0xff;
    cluster.partition = (rawCluster >> 8) & 0x7;
    cluster.size = ((rawCluster >> 12) & 0x7) + 1;
    cluster.valid = (cluster.pad != 0xff);
    cluster.l1a = (rawCluster >> 15) & 0x1;
    return cluster;
}

std::vector<std::vector<gem::hardware::amc::trigger::clusterInfo>> gem::hardware::amc::trigger::readoutClusterDAQ(
    const uint8_t ohN,
    const uint16_t windowWidth,
    const uint16_t triggerPosition,
    const unsigned nTriggers,
    const bool triggerOnCluster,
    const bool triggerOnL1A)
{
    std::vector<std::vector<clusterInfo>> result;

    // Sanity checks
    if ((windowWidth < 1) || (windowWidth > CLUSTER_DAQ_DEPTH))
        throw std::out_of_range(fmt::format(FMT_STRING("Window width must in the range [1, {:d}]"), CLUSTER_DAQ_DEPTH));

    if ((triggerPosition >= windowWidth) || (triggerPosition > CLUSTER_DAQ_DEPTH - CLUSTER_DAQ_LATENCY))
        throw std::out_of_range(fmt::format(
            FMT_STRING("Trigger position must be within the readout window and in the range [0, {:d}]"),
            CLUSTER_DAQ_DEPTH - CLUSTER_DAQ_LATENCY));

    // Cluster DAQ configuration
    utils::writeReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.OH_SELECT", ohN);
    utils::writeReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.FIFO_TRIGGER_DELAY", CLUSTER_DAQ_DEPTH - CLUSTER_DAQ_LATENCY - triggerPosition);
    utils::writeReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.FIFO_EN_L1A_TRIGGER", triggerOnL1A);
    utils::writeReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.FIFO_EN_SBIT_TRIGGER", triggerOnCluster);

    for (unsigned iTrigger = 0; iTrigger < nTriggers; ++iTrigger) {
        std::vector<clusterInfo> t_clusters;

        // Wait until the cluster DAQ triggers
        utils::writeReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.RESET", 1);
        while (utils::readReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.FIFO_EMPTY"))
            std::this_thread::sleep_for(std::chrono::milliseconds(5));

        // Readout the desired window
        for (uint16_t iSample = 0; iSample < 4 * windowWidth; ++iSample) {
            const auto rawClusters = utils::readReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.FIFO_DATA");

            auto cluster1 = decodeCluster(rawClusters & 0xffff);
            cluster1.bx = iSample / 4 - triggerPosition;
            if (cluster1.valid)
                t_clusters.push_back(cluster1);

            auto cluster2 = decodeCluster((rawClusters >> 16) & 0xffff);
            cluster2.bx = iSample / 4 - triggerPosition;
            if (cluster2.valid)
                t_clusters.push_back(cluster2);
        }

        result.push_back(t_clusters);
    }

    return result;
}

// remote functions

void gem::hardware::amc::trigger::enableLinksToEMTF::operator()(const uint32_t optohybridMask) const
{
    utils::writeReg("BEFE.GEM.TRIGGER.CTRL.OH_KILL_MASK", ~optohybridMask);
}

uint32_t gem::hardware::amc::trigger::getEnabledLinksToEMTF::operator()() const
{
    return ~utils::readReg("BEFE.GEM.TRIGGER.CTRL.OH_KILL_MASK") & amc::full_oh_mask();
}

uint32_t gem::hardware::amc::trigger::getValidLinks::operator()(const uint32_t optohybridMask, const bool reset) const
{
    uint32_t validLinks = optohybridMask;

#if defined(GEM_IS_CTP7) && defined(GEM_IS_GE11)
    const std::vector<std::string> status_registers = { "NOT_IN_TABLE_CNT", "OVERFLOW_CNT", "UNDERFLOW_CNT", "MISSED_COMMA_CNT" };

    size_t attempt = 0;
    do {
        validLinks = optohybridMask;

        // Reset the links and wait 1 orbit (3564 BX) for the 8b10b comma word to be seen
        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!((optohybridMask >> ohN) & 0x1))
                continue;

            utils::writeReg(fmt::format(FMT_STRING("BEFE.GEM.OH_LINKS.OH{:d}.TRIGGER_LINK_RESET"), ohN), 1);
        }
        std::this_thread::sleep_for(std::chrono::nanoseconds(25) * 4000);

        // Wait for the errors to build up
        utils::writeReg("BEFE.GEM.TRIGGER.CTRL.RX_LINK_CNT_RESET", optohybridMask);
        std::this_thread::sleep_for(std::chrono::milliseconds(50));

        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!((optohybridMask >> ohN) & 0x1))
                continue;

            for (size_t linkN = 0; linkN < 2; ++linkN) {
                // Check the link status
                bool valid = true;
                for (const auto& r : status_registers) {
                    const auto value = utils::readReg(fmt::format(FMT_STRING("BEFE.GEM.TRIGGER.OH{:d}.LINK{:d}_{:s}"), ohN, linkN, r));
                    LOG4CPLUS_DEBUG(logger, fmt::format(FMT_STRING("Value of BEFE.GEM.TRIGGER.OH{:d}.LINK{:d}_{:s} is {:d}"), ohN, linkN, r, value));

                    if (value != 0)
                        valid = false;
                }

                // Mark the link as bad
                if (!valid)
                    validLinks &= ~(1ULL << ohN);

                // Reset the link, if requested
                if (reset && !valid) {
                    uint8_t mgtN = (linkN == 0) ? ctp7_ge11_trigger_links_map.at(ohN).first : ctp7_ge11_trigger_links_map.at(ohN).second;
                    utils::writeReg(fmt::format(FMT_STRING("BEFE.MGTS.MGT{}.RESET.RX_RESET"), mgtN), 1);
                }
            }
        }

        if (validLinks == optohybridMask)
            break;

        LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Not all trigger links valid {:#x} (target {:#x}) ({:d} out of {:d})"), validLinks, optohybridMask, attempt + 1, 10));
    } while (reset && (++attempt < 10));
#endif

    LOG4CPLUS_INFO(logger, fmt::format(FMT_STRING("Valid trigger links {:#x} (target {:#x})"), validLinks, optohybridMask));

    return validLinks;
}

std::vector<std::vector<gem::hardware::amc::trigger::clusterInfo>> gem::hardware::amc::trigger::readoutSbitMonitor::operator()(const uint8_t ohN, const uint32_t acquireTime) const
{
    std::vector<std::vector<clusterInfo>> result;

    // Register cache optimization
    const utils::RegInfo sbitMonResetReg = utils::getReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.RESET");
    const utils::RegInfo sbitMonL1ADelayReg = utils::getReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.L1A_DELAY");
    utils::RegInfo sbitMonCluster[CLUSTERS_PER_BX];
    for (size_t iCluster = 0; iCluster < CLUSTERS_PER_BX; ++iCluster)
        sbitMonCluster[iCluster] = utils::getReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.CLUSTER" + std::to_string(iCluster));

    utils::writeReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.OH_SELECT", ohN);

    for (time_t startTime = time(nullptr);
         uint32_t(difftime(time(nullptr), startTime)) <= acquireTime;) {
        // Wait until the S-bit monitor has triggered
        //
        // Note how we use the fact that the L1A delay register is reset to 0 and
        // cannot take this value once triggered (even in case of simultaneous L1A)
        utils::writeReg(sbitMonResetReg, 0x1);
        while (!utils::readReg(sbitMonL1ADelayReg))
            std::this_thread::sleep_for(std::chrono::milliseconds(5));

        // Wait for 4095 clock cycles then read the L1A delay
        std::this_thread::sleep_for(std::chrono::nanoseconds(0xfff * 25));
        uint32_t l1ADelay = utils::readReg(sbitMonL1ADelayReg);
        if (l1ADelay > 0xfff) {
            l1ADelay = 0xfff;
        }

        // Get the clusters
        std::vector<clusterInfo> clusters;
        for (size_t iCluster = 0; iCluster < CLUSTERS_PER_BX; ++iCluster) {
            const auto rawCluster = utils::readReg(sbitMonCluster[iCluster]);
            const auto cluster = decodeCluster(rawCluster);
            if (cluster.valid)
                clusters.push_back(cluster);
        }

        result.push_back(clusters);
    }

    return result;
}
