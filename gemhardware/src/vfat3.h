/// @file

#ifndef SRC_GEM_HARDWARE_VFAT3_H
#define SRC_GEM_HARDWARE_VFAT3_H

#include <gem/hardware/vfat3.h>

#include "hw_constants.h"
#include "utils.h"

#include <array>
#include <optional>
#include <stdint.h>
#include <vector>

namespace gem {
namespace hardware {
    namespace vfat3 {

        /// @brief Decode a Reed--Muller encoded VFAT3 ChipID
        ///
        /// @param @c encChipID 32-bit encoded chip ID to decode
        ///
        /// @returns decoded VFAT3 chip ID
        uint16_t decodeChipID(uint32_t encChipID);

        /// @brief Mask or unmask the channel(s) of interest
        ///
        /// @warning This method does not throw on register access errors.
        ///          Check the return value to avoid ignoring errors silently.
        ///
        /// @param @c ohN OptoHybrid link number
        /// @param @c vfat_mask VFAT mask
        /// @param @c channel Channel of interest (use @c gem::hardware::vfat3::VFAT_ALL_CHANNELS to target all channels)
        /// @param @c enable Whether to mask (enable) or unmask (disable) the channel of interest
        ///
        /// @returns The VFAT mask for which the masking was fully successful
        uint32_t enableChannel(uint8_t ohN, uint32_t vfat_mask, uint8_t channel = VFAT_ALL_CHANNELS, bool enable = true);

        /// @brief Set the trimming parameters for the channel(s) of interest
        ///
        /// While the triming parameters are actually two registers per channel in the VFAT3,
        /// @c ARM_TRIM_POLARITY and @c ARM_TRIM_AMPLITUDE, this method takes a single @c trimming
        /// argument. The polarity and amplitutde are internally derived and writen to the appropriate
        /// registers.
        ///
        /// @warning This method does not throw on register access errors.
        ///          Check the return value to avoid ignoring errors silently.
        ///
        /// @param @c ohN OptoHybrid link number
        /// @param @c vfat_mask VFAT mask
        /// @param @c channel Channel of interest (use @c gem::hardware::vfat3::VFAT_ALL_CHANNELS to target all channels)
        /// @param @c trimming Signed trimming value (see description for more details)
        ///
        /// @returns The VFAT mask for which setting the trimming parameters was fully successful
        uint32_t setTrimmingValue(uint8_t ohN, uint32_t vfat_mask, uint8_t channel = VFAT_ALL_CHANNELS, int16_t trimming = 0);

        /// @brief Prepare the structure needed for the @c doBroadcastWrite and @c doBroadcastRead functions
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c register_name VFAT register name (as in the address table)
        ///
        /// @returns The structure to be passed to the @c doBroadcastWrite and @c doBroadcastRead functions
        std::array<utils::RegInfo, oh::VFATS_PER_OH> prepareBroadcast(uint32_t ohN, const std::string& register_name);

        /// @brief Performs a write transaction on a specified VFAT registers
        ///
        /// Consider using the @c broadcastWrite method in non-critical sections.
        ///
        /// @warning This method does not throw on register access errors.
        ///          Check the return value to avoid ignoring errors silently.
        ///
        /// @param @c registers Registers to write (can be prepared with the @c prepareBroadcast function)
        /// @param @c vfat_mask VFAT mask
        /// @param @c value Value to write
        ///
        /// @returns The VFAT mask for which the writes were successful
        uint32_t doBroadcastWrite(const std::array<utils::RegInfo, oh::VFATS_PER_OH>& registers, uint32_t vfat_mask, uint32_t value);

        /// @brief Performs a read transaction on a specified VFAT registers
        ///
        /// Consider using the @c broadcastRead method in non-critical sections.
        //
        /// @warning This method does not throw on register access errors.
        ///          In case of failed reads - or masked VFAT -, the @c std::nullopt
        ///          value is used as as placeholder, indicating that no value was
        ///          successfully read.
        ///
        /// @param @c registers Registers to read (can be prepared with the @c prepareBroadcast function)
        /// @param @c vfat_mask VFAT mask
        ///
        /// @returns @c A vector holding the results of the read transactions, if successful
        std::vector<std::optional<uint32_t>> doBroadcastRead(const std::array<utils::RegInfo, oh::VFATS_PER_OH>& registers, const uint32_t vfat_mask);

    } // namespace gem::hardware::vfat3
} // namespace gem::hardware
} // namespace gem

#endif
