/// @file

#include "libi2c.h"

#include <fmt/format.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <sys/ioctl.h>

#include <fcntl.h>
#include <stdexcept>
#include <unistd.h>

gem::hardware::i2c_device::i2c_device(const std::string& controller_filename, const uint16_t slave_address)
    : m_slave_address(slave_address)
{
    m_controller_fd = open(controller_filename.data(), O_RDWR);
    if (m_controller_fd < 0)
        throw std::runtime_error(fmt::format(FMT_STRING("Cannot open {:s}"), controller_filename));
}

gem::hardware::i2c_device::i2c_device(i2c_device&& other)
{
    *this = std::move(other);
}

gem::hardware::i2c_device& gem::hardware::i2c_device::operator=(i2c_device&& other)
{
    // Swap the file descriptors so *this will be closed during destruction
    std::swap(this->m_controller_fd, other.m_controller_fd);

    this->m_slave_address = other.m_slave_address;

    return *this;
}

gem::hardware::i2c_device::~i2c_device()
{
    close(m_controller_fd);
}

uint8_t gem::hardware::i2c_device::read(const uint8_t address)
{
    uint8_t outbuf = address;
    uint8_t inbuf = 0;

    struct i2c_msg ioctl_msgs[2] = { 0 };

    // Select the register
    ioctl_msgs[0].addr = m_slave_address;
    ioctl_msgs[0].flags = 0;
    ioctl_msgs[0].len = 1;
    ioctl_msgs[0].buf = &outbuf;

    // Read the register value
    ioctl_msgs[1].addr = m_slave_address;
    ioctl_msgs[1].flags = I2C_M_RD;
    ioctl_msgs[1].len = 1;
    ioctl_msgs[1].buf = &inbuf;

    struct i2c_rdwr_ioctl_data ioctl_data = { 0 };
    ioctl_data.msgs = ioctl_msgs;
    ioctl_data.nmsgs = 2;

    if (ioctl(m_controller_fd, I2C_RDWR, &ioctl_data) < 0)
        throw std::runtime_error(fmt::format(FMT_STRING("Failed to read {:d} with ioctl(I2C_RDWR)"), address));

    return inbuf;
}

void gem::hardware::i2c_device::write(const uint8_t address, const uint8_t value)
{
    uint8_t outbuf[2] = { address, value };

    // Select the register, then write its value
    struct i2c_msg ioctl_msg = { 0 };
    ioctl_msg.addr = m_slave_address;
    ioctl_msg.flags = 0;
    ioctl_msg.len = 2;
    ioctl_msg.buf = outbuf;

    struct i2c_rdwr_ioctl_data ioctl_data = { 0 };
    ioctl_data.msgs = &ioctl_msg;
    ioctl_data.nmsgs = 1;

    if (ioctl(m_controller_fd, I2C_RDWR, &ioctl_data) < 0)
        throw std::runtime_error(fmt::format(FMT_STRING("Failed to write {:d} to {:d} with ioctl(I2C_RDWR)"), value, address));
}
