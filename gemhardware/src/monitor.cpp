/// @file
/// @brief Contains functions for hardware monitoring

#include <gem/hardware/monitor.h>

#include "exception.h"
#include "gbt.h"
#include "hw_constants.h"
#include "lpgbt.h"
#include "optohybrid.h"
#include "sca.h"
#include "utils.h"
#include "vfat3.h"

#include <chrono>
#include <cmath>
#include <cstddef>
#include <fstream>
#include <thread>
#include <vector>

std::map<std::string, std::variant<uint32_t, double>> gem::hardware::monitor::getTTCMain::operator()() const
{
    std::map<std::string, std::variant<uint32_t, double>> ttcMon;

    ttcMon["BC0_LOCKED"] = utils::readReg("BEFE.GEM.TTC.STATUS.BC0.LOCKED");
    ttcMon["BC0_UNLOCK_CNT"] = utils::readReg("BEFE.GEM.TTC.STATUS.BC0.UNLOCK_CNT");
    ttcMon["CMD_ENABLED"] = utils::readReg("BEFE.GEM.TTC.CTRL.CMD_ENABLE");
    ttcMon["GENERATOR_ENABLED"] = utils::readReg("BEFE.GEM.TTC.GENERATOR.ENABLE");
    ttcMon["L1A_ENABLED"] = utils::readReg("BEFE.GEM.TTC.CTRL.L1A_ENABLE");
    ttcMon["L1A_ID"] = utils::readReg("BEFE.GEM.TTC.L1A_ID");
    ttcMon["L1A_RATE"] = utils::readReg("BEFE.GEM.TTC.L1A_RATE");
    ttcMon["MMCM_LOCKED"] = utils::readReg("BEFE.GEM.TTC.STATUS.CLK.MMCM_LOCKED");
    ttcMon["MMCM_UNLOCK_CNT"] = utils::readReg("BEFE.GEM.TTC.STATUS.CLK.MMCM_UNLOCK_CNT");
    uint32_t phase = utils::readReg("BEFE.GEM.TTC.STATUS.CLK.PHASE_MONITOR.PHASE");
    ttcMon["PHASE"] = double(phase) * (25000. / 13417.);
    ttcMon["PHASE_JUMP_CNT"] = utils::readReg("BEFE.GEM.TTC.STATUS.CLK.PHASE_MONITOR.PHASE_JUMP_CNT");
    ttcMon["PHASE_LOCKED"] = utils::readReg("BEFE.GEM.TTC.STATUS.CLK.PHASE_LOCKED");
    ttcMon["PHASE_UNLOCK_CNT"] = utils::readReg("BEFE.GEM.TTC.STATUS.CLK.PHASE_UNLOCK_CNT");
    ttcMon["TTC_DOUBLE_ERROR_CNT"] = utils::readReg("BEFE.GEM.TTC.STATUS.TTC_DOUBLE_ERROR_CNT");
    ttcMon["TTC_SINGLE_ERROR_CNT"] = utils::readReg("BEFE.GEM.TTC.STATUS.TTC_SINGLE_ERROR_CNT");

    return ttcMon;
}

std::map<std::string, uint32_t> gem::hardware::monitor::getTriggerMain::operator()() const
{

    std::map<std::string, uint32_t> trigMon;

    trigMon["OR_TRIGGER_RATE"] = utils::readReg("BEFE.GEM.TRIGGER.STATUS.OR_TRIGGER_RATE");

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        const std::string keyPrefix = "OH" + std::to_string(ohN) + ".";
        const std::string regPrefix = "BEFE.GEM.TRIGGER.OH" + std::to_string(ohN) + ".";
        trigMon[keyPrefix + "LINK0_CLUSTER_OVERFLOW_CNT"] = utils::readReg(regPrefix + "LINK0_SBIT_OVERFLOW_CNT");
        trigMon[keyPrefix + "LINK1_CLUSTER_OVERFLOW_CNT"] = utils::readReg(regPrefix + "LINK1_SBIT_OVERFLOW_CNT");
        trigMon[keyPrefix + "TRIGGER_RATE"] = utils::readReg(regPrefix + "TRIGGER_RATE");
    }

    return trigMon;
}

std::map<std::string, uint32_t> gem::hardware::monitor::getTriggerFE::operator()(const uint32_t ohMask) const
{
    std::map<std::string, uint32_t> trigMon;

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!((ohMask >> ohN) & 0x1))
            continue;

        const std::string keyPrefix = "OH" + std::to_string(ohN) + ".";
        const std::string regPrefix = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".";

        trigMon[keyPrefix + "FPGA.CLUSTER_RATE"] = utils::readReg(regPrefix + "FPGA.CONTROL.SBITS.CLUSTER_RATE");
        trigMon[keyPrefix + "FPGA.TRIGGER_RATE"] = utils::readReg(regPrefix + "FPGA.TRIG.CNT.CLUSTER_COUNT");
        trigMon[keyPrefix + "FPGA.CLUSTER_OVERFLOW_CNT"] = utils::readReg(regPrefix + "FPGA.TRIG.CTRL.CNT_OVERFLOW");
    }

    return trigMon;
}

std::map<std::string, uint32_t> gem::hardware::monitor::getTriggerBE::operator()() const
{
    std::map<std::string, uint32_t> trigMon;

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        const std::string keyPrefix = "OH" + std::to_string(ohN) + ".";
        const std::string regPrefix = "BEFE.GEM.TRIGGER.OH" + std::to_string(ohN) + ".";

        trigMon[keyPrefix + "LINK0_NOT_IN_TABLE_CNT"] = utils::readReg(regPrefix + "LINK0_NOT_IN_TABLE_CNT");
        trigMon[keyPrefix + "LINK1_NOT_IN_TABLE_CNT"] = utils::readReg(regPrefix + "LINK1_NOT_IN_TABLE_CNT");
        trigMon[keyPrefix + "LINK0_CRC_ERROR_CNT"] = utils::readReg(regPrefix + "LINK0_CRC_ERR_CNT");
        trigMon[keyPrefix + "LINK1_CRC_ERROR_CNT"] = utils::readReg(regPrefix + "LINK1_CRC_ERR_CNT");
        trigMon[keyPrefix + "LINK0_MISSED_MARKER_CNT"] = utils::readReg(regPrefix + "LINK0_MISSED_COMMA_CNT");
        trigMon[keyPrefix + "LINK1_MISSED_MARKER_CNT"] = utils::readReg(regPrefix + "LINK1_MISSED_COMMA_CNT");
        trigMon[keyPrefix + "BC0_MISALIGN_CNT"] = utils::readReg(regPrefix + "BC0_MISALIGN_CNT");
    }

    return trigMon;
}

std::map<std::string, std::variant<uint32_t, double>> gem::hardware::monitor::getDAQMain::operator()() const
{
    std::map<std::string, std::variant<uint32_t, double>> daqMon;

    daqMon["DAQ_ENABLE"] = utils::readReg("BEFE.GEM.DAQ.CONTROL.DAQ_ENABLE");
    daqMon["INPUT_ENABLE_MASK"] = utils::readReg("BEFE.GEM.DAQ.CONTROL.INPUT_ENABLE_MASK");
    daqMon["TTS_STATE"] = utils::readReg("BEFE.GEM.DAQ.STATUS.TTS_STATE");
    daqMon["BACKPRESSURE"] = utils::readReg("BEFE.GEM.DAQ.STATUS.DAQ_BACKPRESSURE");
    daqMon["DAQ_LINK_READY"] = utils::readReg("BEFE.GEM.DAQ.STATUS.DAQ_LINK_RDY");
    daqMon["DAQ_EVENTS_SENT"] = utils::readReg("BEFE.GEM.DAQ.STATUS.EVT_SENT");
    daqMon["DAQ_THROUGHPUT"] = double(utils::readReg("BEFE.GEM.DAQ.STATUS.DAQ_WORD_RATE")) * 64 / 1024 / 1024; // Convert word rate to Mbps
    daqMon["DAQ_OFIFO_DATA_CNT"] = utils::readReg("BEFE.GEM.DAQ.STATUS.DAQ_FIFO_DATA_CNT");
    daqMon["DAQ_OFIFO_NEAR_FULL_CNT"] = utils::readReg("BEFE.GEM.DAQ.STATUS.DAQ_FIFO_NEAR_FULL_CNT");
    daqMon["DAQ_OFIFO_HAD_OFLOW"] = utils::readReg("BEFE.GEM.DAQ.STATUS.DAQ_OUTPUT_FIFO_HAD_OVERFLOW");
    daqMon["SPY_EVENTS_SENT"] = utils::readReg("BEFE.GEM.DAQ.STATUS.SPY.SPY_EVENTS_SENT");
    daqMon["SPY_THROUGHPUT"] = double(utils::readReg("BEFE.GEM.DAQ.STATUS.SPY.SPY_WORD_RATE")) * 16 / 1024 / 1024; // Convert word rate to Mbps
    daqMon["SPY_FIFO_AFULL_CNT"] = utils::readReg("BEFE.GEM.DAQ.STATUS.SPY.SPY_FIFO_AFULL_CNT");
    daqMon["SPY_FIFO_HAD_OFLOW"] = utils::readReg("BEFE.GEM.DAQ.STATUS.SPY.ERR_SPY_FIFO_HAD_OFLOW");
    daqMon["L1A_FIFO_DATA_CNT"] = utils::readReg("BEFE.GEM.DAQ.STATUS.L1A_FIFO_DATA_CNT");
    daqMon["L1A_FIFO_NEAR_FULL_CNT"] = utils::readReg("BEFE.GEM.DAQ.STATUS.L1A_FIFO_NEAR_FULL_CNT");
    daqMon["L1A_FIFO_HAD_OFLOW"] = utils::readReg("BEFE.GEM.DAQ.STATUS.L1A_FIFO_HAD_OVERFLOW");

    return daqMon;
}

std::map<std::string, uint32_t> gem::hardware::monitor::getDAQOH::operator()() const
{
    std::map<std::string, uint32_t> daqMon;

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        const std::string keyPrefix = "OH" + std::to_string(ohN) + ".";
        const std::string regPrefix = "BEFE.GEM.DAQ.OH" + std::to_string(ohN) + ".";

        daqMon[keyPrefix + "EVENT_FIFO_DATA_CNT"] = utils::readReg(regPrefix + "COUNTERS.EVT_FIFO_DATA_CNT");
        daqMon[keyPrefix + "EVENT_NUMBER"] = utils::readReg(regPrefix + "COUNTERS.EVN");
        daqMon[keyPrefix + "EVENT_RATE"] = utils::readReg(regPrefix + "COUNTERS.EVT_RATE");
        daqMon[keyPrefix + "INPUT_FIFO_DATA_CNT"] = utils::readReg(regPrefix + "COUNTERS.INPUT_FIFO_DATA_CNT");
        daqMon[keyPrefix + "STATUS.EVENT_FIFO_HAD_OFLOW"] = utils::readReg(regPrefix + "STATUS.EVENT_FIFO_HAD_OFLOW");
        daqMon[keyPrefix + "STATUS.EVENT_TOO_BIG"] = utils::readReg(regPrefix + "STATUS.EVENT_TOO_BIG");
        daqMon[keyPrefix + "STATUS.INPUT_FIFO_HAD_OFLOW"] = utils::readReg(regPrefix + "STATUS.INPUT_FIFO_HAD_OFLOW");
        daqMon[keyPrefix + "STATUS.INPUT_FIFO_HAD_UFLOW"] = utils::readReg(regPrefix + "STATUS.INPUT_FIFO_HAD_UFLOW");
        daqMon[keyPrefix + "STATUS.TTS_STATE"] = utils::readReg(regPrefix + "STATUS.TTS_STATE");
        daqMon[keyPrefix + "STATUS.VFAT_MIXED_BC"] = utils::readReg(regPrefix + "STATUS.VFAT_MIXED_BC");
        daqMon[keyPrefix + "STATUS.VFAT_MIXED_EC"] = utils::readReg(regPrefix + "STATUS.VFAT_MIXED_EC");
        daqMon[keyPrefix + "STATUS.VFAT_TOO_MANY"] = utils::readReg(regPrefix + "STATUS.VFAT_TOO_MANY");
        daqMon[keyPrefix + "VFAT_BLOCK_INVALID_BC_CNT"] = utils::readReg(regPrefix + "COUNTERS.VFAT_BLOCK_INVALID_BC_CNT");
        daqMon[keyPrefix + "VFAT_BLOCK_INVALID_CRC_CNT"] = utils::readReg(regPrefix + "COUNTERS.VFAT_BLOCK_INVALID_CRC_CNT");
    }

    return daqMon;
}

std::map<std::string, uint32_t> gem::hardware::monitor::getGBTLink::operator()() const
{
    std::map<std::string, uint32_t> gbtMon;

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        for (std::size_t gbtN = 0; gbtN < gbt::GBTS_PER_OH; ++gbtN) {
            const std::string respName = "OH" + std::to_string(ohN) + ".GBT" + std::to_string(gbtN);
            const std::string regName = "BEFE.GEM.OH_LINKS." + respName;

            gbtMon[respName + ".READY"] = utils::readReg(regName + "_READY");
            gbtMon[respName + ".WAS_NOT_READY"] = utils::readReg(regName + "_WAS_NOT_READY");
            gbtMon[respName + ".FEC_ERR_CNT"] = utils::readReg(regName + "_FEC_ERR_CNT");
        }
    }

    return gbtMon;
}

std::map<std::string, uint32_t> gem::hardware::monitor::getGBTMain::operator()() const
{
    std::map<std::string, uint32_t> gbtMon;

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        const auto lockedGBT = gbt::getLockedGBT {}(ohN, false /* instantaneous only */);

        for (size_t gbtN = 0; gbtN < gbt::GBTS_PER_OH; gbtN++) {
            if (!((lockedGBT >> gbtN) & 0x1))
                continue;

            try {
                uint32_t value = 0;

#if !defined(GEM_IS_ME0)
                // Single 8-bits register
                value = gbt::readGBTReg(ohN, gbtN, 435 /* FECcorrectionCount */);
#else
                // Multiple (4) 8-bits registers
                // From MSB to LSB
                //   0x1c6...0x1c9 -- DLDPFecCorrectionCount0...DLDPFecCorrectionCount3
                for (size_t i = 0; i < 4; ++i)
                    value |= gbt::readGBTReg(ohN, gbtN, 0x1c6 + i) << (8 * (3 - i));
#endif

                const std::string responseKey = "OH" + std::to_string(ohN) + ".GBT" + std::to_string(gbtN) + ".DOWN_FEC_ERR_CNT";
                gbtMon[responseKey] = value;
            } catch (const gbt::gbt_ic_error&) {
                // Failed transaction? Nothing to report.
            }
        }
    }

    return gbtMon;
}

std::map<std::string, uint32_t> gem::hardware::monitor::getOHMain::operator()(const uint32_t ohMask) const
{
    std::map<std::string, uint32_t> ohMon;

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!((ohMask >> ohN) & 0x1))
            continue;

        const std::string keyPrefix = "OH" + std::to_string(ohN) + ".";
        const std::string regPrefix = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".";

        ohMon[keyPrefix + "FW_VERSION"] = utils::readReg(regPrefix + "FPGA.CONTROL.HOG.OH_VER");
    }

    return ohMon;
}

std::map<std::string, std::variant<uint32_t, double>> gem::hardware::monitor::getOHSysmon::operator()(const uint32_t ohMask) const
{

    std::map<std::string, std::variant<uint32_t, double>> sysMon;

    // Maps the monitorable names to the register names
    const std::map<std::string, std::string> alarms = {
        { "FPGA_CORE_TEMP_ALARM", "OVERTEMP" },
        { "FPGA_CORE_TEMP_ALARM_CNT", "CNT_OVERTEMP" },
        { "FPGA_VCCINT_ALARM", "VCCINT_ALARM" },
        { "FPGA_VCCINT_ALARM_CNT", "CNT_VCCINT_ALARM" },
        { "FPGA_VCCAUX_ALARM", "VCCAUX_ALARM" },
        { "FPGA_VCCAUX_ALARM_CNT", "CNT_VCCAUX_ALARM" },
    };

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!((ohMask >> ohN) & 0x1))
            continue;

        const std::string keyName = "OH" + std::to_string(ohN) + ".";
        const std::string regBase = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".FPGA.ADC.CTRL.";

        // Generic alarms
        for (auto const& alarm : alarms)
            sysMon[keyName + alarm.first] = utils::readReg(regBase + alarm.second);

        // Formula to convert the ADC values to temperature taken from Virtex-6 FPGA System Monitor - UG370 (v1.2)
        utils::writeReg(regBase + "ADR_IN", 0);
        sysMon[keyName + "FPGA_CORE_TEMP"] = (double((utils::readReg(regBase + "DATA_OUT") >> 6) & 0x3ff) * 503.975 / 1024) - 273.15;

        // Formula to convert the ADC values to voltages taken from Virtex-6 FPGA System Monitor - UG370 (v1.2)
        utils::writeReg(regBase + "ADR_IN", 1);
        sysMon[keyName + "FPGA_VCCINT"] = 3 * (double((utils::readReg(regBase + "DATA_OUT") >> 6) & 0x3ff) / 1024);
        utils::writeReg(regBase + "ADR_IN", 2);
        sysMon[keyName + "FPGA_VCCAUX"] = 3 * (double((utils::readReg(regBase + "DATA_OUT") >> 6) & 0x3ff) / 1024);
    }

    return sysMon;
}

std::map<std::string, uint32_t> gem::hardware::monitor::getSCAMain::operator()() const
{
    std::map<std::string, uint32_t> scaMon;

    const uint32_t sca_status_ready = utils::readReg("BEFE.GEM.SLOW_CONTROL.SCA.STATUS.READY");
    const uint32_t sca_status_critical_error = utils::readReg("BEFE.GEM.SLOW_CONTROL.SCA.STATUS.CRITICAL_ERROR");

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        const std::string regName = "BEFE.GEM.SLOW_CONTROL.SCA.STATUS.NOT_READY_CNT_OH" + std::to_string(ohN);

        const std::string keyPrefix = "OH" + std::to_string(ohN) + ".SCA.STATUS.";
        scaMon[keyPrefix + "READY"] = (sca_status_ready >> ohN) & 0x1;
        scaMon[keyPrefix + "NOT_READY_CNT"] = utils::readReg(regName);
        scaMon[keyPrefix + "CRITICAL_ERROR"] = (sca_status_critical_error >> ohN) & 0x1;
    }

    return scaMon;
}

uint32_t gem::hardware::monitor::getOHMask::operator()() const
{
    uint32_t mask = 0;

#if !defined(GEM_IS_ME0)
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        try {
            utils::readReg("BEFE.GEM.OH.OH" + std::to_string(ohN) + ".FPGA.CONTROL.LOOPBACK.DATA");
            // If slow-control communication is possible, the OptoHybrid should be monitored
            mask |= (1 << ohN);
        } catch (const xhal::memhub::memory_access_error&) {
        }
    }
#endif

    return mask;
}

uint32_t gem::hardware::monitor::getSCAMask::operator()() const
{
    return sca::scaReady();
}

std::map<std::string, uint32_t> gem::hardware::monitor::getVFATLink::operator()() const
{
    std::map<std::string, uint32_t> vfatMon;

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        const std::string keyBase = "OH" + std::to_string(ohN) + ".";
        const std::string regBase = "BEFE.GEM.OH_LINKS.OH" + std::to_string(ohN) + ".";

        vfatMon[keyBase + "VFAT_MASK"] = vfat3::getVFATMask {}(ohN);

        for (std::size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
            const std::string vfatKeyBase = keyBase + "VFAT" + std::to_string(vfatN) + ".";
            const std::string vfatRegBase = regBase + "VFAT" + std::to_string(vfatN) + ".";

            vfatMon[vfatKeyBase + "SYNC_ERR_CNT"] = utils::readReg(vfatRegBase + "SYNC_ERR_CNT");
            vfatMon[vfatKeyBase + "DAQ_EVENT_CNT"] = utils::readReg(vfatRegBase + "DAQ_EVENT_CNT");
            vfatMon[vfatKeyBase + "DAQ_CRC_ERROR_CNT"] = utils::readReg(vfatRegBase + "DAQ_CRC_ERROR_CNT");
        }
    }

    return vfatMon;
}

std::map<std::string, double> gem::hardware::monitor::getSCACurrent::operator()(const uint32_t ohMask) const
{
    std::map<std::string, double> currentMap;

#if !defined(GEM_IS_ME0)
    for (const auto& [currentName, adcChannel] : sca::ADC_CURRENT_CHANNELS) {
        const auto t_raw = sca::readSCAADC(ohMask, adcChannel);

        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!t_raw.at(ohN))
                continue;

            const double R = 0.010; // 10 mOhm sensing resistor

            const double raw = *t_raw.at(ohN) & 0xfff;
            currentMap["OH" + std::to_string(ohN) + "." + currentName + ".RAW"] = raw;

            const double voltage = raw * sca::ADC_TO_VOLT * 4 /* voltage divider */ / 20 /* sense amplifier gain */;
            const double current = voltage / R;
            currentMap["OH" + std::to_string(ohN) + "." + currentName] = current;
        }
    }
#else
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        const auto lockedGBT = gbt::getLockedGBT {}(ohN, false /* instantaneous only */);

        for (const auto& [currentName, adcChannel] : lpgbt::ADC_CURRENT_CHANNELS) {
            if (!((lockedGBT >> adcChannel.first) & 0x1))
                continue;

            const double R = 0.010; // 10 mOhm sensing resistor

            // For a reason still to be understood by the GEM Electronics group,
            // the differential measurement mode does not appear to work to measure
            // the GEB currents through a shunt resistor. This code emulates the
            // differential mode with two single ended measurements.
            const auto rawP = lpgbt::readADC(ohN, adcChannel.first, adcChannel.second.inp);
            const auto rawN = lpgbt::readADC(ohN, adcChannel.first, adcChannel.second.inn);
            if (!rawP || !rawN)
                continue;

            currentMap["OH" + std::to_string(ohN) + "." + currentName + ".INP.RAW"] = *rawP;
            currentMap["OH" + std::to_string(ohN) + "." + currentName + ".INN.RAW"] = *rawN;

            const double voltageP = *rawP * lpgbt::ADC_TO_VOLT * 2 /* external voltage divider */;
            const double voltageN = *rawN * lpgbt::ADC_TO_VOLT * 2 /* external voltage divider */;
            const double current = (voltageP - voltageN) / R;
            currentMap["OH" + std::to_string(ohN) + "." + currentName] = current;
        }
    }
#endif

    return currentMap;
}

std::map<std::string, double> gem::hardware::monitor::getSCATemperature::operator()(const uint32_t ohMask) const
{
    std::map<std::string, double> temperatureMap;

#if !defined(GEM_IS_ME0)
    // Internal SCA temperature sensor
    {
        const auto t_raw = sca::readSCAADC(ohMask, sca::ADCChannel::SCA_TEMP);

        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!t_raw.at(ohN))
                continue;

            const double raw = *t_raw.at(ohN) & 0xfff;
            temperatureMap["OH" + std::to_string(ohN) + ".SCA_TEMP.RAW"] = raw;

            const double temperature = (raw * sca::ADC_TO_VOLT - sca::ADC_TEMP_INTERCEPT) / sca::ADC_TEMP_SLOPE;
            temperatureMap["OH" + std::to_string(ohN) + ".SCA_TEMP"] = temperature;
        }
    }

    // SCA ADC current source calibration
    std::vector<double> adcCurrent(amc::number_oh(), sca::ADC_NOMINAL_CURRENT);
    {
        const auto t_raw = sca::readSCAADC(ohMask, sca::ADC_CURRENT_CALIBRATION_CHANNEL);

        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!t_raw.at(ohN))
                continue;

            const double raw = *t_raw.at(ohN) & 0xfff;
            temperatureMap["OH" + std::to_string(ohN) + ".ADC_CURRENT.RAW"] = raw;

            // Precision resistor -- default case (1k)
            double R = 1000; // in Ohm
#if defined(GEM_IS_GE11)
            // Precision resistor -- special GE1/1 case (1k or 1k + 3k)
            //
            // During the GE1/1 OptoHybrid early production, no precision resistor
            // was installed for the calibration of the SCA current source. Instead,
            // a voltage divider for the 1.8V PROM power line was in place, but unused
            // due to the absence of the FPGA PROM and its associated FEAST. In view
            // of need for a SCA current source calibration, that circuit was replaced
            // by a precision 1k resistor during the late production. Instead of relying
            // on a potentially inaccurate DB, an heuristic is used here to discriminate
            // between the two cases. The value of 350 DAC units was chosen based on the
            // obvious clearance between the two distributions.
            //
            // Voltage divider    -- 1k + 3k = 750 -- 0.075V @ 100µA ~307 DAC units
            // Precision resistor -- 1k            -- 0.100V @ 100µA ~410 DAC units
            //
            // Note: for a yet-to-be-understood reason, the 1k and 3k resistors behave
            // like in a parallel circuit whereas the expectation would be for the 3k
            // branch to be unconnected and so without impact on the total resistance.
            if (raw < 350)
                R = 750; // in Ohm
#endif
            const double current = (raw * sca::ADC_TO_VOLT) / R;
            temperatureMap["OH" + std::to_string(ohN) + ".ADC_CURRENT"] = current * 1000000 /* in µA */;

            // Update the ADC current source value for subsequent usage if reasonable (+/- 50%)
            if ((0.5 * adcCurrent.at(ohN) < current) && (current < 1.5 * adcCurrent.at(ohN)))
                adcCurrent.at(ohN) = current;
        }
    }

    // External SCA temperature PT sensors
    for (const auto& [temperatureName, adcChannel] : sca::ADC_PT_CHANNELS) {
        const auto t_raw = sca::readSCAADC(ohMask, adcChannel);

        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!t_raw.at(ohN))
                continue;

            // Constants and formula taken from
            // http://www.code10.info/index.php%3Foption%3Dcom_content%26view%3Darticle%26id%3D82:measuring-temperature-platinum-resistance-thermometers%26catid%3D60:temperature%26Itemid%3D83
            const double a = 0.00390802;
            const double b = -0.000000580195;

            const double raw = *t_raw.at(ohN) & 0xfff;
            temperatureMap["OH" + std::to_string(ohN) + "." + temperatureName + ".RAW"] = raw;

            const double R = raw * sca::ADC_TO_VOLT / adcCurrent.at(ohN);

            // Reference resistance -- default case (PT1000)
            double R_0 = 1000; // in Ohm
#if defined(GEM_IS_GE11)
            // Reference resistance -- special GE1/1 case (PT100 or PT1000)
            //
            // In GE1/1 both PT100 (early production) and PT1000 (late production)
            // are used. Instead of relying on a potentially inaccurate DB, a
            // threshold value is used to distinguish between the two cases. The
            // value of 470 Ohm is chosen to give enough margin between the two
            // sensors.
            //
            // PT100  --  ~80 Ohm @ -50°C -  ~140 Ohm @ 100°C
            // PT1000 -- ~800 Ohm @ -50°C - ~1400 Ohm @ 100°C
            if (R < 470)
                R_0 = 100; // in Ohm
#endif

            const double temperature = (-R_0 * a + sqrt(R_0 * R_0 * a * a - (4 * R_0 * b * (R_0 - R)))) / (2 * R_0 * b);
            temperatureMap["OH" + std::to_string(ohN) + "." + temperatureName] = temperature;
        }
    }
#else
    // External lpGBT temperature sensors
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        const auto lockedGBT = gbt::getLockedGBT {}(ohN, false /* instantaneous only */);

        for (const auto& [temperatureName, adcChannel] : lpgbt::ADC_TEMP_CHANNELS) {
            if (!((lockedGBT >> adcChannel.first) & 0x1))
                continue;

            const bool isThermistor = (temperatureName.find("_TH") != std::string::npos);
            const uint8_t thermistorCurrentDAC = (temperatureName.find("VTRX") != std::string::npos) ? 60 : 20;

            const auto raw = lpgbt::readADC(ohN, adcChannel.first, adcChannel.second,
                lpgbt::adcGain::X2, isThermistor ? std::optional<uint8_t>(thermistorCurrentDAC) : std::nullopt);
            if (!raw)
                continue;

            temperatureMap["OH" + std::to_string(ohN) + "." + temperatureName + ".RAW"] = *raw;

            if (isThermistor) {
                const double R = *raw * lpgbt::ADC_TO_VOLT / (thermistorCurrentDAC * lpgbt::DAC_TO_AMPS);

                // Using the Steinhart–Hart equation model to compute the
                // temperature from the thermistor resistance.
                //
                // * The VTRx+ 1k constants are computed based on the nominal
                //   resistances at 15°C, 25°C, and 60°C taken from the Murata
                //   NCP03XM102E05RL datasheet.
                //
                // * The ASIAGO 10k constants are computed based on the 25/50°C
                //   and 25/85°C B constants taken from the TDK NTCG103JX103DT1S
                //   datasheet.
                //
                // * The GEB 10k constants are computed based on the nominal
                //   resistances at 15°C, 25°C, and 60°C taken from the official
                //   Murata NCP03XH103F05RL web page.
                double a = 0, b = 0, c = 0;
                if (temperatureName.find("VTRX") != std::string::npos) {
                    a = 1.484040797e-3;
                    b = 2.614267766e-4;
                    c = 1.944780147e-7;
                } else if (temperatureName.find("OH") != std::string::npos) {
                    a = 0.8730639236e-3;
                    b = 2.539939946e-4;
                    c = 1.812087081e-7;
                } else if (temperatureName.find("GEB") != std::string::npos) {
                    a = 0.9387533695e-3;
                    b = 2.428064047e-4;
                    c = 2.290152779e-7;
                }

                const double temperature = 1. / (a + b * log(R) + c * pow(log(R), 3)) - 273.15;
                temperatureMap["OH" + std::to_string(ohN) + "." + temperatureName] = temperature;
            }
        }
    }
#endif

    return temperatureMap;
}

std::map<std::string, double> gem::hardware::monitor::getSCAVoltage::operator()(const uint32_t ohMask) const
{
    std::map<std::string, double> voltageMap;

#if !defined(GEM_IS_ME0)
    for (const auto& [voltageName, adcChannel] : sca::ADC_VOLTAGE_CHANNELS) {
        const auto t_raw = sca::readSCAADC(ohMask, adcChannel);

        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!t_raw.at(ohN))
                continue;

            const double raw = *t_raw.at(ohN) & 0xfff;
            voltageMap["OH" + std::to_string(ohN) + "." + voltageName + ".RAW"] = raw;

            // Voltage divider -- default case
            double voltageDivider = 4;
#if defined(GEM_IS_GE11)
            // Voltage divider -- special GE1/1 VFAT FEAST case
            if (voltageName.find("V1P2_VFAT_FQ") != std::string::npos)
                voltageDivider = 2;
#endif
            const double voltage = raw * sca::ADC_TO_VOLT * voltageDivider;
            voltageMap["OH" + std::to_string(ohN) + "." + voltageName] = voltage;
        }
    }
#else
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        const auto lockedGBT = gbt::getLockedGBT {}(ohN, false /* instantaneous only */);

        for (const auto& [voltageName, adcChannel] : lpgbt::ADC_VOLTAGE_CHANNELS) {
            if (!((lockedGBT >> adcChannel.first) & 0x1))
                continue;

            const auto raw = lpgbt::readADC(ohN, adcChannel.first, adcChannel.second);
            if (!raw)
                continue;

            voltageMap["OH" + std::to_string(ohN) + "." + voltageName + ".RAW"] = *raw;

            double voltage = *raw * lpgbt::ADC_TO_VOLT;

            const std::string isVIN = "VIN";
            const std::string isV2P5 = "V2P5";
            const std::string isVREF = "VREF";
            const std::string isVSSA = "VSSA";
            if (std::equal(isVIN.rbegin(), isVIN.rend(), voltageName.rbegin())) {
                voltage *= (750 + 10000) / 750; /* external voltage divider */
            } else if (std::equal(isV2P5.rbegin(), isV2P5.rend(), voltageName.rbegin())) {
                voltage *= 3; /* external voltage divider */
            } else if (std::equal(isVREF.rbegin(), isVREF.rend(), voltageName.rbegin())) {
                voltage *= 2; /* internal voltage divider */
            } else if (std::equal(isVSSA.rbegin(), isVSSA.rend(), voltageName.rbegin())) {
                /* no voltage divider */
            } else if (voltageName.find("V1P2") != std::string::npos) {
                voltage *= 2; /* external voltage divider */
            } else {
                voltage /= 0.42; /* internal voltage divider */
            }

            voltageMap["OH" + std::to_string(ohN) + "." + voltageName] = voltage;
        }
    }
#endif

    return voltageMap;
}

std::map<std::string, double> gem::hardware::monitor::getSCARSSI::operator()(const uint32_t ohMask) const
{
    std::map<std::string, double> rssiMap;

#if !defined(GEM_IS_ME0)
    for (size_t gbtN = 0; gbtN < sca::ADC_RSSI_CHANNELS.size(); ++gbtN) {
        const auto t_raw = sca::readSCAADC(ohMask, sca::ADC_RSSI_CHANNELS.at(gbtN));

        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!t_raw.at(ohN))
                continue;

            const double raw = *t_raw.at(ohN) & 0xfff;
            rssiMap["OH" + std::to_string(ohN) + ".RSSI" + std::to_string(gbtN) + ".RAW"] = raw;

            double rssi = raw * sca::ADC_TO_VOLT;
            rssi = (((rssi / (sca::ADC_RSSI_R2 / (sca::ADC_RSSI_R1 + sca::ADC_RSSI_R2))) - sca::ADC_RSSI_VCC) / sca::ADC_RSSI_R1) * -1;
            rssi *= 1000000; /* in µA */

            // In GE1/1, the RSSI cannot always be measured due to hardware bug
            // We set an empiric cut at 25 ADC units to as definition of a malfunctioning circuit
            if (raw > 25)
                rssiMap["OH" + std::to_string(ohN) + ".RSSI" + std::to_string(gbtN)] = rssi;
        }
    }
#else
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        const auto lockedGBT = gbt::getLockedGBT {}(ohN, false /* instantaneous only */);

        for (size_t vtrxN = 0; vtrxN < lpgbt::ADC_RSSI_CHANNELS.size(); ++vtrxN) {
            const auto& adcChannel = lpgbt::ADC_RSSI_CHANNELS.at(vtrxN);

            if (!((lockedGBT >> adcChannel.first) & 0x1))
                continue;

            const auto raw = lpgbt::readADC(ohN, adcChannel.first, adcChannel.second);
            if (!raw)
                continue;

            rssiMap["OH" + std::to_string(ohN) + ".RSSI" + std::to_string(vtrxN) + ".RAW"] = *raw;

            double rssi = *raw * lpgbt::ADC_TO_VOLT;
            rssi = rssi * ((lpgbt::ADC_RSSI_R2 + lpgbt::ADC_RSSI_R3) / lpgbt::ADC_RSSI_R3); // Voltage divider
            rssi = (lpgbt::ADC_RSSI_VCC - rssi) / lpgbt::ADC_RSSI_R1;
            rssi *= 1000000;
            rssiMap["OH" + std::to_string(ohN) + ".RSSI" + std::to_string(vtrxN)] = rssi;
        }
    }
#endif

    return rssiMap;
}

std::map<std::string, uint32_t> gem::hardware::monitor::getmonCTP7dump::operator()(const std::string& fname) const
{
    LOG4CPLUS_INFO(logger, "Using registers found in: " << fname);
    std::ifstream ifs(fname);
    if (!ifs) {
        std::stringstream errmsg;
        errmsg << "Error opening file: " << fname;
        LOG4CPLUS_ERROR(logger, errmsg.str());
        throw std::runtime_error(errmsg.str());
    }

    std::map<std::string, uint32_t> ctp7Dump;

    std::vector<std::pair<std::string, std::string>> regNames;
    std::string line;
    while (std::getline(ifs, line)) {
        // format0x658030f8 r    BEFE.GEM.OH_LINKS.OH11.VFAT23.DAQ_CRC_ERROR_CNT          0x00000000
        std::replace(line.begin(), line.end(), '\t', ' ');
        std::stringstream ss(line);
        std::string token;
        std::size_t count = 0;
        std::vector<std::string> tmp;
        while (std::getline(ss, token, ' ')) {
            if (token.empty())
                continue;

            if (count == 0 || count == 2) {
                tmp.push_back(token);
                LOG4CPLUS_DEBUG(logger, "Pushing back " << token << " as value " << count);
            }
            ++count;
        }
        ctp7Dump[tmp.at(0)] = utils::readReg(tmp.at(1));
    }

    return ctp7Dump;
}
std::map<std::string, uint32_t> gem::hardware::monitor::getOHSEU::operator()(const uint32_t ohMask) const
{
    std::map<std::string, uint32_t> seuMon;

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!((ohMask >> ohN) & 0x1))
            continue;

        const std::string keyPrefix = "OH" + std::to_string(ohN) + ".";
        const std::string regPrefix = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".";

        seuMon[keyPrefix + "FPGA.SEM_STATE"] = utils::readReg(regPrefix + "FPGA.CONTROL.SEM.SEM_STATE");
        seuMon[keyPrefix + "FPGA.SEM_ALIVE"] = utils::readReg(regPrefix + "FPGA.CONTROL.SEM.SEM_ALIVE");
        seuMon[keyPrefix + "FPGA.SEU_CORRECTION_CNT"] = utils::readReg(regPrefix + "FPGA.CONTROL.SEM.CNT_SEM_CORRECTION");
        seuMon[keyPrefix + "FPGA.SEU_CORRECTABLE_CNT"] = utils::readReg(regPrefix + "FPGA.CONTROL.SEM.CNT_SEM_CORRECTABLE");
        seuMon[keyPrefix + "FPGA.SEU_UNCORRECTABLE_CNT"] = utils::readReg(regPrefix + "FPGA.CONTROL.SEM.CNT_SEM_UNCORRECTABLE");
        seuMon[keyPrefix + "FPGA.TMR_TTC_ERR_CNT"] = utils::readReg(regPrefix + "FPGA.CONTROL.TMR.TTC_TMR_ERR_CNT");
        seuMon[keyPrefix + "FPGA.TMR_IPB_SWITCH_ERR_CNT"] = utils::readReg(regPrefix + "FPGA.CONTROL.TMR.IPB_SWITCH_TMR_ERR_CNT");
        seuMon[keyPrefix + "FPGA.TMR_IPB_SLAVE_ERR_CNT"] = utils::readReg(regPrefix + "FPGA.CONTROL.TMR.IPB_SLAVE_TMR_ERR_CNT");
        seuMon[keyPrefix + "FPGA.TMR_CLUSTER_ERR_CNT"] = utils::readReg(regPrefix + "FPGA.TRIG.TMR.CLUSTER_TMR_ERR_CNT");
        seuMon[keyPrefix + "FPGA.TMR_SBIT_RX_ERR_CNT"] = utils::readReg(regPrefix + "FPGA.TRIG.TMR.SBIT_RX_TMR_ERR_CNT");
        seuMon[keyPrefix + "FPGA.TMR_TRIG_FORMATTER_ERR_CNT"] = utils::readReg(regPrefix + "FPGA.TRIG.TMR.TRIG_FORMATTER_TMR_ERR_CNT");
        seuMon[keyPrefix + "FPGA.TMR_TRIG_IPB_SLAVE_ERR_CNT"] = utils::readReg(regPrefix + "FPGA.TRIG.TMR.IPB_SLAVE_TMR_ERR_CNT");
        seuMon[keyPrefix + "FPGA.TMR_GBT_LINK_ERR_CNT"] = utils::readReg(regPrefix + "FPGA.GBT.TMR.GBT_LINK_TMR_ERR_CNT");
        seuMon[keyPrefix + "FPGA.TMR_GBT_SERDES_ERR_CNT"] = utils::readReg(regPrefix + "FPGA.GBT.TMR.GBT_SERDES_TMR_ERR_CNT");
        seuMon[keyPrefix + "FPGA.TMR_GBT_IPB_SLAVE_ERR_CNT"] = utils::readReg(regPrefix + "FPGA.GBT.TMR.IPB_SLAVE_TMR_ERR_CNT");
    }

    return seuMon;
}

std::map<std::string, uint32_t> gem::hardware::monitor::getFEStatus::operator()(const uint32_t ohMask) const
{
    std::map<std::string, uint32_t> statusMap;

    const auto programmedFPGA = oh::getProgrammedFPGA {}(ohMask);
    statusMap["PROGRAMMED_FPGA"] = programmedFPGA.first;
    statusMap["PROGRAMMED_FPGA_VALID"] = programmedFPGA.second;

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        const auto rawRunMode = vfat3::broadcastRead {}(ohN, vfat3::getSyncedVFAT {}(ohN), "CFG_RUN");

        uint32_t runMode = 0, runModeValid = 0;
        for (size_t vfatN = 0; vfatN < rawRunMode.size(); ++vfatN) {
            if (rawRunMode[vfatN] == 0) {
                runModeValid |= (1ULL << vfatN);
            } else if (rawRunMode[vfatN] == 1) {
                runMode |= (1ULL << vfatN);
                runModeValid |= (1ULL << vfatN);
            }
        }

        const std::string keyPrefix = "OH" + std::to_string(ohN) + ".";
        statusMap[keyPrefix + "VFAT_RUN_MODE"] = runMode;
        statusMap[keyPrefix + "VFAT_RUN_MODE_VALID"] = runModeValid;
    }

    return statusMap;
}
