/// @file
/// @brief AMC methods for RPC modules

#include <gem/hardware/amc.h>

#include "board/mgt.h"
#include "board/promless.h"
#include "gbt.h"
#include "hw_constants.h"
#include "utils.h"
#include <gem/hardware/amc/blaster_ram.h>
#include <gem/hardware/amc/daq.h>
#include <gem/hardware/amc/ttc.h>
#include <gem/hardware/board.h>

#include <fmt/format.h>

#include <chrono>
#include <limits.h>
#include <thread>
#include <time.h>
#include <unistd.h>

gem::hardware::amc::facts gem::hardware::amc::getFacts::operator()() const
{
    facts t_facts;

    // Hostname
    size_t size = HOST_NAME_MAX + 1;
    char hostname[size];
    if (gethostname(hostname, size)) {
        throw std::runtime_error("Cannot get AMC hostname");
    }
    t_facts.hostname = hostname;

    // Firmware/system constants
    t_facts.number_oh = number_oh();
    t_facts.number_gbt = gbt::GBTS_PER_OH;
    t_facts.number_vfat = oh::VFATS_PER_OH;

    return t_facts;
}

void gem::hardware::amc::recover::operator()() const
{
    LOG4CPLUS_INFO(logger, "Recover the AMC");

#if defined(GEM_IS_CTP7)
    board::recover {}();
    board::reset {}();
    board::configure {}();
#endif

    const auto firmwareGEMStation = utils::readReg("BEFE.GEM.GEM_SYSTEM.RELEASE.GEM_STATION");
    if (firmwareGEMStation != SW_GEM_STATION)
        throw std::runtime_error(fmt::format(FMT_STRING("Mismatch between firmware ({:d}) and software ({:d}) GEM station"), firmwareGEMStation, SW_GEM_STATION));

    // Apply the GBT polarities
    for (size_t ohN = 0; ohN < number_oh(); ++ohN) {
        for (size_t gbtN = 0; gbtN < gbt::GBTS_PER_OH; ++gbtN) {
            const auto tx_link_idx = utils::readReg(fmt::format(FMT_STRING("BEFE.GEM.GEM_SYSTEM.RELEASE.OH_LINK_CONFIG.OH{:d}.GBT{:d}_TX"), ohN, gbtN));
            board::set_link_polarity(tx_link_idx, true /* TX */, MGT_TX_POLARITY_GBT);

            const auto rx_link_idx = utils::readReg(fmt::format(FMT_STRING("BEFE.GEM.GEM_SYSTEM.RELEASE.OH_LINK_CONFIG.OH{:d}.GBT{:d}_RX"), ohN, gbtN));
            board::set_link_polarity(rx_link_idx, false /* RX */, MGT_RX_POLARITY_GBT);
        }
    }
}

void gem::hardware::amc::reset::operator()() const
{
    LOG4CPLUS_INFO(logger, "Reset the AMC registers");

    // Bring back all registers to their default values set by the firmware
    const uint32_t targetPhase = utils::readReg("BEFE.GEM.TTC.CTRL.LOCKMON_TARGET_PHASE");
    utils::writeReg("BEFE.GEM.GEM_SYSTEM.CTRL.IPBUS_RESET", 1);

    // The board layer is responsible for the TTC clock phase alignement. However
    // the firmware assumes otherwise. Make SURE to disable the feature here.
    // Also make sure to restore the target phase for monitoring purposes.
    utils::writeReg("BEFE.GEM.TTC.CTRL.DISABLE_PHASE_ALIGNMENT", 1);
    utils::writeReg("BEFE.GEM.TTC.CTRL.LOCKMON_TARGET_PHASE", targetPhase);

    // Ues a saner default value for specific registers
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.INPUT_ENABLE_MASK", 0); // Treat all inputs equally, i.e. disabled
    utils::writeReg("BEFE.GEM.TRIGGER.CTRL.OH_KILL_MASK", -1); // Mask all OptoHybrids from the trigger TX
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.SPY.SPY_DEST_MAC_1", 0xcafe); // 0xBEFE goes to take a coffee
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.SPY.SPY_DEST_MAC_2", 0x0);
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.SPY.SPY_DEST_MAC_3", 0x0);
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.SPY.SPY_SOURCE_MAC_1", 0xbefe); // We have a 0xBEFE sender
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.SPY.SPY_SOURCE_MAC_2", 0x0);
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.SPY.SPY_SOURCE_MAC_3", 0x0);
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.SPY.SPY_ETHERTYPE", 0x88b5); // Experimental Type 1
    utils::writeReg("BEFE.GEM.DAQ.CONTROL.DAV_TIMEOUT", 0x500); // Default chamber data available timeout

    // Write the correct HDLC addresses to the appropriate registers
    for (uint32_t i = 0; i < oh::VFATS_PER_OH; i++) {
        const auto register_hdlc_address = std::string { "BEFE.GEM.GEM_SYSTEM.VFAT3.VFAT" } + std::to_string(i) + "_HDLC_ADDRESS";
        utils::writeReg(register_hdlc_address, vfat::HDLC_ADDRESSES.at(i));
    }

    // Reset the system
    LOG4CPLUS_INFO(logger, "Reset the AMC");
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    utils::writeReg("BEFE.GEM.GEM_SYSTEM.CTRL.GLOBAL_RESET", 1);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
}

void gem::hardware::amc::configure::operator()() const
{
#if !defined(GEM_IS_ME0)
    LOG4CPLUS_INFO(logger, "Retrieve the OptoHybrid firmware size from the board");
    utils::writeReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.FIRMWARE_SIZE", board::promless_size());
#endif

    LOG4CPLUS_INFO(logger, "Configure the AMC registers");
    const auto data = utils::read_configuration_file("amc/config.cfg");
    for (const auto& i : data)
        utils::writeReg("BEFE.GEM." + i.first, i.second);
}
