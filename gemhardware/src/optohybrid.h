/// @file

#ifndef SRC_GEM_HARDWARE_OPTOHYBRID_H
#define SRC_GEM_HARDWARE_OPTOHYBRID_H

#include <gem/hardware/optohybrid.h>

namespace gem {
namespace hardware {
    namespace oh {

        /// @brief Attempts to initialize the OptoHybrid trigger links
        ///
        /// @details Technically, this function tries to make sure that the
        ///     MGT TX are sending valid data by looking at the bitstream with
        ///     the MGT RX in loopback mode. This is implemented only for GE1/1
        ///     that suffers from the trigger links initialization issue.
        ///
        /// @warning This method does not throw on register access errors.
        ///          Check the return value to avoid ignoring errors silently.
        ///
        /// @param @c ohN OptoHybrid of interest
        ///
        /// @return Whether or not the slow-control communication was reliable
        ///     during the links initialization. It *does* not guarantee that
        ///     the links are successfully initialized.
        bool initialize_trigger_links(uint32_t ohN);

    } // namespace gem::hardware::oh
} // namespace gem::hardware
} // namespace gem

#endif // SRC_GEM_HARDWARE_OPTOHYBRID_H
