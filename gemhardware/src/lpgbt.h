/// @file

#ifndef SRC_GEM_HARDWARE_LPGBT_H
#define SRC_GEM_HARDWARE_LPGBT_H

#include <array>
#include <optional>
#include <stdint.h>
#include <utility>

namespace gem {
namespace hardware {
    namespace lpgbt {

        /// @brief Describes an LpGBT ADC input, either single-ended or differential
        struct adcChannel {
            /// @brief Constructor
            ///
            /// Defaults to single-ended input if only a single channel is
            /// provided.
            constexpr adcChannel(uint8_t p, uint8_t n = 0xf)
                : inp(p)
                , inn(n)
            {
            }

            uint8_t inp; ///< Differential positive input
            uint8_t inn; ///< Differential negative input
        };

        /// @brief Allows to select the LpGBT ADC gain
        enum struct adcGain : uint8_t {
            X2 = 0x0, ///< x1 in single-ended mode
            X8 = 0x1, ///< x4 in single-ended mode
            X16 = 0x2, ///< x8 in single-ended mode
            X32 = 0x3, ///< x16 in single-ended mode
        };

        /// @brief Enable the ADC lpGBT feature
        ///
        /// @param @c ohN OptoHybrid index
        /// @param @c gbtN GBT index within an OptoHybrid
        void enableADC(uint32_t ohN, uint8_t gbtN);

        /// @brief Read raw values from the chosen ADC input channel
        ///
        /// @param @c ohN OptoHybrid index
        /// @param @c gbtN GBT index within an OptoHybrid
        /// @param @c channel ADC channel
        /// @param @c currentSource Current source DAC value to provide on the positive input
        std::optional<uint32_t> readADC(uint32_t ohN, uint8_t gbtN, adcChannel channel, adcGain gain = adcGain::X2, std::optional<uint8_t> currentSource = std::nullopt);

        /// @brief Trigger a VFAT external reset
        ///
        /// @param @c ohN OptoHybrid index
        /// @param @c vfatMask Mask containing the VFAT to be externally reset
        ///
        /// @return Mask for which the VFAT reset has been applied
        uint32_t resetVFAT(uint32_t ohN, uint32_t vfatMask);

        /// @brief Read one (32 bits) e-fuse bank from the specified LpGBT
        ///
        /// @param @c ohN OptoHybrid index
        /// @param @c gbtN GBT index within an OptoHybrid
        /// @param @c address E-fuse address (must be a multiple of 4 bytes)
        ///
        /// @return The e-fuse bank value if the read operation succeeded
        std::optional<uint32_t> readFuseBank(uint32_t ohN, uint8_t gbtN, uint16_t address);

        /// @brief Perform an EOM scan on a given LpGBT
        ///
        /// Can throw on errors communicating with the front-end.
        ///
        /// @param @c ohN OptoHybrid index
        /// @param @c gbtN GBT index within an OptoHybrid
        /// @param @c samplesBX Acquisition period per point in bunch crossings
        std::array<std::array<std::pair<uint64_t, uint64_t>, 31>, 64> eyeOpeningMonitorScan(const uint32_t ohN, const uint8_t gbtN, uint32_t samplesBX);

    } // namespace gem::hardware::lpgbt
} // namespace gem::hardware
} // namespace gem

#endif // SRC_GEM_HARDWARE_LPGBT_H
