/// @brief

#ifndef SRC_GEM_HARDWARE_OPTICAL_H
#define SRC_GEM_HARDWARE_OPTICAL_H

#include "libi2c.h"

#include <stdint.h>

namespace gem {
namespace hardware {
    namespace optical {

        /// @brief Base class for the optical transceivers management
        class base_transceiver {
        public:
            /// @brief Destructor
            virtual ~base_transceiver() {}

            /// @brief Returns the measured input optical power in units of 0.1 µW
            virtual uint16_t measure_rx_power(uint8_t channel) = 0;

            /// @brief Returns the measured output optical power in units of 0.1 µW
            virtual uint16_t measure_tx_power(uint8_t channel) = 0;
        };

        /// @brief CXP transceivers management
        class cxp_transceiver : public base_transceiver {
        public:
            /// @brief Constructor
            cxp_transceiver(i2c_device&& rx_device, i2c_device&& tx_device);

            /// @brief Destructor
            virtual ~cxp_transceiver();

            uint16_t measure_rx_power(uint8_t channel) override;

            uint16_t measure_tx_power(uint8_t channel) override;

        protected:
            i2c_device m_rx_device; ///< Interface to the RX I2C device
            i2c_device m_tx_device; ///< Interface to the TX I2C device
        };

        /// @brief MiniPOD transceivers management
        class mp_transceiver : public base_transceiver {
        public:
            /// @brief Constructor
            mp_transceiver(i2c_device&& rx_device, i2c_device&& tx_device);

            /// @brief Destructor
            virtual ~mp_transceiver();

            uint16_t measure_rx_power(uint8_t channel) override;

            uint16_t measure_tx_power(uint8_t channel) override;

        protected:
            i2c_device m_rx_device; ///< Interface to the RX I2C device
            i2c_device m_tx_device; ///< Interface to the TX I2C device
        };

        /// @brief Returns the measured input optical power in 0.1 µW for a given link
        ///
        /// @param @c link_number Link number according to the board/firmware convention
        uint16_t measure_rx_power(uint16_t link_number);

        /// @brief Returns the measured output optical power in 0.1 µW for a given link
        ///
        /// @param @c link_number Link number according to the board/firmware convention
        uint16_t measure_tx_power(uint16_t link_number);

    } // namespace gem::hardware::optical
} // namespace gem::hardware
} // namespace gem

#endif // SRC_GEM_HARDWARE_OPTICAL_H
