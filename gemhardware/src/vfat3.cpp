/// @file
/// @brief RPC module for VFAT3 methods

#include "vfat3.h"

#include "exception.h"
#include "hw_constants.h"
#include "utils.h"
#include <gem/hardware/amc.h>

#include <fmt/format.h>
#include <reedmuller.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <memory>
#include <mutex>
#include <string>
#include <thread>

// local functions

uint16_t gem::hardware::vfat3::decodeChipID(const uint32_t encChipID)
{
    // can the generator be static to limit creation/destruction of resources?
    static reedmuller rm = 0;
    static std::unique_ptr<int[]> encoded = nullptr;
    static std::unique_ptr<int[]> decoded = nullptr;

    if ((!(rm = reedmuller_init(2, 5)))
        || (!(encoded = std::make_unique<int[]>(rm->n)))
        || (!(decoded = std::make_unique<int[]>(rm->k)))) {
        std::stringstream errmsg;
        errmsg << "Out of memory";

        reedmuller_free(rm);

        throw std::runtime_error(errmsg.str());
    }

    uint32_t maxcode = reedmuller_maxdecode(rm);
    if (encChipID > maxcode) {
        std::stringstream errmsg;
        errmsg << std::hex << std::setw(8) << std::setfill('0') << encChipID
               << " is larger than the maximum decodeable by RM(2,5)"
               << std::hex << std::setw(8) << std::setfill('0') << maxcode
               << std::dec;
        throw std::out_of_range(errmsg.str());
    }

    for (int j = 0; j < rm->n; ++j)
        encoded.get()[(rm->n - j - 1)] = (encChipID >> j) & 0x1;

    const int result = reedmuller_decode(rm, encoded.get(), decoded.get());

    if (result) {
        uint16_t decChipID = 0x0;

        char tmp_decoded[1024];
        char* dp = tmp_decoded;

        for (int j = 0; j < rm->k; ++j)
            dp += sprintf(dp, "%d", decoded.get()[j]);

        char* p;
        errno = 0;

        const uint32_t conv = strtoul(tmp_decoded, &p, 2);
        if (errno != 0 || *p != '\0') {
            std::stringstream errmsg;
            errmsg << "Unable to convert " << std::string(tmp_decoded) << " to int type";

            reedmuller_free(rm);

            throw std::runtime_error(errmsg.str());
        } else {
            decChipID = conv;
            reedmuller_free(rm);
            return decChipID;
        }
    } else {
        std::stringstream errmsg;
        errmsg << "Unable to decode message 0x"
               << std::hex << std::setw(8) << std::setfill('0') << encChipID
               << ", probably more than " << reedmuller_strength(rm) << " errors";

        reedmuller_free(rm);
        throw std::runtime_error(errmsg.str());
    }
}

uint32_t gem::hardware::vfat3::enableChannel(const uint8_t ohN, const uint32_t vfat_mask, const uint8_t channel, const bool enable)
{
    uint32_t result = 0;

    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        if (!((vfat_mask >> vfatN) & 0x1))
            continue;

        try {
            const std::string register_base = fmt::format(FMT_STRING("BEFE.GEM.OH.OH{}.GEB.VFAT{}.VFAT_CHANNELS.CHANNEL"), ohN, vfatN);

            if (channel != VFAT_ALL_CHANNELS) { // There is a single channel of interest
                const std::string register_name = register_base + std::to_string(channel) + ".MASK";
                utils::writeReg(register_name, !enable);
            } else { // Apply on all channels
                for (size_t i = 0; i < 128; ++i) {
                    const std::string register_name = register_base + std::to_string(i) + ".MASK";
                    utils::writeReg(register_name, !enable);
                }
            }
        } catch (const xhal::memhub::memory_access_error& e) {
            LOG4CPLUS_WARN(logger, e.what());
            // Do not mark the VFAT as successfully configured
            continue;
        }

        result |= (1ULL << vfatN);
    }

    return result;
}

uint32_t gem::hardware::vfat3::setTrimmingValue(const uint8_t ohN, const uint32_t vfat_mask, const uint8_t channel, const int16_t trimming)
{
    uint32_t result = 0;

    // If trimming is negative then we set polarity to 1, otherwise we set polarity to 0
    // The amplitude is the absolute value of the signed trimming parameter
    const uint16_t polarity = trimming < 0 ? 1 : 0;
    const uint16_t amplitude = trimming < 0 ? -trimming : trimming;

    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        if (!((vfat_mask >> vfatN) & 0x1))
            continue;

        try {
            const std::string register_base = fmt::format(FMT_STRING("BEFE.GEM.OH.OH{}.GEB.VFAT{}.VFAT_CHANNELS.CHANNEL"), ohN, vfatN);

            if (channel != VFAT_ALL_CHANNELS) { // There is a single channel of interest
                const std::string polarity_register_name = register_base + std::to_string(channel) + ".ARM_TRIM_POLARITY";
                const std::string amplitude_register_name = register_base + std::to_string(channel) + ".ARM_TRIM_AMPLITUDE";
                utils::writeReg(polarity_register_name, polarity);
                utils::writeReg(amplitude_register_name, amplitude);
            } else { // Apply on all channels
                for (size_t i = 0; i < 128; ++i) {
                    const std::string polarity_register_name = register_base + std::to_string(i) + ".ARM_TRIM_POLARITY";
                    const std::string amplitude_register_name = register_base + std::to_string(i) + ".ARM_TRIM_AMPLITUDE";
                    utils::writeReg(polarity_register_name, polarity);
                    utils::writeReg(amplitude_register_name, amplitude);
                }
            }
        } catch (const xhal::memhub::memory_access_error& e) {
            LOG4CPLUS_WARN(logger, e.what());
            // Do not mark the VFAT as successfully configured
            continue;
        }

        result |= (1ULL << vfatN);
    }

    return result;
}

std::array<gem::hardware::utils::RegInfo, gem::hardware::oh::VFATS_PER_OH> gem::hardware::vfat3::prepareBroadcast(const uint32_t ohN, const std::string& register_name)
{
    std::array<utils::RegInfo, oh::VFATS_PER_OH> registers;
    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        const std::string t_register_name = fmt::format(FMT_STRING("BEFE.GEM.OH.OH{:d}.GEB.VFAT{:d}.{:s}"), ohN, vfatN, register_name);
        registers[vfatN] = utils::getReg(t_register_name);
    }
    return registers;
}

uint32_t gem::hardware::vfat3::doBroadcastWrite(const std::array<utils::RegInfo, oh::VFATS_PER_OH>& registers, const uint32_t vfat_mask, const uint32_t value)
{
    uint32_t result = 0;

    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        if (!((vfat_mask >> vfatN) & 0x1))
            continue;

        try {
            utils::writeReg(registers[vfatN], value);
        } catch (const xhal::memhub::memory_access_error& e) {
            LOG4CPLUS_WARN(logger, e.what());
            // Do not mark the write as successful
            continue;
        }

        result |= (1ULL << vfatN);
    }

    return result;
}

std::vector<std::optional<uint32_t>> gem::hardware::vfat3::doBroadcastRead(const std::array<utils::RegInfo, oh::VFATS_PER_OH>& registers, const uint32_t vfat_mask)
{
    std::vector<std::optional<uint32_t>> result;

    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        std::optional<uint32_t> data = std::nullopt;
        if ((vfat_mask >> vfatN) & 0x1) {
            try {
                data = utils::readReg(registers[vfatN]);
            } catch (const xhal::memhub::memory_access_error& e) {
                LOG4CPLUS_WARN(logger, e.what());
                // data is still std::nullopt, i.e. the expectation for a failed read
            }
        }
        result.push_back(data);
    }

    return result;
}

// remote functions

uint32_t gem::hardware::vfat3::getSyncedVFAT::operator()(const uint32_t ohN) const
{
    uint32_t vfat_mask = 0;

    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        std::string regName = "BEFE.GEM.OH_LINKS.OH" + std::to_string(ohN) + ".VFAT" + std::to_string(vfatN);
        bool link_good = utils::readReg(regName + ".LINK_GOOD");
        uint32_t link_errors = utils::readReg(regName + ".SYNC_ERR_CNT");
        vfat_mask = vfat_mask | ((link_good && !link_errors) << vfatN);
    }

    return vfat_mask;
}

uint32_t gem::hardware::vfat3::getVFATMask::operator()(const uint32_t ohN) const
{
    return ~utils::readReg(fmt::format(FMT_STRING("BEFE.GEM.OH_LINKS.OH{}.VFAT_MASK"), ohN)) & oh::FULL_VFAT_MASK;
}

void gem::hardware::vfat3::setVFATMask::operator()(const uint32_t ohN, const uint32_t vfat_mask) const
{
    utils::writeReg(fmt::format(FMT_STRING("BEFE.GEM.OH_LINKS.OH{}.VFAT_MASK"), ohN), ~vfat_mask & oh::FULL_VFAT_MASK);
}

uint32_t gem::hardware::vfat3::updateVFATMask::operator()(const uint32_t ohN, const uint32_t vfat_mask) const
{
    // We are performing an atomic RWM
    std::lock_guard<decltype(*memhub)> lock(*memhub);

    const uint32_t sane_mask = vfat_mask & getVFATMask {}(ohN);
    setVFATMask {}(ohN, sane_mask);

    return sane_mask;
}

uint32_t gem::hardware::vfat3::broadcastWrite::operator()(const uint32_t ohN, const uint32_t vfat_mask, const std::string& register_name, const uint32_t value) const
{
    const auto registers = prepareBroadcast(ohN, register_name);
    return doBroadcastWrite(registers, vfat_mask, value);
}

std::vector<std::optional<uint32_t>> gem::hardware::vfat3::broadcastRead::operator()(const uint32_t ohN, const uint32_t vfat_mask, const std::string& register_name) const
{
    const auto registers = prepareBroadcast(ohN, register_name);
    return doBroadcastRead(registers, vfat_mask);
}

uint32_t gem::hardware::vfat3::enableAllChannels::operator()(const uint32_t ohN, const uint32_t vfat_mask) const
{
    return vfat3::enableChannel(ohN, vfat_mask, vfat3::VFAT_ALL_CHANNELS, true);
}

uint32_t gem::hardware::vfat3::confCalPulse::operator()(const uint32_t ohN, const uint32_t vfat_mask, const calPulseConfig& config) const
{
    uint32_t result = 0;

    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        if (!((vfat_mask >> vfatN) & 0x1))
            continue;

        try {
            const std::string register_base = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".GEB.VFAT" + std::to_string(vfatN);

            utils::writeReg(register_base + ".CFG_CAL_MODE", static_cast<uint32_t>(config.mode));

            if (config.mode == VFATCalibrationMode::VOLTAGE) {
                utils::writeReg(register_base + ".CFG_CAL_DAC", config.cal_dac);
                utils::writeReg(register_base + ".CFG_CAL_DUR", config.duration);
            } else if (config.mode == VFATCalibrationMode::CURRENT) {
                // Q = CAL DUR[s] * CAL DAC * 10nA * CAL FS[%] (00 = 25%, 01 = 50%, 10 = 75%, 11 = 100%)
                utils::writeReg(register_base + ".CFG_CAL_DAC", config.cal_dac);
                utils::writeReg(register_base + ".CFG_CAL_DUR", config.duration);
                utils::writeReg(register_base + ".CFG_CAL_FS", config.scale_factor);
            }
        } catch (const xhal::memhub::memory_access_error& e) {
            LOG4CPLUS_WARN(logger, e.what());
            // Register access error, do not set the success flag
            continue;
        }

        result |= (1ULL << vfatN);
    }

    return result;
}

uint32_t gem::hardware::vfat3::enableChannelCalPulse::operator()(const uint8_t ohN, const uint32_t vfat_mask, const uint8_t channel, const bool enable) const
{
    uint32_t result = 0;

    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        if (!((vfat_mask >> vfatN) & 0x1))
            continue;

        try {
            const std::string register_base = fmt::format(FMT_STRING("BEFE.GEM.OH.OH{}.GEB.VFAT{}.VFAT_CHANNELS.CHANNEL"), ohN, vfatN);

            if (channel != VFAT_ALL_CHANNELS) { // There is a single channel of interest
                const std::string register_name = register_base + std::to_string(channel) + ".CALPULSE_ENABLE";
                utils::writeReg(register_name, enable);
            } else { // Apply on all chanels
                for (size_t i = 0; i < 128; ++i) {
                    const std::string register_name = register_base + std::to_string(i) + ".CALPULSE_ENABLE";
                    utils::writeReg(register_name, enable);
                }
            }
        } catch (const xhal::memhub::memory_access_error& e) {
            LOG4CPLUS_WARN(logger, e.what());
            // Do not mark the VFAT as successfully configured
            continue;
        }

        result |= (1ULL << vfatN);
    }
    return result;
}

uint32_t gem::hardware::vfat3::configure::operator()(const uint16_t ohN, uint32_t vfatMask, const bool run) const
{
    LOG4CPLUS_INFO(logger, fmt::format(FMT_STRING("Configuring VFAT {:#x} on OptoHybrid {:d}"), vfatMask, ohN));

    // Try to configure all unmasked VFAT
    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        if (!((vfatMask >> vfatN) & 0x1))
            continue;

        const std::string config_filename = fmt::format(FMT_STRING("vfat/config-oh{}-vfat{}.cfg"), ohN, vfatN);
        const std::string register_base = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".GEB.VFAT" + std::to_string(vfatN) + ".";

        try {
            const auto data = utils::read_configuration_file(config_filename);
            for (const auto& i : data)
                utils::writeReg(register_base + i.first, i.second);

            utils::writeReg(register_base + "CFG_RUN", run ? 1 : 0);
        } catch (const xhal::memhub::memory_access_error& e) {
            // Configuring this VFAT failed, log and report the error, and continue
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Failed to configure OH {:d} - VFAT {:d} with {:s}"), ohN, vfatN, e.what()));
            vfatMask ^= (1 << vfatN);
        }
    }

    return vfatMask;
}

std::vector<uint32_t> gem::hardware::vfat3::getChannelRegistersVFAT3::operator()(const uint16_t& ohN, const uint32_t& vfatMask) const
{
    // Check VFAT synchronization
    const uint32_t goodVFATs = vfat3::getSyncedVFAT {}(ohN);
    if (vfatMask != goodVFATs) {
        std::stringstream errmsg;
        errmsg << "One of the enabled VFATs is not sync'd: "
               << "goodVFATs: 0x" << std::hex << std::setw(8) << std::setfill('0') << goodVFATs << std::dec;
        throw std::runtime_error(errmsg.str());
    }

    LOG4CPLUS_INFO(logger, "Read channel register settings");

    std::vector<uint32_t> chanRegData(oh::VFATS_PER_OH * 128);

    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        if (!((vfatMask >> vfatN) & 0x1)) {
            for (size_t chan = 0; chan < 128; ++chan) {
                const uint32_t idx = vfatN * 128 + chan;
                chanRegData.at(idx) = 0x0; // FIXME 0x0 or 0xdeaddead
                // chanRegData.push_back(0x0); // FIXME 0x0 or 0xdeaddead, only push_back/emplace_back if we don't preallocate
            }
            continue;
        }

        for (size_t chan = 0; chan < 128; ++chan) {
            const uint32_t idx = vfatN * 128 + chan;

            const std::string regName = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".GEB.VFAT" + std::to_string(vfatN) + ".VFAT_CHANNELS.CHANNEL" + std::to_string(chan);

            // Build the channel register
            LOG4CPLUS_INFO(logger, "Reading channel register for VFAT" << vfatN << " channel " << chan);
            chanRegData.at(idx) = utils::readReg(regName);
            // chanRegData.push_back(readRawAddress(chanAddr));  // FIXME only push_back/emplace_back if we don't preallocate
            std::this_thread::sleep_for(std::chrono::microseconds(200));
        }
    }

    return chanRegData;
}

void gem::hardware::vfat3::setChannelRegistersVFAT3Simple::operator()(const uint16_t& ohN, const uint32_t& vfatMask, const std::vector<uint32_t>& chanRegData) const
{
    // Check VFAT synchronization
    const uint32_t goodVFATs = vfat3::getSyncedVFAT {}(ohN);
    if (vfatMask != goodVFATs) {
        std::stringstream errmsg;
        errmsg << "One of the enabled VFATs is not sync'd: "
               << "goodVFATs: 0x" << std::hex << std::setw(8) << std::setfill('0') << goodVFATs << std::dec;
        throw std::runtime_error(errmsg.str());
    }

    if (chanRegData.size() != oh::VFATS_PER_OH * 128) {
        std::stringstream errmsg;
        errmsg << "The provided channel configuration data has the wrong size: "
               << chanRegData.size() << " != " << oh::VFATS_PER_OH * 128;
        throw std::runtime_error(errmsg.str());
    }

    LOG4CPLUS_INFO(logger, "Write channel register settings");
    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        if (!((vfatMask >> vfatN) & 0x1)) {
            continue;
        }

        for (size_t chan = 0; chan < 128; ++chan) {
            const uint32_t idx = vfatN * 128 + chan;

            const std::string regName = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".GEB.VFAT" + std::to_string(vfatN) + ".VFAT_CHANNELS.CHANNEL" + std::to_string(chan);
            utils::writeReg(regName, chanRegData[idx]);
            std::this_thread::sleep_for(std::chrono::microseconds(200));
        }
    }

    return;
}

void gem::hardware::vfat3::setChannelRegistersVFAT3::operator()(const uint16_t& ohN,
    const uint32_t& vfatMask,
    const std::vector<uint32_t>& calEnable,
    const std::vector<uint32_t>& masks,
    const std::vector<uint32_t>& trimARM,
    const std::vector<uint32_t>& trimARMPol,
    const std::vector<uint32_t>& trimZCC,
    const std::vector<uint32_t>& trimZCCPol) const
{
    // Check VFAT synchronization
    const uint32_t goodVFATs = vfat3::getSyncedVFAT {}(ohN);
    if (vfatMask != goodVFATs) {
        std::stringstream errmsg;
        errmsg << "One of the enabled VFATs is not sync'd: "
               << "goodVFATs: 0x" << std::hex << std::setw(8) << std::setfill('0') << goodVFATs << std::dec;
        throw std::runtime_error(errmsg.str());
    }

    // FIXME should run this check for all vectors?
    // FIXME should get rid of this function altogether?
    if (calEnable.size() != oh::VFATS_PER_OH * 128) {
        std::stringstream errmsg;
        errmsg << "The provided channel configuration data calEnable has the wrong size: "
               << calEnable.size() << " != " << oh::VFATS_PER_OH * 128;
        throw std::runtime_error(errmsg.str());
    }

    LOG4CPLUS_INFO(logger, "Write channel register settings");
    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        if (!((vfatMask >> vfatN) & 0x1)) {
            continue;
        }

        uint32_t chanRegVal = 0x0;
        for (size_t chan = 0; chan < 128; ++chan) {
            const uint32_t idx = vfatN * 128 + chan;

            const std::string regName = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".GEB.VFAT" + std::to_string(vfatN) + ".VFAT_CHANNELS.CHANNEL" + std::to_string(chan);

            if (trimARM[idx] > 0x3F || trimARM[idx] < 0x0) {
                std::stringstream errmsg;
                errmsg << "The arming comparator trim value must be positive in the range [0x0,0x3F]. Value given for VFAT"
                       << vfatN << " chan " << chan << ": 0x" << std::hex << std::setw(2) << std::setfill('0') << trimARM[idx] << std::dec;
                // FIXME should this throw or skip?
                // throw std::invalid_argument(errmsg.str());
                LOG4CPLUS_WARN(logger, errmsg.str());
                continue;
            } else if (trimZCC[idx] > 0x3F || trimZCC[idx] < 0x0) {
                std::stringstream errmsg;
                errmsg << "The zero crossing comparator trim value must be positive in the range [0x0,0x3F]. Value given for VFAT"
                       << vfatN << " chan " << chan << ": 0x" << std::hex << std::setw(2) << std::setfill('0') << trimZCC[idx] << std::dec;
                // FIXME should this throw or skip?
                // throw std::invalid_argument(errmsg.str());
                LOG4CPLUS_WARN(logger, errmsg.str());
                continue;
            }

            // Build the channel register
            LOG4CPLUS_INFO(logger, "Setting channel register for VFAT" << vfatN << " chan " << chan);
            chanRegVal = (calEnable[idx] << 15) + (masks[idx] << 14) + (trimZCCPol[idx] << 13) + (trimZCC[idx] << 7) + (trimARMPol[idx] << 6) + (trimARM[idx]);
            utils::writeReg(regName, chanRegVal);
            std::this_thread::sleep_for(std::chrono::microseconds(200)); // FIXME why?
        }
    }

    return;
}

std::map<std::string, std::vector<uint32_t>> gem::hardware::vfat3::statusVFAT3s::operator()(const uint16_t& ohN) const
{
    std::string regs[] = {
        "CFG_PULSE_STRETCH",
        "CFG_SYNC_LEVEL_MODE",
        "CFG_FP_FE",
        "CFG_RES_PRE",
        "CFG_CAP_PRE",
        "CFG_PT",
        "CFG_SEL_POL",
        "CFG_FORCE_EN_ZCC",
        "CFG_SEL_COMP_MODE",
        "CFG_VREF_ADC",
        "CFG_IREF",
        "CFG_THR_ARM_DAC",
        "CFG_LATENCY",
        "CFG_CAL_SEL_POL",
        "CFG_CAL_DAC",
        "CFG_CAL_MODE",
        "CFG_BIAS_CFD_DAC_2",
        "CFG_BIAS_CFD_DAC_1",
        "CFG_BIAS_PRE_I_BSF",
        "CFG_BIAS_PRE_I_BIT",
        "CFG_BIAS_PRE_I_BLCC",
        "CFG_BIAS_PRE_VREF",
        "CFG_BIAS_SH_I_BFCAS",
        "CFG_BIAS_SH_I_BDIFF",
        "CFG_BIAS_SH_I_BFAMP",
        "CFG_BIAS_SD_I_BDIFF",
        "CFG_BIAS_SD_I_BSF",
        "CFG_BIAS_SD_I_BFCAS",
        "CFG_RUN"
    };

    std::map<std::string, std::vector<uint32_t>> values;

    LOG4CPLUS_INFO(logger, "Reading VFAT3 status");

    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        std::string regBase = "BEFE.GEM.OH_LINKS.OH" + std::to_string(ohN) + ".VFAT" + std::to_string(vfatN) + ".";
        for (auto& reg : regs) {
            values[reg].push_back(utils::readReg(regBase + reg));
        }
    }

    return values;
}

std::vector<std::optional<uint32_t>> gem::hardware::vfat3::getVFAT3ChipIDs::operator()(const uint16_t ohN, const uint32_t vfatMask, const bool rawID) const
{
    std::vector<std::optional<uint32_t>> chipIDs;

    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        if (!((vfatMask >> vfatN) & 0x1)) {
            chipIDs.push_back(std::nullopt);
            continue;
        }

        uint32_t chipID = 0;
        try {
            const std::string regName = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".GEB.VFAT" + std::to_string(vfatN) + ".HW_CHIP_ID";
            uint32_t chipID = utils::readReg(regName);

            if (rawID) {
                chipIDs.push_back(chipID);
            } else {
                const uint16_t decChipID = decodeChipID(chipID);
                std::stringstream msg;
                msg << "OH" << ohN << "::VFAT" << vfatN << ": chip ID is:"
                    << std::hex << std::setw(8) << std::setfill('0') << chipID << std::dec
                    << "(raw) or "
                    << std::hex << std::setw(8) << std::setfill('0') << decChipID << std::dec
                    << "(decoded)";
                LOG4CPLUS_INFO(logger, msg.str());

                chipIDs.push_back(decChipID);
            }
        } catch (const xhal::memhub::memory_access_error& e) {
            std::stringstream errmsg;
            errmsg << "Error while reading chip ID: " << e.what();
            LOG4CPLUS_ERROR(logger, errmsg.str());
            chipIDs.push_back(std::nullopt);
        } catch (const std::runtime_error& e) {
            std::stringstream errmsg;
            errmsg << "Error while decoding chip ID 0x" << std::hex << chipID << ": " << e.what();
            LOG4CPLUS_ERROR(logger, errmsg.str());
            chipIDs.push_back(std::nullopt);
        }
    }

    return chipIDs;
}
