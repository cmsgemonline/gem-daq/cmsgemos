/// @file

#include <gem/hardware/expert.h>

#include "gbt.h"
#include "hw_constants.h"
#include "utils.h"
#include "vfat3.h"
#include <gem/hardware/amc/daq.h>
#include <gem/hardware/blaster.h>

#include <fmt/format.h>

uint32_t gem::hardware::expert::maskUnstableVFAT::operator()(uint32_t const optohybridMask) const
{
    uint32_t empty_oh_mask = 0;
    uint32_t number_of_vfat_masked = 0;

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (((optohybridMask >> ohN) & 0x1) == 0)
            continue;

        // Just for accounting
        const uint32_t original_mask = vfat3::getVFATMask {}(ohN);

        // Keep only the sync'ed VFAT with locked GBT
        const uint32_t gbt_sync_mask = std::get<1>(gbt::getReachableFrontend(gbt::getLockedGBT {}(ohN, true /* history matters */)));
        const uint32_t in_sync_mask = vfat3::getSyncedVFAT {}(ohN);

        // Update the VFAT mask
        const uint32_t final_mask = vfat3::updateVFATMask {}(ohN, gbt_sync_mask & in_sync_mask);

        // No VFAT enabled? Mark the OH as "empty"
        if (!final_mask)
            empty_oh_mask |= (1ULL << ohN);

        // Some accounting
        for (uint32_t mask = original_mask; mask != 0; mask >>= 1)
            if (mask & 0x1)
                ++number_of_vfat_masked;
        for (uint32_t mask = final_mask; mask != 0; mask >>= 1)
            if (mask & 0x1)
                --number_of_vfat_masked;
    }

    // Mask the OptoHybrids that do not have any VFAT enabled
    const uint32_t new_oh_mask = amc::daq::getDAQLinkInputMask {}() & ~empty_oh_mask;
    amc::daq::setDAQLinkInputMask {}(new_oh_mask);

    return number_of_vfat_masked;
}

uint32_t gem::hardware::expert::recoverSEU::operator()(uint32_t const optohybridMask) const
{
    uint32_t nRecoveredOH = 0;

#if !defined(GEM_IS_ME0)
    // Establish the list of OptoHybrid FPGA with visible uncorrectable SEU
    uint32_t semMask = 0;
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (((optohybridMask >> ohN) & 0x1) == 0)
            continue;

        const std::string semBase = fmt::format(FMT_STRING("BEFE.GEM.OH.OH{:d}.FPGA.CONTROL.SEM."), ohN);

        try {
            // The SEM IP core stops correcting after the first uncorrectable error
            if (utils::readReg(semBase + "CNT_SEM_UNCORRECTABLE") != 0) {
                semMask |= (1ULL << ohN);
                continue;
            }

            // Reconfigure the FPGA for any kind of error
            const auto state = utils::readReg(semBase + "SEM_STATE");
            switch (state) {
            case 0x01: // Idle
            case 0x02: // Initialization
            case 0x04: // Observation
            case 0x08: // Correction
            case 0x10: // Classification
            case 0x20: // Injection
                break;
            case 0x3e: // Fatal error
            default: // Unknown error
                semMask |= (1ULL << ohN);
                continue;
            }

            // The SEM must be alive if in Observation state
            if (state == 0x04 && utils::readReg(semBase + "SEM_ALIVE") != 1) {
                semMask |= (1ULL << ohN);
                continue;
            }
        } catch (const xhal::memhub::memory_access_error& e) {
            // NOTE Ignore communication failures for now. Will need to
            // understand from ops if reprogramming is desired in this case as
            // well. If so, better control & monitoring of the automasker will
            // likely be need as well.
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Cannot communicate with OptoHybrid FPGA {:d}: {:s}"), ohN, e.what()));
        }
    }

    // No error detected
    if (semMask == 0)
        return 0;

    // Recovering...
    LOG4CPLUS_INFO(logger, fmt::format(FMT_STRING("Recovering SEU in OptoHybrid FPGA: {:#x}"), semMask));
    const uint32_t recoveredMask = blaster::configureOptoHybridFPGA {}(semMask);
    LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Recovered SEU in OptoHybrid FPGA: {:#x} ({:#x} failed)"), recoveredMask, ~recoveredMask & semMask));

    // Some accounting
    for (uint32_t mask = recoveredMask; mask != 0; mask >>= 1)
        if (mask & 0x1)
            ++nRecoveredOH;
#endif

    return nRecoveredOH;
}
