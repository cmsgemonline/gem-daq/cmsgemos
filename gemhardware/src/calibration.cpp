/// @file

#include <gem/hardware/calibration.h>

#include "exception.h"
#include "gbt.h"
#include "hw_constants.h"
#include "lpgbt.h"
#include "utils.h"
#include "vfat3.h"
#include <gem/hardware/amc.h>
#include <gem/hardware/amc/daq.h>

#include <fmt/format.h>

#include <chrono>
#include <math.h>
#include <mutex>
#include <numeric>
#include <optional>
#include <thread>

namespace /* anonymous */ {

/// @brief Tries to perform a register write to the S-bit counters
///
/// @param @c ohN The OptoHybrid index
/// @param @c regName The local register name
/// @param @c value The value to write
///
/// @return @c true if the write was successful, @c false otherwise
bool writeSbitCounter(const uint32_t ohN, const std::string& regName, const uint32_t value)
{
#if !defined(GEM_IS_ME0)
    try {
        const std::string fullName = fmt::format(FMT_STRING("BEFE.GEM.OH.OH{:d}.FPGA.TRIG.CNT.{:s}"), ohN, regName);
        gem::hardware::utils::writeReg(fullName, value);
    } catch (const xhal::memhub::memory_access_error&) {
        return false;
    }
#else
    const std::string fullName = fmt::format(FMT_STRING("BEFE.GEM.SBIT_ME0.OH{:d}.CNT.{:s}"), ohN, regName);
    gem::hardware::utils::writeReg(fullName, value);
#endif
    return true;
}

/// @brief Tries to perform a register read to the S-bit counters
///
/// @param @c ohN The OptoHybrid index
/// @param @c regName The local register name
///
/// @return An @c std::optional containing the if the read was successful, and containing @c std::nullopt otherwise
std::optional<std::uint32_t> readSbitCounter(const uint32_t ohN, const std::string& regName)
{
#if !defined(GEM_IS_ME0)
    try {
        const std::string fullName = fmt::format(FMT_STRING("BEFE.GEM.OH.OH{:d}.FPGA.TRIG.CNT.{:s}"), ohN, regName);
        return gem::hardware::utils::readReg(fullName);
    } catch (const xhal::memhub::memory_access_error&) {
        return std::nullopt;
    }
#else
    const std::string fullName = fmt::format(FMT_STRING("BEFE.GEM.SBIT_ME0.OH{:d}.CNT.{:s}"), ohN, regName);
    return gem::hardware::utils::readReg(fullName);
#endif
}

} // anonymous namespace

void gem::hardware::calibration::ttcGenToggle::operator()(const bool enable) const
{
    if (enable) {
        // Internal TTC generator enabled, TTC cmds from backplane are ignored
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.ENABLE", 0x1);
    } else {
        // Internal TTC generator disabled, TTC cmds from backplane
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.ENABLE", 0x0);
    }
}

void gem::hardware::calibration::ttcGenConf::operator()(const uint32_t pulseDelay, const uint32_t l1aInterval,
    const bool enable) const
{
    LOG4CPLUS_INFO(logger, "Entering ttcGenConf");

    utils::writeReg("BEFE.GEM.TTC.GENERATOR.RESET", 0x1);
    utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_L1A_GAP", l1aInterval);
    utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_CALPULSE_TO_L1A_GAP", pulseDelay);

    ttcGenToggle {}(enable);
}

gem::hardware::calibration::clusterMaskScanResult gem::hardware::calibration::clusterMaskRateScanParallel::operator()(uint32_t ohMask,
    const uint8_t min,
    const uint8_t max,
    const uint32_t waitTime,
    const uint32_t l1aInterval) const
{
    calibration::clusterMaskScanResult outData;

#if !defined(GEM_IS_ME0)
    // - Hold the critical TTC: configure it for 100 KHz
    utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_L1A_GAP", l1aInterval);

    // Initialize the system
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!((ohMask >> ohN) & 0x1))
            continue;

        // Configure the counters to reset after SBIT_CNT_TIME_MAX BX
        try {
            const std::string regBase = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".FPGA.TRIG.CNT.SBIT_CNT_";
            utils::writeReg(regBase + "PERSIST", 0x0);
            utils::writeReg(regBase + "TIME_MAX", static_cast<uint32_t>(0x02638e98 * waitTime));
        } catch (const xhal::memhub::memory_access_error& e) {
            // Cannot configure the coutners, disable the OH
            ohMask ^= (1ULL << ohN);
        }
    }

    // Scan each point
    for (int32_t delay = min - 2; delay <= max; delay += 1) {
        LOG4CPLUS_INFO(logger, fmt::format(FMT_STRING("Setting delay to {:d} for all OptoHybrids in {:#03x}"), delay, ohMask));
        // First point in the loop min -2 : measure the rate not having the  L1A started yet.
        // Second point in the loop  min-1 : measure the rate with trigger enabled at 100KHz
        uint32_t bitmask = (delay < min) ? 0 : (1ULL << delay);

        if (delay == min - 1) {
            ttcGenToggle {}(true);
            // Send triggers
            utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_START", 0x1);
            utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_L1A_COUNT", 0x0);
        }

        // Prepare the new scan point
        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!((ohMask >> ohN) & 0x1))
                continue;

            // Set the scan register value
            const std::string bitmaskReg = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".FPGA.TRIG.L1A_MASK.L1A_MASK_BITMASK";

            // Write the dealy and width and reset the counters
            try {
                utils::writeReg(bitmaskReg, bitmask);
                utils::writeReg("BEFE.GEM.OH.OH" + std::to_string(ohN) + ".FPGA.TRIG.CNT.RESET", 0x1);
            } catch (const xhal::memhub::memory_access_error&) {
                // Don't read the rate if the counters could not be reset add it to the OhMask
                ohMask ^= (1ULL << ohN);
            }
        }

        std::this_thread::sleep_for(std::chrono::seconds(waitTime));

        // Read the counters
        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!((ohMask >> ohN) & 0x1))
                continue;

            calibration::clusterMaskRate t_clusterMask = {
                .delay = 0,
                .cluster_rate_unmasked = 0,
                .cluster_rate_masked = 0,
                .trigger_rate = 0,
                .l1a_period = 0,
            };

            if (delay == min - 2) {
                // scan point with masking disabled and L1A disabled
                t_clusterMask.delay = -2;
                t_clusterMask.l1a_period = 0;
            } else if (delay == min - 1) {
                // scan point with masking disabled and L1A enabled
                t_clusterMask.delay = -1;
                t_clusterMask.l1a_period = l1aInterval;
            } else {
                t_clusterMask.delay = delay;
                t_clusterMask.l1a_period = l1aInterval;
            }

            try {
                t_clusterMask.trigger_rate = utils::readReg(fmt::format(FMT_STRING("BEFE.GEM.OH.OH{:d}.FPGA.TRIG.CNT.CLUSTER_COUNT"), ohN));
                t_clusterMask.cluster_rate_unmasked = utils::readReg(fmt::format(FMT_STRING("BEFE.GEM.OH.OH{:d}.FPGA.CONTROL.SBITS.CLUSTER_RATE"), ohN));
                t_clusterMask.cluster_rate_masked = utils::readReg(fmt::format(FMT_STRING("BEFE.GEM.OH.OH{:d}.FPGA.CONTROL.SBITS.CLUSTER_RATE_MASKED"), ohN));
                outData[ohN].push_back(t_clusterMask);
            } catch (const xhal::memhub::memory_access_error&) {
                // We cannot read the value, ok, we don't store it...
            }
        }
    }
#endif

    return outData;
}
gem::hardware::calibration::sbitRateScanResult gem::hardware::calibration::sbitRateScanParallel::operator()(uint32_t ohMask,
    const uint8_t ch,
    const uint16_t dacMin,
    const uint16_t dacMax,
    const uint16_t dacStep,
    const std::string& scanReg,
    const uint32_t waitTime,
    const bool toggleRunMode) const
{
    calibration::sbitRateScanResult outData;

    std::vector<uint32_t> vfatmask(amc::number_oh(), 0);

    // Initialize the system
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!((ohMask >> ohN) & 0x1))
            continue;

        vfatmask.at(ohN) = vfat3::getVFATMask {}(ohN);

        // Disable the hysteresis feature when playing with the threshold
        if (scanReg == "CFG_THR_ARM_DAC")
            vfatmask.at(ohN) &= vfat3::broadcastWrite {}(ohN, vfatmask.at(ohN), "CFG_EN_HYST", 0);

        // Apply channel masking if desired
        if (ch != vfat3::VFAT_ALL_CHANNELS) {
            vfatmask.at(ohN) &= vfat3::enableChannel(ohN, vfatmask.at(ohN), vfat3::VFAT_ALL_CHANNELS, false);
            vfatmask.at(ohN) &= vfat3::enableChannel(ohN, vfatmask.at(ohN), ch, true);
        }

        // Toggle the VFAT run mode
        if (toggleRunMode) {
            vfatmask.at(ohN) &= vfat3::broadcastWrite {}(ohN, vfatmask.at(ohN), "CFG_RUN", 0x0);
            vfatmask.at(ohN) &= vfat3::broadcastWrite {}(ohN, vfatmask.at(ohN), "CFG_RUN", 0x1);
        }

        // Configure the counters to reset after SBIT_CNT_TIME_MAX BX
        if (!writeSbitCounter(ohN, "SBIT_CNT_PERSIST", 0x0)
            || !writeSbitCounter(ohN, "SBIT_CNT_TIME_MAX", 0x02638e98 * waitTime)) {
            // Cannot configure the counters, disable the OH
            ohMask ^= (1ULL << ohN);
            vfatmask.at(ohN) = 0;
        }

        LOG4CPLUS_INFO(logger, fmt::format(FMT_STRING("VFAT mask for OH{:d} determined to be {:#08x}"), ohN, vfatmask.at(ohN)));
    }

    // Scan each point
    for (int32_t dacVal = dacMax; dacVal >= dacMin; dacVal -= dacStep) {
        LOG4CPLUS_INFO(logger, fmt::format(FMT_STRING("Setting {:s} to {:d} for all OptoHybrids in {:#03x}"), scanReg, dacVal, ohMask));

        // Prepare the new scan point
        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!((ohMask >> ohN) & 0x1))
                continue;

            // Set the scan register value
            vfatmask.at(ohN) &= vfat3::broadcastWrite {}(ohN, vfatmask.at(ohN), scanReg, dacVal);

            // Reset the counters
            if (!writeSbitCounter(ohN, "RESET", 0x1)) {
                // Mask the whole OH if the counters could not be reset
                ohMask ^= (1ULL << ohN);
                vfatmask.at(ohN) = 0;
            }
        }

        std::this_thread::sleep_for(std::chrono::seconds(waitTime));

        // Read the counters
        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!((ohMask >> ohN) & 0x1))
                continue;

            for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
                if (!((vfatmask.at(ohN) >> vfatN) & 0x1))
                    continue;

                const std::string vfatRegName = fmt::format(FMT_STRING("VFAT{:d}_SBITS"), vfatN);
                if (const auto value = readSbitCounter(ohN, vfatRegName)) {
                    outData[ohN][vfatN][dacVal] = *value;
                } else {
                    // Skip the point and mask the VFAT
                    vfatmask.at(ohN) ^= (1ULL << vfatN);
                }
            }
        }
    }

    // Clean up incomplete scans
    for (auto ohIt = outData.begin(); ohIt != outData.end();) {
        // All DAC values need to be present for a given VFAT
        for (auto vfatIt = ohIt->second.cbegin(); vfatIt != ohIt->second.cend();) {
            if (vfatIt->second.size() != (dacMax - dacMin) / dacStep + 1)
                vfatIt = ohIt->second.erase(vfatIt);
            else
                ++vfatIt;
        }

        // Remove the OH if it has no VFAT left
        if (ohIt->second.size() == 0)
            ohIt = outData.erase(ohIt);
        else
            ++ohIt;
    }

    return outData;
}

std::vector<uint32_t> gem::hardware::calibration::checkSbitMappingWithCalPulse::operator()(const uint16_t& ohN,
    const uint32_t& vfatN,
    const uint32_t& vfatMask,
    const bool& useCalPulse,
    const bool& currentPulse,
    const uint32_t& calScaleFactor,
    const uint32_t& nevts,
    const uint32_t& l1aInterval,
    const uint32_t& pulseDelay) const
{
    // Calpulse parameters sanity check
    if (currentPulse && calScaleFactor > 3) {
        std::stringstream errmsg;
        errmsg << "Bad value for CFG_CAL_FS: 0x"
               << std::hex << calScaleFactor << std::dec
               << ". Possible values are {0b00, 0b01, 0b10, 0b11}";
        throw std::runtime_error(errmsg.str());
    }

    // Check VFAT synchronization
    const uint32_t goodVFATs = vfat3::getSyncedVFAT {}(ohN);
    if (vfatMask != goodVFATs) {
        std::stringstream errmsg;
        errmsg << "One of the enabled VFATs is not sync'd: "
               << "goodVFATs: 0x" << std::hex << std::setw(8) << std::setfill('0') << goodVFATs << std::dec;
        throw std::runtime_error(errmsg.str());
    }

    std::vector<uint32_t> outData;

    // Get current channel register data, mask all channels and disable calpulse
    auto const chanRegData_orig = vfat3::getChannelRegistersVFAT3 {}(ohN, vfatMask);
    std::vector<uint32_t> chanRegData_tmp(oh::CHANNELS_PER_OH);
    for (size_t idx = 0; idx < oh::CHANNELS_PER_OH; ++idx) {
        chanRegData_tmp[idx] = chanRegData_orig[idx] + (0x1 << 14); // set channel mask to true
        chanRegData_tmp[idx] = chanRegData_tmp[idx] - (0x1 << 15); // disable calpulse
    }
    vfat3::setChannelRegistersVFAT3Simple {}(ohN, vfatMask, chanRegData_tmp);

    // Setup TTC Generator
    ttcGenConf {}(pulseDelay, l1aInterval, true);
    utils::writeReg("BEFE.GEM.TTC.GENERATOR.SINGLE_RESYNC", 0x1);
    utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_L1A_COUNT", 0x1); // One pulse at a time
    const utils::RegInfo ttcStartReg = utils::getReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_START");

    // Set all chips out of run mode
    vfat3::broadcastWrite {}(ohN, vfatMask, "CFG_RUN", 0x0);

    // Take the VFATs out of slow control only mode
    utils::writeReg("BEFE.GEM.GEM_SYSTEM.VFAT3.SC_ONLY_MODE", 0x0);

    // Setup the s-bit monitor
    const uint32_t nclusters = 8;
    utils::writeReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.OH_SELECT", ohN);
    const utils::RegInfo sbitMonResetReg = utils::getReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.RESET");
    utils::RegInfo sbitClusterReg[nclusters];
    for (size_t iCluster = 0; iCluster < nclusters; ++iCluster) {
        sbitClusterReg[iCluster] = utils::getReg("BEFE.GEM.TRIGGER.SBIT_MONITOR.CLUSTER" + std::to_string(iCluster));
    }

    if (!((vfatMask >> vfatN) & 0x1)) {
        std::stringstream errmsg;
        errmsg << "The VFAT of interest " << vfatN << " should be part of the vfatMask 0x"
               << std::hex << std::setw(8) << std::setfill('0') << vfatMask << std::dec;
        throw std::runtime_error(errmsg.str());
    }

    const std::string regBase = "BEFE.GEM.OH.OH" + std::to_string(ohN);
    const std::string vfatRegBase = regBase + ".GEB.VFAT" + std::to_string(vfatN);
    // mask all other vfats from trigger
    const uint32_t vfatN_as_mask = 0x1 << vfatN;
    const uint32_t vfatN_as_mask_trigger = ~vfatN_as_mask & 0xffffff;
    utils::writeReg(regBase + ".FPGA.TRIG.CTRL.VFAT_MASK", vfatN_as_mask_trigger);

    // Configure the calpulsing
    vfat3::calPulseConfig t_calpulse_config;
    if (useCalPulse && currentPulse)
        t_calpulse_config.mode = vfat3::VFATCalibrationMode::CURRENT;
    if (useCalPulse && !currentPulse)
        t_calpulse_config.mode = vfat3::VFATCalibrationMode::VOLTAGE;
    t_calpulse_config.scale_factor = calScaleFactor;
    vfat3::confCalPulse {}(ohN, vfatN_as_mask, t_calpulse_config);

    // Place this vfat into run mode
    utils::writeReg(vfatRegBase + ".CFG_RUN", 0x1);

    for (size_t chan = 0; chan < 128; ++chan) {
        utils::writeReg(vfatRegBase + ".VFAT_CHANNELS.CHANNEL" + std::to_string(chan) + ".MASK", 0x0);

        // Turn on the calpulse for this channel
        if (vfatN_as_mask != vfat3::enableChannelCalPulse {}(ohN, vfatN_as_mask, chan, useCalPulse)) {
            // Calibration pulse is not configured correctly
            std::stringstream errmsg;
            errmsg << "Unable to configure CalPulse " << useCalPulse
                   << " for " << ohN << " mask 0x"
                   << std::hex << std::setw(8) << std::setfill('0') << (~(0x1 << vfatN) & 0xffffff) << std::dec
                   << "channel " << chan;
        }

        // Start Pulsing
        for (size_t iPulse = 0; iPulse < nevts; ++iPulse) {
            // Reset monitors
            utils::writeReg(sbitMonResetReg, 0x1);

            // Start the TTC Generator
            if (useCalPulse) {
                utils::writeReg(ttcStartReg, 0x1);
            }

            // Sleep for 200 us + pulseDelay * 25 ns * (0.001 us / ns)
            std::this_thread::sleep_for(std::chrono::microseconds(200 + static_cast<int>(ceil(pulseDelay * 25 * 0.001))));

            // Check clusers
            for (size_t cluster = 0; cluster < nclusters; ++cluster) {
                // bits [10:0] is the address of the cluster
                // bits [14:12] is the cluster size
                // bits 15 and 11 are not used
                const uint32_t thisCluster = utils::readReg(sbitClusterReg[cluster]);
                const uint32_t clusterSize = (thisCluster >> 12) & 0x7;
                const uint32_t sbitAddress = (thisCluster & 0x7ff);
                const bool isValid = (sbitAddress < oh::SBITS_PER_OH); // Possible values are [0,(oh::SBITS_PER_OH)-1]
                const int vfatObserved = 7 - static_cast<int>(sbitAddress / 192) + static_cast<int>((sbitAddress % 192) / 64) * 8;
                const int sbitObserved = sbitAddress % 64;

                outData.push_back(((clusterSize & 0x7) << 27)
                    + ((isValid & 0x1) << 26)
                    + ((vfatObserved & 0x1f) << 21)
                    + ((vfatN & 0x1f) << 16)
                    + ((sbitObserved & 0xff) << 8)
                    + (chan & 0xff));

                if (isValid) {
                    LOG4CPLUS_INFO(logger, "valid sbit data: useCalPulse " << useCalPulse << "; thisClstr 0x" << std::hex << thisCluster << std::dec << "; clstrSize 0x" << std::hex << clusterSize << std::dec << "; sbitAddr 0x" << std::hex << sbitAddress << std::dec << "; isValid 0x" << std::hex << isValid << std::dec << "; vfatN " << vfatN << "; vfatObs " << vfatObserved << "; chan " << chan << "; sbitObs " << sbitObserved);
                }
            }
        }

        // Turn off the calpulse for this channel
        if (vfatN_as_mask != vfat3::enableChannelCalPulse {}(ohN, vfatN_as_mask, chan, false)) {
            // Calibration pulse is not configured correctly
            std::stringstream errmsg;
            errmsg << "Unable to configure CalPulse OFF for OH" << ohN << " with mask 0x"
                   << std::hex << std::setw(6) << std::setfill('0') << (~((0x1) << vfatN) & 0xffffff) << std::dec
                   << " channel " << chan;
            throw std::runtime_error(errmsg.str());
        }

        // mask this channel
        utils::writeReg("BEFE.GEM.OH.OH" + std::to_string(ohN)
                + ".GEB.VFAT" + std::to_string(vfatN)
                + ".VFAT_CHANNELS.CHANNEL" + std::to_string(chan) + ".MASK",
            0x1);
    }

    utils::writeReg(vfatRegBase + ".CFG_RUN", 0x0);

    ttcGenToggle {}(false);

    // Return channel register settings to their original values
    vfat3::setChannelRegistersVFAT3Simple {}(ohN, vfatMask, chanRegData_orig);

    // Set trigger vfat mask for this OH back to 0
    utils::writeReg(regBase + ".FPGA.TRIG.CTRL.VFAT_MASK", 0x0);

    return outData;
}

std::map<std::string, std::vector<uint32_t>> gem::hardware::calibration::checkSbitRateWithCalPulse::operator()(const uint16_t& ohN,
    const uint32_t& vfatN,
    const uint32_t& vfatMask,
    const bool& useCalPulse,
    const bool& currentPulse,
    const uint32_t& calScaleFactor,
    const uint32_t& waitTime,
    const uint32_t& pulseRate,
    const uint32_t& pulseDelay) const
{
    // Calpulse parameters sanity check
    if (currentPulse && calScaleFactor > 3) {
        std::stringstream errmsg;
        errmsg << "Bad value for CFG_CAL_FS: 0x"
               << std::hex << calScaleFactor << std::dec
               << ". Possible values are {0b00, 0b01, 0b10, 0b11}";
        throw std::runtime_error(errmsg.str());
    }

    // Check VFAT synchronization
    const uint32_t goodVFATs = vfat3::getSyncedVFAT {}(ohN);
    if (vfatMask != goodVFATs) {
        std::stringstream errmsg;
        errmsg << "One of the enabled VFATs is not sync'd: "
               << "goodVFATs: 0x" << std::hex << std::setw(8) << std::setfill('0') << goodVFATs << std::dec;
        throw std::runtime_error(errmsg.str());
    }

    std::map<std::string, std::vector<uint32_t>> outData;

    // Get current channel register data, mask all channels and disable calpulse
    LOG4CPLUS_INFO(logger, "Storing VFAT3 channel registers for ohN " << ohN);
    auto const chanRegData_orig = vfat3::getChannelRegistersVFAT3 {}(ohN, vfatMask);
    std::vector<uint32_t> chanRegData_tmp(oh::CHANNELS_PER_OH);
    LOG4CPLUS_INFO(logger, "Masking all channels and disabling CalPulse for VFATs on ohN " << ohN);

    for (size_t idx = 0; idx < oh::CHANNELS_PER_OH; ++idx) {
        chanRegData_tmp.at(idx) = chanRegData_orig.at(idx) + (0x1 << 14); // set channel mask to true
        chanRegData_tmp.at(idx) = chanRegData_tmp.at(idx) - (0x1 << 15); // disable calpulse
    }
    vfat3::setChannelRegistersVFAT3Simple {}(ohN, vfatMask, chanRegData_tmp);

    // Setup TTC Generator
    uint32_t l1aInterval = 0;
    if (pulseRate > 0) {
        l1aInterval = int(40079000 / pulseRate);
    } else {
        l1aInterval = 0;
    }

    const utils::RegInfo ttcResetReg = utils::getReg("BEFE.GEM.TTC.GENERATOR.RESET");
    const utils::RegInfo ttcStartReg = utils::getReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_START");

    const std::string regBase = "BEFE.GEM.OH.OH" + std::to_string(ohN);
    const std::string vfatRegBase = regBase + ".GEB.VFAT" + std::to_string(vfatN);

    // Get Trigger addresses
    utils::RegInfo ohTrgRateReg[oh::VFATS_PER_OH + 2];
    // idx 0->oh::VFATS_PER_OH VFAT counters; idx oh::VFATS_PER_OH+1 - rate measured by OH FPGA; last idx - rate measured by CTP7
    for (size_t vfat = 0; vfat < oh::VFATS_PER_OH; ++vfat) {
        ohTrgRateReg[vfat] = utils::getReg(regBase + ".FPGA.TRIG.CNT.VFAT" + std::to_string(vfat) + "_SBITS");
    }
    ohTrgRateReg[oh::VFATS_PER_OH] = utils::getReg(regBase + ".FPGA.TRIG.CNT.CLUSTER_COUNT");
    ohTrgRateReg[oh::VFATS_PER_OH + 1] = utils::getReg("BEFE.GEM.TRIGGER.OH" + std::to_string(ohN) + ".TRIGGER_RATE");
    const utils::RegInfo trgCntResetOHReg = utils::getReg(regBase + ".FPGA.TRIG.CNT.RESET");
    const utils::RegInfo trgCntResetCTP7Reg = utils::getReg("BEFE.GEM.TRIGGER.CTRL.CNT_RESET");

    // Set all chips out of run mode
    LOG4CPLUS_INFO(logger, "Writing CFG_RUN to 0x0 for all VFATs on OH" << ohN << " using vfatMask 0x" << std::hex << std::setw(8) << std::setfill('0') << vfatMask << std::dec);
    vfat3::broadcastWrite {}(ohN, vfatMask, "CFG_RUN", 0x0);

    // Take the VFATs out of slow control only mode
    LOG4CPLUS_INFO(logger, "Taking VFAT3s out of slow control only mode");
    utils::writeReg("BEFE.GEM.GEM_SYSTEM.VFAT3.SC_ONLY_MODE", 0x0);

    // Prep the s-bit counters
    LOG4CPLUS_INFO(logger, "Preping s-bit counters for OH" << ohN);
    utils::writeReg(regBase + ".FPGA.TRIG.CNT.SBIT_CNT_PERSIST", 0x0); // reset all counters after SBIT_CNT_TIME_MAX
    utils::writeReg(regBase + ".FPGA.TRIG.CNT.SBIT_CNT_TIME_MAX", uint32_t(0x02638e98 * waitTime / 1000.));
    // count for a number of BX's specified by waitTime

    if (!((vfatMask >> vfatN) & 0x1)) {
        std::stringstream errmsg;
        errmsg << "The VFAT of interest " << vfatN << " should be part of the vfatMask 0x"
               << std::hex << std::setw(8) << std::setfill('0') << vfatMask << std::dec;
        throw std::runtime_error(errmsg.str());
    }

    const uint32_t vfatN_as_mask = 0x1 << vfatN;
    const uint32_t vfatN_as_mask_trigger = ~vfatN_as_mask & 0xffffff;
    LOG4CPLUS_INFO(logger, "Masking VFATs 0x" << std::hex << std::setw(8) << std::setfill('0') << vfatN_as_mask_trigger << std::dec << " from trigger in ohN " << ohN);
    utils::writeReg(regBase + ".FPGA.TRIG.CTRL.VFAT_MASK", vfatN_as_mask_trigger);

    // Configure the calpulsing
    vfat3::calPulseConfig t_calpulse_config;
    if (useCalPulse && currentPulse)
        t_calpulse_config.mode = vfat3::VFATCalibrationMode::CURRENT;
    if (useCalPulse && !currentPulse)
        t_calpulse_config.mode = vfat3::VFATCalibrationMode::VOLTAGE;
    t_calpulse_config.scale_factor = calScaleFactor;
    vfat3::confCalPulse {}(ohN, vfatN_as_mask, t_calpulse_config);

    LOG4CPLUS_INFO(logger, "Placing VFAT" << vfatN << " on OH" << ohN << " in run mode");
    utils::writeReg(regBase + ".GEB.VFAT" + std::to_string(vfatN) + ".CFG_RUN", 0x1);

    LOG4CPLUS_INFO(logger, "Looping over all channels of VFAT" << vfatN << " on OH" << ohN);
    for (size_t chan = 0; chan < 128; ++chan) {
        const std::string chRegBase = vfatRegBase + ".VFAT_CHANNELS.CHANNEL" + std::to_string(chan);
        LOG4CPLUS_INFO(logger, "Unmasking channel " << chan << " on VFAT" << vfatN << " of OH" << ohN);
        utils::writeReg(chRegBase + ".MASK", 0x0);

        LOG4CPLUS_INFO(logger, "Enabling CalPulse for channel " << chan << " on VFAT" << vfatN << " of OH" << ohN);
        if (vfatN_as_mask != vfat3::enableChannelCalPulse {}(ohN, vfatN_as_mask, chan, useCalPulse)) {
            // Calibration pulse is not configured correctly
            std::stringstream errmsg;
            errmsg << "Unable to configure CalPulse " << useCalPulse << " for " << ohN
                   << " mask 0x" << std::hex << std::setw(8) << std::setfill('0') << vfatN_as_mask << std::dec
                   << " channel " << chan;
            throw std::runtime_error(errmsg.str());
        }

        // Reset counters
        LOG4CPLUS_INFO(logger, "Reseting trigger counters on OH & CTP7");
        utils::writeReg(trgCntResetOHReg, 0x1);
        utils::writeReg(trgCntResetCTP7Reg, 0x1);

        // Start the TTC Generator
        LOG4CPLUS_INFO(logger, "Configuring TTC Generator to use OH" << ohN << " with pulse delay " << pulseDelay << " and l1aInterval " << l1aInterval);
        ttcGenConf {}(pulseDelay, l1aInterval, true);
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.SINGLE_RESYNC", 0x1);
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_L1A_COUNT", 0x0);
        LOG4CPLUS_INFO(logger, "Starting TTC Generator");
        utils::writeReg(ttcStartReg, 0x1);

        // Sleep for waitTime of milliseconds
        std::this_thread::sleep_for(std::chrono::milliseconds(waitTime));

        // Read All Trigger Registers
        LOG4CPLUS_INFO(logger, "Reading trigger counters");
        outData["CTP7"].push_back(utils::readReg(ohTrgRateReg[oh::VFATS_PER_OH + 1]));
        outData["FPGA"].push_back(utils::readReg(ohTrgRateReg[oh::VFATS_PER_OH])); // *waitTime/1000.);
        outData["VFAT"].push_back(utils::readReg(ohTrgRateReg[vfatN])); // *waitTime/1000.;

        // Reset the TTC Generator
        LOG4CPLUS_INFO(logger, "Stopping TTC Generator");
        utils::writeReg(ttcResetReg, 0x1);

        // Turn off the calpulse for this channel
        LOG4CPLUS_INFO(logger, "Disabling CalPulse for channel " << chan << " on VFAT" << vfatN << " of OH" << ohN);
        if (vfatN_as_mask != vfat3::enableChannelCalPulse {}(ohN, vfatN_as_mask, chan, false)) {
            // Calibration pulse is not configured correctly
            std::stringstream errmsg;
            errmsg << "Unable to disable CalPulse for " << ohN
                   << " mask 0x" << std::hex << std::setw(8) << std::setfill('0') << vfatN_as_mask << std::dec
                   << "channel " << chan;
            throw std::runtime_error(errmsg.str());
        }

        LOG4CPLUS_INFO(logger, "Masking channel " << chan << " on VFAT" << vfatN << " of OH" << ohN);
        utils::writeReg(chRegBase + std::to_string(chan) + ".MASK", 0x1);
    }

    // Place this vfat out of run mode FIXME why??
    LOG4CPLUS_INFO(logger, "Finished looping over all channels. Taking VFAT" << vfatN << " on OH" << ohN << " out of run mode");
    utils::writeReg(regBase + ".GEB.VFAT" + std::to_string(vfatN) + ".CFG_RUN", 0x0);

    LOG4CPLUS_INFO(logger, "Disabling TTC Generator");
    ttcGenToggle {}(false);

    LOG4CPLUS_INFO(logger, "Reverting VFAT3 channel registers for OH" << ohN << " to original values");
    vfat3::setChannelRegistersVFAT3Simple {}(ohN, vfatMask, chanRegData_orig);

    // Set trigger VFAT mask for this OH back to 0
    LOG4CPLUS_INFO(logger, "Reverting BEFE.GEM.OH.OH" << ohN << ".FPGA.TRIG.CTRL.VFAT_MASK to 0x0");
    utils::writeReg(regBase + ".FPGA.TRIG.CTRL.VFAT_MASK", 0x0);

    return outData;
}

gem::hardware::calibration::dacScanResult gem::hardware::calibration::dacScan::operator()(const uint16_t ohN,
    uint32_t vfatMask,
    const std::string& dacName,
    const uint16_t dacStep,
    const bool useExtRefADC) const
{
    constexpr uint32_t nReads = 100;

    // DAC name sanity check
    if (calibration::vfat3DACSelectAndSize.count(dacName) == 0) {
        std::string errmsg = "DAC register " + dacName + " not found.";
        throw std::runtime_error(errmsg);
    }

    LOG4CPLUS_INFO(logger, "Scanning DAC " << dacName);

    // Cache the registers addresses
    utils::RegInfo adcReg[oh::VFATS_PER_OH];
    utils::RegInfo adcCacheUpdateReg[oh::VFATS_PER_OH];
    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        const std::string register_base = fmt::format(FMT_STRING("BEFE.GEM.OH.OH{:d}.GEB.VFAT{:d}."), ohN, vfatN);

        if (useExtRefADC) {
            adcReg[vfatN] = utils::getReg(register_base + "ADC1_CACHED");
            adcCacheUpdateReg[vfatN] = utils::getReg(register_base + "ADC1_UPDATE");
        } else {
            adcReg[vfatN] = utils::getReg(register_base + "ADC0_CACHED");
            adcCacheUpdateReg[vfatN] = utils::getReg(register_base + "ADC0_UPDATE");
        }
    }

    // Output container
    gem::hardware::calibration::dacScanResult outData;

    // Determine the ADC and DAC parameters
    const auto& adcSelect = calibration::vfat3DACSelectAndSize.at(dacName).adcSelect;
    const uint32_t dacMax = calibration::vfat3DACSelectAndSize.at(dacName).max;
    const uint32_t dacMin = calibration::vfat3DACSelectAndSize.at(dacName).min;

    // Configure the DAC monitoring
    vfatMask &= vfat3::broadcastWrite {}(ohN, vfatMask, "CFG_MONITOR_SELECT", adcSelect);

    for (uint32_t dacVal = dacMin; dacVal <= dacMax; dacVal += dacStep) {
        // Update the DAC value
        vfatMask &= vfat3::broadcastWrite {}(ohN, vfatMask, dacName, dacVal);

        for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
            if (!((vfatMask >> vfatN) & 0x1))
                continue;

            try {

                float adcValArray[nReads] = { 0 };
                for (size_t i = 0; i < nReads; ++i) {
                    // Trigger an ADC measurement
                    utils::readReg(adcCacheUpdateReg[vfatN]);
                    // Wait for the cache to be updated (20 us, including a 50% safety factor)
                    std::this_thread::sleep_for(std::chrono::microseconds(20));
                    adcValArray[i] = utils::readReg(adcReg[vfatN]) & 0x3ff;
                }

                // Calculate mean and std with https://stackoverflow.com/a/12405793
                float adcValMean = (std::accumulate(std::begin(adcValArray), std::end(adcValArray), 0.0)) / nReads;
                outData[vfatN][dacVal].first = adcValMean;
                float accum = 0.0;
                std::for_each(std::begin(adcValArray), std::end(adcValArray), [&](const double d) {
                    accum += (d - adcValMean) * (d - adcValMean);
                });
                float adcValArrayStd = sqrt(accum / (nReads - 1));
                outData[vfatN][dacVal].second = adcValArrayStd;
            } catch (const xhal::memhub::memory_access_error& e) {
                // Do not record the point and mask the VFAT
                vfatMask ^= (1ULL << vfatN);
            }
        }
    }

    // Clean up incomplete scans
    for (auto it = outData.cbegin(); it != outData.cend();) {
        if (it->second.size() != (dacMax - dacMin) / dacStep + 1)
            it = outData.erase(it);
        else
            ++it;
    }

    return outData;
}

void gem::hardware::calibration::scurveScan::operator()(uint32_t ohMask,
    const uint16_t dacMin,
    const uint16_t dacMax,
    const uint16_t dacStep,
    const uint32_t nEvts,
    const uint32_t l1aInterval,
    const uint8_t pulseDelay,
    const uint16_t latency,
    const uint8_t pulseStretch,
    const bool thresholdType,
    const uint8_t threshold,
    const bool trimmingSourceType,
    const int16_t trimming) const
{
    std::vector<uint32_t> vfatMask(amc::number_oh(), 0);

    // Optimization strategy
    //
    // The S-curves scan routine is mostly slowed down by the slow-control register accesses.
    // In order to improve the performances, the register access must be optimized and minimized
    // in the inner loop (first level of action). Two measures have been put in place: (1) avoid
    // needless register accesses (i.e. re-write the same value) and (2) avoid costly LMDB lookups,
    // but prefer using a register information cache.
    //
    // - Holds the most recently applied VFAT mask (avoid updating needlessly the mask register)
    std::vector<uint32_t> currentVFATMask(amc::number_oh(), 0);
    // - Holds the VFAT DAQ register properties (avoid looking up the registers in DB)
    std::vector<std::array<utils::RegInfo, oh::VFATS_PER_OH>> vfatDACReg(amc::number_oh());
    // - Hold the critical TTC
    const utils::RegInfo ttcStartReg = utils::getReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_START");
    const utils::RegInfo ttcRunningReg = utils::getReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_RUNNING");

    // Configure the calpulsing
    vfat3::calPulseConfig t_calpulse_config;
    t_calpulse_config.mode = vfat3::VFATCalibrationMode::VOLTAGE;
    // Value is set to half of the L1A interval to ensure the analogue channel recovers its baseline.
    // Note that one should let at least 1µs (40 BX) for the recovery to be complete.
    t_calpulse_config.duration = l1aInterval / 2;
    if (t_calpulse_config.duration < 40)
        throw std::out_of_range("CalPulse duration below 40 BX does not allow channel baseline recovery");

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!((ohMask >> ohN) & 0x1))
            continue;

        vfatMask[ohN] = vfat3::getVFATMask {}(ohN);

        // Configure the VFATs for S-curves
        vfatMask[ohN] &= vfat3::broadcastWrite {}(ohN, vfatMask[ohN], "CFG_LATENCY", latency);
        vfatMask[ohN] &= vfat3::broadcastWrite {}(ohN, vfatMask[ohN], "CFG_PULSE_STRETCH", pulseStretch);

        // Set threshold from calibration interface if thresholdType true (1)
        // Otherwise the ones from the configuration files are to be used
        if (thresholdType) {
            vfatMask[ohN] &= vfat3::broadcastWrite {}(ohN, vfatMask[ohN], "CFG_THR_ARM_DAC", threshold);
            vfatMask[ohN] &= vfat3::broadcastWrite {}(ohN, vfatMask[ohN], "CFG_EN_HYST", 0);
        }

        // Unmask (i.e. scan) all channels
        vfatMask[ohN] &= vfat3::enableChannel(ohN, vfatMask[ohN], vfat3::VFAT_ALL_CHANNELS, true);

        // Configure the calibration module
        vfatMask[ohN] &= vfat3::confCalPulse {}(ohN, vfatMask[ohN], t_calpulse_config);

        // Make sure calpulses are disabled for all channels
        vfatMask[ohN] &= vfat3::enableChannelCalPulse {}(ohN, vfatMask[ohN], vfat3::VFAT_ALL_CHANNELS, false);

        // Prepare optimization structures
        vfatDACReg[ohN] = vfat3::prepareBroadcast(ohN, "CFG_CAL_DAC");
        currentVFATMask[ohN] = vfat3::updateVFATMask {}(ohN, vfatMask[ohN]);

        if (trimmingSourceType)
            vfatMask[ohN] &= vfat3::setTrimmingValue(ohN, vfatMask[ohN], vfat3::VFAT_ALL_CHANNELS, trimming);

        LOG4CPLUS_INFO(logger, fmt::format(FMT_STRING("Will start S-curves with VFAT mask {:#x} for OH{:d}"), vfatMask[ohN], ohN));
    }

    // Configure the TTC generator: set the pulse delay, the number of L1As per cycle, and the L1A period
    utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_CALPULSE_TO_L1A_GAP", pulseDelay);
    utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_L1A_COUNT", nEvts);
    utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_L1A_GAP", l1aInterval);

    // Loop over the channels
    for (uint16_t chN = 0; chN < vfat3::VFAT_ALL_CHANNELS; ++chN) {
        // Enable calpulses on the channel
        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN)
            if ((ohMask >> ohN) & 0x1)
                vfatMask[ohN] &= vfat3::enableChannelCalPulse {}(ohN, vfatMask[ohN], chN, true);

        // Set the channel in the data stream
        utils::writeReg("BEFE.GEM.DAQ.CONTROL.CALIBRATION_MODE_CHAN", chN);

        // Loop over the DAC values
        for (uint32_t dacVal = dacMin; dacVal <= dacMax; dacVal += dacStep) {
            // Write the DAC value in the front-end
            for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
                if ((ohMask >> ohN) & 0x1) {
                    vfatMask[ohN] &= vfat3::doBroadcastWrite(vfatDACReg[ohN], vfatMask[ohN], dacVal);

                    // Update the firmware VFAT mask iff. it changed w.r.t. the previous point
                    if (currentVFATMask[ohN] != vfatMask[ohN]) {
                        vfatMask[ohN] = vfat3::updateVFATMask {}(ohN, vfatMask[ohN]);
                        if (vfatMask[ohN] == 0) {
                            ohMask ^= (1ULL << ohN);
                            amc::daq::setDAQLinkInputMask {}(ohMask);
                        }
                        currentVFATMask[ohN] = vfatMask[ohN];
                    }
                }
            }

            // Write the DAC value in the RunParams
            const uint32_t runParams = (((0 & 0x1) << 21) | ((0 & 0x3ff) << 11) | ((0 & 0x7) << 8) | (dacVal & 0xff));
            amc::daq::setRunParameters {}(runParams);

            // Send triggers
            utils::writeReg(ttcStartReg, 0x1);
            // Wait until the scan for the point is done
            while (utils::readReg(ttcRunningReg)) {
            }
        }

        // Disable calpulses on the channel
        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN)
            if ((ohMask >> ohN) & 0x1)
                vfatMask[ohN] &= vfat3::enableChannelCalPulse {}(ohN, vfatMask[ohN], chN, false);
    }

    // Disable the calibration modules of the VFATs
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN)
        if ((ohMask >> ohN) & 0x1)
            // Note, no need to make it robust, a re-configuration is expected after a scan
            vfat3::enableChannelCalPulse {}(ohN, vfatMask[ohN], vfat3::VFAT_ALL_CHANNELS, false);
}

gem::hardware::calibration::calPulseScanResult gem::hardware::calibration::calPulseScan::operator()(const uint32_t ohN,
    const uint32_t nEvts,
    const uint16_t calDac,
    const bool thresholdType,
    const uint8_t threshold,
    const uint32_t l1aInterval,
    const uint8_t pulseDelay,
    const uint16_t latency,
    const uint8_t pulseStretch) const
{
    calibration::calPulseScanResult outData;

    uint32_t vfatMask = vfat3::getVFATMask {}(ohN);

    // Configure DAQ parameters
    vfatMask &= vfat3::broadcastWrite {}(ohN, vfatMask, "CFG_LATENCY", latency);
    vfatMask &= vfat3::broadcastWrite {}(ohN, vfatMask, "CFG_PULSE_STRETCH", pulseStretch);

    // Unmask (i.e. scan) all channels
    vfatMask &= vfat3::enableChannel(ohN, vfatMask, vfat3::VFAT_ALL_CHANNELS, true);

    // Configure the calibration module
    vfat3::calPulseConfig t_calpulse_config;
    {
        t_calpulse_config.mode = vfat3::VFATCalibrationMode::VOLTAGE;
        t_calpulse_config.cal_dac = calDac;
        // Value is set to half of the L1A interval to ensure the analogue channel recovers its baseline.
        // Note that one should let at least 1µs (40 BX) for the recovery to be complete.
        t_calpulse_config.duration = l1aInterval / 2;
        if (t_calpulse_config.duration < 40)
            throw std::out_of_range("CalPulse duration below 40 BX does not allow channel baseline recovery");
    }
    vfatMask &= vfat3::confCalPulse {}(ohN, vfatMask, t_calpulse_config);
    vfatMask &= vfat3::enableChannelCalPulse {}(ohN, vfatMask, vfat3::VFAT_ALL_CHANNELS, false);

    // Set threshold from calibration interface if thresholdType true (1)
    // Otherwise the ones from the configuration files are to be used
    if (thresholdType) {
        vfatMask &= vfat3::broadcastWrite {}(ohN, vfatMask, "CFG_THR_ARM_DAC", threshold);
        vfatMask &= vfat3::broadcastWrite {}(ohN, vfatMask, "CFG_EN_HYST", 0);
    }

    LOG4CPLUS_INFO(logger, fmt::format(FMT_STRING("Will start calibration pulse scan routine with VFAT mask {:#x} for OH{:d}"), vfatMask, ohN));

    // Configure the DAQ monitor
    utils::writeReg("BEFE.GEM.GEM_TESTS.VFAT_DAQ_MONITOR.CTRL.OH_SELECT", ohN);

    // Configure the TTC generator: set the pulse delay, the number of L1As per cycle, and the L1A period
    ttcGenToggle {}(true);
    utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_CALPULSE_TO_L1A_GAP", pulseDelay);
    utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_L1A_COUNT", nEvts);
    utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_L1A_GAP", l1aInterval);

    // Loop over the channels
    for (uint16_t chN = 0; chN < vfat3::VFAT_ALL_CHANNELS; ++chN) {
        // Enable calpulses on the channel
        vfatMask &= vfat3::enableChannelCalPulse {}(ohN, vfatMask, chN, true);

        // Configure the DAQ monitor
        utils::writeReg("BEFE.GEM.GEM_TESTS.VFAT_DAQ_MONITOR.CTRL.VFAT_CHANNEL_SELECT", chN);
        utils::writeReg("BEFE.GEM.GEM_TESTS.VFAT_DAQ_MONITOR.CTRL.ENABLE", 1);
        utils::writeReg("BEFE.GEM.GEM_TESTS.VFAT_DAQ_MONITOR.CTRL.RESET", 1);

        // Send triggers
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_START", 0x1);
        // Wait until the scan for the point is done
        while (utils::readReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_RUNNING")) {
        }

        // Stop recording data in the DAQ monitor
        utils::writeReg("BEFE.GEM.GEM_TESTS.VFAT_DAQ_MONITOR.CTRL.ENABLE", 0);

        for (uint8_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
            if (!((vfatMask >> vfatN) & 0x1))
                continue;

            const uint32_t events = utils::readReg(fmt::format(FMT_STRING("BEFE.GEM.GEM_TESTS.VFAT_DAQ_MONITOR.VFAT{:d}.GOOD_EVENTS_COUNT"), vfatN));
            if (events != nEvts)
                continue;

            const uint32_t hits = utils::readReg(fmt::format(FMT_STRING("BEFE.GEM.GEM_TESTS.VFAT_DAQ_MONITOR.VFAT{:d}.CHANNEL_FIRE_COUNT"), vfatN));
            outData[vfatN].push_back(hits);
        }

        // Disable calpulses on the channel
        vfatMask &= vfat3::enableChannelCalPulse {}(ohN, vfatMask, chN, false);
    }

    // Clean up the unstable VFAT
    for (auto it = outData.cbegin(); it != outData.cend();) {
        if (it->second.size() != vfat3::VFAT_ALL_CHANNELS)
            it = outData.erase(it);
        else
            ++it;
    }

    return outData;
}

void gem::hardware::calibration::thresholdScan::operator()(uint32_t ohMask,
    const uint16_t dacMin,
    const uint16_t dacMax,
    const uint16_t dacStep,
    const uint32_t nEvts,
    const uint32_t l1aInterval) const
{
    std::vector<uint32_t> vfatMask(amc::number_oh(), 0);

    // Optimization strategy
    //
    // The threshold scan routine is mostly slowed down by the slow-control register accesses.
    // In order to improve the performances, the register access must be optimized and minimized
    // when reasonable. Two measures have been put in place: (1) avoid needless register accesses
    // (i.e. re-write the same value) and (2) avoid costly LMDB lookups, but prefer using a register
    // information cache.
    //
    // - Holds the most recently applied VFAT mask (avoid updating needlessly the mask register)
    std::vector<uint32_t> currentVFATMask(amc::number_oh(), 0);
    // - Hold the critical TTC
    const utils::RegInfo ttcStartReg = utils::getReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_START");
    const utils::RegInfo ttcRunningReg = utils::getReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_RUNNING");

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!((ohMask >> ohN) & 0x1))
            continue;

        vfatMask[ohN] = vfat3::getVFATMask {}(ohN);

        // Unmask (i.e. scan) all channels
        vfatMask[ohN] &= vfat3::enableChannel(ohN, vfatMask[ohN], vfat3::VFAT_ALL_CHANNELS, true);

        // Disable the hysteresis feature when playing with the threshold
        vfatMask[ohN] &= vfat3::broadcastWrite {}(ohN, vfatMask[ohN], "CFG_EN_HYST", 0);

        currentVFATMask[ohN] = vfat3::updateVFATMask {}(ohN, vfatMask[ohN]);

        LOG4CPLUS_INFO(logger, fmt::format(FMT_STRING("Will start threshold scan with VFAT mask {:#x} for OH{:d}"), vfatMask[ohN], ohN));
    }

    // Configure the TTC generator: set the number of L1As per cycle, and the L1A period
    utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_L1A_COUNT", nEvts);
    utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_L1A_GAP", l1aInterval);

    // Loop over the DAC values
    for (uint32_t dacVal = dacMin; dacVal <= dacMax; dacVal += dacStep) {
        // Write the DAC value in the front-end
        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if ((ohMask >> ohN) & 0x1) {
                vfatMask[ohN] &= vfat3::broadcastWrite {}(ohN, vfatMask[ohN], "CFG_THR_ARM_DAC", dacVal);

                // Update the firmware VFAT mask iff. it changed w.r.t. the previous point
                if (currentVFATMask[ohN] != vfatMask[ohN]) {
                    vfatMask[ohN] = vfat3::updateVFATMask {}(ohN, vfatMask[ohN]);
                    if (vfatMask[ohN] == 0) {
                        ohMask ^= (1ULL << ohN);
                        amc::daq::setDAQLinkInputMask {}(ohMask);
                    }
                    currentVFATMask[ohN] = vfatMask[ohN];
                }
            }
        }

        // Write the DAC value in the RunParams
        const uint32_t runParams = (((0 & 0x1) << 21) | ((0 & 0x3ff) << 11) | ((0 & 0x7) << 8) | (dacVal & 0xff));
        amc::daq::setRunParameters {}(runParams);

        // Send triggers
        utils::writeReg(ttcStartReg, 0x1);
        // Wait until the scan for the point is done
        while (utils::readReg(ttcRunningReg)) {
        }
    }
}

gem::hardware::calibration::eyeOpeningMonitorScanResult gem::hardware::calibration::eyeOpeningMonitorScan::operator()(const uint32_t ohN, const uint32_t samplesBX, const uint8_t eqAttnGain) const
{
#if defined(GEM_IS_ME0)
    eyeOpeningMonitorScanResult result;
    try {
        const auto lockedGBT = 0x55 /* master only */ & gbt::getLockedGBT {}(ohN, false /* instantaneous only */);
        for (size_t gbtN = 0; gbtN < gbt::GBTS_PER_OH; ++gbtN) {
            if (!((lockedGBT >> gbtN) & 0x1))
                continue;

            LOG4CPLUS_INFO(logger, fmt::format(FMT_STRING("Eye opening monitor scan running on OH{:d}-GBT{:d}"), ohN, gbtN));
            gbt::writeGBTReg(ohN, gbtN, 0x037 /* EQConfig */, eqAttnGain, 0x18 /* EQAttenuation */);
            result[gbtN] = lpgbt::eyeOpeningMonitorScan(ohN, gbtN, samplesBX);
        }
    } catch (const gbt::gbt_ic_error& e) {
        LOG4CPLUS_WARN(logger, "Eye opening monitor scan failed with " << e.what());
        // Nothing else to do, no result recorded
    }
    return result;
#else
    return {}; // no GBTx equivalent
#endif
}
