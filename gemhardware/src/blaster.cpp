/// @file

#include <gem/hardware/blaster.h>

#include "amc/trigger.h"
#include "exception.h"
#include "gbt.h"
#include "hw_constants.h"
#include "optohybrid.h"
#include "sca.h"
#include "utils.h"
#include "vfat3.h"
#include <gem/hardware/amc/ttc.h>

#include <fmt/format.h>

#include <chrono>
#include <thread>

uint32_t gem::hardware::blaster::execute::operator()(const uint32_t optohybridMask) const
{
#if !defined(GEM_IS_ME0)
    // Initialize the GBT mask according to the OptoHybrid mask
    std::vector<uint8_t> gbtMask(amc::number_oh(), gbt::FULL_GBT_MASK);

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN)
        if (!((optohybridMask >> ohN) & 0x1))
            gbtMask[ohN] = 0;

    // Check the GBT status before programming
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!gbtMask[ohN])
            continue;

        const auto locked_gbt = gbt::getLockedGBT {}(ohN, false /* instantaneous only */);
        if ((locked_gbt & gbtMask[ohN]) != gbtMask[ohN]) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Unlocked links {:#x} before programming the GBT on OH{:d}"), ~locked_gbt & gbtMask[ohN], ohN));
            gbtMask[ohN] = locked_gbt & gbtMask[ohN];
        }
    }

    // Configure the GBT
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!gbtMask[ohN])
            continue;

        const auto configured_gbt = gbt::configure {}(ohN, gbtMask[ohN]);
        if ((configured_gbt & gbtMask[ohN]) != gbtMask[ohN]) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Could not configure GBT {:#x} on OH{:d}"), ~configured_gbt & gbtMask[ohN], ohN));
            gbtMask[ohN] = configured_gbt & gbtMask[ohN];
        }
    }
#else
    // Initialize the GBT master mask according to the OptoHybrid mask
    std::vector<uint8_t> gbtMasterMask(amc::number_oh(), 0x55 & gbt::FULL_GBT_MASK);

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN)
        if (!((optohybridMask >> ohN) & 0x1))
            gbtMasterMask[ohN] = 0;

    // Reset the master GBT
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!gbtMasterMask[ohN])
            continue;

        /* blind = */ gbt::reset_gbt(ohN, gbtMasterMask[ohN]);
    }

    std::this_thread::sleep_for(std::chrono::seconds(2));

    // Reset the VTRx+ TX lasers driver
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!gbtMasterMask[ohN])
            continue;

        /* blind = */ gbt::reset_vtrx_lasers(ohN, gbtMasterMask[ohN]);
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(10));

    // Enable the VTRx+ TX lasers
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!gbtMasterMask[ohN])
            continue;

        /* blind = */ gbt::enable_vtrx_lasers(ohN, gbtMasterMask[ohN]);
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(10));

    // Check the master GBT status before programming
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!gbtMasterMask[ohN])
            continue;

        const auto locked_gbt = gbt::getLockedGBT {}(ohN, false /* instantaneous only */);
        if ((locked_gbt & gbtMasterMask[ohN]) != gbtMasterMask[ohN]) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Unlocked master links {:#x} before programming the GBT on OH{:d}"), ~locked_gbt & gbtMasterMask[ohN], ohN));
            gbtMasterMask[ohN] = locked_gbt & gbtMasterMask[ohN];
        }
    }

    // Configure the master GBT
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!gbtMasterMask[ohN])
            continue;

        const auto configured_gbt = gbt::configure {}(ohN, gbtMasterMask[ohN]);
        if ((configured_gbt & gbtMasterMask[ohN]) != gbtMasterMask[ohN]) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Could not configure master GBT {:#x} on OH{:d}"), ~configured_gbt & gbtMasterMask[ohN], ohN));
            gbtMasterMask[ohN] = configured_gbt & gbtMasterMask[ohN];
        }
    }

    // Check the master GBT status after programming
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!gbtMasterMask[ohN])
            continue;

        const auto locked_gbt = gbt::getLockedGBT {}(ohN, false /* instantaneous only */);
        if ((locked_gbt & gbtMasterMask[ohN]) != gbtMasterMask[ohN]) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Unlocked master links {:#x} after programming the GBT on OH{:d}"), ~locked_gbt & gbtMasterMask[ohN], ohN));
            gbtMasterMask[ohN] = locked_gbt & gbtMasterMask[ohN];
        }
    }

    // Initialize the GBT slave mask according to the GBT master mask
    std::vector<uint8_t> gbtSlaveMask(amc::number_oh(), 0xaa & gbt::FULL_GBT_MASK);

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN)
        gbtSlaveMask[ohN] &= (gbtMasterMask[ohN] << 1);

    // Reset the slave GBT through the master GPIO
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!gbtSlaveMask[ohN])
            continue;

        const auto resetted_gbt = gbt::reset_slave_gbt(ohN, gbtSlaveMask[ohN] >> 1) << 1; // Via the corresponding master
        if ((resetted_gbt & gbtSlaveMask[ohN]) != gbtSlaveMask[ohN]) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Could not reset slave GBT {:#x} on OH{:d}"), ~resetted_gbt & gbtSlaveMask[ohN], ohN));
            gbtSlaveMask[ohN] = resetted_gbt & gbtSlaveMask[ohN];
        }
    }

    std::this_thread::sleep_for(std::chrono::seconds(2));

    // Configure the slave GBT
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!gbtSlaveMask[ohN])
            continue;

        const auto configured_gbt = gbt::configure {}(ohN, gbtSlaveMask[ohN]);
        if ((configured_gbt & gbtSlaveMask[ohN]) != gbtSlaveMask[ohN]) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Could not configure slave GBT {:#x} on OH{:d}"), ~configured_gbt & gbtSlaveMask[ohN], ohN));
            gbtSlaveMask[ohN] = configured_gbt & gbtSlaveMask[ohN];
        }
    }

    // Combine the master and slave masks into a global mask
    std::vector<uint8_t> gbtMask = gbtMasterMask;

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN)
        gbtMask[ohN] = gbtMasterMask[ohN] | gbtSlaveMask[ohN];

    // Manually reset all VFAT
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN)
        /* ignored = */ lpgbt::resetVFAT(ohN, oh::FULL_VFAT_MASK);
    std::this_thread::sleep_for(std::chrono::milliseconds(50)); // Ensure VFAT are out of reset
#endif

    // Issue a link reset to clear the GBT status registers
    // From this moment on, the GBT should remain locked/ready
    utils::writeReg("BEFE.GEM.GEM_SYSTEM.CTRL.LINK_RESET", 0x1);

    // Wait for potential loss of lock
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    // Check the GBT status after programming
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!gbtMask[ohN])
            continue;

        auto locked_gbt = gbt::getLockedGBT {}(ohN, true /* history matters */);
        if ((locked_gbt & gbtMask[ohN]) != gbtMask[ohN]) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Unlocked links {:#x} after programming the GBT on OH{:d}"), ~locked_gbt & gbtMask[ohN], ohN));
            gbtMask[ohN] = locked_gbt & gbtMask[ohN];
        }
    }

    // Initialize the SCA mask according to the GBT mask
    uint32_t scaMask = 0;
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN)
        if (std::get<0>(gbt::getReachableFrontend(gbtMask[ohN])))
            scaMask |= (1ULL << ohN);

    // Initialize the FPGA mask according to the SCA mask
    uint32_t fpgaMask = scaMask;

#if !defined(GEM_IS_ME0)
    // Reset the SCA
    {
        const uint32_t sca_status = sca::scaModuleReset(scaMask);
        if ((sca_status & scaMask) != scaMask) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("SCA reset failed on : {:#x}"), ~sca_status & scaMask));
            // Update both the SCA and FPGA masks
            scaMask = sca_status & scaMask;
            fpgaMask = sca_status & fpgaMask;
        }
    }

    // Reset the FPGA (and VFAT)
    {
        const uint32_t sca_status = sca::scaOHExternalReset(scaMask);
        if ((sca_status & scaMask) != scaMask) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Failed to reset OptoHybrid FPGA and VFAT : {:#x}"), ~sca_status & scaMask));
            // Update both the SCA and FPGA masks
            scaMask = sca_status & scaMask;
            fpgaMask = sca_status & fpgaMask;
        }
    }

    // FPGA should be unprogrammed after an external reset
    {
        const uint32_t programmed_fpga = oh::getProgrammedFPGA {}(fpgaMask).first;
        if ((~programmed_fpga & fpgaMask) != fpgaMask) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("OptoHybrid FPGA not unprogrammed after external reset : {:#x}"), programmed_fpga & fpgaMask));
            fpgaMask = ~programmed_fpga & fpgaMask;
        }
    }

    // Get the PROM-less counters
    const uint16_t promless_request_counter = utils::readReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.LOAD_REQUEST_CNT");
    const uint16_t promless_success_counter = utils::readReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.LOAD_SUCCESS_CNT");
    const uint16_t promless_failure_counter = utils::readReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.LOAD_FAIL_CNT");

    // Program the OH FPGA via PROM-less
    std::this_thread::sleep_for(std::chrono::milliseconds(5)); // Wait until INIT_B goes up
    utils::writeReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.GO", 1);
    std::this_thread::sleep_for(std::chrono::milliseconds(350)); // Wait for programming completion

    // Did we loose any GBT during the FPGA programming?
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!gbtMask[ohN])
            continue;

        auto locked_gbt = gbt::getLockedGBT {}(ohN, true /* history matters */);
        if ((locked_gbt & gbtMask[ohN]) != gbtMask[ohN]) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Unlocked links {:#x} after programming the OptoHybrid FPGA on OH {:d}"), ~locked_gbt & gbtMask[ohN], ohN));
            gbtMask[ohN] = locked_gbt & gbtMask[ohN];
        }
    }

    // Did we loose any SCA during the FPGA programming?
    {
        const uint32_t sca_status = sca::scaReady();
        if ((sca_status & scaMask) != scaMask) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("SCA not ready after programming the OptoHybrid FPGA : {:#x}"), ~sca_status & scaMask));
            scaMask = sca_status & scaMask;
        }
    }

    // Did the PROM-less fail?
    {
        bool promless_failed = false;
        promless_failed |= (promless_request_counter + 1u != utils::readReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.LOAD_REQUEST_CNT"));
        promless_failed |= (promless_success_counter + 1u != utils::readReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.LOAD_SUCCESS_CNT"));
        promless_failed |= (promless_failure_counter != utils::readReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.LOAD_FAIL_CNT"));

        if (promless_failed) {
            LOG4CPLUS_ERROR(logger, "PROM-less failed while trying to configure the OptoHybrid FPGA");
            throw std::runtime_error("PROM-less failed while trying to configure the OptoHybrid FPGA");
        }
    }

    // FPGA should be programmed after the PROM-less completion
    {
        const uint32_t programmed_fpga = oh::getProgrammedFPGA {}(fpgaMask).first;
        if ((programmed_fpga & fpgaMask) != fpgaMask) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("OptoHybrid FPGA not programmed after HardReset : {:#x}"), ~programmed_fpga & fpgaMask));
            fpgaMask = programmed_fpga & fpgaMask;
        }
    }

    // Direct OH FPGA communication check
    {
        uint32_t unresponsive_fpga = 0;

        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!((fpgaMask >> ohN) & 0x1))
                continue;

            try {
                utils::readReg("BEFE.GEM.OH.OH" + std::to_string(ohN) + ".FPGA.CONTROL.LOOPBACK.DATA");
            } catch (const xhal::memhub::memory_access_error&) {
                // Communication issue
                unresponsive_fpga |= (1 << ohN);
            }
        }

        if ((~unresponsive_fpga & fpgaMask) != fpgaMask) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Cannot communicate with OptoHybrid FPGA : {:#x}"), unresponsive_fpga & fpgaMask));
            fpgaMask = ~unresponsive_fpga & fpgaMask;
        }
    }

    // Final GBT status check
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!gbtMask[ohN])
            continue;

        auto locked_gbt = gbt::getLockedGBT {}(ohN, true /* history matters */);
        if ((locked_gbt & gbtMask[ohN]) != gbtMask[ohN]) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Unlocked links {:#x} establishing the front-end communication on OH {:d}"), ~locked_gbt & gbtMask[ohN], ohN));
            gbtMask[ohN] = locked_gbt & gbtMask[ohN];
        }
    }

    // Resynchronize the front-end after the Hard Reset
    utils::writeReg("BEFE.GEM.GEM_SYSTEM.CTRL.LINK_RESET", 0x1);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    // Resync' the OptoHybrid SCA and FPGA masks
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN)
        if (!std::get<0>(gbt::getReachableFrontend(gbtMask[ohN])))
            scaMask &= ~(1ULL << ohN);
    fpgaMask &= scaMask;

    // Configure the OH FPGA
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!((fpgaMask >> ohN) & 0x1))
            continue;

        if (!oh::initialize_trigger_links(ohN))
            fpgaMask &= ~(1ULL << ohN);

        if (!oh::configure_fpga {}(ohN))
            fpgaMask &= ~(1ULL << ohN);
    }
#endif

#if defined(GEM_IS_ME0)
    // When using the automatic eLink RX phase alignment feature of the lpGBT,
    // a double link reset is required. The first one aligns the lpGBT receiver
    // while the second one aligns the backend receivers.
    utils::writeReg("BEFE.GEM.GEM_SYSTEM.CTRL.LINK_RESET", 0x1);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
#endif

    // Configure the VFATs
    uint32_t daqMask = scaMask;

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!((daqMask >> ohN) & 0x1))
            continue;

        // Use the reachable VFAT as the starting point
        const uint32_t gbt_vfat_mask = std::get<1>(gbt::getReachableFrontend(gbtMask[ohN]));
        vfat3::setVFATMask {}(ohN, gbt_vfat_mask);

        // Attempt to configure all the sync'ed VFAT
        const uint32_t initial_vfat_mask = vfat3::getSyncedVFAT {}(ohN)&gbt_vfat_mask;
        const uint32_t final_vfat_mask = vfat3::configure {}(ohN, initial_vfat_mask, true /* run mode */);

        // Only enable the configured VFAT
        vfat3::setVFATMask {}(ohN, final_vfat_mask);

        if (!final_vfat_mask) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("No VFAT configured on OH {:d}"), ohN));
            daqMask &= ~(1ULL << ohN);
        }
    }

    // Resynchronize the front-end after configuration
    // For example, re-aligns the trigger links BC0 marker
    utils::writeReg("BEFE.GEM.GEM_SYSTEM.CTRL.LINK_RESET", 0x1);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    // Report as configured the OptoHybrid with both the DAQ and trigger paths configured
    return daqMask & fpgaMask;
}

uint32_t gem::hardware::blaster::configureOptoHybridFPGA::operator()(uint32_t optohybridMask) const
{
    // FIXME Avoid code duplication with the "execute" method. Due to the
    //     interleaved operations, the full sequences would need to be revised.

    using amc::trigger::enableLinksToEMTF, amc::trigger::getEnabledLinksToEMTF, amc::trigger::getValidLinks;

    // Mask the corresponding links to the EMTF to avoid sending garbage
    enableLinksToEMTF {}(getEnabledLinksToEMTF {}() & ~optohybridMask);

    // Reset the OH FPGA via the SCA
    {
        const uint32_t reset_fpga = oh::resetFPGA {}(optohybridMask);
        if ((reset_fpga & optohybridMask) != optohybridMask) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Failed to reset OptoHybrid FPGA on : {:#x}"), ~reset_fpga & optohybridMask));
            optohybridMask = reset_fpga & optohybridMask;
        }
    }

    // FPGA should be unprogrammed after an external reset
    {
        const uint32_t programmed_fpga = oh::getProgrammedFPGA {}(optohybridMask).first;
        if ((~programmed_fpga & optohybridMask) != optohybridMask) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("OptoHybrid FPGA not unprogrammed after external reset : {:#x}"), programmed_fpga & optohybridMask));
            optohybridMask = ~programmed_fpga & optohybridMask;
        }
    }

    // Get the PROM-less counters
    const uint16_t promless_request_counter = utils::readReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.LOAD_REQUEST_CNT");
    const uint16_t promless_success_counter = utils::readReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.LOAD_SUCCESS_CNT");
    const uint16_t promless_failure_counter = utils::readReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.LOAD_FAIL_CNT");

    // Program the OH FPGA via PROM-less
    std::this_thread::sleep_for(std::chrono::milliseconds(5)); // Wait until INIT_B goes up
    utils::writeReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.GO", 1);
    std::this_thread::sleep_for(std::chrono::milliseconds(350)); // Wait for programming completion

    // Did the PROM-less fail?
    {
        bool promless_failed = false;
        promless_failed |= (promless_request_counter + 1u != utils::readReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.LOAD_REQUEST_CNT"));
        promless_failed |= (promless_success_counter + 1u != utils::readReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.LOAD_SUCCESS_CNT"));
        promless_failed |= (promless_failure_counter != utils::readReg("BEFE.GEM.GEM_SYSTEM.PROMLESS.LOAD_FAIL_CNT"));

        if (promless_failed) {
            LOG4CPLUS_ERROR(logger, "PROM-less failed while trying to configure the OptoHybrid FPGA");
            // No need to go further, it failed for all OH. Note how the EMTF links are *not* unmasked.
            return 0;
        }
    }

    // FPGA should be programmed after the PROM-less completion
    {
        const uint32_t programmed_fpga = oh::getProgrammedFPGA {}(optohybridMask).first;
        if ((programmed_fpga & optohybridMask) != optohybridMask) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("OptoHybrid FPGA not programmed after HardReset : {:#x}"), ~programmed_fpga & optohybridMask));
            optohybridMask = programmed_fpga & optohybridMask;
        }
    }

    // Direct OH FPGA communication check
    {
        uint32_t unresponsive_fpga = 0;

        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if (!((optohybridMask >> ohN) & 0x1))
                continue;

            try {
                utils::readReg("BEFE.GEM.OH.OH" + std::to_string(ohN) + ".FPGA.CONTROL.LOOPBACK.DATA");
            } catch (const xhal::memhub::memory_access_error&) {
                // Communication issue
                unresponsive_fpga |= (1 << ohN);
            }
        }

        if ((~unresponsive_fpga & optohybridMask) != optohybridMask) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Cannot communicate with OptoHybrid FPGA : {:#x}"), unresponsive_fpga & optohybridMask));
            optohybridMask = ~unresponsive_fpga & optohybridMask;
        }
    }

    // Configure the OH FPGA
    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        if (!((optohybridMask >> ohN) & 0x1))
            continue;

        if (!oh::initialize_trigger_links(ohN))
            optohybridMask &= ~(1ULL << ohN);

        if (!oh::configure_fpga {}(ohN))
            optohybridMask &= ~(1ULL << ohN);
    }

    // Unmask links to the EMTF and restore trigger flow if all OK
    enableLinksToEMTF {}(getEnabledLinksToEMTF {}() | getValidLinks {}(optohybridMask, true /* reset links if needed */));

    return optohybridMask;
}
