/// @file

#ifndef SRC_GEM_HARDWARE_EXCEPTION_H
#define SRC_GEM_HARDWARE_EXCEPTION_H

#include <xhal/memhub/memhub.h>

#include <exception>
#include <string>

namespace gem {
namespace hardware {

    /// @brief Exception representing an error accessing a register
    class register_acces_error : public xhal::memhub::memory_access_error {
    public:
        /// @brief Constructor
        ///
        /// @param @c register_name the name of the register
        /// @param @c memory_error low-level information about the transaction failure
        explicit register_acces_error(const std::string& register_name, const memory_access_error& memory_error);

        /// @brief Returns the register name for which the transaction failed
        const char* register_name() const noexcept { return m_register_name.data(); }

    protected:
        std::string m_register_name = ""; ///< Name of the register for which the transaction failed
    };

} // namespace gem::hardware
} // namespace gem

#endif // SRC_GEM_HARDWARE_EXCEPTION_H
