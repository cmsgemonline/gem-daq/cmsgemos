/// @file

#ifndef SRC_GEM_HARDWARE_SCA_H
#define SRC_GEM_HARDWARE_SCA_H

#include "sca_enums.h"

#include <optional>
#include <stdint.h>
#include <vector>

namespace gem {
namespace hardware {
    namespace sca {

        /// @brief Prepare data for use with the SCA communication interfaces
        ///
        /// @details SCA TX/RX data is transmitted using the HDLC protocol, which is 16-bit length, and sent LSB to MSB.
        ///          In the HDLC packet, it is sent/received as [<16:31><0:15>].
        ///          The 0xBEFE firmware stores it as [<7:0><15:8><23:16><31:24>]
        ///
        /// @param @c data is the data to be converted to the appropriate ordering
        uint32_t formatSCAData(uint32_t data);

        /// @brief Returns the SCA for which communication is possible
        uint32_t scaReady();

        /// @brief Reset the SCA module
        ///
        /// @param @c optohybridMask bit list of the @c OptoHybrids to reset
        ///
        /// @return The OptoHybrid mask for which the reset was successful
        uint32_t scaModuleReset(uint32_t optohybridMask);

        /// @brief Trigger an external reset of the OptoHybrid FPGA & VFAT
        ///
        /// @param @c optohybridMask bit list of the @c OptoHybrids for which to trigger an external reset
        ///
        /// @return The OptoHybrid mask for which the reset was successful
        uint32_t scaOHExternalReset(uint32_t optohybridMask);

        /// @brief Execute a command using the SCA interface
        ///
        /// Generic function to drive commands to all SCA modules
        ///
        /// @param @c optohybridMask bit list of @c OptoHybrids to send the commands to
        /// @param @c ch channel to communicate with
        /// @param @c cmd command to send
        /// @param @c len length of the data to send, available 1,2,4
        /// @param @c data to send to the command
        ///
        /// @returns The SCA for which the command was successful
        uint32_t sendSCACommand(uint32_t optohybridMask, SCAChannel ch, uint8_t cmd, uint8_t len, uint32_t data);

        /// @brief Execute a command using the SCA interface, and read the reply from the SCA
        ///
        /// @param @c optohybridMask bit list of @c OptoHybrids to send the commands to
        /// @param @c ch channel to communicate with
        /// @param @c cmd command to send
        /// @param @c len length of the data to send, available 1,2,4
        /// @param @c data to send to the command
        std::vector<std::optional<uint32_t>> sendSCACommandWithReply(uint32_t optohybridMask, SCAChannel ch, uint8_t cmd, uint8_t len, uint32_t data);

        /// @brief Execute a command using the SCA CTRL interface
        ///
        /// The CTRL module contains:
        /// * Generic SCA R/W control registers
        /// * Chip ID (on channel 0x14, ADC)
        /// * SEU counter and reset (on channel 0x13, JTAG)
        ///
        /// @param @c optohybridMask bit list of @c OptoHybrids to send the commands to
        /// @param @c cmd which command to send, @ref sca_enums.h
        /// @param @c data to send to the CTRL command, default is 0x0
        std::vector<std::optional<uint32_t>> scaCTRLCommand(uint32_t optohybridMask, CTRLCommand cmd, uint32_t data = 0x0);

        /// @brief Execute a command using the SCA GPIO interface
        ///
        /// @param @c optohybridMask bit list of @c OptoHybrids to send the commands to
        /// @param @c ch GPIO channel to communicate with
        /// @param @c cmd which command to send, @ref sca_enums.h
        /// @param @c len length of the data to send, available 1,2,4
        /// @param @c data to send to the GPIO command
        std::vector<std::optional<uint32_t>> scaGPIOCommand(uint32_t optohybridMask, GPIOCommand cmd, uint8_t len, uint32_t data);

        /// @brief Read raw values from the chosen ADC input channel
        ///
        /// @details The current source is enabled automatically following @c gem::hardware::sca::ADC_CURRENT_MASK
        ///
        /// @param @c optohybridMask bit list of @c OptoHybrids to send the commands to
        /// @param @c ch ADC channel to communicate with
        std::vector<std::optional<uint32_t>> readSCAADC(uint32_t optohybridMask, sca::ADCChannel ch);

        /// @brief Read the Chip ID from the SCA ASIC
        ///
        /// @param @c optohybridMask bit list of @c OptoHybrids to send the commands to
        std::vector<std::optional<uint32_t>> readSCAChipID(uint32_t optohybridMask);

        /// @brief Read the SEU counter from the SCA ASIC
        ///
        /// @param @c optohybridMask bit list of @c OptoHybrids to send the commands to
        /// @param @c reset true to reset the counter before reading
        std::vector<std::optional<uint32_t>> readSCASEUCounter(uint32_t optohybridMask, bool reset = false);

        /// @brief Reset the SCA SEU counter
        ///
        /// @param @c optohybridMask bit list of @c OptoHybrids to send the reset command to
        void resetSCASEUCounter(uint32_t optohybridMask);

    } // namespace gem::hardware::sca
} // namespace gem::hardware
} // namespace gem

#endif // SRC_GEM_HARDWARE_SCA_H
