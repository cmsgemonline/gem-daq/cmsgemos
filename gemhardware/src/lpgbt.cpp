/// @file
/// @brief Contains lpGBT-specific features
///
/// The full documentation of the ASIC can be found at
/// https://lpgbt.web.cern.ch/lpgbt/

#include "lpgbt.h"

#include "gbt.h"
#include "hw_constants.h"
#include "utils.h"

#include <fmt/format.h>

#include <chrono>
#include <cmath>
#include <stdexcept>
#include <thread>

namespace gem::hardware::lpgbt {

std::optional<uint32_t> readFuseBank(uint32_t ohN, uint8_t gbtN, uint16_t address)
{
    if (address % 4)
        throw std::invalid_argument(fmt::format(FMT_STRING("Incorrect e-fuse bank address (address = {:#02x})"), address));

    gbt::writeGBTReg(ohN, gbtN, 0x11e /* FUSEBlowAddH */, (address >> 8) & 0xff);
    gbt::writeGBTReg(ohN, gbtN, 0x11f /* FUSEBlowAddL */, address & 0xff);
    gbt::writeGBTReg(ohN, gbtN, 0x119 /* FUSEControl */, 0x2);

    size_t timeout = 10;
    while (!gbt::readGBTReg(ohN, gbtN, 0x1b1 /* FUSEStatus */, 0x4)) {
        if (timeout-- == 0) {
            LOG4CPLUS_WARN(logger, "Reading e-fuse bank timed out");
            gbt::writeGBTReg(ohN, gbtN, 0x119 /* FUSEControl */, 0);
            return {};
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    uint32_t outval = 0;
    outval |= gbt::readGBTReg(ohN, gbtN, 0x1b2);
    outval |= gbt::readGBTReg(ohN, gbtN, 0x1b3) << 8;
    outval |= gbt::readGBTReg(ohN, gbtN, 0x1b4) << 16;
    outval |= gbt::readGBTReg(ohN, gbtN, 0x1b5) << 24;

    gbt::writeGBTReg(ohN, gbtN, 0x119 /* FUSEControl */, 0);

    return outval;
}

void enableADC(uint32_t ohN, uint8_t gbtN)
{
    // Enable ADC
    gbt::writeGBTReg(ohN, gbtN, 0x123 /* ADCConfig */, 0x4);

    // Enable internal voltage reference
    gbt::writeGBTReg(ohN, gbtN, 0x01c /* VREFCNTR */, 0x1, 0x80);

    // Enable ADC current source (DAC)
    gbt::writeGBTReg(ohN, gbtN, 0x06a /* DACConfigH */, 0x1, 0x40);

    // Reset internal temperature sensor and enable internal voltage dividers
    gbt::writeGBTReg(ohN, gbtN, 0x122 /* ADCMon */, 0x20);
    gbt::writeGBTReg(ohN, gbtN, 0x122 /* ADCMon */, 0x1d);

    // Wait for stabilization
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
}

std::optional<uint32_t> readADC(const uint32_t ohN, const uint8_t gbtN, const adcChannel channel, const adcGain gain, const std::optional<uint8_t> currentSource)
{
    // FIXME: Should it be atomic?

    uint8_t cfgGain = static_cast<uint8_t>(gain) & 0x3;

    // Configure and enable the current DAC if requested
    if (currentSource) {
        gbt::writeGBTReg(ohN, gbtN, 0x06d /* CURDACCHN */, 1ULL << channel.inp);
        gbt::writeGBTReg(ohN, gbtN, 0x06c /* CURDACValue */, *currentSource);
    }

    // Select channel
    gbt::writeGBTReg(ohN, gbtN, 0x121 /* ADCSelect */, ((channel.inp & 0xf) << 4) | (channel.inn & 0xf));

    // Convert!
    gbt::writeGBTReg(ohN, gbtN, 0x123 /* ADCConfig */, 0x80 | 0x4 | cfgGain);

    // Wait until done...
    size_t timeout = 10;
    while (!gbt::readGBTReg(ohN, gbtN, 0x1ca /* ADCStatusH */, 0x40)) {
        if (timeout-- == 0) {
            // Cancel the convertion
            gbt::writeGBTReg(ohN, gbtN, 0x123 /* ADCConfig */, 0x4 | cfgGain);
            return {};
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    uint16_t adcVal = gbt::readGBTReg(ohN, gbtN, 0x1cb /* ADCStatusL */);
    adcVal |= gbt::readGBTReg(ohN, gbtN, 0x1ca /* ADCStatusH */, 0x3) << 8;

    // Terminate the conversion cycle
    gbt::writeGBTReg(ohN, gbtN, 0x123 /* ADCConfig */, 0x4 | cfgGain);

    return adcVal;
}

uint32_t resetVFAT(const uint32_t ohN, const uint32_t vfatMask)
{
    uint32_t okMask { 0 }; // Set if reset applied

    for (uint8_t gbtN = 0; gbtN < gbt::GBTS_PER_OH; ++gbtN) {
        // Build the GPIO masks and associated VFAT masks based on the hardware constants
        uint8_t gpioMaskLow { 0 }, gpioMaskHigh { 0 };
        uint32_t vfatMaskLow { 0 }, vfatMaskHigh { 0 };

        for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
            if (VFAT_TO_RESET_GPIO.at(vfatN).first != gbtN)
                continue;
            if (!((vfatMask >> vfatN) & 0x1))
                continue;

            if (VFAT_TO_RESET_GPIO[vfatN].second > 7) {
                gpioMaskHigh |= (1 << (VFAT_TO_RESET_GPIO[vfatN].second - 8));
                vfatMaskHigh |= (1 << vfatN);
            } else {
                gpioMaskLow |= (1 << VFAT_TO_RESET_GPIO[vfatN].second);
                vfatMaskLow |= (1 << vfatN);
            }
        }

        // Apply reset (low bits)
        try {
            if (gpioMaskLow) {
                gbt::writeGBTReg(ohN, gbtN, 0x056, 0xff, gpioMaskLow); // Set GPIO high
                std::this_thread::sleep_for(std::chrono::milliseconds(50));
                gbt::writeGBTReg(ohN, gbtN, 0x056, 0x00, gpioMaskLow); // Set GPIO low
                okMask |= vfatMaskLow;
            }
        } catch (const gbt::gbt_ic_error& e) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Failed to reset VFAT {:#x} with: {:s}"), vfatMaskLow, e.what()));
        }

        // Apply reset (high bits)
        try {
            if (gpioMaskHigh) {
                gbt::writeGBTReg(ohN, gbtN, 0x055, 0xff, gpioMaskHigh); // Set GPIO high
                std::this_thread::sleep_for(std::chrono::milliseconds(50));
                gbt::writeGBTReg(ohN, gbtN, 0x055, 0x00, gpioMaskHigh); // Set GPIO low
                okMask |= vfatMaskHigh;
            }
        } catch (const gbt::gbt_ic_error& e) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Failed to reset VFAT {:#x} with: {:s}"), vfatMaskHigh, e.what()));
        }
    }

    return okMask;
}

std::array<std::array<std::pair<uint64_t, uint64_t>, 31>, 64> eyeOpeningMonitorScan(const uint32_t ohN, const uint8_t gbtN, const uint32_t samplesBX)
{
    std::array<std::array<std::pair<uint64_t, uint64_t>, 31>, 64> eye;

    // Enable EOM circuitry
    gbt::writeGBTReg(ohN, gbtN, 0x124 /* EOMConfigH */, 0x1 /* EOMEnable */);

    for (uint8_t y_axis = 0; y_axis < 31; y_axis++) {
        // Comparison voltage in steps of [-VDDRX/2:VDDRX/30:VDDRX/2] V
        // Note: value 32 is invalid
        gbt::writeGBTReg(ohN, gbtN, 0x126 /* EOMvofSel */, y_axis & 0x1f);

        for (uint8_t x_axis = 0; x_axis < 64; x_axis++) {
            // Sampling phase in steps of [0:1/(fvco*64):63/(fvco*64)] second
            gbt::writeGBTReg(ohN, gbtN, 0x125 /* EOMConfigL */, x_axis & 0x3f);

            std::this_thread::sleep_for(std::chrono::milliseconds(5));

            // The EOMCounterValue counter may overflow for samplesBX above
            // 2048 (EOMendOfCountSel above 10 in the LpGBT documentation). If
            // the user requests gating for more BX, run by chunks of maximum
            // 2048 BX. Note that due to power-of-two rounding, the number of BX
            // can be slightly higher than requested.
            for (int64_t samples = samplesBX; samples > 0; samples -= 2048) {
                uint8_t log2Samples = 0;
                if (samples >= 2048)
                    log2Samples = 10;
                else
                    log2Samples = std::ceil(std::log2(samples)) - 1;

                const uint8_t config = log2Samples << 4 /* EOMendOfCountSel */ | 0x1 /* EOMEnable */;

                // Start the measurement
                gbt::writeGBTReg(ohN, gbtN, 0x124 /* EOMConfigH */, config | 0x2);

                // wait until measurement is finished
                uint8_t status;
                do {
                    status = gbt::readGBTReg(ohN, gbtN, 0x1cc /* EOMStatus */);
                } while ((status & 0x2) && !(status & 0x1));

                // Read and store measurement result
                const uint16_t counterValueMsb = gbt::readGBTReg(ohN, gbtN, 0x1cd /* EOMCounterValueH */);
                const uint16_t counterValueLsb = gbt::readGBTReg(ohN, gbtN, 0x1ce /* EOMCounterValueL */);
                eye[x_axis][y_axis].first += (counterValueMsb << 8) | counterValueLsb;

                const uint16_t counter40MMsb = gbt::readGBTReg(ohN, gbtN, 0x1cf /* EOMCounter40MH */);
                const uint16_t counter40MLsb = gbt::readGBTReg(ohN, gbtN, 0x1d0 /* EOMCounter40ML */);
                eye[x_axis][y_axis].second += (counter40MMsb << 8) | counter40MLsb;

                // Stop the measurement
                gbt::writeGBTReg(ohN, gbtN, 0x124 /* EOMConfigH */, 0x1 /* EOMEnable */);
            }
        }
    }

    // Disable EOM circuitry
    gbt::writeGBTReg(ohN, gbtN, 0x124 /* EOMConfigH */, 0x0);

    return eye;
}

} // namespace gem::hardware::lpgbt
