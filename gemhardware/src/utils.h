/// @file

#ifndef SRC_GEM_HARDWARE_UTILS_H
#define SRC_GEM_HARDWARE_UTILS_H

#include <gem/hardware/utils.h>

#include "XHALXMLParser.h"
#include <xhal/memhub/memhub.h>

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <functional>
#include <istream>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace gem {
namespace hardware {

    extern log4cplus::Logger logger;

    extern std::unique_ptr<xhal::memhub::base_memhub> memhub;

    namespace utils {

        constexpr uint32_t LMDB_SIZE = 1UL * 1024UL * 1024UL * 50UL; ///< Maximum size of the LMDB object, currently 50 MiB

        /// @brief Contains information stored in the address table for a given register node
        struct RegInfo {
            uint32_t address; ///< Register address
            uint32_t mask; ///< Register mask
            uint32_t size; ///< Register size (in 32-bit words)
            uint8_t permissions; ///< Register permissions: r, w, rw
            uint8_t mode; ///< Register mode: single, block

            static constexpr uint8_t is_writable = 0x1;
            static constexpr uint8_t is_readable = 0x2;

            static constexpr uint8_t is_single = 0x1;
            static constexpr uint8_t is_block = 0x2;
        };

        /// @brief Tokenize a string based on a delimieter
        ///
        /// @param @c s The @c std::string object to tokenize
        /// @param @c delim @c char value to use as tokenizer
        /// @tparam @c Out @c Out value to use as tokenizer
        template <typename Out>
        void split(const std::string& s, char delim, Out result)
        {
            std::stringstream ss;
            ss.str(s);
            std::string item;
            while (std::getline(ss, item, delim)) {
                *(result++) = item;
            }
        }

        /// @brief Tokenize a string based on a delimieter
        ///
        /// @param @c s The @c std::string object to tokenize
        /// @param @c delim @c char value to use as tokenizer
        std::vector<std::string> split(const std::string& s, char delim);

        /// @brief Safe and efficient rotate function
        ///
        /// Rotates @c x right by @c n bits.
        ///
        /// This function follows the implementation proposed by John Regehr.
        /// See https://blog.regehr.org/archives/1063 for more details.
        /// details.
        inline uint32_t rotr32(const uint32_t x, const uint32_t n)
        {
            assert(n < 32);
            return (x >> n) | (x << (-n & 31));
        }

        /// @brief Bitwise 5-way majority voter
        template <typename T>
        inline T majority5(const T a, const T b, const T c, const T d, const T e)
        {
            return (a & b & c) | (a & b & d) | (a & b & e) | (a & c & d) | (a & c & e) | (a & d & e) | (b & c & d) | (b & c & e) | (b & d & e) | (c & d & e);
        }

        /// @brief This function initializes the @c log4cplus logging system
        ///
        /// MUST be called before using the @c gem::hardware library
        ///
        /// @param @c configuration must contain the @c log4cplus configuration
        void init_logging(std::istream& configuration);

        /// @brief Converts a libc error number into a describing string
        std::string get_errno_string(int errnum);

        /// @brief This function executes the system command passed as parameter
        ///
        /// @param @c command The system command to be executed
        /// @throw @c std::runtime_error if the command fails
        void execute_command(const std::string& command);

        /// @brief Returns the directory containing the backend firmware artifacts
        ///
        /// The path is selected in the following order:
        /// 1. Environment variable GEM_BACKEND_FIRMWARE
        /// 2. <configuration-dir>/backend-firmware
        std::string get_backend_firmware_dir();

        /// @brief Returns the directory containing the optohybrid firmware artifacts
        ///
        /// The path is selected in the following order:
        /// 1. Environment variable GEM_OPTOHYBRID_FIRMWARE
        /// 2. <configuration-dir>/backend-firmware
        std::string get_optohybrid_firmware_dir();

        /// @brief Updates the LMDB address table
        void update_address_table();

        /// @brief Loads a key-value configuration file from the configuration path
        ///
        /// The configuration files are stored under <configuration-dir> and contains key-value pairs.
        /// Empty lines are skipped; comment character is @c #.
        ///
        /// @param \c filename The configuration filename path, under the configuration path
        /// @returns An \c std::map containing the key-value pairs
        std::map<std::string, uint32_t> read_configuration_file(const std::string& filename);

        /// @brief Parse a CTP7 clock configuration file
        ///
        /// The clock configuration is taken from the first non-empty and non-commented line (comment character @c #) and has the following syntax:
        ///
        /// <synthA-config> <synthB-config> <back0> <back1> <front1> <front0>
        ///
        /// The values for synth{A,B}-config are an 320_160, 128_192, 250, or an absolute or relative path to a synthesizer configuration file
        /// The values for {front,back}{A,B} are the synthetizer outputs: A0, A1, B0, B1
        ///
        /// @returns A string containing the arguments that can be used for @c clockinit
        std::string get_clock_configuration();

        /// @brief return 1 if the given bit in word is 1 else 0
        ///
        /// @param @c word: an unsigned int of 32 bit
        /// @param @c bit: integer that specifies a particular bit position
        uint32_t bitCheck(uint32_t word, int bit);

        /// @brief returns the number of nonzero bits in an integer
        ///
        /// @param @c value integer to check the number of nonzero bits
        uint32_t getNumNonzeroBits(uint32_t value);

        /// @brief returns the number of nonzero bits in an integer
        ///
        /// @param @c value integer to check the number of nonzero bits
        uint32_t getNumNonzeroBits(uint32_t value);

        /// @brief Open the LMDB database
        ///
        /// This function MUST be called before any register access
        void init_lmdb();

        /// @brief Connect to the memory service
        ///
        /// This function MUST be called before any register access
        void init_memhub();

        /// @brief Tries @c trials times to check that the @c condition is satisfied
        ///
        /// @param @c trials Number of attempts to check the condition
        /// @param @c condition Condition to be satisfied (@c true means success)
        /// @param @c message Message used as warning in the log and exception message
        ///
        /// @throws @c std::runtime_error with the @message if the condition is not met after @trials attempts
        void assert_n(const size_t trials, std::function<bool()> condition, const std::string& message);

        /// @brief Returns whether the register exists or not
        ///
        /// @param @c regName Register name
        ///
        /// @returns @c true if the register exists, @c false otherwise
        bool regExists(const std::string& regName);

        /// @brief Returns the properties of the register associated to its name
        ///
        /// @throw @c std::runtime_error if the LMDB database is not opened or the register does not exist.
        ///
        /// @param @c regName Register name
        ///
        /// @returns The @c RegInfo structure containing the register properties.
        RegInfo getReg(const std::string& regName);

        /// @brief Finds all registers containing all @c parts, in order
        ///
        /// @param @parts to be looked for, in order, in the register names
        ///
        /// @return The list of register names
        std::vector<std::string> find_registers(const std::vector<std::string>& parts);

        /// @brief Writes a value to a raw register address
        ///
        /// @param @c address Address of the register
        /// @param @c value Value to write to the register
        /// @param @c mask Mask of the register
        void writeRawAddress(uint32_t address, uint32_t value, uint32_t mask = 0xffffffff);

        /// @brief Reads a value from raw register address
        ///
        /// @param @c address Address of the register
        /// @param @c mask Mask of the register
        ///
        /// @returns Value of the register
        uint32_t readRawAddress(uint32_t address, uint32_t mask = 0xffffffff);

        /// @brief Writes a value to a register. Register mask is applied
        ///
        /// @param @c reg Register properties
        /// @param @c value Value to write
        void writeReg(const RegInfo& reg, uint32_t value);

        /// @brief Reads a value from register. Register mask is applied
        ///
        /// @param @c reg Register properties
        ///
        /// @returns @c uint32_t register value
        uint32_t readReg(const RegInfo& reg);

        /// @brief Writes a value to a register. Register mask is applied
        ///
        /// @param @c regName Register name
        /// @param @c value Value to write
        void writeReg(const std::string& regName, uint32_t value);

        /// @brief Reads a value from register. Register mask is applied
        ///
        /// @param @c regName Register name
        ///
        /// @returns @c uint32_t register value
        uint32_t readReg(const std::string& regName);

        /// @brief Reads a block of values from a contiguous address space.
        ///
        /// @param @c regName Register name of the block to be read
        /// @param @c size number of words to read (should this just come from the register properties?
        /// @param @c result Pointer to an array to hold the result
        /// @param @c offset Start reading from an offset from the base address returned by regName
        ///
        /// @returns the number of uint32_t words in the result (or better to return a std::vector?
        uint32_t readBlock(const std::string& regName, uint32_t* result, const uint32_t& size, const uint32_t& offset = 0);

        /// @brief Writes a block of values to a contiguous address space.
        ///
        /// @detail Block writes are allowed on 'single' registers, provided:
        ///         * The size of the data to be written is 1
        ///         * The register does not have a mask
        ///         Block writes are allowed on 'block' and 'fifo/incremental/port' type addresses provided:
        ///         * The size does not overrun the block as defined in the address table
        ///         * Including cases where an offset is provided, baseaddr+offset+size > blocksize
        ///
        /// @param @c regName Register name of the block to be written
        /// @param @c values Values to write to the block
        /// @param @c offset Start writing at an offset from the base address returned by regName
        void writeBlock(const std::string& regName, const uint32_t* values, const uint32_t& size, const uint32_t& offset = 0);

    } // namespace gem::hardware::utils
} // namespace gem::hardware
} // namespace gem

#endif
