/// @file

#include <gem/hardware/board.h>

#include "board/board.h"
#include "board/mgt.h"
#include "board/promless.h"
#include "utils.h"

#include <chrono>
#include <thread>

void gem::hardware::board::configure::operator()() const
{
#if !defined(GEM_IS_ME0)
    // Load the OptoHybrid firmware into RAM
    const std::string optohybrid_fw_filename = utils::get_optohybrid_firmware_dir() + "/bitstream.bin";
    board::load_promless(optohybrid_fw_filename);
#endif

    LOG4CPLUS_INFO(logger, "Configure the back-end board registers");
    const auto data = utils::read_configuration_file("board/config.cfg");
    for (const auto& i : data)
        utils::writeReg("BEFE." + i.first, i.second);
}

void gem::hardware::board::recover::operator()() const
{
    board::cold_reset();

    // This is a bit of a strange register...
    // While the register is present in the AMC address space, the feature is
    // actually implemented in the system. Also only the CTP7 has the TTC phase
    // alignment feature implemented in order to properly sample the TTC stream
    // coming from the AMC13.
    //
    // Make sure to disable the feature here to avoid unexpeced side effects.
    // If and when needed, the board package will re-enable it.
    utils::writeReg("BEFE.GEM.TTC.CTRL.DISABLE_PHASE_ALIGNMENT", 1);
}

void gem::hardware::board::reset::operator()() const
{
    // Reset the internal clocks
    board::reset_all_mgt_plls();
    board::reset_ttc_clock();

    // Reset the MGT to their default configuration
    board::configure_all_mgts();
    board::reset_all_mgts();

    // Reset the firmware logic
#if !defined(GEM_IS_CTP7)
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    utils::writeReg("BEFE.SYSTEM.CTRL.USER_LOGIC_RESET", 1);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
#endif
}
