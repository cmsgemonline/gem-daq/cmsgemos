/// @file

#ifndef SRC_GEM_HARDWARE_LIBI2C_H
#define SRC_GEM_HARDWARE_LIBI2C_H

#include <stdint.h>
#include <string>

namespace gem {
namespace hardware {

    /// @brief Interface to perform register transactions on an I2C device
    class i2c_device {
    public:
        /// @brief Constructor
        ///
        /// @param @c controller_filename Path to the I2C controller (usually /dev/i2c-<n>)
        /// @param @c slave_address Address of the device on the I2C bus
        i2c_device(const std::string& controller_filename, uint16_t slave_address);

        /// @brief Move constructor
        i2c_device(i2c_device&& other);

        /// @brief Move assignment operator
        i2c_device& operator=(i2c_device&& other);

        /// @brief Destructor
        virtual ~i2c_device();

        /// @brief Read a single I2C register
        ///
        /// @param @c address Internal address of the register
        ///
        /// @throw @c std::runtime_error in case of transaction error
        uint8_t read(uint8_t address);

        /// @brief Writes a single I2C register
        ///
        /// @param @c address Internal address of the register
        /// @param @c value Value to write in the register
        ///
        /// @throw @c std::runtime_error in case of transaction error
        void write(uint8_t address, uint8_t value);

    protected:
        int m_controller_fd = -1; ///< I2C controller file descriptor
        uint16_t m_slave_address = -1; ///< Address of the device on the bus
    };

} // namespace gem::hardware
} // namespace gem

#endif // SRC_GEM_HARDWARE_LIBI2C_H
