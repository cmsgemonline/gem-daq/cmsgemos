/// @file

#include "exception.h"

#include <fmt/format.h>

gem::hardware::register_acces_error::register_acces_error(const std::string& register_name, const memory_access_error& memory_error)
    : memory_access_error(memory_error)
    , m_register_name(register_name)
{
    m_what = fmt::format(FMT_STRING("Cannot access {:s} [address = {:#x}, direction = {:s}, details = {:s}]"),
        m_register_name, m_address, to_string(m_direction), m_message);
}
