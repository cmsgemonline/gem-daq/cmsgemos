/// @file
/// @brief Header containing helper functions to check the hardware related constants.

#ifndef GEM_HARDWARE_HW_CONSTANTS_CHECKS_H
#define GEM_HARDWARE_HW_CONSTANTS_CHECKS_H

#include "hw_constants.h"

#include <fmt/format.h>

#include <sstream>

/// @brief This namespace holds checks for constants related to the GBT.
namespace gem {
namespace hardware {
    namespace gbt {

        /// @brief This function checks the phase parameter validity.
        ///
        /// @throws @c std::range_error if the specified phase is outside the allowed range
        ///
        /// @param @c phase Phase value to check
        inline void checkPhase(uint8_t phase)
        {
            if (phase == PHASE_INVALID)
                return; // This is a valid phase to write
            if (phase < PHASE_MIN)
                throw std::range_error(fmt::format(FMT_STRING("The phase parameter supplied ({:d}) is smaller than the minimum phase ({})."), phase, PHASE_MIN));
            if (phase > PHASE_MAX)
                throw std::range_error(fmt::format(FMT_STRING("The phase parameter supplied ({:d}) is larger than the maximum phase ({})."), phase, PHASE_MAX));
        }

    } // namespace gem::hardware::gbt
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_HW_CONSTANTS_CHECKS_H
