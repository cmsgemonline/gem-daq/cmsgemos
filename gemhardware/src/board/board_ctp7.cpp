/// @file

#include "board.h"
#include "mgt.h"
#include "promless.h"

#include "../utils.h"

#include <fmt/format.h>

#include <chrono>
#include <fstream>
#include <mutex>
#include <thread>

namespace /* anonymous */ {

constexpr uint32_t GEM_LOADER_ENABLE_ADDRESS = 0x2a000000; ///< Set bit 0 to enable the GEM loader
constexpr uint32_t GEM_LOADER_SIZE_ADDRESS = 0x2a000004; ///< Size (in bytes) of the firmware bitstream to load
constexpr uint32_t GEM_LOADER_REQUEST_ADDRESS = 0x2a000008; ///< A 0-to-1 transition triggers a GEM loader request (but does NOT load the firmware to the front-end)

} // anonymous namespace

// == board.h ==

void gem::hardware::board::cold_reset()
{
    // == Enable the optical TX lasers ==
    LOG4CPLUS_INFO(logger, "Enable the optical TX lasers");
    utils::execute_command("/bin/txpower enable");

    // == Reset the synthesizers (Si5324) ==
    LOG4CPLUS_INFO(logger, "Reset the synthesizers (Si5324)");

    const std::string ctp7_clock_config = utils::get_clock_configuration();
    utils::execute_command(std::string { "/bin/clockinit " } + ctp7_clock_config);

    // Wait for the PLL to lock
    std::this_thread::sleep_for(std::chrono::seconds(1));

    // == Load the Virtex-7 firmware ==
    LOG4CPLUS_INFO(logger, "Load the Virtex-7 firmware");
    const std::string ctp7_fw_filename = utils::get_backend_firmware_dir() + "/bitstream.bit";
    {
        // Prevent all register accesses during an FPGA reload
        std::lock_guard<decltype(*memhub)> lock(*memhub);
        utils::execute_command(std::string { "/bin/v7load " } + ctp7_fw_filename);
    }

    // Wait for FPGA to initialize
    std::this_thread::sleep_for(std::chrono::seconds(1));

    utils::assert_n(
        10,
        []() -> bool {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            return utils::readReg("BEFE.GEM.TTC.STATUS.CLK.MMCM_LOCKED") == 1;
        },
        "Imposible to lock the Virtex-7 TTC MMCM");
}

void gem::hardware::board::reset_ttc_clock()
{
    LOG4CPLUS_INFO(logger, "Align the TTC clock phase");

    const uint32_t targetPhase = 0x100; // FIXME: Take me from the configuration
    utils::writeReg("BEFE.GEM.TTC.CTRL.LOCKMON_TARGET_PHASE", targetPhase);

    // Disable the TTC commands during the alignment procedure
    utils::writeReg("BEFE.GEM.TTC.CTRL.CMD_ENABLE", 0);

    utils::writeReg("BEFE.GEM.TTC.CTRL.DISABLE_PHASE_ALIGNMENT", 0);

    utils::assert_n(
        10,
        []() -> bool {
            utils::writeReg("BEFE.GEM.TTC.CTRL.PHASE_ALIGNMENT_RESET", 1);
            // Wait for the alignent procedure to complete
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            // The procedure must complete and reach the target phase to be successful
            return ((utils::readReg("BEFE.GEM.TTC.STATUS.CLK.SYNC_DONE") == 1) && (utils::readReg("BEFE.GEM.TTC.STATUS.CLK.PHASE_LOCKED") == 1));
        },
        "Failed to align the TTC clock phase");

    utils::writeReg("BEFE.GEM.TTC.CTRL.DISABLE_PHASE_ALIGNMENT", 1);

    // TTC commands can be enabled once the phase is aligned
    utils::writeReg("BEFE.GEM.TTC.CTRL.CMD_ENABLE", 1);

    utils::writeReg("BEFE.GEM.TTC.CTRL.CNT_RESET", 1);
}

// == mgt.h ==

std::optional<gem::hardware::board::link_config_t> gem::hardware::board::get_link_config(const uint8_t link_idx)
{
    static const std::vector<link_config_t> link_config = {
        // The CTP7 firmware does not provide any description registers in its
        // registers. Hard-code the values.
        { 1, 2, false, false }, // fiber 0
        { 3, 0, false, false }, // fiber 1
        { 5, 4, false, false }, // fiber 2
        { 0, 3, false, false }, // fiber 3
        { 2, 5, false, false }, // fiber 4
        { 4, 1, false, false }, // fiber 5
        { 10, 7, false, false }, // fiber 6
        { 8, 9, false, false }, // fiber 7
        { 6, 10, false, false }, // fiber 8
        { 11, 6, false, false }, // fiber 9
        { 9, 8, false, false }, // fiber 10
        { 7, 11, false, true }, // fiber 11
        { 13, 15, false, false }, // fiber 12
        { 15, 12, false, false }, // fiber 13
        { 17, 16, false, false }, // fiber 14
        { 12, 14, false, false }, // fiber 15
        { 14, 18, false, false }, // fiber 16
        { 16, 13, false, false }, // fiber 17
        { 22, 19, false, false }, // fiber 18
        { 20, 23, false, false }, // fiber 19
        { 18, 20, false, false }, // fiber 20
        { 23, 17, false, false }, // fiber 21
        { 21, 21, false, false }, // fiber 22
        { 19, 22, false, false }, // fiber 23
        { 25, 27, false, false }, // fiber 24
        { 27, 24, false, false }, // fiber 25
        { 29, 28, false, false }, // fiber 26
        { 24, 26, false, false }, // fiber 27
        { 26, 30, false, false }, // fiber 28
        { 28, 25, false, false }, // fiber 29
        { 34, 31, false, false }, // fiber 30
        { 32, 35, false, false }, // fiber 31
        { 30, 32, false, false }, // fiber 32
        { 35, 29, false, false }, // fiber 33
        { 33, 33, false, false }, // fiber 34
        { 31, 34, false, false }, // fiber 35
        { std::nullopt, std::nullopt, false, false }, // fiber 36
        { std::nullopt, 66, false, false }, // fiber 37
        { std::nullopt, 64, false, false }, // fiber 38
        { std::nullopt, 65, false, false }, // fiber 39
        { std::nullopt, 62, false, false }, // fiber 40
        { std::nullopt, 63, false, false }, // fiber 41
        { std::nullopt, 61, false, false }, // fiber 42
        { std::nullopt, 60, false, false }, // fiber 43
        { std::nullopt, 59, false, false }, // fiber 44
        { std::nullopt, 58, false, false }, // fiber 45
        { std::nullopt, 57, false, false }, // fiber 46
        { std::nullopt, 56, false, false }, // fiber 47
        { 59, 54, false, false }, // fiber 48
        { 56, 55, false, false }, // fiber 49
        { 63, 52, false, false }, // fiber 50
        { 52, 53, false, false }, // fiber 51
        { 62, 50, false, false }, // fiber 52
        { 53, 51, false, false }, // fiber 53
        { 61, 49, false, false }, // fiber 54
        { 54, 48, false, false }, // fiber 55
        { 60, 47, false, false }, // fiber 56
        { 55, 46, false, false }, // fiber 57
        { 58, 45, false, false }, // fiber 58
        { 57, 44, false, false }, // fiber 59
        { std::nullopt, std::nullopt, false, false }, // fiber 60
        { std::nullopt, std::nullopt, false, false }, // fiber 61
        { std::nullopt, 43, false, false }, // fiber 62
        { std::nullopt, std::nullopt, false, false }, // fiber 63
        { std::nullopt, 42, false, false }, // fiber 64
        { std::nullopt, std::nullopt, false, false }, // fiber 65
        { std::nullopt, 40, false, false }, // fiber 66
        { std::nullopt, 36, false, true }, // fiber 67
        { std::nullopt, 41, false, false }, // fiber 68
        { std::nullopt, 37, false, true }, // fiber 69
        { std::nullopt, 38, false, false }, // fiber 70
        { std::nullopt, 39, false, false }, // fiber 71
    };

    return (link_idx < link_config.size()) ? std::make_optional(link_config[link_idx]) : std::nullopt;
}

void gem::hardware::board::set_link_polarity(const uint8_t link_idx, const bool is_tx, const bool polarity)
{
    const auto link_config = get_link_config(link_idx);
    if (!link_config)
        return;

    if (is_tx && link_config->tx_mgt_idx) {
        utils::writeReg("BEFE.MGTS.MGT" + std::to_string(*link_config->tx_mgt_idx) + ".CTRL.TX_POLARITY", link_config->tx_inverted ^ polarity);
    } else if (link_config->rx_mgt_idx) {
        utils::writeReg("BEFE.MGTS.MGT" + std::to_string(*link_config->rx_mgt_idx) + ".CTRL.RX_POLARITY", link_config->rx_inverted ^ polarity);
    }
}

void gem::hardware::board::configure_all_mgts()
{
    LOG4CPLUS_INFO(logger, "Configure the MGT");

    for (uint32_t i = 0; i < 68; i++) {
        const auto register_base = std::string { "BEFE.MGTS.MGT" } + std::to_string(i) + ".CTRL.";
        utils::writeReg(register_base + "CPLL_POWERDOWN", 0);
        utils::writeReg(register_base + "LOOPBACK", 0);
        utils::writeReg(register_base + "RX_LOW_POWER_MODE", 1);
        utils::writeReg(register_base + "RX_POWERDOWN", 0);
        utils::writeReg(register_base + "TX_INHIBIT", 0);
        utils::writeReg(register_base + "TX_POWERDOWN", 0);
    }

    // Default to unswapped polarity for all MGT assigned to links
    for (uint8_t link_idx = 0;; ++link_idx) {
        if (!get_link_config(link_idx))
            break;

        set_link_polarity(link_idx, true /* TX */, false);
        set_link_polarity(link_idx, false /* RX */, false);
    }
}

void gem::hardware::board::reset_all_mgts()
{
    LOG4CPLUS_INFO(logger, "Reset the MGT");

    for (uint32_t i = 0; i < 68; i++) {
        const auto register_base = std::string { "BEFE.MGTS.MGT" } + std::to_string(i) + ".RESET.";
        utils::writeReg(register_base + "TX_RESET", 1);
        utils::writeReg(register_base + "RX_RESET", 1);
    }

    utils::assert_n(
        10,
        []() -> bool {
            // Wait for the reset procedure completion
            std::this_thread::sleep_for(std::chrono::milliseconds(10));

            // Check the MGT status
            std::vector<uint32_t> notDoneTx, notDoneRx;
            for (uint32_t i = 0; i < 68; i++) {
                const auto register_base = std::string { "BEFE.MGTS.MGT" } + std::to_string(i) + ".STATUS.";
                if (utils::readReg(register_base + "TX_RESET_DONE") != 1)
                    notDoneTx.push_back(i);
                if (utils::readReg(register_base + "RX_RESET_DONE") != 1)
                    notDoneRx.push_back(i);
            }

            if (!notDoneTx.empty() || !notDoneRx.empty()) {
                LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Failed to reset the MGT (Tx: {} - Rx: {})"), fmt::join(notDoneTx, ", "), fmt::join(notDoneRx, ", ")));
                return false; // all MGT must be reset
            }

            return true; // success
        },
        "Failed to reset the MGT");
}

void gem::hardware::board::reset_all_mgt_plls()
{
    LOG4CPLUS_INFO(logger, "Reset the MGT PLL");

    // The CPLL must be powered off first due to a Virtex-7 silicon bug
    for (uint32_t i = 0; i < 68; i++) {
        const auto register_base = std::string { "BEFE.MGTS.MGT" } + std::to_string(i) + ".CTRL.";
        utils::writeReg(register_base + "CPLL_POWERDOWN", 1);
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    for (uint32_t i = 0; i < 68; i++) {
        const auto register_base = std::string { "BEFE.MGTS.MGT" } + std::to_string(i) + ".CTRL.";
        utils::writeReg(register_base + "CPLL_POWERDOWN", 0);
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    // Proceed with the CPLL and QPLL resets
    for (uint32_t i = 0; i < 68; i++) {
        const auto register_base = std::string { "BEFE.MGTS.MGT" } + std::to_string(i) + ".RESET.";
        utils::writeReg(register_base + "CPLL_RESET", 1);
        utils::writeReg(register_base + "QPLL_RESET", 1);
    }

    utils::assert_n(
        10,
        []() -> bool {
            std::this_thread::sleep_for(std::chrono::milliseconds(50));

            std::vector<uint32_t> notLockedCPLL, notLockedQPLL;
            for (uint32_t i = 0; i < 68; i++) {
                const auto register_base = std::string { "BEFE.MGTS.MGT" } + std::to_string(i) + ".STATUS.";
                if (utils::readReg(register_base + "CPLL_LOCKED") != 1)
                    notLockedCPLL.push_back(i);
                if (utils::readReg(register_base + "QPLL_LOCKED") != 1)
                    notLockedQPLL.push_back(i);
            }

            if (!notLockedCPLL.empty() || !notLockedQPLL.empty()) {
                LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Failed to reset the MGT PLL (CPLL: {} - QPLL: {})"), fmt::join(notLockedCPLL, ", "), fmt::join(notLockedQPLL, ", ")));
                return false; // all PLL must lock
            }

            return true; // sucess
        },
        "Failed to reset the MGT PLL");
}

// == promless.h ==

void gem::hardware::board::load_promless(const std::string& filename)
{
    LOG4CPLUS_INFO(logger, "Load the OptoHybrid firmware into RAM");

    // Get the firmware and its size (rounded-up to a multiple of 4 bytes)
    std::ifstream optohybrid_fw(filename, std::ifstream::ate | std::ifstream::binary);
    const uint32_t optohybrid_fw_size = ((static_cast<uint32_t>(optohybrid_fw.tellg()) + 3) / 4) * 4;

    // GEM loader IP core
    utils::execute_command(std::string { "/bin/gemloader load " } + filename);
    utils::writeRawAddress(GEM_LOADER_SIZE_ADDRESS, optohybrid_fw_size);
    utils::writeRawAddress(GEM_LOADER_ENABLE_ADDRESS, 0x1); // Enable bit
}

uint32_t gem::hardware::board::promless_size()
{
    return utils::readRawAddress(GEM_LOADER_SIZE_ADDRESS);
}
