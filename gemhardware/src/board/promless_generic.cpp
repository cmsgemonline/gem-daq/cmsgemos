/// @file

#include "promless.h"

#include "../utils.h"

#include <fstream>

void gem::hardware::board::load_promless(const std::string& filename)
{
    LOG4CPLUS_INFO(logger, "Load the OptoHybrid firmware into RAM");

    if (!utils::regExists("BEFE.PROMLESS.FIRMWARE_SIZE")) {
        LOG4CPLUS_INFO(logger, "Board without PROM-less OptoHybrid support, skipping");
        return;
    }

    std::ifstream optohybrid_fw(filename, std::ifstream::ate | std::ifstream::binary);
    if (!optohybrid_fw)
        throw std::runtime_error("Error while opening OptoHybrid firmware file");

    // Get the firmware size (rounded-up to a multiple of 4 bytes)
    const uint32_t optohybrid_fw_size = ((static_cast<uint32_t>(optohybrid_fw.tellg()) + 3) / 4) * 4;

    // Write the data
    optohybrid_fw.seekg(0);
    utils::writeReg("BEFE.PROMLESS.RESET_ADDR", 1);

    for (size_t address = 0; address < optohybrid_fw_size / 4; ++address) {
        uint32_t value = 0;
        optohybrid_fw.read(reinterpret_cast<char*>(&value), sizeof(value));
        utils::writeReg("BEFE.PROMLESS.WRITE_DATA", value);
    }

    // Write the size
    utils::writeReg("BEFE.PROMLESS.FIRMWARE_SIZE", optohybrid_fw_size);
}

uint32_t gem::hardware::board::promless_size()
{
    return utils::readReg("BEFE.PROMLESS.FIRMWARE_SIZE");
}
