/// @file

#ifndef SRC_GEM_HARDWARE_BOARD_BOARD_H
#define SRC_GEM_HARDWARE_BOARD_BOARD_H

#include <string>

// Forward declarations
namespace xhal {
namespace memhub {
    class register_memhub;
    class sigbus_memhub;
}
}

namespace gem {
namespace hardware {
    namespace board {

        /// @brief CTP7 specific namespace.
        namespace ctp7 {
            using memory_type = xhal::memhub::sigbus_memhub;
            constexpr uint32_t memory_base = 0x4000'0000;
            constexpr uint32_t memory_size = 0x4000'0000;

            constexpr uint32_t register_base = 0x2400'0000;
        }

        /// @brief CVP13 specific namespace.
        namespace cvp13 {
            using memory_type = xhal::memhub::register_memhub;
            constexpr uint32_t memory_base = 0x0;
            constexpr uint32_t memory_size = 0x0400'0000;

            constexpr uint32_t register_base = 0x0;
        }

        /// @brief APEX specific namespace.
        namespace apex {
            using memory_type = xhal::memhub::sigbus_memhub;
            constexpr uint32_t memory_base = 0x5000'0000; // FPGA0
            // constexpr uint32_t memory_base = 0x5800'0000; // FPGA1
            constexpr uint32_t memory_size = 0x0400'0000;

            constexpr uint32_t register_base = 0x0;
        }

        /// @brief X2O specific namespace.
        namespace x2o {
            using memory_type = xhal::memhub::register_memhub;
            constexpr uint64_t memory_base = 0x4'5000'0000; // FPGA0
            // constexpr uint64_t memory_base = 0x4'6000'0000; // FPGA1
            constexpr uint32_t memory_size = 0x0400'0000;

            constexpr uint32_t register_base = 0x0;
        }

        using namespace GEM_BACKEND;

        /// @brief Recover the backend board
        void cold_reset();

        /// @brief Reset the main TTC clock
        void reset_ttc_clock();

    } // namespace gem::hardware::board
} // namespace gem::hardware
} // namespace gem

#endif // SRC_GEM_HARDWARE_BOARD_BOARD_H
