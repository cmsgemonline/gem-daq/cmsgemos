/// @file

#ifndef SRC_GEM_HARDWARE_BOARD_MGT_H
#define SRC_GEM_HARDWARE_BOARD_MGT_H

#include <cstdint>
#include <optional>

namespace gem {
namespace hardware {
    namespace board {

        /// @brief Structure describing the configuration of a link
        struct link_config_t {
            std::optional<uint8_t> tx_mgt_idx; ///< Index of the MGT TX associated to this link TX, if any
            std::optional<uint8_t> rx_mgt_idx; ///< Index of the MGT RX associated to this link RX, if any
            bool tx_inverted; ///< Whether or not the TX is inverted on the board
            bool rx_inverted; ///< Whether or not the RX is inverted on the board
        };

        /// @brief Retrieves the configuration of a given link
        ///
        /// @param link_idx The link index
        ///
        /// @return The link configuration structure if the link exists
        std::optional<link_config_t> get_link_config(uint8_t link_idx);

        /// @brief Change the link polarity to @c polarity
        ///
        /// @param @c link_idx The link index
        /// @param @c is_tx Whether the TX or RX link is targeted
        /// @param @c polarity The desired polarity
        void set_link_polarity(uint8_t link_idx, bool is_tx, bool polarity);

        /// @brief Configures all FPGA multi-gigabit transceivers
        ///
        /// Note: any board polarity swap will be undone if the MGT has an
        ///       associated link. Otherwise, polarity is left undefined.
        void configure_all_mgts();

        /// @brief Reset all FPGA multi-gigabit transceivers
        ///
        /// Will succeed if and only if the clocks are stable (see @c reset_all_mgt_plls)
        void reset_all_mgts();

        /// @brief Reset all FPGA CPLL (when applicable) and QPLL
        void reset_all_mgt_plls();

    } // namespace gem::hardware::board
} // namespace gem::hardware
} // namespace gem

#endif // SRC_GEM_HARDWARE_BOARD_H
