/// @file

#ifndef SRC_GEM_HARDWARE_BOARD_PROMLESS_H
#define SRC_GEM_HARDWARE_BOARD_PROMLESS_H

#include <string>

namespace gem {
namespace hardware {
    namespace board {

        /// @brief Load the OptoHybrid firmware to memory for the PROM-less programming
        ///
        /// @param @c filename Path to the bitstream
        void load_promless(const std::string& filename);

        /// @brief Retrieves the size of the OptoHybrid firmware as configured in the PROM-less provider
        ///
        /// @return The size of the OptoHybrid firmware in bytes
        uint32_t promless_size();

    } // namespace gem::hardware::board
} // namespace gem::hardware
} // namespace gem

#endif // SRC_GEM_HARDWARE_BOARD_H
