/// @file

#include "mgt.h"

#include "../utils.h"

#include <fmt/format.h>

#include <chrono>
#include <thread>

namespace /* anonymous */ {

/// @brief Maps the MGT firmware types into symbolic constants
enum struct mgt_type_t : uint8_t {
    MGT_NULL = 0,
    MGT_GBTX = 1,
    MGT_LPGBT = 2,
    MGT_3P2G_8B10B = 3,
    MGT_TX_LPGBT_RX_3P2G_8B10B = 4,
    // 5 CSC-only
    // 6 CSC-only
    MGT_GBE = 7,
    // 8 CSC-only
    // 9 CSC-only
    MGT_TX_GBE_RX_LPGBT = 10,
};

} // namespace anonymous

std::optional<gem::hardware::board::link_config_t> gem::hardware::board::get_link_config(const uint8_t link_idx)
{
    static const std::vector<link_config_t> link_config = []() {
        // Extract the link configuration from the in-firmware description
        // registers.
        std::vector<link_config_t> tmp;

        const uint8_t number_mgts = utils::readReg("BEFE.SYSTEM.RELEASE.NUM_MGTS");
        const uint8_t number_links = utils::readReg("BEFE.SYSTEM.RELEASE.NUM_LINKS");

        for (uint8_t i = 0; i < number_links; i++) {
            const uint8_t tx_mgt_idx = utils::readReg("BEFE.SYSTEM.LINK_CONFIG.LINK" + std::to_string(i) + ".TX_MGT_IDX");
            const uint8_t rx_mgt_idx = utils::readReg("BEFE.SYSTEM.LINK_CONFIG.LINK" + std::to_string(i) + ".RX_MGT_IDX");
            const bool tx_inverted = utils::readReg("BEFE.SYSTEM.LINK_CONFIG.LINK" + std::to_string(i) + ".TX_INVERTED");
            const bool rx_inverted = utils::readReg("BEFE.SYSTEM.LINK_CONFIG.LINK" + std::to_string(i) + ".RX_INVERTED");
            tmp.push_back({ (tx_mgt_idx < number_mgts) ? std::make_optional(tx_mgt_idx) : std::nullopt,
                (rx_mgt_idx < number_mgts) ? std::make_optional(rx_mgt_idx) : std::nullopt,
                tx_inverted,
                rx_inverted });
        }

        return tmp;
    }();

    return (link_idx < link_config.size()) ? std::make_optional(link_config[link_idx]) : std::nullopt;
}

void gem::hardware::board::set_link_polarity(const uint8_t link_idx, const bool is_tx, const bool polarity)
{
    const auto link_config = get_link_config(link_idx);
    if (!link_config)
        return;

    if (is_tx && link_config->tx_mgt_idx) {
        utils::writeReg("BEFE.MGTS.MGT" + std::to_string(*link_config->tx_mgt_idx) + ".CTRL.TX_POLARITY", link_config->tx_inverted ^ polarity);
    } else if (link_config->rx_mgt_idx) {
        utils::writeReg("BEFE.MGTS.MGT" + std::to_string(*link_config->rx_mgt_idx) + ".CTRL.RX_POLARITY", link_config->rx_inverted ^ polarity);
    }
}

void gem::hardware::board::configure_all_mgts()
{
    LOG4CPLUS_INFO(logger, "Configure the MGT");

    const uint32_t number_mgts = utils::readReg("BEFE.SYSTEM.RELEASE.NUM_MGTS");

    for (uint32_t i = 0; i < number_mgts; i++) {
        const auto register_base = std::string { "BEFE.MGTS.MGT" } + std::to_string(i) + ".CTRL.";
        utils::writeReg(register_base + "LOOPBACK", 0);
        utils::writeReg(register_base + "QPLL_POWERDOWN", 0);
        utils::writeReg(register_base + "RX_LOW_POWER_MODE", 1);
        utils::writeReg(register_base + "RX_POWERDOWN", 0);
        utils::writeReg(register_base + "TX_DIFF_CTRL", 0x18);
        utils::writeReg(register_base + "TX_INHIBIT", 0);
        utils::writeReg(register_base + "TX_MAIN_CURSOR", 0);
        utils::writeReg(register_base + "TX_POST_CURSOR", 0);
        utils::writeReg(register_base + "TX_POWERDOWN", 0);
        utils::writeReg(register_base + "TX_PRE_CURSOR", 0);
    }

    // Default to unswapped polarity for all MGT assigned to links
    for (uint8_t link_idx = 0;; ++link_idx) {
        if (!get_link_config(link_idx))
            break;

        set_link_polarity(link_idx, true /* TX */, false);
        set_link_polarity(link_idx, false /* RX */, false);
    }
}

void gem::hardware::board::reset_all_mgts()
{
    LOG4CPLUS_INFO(logger, "Reset the MGT");

    const uint32_t number_mgts = utils::readReg("BEFE.SYSTEM.RELEASE.NUM_MGTS");

    for (uint32_t i = 0; i < number_mgts; i++) {
        const auto register_base = std::string { "BEFE.MGTS.MGT" } + std::to_string(i) + ".CTRL.";
        utils::writeReg(register_base + "TX_RESET", 1);
        utils::writeReg(register_base + "RX_RESET", 1);
    }

    utils::assert_n(
        10,
        [number_mgts]() -> bool {
            // Wait for the reset procedure completion
            std::this_thread::sleep_for(std::chrono::milliseconds(10));

            // Check the MGT status
            std::vector<uint32_t> notDoneTx, notDoneRx;
            for (uint32_t i = 0; i < number_mgts; i++) {
                const auto register_base = std::string { "BEFE.MGTS.MGT" } + std::to_string(i) + ".STATUS.";
                if (utils::readReg(register_base + "TX_RESET_DONE") != 1)
                    notDoneTx.push_back(i);
                if (utils::readReg(register_base + "RX_RESET_DONE") != 1)
                    notDoneRx.push_back(i);
            }

            if (!notDoneTx.empty() || !notDoneRx.empty()) {
                LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Failed to reset the MGT (Tx: {} - Rx: {})"), fmt::join(notDoneTx, ", "), fmt::join(notDoneRx, ", ")));
                return false; // all MGT must be reset
            }

            return true; // success
        },
        "Failed to reset the MGT");
}

void gem::hardware::board::reset_all_mgt_plls()
{
    LOG4CPLUS_INFO(logger, "Reset the MGT PLL");

    const uint32_t number_mgts = utils::readReg("BEFE.SYSTEM.RELEASE.NUM_MGTS");

    for (uint32_t i = 0; i < number_mgts; i++) {
        const auto register_base = std::string { "BEFE.MGTS.MGT" } + std::to_string(i) + ".CTRL.";
        utils::writeReg(register_base + "CPLL_RESET", 1);
        utils::writeReg(register_base + "QPLL0_RESET", 1);
        utils::writeReg(register_base + "QPLL1_RESET", 1);
    }

    utils::assert_n(
        10,
        [number_mgts]() -> bool {
            std::this_thread::sleep_for(std::chrono::milliseconds(50));

            std::vector<uint32_t> notLockedCPLL, notLockedQPLL0, notLockedQPLL1;
            for (uint32_t i = 0; i < number_mgts; i++) {
                const auto register_base = std::string { "BEFE.MGTS.MGT" } + std::to_string(i) + ".STATUS.";
                /* if (utils::readReg(register_base + "CPLL_LOCKED") != 1) */
                /*     notLockedCPLL.push_back(i); */
                if (utils::readReg(register_base + "QPLL0_LOCKED") != 1)
                    notLockedQPLL0.push_back(i);
                if (utils::readReg(register_base + "QPLL1_LOCKED") != 1)
                    notLockedQPLL1.push_back(i);
            }

            if (!notLockedCPLL.empty() || !notLockedQPLL0.empty() || !notLockedQPLL1.empty()) {
                LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Failed to reset the MGT PLL (CPLL: {} - QPLL0: {} - QPLL1: {})"), fmt::join(notLockedCPLL, ", "), fmt::join(notLockedQPLL0, ", "), fmt::join(notLockedQPLL1, ", ")));
                return false; // all PLL must lock
            }

            return true; // sucess
        },
        "Failed to reset the MGT PLL");
}
