/// @file
/// @brief AMC SCA methods for RPC modules

#include "sca.h"

#include "exception.h"
#include "hw_constants.h"
#include "utils.h"

#include <fmt/format.h>

#include <chrono>
#include <mutex>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

uint32_t gem::hardware::sca::formatSCAData(const uint32_t data)
{
    return (
        ((data & 0xff000000) >> 24) + ((data >> 8) & 0x0000ff00) + ((data & 0x0000ff00) << 8) + ((data & 0x000000ff) << 24));
}

uint32_t gem::hardware::sca::scaReady()
{
    // To be ready an SCA must be ready and exempt of critical error
    const uint32_t ready = utils::readReg("BEFE.GEM.SLOW_CONTROL.SCA.STATUS.READY");
    const uint32_t error = utils::readReg("BEFE.GEM.SLOW_CONTROL.SCA.STATUS.CRITICAL_ERROR");
    return ready & ~error;
}

uint32_t gem::hardware::sca::scaModuleReset(uint32_t optohybridMask)
{
    // Atomic operation (reset + status check)
    std::lock_guard<decltype(*memhub)> lock(*memhub);

    // Reset the desired SCA
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.SCA.CTRL.MODULE_RESET", optohybridMask);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));

    // Return the SCA for which the reset was successful
    return scaReady() & optohybridMask;
}

uint32_t gem::hardware::sca::scaOHExternalReset(const uint32_t optohybridMask)
{
    // Atomic operation (reset + status check)
    std::lock_guard<decltype(*memhub)> lock(*memhub);

    utils::writeReg("BEFE.GEM.SLOW_CONTROL.SCA.CTRL.OH_EXT_RESET", optohybridMask);

    // Return the SCA for which the reset was successful
    return scaReady() & optohybridMask;
}

uint32_t gem::hardware::sca::sendSCACommand(const uint32_t optohybridMask, const SCAChannel ch, const uint8_t cmd, const uint8_t len, const uint32_t data)
{
    // Make the whole function atomic
    // From the multi-register transaction to the SCA status check
    std::lock_guard<decltype(*memhub)> lock(*memhub);

    utils::writeReg("BEFE.GEM.SLOW_CONTROL.SCA.MANUAL_CONTROL.LINK_ENABLE_MASK", optohybridMask);

    utils::writeReg("BEFE.GEM.SLOW_CONTROL.SCA.MANUAL_CONTROL.SCA_CMD.SCA_CMD_CHANNEL", static_cast<uint8_t>(ch));
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.SCA.MANUAL_CONTROL.SCA_CMD.SCA_CMD_COMMAND", cmd);
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.SCA.MANUAL_CONTROL.SCA_CMD.SCA_CMD_LENGTH", len);
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.SCA.MANUAL_CONTROL.SCA_CMD.SCA_CMD_DATA", formatSCAData(data));

    try {
        utils::writeReg("BEFE.GEM.SLOW_CONTROL.SCA.MANUAL_CONTROL.SCA_CMD.SCA_CMD_EXECUTE", 0x1);
    } catch (const xhal::memhub::memory_access_error&) {
        // A transaction error does not always mean an SCA transaction error,
        // but can merely be triggered by an AXI timeout. In such cases, wait a
        // bit longer in order to make sure that the SCA transaction is
        // completed. The timing is based on ADC conversions, measured at most
        // at ~200 µs, with a 10x safety factor.
        //
        // Rely instead on the critical error flag that is set when the SCA
        // transaction fails/timeouts. Errors in the SCA packets are not
        // considered at the moment since they appear to be unreliable.
        std::this_thread::sleep_for(std::chrono::milliseconds(2));
    }

    return scaReady() & optohybridMask;
}

std::vector<std::optional<uint32_t>> gem::hardware::sca::sendSCACommandWithReply(uint32_t optohybridMask, const SCAChannel ch, const uint8_t cmd, const uint8_t len, const uint32_t data)
{
    std::vector<std::optional<uint32_t>> reply;
    reply.reserve(amc::number_oh());

    {
        // Make sure no other transactions happens before safely storing the SCA reply values
        std::lock_guard<decltype(*memhub)> lock(*memhub);

        optohybridMask = sendSCACommand(optohybridMask, ch, cmd, len, data);

        for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
            if ((optohybridMask >> ohN) & 0x1) {
                const auto t_register_name = fmt::format(FMT_STRING("BEFE.GEM.SLOW_CONTROL.SCA.MANUAL_CONTROL.SCA_REPLY_OH{}.SCA_RPY_DATA"), ohN);
                reply.push_back(formatSCAData(utils::readReg(t_register_name)));
            } else
                reply.push_back(std::nullopt);
        }
    }

    return reply;
}

std::vector<std::optional<uint32_t>> gem::hardware::sca::scaCTRLCommand(const uint32_t optohybridMask, const CTRLCommand cmd, const uint32_t data)
{
    std::vector<std::optional<uint32_t>> result;

    switch (cmd) {
    case CTRLCommand::CTRL_W_CRB:
        sendSCACommand(optohybridMask, SCAChannel::CTRL, static_cast<uint8_t>(cmd), 0x1, data);
        break;
    case CTRLCommand::CTRL_R_CRB:
        result = sendSCACommandWithReply(optohybridMask, SCAChannel::CTRL, static_cast<uint8_t>(cmd), 0x1, data);
        break;
    case CTRLCommand::CTRL_W_CRC:
        sendSCACommand(optohybridMask, SCAChannel::CTRL, static_cast<uint8_t>(cmd), 0x1, data);
        break;
    case CTRLCommand::CTRL_R_CRC:
        result = sendSCACommandWithReply(optohybridMask, SCAChannel::CTRL, static_cast<uint8_t>(cmd), 0x1, data);
        break;
    case CTRLCommand::CTRL_W_CRD:
        sendSCACommand(optohybridMask, SCAChannel::CTRL, static_cast<uint8_t>(cmd), 0x1, data);
        break;
    case CTRLCommand::CTRL_R_CRD:
        result = sendSCACommandWithReply(optohybridMask, SCAChannel::CTRL, static_cast<uint8_t>(cmd), 0x1, data);
        break;
    case CTRLCommand::CTRL_R_ID:
        result = sendSCACommandWithReply(optohybridMask, SCAChannel::ADC, static_cast<uint8_t>(cmd), 0x1, 0x1);
        break;
    case CTRLCommand::CTRL_R_SEU:
        result = sendSCACommandWithReply(optohybridMask, SCAChannel::JTAG, static_cast<uint8_t>(cmd), 0x1, 0x0);
        break;
    case CTRLCommand::CTRL_C_SEU:
        result = sendSCACommandWithReply(optohybridMask, SCAChannel::JTAG, static_cast<uint8_t>(cmd), 0x1, 0x0);
        break;
    default:
        throw std::runtime_error("Unknown SCA CTRL command: " + std::to_string(static_cast<uint8_t>(cmd)));
    }

    return result;
}

std::vector<std::optional<uint32_t>> gem::hardware::sca::scaGPIOCommand(const uint32_t optohybridMask, const GPIOCommand cmd, const uint8_t len, const uint32_t data)
{
    return sendSCACommandWithReply(optohybridMask, SCAChannel::GPIO, static_cast<uint8_t>(cmd), len, data);
}

std::vector<std::optional<uint32_t>> gem::hardware::sca::readSCAADC(uint32_t optohybridMask, const ADCChannel ch)
{
    // Activate the current source if needed
    optohybridMask = sendSCACommand(optohybridMask, SCAChannel::ADC, static_cast<uint8_t>(ADCCommand::ADC_W_CURR), 0x4, ADC_CURRENT_MASK);

    // Select the ADC channel
    optohybridMask = sendSCACommand(optohybridMask, SCAChannel::ADC, static_cast<uint8_t>(ADCCommand::ADC_W_MUX), 0x4, static_cast<uint8_t>(ch));

    // Convert!
    return sendSCACommandWithReply(optohybridMask, SCAChannel::ADC, static_cast<uint8_t>(ADCCommand::ADC_GO), 0x4, 0x1);
}

std::vector<std::optional<uint32_t>> gem::hardware::sca::readSCAChipID(const uint32_t optohybridMask)
{
    return scaCTRLCommand(optohybridMask, CTRLCommand::CTRL_R_ID);
}

std::vector<std::optional<uint32_t>> gem::hardware::sca::readSCASEUCounter(const uint32_t optohybridMask, const bool reset)
{
    if (reset)
        resetSCASEUCounter(optohybridMask);

    return scaCTRLCommand(optohybridMask, CTRLCommand::CTRL_R_SEU);
}

void gem::hardware::sca::resetSCASEUCounter(const uint32_t optohybridMask)
{
    scaCTRLCommand(optohybridMask, CTRLCommand::CTRL_C_SEU);
}
