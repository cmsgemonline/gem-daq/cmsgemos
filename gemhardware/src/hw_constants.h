/// @file
/// @brief Header containing the hardware related constants.

#ifndef GEM_HARDWARE_HW_CONSTANTS_H
#define GEM_HARDWARE_HW_CONSTANTS_H

#include "lpgbt.h"
#include "sca_enums.h"
#include "utils.h"

#include <array>
#include <stdint.h>
#include <string>

namespace gem {
namespace hardware {

    /// @brief ME0 specific namespace
    namespace me0 {
        constexpr uint8_t SW_GEM_STATION = 0; ///< GEM station for which gemhardware is built
    } // namespace gem::hardware::amc::me0

    /// @brief GE1/1 specific namespace
    namespace ge11 {
        constexpr uint8_t SW_GEM_STATION = 1; ///< GEM station for which gemhardware is built
    } // namespace gem::hardware::amc::ge11

    /// @brief GE2/1 specific namespace
    namespace ge21 {
        constexpr uint8_t SW_GEM_STATION = 2; ///< GEM station for which gemhardware is built
    } // namespace gem::hardware::amc::ge21

    using namespace GEM_STATION;

    /// @brief This namespace hold the constants related to the AMC
    namespace amc {
        /// @brief Returns the number of OptoHybrids supported by the AMC
        ///
        /// @details This value is a firmware constant so it can easily be
        ///          cached. Cache it as a static variable to avoid useless and
        ///          slow register accesses in thight loops,...
        inline uint8_t number_oh()
        {
            static uint8_t value = []() {
                try {
                    const auto hw_number_oh = utils::readReg("BEFE.GEM.GEM_SYSTEM.RELEASE.NUM_OF_OH");
                    return hw_number_oh > 16 ? 16 : hw_number_oh;
                } catch (const xhal::memhub::memory_access_error& e) {
                    // Prevent a back-end memory error to be caught as a front-end communication instability
                    const auto message = std::string { "Could not retrieve the number of OptoHybrid with '" } + e.what() + "'";
                    throw std::runtime_error(message);
                }
            }();
            return value;
        };

        /// @brief Returns the mask with all OptoHybrids supported by the AMC enabled
        inline uint32_t full_oh_mask()
        {
            return (1U << number_oh()) - 1;
        };

        /// @brief ME0 specific namespace
        namespace me0 {
            constexpr bool MGT_RX_POLARITY_GBT = 0; ///< Whether the RX polarity should be inverted or not on the lpGBT links
            constexpr bool MGT_TX_POLARITY_GBT = 0; ///< Whether the TX polarity should be inverted or not on the lpGBT links
        } // namespace gem::hardware::amc::me0

        /// @brief GE1/1 specific namespace.
        namespace ge11 {
            constexpr bool MGT_RX_POLARITY_GBT = 0; ///< Whether the RX polarity should be inverted or not on GBTx links
            constexpr bool MGT_TX_POLARITY_GBT = 1; ///< Whether the TX polarity should be inverted or not on GBTx links
        } // namespace gem::hardware::amc::ge11

        /// @brief GE2/1 specific namespace
        namespace ge21 {
            constexpr bool MGT_RX_POLARITY_GBT = 1; ///< Whether the RX polarity should be inverted or not on GBTx links
            constexpr bool MGT_TX_POLARITY_GBT = 0; ///< Whether the TX polarity should be inverted or not on GBTx links
        } // namespace gem::hardware::amc::ge21

        using namespace GEM_STATION;
    } // namespace gem::hardware::amc

    /// @brief This namespace hold the constants related to the OptoHybrid
    namespace oh {
        /// @brief ME0 specific namespace.
        namespace me0 {
            constexpr uint32_t VFATS_PER_OH = 24; ///< The number of VFAT's per OptoHybrid
            constexpr size_t OH_SINGLE_RAM_SIZE = 2 * 100; ///< Per-OH RAM size: for ME0 100 32-bit words of configuration per OH plus the corresponding OH local address
            constexpr uint32_t CHANNELS_PER_OH = 3072; ///< The number of VFAT channels per OptoHybrid
            constexpr uint32_t SBITS_PER_OH = 1536; ///< The number of S-bits per OptoHybrid
        } // namespace gem::hardware::oh::me0

        /// @brief GE1/1 specific namespace
        namespace ge11 {
            constexpr uint32_t VFATS_PER_OH = 24; ///< The number of VFAT's per OptoHybrid
            constexpr size_t OH_SINGLE_RAM_SIZE = 2 * 100; ///< Per-OH RAM size: for GE1/1 100 32-bit words of configuration per OH plus the corresponding OH local address
            constexpr uint32_t CHANNELS_PER_OH = 3072; ///< The number of VFAT channels per OptoHybrid
            constexpr uint32_t SBITS_PER_OH = 1536; ///< The number of S-bits per OptoHybrid
        } // namespace gem::hardware::oh::ge11

        /// @brief GE2/1 specific namespace.
        namespace ge21 {
            constexpr uint32_t VFATS_PER_OH = 12; ///< The number of VFAT's per OptoHybrid
            constexpr size_t OH_SINGLE_RAM_SIZE = 2 * 100; ///< Per-OH RAM size: for GE2/1 100 32-bit words of configuration per OH plus the corresponding OH local address
            constexpr uint32_t CHANNELS_PER_OH = 1536; ///< The number of VFAT channels per OptoHybrid
            constexpr uint32_t SBITS_PER_OH = 768; ///< The number of S-bits per OptoHybrid
        } // namespace gem::hardware::oh::ge21

        using namespace GEM_STATION;

        constexpr uint32_t FULL_VFAT_MASK = (1u << VFATS_PER_OH) - 1; ///< Mask with all VFATs enabled
    } // namespace gem::hardware::oh

    /// @brief This namespace hold the constants related to the OptoHybrid
    namespace vfat {
        /// @brief ME0 specific namespace
        namespace me0 {
            constexpr size_t VFAT_SINGLE_RAM_SIZE = 74; ///< Per-VFAT RAM size: for ME0 24 VFATs per OH, 74 32-bit words of configuration per VFAT (147 16-bit VFAT configurations -> 32-bit words + padding)

            /// @brief HDLC addresses for each VFAT number
            ///
            /// Found in ME0 GEB documentation:
            /// https://twiki.cern.ch/twiki/pub/CMS/ME0PKU/ME0_GEB_specification_PKU_v1.0.pdf#page=6
            constexpr std::array<uint32_t, 24> HDLC_ADDRESSES {
                { 4, 3, 10, 9, 1, 3, 7, 9, 1, 5, 7, 11, 4, 5, 10, 11, 2, 6, 8, 12, 2, 6, 8, 12 }
            };
        } // namespace gem::hardware:vfat::me0

        /// @brief GE1/1 specific namespace
        namespace ge11 {
            constexpr size_t VFAT_SINGLE_RAM_SIZE = 74; ///< Per-VFAT RAM size: for GE1/1 24 VFATs per OH, 74 32-bit words of configuration per VFAT (147 16-bit VFAT configurations -> 32-bit words + padding)

            /// @brief HDLC addresses for each VFAT number
            ///
            /// All addresses are 0 since GE1/1 doesn't use the HDLC addressing.
            constexpr std::array<uint32_t, 24> HDLC_ADDRESSES {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
            };
        } // namespace gem::hardware:vfat::ge11

        /// @brief GE2/1 specific namespace
        namespace ge21 {
            constexpr size_t VFAT_SINGLE_RAM_SIZE = 74; ///< Per-VFAT RAM size: for GE2/1 12 VFATs per OH, 74 32-bit words of configuration per VFAT (147 16-bit VFAT configurations -> 32-bit words + padding)

            /// @brief HDLC addresses for each VFAT number
            ///
            /// Found in GE2/1 GEB documentation:
            /// https://twiki.cern.ch/twiki/pub/CMS/GE21GEBBoardTwiki/GE21_GEB_Interfaces_Specification_v2.pdf#page=18
            constexpr std::array<uint32_t, 12> HDLC_ADDRESSES {
                { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }
            };
        } // namespace gem::hardware:vfat::ge21

        using namespace GEM_STATION;
    } // namespace gem::hardware::vfat

    /// @brief This namespace hold the constants related to the GBT
    namespace gbt {
        /// @brief ME0 specific namespace
        namespace me0 {
            constexpr uint32_t GBTS_PER_OH = 8; ///< The number of GBT's per OptoHybrid (should be part of the oh namespace, as VFATS_PER_OH)

            constexpr uint16_t GBT_CONFIG_SIZE = 256; ///< Size of the GBT configuration address space
            constexpr uint16_t GBT_RW_REG_SIZE = 336; ///< Size of the GBT writable address space
            constexpr uint16_t GBT_RO_REG_SIZE = 494; ///< Size of the GBT readable address space

            /// @brief Mapping from VFAT index to GBT index
            constexpr std::array<uint32_t, 24> VFAT_TO_GBT {
                { 0, 0, 2, 2, 4, 4, 6, 6, 0, 1, 2, 3, 4, 5, 6, 7, 1, 1, 3, 3, 5, 5, 7, 7 }
            };

            /// @brief Mapping from VFAT index to the elink of its corresponding GBT.
            constexpr std::array<uint8_t, 24> VFAT_TO_ELINK {
                { 25, 27, 25, 27, 3, 27, 3, 27, 3, 11, 3, 11, 25, 11, 25, 11, 24, 6, 24, 6, 24, 6, 24, 6 }
            };

            constexpr uint16_t ELINK_MAX = 28; ///< Highest supported eLink
        } // namespace gem::hardware::gbt::me0

        /// @brief GE1/1 specific namespace
        namespace ge11 {
            constexpr uint32_t GBTS_PER_OH = 3; ///< The number of GBT's per OptoHybrid (should be part of the oh namespace, as VFATS_PER_OH)

            constexpr uint16_t GBT_CONFIG_SIZE = 366; ///< Size of the GBT configuration address space
            constexpr uint16_t GBT_RW_REG_SIZE = 366; ///< Size of the GBT writable address space
            constexpr uint16_t GBT_RO_REG_SIZE = 436; ///< Size of the GBT readable address space

            /// @brief Mapping from VFAT index to GBT index
            constexpr std::array<uint32_t, 24> VFAT_TO_GBT {
                { 1, 1, 1, 1, 1, 1, 1, 0, 1, 2, 2, 2, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2, 0 }
            };

            /// @brief Mapping from VFAT index to the elink of its corresponding GBT
            constexpr std::array<uint8_t, 24> VFAT_TO_ELINK {
                { 21, 37, 9, 13, 5, 33, 25, 25, 17, 5, 21, 17, 13, 9, 5, 1, 29, 33, 25, 29, 9, 13, 37, 33 }
            };

            constexpr uint16_t ELINK_MAX = 56; ///< Highest supported eLink
        } // namespace gem::hardware::gbt::ge11

        /// @brief GE2/1 specific namespace
        namespace ge21 {
            constexpr uint32_t GBTS_PER_OH = 2; ///< The number of GBT's per OptoHybrid (should be part of the oh namespace, as VFATS_PER_OH)

            constexpr uint16_t GBT_CONFIG_SIZE = 366; ///< Size of the GBT configuration address space
            constexpr uint16_t GBT_RW_REG_SIZE = 366; ///< Size of the GBT writable address space
            constexpr uint16_t GBT_RO_REG_SIZE = 436; ///< Size of the GBT readable address space

            /// @brief Mapping from VFAT index to GBT index
            constexpr std::array<uint32_t, 12> VFAT_TO_GBT {
                { 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 }
            };

            /// @brief Mapping from VFAT index to the elink of its corresponding GBT.
            constexpr std::array<uint8_t, 12> VFAT_TO_ELINK {
                { 5, 13, 1, 21, 9, 17, 17, 9, 21, 1, 13, 5 }
            };

            constexpr uint16_t ELINK_MAX = 56; ///< Highest supported eLink
        } // namespace gem::hardware::gbt::ge21

        using namespace GEM_STATION;

        constexpr uint32_t FULL_GBT_MASK = (1u << GBTS_PER_OH) - 1; ///< Mask with all GBTs enabled

        constexpr size_t GBT_SINGLE_RAM_SIZE = (GBT_CONFIG_SIZE + 3) / 4; ///< Per-GBT RAM size (8-bit GBT configuration registers + padding)

    } // namespace gem::hardware::gbt

    namespace sca {
        /// @brief ME0 specific namespace
        namespace me0 { /* no SCA */
            constexpr uint32_t ADC_CURRENT_MASK = 0; // FIXME no SCA on ME0
            constexpr size_t GPIO_FPGA_DONE_INPUT = 0; // FIXME no FPGA on ME0
        }

        /// @brief GE1/1 specific namespace
        namespace ge11 {
            constexpr uint32_t ADC_CURRENT_MASK = (1 << 0 /* VTTx CSC PT */) | (1 << 4 /* VTTx GEM PT */) | (1 << 7 /* GBT0 PT */) | (1 << 8 /* FPGA PT */) | (1 << 14 /* 1k calibration */);

            constexpr ADCChannel ADC_CURRENT_CALIBRATION_CHANNEL = ADCChannel::ADC_CH14;

            /// @brief List of voltages that can be measured through the SCA ADC
            ///
            /// The first element of the pair refers to the monitoring name
            /// while the second to the ADC channel
            const std::array<std::pair<std::string, ADCChannel>, 9> ADC_VOLTAGE_CHANNELS {
                { { "V1P0_FPGA_CORE", ADCChannel::ADC_CH17 },
                    { "V1P0_FPGA_MGT", ADCChannel::ADC_CH27 },
                    { "V1P2_FPGA_MGT", ADCChannel::ADC_CH30 },
                    { "V1P2_VFAT_FQA", ADCChannel::ADC_CH22 },
                    { "V1P2_VFAT_FQB", ADCChannel::ADC_CH23 },
                    { "V1P2_VFAT_FQC", ADCChannel::ADC_CH29 },
                    { "V1P2_VFAT_FQD", ADCChannel::ADC_CH26 },
                    { "V1P5", ADCChannel::ADC_CH24 },
                    { "V2P5", ADCChannel::ADC_CH15 } }
            };

            /// @brief List of currents that can be measured through the SCA ADC
            ///
            /// The first element of the pair refers to the monitoring name
            /// while the second to the ADC channel
            const std::array<std::pair<std::string, ADCChannel>, 0> ADC_CURRENT_CHANNELS { /* no current monitoring on GE1/1 */ };

            /// @brief List of temperature sensors connected to the SCA
            ///
            /// The first element of the pair refers to the monitoring name
            /// while the second to the ADC channel
            const std::array<std::pair<std::string, ADCChannel>, 4> ADC_PT_CHANNELS {
                { { "FPGA_PT", ADCChannel::ADC_CH08 },
                    { "GBT0_PT", ADCChannel::ADC_CH07 },
                    { "VTTX_CSC_PT", ADCChannel::ADC_CH00 },
                    { "VTTX_GEM_PT", ADCChannel::ADC_CH04 } }
            };

            constexpr double ADC_RSSI_R1 = 3000.0;
            constexpr double ADC_RSSI_R2 = 1000.0;
            constexpr double ADC_RSSI_VCC = 2.6;

            /// @brief List of channels used to measure the VTRx RSSI
            constexpr std::array<ADCChannel, 3> ADC_RSSI_CHANNELS {
                { ADCChannel::ADC_CH21, ADCChannel::ADC_CH19, ADCChannel::ADC_CH18 }
            };

            // @brief SCA GPIO input to which the FPGA DONE signal is connected
            constexpr size_t GPIO_FPGA_DONE_INPUT = 6;
        }

        /// @brief GE2/1 specific namespace
        namespace ge21 {
            constexpr uint32_t ADC_CURRENT_MASK = (1 << 13 /* GBT0 PT */) | (1 << 14 /* GBT1 PT */) | (1 << 15 /* VTRx PT */) | (1 << 16 /* V1P5 PT */) | (1 << 17 /* 1k calibration */);

            constexpr ADCChannel ADC_CURRENT_CALIBRATION_CHANNEL = ADCChannel::ADC_CH17;

            const std::array<std::pair<std::string, ADCChannel>, 6> ADC_VOLTAGE_CHANNELS {
                { { "V1P0_FPGA_CORE", ADCChannel::ADC_CH00 },
                    { "V1P0_FPGA_MGT", ADCChannel::ADC_CH01 },
                    { "V1P2_FPGA_MGT", ADCChannel::ADC_CH02 },
                    { "V1P5", ADCChannel::ADC_CH04 },
                    { "V1P8", ADCChannel::ADC_CH03 },
                    { "V2P5", ADCChannel::ADC_CH05 } }
            };

            const std::array<std::pair<std::string, ADCChannel>, 5> ADC_CURRENT_CHANNELS {
                { { "I1P2_VFAT_A", ADCChannel::ADC_CH11 },
                    { "I1P2_VFAT_D", ADCChannel::ADC_CH12 },
                    { "I1P5", ADCChannel::ADC_CH09 },
                    { "I1P8", ADCChannel::ADC_CH08 },
                    { "I2P5", ADCChannel::ADC_CH10 } }
            };

            const std::array<std::pair<std::string, ADCChannel>, 4> ADC_PT_CHANNELS {
                { { "GBT0_PT", ADCChannel::ADC_CH13 },
                    { "GBT1_PT", ADCChannel::ADC_CH14 },
                    { "VTRX_PT", ADCChannel::ADC_CH15 },
                    { "V1P5_PT", ADCChannel::ADC_CH16 } }
            };

            constexpr double ADC_RSSI_R1 = 2000.0;
            constexpr double ADC_RSSI_R2 = 1000.0;
            constexpr double ADC_RSSI_VCC = 2.5;

            constexpr std::array<ADCChannel, 2> ADC_RSSI_CHANNELS {
                { ADCChannel::ADC_CH06, ADCChannel::ADC_CH07 }
            };

            constexpr size_t GPIO_FPGA_DONE_INPUT = 6;
        }

        using namespace GEM_STATION;

        constexpr double ADC_TO_VOLT = 1. / 4096; // 12-bits ADC; 1V reference [V/ADC unit]
        constexpr double ADC_NOMINAL_CURRENT = 0.0001; // [A]

        // Constants for the SCA internal temperature sensor
        // Taken by eye from the SCA manual
        constexpr double ADC_TEMP_SLOPE = -0.140 / 75; // [V/deg(C)]
        constexpr double ADC_TEMP_INTERCEPT = 0.715; // [V]
    } // namespace gem::hardware::sca

    namespace lpgbt {
        /// @brief ME0 specific namespace
        namespace me0 {
            /// @brief Channels connected to a precision 1k resistor
            constexpr lpgbt::adcChannel ADC_PRECISION_CHANNELS[2] = { 7, 3 };

            const std::array<std::pair<std::string, std::pair<uint8_t, lpgbt::adcChannel>>, 50> ADC_VOLTAGE_CHANNELS {
                {
                    { "ASIAGO0.V2P5", { 1, 1 } },
                    { "ASIAGO1.V2P5", { 3, 1 } },
                    { "ASIAGO2.V2P5", { 5, 1 } },
                    { "ASIAGO3.V2P5", { 7, 1 } },
                    { "GBT0.VDD", { 0, 12 } },
                    { "GBT0.VDDA", { 0, 13 } },
                    { "GBT0.VDDRX", { 0, 11 } },
                    { "GBT0.VDDTX", { 0, 10 } },
                    { "GBT0.VREF", { 0, 15 } },
                    { "GBT1.VDD", { 1, 12 } },
                    { "GBT1.VDDA", { 1, 13 } },
                    { "GBT1.VDDRX", { 1, 11 } },
                    { "GBT1.VDDTX", { 1, 10 } },
                    { "GBT1.VREF", { 1, 15 } },
                    { "GBT2.VDD", { 2, 12 } },
                    { "GBT2.VDDA", { 2, 13 } },
                    { "GBT2.VDDRX", { 2, 11 } },
                    { "GBT2.VDDTX", { 2, 10 } },
                    { "GBT2.VREF", { 2, 15 } },
                    { "GBT3.VDD", { 3, 12 } },
                    { "GBT3.VDDA", { 3, 13 } },
                    { "GBT3.VDDRX", { 3, 11 } },
                    { "GBT3.VDDTX", { 3, 10 } },
                    { "GBT3.VREF", { 3, 15 } },
                    { "GBT4.VDD", { 4, 12 } },
                    { "GBT4.VDDA", { 4, 13 } },
                    { "GBT4.VDDRX", { 4, 11 } },
                    { "GBT4.VDDTX", { 4, 10 } },
                    { "GBT4.VREF", { 4, 15 } },
                    { "GBT5.VDD", { 5, 12 } },
                    { "GBT5.VDDA", { 5, 13 } },
                    { "GBT5.VDDRX", { 5, 11 } },
                    { "GBT5.VDDTX", { 5, 10 } },
                    { "GBT5.VREF", { 5, 15 } },
                    { "GBT6.VDD", { 6, 12 } },
                    { "GBT6.VDDA", { 6, 13 } },
                    { "GBT6.VDDRX", { 6, 11 } },
                    { "GBT6.VDDTX", { 6, 10 } },
                    { "GBT6.VREF", { 6, 15 } },
                    { "GBT7.VDD", { 7, 12 } },
                    { "GBT7.VDDA", { 7, 13 } },
                    { "GBT7.VDDRX", { 7, 11 } },
                    { "GBT7.VDDTX", { 7, 10 } },
                    { "GBT7.VREF", { 7, 15 } },
                    { "GEBN.VIN", { 0, 6 } },
                    { "GEBN.V1P2_A", { 0, 0 } },
                    { "GEBN.V1P2_D", { 2, 0 } },
                    { "GEBW.VIN", { 4, 6 } },
                    { "GEBW.V1P2_A", { 4, 0 } },
                    { "GEBW.V1P2_D", { 6, 0 } },
                }
            };

            const std::array<std::pair<std::string, std::pair<uint8_t, lpgbt::adcChannel>>, 4> ADC_CURRENT_CHANNELS {
                {
                    { "GEBN.I1P2_D", { 0, { 3, 0 } } },
                    { "GEBN.I1P2_A", { 2, { 3, 0 } } },
                    { "GEBW.I1P2_D", { 4, { 3, 0 } } },
                    { "GEBW.I1P2_A", { 6, { 3, 0 } } },
                }
            };

            const std::array<std::pair<std::string, std::pair<uint8_t, lpgbt::adcChannel>>, 22> ADC_TEMP_CHANNELS {
                {
                    { "ASIAGO0.OH_TH", { 1, 6 } },
                    { "ASIAGO0.VTRX_TH", { 1, 0 } },
                    { "ASIAGO1.OH_TH", { 3, 6 } },
                    { "ASIAGO1.VTRX_TH", { 3, 0 } },
                    { "ASIAGO2.OH_TH", { 5, 6 } },
                    { "ASIAGO2.VTRX_TH", { 5, 0 } },
                    { "ASIAGO3.OH_TH", { 7, 6 } },
                    { "ASIAGO3.VTRX_TH", { 7, 0 } },
                    // FIXME: Conversion factors?
                    { "GBT0.TEMP", { 0, 14 } },
                    { "GBT1.TEMP", { 1, 14 } },
                    { "GBT2.TEMP", { 2, 14 } },
                    { "GBT3.TEMP", { 3, 14 } },
                    { "GBT4.TEMP", { 4, 14 } },
                    { "GBT5.TEMP", { 5, 14 } },
                    { "GBT6.TEMP", { 6, 14 } },
                    { "GBT7.TEMP", { 7, 14 } },
                    { "GEBN.T1P2_A_TH", { 2, 1 } },
                    { "GEBN.T1P2_D_TH", { 0, 1 } },
                    { "GEBN.T2P5_TH", { 2, 6 } },
                    { "GEBW.T1P2_A_TH", { 6, 1 } },
                    { "GEBW.T1P2_D_TH", { 4, 1 } },
                    { "GEBW.T2P5_TH", { 6, 6 } },
                }
            };

            constexpr double ADC_RSSI_R1 = 4700.0;
            constexpr double ADC_RSSI_R2 = 1000000.0;
            constexpr double ADC_RSSI_R3 = 470000.0;
            constexpr double ADC_RSSI_VCC = 2.5;

            constexpr std::array<std::pair<uint8_t, lpgbt::adcChannel>, 4> ADC_RSSI_CHANNELS {
                { { 1, 5 }, { 3, 5 }, { 5, 5 }, { 7, 5 } }
            };

            /// @brief Mapping from the VFAT number to the (LpGBT, GPIO) pair controlling the external reset
            constexpr std::array<std::pair<uint8_t, uint8_t>, 24> VFAT_TO_RESET_GPIO {
                {
                    { 0, 8 },
                    { 0, 2 },
                    { 2, 8 },
                    { 2, 2 },
                    { 4, 0 },
                    { 4, 2 },
                    { 6, 0 },
                    { 6, 2 },
                    { 0, 0 },
                    { 1, 11 },
                    { 2, 0 },
                    { 3, 11 },
                    { 4, 8 },
                    { 5, 11 },
                    { 6, 8 },
                    { 7, 11 },
                    { 1, 9 },
                    { 1, 10 },
                    { 3, 9 },
                    { 3, 10 },
                    { 5, 9 },
                    { 5, 10 },
                    { 7, 9 },
                    { 7, 10 },
                }
            };
        }

        /// @brief GE1/1 specific namespace
        namespace ge11 { /* no lpGBT */
        }

        /// @brief GE2/1 specific namespace
        namespace ge21 { /* no lpGBT */
        }

        using namespace GEM_STATION;

        constexpr double ADC_TO_VOLT = 1. / 1024; // 10-bits ADC; 1V reference [V/ADC unit]
        constexpr double DAC_TO_AMPS = 900e-6 / 256; // 8-bits DAC; 900µA full-scale [A/DAC unit]
    } // namespace gem::hardware::lpgbt

} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_HW_CONSTANTS_H
