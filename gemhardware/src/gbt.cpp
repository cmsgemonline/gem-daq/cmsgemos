/// @file

#include "gbt.h"

#include "exception.h"
#include "hw_constants.h"
#include "hw_constants_checks.h"
#include "lpgbt.h"
#include "utils.h"
#include "vfat3.h"

#include <fmt/format.h>

#include <chrono>
#include <fstream>
#include <mutex>
#include <sstream>
#include <string>
#include <thread>

// local functions

gem::hardware::gbt::gbt_ic_error::gbt_ic_error(const uint32_t ohN, const uint32_t gbtN, const uint16_t address, const direction_t direction, const std::string& message)
    : m_ohN(ohN)
    , m_gbtN(gbtN)
    , m_address(address)
    , m_direction(direction)
    , m_message(message)
{
    m_what = fmt::format(FMT_STRING("Cannot access {:d} on OH{:d}-GBT{:d} [direction = {:s}, details = {:s}]"),
        m_address, m_ohN, m_gbtN, to_string(m_direction), m_message);
}

const char* gem::hardware::gbt::gbt_ic_error::to_string(const direction_t& direction)
{
    if (direction == direction_t::read)
        return "read";
    else if (direction == direction_t::write)
        return "write";
    else if (direction == direction_t::unknown)
        return "unknown";
    else
        return "invalid";
}

std::string gem::hardware::gbt::gbt_ic_error::create_message_from_ic_status()
{
    std::vector<std::string> reasons;

    if (!utils::readReg("BEFE.GEM.SLOW_CONTROL.IC.READ_STATUS.ADDRESS"))
        reasons.push_back("register address");
    if (!utils::readReg("BEFE.GEM.SLOW_CONTROL.IC.READ_STATUS.DOWNLINK_PARITY"))
        reasons.push_back("downlink parity");
    if (!utils::readReg("BEFE.GEM.SLOW_CONTROL.IC.READ_STATUS.GBT_I2C_ADDR"))
        reasons.push_back("i2c address");
    if (!utils::readReg("BEFE.GEM.SLOW_CONTROL.IC.READ_STATUS.READ_DONE"))
        reasons.push_back("read done");
    if (!utils::readReg("BEFE.GEM.SLOW_CONTROL.IC.READ_STATUS.READ_WRITE_LENGTH"))
        reasons.push_back("r/w length");
    if (!utils::readReg("BEFE.GEM.SLOW_CONTROL.IC.READ_STATUS.UPLINK_PARITY"))
        reasons.push_back("uplink parity");

    return fmt::format(FMT_STRING("wrong {:s}"), fmt::join(reasons, ", "));
}

std::pair<bool, uint32_t> gem::hardware::gbt::getReachableFrontend(uint8_t gbtMask)
{
#if !defined(GEM_IS_ME0)
    // Consider GBT0 as a master GBT (through the SCA)
    if (!(gbtMask & 0x1))
        gbtMask = 0;
#else
    // Disable the slave lpGBT (odd bits) if the corresponding master lpGBT
    // (lower even bits) is not locked
    const uint8_t masterMask = gbtMask & 0x55;
    const uint8_t slaveMask = gbtMask & 0xaa;
    gbtMask = masterMask | (slaveMask & (masterMask << 1));
#endif

    // No GBT is locked, there is nothing reachable
    if (!gbtMask)
        return { 0, 0 };

    // VFAT connected to a locked GBT can be accessed
    uint32_t vfatMask = 0;
    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN)
        if ((gbtMask >> VFAT_TO_GBT[vfatN]) & 0x1)
            vfatMask |= (1ULL << vfatN);

    return { 1, vfatMask };
}

std::array<gem::hardware::gbt::config_t, gem::hardware::gbt::GBTS_PER_OH> gem::hardware::gbt::getDefaultGBTConfigurations()
{
    const std::string optohybrid_firmware_dir = utils::get_optohybrid_firmware_dir();

    // Return object
    std::array<config_t, GBTS_PER_OH> gbtConfigurations;

    // Go through all GBT
    for (uint8_t gbtN = 0; gbtN < GBTS_PER_OH; ++gbtN) {
        config_t& gbtConfig = gbtConfigurations.at(gbtN);

        // Open configuration file
        const std::string gbtConfigFileName = optohybrid_firmware_dir + "/gbt" + std::to_string(gbtN) + ".txt";
        std::ifstream gbtConfigFile;
        gbtConfigFile.exceptions(std::ifstream::badbit);
        gbtConfigFile.open(gbtConfigFileName);

        /// Load GBT configuration from file
        ///
        /// The file must contain the GBT_CONFIG_SIZE values describing the
        /// the GBT configuration. It can optionally contain additional registers
        /// and/or meta-data values coming from programmer tool. Those are
        /// ignored and excluded from the configuration.
        ///
        /// Note 1: The @c config_t type defines an array of @c uint8_t. Since
        ///         it is a typedef for <tt> unsigned char </tt> it will be
        ///         treated as a character type by iostreams. Use a temporary
        ///         @c uint16_t variable and then cast it down.
        ///
        /// Note 2: The configuration file format is slightly different between
        ///         the GBTx and the lpGBT. The former one contains only the
        ///         register values while the latter one contains both the
        ///         register addresses and values. Use the address as a cross-check
        ///         against the line number.
        uint16_t gbtAddress = 0, value = 0;
#if !defined(GEM_IS_ME0)
        while (gbtConfigFile >> std::hex >> value) {
#else
        uint16_t configAddress = 0;
        while (gbtConfigFile >> std::hex >> configAddress >> value) {
            if (configAddress != gbtAddress)
                throw std::runtime_error("Sparse default lpGBT configuration files not supported");

#endif
            gbtConfig[gbtAddress] = value;
            if (++gbtAddress == GBT_CONFIG_SIZE)
                break;
        }

        if (gbtAddress != GBT_CONFIG_SIZE) {
            throw std::range_error("GBT configuration file size doesn't correspond to the expected one: size is " + std::to_string(gbtAddress));
        }
    }

    return gbtConfigurations;
}

uint8_t gem::hardware::gbt::readGBTReg(const uint32_t ohN, const uint32_t gbtN, const uint16_t address, const uint8_t mask, const bool unsafe)
{
    if (gbtN >= GBTS_PER_OH)
        throw gbt_ic_error(ohN, gbtN, address, gbt_ic_error::direction_t::read, "gbtN parameter larger than the number of GBT per OH (" + std::to_string(GBTS_PER_OH) + ")");

    if (address >= GBT_RO_REG_SIZE)
        throw gbt_ic_error(ohN, gbtN, address, gbt_ic_error::direction_t::read, "address outside of the read range [0," + std::to_string(GBT_RO_REG_SIZE) + "]");

    const auto shift = ::ffsl(mask);
    if (shift == 0)
        throw gbt_ic_error(ohN, gbtN, address, gbt_ic_error::direction_t::read, "mask cannot be 0x0");

    // This is a multi-register transaction
    std::lock_guard<decltype(*memhub)> lock(*memhub);

    // Configure communication channel
    const uint32_t linkN = ohN * GBTS_PER_OH + gbtN;
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.GBT_LINK_SELECT", linkN);

#if !defined(GEM_IS_ME0)
    // Rely on firmware defaults for GEx/1
#else
    // lpGBT master (slave) use I²C address 0x70 (0x71)
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.GBT_FRAME_FORMAT", 2);
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.GBT_I2C_ADDR", !(gbtN % 2) ? 0x70 : 0x71);
#endif

    // Prepare the transaction
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.ADDRESS", address);
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.READ_WRITE_LENGTH", 1);

    try {
        utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.EXECUTE_READ", 1);
        uint8_t data = utils::readReg("BEFE.GEM.SLOW_CONTROL.IC.READ_DATA");

        // Apply the mask
        // Note that the shift variable is starting at 1 (!)
        data = (data & mask) >> (shift - 1);

        return data;
    } catch (const xhal::memhub::memory_access_error&) {
        const auto error = gbt_ic_error(ohN, gbtN, address, gbt_ic_error::direction_t::read, gbt_ic_error::create_message_from_ic_status());
        if (!unsafe)
            throw error;
        LOG4CPLUS_WARN(logger, error.what());
        return 0;
    }
}

void gem::hardware::gbt::writeGBTReg(const uint32_t ohN, const uint32_t gbtN, const uint16_t address, uint8_t value, const uint8_t mask, const bool unsafe)
{
    if (gbtN >= GBTS_PER_OH)
        throw gbt_ic_error(ohN, gbtN, address, gbt_ic_error::direction_t::write, "gbtN parameter larger than the number of GBT per OH (" + std::to_string(GBTS_PER_OH) + ")");

    if (address >= GBT_RW_REG_SIZE)
        throw gbt_ic_error(ohN, gbtN, address, gbt_ic_error::direction_t::write, "address outside of the write range [0," + std::to_string(GBT_RW_REG_SIZE) + "]");

    const auto shift = ::ffsl(mask);
    if (shift == 0)
        throw gbt_ic_error(ohN, gbtN, address, gbt_ic_error::direction_t::write, "mask cannot be 0x0");

    // This is a multi-register transaction
    std::lock_guard<decltype(*memhub)> lock(*memhub);

    // Configure communication channel
    const uint32_t linkN = ohN * GBTS_PER_OH + gbtN;
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.GBT_LINK_SELECT", linkN);

#if !defined(GEM_IS_ME0)
    // Rely on firmware defaults for GEx/1
#else
    // lpGBT master (slave) use I²C address 0x70 (0x71)
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.GBT_FRAME_FORMAT", 2);
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.GBT_I2C_ADDR", !(gbtN % 2) ? 0x70 : 0x71);
#endif

    // Prepare the transaction
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.ADDRESS", address);
    utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.READ_WRITE_LENGTH", 1);

    if (mask == 0xff) {
        utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.WRITE_DATA", value);
        try {
            utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.EXECUTE_WRITE", 1);
        } catch (const xhal::memhub::memory_access_error&) {
            const auto error = gbt_ic_error(ohN, gbtN, address, gbt_ic_error::direction_t::write, gbt_ic_error::create_message_from_ic_status());
            if (!unsafe)
                throw error;
            LOG4CPLUS_WARN(logger, error.what());
        }
    } else {
        uint8_t data = 0;
        try {
            utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.EXECUTE_READ", 1);
            data = utils::readReg("BEFE.GEM.SLOW_CONTROL.IC.READ_DATA");
        } catch (const xhal::memhub::memory_access_error&) {
            const auto error = gbt_ic_error(ohN, gbtN, address, gbt_ic_error::direction_t::read, gbt_ic_error::create_message_from_ic_status());
            if (!unsafe)
                throw error;
            LOG4CPLUS_WARN(logger, error.what());
        }

        // Apply the mask
        // Note that the shift variable is starting at 1 (!)
        data = (data & ~mask);
        value = (value << (shift - 1)) & mask;
        data = data | value;

        utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.WRITE_DATA", data);
        try {
            utils::writeReg("BEFE.GEM.SLOW_CONTROL.IC.EXECUTE_WRITE", 1);
        } catch (const xhal::memhub::memory_access_error&) {
            const auto error = gbt_ic_error(ohN, gbtN, address, gbt_ic_error::direction_t::write, gbt_ic_error::create_message_from_ic_status());
            if (!unsafe)
                throw error;
            LOG4CPLUS_WARN(logger, error.what());
        }
    }
}

void gem::hardware::gbt::reset_gbt(const uint32_t ohN, uint8_t gbtMask)
{
#if defined(GEM_IS_ME0)
    for (uint8_t gbtN = 0; gbtN < GBTS_PER_OH; ++gbtN) {
        if (!((gbtMask >> gbtN) & 0x1))
            continue;

        writeGBTReg(ohN, gbtN, 0x140, 0xa3, 0xff, true /* unsafe */); // Enable PUSMForceState
        writeGBTReg(ohN, gbtN, 0x13f, 0x80, 0xff, true /* unsafe */); // Force FSM to state ARESET
    }
#endif
}

void gem::hardware::gbt::reset_vtrx_lasers(const uint32_t ohN, uint8_t gbtMask)
{
#if defined(GEM_IS_ME0)
    if ((gbtMask & 0x55) != gbtMask)
        throw std::runtime_error("Cannot reset VTRx+ lasers from slave LpGBT");

    for (uint8_t gbtN = 0; gbtN < GBTS_PER_OH; ++gbtN) {
        if (!((gbtMask >> gbtN) & 0x1))
            continue;

        writeGBTReg(ohN, gbtN, 0x053, 0x20, 0xff, true /* unsafe */); // Set GPIO #13 as output
        writeGBTReg(ohN, gbtN, 0x055, 0x00, 0xff, true /* unsafe */); // Set GPIO #13 low
        writeGBTReg(ohN, gbtN, 0x055, 0x20, 0xff, true /* unsafe */); // Set GPIO #13 high
    }
#endif
}

void gem::hardware::gbt::enable_vtrx_lasers(const uint32_t ohN, uint8_t gbtMask)
{
#if defined(GEM_IS_ME0)
    if ((gbtMask & 0x55) != gbtMask)
        throw std::runtime_error("Cannot enable VTRx+ lasers from slave LpGBT");

    for (uint8_t gbtN = 0; gbtN < GBTS_PER_OH; ++gbtN) {
        if (!((gbtMask >> gbtN) & 0x1))
            continue;

        writeGBTReg(ohN, gbtN, 0x110, 0x08, 0xff, true /* unsafe */); // Two bytes at 100 kHz
        writeGBTReg(ohN, gbtN, 0x114, 0x00, 0xff, true /* unsafe */); // I2C_WRITE_CR command
        std::this_thread::sleep_for(std::chrono::milliseconds(10));

        writeGBTReg(ohN, gbtN, 0x110, 0x00, 0xff, true /* unsafe */); // First word - VTRx+ register address
        writeGBTReg(ohN, gbtN, 0x111, 0x03, 0xff, true /* unsafe */); // Second word - Data (enable TX lasers 0 & 1)
        writeGBTReg(ohN, gbtN, 0x114, 0x08, 0xff, true /* unsafe */); // I2C_W_MULTI_4BYTE0 command
        std::this_thread::sleep_for(std::chrono::milliseconds(10));

        writeGBTReg(ohN, gbtN, 0x10f, 0x50, 0xff, true /* unsafe */); // Set the VTRx+ I²C address
        writeGBTReg(ohN, gbtN, 0x114, 0x0c, 0xff, true /* unsafe */); // I2C_WRITE_MULTI command
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
#endif
}

uint8_t gem::hardware::gbt::reset_slave_gbt(const uint32_t ohN, uint8_t gbtMask)
{
#if defined(GEM_IS_ME0)
    if ((gbtMask & 0x55) != gbtMask)
        throw std::runtime_error("Slave LpGBT external reset supported only from master LpGBT");

    for (uint8_t gbtN = 0; gbtN < GBTS_PER_OH; ++gbtN) {
        if (!((gbtMask >> gbtN) & 0x1))
            continue;

        try {
            writeGBTReg(ohN, gbtN, 0x055, 0x0, 0x2); // Set GPIO #9 low
            writeGBTReg(ohN, gbtN, 0x055, 0x1, 0x2); // Set GPIO #9 high
        } catch (const gbt_ic_error&) {
            gbtMask &= ~(1U << gbtN);
        }
    }

    return gbtMask;
#else
    return 0;
#endif
}

void gem::hardware::gbt::set_elink_rx_phase_tracking_mode(const uint32_t ohN, const uint32_t gbtN, const gem::hardware::gbt::eLinkRXPhaseTrackingMode mode)
{
#if defined(GEM_IS_ME0)
    // Configure RX phase tracking mode for all 7 eLinks groups. See lpGBT
    // documentation for more details.
    //
    // Corresponding registers = EPRX0TrackMode...EPRX6TrackMode
    //                         = EPRX0Control[1:0]...EPRX6Control[1:0]
    //                         = 0x0c8[1:0]...0x0ce[1:0]
    for (uint16_t address = 0x0c8; address <= 0x0ce; ++address)
        writeGBTReg(ohN, gbtN, address, static_cast<uint8_t>(mode), 0x3);
#endif
}

// remote functions

std::vector<std::vector<uint32_t>> gem::hardware::gbt::scanGBTPhases::operator()(const uint32_t ohN, const uint32_t number_slow, const uint32_t number_fast) const
{
    LOG4CPLUS_INFO(logger, "Scanning the phases for OH" << ohN);

    // Helper functions
    const auto repeated_read = [](const std::string& register_name, size_t number_reads) {
        for (size_t i = 0; i < number_reads; ++i) {
            try {
                utils::readReg(register_name);
            } catch (const xhal::memhub::memory_access_error&) {
                return true;
            }
        };
        return false;
    };

    const auto repeated_write = [](const std::string& register_name, const uint32_t value, size_t number_writes) {
        for (size_t i = 0; i < number_writes; ++i) {
            try {
                utils::writeReg(register_name, value);
            } catch (const xhal::memhub::memory_access_error&) {
                return true;
            }
        };
        return false;
    };

    // Return object
    std::vector<std::vector<uint32_t>> results;
    results.reserve(oh::VFATS_PER_OH);
    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN)
        results.push_back(std::vector<uint32_t>(PHASE_MAX - PHASE_MIN + 1));

    // Enable all VFATs
    vfat3::setVFATMask {}(ohN, oh::FULL_VFAT_MASK);

    // Perform the scan
    for (size_t phase = PHASE_MIN; phase <= PHASE_MAX; ++phase) {
        // Set RX phase tracking mode to "Fixed"
        for (size_t gbtN = 0; gbtN < gbt::GBTS_PER_OH; ++gbtN) {
            try {
                set_elink_rx_phase_tracking_mode(ohN, gbtN, eLinkRXPhaseTrackingMode::FIXED);
            } catch (const gbt_ic_error&) {
                for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN)
                    if (VFAT_TO_GBT.at(vfatN) == gbtN)
                        results[vfatN][phase] |= PHASE_SCAN_SET_PHASE_MODE_ERROR;
            }
        }

        // Set the new phases
        for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
            try {
                writeGBTPhase {}(ohN, VFAT_TO_GBT.at(vfatN), VFAT_TO_ELINK.at(vfatN), phase);
            } catch (const gbt_ic_error&) {
                results[vfatN][phase] |= PHASE_SCAN_SET_PHASE_ERROR;
            }
        }

        // Reset the counters
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        utils::writeReg("BEFE.GEM.GEM_SYSTEM.CTRL.LINK_RESET", 1);
        std::this_thread::sleep_for(std::chrono::milliseconds(10));

        // Start with slow-control transactions
        for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
            const std::string vfat_base = "BEFE.GEM.OH.OH" + std::to_string(ohN) + ".GEB.VFAT" + std::to_string(vfatN) + ".";
            // Read-only registers
            results[vfatN][phase] |= repeated_read(vfat_base + "HW_ID", number_slow) ? PHASE_SCAN_SC_READ_ERROR : 0;
            results[vfatN][phase] |= repeated_read(vfat_base + "HW_ID_VER", number_slow) ? PHASE_SCAN_SC_READ_ERROR : 0;
            results[vfatN][phase] |= repeated_read(vfat_base + "HW_CHIP_ID", number_slow) ? PHASE_SCAN_SC_READ_ERROR : 0;

            // Read-write register
            results[vfatN][phase] |= repeated_write(vfat_base + "TEST_REG", 0, number_slow) ? PHASE_SCAN_SC_WRITE_ERROR : 0;
            results[vfatN][phase] |= repeated_read(vfat_base + "TEST_REG", number_slow) ? PHASE_SCAN_SC_READ_ERROR : 0;

            // Attempt to set the VFAT into run mode for the DAQ transactions test
            // Failing to do so will result in a systematically bad phase
            results[vfatN][phase] |= repeated_write(vfat_base + "CFG_RUN", 1, number_slow) ? PHASE_SCAN_SC_RUN_MODE_ERROR : 0;
        }

        // Send L1A to receive DAQ packets
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.ENABLE", 1);
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_L1A_COUNT", number_fast);
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_L1A_GAP", 40);
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_START", 1);
        while (utils::readReg("BEFE.GEM.TTC.GENERATOR.CYCLIC_RUNNING")) {
        }
        utils::writeReg("BEFE.GEM.TTC.GENERATOR.ENABLE", 0);

        // Look at the error counters
        for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
            const std::string vfat_link_base = ("BEFE.GEM.OH_LINKS.OH" + std::to_string(ohN) + ".VFAT" + std::to_string(vfatN) + ".");
            results[vfatN][phase] |= utils::readReg(vfat_link_base + "DAQ_CRC_ERROR_CNT") ? PHASE_SCAN_DAQ_CRC_ERROR : 0;
            results[vfatN][phase] |= utils::readReg(vfat_link_base + "SYNC_ERR_CNT") ? PHASE_SCAN_SYNC_ERROR : 0;
        }
    }

    return results;
}

void gem::hardware::gbt::writeGBTPhase::operator()(const uint32_t ohN, const uint32_t gbtN, const uint16_t eLinkN, const uint8_t phase) const
{
    LOG4CPLUS_INFO(logger, fmt::format(FMT_STRING("Writing phase {} to eLink #{} of GBT #{} on OH #{}"), phase, eLinkN, gbtN, ohN));

    // ohN check
    std::stringstream errmsg;
    if (ohN >= amc::number_oh()) {
        errmsg << "The ohN parameter supplied (" << ohN << ") exceeds the number of OH's supported by the CTP7 (" << amc::number_oh() << ").";
        throw std::range_error(errmsg.str());
    }

    if (eLinkN > ELINK_MAX)
        throw std::range_error(fmt::format(FMT_STRING("eLink ({}) above supported range ([0, {}])"), eLinkN, ELINK_MAX));

    // phase check
    checkPhase(phase);

#if !defined(GEM_IS_ME0)
    // Derive the register address and bitmask based on the eLink number
    // More details can be found in the GBTx user manual
    const uint16_t eGroup = eLinkN / 8;
    const uint16_t eChannel = eLinkN % 8;

    uint16_t baseAddr = 66 + 24 * eGroup;
    if (eLinkN == ELINK_MAX) // EC eLink
        baseAddr--;
    else
        baseAddr += (7 - eChannel) / 2;

    uint8_t bitmask = (eChannel % 2) ? 0x0f : 0xf0;

    // Write the 3 copies of the triplicated register
    for (size_t regN = 0; regN < 3; ++regN) {
        const uint16_t regAddr = baseAddr + 4 * regN;
        writeGBTReg(ohN, gbtN, regAddr, phase, bitmask);
    }
#else
    const uint16_t regAddr = 208 + eLinkN;
    writeGBTReg(ohN, gbtN, regAddr, phase, 0xf0);
#endif
}

uint8_t gem::hardware::gbt::configure::operator()(const uint32_t ohN, uint8_t gbtMask) const
{
    LOG4CPLUS_INFO(logger, fmt::format(FMT_STRING("Writing GBT configuration for {:#x} on OH{:d}"), gbtMask, ohN));

    // Start from the default GBT configuration
    const auto gbtConfigurations = getDefaultGBTConfigurations();

    for (uint8_t gbtN = 0; gbtN < GBTS_PER_OH; ++gbtN) {
        if (!((gbtMask >> gbtN) & 0x1))
            continue;

        try {
#if defined(GEM_IS_GE11)
            // Write all registers
            for (size_t address = 0; address < gbtConfigurations.at(gbtN).size(); ++address) {
                // This is a very special GE1/1 case...
                //
                // Even though the GBTx 1&2 do not need to communicate with an
                // SCA, the register 248 (i.e. paEnableEC/Enable EC has been
                // set to 0x7 (i.e. enabled). It appears that disabling the EC
                // port, when previously enabled, can corrupted the current IC
                // transaction or the next ones. We implement two mitigation
                // measures:
                //
                // 1. Introduce a pause after programming register 248 in order
                //    to force any error to flush.
                // 2. Introduce a re-try on register 248. While the first
                //    transaction can be reported as failed, it does succeed.
                //    The second attempt is present to ensure the register
                //    value.
                if (gbtN > 0 && address == 248) {
                    try {
                        writeGBTReg(ohN, gbtN, address, gbtConfigurations[gbtN][address]);
                        std::this_thread::sleep_for(std::chrono::milliseconds(1));
                    } catch (const gbt_ic_error&) {
                        writeGBTReg(ohN, gbtN, address, gbtConfigurations[gbtN][address]);
                    }
                    continue;
                }

                writeGBTReg(ohN, gbtN, address, gbtConfigurations[gbtN][address]);
            }
#elif defined(GEM_IS_ME0)
            // Skip the chip ID, user ID, and calibration constant registers
            for (size_t address = 0x20; address < gbtConfigurations.at(gbtN).size(); ++address) {
                // Similarly to the GE1/1 special case, here is one for ME0...
                //
                // As described by the LpGBT team, the EQConfig [0x037] register
                // is a sensitive register in the sense that it can affect the
                // downlink stability. It was additionally shown that, upon changes,
                // the chip clock phase can suddenly change. That clock phase jump
                // may or may not upset the FPGA CDR (it happens when the FPGA CDR
                // bandwidth is lower than the LpGBT). The following mitigation is
                // implemented:
                //
                // * When the EQConfig register is written on a master LpGBT, allow
                //   a first possible failure whose root cause is known. In such a
                //   situation, wait for the link to relock, and re-attempt the write
                //   to make sure everything is in order.
                if (gbtN % 2 == 0 && address == 0x037) {
                    try {
                        writeGBTReg(ohN, gbtN, address, gbtConfigurations[gbtN][address]);
                    } catch (const gbt_ic_error&) {
                        std::this_thread::sleep_for(std::chrono::milliseconds(1));
                        writeGBTReg(ohN, gbtN, address, gbtConfigurations[gbtN][address]);
                    }
                    continue;
                }

                writeGBTReg(ohN, gbtN, address, gbtConfigurations[gbtN][address]);
            }

            set_elink_rx_phase_tracking_mode(ohN, gbtN, eLinkRXPhaseTrackingMode::CONTINOUS_INITIAL);
            lpgbt::enableADC(ohN, gbtN);
#else
            // Write all all registers
            for (size_t address = 0; address < gbtConfigurations.at(gbtN).size(); ++address)
                writeGBTReg(ohN, gbtN, address, gbtConfigurations[gbtN][address]);
#endif
        } catch (const gbt_ic_error& e) {
            LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Failed to configure GBT #{:d} on OH #{:d} - masking GBT (reason: {:s})"), gbtN, ohN, e.what()));
            gbtMask &= ~(1U << gbtN);
        }
    }

    // TODO: Generalize to per-GBT configuration files
    // Write the system specific GBT phases
    const std::string config_filename = fmt::format(FMT_STRING("gbt/config-oh{}-gbt.cfg"), ohN);
    auto data = utils::read_configuration_file(config_filename);

    for (size_t vfatN = 0; vfatN < oh::VFATS_PER_OH; ++vfatN) {
        const std::string key = fmt::format(FMT_STRING("PHASE_VFAT{}"), vfatN);

        const auto it = data.find(key);
        if (it != data.end()) {
            // Sanitize the phase
            // Bits above bit 3 allow to store additional information for the user
            const auto phase = it->second & 0xf;

            const uint32_t gbtN = VFAT_TO_GBT.at(vfatN);
            if (!((gbtMask >> gbtN) & 0x1)) {
                data.erase(it);
                continue;
            }

            try {
                writeGBTPhase {}(ohN, gbtN, VFAT_TO_ELINK.at(vfatN), phase);
            } catch (const gbt_ic_error&) {
                LOG4CPLUS_WARN(logger, fmt::format(FMT_STRING("Failed to configure register on GBT #{:d} on OH #{:d} - masking GBT"), gbtN, ohN));
                gbtMask &= ~(1U << gbtN);
            }

            // Remove the key from further processing after the value is applied
            data.erase(it);
        }
    }

    // We have unused keys, that's an error
    if (data.size() != 0) {
        std::string errmsg = "Invalid keys in GBT configuration file: ";
        for (const auto& key_value : data)
            errmsg += key_value.first + " ";
        throw std::runtime_error(errmsg);
    }

    return gbtMask;
}

uint32_t gem::hardware::gbt::getGBTStatus::operator()(const bool history) const
{
    uint32_t optohybridMask = 0;

    for (size_t ohN = 0; ohN < amc::number_oh(); ++ohN) {
        bool optohybrid_status = true;

        for (size_t gbtN = 0; gbtN < gbt::GBTS_PER_OH; gbtN++) {
            const std::string base_name = fmt::format(FMT_STRING("BEFE.GEM.OH_LINKS.OH{:d}.GBT{:d}_"), ohN, gbtN);
            optohybrid_status &= utils::readReg(base_name + "READY");

            if (history) {
                optohybrid_status &= !utils::readReg(base_name + "WAS_NOT_READY");
                optohybrid_status &= !utils::readReg(base_name + "RX_HAD_OVERFLOW");
                optohybrid_status &= !utils::readReg(base_name + "RX_HAD_UNDERFLOW");
            }
        }

        optohybridMask |= (optohybrid_status << ohN);
    }

    return optohybridMask;
}

uint8_t gem::hardware::gbt::getLockedGBT::operator()(const uint32_t ohN, const bool history) const
{
    uint8_t gbt_mask = 0;

    for (size_t i = 0; i < gbt::GBTS_PER_OH; i++) {
        const std::string base_name = fmt::format(FMT_STRING("BEFE.GEM.OH_LINKS.OH{:d}.GBT{:d}_"), ohN, i);

        bool gbt_status = utils::readReg(base_name + "READY");

        if (history) {
            gbt_status &= !utils::readReg(base_name + "WAS_NOT_READY");
            gbt_status &= !utils::readReg(base_name + "RX_HAD_OVERFLOW");
            gbt_status &= !utils::readReg(base_name + "RX_HAD_UNDERFLOW");
        }

        gbt_mask |= (gbt_status << i);
    }

    return gbt_mask;
}

std::vector<std::optional<uint32_t>> gem::hardware::gbt::readGBTSerialNumber::operator()(const uint32_t ohN) const
{
    std::vector<std::optional<uint32_t>> serials;

    const auto lockedGBT = gbt::getLockedGBT {}(ohN, false /* instantaneous only */);

    for (size_t gbtN = 0; gbtN < gbt::GBTS_PER_OH; gbtN++) {
        if (!((lockedGBT >> gbtN) & 0x1)) {
            serials.push_back(std::nullopt);
            continue;
        }

        uint32_t value = 0;
        try {
#if !defined(GEM_IS_ME0)
            // Multiple (2) 8-bits registers
            // From MSB to LSB
            //   368..367 -- TestFuse2...TestFuse1
            for (size_t i = 0; i < 2; ++i)
                value |= gbt::readGBTReg(ohN, gbtN, 367 + i) << (8 * i);
#else
            // The LpGBT chip ID is stored in the e-fuses and not copied to
            // registers when the boot mode is set to ROM. Read the value
            // directly from the e-fuses.
            //
            // In order to mitigate the e-fuses reliability issue, not only 1,
            // but 5 bit-shifted copies are stored. Take the majority of those
            // 5 copies, bit-wise.
            //
            // For further protection, the chip ID is actually encoded as sa
            // SECDED Hamming code. It allows to detect and correct any single
            // bit error, and detect any double bits error. TODO Decoding
            // remains to be implemented.
            const uint32_t a = utils::rotr32(*lpgbt::readFuseBank(ohN, gbtN, 0x00), 0);
            // Note: register bank 0x04 is reserved for the (unused) User ID
            const uint32_t b = utils::rotr32(*lpgbt::readFuseBank(ohN, gbtN, 0x08), 6);
            const uint32_t c = utils::rotr32(*lpgbt::readFuseBank(ohN, gbtN, 0x0c), 12);
            const uint32_t d = utils::rotr32(*lpgbt::readFuseBank(ohN, gbtN, 0x10), 18);
            const uint32_t e = utils::rotr32(*lpgbt::readFuseBank(ohN, gbtN, 0x14), 24);

            value = utils::majority5(a, b, c, d, e);
#endif
        } catch (const gbt::gbt_ic_error&) {
            serials.push_back(std::nullopt);
            continue;
        }

        serials.push_back(value);
    }

    return serials;
}
