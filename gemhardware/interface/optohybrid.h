/// @file
/// @brief RPC module for @c OptoHybrid methods

#ifndef GEM_HARDWARE_OPTOHYBRID_H
#define GEM_HARDWARE_OPTOHYBRID_H

#include <xhal/common/common.h>

#include <map>
#include <optional>
#include <string>
#include <utility>
#include <vector>

namespace gem {
namespace hardware {
    namespace oh {

        /// @brief Configures the OH FPGA based on back-end configuration files
        ///
        /// The configurations file are stored under <configuration-path>/oh/config-oh{x}.cfg and
        /// must contain key-value pairs with @c key being the FPGA register name and @c value
        /// being the register value to apply.
        ///
        /// @param @c ohN OptoHybrid number
        ///
        /// @returns Whether or not the FPGA configuration was successful
        struct configure_fpga : public xhal::common::Method {
            bool operator()(uint32_t ohN) const;
        };

        /// @brief Return the OptoHybrid mask for which the FPGA is programmed
        ///
        /// @details More technically, this command reads the "DONE" output of the FPGA.
        ///          If the value cannot be retrieved, the FPGA is considered as not programmed.
        ///
        /// @param @c optohybridMask Set of links for which to check the FPGA programming status
        ///
        /// @return An @c std::pair whose first component is the programmed FPGA
        ///         mask and whose second component is the validity mask of the
        ///         readout.
        struct getProgrammedFPGA : public xhal::common::Method {
            std::pair<uint32_t, uint32_t> operator()(const uint32_t optohybridMask) const;
        };

        /// @brief Reset the FPGA on the requested OptoHybrid via the SCA
        ///
        /// @param @c optohybridMask Set of links for which to reset the FPGA
        ///
        /// @return A bitmask containing the OptoHybrid that have been
        ///     successfully reset
        struct resetFPGA : public xhal::common::Method {
            uint32_t operator()(const uint32_t optohybridMask) const;
        };

        /// @brief Return the OptoHybrid FPGA DNA
        ///
        /// @param @c ohMask The OptoHybrid mask
        ///
        /// @return A vector of FPGA DNA, if successfully retrieved
        struct getOHfpgaDNA : public xhal::common::Method {
            std::vector<std::optional<uint64_t>> operator()(uint32_t ohMask) const;
        };

    } // namespace gem::hardware::oh
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_OPTOHYBRID_H
