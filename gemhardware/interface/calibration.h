/// @file
/// @brief Calibration routines

#ifndef GEM_HARDWARE_CALIBRATION_H
#define GEM_HARDWARE_CALIBRATION_H

#include <xhal/common/common.h>

#include <map>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

namespace gem {
namespace hardware {
    namespace calibration {

        /// @brief Struct with info relative to a given DAC register name
        struct dacInfo {
            uint8_t adcSelect; ///< Index of the monitoring ADC
            uint8_t min; ///< DAC minimal value
            uint8_t max; ///< DAC maximal value
        };

        /// @brief Maps the VFAT3 DACs and their ranges
        ///
        /// @detail key is the register name (dacName) value
        ///         value is a dacInfo (adcSelect, dacMin, dacMax)
        const std::unordered_map<std::string, dacInfo> vfat3DACSelectAndSize {
            { "CFG_CAL_DAC", dacInfo { 1, 0, 0xff } },
            { "CFG_BIAS_PRE_I_BIT", dacInfo { 2, 0, 0xff } },
            { "CFG_BIAS_PRE_I_BLCC", dacInfo { 3, 0, 0x3f } },
            { "CFG_BIAS_PRE_I_BSF", dacInfo { 4, 0, 0x3f } },
            { "CFG_BIAS_SH_I_BFCAS", dacInfo { 5, 0, 0xff } },
            { "CFG_BIAS_SH_I_BDIFF", dacInfo { 6, 0, 0xff } },
            { "CFG_BIAS_SD_I_BDIFF", dacInfo { 7, 0, 0xff } },
            { "CFG_BIAS_SD_I_BFCAS", dacInfo { 8, 0, 0xff } },
            { "CFG_BIAS_SD_I_BSF", dacInfo { 9, 0, 0x3f } },
            { "CFG_BIAS_CFD_DAC_1", dacInfo { 10, 0, 0x3f } },
            { "CFG_BIAS_CFD_DAC_2", dacInfo { 11, 0, 0x3f } },
            { "CFG_HYST", dacInfo { 12, 0, 0x3f } },
            //{"NOREG_CFGIREFLOCAL",dacInfo{13, 0, 0}}, //CFD Ireflocal
            { "CFG_THR_ARM_DAC", dacInfo { 14, 0, 0xff } }, // ADC measures current
            // { "CFG_THR_ARM_DAC",dacInfo{35, 0, 0xff} },//ADC measures voltage
            { "CFG_THR_ZCC_DAC", dacInfo { 15, 0, 0xff } }, // ADC measures current
            //{ "CFG_THR_ZCC_DAC",dacInfo{36, 0, 0xff} }, //ADC measures voltage
            // { "CFG_CAL_DAC",dacInfo{33, 0, 0xff} },//ADC measures voltage
            { "CFG_BIAS_PRE_VREF", dacInfo { 34, 0, 0xff } },
            { "CFG_VREF_ADC", dacInfo { 39, 0, 0x3 } }, // Taken from the DB
        };

        /// @brief Toggles the TTC Generator
        ///
        /// enable = true (false) turn on CTP7 internal TTC generator and ignore ttc commands from backplane for this AMC (turn off CTP7 internal TTC generator and take ttc commands from backplane link)
        ///
        /// @param @c enable See detailed mehod description
        struct ttcGenToggle : xhal::common::Method {
            void operator()(const bool enable) const;
        };

        /// @brief Configures TTC generator
        ///
        /// * pulseDelay (only for enable = true), delay between CalPulse and L1A
        /// * l1aInterval (only for enable = true), how often to repeat signals
        /// * enable = true (false) turn on CTP7 internal TTC generator and ignore ttc commands from backplane for this AMC (turn off CTP7 internal TTC generator and take ttc commands from backplane link)
        ///
        /// @param @c pulseDelay Delay between CalPulse and L1A
        /// @param @c l1aInterval How often to repeat signals (only for enable = true)
        /// @param @c enable If true (false) ignore (take) ttc commands from backplane for this AMC (affects all links)
        struct ttcGenConf : xhal::common::Method {
            void operator()(const uint32_t pulseDelay, const uint32_t l1aInterval,
                const bool enable) const;
        };

        /// @brief Structure for the rates measured for acertain delay settings during a cluster mask scan
        ///
        /// The first argument is the delay setting, the second is the value of the mask width, then the cluster rate with disabled and enabled masking, then the registered trigger cluster count, the L1A period used for generating L1A during the scan: 0 means disabled L1A, 400 means 100kHz.
        struct clusterMaskRate {
            int8_t delay;
            uint32_t cluster_rate_unmasked;
            uint32_t cluster_rate_masked;
            uint32_t trigger_rate;
            uint32_t l1a_period;

            /// @brief @c cereal serialization
            template <class Archive>
            void serialize(Archive& archive)
            {
                archive(delay, cluster_rate_unmasked, cluster_rate_masked, trigger_rate, l1a_period);
            }
        };

        /// @brief Structure for the result of a cluster mask scan result
        ///
        /// The first argument is expected to be the OH number, the second is the value of the cluserMaskRate for the scanned delay point.
        using clusterMaskScanResult = std::map<uint32_t /* ohN */, std::vector<clusterMaskRate>>;

        /// @brief Parallel trigger cluster mask rate scan.
        ///
        /// @detail Measures the cluster rate seen by OHv3 ohN.
        //          A map is returned where each key is the ohN and the value is a  vector with cluster mask rate results
        ///         Will scan from Min to Max.
        ///         First point -2 measures the rate not having the L1A started yet.
        ///         Second point -1 measures the rate with trigger enabled at the desired rate (e.g. 100KHz)
        ///         Then the delay value is scanned with the desired width for the masking
        ///         Each measured point will last the time inteval
        ///
        /// @param @c ohMask OptoHybrid mask
        /// @param @c Min Minimal value of scan variable
        /// @param @c Max Maximal value of scan variable
        /// @param @c waitTime Amount of time to collect data, applies to the oh cluster overall rate,
        /// @param @c l1aInterval period in BX between 2 desired L1A (400 means 100KHz)
        /// @returns an @c std::map of scan value key to vector of cluste mask rates; 1 for each OH;
        struct clusterMaskRateScanParallel : xhal::common::Method {
            clusterMaskScanResult operator()(
                uint32_t ohMask,
                uint8_t min,
                uint8_t max,
                uint32_t waitTime = 1,
                uint32_t l1aInterval = 400) const;
        };

        /// @brief Structure for the result of a S-bit rate scan
        ///
        /// The first argument is expected to be the OH number, the second is the VFAT number, the third is the value of the DAC registered, and the forth is the registered S-bit count.
        using sbitRateScanResult = std::map<uint32_t /* ohN */, std::map<uint32_t /* vfatN */, std::map<uint32_t, uint32_t>>>;

        /// @brief Parallel s-bit rate scan.
        ///
        /// @detail Measures the s-bit rate seen by OHv3 ohN for the non-masked VFATs defined in
        ///         vfatMask as a function of scanReg.
        ///         Will scan from dacMin to dacMax in steps of dacStep.
        ///         The x-values (e.g. scanReg values) will be keys in the output dictionary
        ///         For each VFAT the y-values (e.g. rate) will be stored in a vector
        ///         For the overall y-value (e.g. rate) will be stored in the output vector, as the final entry
        ///         Each measured point will take one second
        ///         The measurement is performed for all channels (ch=128) or a specific channel (0 <= ch <= 127)
        ///
        /// @param @c ohMask OptoHybrid mask
        /// @param @c ch Channel of interest
        /// @param @c dacMin Minimal value of scan variable
        /// @param @c dacMax Maximal value of scan variable
        /// @param @c dacStep Scan variable change step
        /// @param @c scanReg DAC register to scan over name
        /// @param @c waitTime Amount of time to collect data, applies to the per-VFAT rates not the overall rate,
        ///        maximum is just more than 107 seconds
        /// @param @c toggleRunMode Boolean to toggle or not the VFAT run mode between consecutive scans
        ///
        /// @returns an @c std::map of DAC value key to vector of rates; 1 for each VFAT
        struct sbitRateScanParallel : xhal::common::Method {
            sbitRateScanResult operator()(
                uint32_t ohMask,
                uint8_t ch,
                uint16_t dacMin,
                uint16_t dacMax,
                uint16_t dacStep,
                const std::string& scanReg,
                uint32_t waitTime = 1,
                bool toggleRunMode = true) const;
        };

        /// @brief Checks that the s-bit mapping is correct using the calibration pulse of the VFAT
        ///
        /// @details With all but one channel masked
        ///          1) pulse a given channel
        ///          2) then check which s-bits are seen by the CTP7
        ///          3) repeats for all channels on vfatN
        ///          reports the (VFAT,chan) pulsed and (VFAT,s-bit) observed where s-bit=chan*2; additionally reports if the cluster was valid.
        ///
        /// @details The s-bit Monitor stores the 8 s-bits that are sent from the OH (they are all sent at the same time and correspond to the same clock cycle).
        /// Each s-bit clusters readout from the s-bit Monitor is a 16 bit word with bits [0:10] being the s-bit address and bits [12:14] being the s-bit size, bits 11 and 15 are not used.
        ///          The possible values of the s-bit Address are [0,1535].
        ///          Clusters with address less than 1536 are considered valid (e.g. there was an s-bit); otherwise an invalid (no s-bit) cluster is returned.
        ///          The s-bit address maps to a given trigger pad following the equation @f$s-bit = addr % 64@f$.
        ///          There are 64 such trigger pads per VFAT.
        ///          Each trigger pad corresponds to two VFAT channels.
        ///          The s-bit to channel mapping follows @f$s-bit=floor(chan/2)@f$.
        ///          You can determine the VFAT position of the s-bit via the equation @f$vfatPos=7-int(addr/192)+int((addr%192)/64)*8@f$.
        ///          The s-bit size represents the number of adjacent trigger pads are part of this cluster.
        ///          The s-bit address always reports the lowest trigger pad number in the cluster.
        ///          The s-bit size takes values [0,7].
        ///          So an s-bit cluster with address 13 and with size of 2 includes 3 trigger pads for a total of 6 VFAT channels and starts at channel @f$13*2=26@f$ and continues to channel @f$(2*15)+1=31@f$.
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c vfatN specific VFAT position to be tested
        /// @param @c vfatMask VFATs to be included in the trigger
        /// @param @c useCalPulse true (false) checks s-bit mapping with calpulse on (off); useful for measuring noise
        /// @param @c currentPulse Selects whether to use current or volage pulse
        /// @param @c calScaleFactor
        /// @param @c nevts the number of cal pulses to inject per channel
        /// @param @c l1aInterval How often to repeat signals (only for enable = true)
        /// @param @c pulseDelay delay between CalPulse and L1A
        ///
        /// @returns an @c std::vector containing the results of the scan with the following format
        ///          bits [0,7] channel pulsed
        ///          bits [8:15] s-bit observed
        ///          bits [16:20] VFAT pulsed
        ///          bits [21,25] VFAT observed
        ///          bit 26 isValid
        ///          bits [27,29] cluster size
        ///
        struct checkSbitMappingWithCalPulse : xhal::common::Method {
            std::vector<uint32_t> operator()(const uint16_t& ohN,
                const uint32_t& vfatN,
                const uint32_t& vfatMask,
                const bool& useCalPulse,
                const bool& currentPulse,
                const uint32_t& calScaleFactor,
                const uint32_t& nevts,
                const uint32_t& l1aInterval,
                const uint32_t& pulseDelay) const;
        };

        /// @brief With all but one channel masked, pulses a given channel, and then checks the rate of s-bits seen by the OH FPGA and CTP7, repeats for all channels; reports the rate observed
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c vfatN specific VFAT position to be tested
        /// @param @c vfatMask VFATs to be included in the trigger
        /// @param @c useCalPulse true (false) checks s-bit mapping with calpulse on (off); useful for measuring noise
        /// @param @c currentPulse Selects whether to use current or volage pulse
        /// @param @c calScaleFactor
        /// @param @c waitTime Measurement duration per point in milliseconds
        /// @param @c pulseRate rate of calpulses to be sent in Hz
        /// @param @c pulseDelay delay between CalPulse and L1A
        ///
        /// @returns an @c std::map of @c std::string to @c std::vector with one entry for each channel, where the keys are:
        ///          "CTP7" stores the value of BEFE.GEM.TRIGGER.OHX.TRIGGER_RATE for X = ohN
        ///          "FPGA" stores the value of BEFE.GEM.OH.OHX.FPGA.TRIG.CNT.CLUSTER_COUNT N.B. not converted to rate!
        ///          "VFAT" stores the value of BEFE.GEM.OH.OHX.FPGA.TRIG.CNT.VFATY_SBITS for X = ohN and Y the vfat number
        ///          N.B. FPGA and VFAT values are *not* converted to a rate, this can be done by *waitTime/1000.;
        ///
        struct checkSbitRateWithCalPulse : xhal::common::Method {
            std::map<std::string, std::vector<uint32_t>> operator()(const uint16_t& ohN,
                const uint32_t& vfatN,
                const uint32_t& vfatMask,
                const bool& useCalPulse,
                const bool& currentPulse,
                const uint32_t& calScaleFactor,
                const uint32_t& waitTime,
                const uint32_t& pulseRate,
                const uint32_t& pulseDelay) const;
        };

        /// @brief Structure for the result of a DAC scan
        ///
        /// The first argument is expected to be the VFAT number, the second is the value of the DAC register scanned, and the third is the pair of the ADC mean value and its standard deviation.
        using dacScanResult = std::map<uint8_t /* vfatN */, std::map<uint8_t, std::pair<float, float>>>;

        /// @brief configures the VFAT3 DAC Monitoring and then scans the DAC and records the measured ADC values for all unmasked VFATs
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c vfatMask VFAT mask
        /// @param @c dacName register name linked to Monitor Sel for ADC monitoring in VFAT3, see documentation for GBL_CFG_CTR_4 in VFAT3 manual for more details
        /// @param @c dacStep step size to scan the dac in
        /// @param @c useExtRefADC if (true) false use the (externally) internally referenced ADC on the VFAT3 for monitoring
        ///
        /// @returns a @c std::map<uint8_t, std::map<uint8_t, std::pair<float, float >>> dacScanResult object
        struct dacScan : xhal::common::Method {
            dacScanResult operator()(uint16_t ohN,
                uint32_t vfatMask,
                const std::string& dacName,
                uint16_t dacStep = 1,
                bool useExtRefADC = false) const;
        };

        /// @brief Take S-curve scans for all unmasked VFATs
        ///
        /// @param @c ohMask OptoHybrid mask
        /// @param @c dacMin Minimal value of scan variable
        /// @param @c dacMax Maximal value of scan variable
        /// @param @c dacStep Step size to scan the dac in
        /// @param @c nEvts The number of events per point
        /// @param @c l1aInterval The interval between two subsequent L1As
        /// @param @c pulseDealy The delay of the L1A with respect to the calibration pulse
        /// @param @c latency Latency setting
        /// @param @c pulseStretch the Pulse stretch
        /// @param @c thresholdType If false the THR_ARM_DAC used is from the configuration files, otherwise it is set with the threshold parameter
        /// @param @c threshold Threshold value used if thresholdType is true
        /// @param @c trimmingSourceType If false the trimming parameters are taken from the configuration files, otherwise they are taken from trimming argument
        /// @param @c trimming Trimming value used if trimmingSourceType is true
        struct scurveScan : xhal::common::Method {
            void operator()(uint32_t ohMask,
                uint16_t dacMin,
                uint16_t dacMax,
                uint16_t dacStep,
                uint32_t nEvts,
                uint32_t l1aInterval,
                uint8_t pulseDelay,
                uint16_t latency,
                uint8_t pulseStretch,
                bool thresholdType,
                uint8_t threshold,
                bool trimmingSourceType,
                int16_t trimming) const;
        };

        /// @brief Structure holding the calPulse scan results
        ///
        /// The map index is the VFAT number whereas the map element is a vector containing
        /// the number of hits registered for a given channel.
        using calPulseScanResult = std::map<uint8_t /* vfatN */, std::vector<uint16_t>>;

        /// @brief Scan all channels with calibration pulses and returns the number of hits
        ///
        /// The current implementation is such that only VFAT for which all channels
        /// were scanned properly are reported.
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c nEvts The number of trigger/events/calibration pulses to send per channel
        /// @param @c thresholdType If false the THR_ARM_DAC used is taken from the configuration files,
        ///             otherwise it is set taken from the @c threshold parameter
        /// @param @c threshold Threshold value used if thresholdType is @c true
        /// @param @c l1aInterval The trigger (L1A) period
        /// @param @c pulseDealy The delay of the L1A with respect to the calibration pulse
        /// @param @c latency Latency to readout the VFAT DAQ data
        /// @param @c pulseStretch Pulse stretch to apply to the VFAT hits
        ///
        /// @return A @c calPulseScan structure containing the number of hits
        struct calPulseScan : xhal::common::Method {
            calPulseScanResult operator()(uint32_t ohN,
                uint32_t nEvts,
                uint16_t calDac,
                bool thresholdType,
                uint8_t threshold,
                uint32_t l1aInterval,
                uint8_t pulseDelay,
                uint16_t latency,
                uint8_t pulseStretch) const;
        };

        /// @brief Take threshold ARM DAC scans for all unmasked VFATs
        ///
        /// @param @c ohMask OptoHybrid mask
        /// @param @c dacMin Minimal value of scan variable
        /// @param @c dacMax Maximal value of scan variable
        /// @param @c dacStep Step size to scan the dac in
        /// @param @c nEvts The number of events per point
        /// @param @c l1aInterval The interval between two subsequent L1As
        struct thresholdScan : xhal::common::Method {
            void operator()(uint32_t ohMask,
                uint16_t dacMin,
                uint16_t dacMax,
                uint16_t dacStep,
                uint32_t nEvts,
                uint32_t l1aInterval) const;
        };

        /// @brief Structure holding the EOM LpGBT scan results
        ///
        /// The map index is the GBT number whereas the map element is an array
        /// containing pairs of the number of transitions registered and
        /// acquisition period (in BX) for a given X-Y point.
        using eyeOpeningMonitorScanResult = std::map<uint32_t /* gbtN */, std::array<std::array<std::pair<uint64_t, uint64_t>, 31>, 64>>;

        /// @brief Perform an EOM scan on all locked LpGBT on @c ohN
        ///
        /// @param @c samplesBX Acquisition period per point in bunch crossings
        /// @param @c eqAttnGain LpGBT receiver attenuation gain
        ///
        /// See @c gem::hardware::lpgbt::eyeOpeningMonitorScan for more details.
        struct eyeOpeningMonitorScan : xhal::common::Method {
            eyeOpeningMonitorScanResult operator()(uint32_t ohN, uint32_t samplesBX, uint8_t eqAttnGain) const;
        };

    } // namespace gem::hardware::calibration
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_CALIBRATION_H
