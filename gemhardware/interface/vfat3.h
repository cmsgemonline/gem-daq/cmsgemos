/// @file
/// @brief RPC module for VFAT3 methods

#ifndef GEM_HARDWARE_VFAT3_H
#define GEM_HARDWARE_VFAT3_H

#include <xhal/common/common.h>

#include <map>
#include <optional>
#include <stdint.h>
#include <string>
#include <vector>

namespace gem {
namespace hardware {
    namespace vfat3 {

        ////@brief Symbolic variable to select all channels in a vfat
        static constexpr uint8_t VFAT_ALL_CHANNELS = 128;

        /// @brief VFAT calibration mode
        ///
        /// Modes in which the VFAT calibration circuit can be configured
        enum struct VFATCalibrationMode : uint8_t {
            DISABLED = 0x0, ///< Disable calibration mode
            VOLTAGE = 0x1, ///< Calibration circuit operates in voltage pulse mode
            CURRENT = 0x2, ///< Calibration circuit operates in current source mode
        };

        /// @brief Configuration of the VFAT calibration circuit
        ///
        /// The default configuration disables the calibration circuit.
        /// The pulse parameters default to the largest possible pulse in current mode.
        struct calPulseConfig {
            VFATCalibrationMode mode = VFATCalibrationMode::DISABLED; ///< Calibration mode

            uint8_t cal_dac = 255; ///< Value of the calibration DAC value (for voltage pulses, high DAC means small pulse)
            uint16_t duration = 511; ///< Pulse duration
            uint8_t scale_factor = 3; ///< Scale factor (only for current pulses; 00 = 25%, 01 = 50%, 10 = 75%, 11 = 100%)

            /// @brief @c cereal serialization
            template <class Archive>
            void serialize(Archive& archive)
            {
                archive(mode, cal_dac, duration, scale_factor);
            }
        };

        /// @brief Returns the VFAT considered synchronized on a given OptoHybrid
        ///
        /// @param @c ohN OptoHybrid optical link number
        ///
        /// @returns Bitmask of sync'ed VFATs
        struct getSyncedVFAT : public xhal::common::Method {
            uint32_t operator()(uint32_t ohN) const;
        };

        /// @brief Returns the VFAT mask as configured on a given OptoHybrid
        ///
        /// @param @c ohN OptoHybrid optical link number
        ///
        /// @returns Bitmask of the unmasked VFATs
        struct getVFATMask : public xhal::common::Method {
            uint32_t operator()(uint32_t ohN) const;
        };

        /// @brief Configure the VFAT mask for a given OptoHybrid
        ///
        /// @warning Prefer @c updateVFATMask to disable VFAT.
        ///          This function should be used only to reset the VFAT mask.
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c vfat_mask Bitmask of the unmasked VFATs
        struct setVFATMask : public xhal::common::Method {
            void operator()(uint32_t ohN, uint32_t vfat_mask) const;
        };

        /// @brief Update the VFAT mask for a given OptoHybrid
        ///
        /// Compared to @c setVFATMask, this function can only disable additional VFAT.
        /// For coherency reasons, the operation is done atomically.
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c vfat_mask Bitmask of the unmasked VFATs
        struct updateVFATMask : public xhal::common::Method {
            uint32_t operator()(uint32_t ohN, uint32_t vfat_mask) const;
        };

        /// @brief Performs a write transaction on a specified register for all unmasked the VFATs of a specified OptoHybrid
        ///
        /// @warning This method does not throw on register access errors.
        ///          Check the return value to avoid ignoring errors silently.
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c vfat_mask VFAT mask
        /// @param @c register_name VFAT register name (as in the address table)
        /// @param @c value Value to write
        ///
        /// @returns The VFAT mask for which the writes were successful
        struct broadcastWrite : public xhal::common::Method {
            uint32_t operator()(uint32_t ohN, uint32_t vfat_mask, const std::string& register_name, uint32_t value) const;
        };

        /// @brief Performs a read transaction on a specified register for all unmasked VFATs of a specified OptoHybrid
        ///
        /// @warning This method does not throw on register access errors.
        ///          In case of failed reads - or masked VFAT -, the @c std::nullopt
        ///          value is used as as placeholder, indicating that no value was
        ///          successfully read.
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c vfat_mask VFAT mask
        /// @param @c register_name VFAT register name (as in the address table)
        ///
        /// @returns @c A vector holding the results of the read transactions, if successful
        struct broadcastRead : public xhal::common::Method {
            std::vector<std::optional<uint32_t>> operator()(uint32_t ohN, uint32_t vfat_mask, const std::string& register_name) const;
        };

        /// @brief Enables all the channels for the unmasked VFATs
        ///
        /// @warning This method does not throw on register access errors.
        ///          Check the return value to avoid ignoring errors silently.
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c vfat_mask Bitmask of the unmasked VFATs
        struct enableAllChannels : public xhal::common::Method {
            uint32_t operator()(uint32_t ohN, uint32_t vfat_mask) const;
        };

        /// @brief Configures the calibration circuit of the VFAT
        ///
        /// @warning This method does not throw on register access errors.
        ///          Check the return value to avoid ignoring errors silently.
        ///
        /// @param @c ohN OptoHybrid link number
        /// @param @c vfat_mask VFAT mask
        /// @param @c config Configuration to be applied
        //
        /// @returns The VFAT mask for which the configuration was fully successful
        struct confCalPulse : xhal::common::Method {
            uint32_t operator()(uint32_t ohN, uint32_t vfat_mask, const calPulseConfig& config) const;
        };

        /// @brief Enable or disable calibration pulses for the channel of interest
        ///
        /// The typical usage is to first disable the calibration pulses on all channels
        /// and, then, enable it on a single channel.
        ///
        /// @warning This method does not throw on register access errors.
        ///          Check the return value to avoid ignoring errors silently.
        ///
        /// @param @c ohN OptoHybrid link number
        /// @param @c vfat_mask VFAT mask
        /// @param @c channel Channel of interest (use @c gem::hardware::vfat3::VFAT_ALL_CHANNELS to target all channels)
        /// @param @c enable Whether to enable or disable the calibration pulses for the channel of interest
        ///
        /// @returns The VFAT mask for which the configuration was fully successful
        struct enableChannelCalPulse : public xhal::common::Method {
            uint32_t operator()(uint8_t ohN, uint32_t vfat_mask, uint8_t channel = VFAT_ALL_CHANNELS, bool enable = false) const;
        };

        /// @brief Configure the VFAT based on back-end configuration files
        ///
        /// The configurations file are stored under <configuration-path>/vfat/config-oh{x}-vfat{y}.cfg and
        /// must contain key-value pairs with @c key being the VFAT register name and @c value
        /// being the register value to apply.
        ///
        /// @param @c ohN OptoHybrid number
        /// @param @c vfatMask VFAT mask
        /// @param @c run configures the VFAT into run mode
        ///
        /// @returns The VFAT mask for which the configuration was successful
        struct configure : public xhal::common::Method {
            uint32_t operator()(uint16_t ohN, uint32_t vfatMask, bool run) const;
        };

        /// @brief reads all channel registers for unmasked vfats and stores values in chanRegData
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c vfatMask VFAT mask
        ///
        /// @returns @c std::vector<uint32_t> containing channel registers with idx = vfatN * 128 + chan
        struct getChannelRegistersVFAT3 : public xhal::common::Method {
            std::vector<uint32_t> operator()(const uint16_t& ohN, const uint32_t& vfatMask) const;
        };

        /// @brief Writes all VFAT3 channel registers based on the channel register
        ///
        /// @description The channel setting vector should contain VFATS_PER_OH*NUM_CHANS entries.
        ///              The (vfatN,ch) pairing determines the index via: idx = vfatN*128 + ch
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c vfatMask VFAT mask
        /// @param @c chanRegData contains the channel registers
        struct setChannelRegistersVFAT3Simple : public xhal::common::Method {
            void operator()(const uint16_t& ohN, const uint32_t& vfatMask, const std::vector<uint32_t>& chanRegData) const;
        };

        /// @brief Writes all VFAT3 channel registers based on the bits in the channel register
        ///
        /// @description Each vector should contain VFATS_PER_OH*NUM_CHANS entries.
        ///              The (vfatN,ch) pairing determines the index via: idx = vfatN*128 + ch
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c vfatMask VFAT mask
        /// @param @c calEnable contains the calibration pulse enable settings
        /// @param @c masks contains the channel mask settings
        /// @param @c trimARM contains the ARM DAC channel trim settings
        /// @param @c trimARMPol contains the ARM DAC channel polarity settings
        /// @param @c trimZCC contains the ZCC DAC channel trim settings
        /// @param @c trimZCCPol contains the ZCC DAC channel polarity settings
        struct setChannelRegistersVFAT3 : public xhal::common::Method {
            void operator()(const uint16_t& ohN,
                const uint32_t& vfatMask,
                const std::vector<uint32_t>& calEnable,
                const std::vector<uint32_t>& masks,
                const std::vector<uint32_t>& trimARM,
                const std::vector<uint32_t>& trimARMPol,
                const std::vector<uint32_t>& trimZCC,
                const std::vector<uint32_t>& trimZCCPol)
                const;
        };

        /// @brief
        ///
        /// @param @c ohN OptoHybrid optical link number
        ///
        /// @returns @c std::map whose keys are the register name and whose values are std::vector<uint32_t> of the values for each VFAT
        struct statusVFAT3s : public xhal::common::Method {
            std::map<std::string, std::vector<uint32_t>> operator()(const uint16_t& ohN) const;
        };

        /// @brief Returns list of values of the most important VFAT3 register
        ///
        /// @param @c ohN OptoHybrid optical link number
        /// @param @c vfatMask VFAT mask
        /// @param @c rawID if true, do not perform Reed--Muller decoding of the read chip ID (default is false)
        ///
        /// @returns @c std::vector containing the chipID values, indexed by the VFAT position number
        struct getVFAT3ChipIDs : public xhal::common::Method {
            std::vector<std::optional<uint32_t>> operator()(uint16_t ohN, uint32_t vfatMask, bool rawID = false) const;
        };

    } // namespace gem::hardware::vfat3
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_VFAT3_H
