/// @file
/// @brief RPC module for daq monitoring methods

#ifndef GEM_HARDWARE_MONITOR_H
#define GEM_HARDWARE_MONITOR_H

#include <xhal/common/common.h>

#include <map>
#include <string>
#include <variant>

namespace gem {
namespace hardware {
    namespace monitor {

        /// @brief Reads a set of TTC monitoring registers
        ///
        /// @returns @c std::map
        struct getTTCMain : xhal::common::Method {
            std::map<std::string, std::variant<uint32_t, double>> operator()() const;
        };

        /// @brief Reads a set of trigger monitoring registers on the back-end
        ///
        /// @returns @c std::map
        struct getTriggerMain : xhal::common::Method {
            std::map<std::string, uint32_t> operator()() const;
        };

        /// @brief Reads a set of trigger monitoring registers on the front-end
        ///
        /// @param @c ohMask A bit number which specifies which OptoHybrids to read from.
        ///        Having a value of 1 in the n^th bit indicates that the n^th OptoHybrid should be considered.
        /// @returns @c std::map
        struct getTriggerFE : xhal::common::Method {
            std::map<std::string, uint32_t> operator()(uint32_t ohMask) const;
        };
        /// @brief Reads a set of trigger monitoring registers at the OH
        ///
        /// @returns @c std::map where keys are the aforementioned register names prefixed with OHX, values are the read register values
        struct getTriggerBE : xhal::common::Method {
            std::map<std::string, uint32_t> operator()() const;
        };

        /// @brief Reads a set of DAQ monitoring registers
        ///
        /// @returns @c std::map
        struct getDAQMain : xhal::common::Method {
            std::map<std::string, std::variant<uint32_t, double>> operator()() const;
        };

        /// @brief Reads a set of DAQ monitoring registers at the OH
        ///
        /// @returns @c std::map
        struct getDAQOH : xhal::common::Method {
            std::map<std::string, uint32_t> operator()() const;
        };

        /// @brief Reads the GBT link status registers (READY, WAS_NOT_READY, etc...)
        ///
        /// @returns @c std::map
        struct getGBTLink : xhal::common::Method {
            std::map<std::string, uint32_t> operator()() const;
        };

        /// @brief Reads a set of the GBTx/lpGBT chip parameters (FEC counters,...)
        ///
        /// @returns @c std::map
        struct getGBTMain : xhal::common::Method {
            std::map<std::string, uint32_t> operator()() const;
        };

        /// @brief Reads a set of OH monitoring registers at the OH
        ///
        /// @param @c ohMask A  bit number which specifies which OptoHybrids to read from.
        ///        Having a value of 1 in the n^th bit indicates that the n^th OptoHybrid should be considered.
        ///
        /// @returns @c std::map
        struct getOHMain : xhal::common::Method {
            std::map<std::string, uint32_t> operator()(uint32_t ohMask) const;
        };

        /// @brief reads FPGA Sysmon values of all unmasked OH's
        ///
        /// @details Reads FPGA core temperature in d°C.
        /// @details Will also check error conditions (over temperature, 1V VCCINT, and 2.5V VCCAUX), and the error conunters for those conditions.
        ///
        /// @param @c ohMask A bit number which specifies which OptoHybrids to read from.
        ///        Having a value of 1 in the n^th bit indicates that the n^th OptoHybrid should be considered.
        ///
        /// @returns @c std::map
        struct getOHSysmon : xhal::common::Method {
            std::map<std::string, std::variant<uint32_t, double>> operator()(uint32_t ohMask = 0xfff) const;
        };

        /// @brief Reads a set of SCA monitoring registers
        ///
        /// @returns @c std::map
        struct getSCAMain : xhal::common::Method {
            std::map<std::string, uint32_t> operator()() const;
        };

        /// @brief Reads the VFAT link status registers (LINK_GOOD, SYNC_ERR_CNT, etc...) for a particular ohMask
        ///
        /// @returns @c std::map
        struct getVFATLink : xhal::common::Method {
            std::map<std::string, uint32_t> operator()() const;
        };

        /// @brief Creates a dump of the required registers, specified in the file passed as argument
        ///
        /// @throws @c std::runtime_error if it is not possible to open @c fname
        ///
        /// @returns @c std::map
        struct getmonCTP7dump : xhal::common::Method {
            std::map<std::string, uint32_t> operator()(const std::string& fname) const;
        };

        /// @brief Returns the OptoHybrid mask for which the monitoring should be enabled
        ///
        /// An OptoHybrid is considered as monitorable if the slow-control communication
        /// with the FPGA can be established.
        ///
        /// @returns @c uint32_t
        struct getOHMask : xhal::common::Method {
            uint32_t operator()() const;
        };

        /// @brief Returns the SCA mask for which the monitoring should be enabled
        ///
        /// @returns @c uint32_t
        struct getSCAMask : xhal::common::Method {
            uint32_t operator()() const;
        };

        /// @brief Read all currents available from the SCA
        ///
        /// @details Values are in mA.
        ///
        /// @param @c ohMask A bit number which specifies which OptoHybrids to read from.
        ///        Having a value of 1 in the n^th bit indicates that the n^th OptoHybrid should be considered.
        ///
        /// @returns @c std::map
        struct getSCACurrent : xhal::common::Method {
            std::map<std::string, double> operator()(uint32_t ohMask) const;
        };

        /// @brief Read all temperatures available from the SCA
        ///
        /// @details Values are in °C.
        ///
        /// @param @c ohMask A bit number which specifies which OptoHybrids to read from.
        ///        Having a value of 1 in the n^th bit indicates that the n^th OptoHybrid should be considered.
        ///
        /// @returns @c std::map
        struct getSCATemperature : xhal::common::Method {
            std::map<std::string, double> operator()(uint32_t ohMask) const;
        };

        /// @brief Read all voltages available from the SCA
        ///
        /// @details Values are in mV.
        ///
        /// @param @c ohMask A bit number which specifies which OptoHybrids to read from.
        ///        Having a value of 1 in the n^th bit indicates that the n^th OptoHybrid should be considered.
        ///
        /// @returns @c std::map
        struct getSCAVoltage : xhal::common::Method {
            std::map<std::string, double> operator()(uint32_t ohMask) const;
        };

        /// @brief Read all available VTRx signal strength sensors
        ///
        /// @details Values are in µA.
        ///
        /// @param @c ohMask A bit number which specifies which OptoHybrids to read from.
        ///        Having a value of 1 in the n^th bit indicates that the n^th OptoHybrid should be considered.
        ///
        /// @returns @c std::map
        struct getSCARSSI : xhal::common::Method {
            std::map<std::string, double> operator()(uint32_t ohMask) const;
        };

        /// @brief Reads a set of Single Event Upset related registers in OH FPGA
        ///
        /// @param @c ohMask A bit number which specifies which OptoHybrids to read from.
        ///        Having a value of 1 in the n^th bit indicates that the n^th OptoHybrid should be considered.
        ///
        /// @returns @c std::map
        struct getOHSEU : xhal::common::Method {
            std::map<std::string, uint32_t> operator()(uint32_t ohMask) const;
        };

        /// @brief Reads a set of registers describing the front-end configuration and running status
        ///
        /// @param @c ohMask A bit number which specifies which OptoHybrids to read from.
        ///        Having a value of 1 in the n^th bit indicates that the n^th OptoHybrid should be considered.
        ///
        /// @returns @c std::map
        struct getFEStatus : xhal::common::Method {
            std::map<std::string, uint32_t> operator()(uint32_t ohMask) const;
        };

    } // namespace gem::hardware::monitor
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_MONITOR_H
