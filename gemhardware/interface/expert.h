/// @file

#ifndef GEM_HARDWARE_EXPERT_H
#define GEM_HARDWARE_EXPERT_H

#include <xhal/common/common.h>

#include <stdint.h>

namespace gem {
namespace hardware {
    namespace expert {

        /// @brief Automatically mask the VFAT considered as unstable
        ///
        /// @detail Any OptoHybrid that does not have any enabled VFAT anymore is masked as well. This is done to prevent any unneeded timeout in the event building.
        ///
        /// @param @c optohybridMask OptoHybrids to consider during the procedure
        ///
        /// @return Number of VFAT masked during the last iteration
        struct maskUnstableVFAT : public xhal::common::Method {
            uint32_t operator()(uint32_t optohybridMask) const;
        };

        /// @brief Recover the system from uncorrectable SEU
        ///
        /// This method aims at recovering (parts of) the system which have
        /// been affected by uncorrectable SEU through local reset,
        /// reprogramming, and reconfiguration.
        ///
        /// Currently implemented only for the OptoHybrid FPGA.
        ///
        /// @param @c optohybridMask OptoHybrids to consider during the procedure
        ///
        /// @returns Number of OH that have been reconfigured to fix SEU
        struct recoverSEU : public xhal::common::Method {
            uint32_t operator()(uint32_t optohybridMask) const;
        };

    } // namespace gem::hardware::expert
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_EXPERT_H
