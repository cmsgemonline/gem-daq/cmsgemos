/// @file

#ifndef GEM_HARDWARE_BOARD_H
#define GEM_HARDWARE_BOARD_H

#include <xhal/common/common.h>

namespace gem {
namespace hardware {
    namespace board {

        /// @brief Configure the board following user's configuration
        struct configure : public xhal::common::Method {
            void operator()() const;
        };

        /// @brief Perform a recovery (hard reset) of the board
        ///
        /// This method should perform all the actions required to bring the board
        /// to a working state from an arbitrary state (e.g. after power up).
        ///
        /// The exact sequence of actions depends on the board and can include:
        /// - An external synthesizers configuration and reset
        /// - An FPGA re-loading
        /// - An optics configuration (e.g. enabling the TX lasers)
        /// - ...
        struct recover : public xhal::common::Method {
            void operator()() const;
        };

        /// @brief Perform a soft reset of the board
        ///
        /// This method aims at bringing back the board to a default state with
        /// minimal impacts on the infrastructure.
        ///
        /// Typically the PLL/clocks, the MGTs, and the user logic are reset to
        /// their default configuration.
        struct reset : public xhal::common::Method {
            void operator()() const;
        };

    } // namespace gem::hardware::board
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_BOARD_H
