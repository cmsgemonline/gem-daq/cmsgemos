/// @file
/// @brief RPC module for GBT methods

#ifndef GEM_HARDWARE_GBT_H
#define GEM_HARDWARE_GBT_H

#include <xhal/common/common.h>

#include <map>
#include <optional>
#include <vector>

namespace gem {
namespace hardware {
    namespace gbt {
        constexpr uint8_t PHASE_MIN = 0; ///< Minimal working phase for the eLink RX GBT phase
        constexpr uint8_t PHASE_MAX = 14; ///< Maximal working phase for the eLink RX GBT phase
        constexpr uint8_t PHASE_INVALID = 15; ///< Invalid phase for the eLink RX GBT phase

        constexpr uint8_t PHASE_DEFAULT_UNKNOWN = 16; ///< Phase to use when the default phase is not yet known (will be reduced to a phase of 0)
        constexpr uint8_t PHASE_DEFAULT_UNKNOWN_TYPE = 32; ///< Phase to use when the chamber type is not known (will be reduced to a phase of 0)

        /// @brief Stores the default phases to be used for a given chamber type
        static const std::map<std::string, std::vector<size_t>> default_phases = {
            { "me0", // Measured on a single layer with prototype electronics
                { 11, 10, 11, 10, PHASE_DEFAULT_UNKNOWN, PHASE_DEFAULT_UNKNOWN, PHASE_DEFAULT_UNKNOWN, PHASE_DEFAULT_UNKNOWN, 7, 7, 7, 12, PHASE_DEFAULT_UNKNOWN, PHASE_DEFAULT_UNKNOWN, PHASE_DEFAULT_UNKNOWN, PHASE_DEFAULT_UNKNOWN, 12, 6, 7, 12, PHASE_DEFAULT_UNKNOWN, PHASE_DEFAULT_UNKNOWN, PHASE_DEFAULT_UNKNOWN, PHASE_DEFAULT_UNKNOWN } },
            { "short",
                { 6, 9, 13, 5, 5, 4, 7, 8, 7, 9, 12, 12, 6, 3, 2, 6, 9, 10, 8, 6, 10, 9, 12, 7 } },
            { "long",
                { 4, 8, 12, 4, 4, 6, 4, 11, 4, 4, 10, 4, 5, 5, 7, 4, 2, 8, 11, 4, 5, 2, 6, 4 } },
            { "m1",
                { 3, 7, 8, 8, 8, 9, 9, 6, 8, 5, 8, 8 } },
            { "m2",
                { 9, 5, 6, 6, 7, 8, 8, 6, 7, 4, 7, 6 } },
            { "m3",
                { 6, 10, 5, 3, 6, 6, 6, 11, 3, 8, 3, 10 } },
            { "m4",
                { 4, 9, 11, 4, 5, 5, 5, 10, 11, 7, 10, 8 } },
            { "m5",
                { 11, 6, 7, 7, 7, 9, 7, 5, 7, 4, 7, 8 } },
            { "m6",
                { 9, 5, 6, 5, 7, 7, 6, 5, 6, 11, 7, 6 } },
            { "m7",
                { 7, 3, 4, 4, 5, 6, 6, 3, 4, 9, 4, 3 } },
            { "m8",
                { 5, 8, 3, 5, 5, 7, 5, 11, 4, 9, 9, 8 } },
        };

        constexpr uint32_t PHASE_SCAN_SYNC_ERROR = 0x1; ///< VFAT synchronization error counter different than 0
        constexpr uint32_t PHASE_SCAN_SC_READ_ERROR = 0x2; ///< Slow-control read failed
        constexpr uint32_t PHASE_SCAN_SC_WRITE_ERROR = 0x4; ///< Slow-control write failed
        constexpr uint32_t PHASE_SCAN_SC_RUN_MODE_ERROR = 0x8; ///< VFAT not set into run mode (will prevent getting DAQ data)
        constexpr uint32_t PHASE_SCAN_DAQ_CRC_ERROR = 0x10; ///< CRC errors detected in the DAQ data
        constexpr uint32_t PHASE_SCAN_SET_PHASE_ERROR = 0x20; ///< Could not set the GBT phase
        constexpr uint32_t PHASE_SCAN_SET_PHASE_MODE_ERROR = 0x40; ///< Could not set the GBT RX phase tracking mode to "Fixed"

        /// @brief Scan the GBT phases for the VFAT connected to one OptoHybrid
        ///
        /// The scan seeks for valid RX phases for the VFAT's of one OptoHybrid.
        ///
        /// @details A phase is considered valid when:
        /// * `SYNC_ERR_CNT = 0`
        /// * `HW_ID`, `HW_ID_VER`, `HW_CHIP_ID`, `TEST_REG`, and `CFG_RUN` can be read/written
        /// * Received DAQ packets upon L1A are not corrupted (CRC check)
        ///
        /// @param @c ohN OptoHybrid index number
        /// @param @c number_slow The number of slow-control transactions to attempt
        /// @param @c number_fast The number of L1A to send
        ///
        /// @return A 2-dimensional array containing the type of error, if any, for each phase of each VFAT in the selected OptoHybrid.
        struct scanGBTPhases : public xhal::common::Method {
            std::vector<std::vector<uint32_t>> operator()(uint32_t ohN, uint32_t number_slow, uint32_t number_fast) const;
        };

        /// @brief Write the phase of a single VFAT
        ///
        /// @throws @c std::range_error if
        ///           * the specified @c ohN is larger than that supported by the firmware
        ///           * the specified @c vfatN is larger than that supported by the firmware
        ///
        /// @param @c ohN OptoHybrid index number
        /// @param @c gbtN GBT index number
        /// @param @c eLinkN eLink index number
        /// @param @c phase Phase value to write. Minimal phase is @c PHASE_MIN and maximal phase is @c PHASE_INVALID.
        struct writeGBTPhase : public xhal::common::Method {
            void operator()(uint32_t ohN, uint32_t gbtN, uint16_t eLinkN, uint8_t phase) const;
        };

        /// @brief Configure the GBT
        ///
        /// @param ohN OptoHybrid index number
        /// @param gbtMask GBT mask of the GBT to be configured
        ///
        /// @returns The GBT mask for which the configuration was successful
        struct configure : public xhal::common::Method {
            uint8_t operator()(uint32_t ohN, uint8_t gbtMask) const;
        };

        /// @brief Return the OptoHybrid mask for which all GBT are locked
        ///
        /// @param @c history If set, also check that no loss of synchronization happened since the last link reset
        struct getGBTStatus : public xhal::common::Method {
            uint32_t operator()(bool history) const;
        };

        /// @brief Returns the GBT considered locked on a given OptoHybrid
        ///
        /// @param @c ohN OptoHybrid index number
        /// @param @c history If set, also check that no loss of synchronization happened since the last link reset
        ///
        /// @returns Bitmask of locked GBT
        struct getLockedGBT : public xhal::common::Method {
            uint8_t operator()(uint32_t ohN, bool history) const;
        };

        /// @brief Reads the GBT serial number
        struct readGBTSerialNumber : public xhal::common::Method {
            std::vector<std::optional<uint32_t>> operator()(uint32_t ohN) const;
        };

    } // namespace gem::hardware::gbt
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_GBT_H
