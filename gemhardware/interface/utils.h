/// @file
/// @brief util methods for RPC modules running on a Zynq

#ifndef GEM_HARDWARE_UTILS_H
#define GEM_HARDWARE_UTILS_H

#include <xhal/common/common.h>

#include <iomanip>
#include <ostream>
#include <string>

namespace gem {
namespace hardware {
    namespace utils {

        /// @brief Reads a value from remote register. Exported method. Register mask is applied. An @c std::runtime_error is thrown if the register cannot be read.
        ///
        /// @param @c regName Register name
        ///
        /// @returns @c uint32_t register value
        struct readRemoteReg : public xhal::common::Method {
            uint32_t operator()(const std::string& regName) const;
        };

        /// @brief Writes a value to remote register. Exported method. Register mask is applied. An @c std::runtime_error is thrown if the register cannot be read.
        ///
        /// @param @c regName Register name
        ///
        /// @param @c value Value to write
        struct writeRemoteReg : public xhal::common::Method {
            void operator()(const std::string& regName, const uint32_t& value) const;
        };

    } // namespace gem::hardware::utils
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_UTILS_H
