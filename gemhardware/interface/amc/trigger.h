/// @file

#ifndef GEM_HARDWARE_AMC_TRIGGER_H
#define GEM_HARDWARE_AMC_TRIGGER_H

#include <xhal/common/common.h>

namespace gem {
namespace hardware {
    namespace amc {
        namespace trigger {

            /// @brief Structure holding all the information about an S-bit cluster
            struct clusterInfo {
                int16_t bx; ///< Bunch crossing, relative to the trigger, associated to the cluster (cluster DAQ only)
                uint16_t l1a_delay; ///< Delay between the cluster and the next L1A (S-bit monitor only)
                uint16_t pad; ///< Cluster pad number within a partition
                uint8_t partition; ///< Cluster partition number
                uint8_t size; ///< Cluster size (pad units)
                bool valid; ///< Whether the cluster is valid or not
                bool l1a; ///< Whether or not an L1A was received at the same BX (cluster DAQ only)

                /// @brief @c cereal serialization
                template <class Archive>
                void serialize(Archive& archive)
                {
                    archive(bx, l1a_delay, pad, partition, size, valid, l1a);
                }
            };

            /// @brief Enable the given OptoHybrids trigger links to the EMTF
            ///
            /// @param @c optohybridMask Defines the OptoHybrid for which the trigger data will be sent
            struct enableLinksToEMTF : public xhal::common::Method {
                void operator()(uint32_t optohybridMask) const;
            };

            /// @brief Returns the OptoHybrids for which the trigger links to the EMTF are enabled
            ///
            /// @returns Bitmask of the OptoHybrids trigger links to the EMTF enabled
            struct getEnabledLinksToEMTF : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @brief Returns the fully working trigger links, from the GEM back-end point of view
            ///
            /// @warning Implemented only for GE1/1 with the CTP7, flavor that is believed to be the only one affected
            ///
            /// @param @c optohybridMask OptoHybrids for which to check the trigger links status
            /// @param @c reset If set, the non-working links receivers are going to be reset in an attempt to fix the communication
            ///
            /// @return The OptoHybrid mask for which the trigger links are valid
            struct getValidLinks : public xhal::common::Method {
                uint32_t operator()(uint32_t optohybridMask, bool reset) const;
            };

            /// @brief Reads out S-bits clusters from OptoHybrid @c ohN for a number of seconds given by acquireTime and returns them to the user
            ///
            /// @warning While the S-bit monitor stores the difference between the S-bit clusters and the next L1A as a 32 bits number,
            ///          any any value higher than 0xFFF (12 bits) will be truncated to 0xFFF. This is done to avoid stalling the routine.
            ///
            /// @param @c ohN OptoHybrid number
            /// @param @c acquireTime Acquisition time on the wall clock in seconds
            ///
            /// @returns All the valid clusters readout during the acquisition period.
            ///          The first (second) index encodes the iteration (cluster) number
            struct readoutSbitMonitor : public xhal::common::Method {
                std::vector<std::vector<clusterInfo>> operator()(uint8_t ohN, uint32_t acquireTime) const;
            };

        } // namespace gem::hardware::amc::trigger
    } // namespace gem::hardware::amc
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_AMC_TRIGGER_H
