/// @file
/// @brief RPC module for AMC DAQ methods

#ifndef GEM_HARDWARE_AMC_DAQ_H
#define GEM_HARDWARE_AMC_DAQ_H

#include <xhal/common/common.h>

namespace gem {
namespace hardware {
    namespace amc {
        namespace daq {

            static constexpr uint32_t DISABLE_LOCAL_READOUT = -1;
            static constexpr uint32_t ENABLE_LOCAL_READOUT = -2;

            /// @brief Whether or not to enable the event builder
            struct enableDAQ : public xhal::common::Method {
                void operator()(bool enable = true) const;
            };

            /// @brief Set the zero suppression mode
            ///
            /// @param @c enable true means any VFAT data packet with all 0's will be suppressed
            struct setZS : public xhal::common::Method {
                void operator()(bool enable = true) const;
            };

            /// @brief Disable zero suppression of VFAT data
            struct disableZS : public xhal::common::Method {
                void operator()() const;
            };

            /// @brief Reset the event builder
            ///
            /// @param @c wait_for_resync Whether the event builder will stay in reset until the next ReSync
            struct resetDAQ : public xhal::common::Method {
                void operator()(bool wait_for_resync = false) const;
            };

            /// @returns Returns the 32 bit word corresponding to the DAQ link control register
            struct getDAQLinkControl : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @returns Returns the 32 bit word corresponding to the DAQ link status register
            struct getDAQLinkStatus : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @returns Returns true if the DAQ link is ready
            struct daqLinkReady : public xhal::common::Method {
                bool operator()() const;
            };

            /// @returns Returns true if the DAQ link is clock is locked
            struct daqClockLocked : public xhal::common::Method {
                bool operator()() const;
            };

            /// @returns Returns true if the TTC is ready
            struct daqTTCReady : public xhal::common::Method {
                bool operator()() const;
            };

            /// @returns Returns the current TTS state asserted by the DAQ link firmware
            struct daqTTSState : public xhal::common::Method {
                uint8_t operator()() const;
            };

            /// @returns Returns true if the event FIFO is almost full (70%)
            struct daqAlmostFull : public xhal::common::Method {
                bool operator()() const;
            };

            /// @returns Returns true if the L1A FIFO is empty (0%)
            struct l1aFIFOIsEmpty : public xhal::common::Method {
                bool operator()() const;
            };

            /// @returns Returns true if the L1A FIFO is almost full (70%)
            struct l1aFIFOIsAlmostFull : public xhal::common::Method {
                bool operator()() const;
            };

            /// @returns Returns true if the L1A FIFO is full (100%)
            struct l1aFIFOIsFull : public xhal::common::Method {
                bool operator()() const;
            };

            /// @returns Returns true if the L1A FIFO is underflos
            struct l1aFIFOIsUnderflow : public xhal::common::Method {
                bool operator()() const;
            };

            /// @returns Returns the number of events built and sent on the DAQ link
            struct getDAQLinkEventsSent : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @returns Returns the curent L1AID (number of L1As received)
            struct getDAQLinkL1AID : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @returns Returns the curent L1A rate (in Hz)
            struct getDAQLinkL1ARate : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @returns Returns
            struct getDAQLinkDisperErrors : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @returns Returns
            struct getDAQLinkNonidentifiableErrors : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @returns Returns the DAQ link input enable mask
            struct getDAQLinkInputMask : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @brief Sets input to be included in the event building
            struct setDAQLinkInputMask : public xhal::common::Method {
                void operator()(uint32_t optohybridMask) const;
            };

            /// @returns Returns the timeout used in the event builder before closing the event and sending the (potentially incomplete) data
            struct getDAQLinkDAVTimeout : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @param @c max is a bool specifying whether to query the max timer or the last timer
            ///
            /// @returns Returns the spent building an event
            struct getDAQLinkDAVTimer : public xhal::common::Method {
                uint32_t operator()(const bool& max) const;
            };

            /// @param @c gtx is the input link status to query
            ///
            /// @returns Returns the the 32-bit word corresponding DAQ status for the specified link
            struct getLinkDAQStatus : public xhal::common::Method {
                uint32_t operator()(const uint8_t& gtx) const;
            };

            /// @param @c gtx is the input link counter to query
            /// @param @c mode specifies whether to query the corrupt VFAT count (0x0) or the event number
            ///
            /// @returns Returns the link counter for the specified mode
            struct getLinkDAQCounters : public xhal::common::Method {
                uint32_t operator()(const uint8_t& gtx, const uint8_t& mode) const;
            };

            /// @param @c gtx is the input link status to query
            ///
            /// @returns Returns a block of the last 7 words received from the OH on the link specified
            struct getLinkLastDAQBlock : public xhal::common::Method {
                uint32_t operator()(const uint8_t& gtx) const;
            };

            /// @returns Returns the timeout before the event builder firmware will close the event and send the data
            struct getDAQLinkInputTimeout : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @returns Returns the run type stored in the data stream
            struct getRunType : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @returns Special run parameters 1,2,3 as a single 24 bit word
            struct getRunParameters : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @brief Set DAQ link timeout
            ///
            /// @param @c inputTO is the number of clock cycles to wait after receipt of last L1A and
            ///        last packet received from the optical link before closing an "event"
            ///        (in units of 160MHz clock cycles, value/4 for 40MHz clock cycles)
            struct setDAQLinkInputTimeout : public xhal::common::Method {
                void operator()(const uint32_t& inputTO = 0x100) const;
            };

            /// @brief Special run type to be written into data stream
            ///
            /// @param @c rtype is the run type
            struct setRunType : public xhal::common::Method {
                void operator()(const uint32_t& rtype) const;
            };

            /// @param @c value is a 24 bit word to write into the run paramter portion of the GEM header
            ///
            /// @returns Set special run parameter to be written into data stream
            struct setRunParameters : public xhal::common::Method {
                void operator()(const uint32_t& value) const;
            };

            /// @brief Configure the FED ID related registers
            ///
            /// @param @c fedID The FED ID number (limited to 12-bits on the µTCA systems)
            /// @param @c slot The slot ID (used only in the µTCA systems)
            struct configureFedID : public xhal::common::Method {
                void operator()(uint32_t fedID, const uint8_t slot) const;
            };

            /// @brief Configure the DAQ module for data taking
            ///
            /// Does the following:
            ///   * Disable the event event builder
            ///   * Apply the user supplied parameters
            ///   * Set the run parameters for physics
            ///   * Reset the event builder
            ///
            /// @param @c enable_daqlink Whether the DAQLink is enabled
            /// @param @c local_readout_prescale Prescale value to use for the local readout (see @c DISABLE_LOCAL_READOUT and @c ENABLE_LOCAL_READOUT to respectively disable and enable the readout path)
            /// @param @c inputEnableMask Input enable mask to set
            /// @param @c enableZS Whether zero suppression is enabled
            struct configureDAQModule : public xhal::common::Method {
                void operator()(bool enable_daqlink = true, uint32_t local_readout_prescale = ENABLE_LOCAL_READOUT, uint32_t inputEnableMask = 0x0, bool enableZS = false) const;
            };

            /// @brief Configures the optimized calibration data format on the VFAT data
            ///
            /// @param @c enable Whether to enable or disable the data format
            /// @param @c channel VFAT channel of interest for the readout
            struct configureAMCCalDataFormat : public xhal::common::Method {
                void operator()(bool enable, uint8_t channel) const;
            };

        } // namespace gem::hardware::amc::daq
    } // namespace gem::hardware::amc
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_AMC_DAQ_H
