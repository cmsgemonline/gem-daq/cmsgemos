/// @file
/// @brief RPC module for AMC TTC methods

#ifndef GEM_HARDWARE_AMC_TTC_H
#define GEM_HARDWARE_AMC_TTC_H

#include <xhal/common/common.h>

namespace gem {
namespace hardware {
    namespace amc {
        namespace ttc {

            /// @brief Enable local TTC generator
            ///
            /// In case the TTC generator is disabled, the TTC commands
            /// are taken from the backplane.
            struct enableTTCGenerator : public xhal::common::Method {
                void operator()(bool enable = true) const;
            };

            /// @brief Send a single HardReset with the TTC generator
            struct sendSingleHardReset : public xhal::common::Method {
                void operator()() const;
            };

            /// @brief Send a single ReSync with the TTC generator
            struct sendSingleReSync : public xhal::common::Method {
                void operator()() const;
            };

            /// @brief Send a (pseudo) ReSync sequence with the TTC generator
            struct sendReSyncSequence : public xhal::common::Method {
                void operator()() const;
            };

            /// @brief Reset the TTC module
            struct ttcModuleReset : public xhal::common::Method {
                void operator()() const;
            };

            /// @brief Reset the MMCM of the TTC module
            struct ttcMMCMReset : public xhal::common::Method {
                void operator()() const;
            };

            /// @brief Reset the counters of the TTC module
            struct ttcCounterReset : public xhal::common::Method {
                void operator()() const;
            };

            /// @brief Whether or not L1As are currently enabled
            struct getL1AEnable : public xhal::common::Method {
                bool operator()() const;
            };

            /// @brief Whether or not to enable L1As
            struct setL1AEnable : public xhal::common::Method {
                void operator()(bool enable = true) const;
            };

            /// @brief Returns the L1A ID received by the TTC module
            struct getL1AID : public xhal::common::Method {
                uint32_t operator()() const;
            };

            /// @brief Returns the curent L1A rate (in Hz)
            struct getL1ARate : public xhal::common::Method {
                uint32_t operator()() const;
            };

        } // namespace gem::hardware::amc::ttc
    } // namespace gem::hardware::amc
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_AMC_TTC_H
