/// @file

#ifndef GEM_HARDWARE_BLASTER_RAM_DEFS_H
#define GEM_HARDWARE_BLASTER_RAM_DEFS_H

#include <stdint.h>

namespace gem {
namespace hardware {
    namespace amc {
        namespace blaster {

            /// @brief BLASTER RAM defs
            enum struct BLASTERType : uint8_t {
                GBT = 0x1, ///< GBT RAM
                OptoHybrid = 0x2, ///< OptoHybrid RAM
                VFAT = 0x4, ///< VFAT RAM
                ALL = 0x7, ///< All RAMs
            };

        } // namespace gem::hardware::amc::blaster
    } // namespace gem::hardware::amc
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_BLASTER_RAM_DEFS_H
