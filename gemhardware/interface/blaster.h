/// @file

#ifndef GEM_HARDWARE_BLASTER_H
#define GEM_HARDWARE_BLASTER_H

#include <xhal/common/common.h>

#include <map>
#include <vector>

namespace gem {
namespace hardware {
    namespace blaster {

        /// @brief Establishes the front-end communication
        ///
        /// - Configures the GBT
        /// - Resets the SCA
        /// - Program the OptoHybrid FPGA via PROM-less
        /// - Configures the OptoHybrid registers
        /// - Configures VFAT
        ///
        /// @param @c optohybridMask OptoHybrids for which to establish the communication
        ///
        /// @return OptoHybrids for which both the DAQ and trigger paths are configured
        struct execute : public xhal::common::Method {
            uint32_t operator()(uint32_t optohybridMask) const;
        };

        /// @brief Configures a subset of OptoHybrid FPGA
        ///
        /// - Masks the corresponding EMTF links
        /// - Resets the OptoHybrid FPGA (only)
        /// - Programs the OptoHybrid FPGA via PROM-less
        /// - Configures the OptoHybrid registers
        /// - Unmasks the EMTF links if all OK
        ///
        /// @param @c optohybridMask OptoHybrids for which to configure the FPGA
        ///
        /// @return OptoHybrids for which the FPGA was configured successfully.
        ///     It does *not* guarantee that the EMTF links are enabled.
        struct configureOptoHybridFPGA : public xhal::common::Method {
            uint32_t operator()(uint32_t optohybridMask) const;
        };

    } // namespace gem::hardware::blaster
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_BLASTER_H
