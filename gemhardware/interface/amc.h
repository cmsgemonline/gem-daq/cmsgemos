/// @file
/// @brief RPC module for AMC methods

#ifndef GEM_HARDWARE_AMC_H
#define GEM_HARDWARE_AMC_H

#include <xhal/common/common.h>

#include <map>
#include <string>
#include <vector>

namespace gem {
namespace hardware {
    namespace amc {

        /// @brief Stores all AMC-related facts
        struct facts {
            std::string hostname; ///< Back-end board hostname
            uint8_t number_oh; ///< Number of OptoHybrids supported by the firmware in this AMC
            uint8_t number_gbt; ///< Number of GBT per OptoHybrid
            uint8_t number_vfat; ///< Number of VFAT per OptoHybrid

            /// @brief @c cereal serialization
            template <class Archive>
            void serialize(Archive& archive)
            {
                archive(hostname, number_oh, number_gbt, number_vfat);
            }
        };

        /// @brief Retrieve all AMC-related facts
        struct getFacts : public xhal::common::Method {
            facts operator()() const;
        };

        /// @brief Perform a recovery (hard reset) of the AMC
        ///
        /// @details This RPC method exists mainly to maintain the compatibility with the CTP7.
        ///     Any board whose system part is included in the 0xBEFE framwork should use the
        ///     methods declared in the @c gem::hardware::board namespace instead.
        struct recover : public xhal::common::Method {
            void operator()() const;
        };

        /// @brief Perform a soft reset of the AMC
        ///
        /// This RPC method configure the back-end with sane defaults and
        /// resets the logic.
        struct reset : public xhal::common::Method {
            void operator()() const;
        };

        /// @brief Configure the AMC
        struct configure : public xhal::common::Method {
            void operator()() const;
        };

    } // namespace gem::hardware::amc
} // namespace gem::hardware
} // namespace gem

#endif
