/// @file

#include "amc/trigger.h"
#include "gbt.h"
#include "optical.h"
#include "utils.h"

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <sstream>

namespace py = pybind11;

constexpr auto LOGGING_CONFIGURATION = R"----(
log4cplus.rootLogger=FATAL,stdout
log4cplus.appender.stdout=log4cplus::ConsoleAppender
log4cplus.appender.stdout.layout=log4cplus::PatternLayout
log4cplus.appender.stdout.layout.ConversionPattern= %h[%i] - %M - %m%n
)----";

PYBIND11_MODULE(gempy, m)
{
    using namespace py::literals;

    // Initialization
    std::istringstream t_logging_configuration { LOGGING_CONFIGURATION };
    gem::hardware::utils::init_logging(t_logging_configuration);

    gem::hardware::utils::init_lmdb();
    gem::hardware::utils::init_memhub();

    // Module description
    m.doc() = R"pbdoc(
        GEM Python bindings
        -------------------
        .. currentmodule:: gempy
        .. autosummary::
           :toctree: _generate
           find_registers
           readReg
           writeReg
    )pbdoc";

    // Optics
    m.def("measure_rx_power", &gem::hardware::optical::measure_rx_power);
    m.def("measure_tx_power", &gem::hardware::optical::measure_tx_power);

    // Register accesses
    m.def("find_registers", &gem::hardware::utils::find_registers);
    m.def("readRawAddress", &gem::hardware::utils::readRawAddress, "address"_a, "mask"_a = 0xffffffff);
    m.def("writeRawAddress", &gem::hardware::utils::writeRawAddress, "address"_a, "value"_a, "mask"_a = 0xffffffff);
    m.def("readReg", static_cast<uint32_t (*)(const std::string&)>(&gem::hardware::utils::readReg));
    m.def("writeReg", static_cast<void (*)(const std::string&, uint32_t)>(&gem::hardware::utils::writeReg));

    // GBT register accesses
    m.def("readGBTReg", &gem::hardware::gbt::readGBTReg, "ohN"_a, "gbtN"_a, "address"_a, "mask"_a = 0xff, "unsafe"_a = false);
    m.def("writeGBTReg", &gem::hardware::gbt::writeGBTReg, "ohN"_a, "gbtN"_a, "address"_a, "value"_a, "mask"_a = 0xff, "unsafe"_a = false);

    // Cluster DAQ
    namespace trigger = gem::hardware::amc::trigger;

    py::class_<trigger::clusterInfo>(m, "clusterInfo")
        .def_readwrite("bx", &trigger::clusterInfo::bx)
        .def_readwrite("pad", &trigger::clusterInfo::pad)
        .def_readwrite("partition", &trigger::clusterInfo::partition)
        .def_readwrite("size", &trigger::clusterInfo::size)
        .def_readwrite("valid", &trigger::clusterInfo::valid)
        .def_readwrite("l1a", &trigger::clusterInfo::l1a);

    m.def("readoutClusterDAQ", &trigger::readoutClusterDAQ,
        "ohN"_a,
        "windowWidth"_a,
        "triggerPosition"_a,
        "nTriggers"_a = 1,
        "triggerOnCluster"_a = true,
        "triggerOnL1A"_a = false);
}
