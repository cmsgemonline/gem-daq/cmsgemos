/// @file
/// @brief the registration of all the RPC methods done inside the module_init function for the different RPC modules are combined into this

#include "utils.h"
#include <gem/hardware/amc.h>
#include <gem/hardware/amc/blaster_ram.h>
#include <gem/hardware/amc/daq.h>
#include <gem/hardware/amc/trigger.h>
#include <gem/hardware/amc/ttc.h>
#include <gem/hardware/blaster.h>
#include <gem/hardware/board.h>
#include <gem/hardware/calibration.h>
#include <gem/hardware/expert.h>
#include <gem/hardware/gbt.h>
#include <gem/hardware/monitor.h>
#include <gem/hardware/optohybrid.h>
#include <gem/hardware/utils.h>
#include <gem/hardware/vfat3.h>

#include <xhal/server/register.h>

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <fstream>
#include <sstream>

/// @brief Environment variable name storing the @c log4cplus configuration filename
constexpr auto LOGGING_CONFIGURATION_ENV = "RPCSVC_LOGGING_CONF";

/// @brief Default @c log4cplus configuration used when the configuration file cannot be read
constexpr auto LOGGING_DEFAULT_CONFIGURATION = R"----(
log4cplus.rootLogger=INFO,syslog
log4cplus.appender.syslog=log4cplus::SysLogAppender
log4cplus.appender.syslog.ident=rpcsvc
log4cplus.appender.syslog.facility=user
log4cplus.appender.syslog.layout=log4cplus::PatternLayout
log4cplus.appender.syslog.layout.ConversionPattern= %h[%i] - %M - %m
)----";

extern "C" {
const char* module_version_key = "";
const int module_activity_color = 4;

void module_init(ModuleManager* modmgr)
{
    /// Tries to read the logging configuration file pointed by LOGGING_CONFIGURATION_ENV
    /// If not found, defaults to the embedded configuration stored in LOGGING_DEFAULT_CONFIGURATION
    const char* const logging_configuration_filename = std::getenv(LOGGING_CONFIGURATION_ENV);

    std::ifstream logging_configuration_file {};
    if (logging_configuration_filename)
        logging_configuration_file.open(logging_configuration_filename);

    if (logging_configuration_file.is_open()) {
        gem::hardware::utils::init_logging(logging_configuration_file);
    } else {
        // Fallback to the default embedded configuration
        std::istringstream t_logging_default_configuration { LOGGING_DEFAULT_CONFIGURATION };
        gem::hardware::utils::init_logging(t_logging_default_configuration);
    }

    gem::hardware::utils::init_lmdb();
    gem::hardware::utils::init_memhub();

    // Register all RPC methods

    // from amc
    xhal::server::registerMethod<gem::hardware::amc::configure>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::getFacts>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::recover>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::reset>(modmgr);

    // from amc/blaster_ram
    xhal::server::registerMethod<gem::hardware::amc::blaster::readConfRAM>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::blaster::writeConfRAM>(modmgr);

    // from amc/daq
    xhal::server::registerMethod<gem::hardware::amc::daq::configureAMCCalDataFormat>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::daq::configureDAQModule>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::daq::configureFedID>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::daq::enableDAQ>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::daq::l1aFIFOIsEmpty>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::daq::resetDAQ>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::daq::setDAQLinkInputMask>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::daq::setDAQLinkInputTimeout>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::daq::setRunParameters>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::daq::setRunType>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::daq::setZS>(modmgr);

    // from amc/trigger
    xhal::server::registerMethod<gem::hardware::amc::trigger::enableLinksToEMTF>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::trigger::getEnabledLinksToEMTF>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::trigger::getValidLinks>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::trigger::readoutSbitMonitor>(modmgr);

    // from amc/ttc
    xhal::server::registerMethod<gem::hardware::amc::ttc::enableTTCGenerator>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::ttc::getL1AEnable>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::ttc::getL1AID>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::ttc::getL1ARate>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::ttc::sendReSyncSequence>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::ttc::sendSingleHardReset>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::ttc::sendSingleReSync>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::ttc::setL1AEnable>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::ttc::ttcCounterReset>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::ttc::ttcMMCMReset>(modmgr);
    xhal::server::registerMethod<gem::hardware::amc::ttc::ttcModuleReset>(modmgr);

    // from blaster
    xhal::server::registerMethod<gem::hardware::blaster::execute>(modmgr);
    xhal::server::registerMethod<gem::hardware::blaster::configureOptoHybridFPGA>(modmgr);

    // from board
    xhal::server::registerMethod<gem::hardware::board::configure>(modmgr);
    xhal::server::registerMethod<gem::hardware::board::recover>(modmgr);
    xhal::server::registerMethod<gem::hardware::board::reset>(modmgr);

    // from calibration
    xhal::server::registerMethod<gem::hardware::calibration::calPulseScan>(modmgr);
    xhal::server::registerMethod<gem::hardware::calibration::checkSbitMappingWithCalPulse>(modmgr);
    xhal::server::registerMethod<gem::hardware::calibration::checkSbitRateWithCalPulse>(modmgr);
    xhal::server::registerMethod<gem::hardware::calibration::clusterMaskRateScanParallel>(modmgr);
    xhal::server::registerMethod<gem::hardware::calibration::dacScan>(modmgr);
    xhal::server::registerMethod<gem::hardware::calibration::eyeOpeningMonitorScan>(modmgr);
    xhal::server::registerMethod<gem::hardware::calibration::sbitRateScanParallel>(modmgr);
    xhal::server::registerMethod<gem::hardware::calibration::scurveScan>(modmgr);
    xhal::server::registerMethod<gem::hardware::calibration::thresholdScan>(modmgr);
    xhal::server::registerMethod<gem::hardware::calibration::ttcGenConf>(modmgr);
    xhal::server::registerMethod<gem::hardware::calibration::ttcGenToggle>(modmgr);

    // from monitor
    xhal::server::registerMethod<gem::hardware::monitor::getDAQMain>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getDAQOH>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getFEStatus>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getGBTLink>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getGBTMain>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getOHMain>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getOHMask>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getOHSEU>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getOHSysmon>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getSCACurrent>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getSCAMain>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getSCAMask>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getSCARSSI>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getSCATemperature>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getSCAVoltage>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getTTCMain>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getTriggerBE>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getTriggerFE>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getTriggerMain>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getVFATLink>(modmgr);
    xhal::server::registerMethod<gem::hardware::monitor::getmonCTP7dump>(modmgr);

    // from expert
    xhal::server::registerMethod<gem::hardware::expert::maskUnstableVFAT>(modmgr);
    xhal::server::registerMethod<gem::hardware::expert::recoverSEU>(modmgr);

    // from gbt
    xhal::server::registerMethod<gem::hardware::gbt::configure>(modmgr);
    xhal::server::registerMethod<gem::hardware::gbt::getGBTStatus>(modmgr);
    xhal::server::registerMethod<gem::hardware::gbt::getLockedGBT>(modmgr);
    xhal::server::registerMethod<gem::hardware::gbt::readGBTSerialNumber>(modmgr);
    xhal::server::registerMethod<gem::hardware::gbt::scanGBTPhases>(modmgr);
    xhal::server::registerMethod<gem::hardware::gbt::writeGBTPhase>(modmgr);

    // from optohybrid
    xhal::server::registerMethod<gem::hardware::oh::configure_fpga>(modmgr);
    xhal::server::registerMethod<gem::hardware::oh::getOHfpgaDNA>(modmgr);
    xhal::server::registerMethod<gem::hardware::oh::getProgrammedFPGA>(modmgr);
    xhal::server::registerMethod<gem::hardware::oh::resetFPGA>(modmgr);

    // from utils
    xhal::server::registerMethod<gem::hardware::utils::readRemoteReg>(modmgr);
    xhal::server::registerMethod<gem::hardware::utils::writeRemoteReg>(modmgr);

    // from vfat3
    xhal::server::registerMethod<gem::hardware::vfat3::broadcastRead>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::broadcastWrite>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::confCalPulse>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::configure>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::enableAllChannels>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::enableChannelCalPulse>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::getChannelRegistersVFAT3>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::getSyncedVFAT>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::getVFAT3ChipIDs>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::getVFATMask>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::setChannelRegistersVFAT3>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::setChannelRegistersVFAT3Simple>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::setVFATMask>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::statusVFAT3s>(modmgr);
    xhal::server::registerMethod<gem::hardware::vfat3::updateVFATMask>(modmgr);
}
}
