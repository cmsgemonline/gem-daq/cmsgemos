/// @file

#include "utils.h"
#include <gem/hardware/utils.h>

#include <sstream>

constexpr auto LOGGING_CONFIGURATION = R"----(
log4cplus.rootLogger=INFO,stdout
log4cplus.appender.stdout=log4cplus::ConsoleAppender
log4cplus.appender.stdout.layout=log4cplus::PatternLayout
log4cplus.appender.stdout.layout.ConversionPattern= %h[%i] - %M - %m%n
)----";

int main(int argc, const char* argv[])
{
    std::istringstream t_logging_configuration { LOGGING_CONFIGURATION };
    gem::hardware::utils::init_logging(t_logging_configuration);

    // This tool only purpose is to update the LMDB address table via the
    // utils::update_address_table() function. As such, there is no need to
    // call the various gemhardware init functions:
    //
    // - utils::init_lmdb()
    //   Fails during boostrap due to the DB being open in read-only mode (cannot
    //   create the LMDB file).
    //
    // - utils::init_memhub()
    //   Fails if the memhub server is not running.

    gem::hardware::utils::update_address_table();

    return 0;
}
