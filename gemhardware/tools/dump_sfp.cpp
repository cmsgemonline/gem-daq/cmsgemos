#include "libi2c.h"

#include <fmt/format.h>

#include <map>
#include <string>
#include <utility>

namespace /* anonymous */ {

/// @brief List of all available SFP and their connection information
///
/// @details The key is the SFP name, the value is the pair (I²C path, I²C address)
std::map<std::string, std::pair<std::string, uint8_t>> sfp_info_map = {
    { "cxp0-tx", { "/dev/i2c-2", 0x50 } },
    { "cxp1-tx", { "/dev/i2c-3", 0x50 } },
    { "cxp2-tx", { "/dev/i2c-4", 0x50 } },
    { "cxp0-rx", { "/dev/i2c-2", 0x54 } },
    { "cxp1-rx", { "/dev/i2c-3", 0x54 } },
    { "cxp2-rx", { "/dev/i2c-4", 0x54 } },
    { "mp0-tx", { "/dev/i2c-1", 0x2c } },
    { "mp0-rx", { "/dev/i2c-1", 0x30 } },
    { "mp1-rx", { "/dev/i2c-1", 0x31 } },
    { "mp2-rx", { "/dev/i2c-1", 0x32 } },
};

} // anonymous namespace

int main(int argc, const char* argv[])
{
    // Dump the registers for all known SFP
    for (const auto& sfp_info : sfp_info_map) {
        fmt::print(FMT_STRING("> Device: {:s} ({:s} - {:#x})\n"), sfp_info.first, sfp_info.second.first, sfp_info.second.second);

        gem::hardware::i2c_device device(sfp_info.second.first, sfp_info.second.second);

        // Lower page
        for (uint16_t address = 0; address < 127; ++address) {
            const auto value = device.read(address);
            const auto character = std::isprint(static_cast<unsigned char>(value)) ? value : '.';
            fmt::print(FMT_STRING("  x.{:03d} - {:#04x} {:c}\n"), address, value, character);
        }

        // Upper page 0
        device.write(127, 0);
        for (uint16_t address = 128; address < 256; ++address) {
            const auto value = device.read(address);
            const auto character = std::isprint(static_cast<unsigned char>(value)) ? value : '.';
            fmt::print(FMT_STRING("  0.{:03d} - {:#04x} {:c}\n"), address, value, character);
        }

        // Upper page 1
        device.write(127, 1);
        for (uint16_t address = 128; address < 256; ++address) {
            const auto value = device.read(address);
            const auto character = std::isprint(static_cast<unsigned char>(value)) ? value : '.';
            fmt::print(FMT_STRING("  1.{:03d} - {:#04x} {:c}\n"), address, value, character);
        }
    }

    return 0;
}
