/// @file

#include <gem/calibration/Calibration.h>

#include <gem/calibration/CalibrationWeb.h>

#include <algorithm>
#include <cstdlib>
#include <iomanip>
#include <map>
#include <set>
#include <vector>

XDAQ_INSTANTIATOR_IMPL(gem::calibration::Calibration);

gem::calibration::Calibration::Calibration(xdaq::ApplicationStub* stub)
    : gem::utils::GEMApplication(stub)
{
    // Configuration parameters
    p_appInfoSpace->fireItemAvailable("nFED", &m_number_of_feds);

    // Web UI
    p_gemWebInterface = new gem::calibration::CalibrationWeb(this);

    xgi::deferredbind(this, this, &Calibration::applyAction, "applyAction");
    xgi::deferredbind(this, this, &Calibration::runScan, "runScan");
}

gem::calibration::Calibration::~Calibration()
{
    CMSGEMOS_DEBUG("gem::calibration::Calibration : Destructor called");
}

void gem::calibration::Calibration::applyAction(xgi::Input* in, xgi::Output* out)
{
    bool t_errorsOccured = false;
    cgicc::Cgicc cgi(in);

    CMSGEMOS_INFO("Calibration::applyAction for calibration cal_type_select" << cgi["cal_type_select"]->getValue());

    m_calType = calTypeSelector.find(cgi["cal_type_select"]->getValue())->second;

    std::map<std::string, std::string>::iterator it = (m_scanParams.find(m_calType)->second).begin();
    while (it != (m_scanParams.find(m_calType)->second).end()) {

        it->second = cgi[it->first]->getValue();
        CMSGEMOS_INFO("Calibration::applyAction for calibration " << static_cast<int>(m_calType) << " : " << it->first << " = " << it->second);
        it++;
    }

    std::stringstream t_stream;
    for (int i = 0; i < m_number_of_feds.value_; ++i) {
        t_stream.clear();
        t_stream.str(std::string());
        t_stream << "fed-id" << std::setfill('0') << std::setw(2) << i + 1;
        bool checked = false;
        checked = cgi.queryCheckbox(t_stream.str());
        if (checked) {
            for (unsigned int j = 0; j < gem::utils::GEMApplication::MAX_AMCS_PER_CRATE; ++j) {
                t_stream.clear();
                t_stream.str(std::string());
                t_stream << "fed-id" << std::setfill('0') << std::setw(2) << i + 1 << ".amc" << std::setfill('0') << std::setw(2) << j + 1;
                checked = cgi.queryCheckbox(t_stream.str());
                if (checked) {
                    std::string amc_id = t_stream.str();
                    m_amcOpticalLinks.emplace(amc_id, 0);
                    t_stream << ".ohMask";
                    uint32_t ohMask = std::stoul(cgi[t_stream.str()]->getValue(), 0, 16);

                    CMSGEMOS_DEBUG("Calibration::applyAction : OH mask for " << t_stream.str() << " ohMask is " << ohMask);
                    if (ohMask > 0xffc) {
                        t_errorsOccured = true;
                        CMSGEMOS_ERROR("Calibration::applyAction : OH mask for " << t_stream.str() << " is out of allowed boundaries! Ignoring it");
                    } else {
                        m_amcOpticalLinks.find(amc_id)->second = ohMask;
                    }
                } else { // end if checked for amc
                    std::string amc_id = t_stream.str();
                    m_amcOpticalLinks.emplace(amc_id, 0);
                    m_amcOpticalLinks.find(amc_id)->second = 0;
                }
            } // end loop over NAMC
        } // end if checked for FED
        else { /// filling also the empty slot.amc with OHMask=0
            for (unsigned int j = 0; j < gem::utils::GEMApplication::MAX_AMCS_PER_CRATE; ++j) {
                t_stream.clear();
                t_stream.str(std::string());
                t_stream << "fed-id" << std::setfill('0') << std::setw(2) << i + 1 << ".amc" << std::setfill('0') << std::setw(2) << j + 1;
                std::string amc_id = t_stream.str();
                m_amcOpticalLinks.emplace(amc_id, 0);
                m_amcOpticalLinks.find(amc_id)->second = 0;
                CMSGEMOS_DEBUG("Calibration::applyAction: ****Empty OH mask for " << t_stream.str() << " ohMask is 0");
            } // end if checked for amc
        }
    } // end loop over FED
    // TODO: should we check if at least one link is selected??

    // DACSCAN type
    if (m_calType == calType::DACSCAN) {
        for (auto& it : m_dacScanTypeParams) {
            t_stream.clear();
            t_stream.str(std::string());
            t_stream << it.second.label;
            bool checked = false;
            checked = cgi.queryCheckbox(t_stream.str());
            if (checked) {
                it.second.min = cgi[it.second.label + "_Min"]->getIntegerValue();
                it.second.max = cgi[it.second.label + "_Max"]->getIntegerValue();
                it.second.scan = true;
                CMSGEMOS_INFO("Calibration::applyAction DAC SCAN  for calibration: checked " << it.second.label << " with par  min " << it.second.min << " max " << it.second.max);
            } else {
                it.second.scan = false;
            }
        }
    }
    // TODO: should we check the range of the parameters?

    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
    if (t_errorsOccured) {
        *out << "{\"status\":1,\"alert\":\"There was an error in the parameters retrieval. Please check the XDAQ log for more information\"}";
    } else {
        *out << "{\"status\":0,\"alert\":\"Parameters successfully applied. Now you can run the scan.\"}";
    }
    /// Just for debugging TODO:Remove
    for (auto it : m_scanParams.find(m_calType)->second) {
        CMSGEMOS_DEBUG("Calibration::end of applyAction for calibration " << static_cast<int>(m_calType) << " : " << it.first << " = " << it.second);
    }

    sendSOAPMessageForCalibration();
}

void gem::calibration::Calibration::runScan(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_INFO("Calibration::runScan received signal to run a scan - will proceed to launch a scan on the AMCManager");
    sendSOAPMessageToLaunchScan();
}

void gem::calibration::Calibration::fillCalibConfigBag(xdata::Bag<ScanInfo>& calibConfigBag)
{

    for (auto it : m_scanParams.find(m_calType)->second) {
        if (calibConfigBag.find(it.first) == calibConfigBag.end()) {
            CMSGEMOS_WARN("Calibration::fillCalibConfigBag key element " << it.first << " not found in ScanInfo bag");
        } else {
            // Fix xdata bugs...
            if (calibConfigBag.find(it.first)->second->type() == "bool") {
                // The xdata::Boolean type only understands "true" and "false" in the fromString method.
                // The "1" and "0" values are not supported. Make the conversion by hand here first.
                const std::string value = std::stoi(it.second) ? "true" : "false";
                calibConfigBag.find(it.first)->second->fromString(value);
            } else {
                calibConfigBag.find(it.first)->second->fromString(it.second);
            }
        }
    }
    if (m_calType == calType::DACSCAN) {
        for (auto it : m_dacScanTypeParams) {
            if (it.second.scan) {
                xdata::Bag<dacScanInfoParam> myDacBag;
                myDacBag.bag.name = it.second.label;
                myDacBag.bag.min = it.second.min;
                myDacBag.bag.max = it.second.max;
                myDacBag.bag.doScan = it.second.scan;
                calibConfigBag.bag.dacScanInfo.push_back(myDacBag);
            }
        }
    }
}

void gem::calibration::Calibration::sendSOAPMessageForCalibration()
{

    std::set<std::string> groups = p_appZone->getGroupNames();
    for (auto i = groups.begin(); i != groups.end(); ++i) {
        CMSGEMOS_DEBUG("GEMCalibration::sendSOAPMessageForCalibration: xDAQ group: " << *i
                                                                                     << " getApplicationGroup() "
                                                                                     << p_appZone->getApplicationGroup(*i)->getName());

        const xdaq::ApplicationGroup* ag = p_appZone->getApplicationGroup(*i);
        std::set<const xdaq::ApplicationDescriptor*> allApps = ag->getApplicationDescriptors();
        for (auto j = allApps.begin(); j != allApps.end(); ++j) {
            std::string classname = (*j)->getClassName();
            if (((*j)->getClassName()).find("gem::") == std::string::npos)
                continue;
            CMSGEMOS_DEBUG("GEMCalibration::sendSOAPMessageForCalibration: xDAQ group: " << *i
                                                                                         << " allApp class name "
                                                                                         << (*j)->getClassName());

            const xdaq::ApplicationDescriptor* app = *j;
            std::string command = "calibParamRetrieve";

            xdata::Bag<ScanInfo> myScanBag;
            fillCalibConfigBag(myScanBag);
            myScanBag.bag.scanType = static_cast<int>(m_calType);
            gem::utils::soap::sendCommandWithParameters(command, { { "scanInfo", &myScanBag } }, p_appContext, p_appDescriptor, app);
        }
    }
}

void gem::calibration::Calibration::sendSOAPMessageToLaunchScan()
{

    std::set<std::string> groups = p_appZone->getGroupNames();
    for (auto i = groups.begin(); i != groups.end(); ++i) {
        CMSGEMOS_DEBUG("GEMCalibration::sendSOAPMessageToLaunchScan: xDAQ group: " << *i
                                                                                   << "getApplicationGroup() "
                                                                                   << p_appZone->getApplicationGroup(*i)->getName());

        const xdaq::ApplicationGroup* ag = p_appZone->getApplicationGroup(*i);
        std::set<const xdaq::ApplicationDescriptor*> allApps = ag->getApplicationDescriptors();

        for (auto j = allApps.begin(); j != allApps.end(); ++j) {
            std::string classname = (*j)->getClassName();
            if (((*j)->getClassName()).find("AMCManager") == std::string::npos)
                continue;
            const xdaq::ApplicationDescriptor* app = *j;
            std::string command = "launchScan";
            gem::utils::soap::sendCommand(command, p_appContext, p_appDescriptor, app);
        }
    }
}

void gem::calibration::Calibration::initializeAndFillOpticalLinksMap(std::map<std::string, xdata::Integer>* amcOpticalLinksMap)
{

    for (auto it : m_amcOpticalLinks) {
        if (amcOpticalLinksMap->find(it.first) == amcOpticalLinksMap->end()) {
            xdata::Integer tmp;
            tmp = it.second;
            amcOpticalLinksMap->insert(std::pair<std::string, xdata::Integer>(it.first, tmp));

        } else {
            amcOpticalLinksMap->find(it.first)->second = it.second;
        }
    }
}

void gem::calibration::Calibration::fillBagFromOpticalLinksMap(std::unordered_map<std::string, xdata::Serializable*>* bag, std::map<std::string, xdata::Integer>* amcOpticalLinksMap)
{

    std::map<std::string, xdata::Integer>::iterator iter;
    for (iter = amcOpticalLinksMap->begin(); iter != amcOpticalLinksMap->end(); ++iter) {
        bag->insert(std::make_pair(iter->first, &(iter->second)));
        CMSGEMOS_DEBUG("GEMCalibration::fillBagFromOpticalLinksMap: mask " << iter->first << " value " << iter->second);
    }
}
