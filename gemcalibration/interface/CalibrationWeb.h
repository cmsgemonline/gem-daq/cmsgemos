/// @file

#ifndef GEM_CALIBRATION_CALIBRATIONWEB_H
#define GEM_CALIBRATION_CALIBRATIONWEB_H

#include <gem/calibration/Calibration.h>
#include <gem/utils/GEMWebApplication.h>

#include <cgicc/HTMLClasses.h>
#include <xdaq/WebApplication.h>
#include <xgi/framework/Method.h>

#include <memory>

namespace gem::calibration {

// Forward declaration
class Calibration;

class CalibrationWeb : public gem::utils::GEMWebApplication {
public:
    CalibrationWeb(Calibration* application);
    virtual ~CalibrationWeb();

    /// @brief Set the interface for the selected calibration routine
    ///
    /// @param calibration routine to be perfomed
    /// @param xgi output for the html
    /// @param number of FED taken from XML configuration file for the amc optical links
    void settingsInterface(calType calType, xgi::Output* out, xdata::Integer number_of_feds);

    /// @brief Selector for the slot and OH mask
    ///
    /// @param xgi output for the html
    /// @param number of FED taken from XML configuration file for the amc optical links
    void slotsAndMasksSelector(xgi::Output* out, xdata::Integer number_of_feds);

    /// @brief Generic selector for a calibration routine parameter
    ///
    /// @param label associated with the parameter
    /// @param parameter name
    /// @param parameter default value
    /// @param xgi output for the html
    void genericParamSelector(std::string labelName, std::string paramName, std::string defaultValue, xgi::Output* out);

    /// @brief Selector for the DAC scan calibration routine parameter with dropdown to select the parameter to scan on
    ///
    /// @param xgi output for the html
    void dacScanV3Selector(xgi::Output* out);

    /// @brief Form selector for the DAC scan calibration routine parameter
    ///
    /// @param parameter name
    /// @param parameter default value
    /// @param xgi output for the html
    void genericParamSelector_dacScan(std::string paramName, int defaultValue, xgi::Output* out);

    /// @brief Radio selector for calibration routine parameter
    ///
    /// @param parameter name for the radio selector
    /// @param default value for the radio selector parameter
    /// @param radio selector parameter options
    /// @param xgi output for the html
    void genericRadioSelector(std::string paramName, std::string paramValue, gem::calibration::Calibration::scanParamsRadioSelector radio_param, xgi::Output* out);

protected:
    void webDefault(xgi::Input* in, xgi::Output* out) override;
    void calibrationPage(xgi::Input* in, xgi::Output* out);

    /// @brief Sets the calibration routine interface from an initial dropdown menu
    void setCalInterface(xgi::Input* in, xgi::Output* out);

private:
    size_t level;
    const std::map<calType, std::string> alertMap {
        { calType::PHYSICS, "Select the parameter, apply, and remember to reconfigure. Enjoy data taking!" },
        { calType::GBTRXPHASE, "To run the routine select the type of component to scan, the number of transactions and triggers." },
        { calType::LATENCY, "To run the routine  indicate the time interval of each step, the  pulse stretch configuration, the minimum and maximum scan values, the step size." },
        { calType::SCURVE, "To run the routine select the cards, the OptoHybrids, the VFATs and links. \
                Indicate the number of events for each position and the latency and pulse stretch configuration." },
        { calType::SBITARMDACSCAN, "To run the routine select the time interval, the THR_ARM_DAC interval and step." },
        { calType::ARMDACSCAN, "To run the routine select the time interval of each step, and the minimum and maximum scan values." },
        { calType::TRIMDAC, "To run the routine select the cards, the OptoHybrids." },
        { calType::DACSCAN, "To run the routine select the cards, and the OptoHybrids." },
        { calType::CALIBRATEARMDAC, "To run the routine select the cards, the Optohybrids. \
                Indicate the number of events for each position and the latency and pulse stretch configuration." },
        { calType::CLUSTERMASKSCAN, "To run the routine select the mask delay (min and max) and width to scan. The masking is disabled for width=0." },
        { calType::CALPULSESCAN, "Specify the number of calibration pulses and their amplitudes to run the routine. \
                This routine can be used to detect some kind of dead channels. \
                Additional expert configuration parameters are also available." },
        { calType::EOMSCAN, "Perform an LpGBT downlink eye-diagram. \
                Note: meaningful only on ME0." },
    };
};

} // namespace gem::calibration

#endif // GEM_CALIBRATION_CALIBRATIONWEB_H
