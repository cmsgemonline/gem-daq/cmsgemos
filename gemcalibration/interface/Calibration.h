/// @file

#ifndef GEM_CALIBRATION_CALIBRATION_H
#define GEM_CALIBRATION_CALIBRATION_H

#include <gem/calibration/GEMCalibEnums.h>
#include <gem/utils/GEMApplication.h>
#include <gem/utils/Lock.h>
#include <gem/utils/LockGuard.h>
#include <gem/utils/soap/GEMSOAPToolBox.h>

#include <xdata/Bag.h>
#include <xdata/Boolean.h>
#include <xdata/Double.h>
#include <xdata/Float.h>
#include <xdata/Integer.h>
#include <xdata/Integer32.h>
#include <xdata/Integer64.h>
#include <xdata/String.h>
#include <xdata/UnsignedInteger32.h>
#include <xdata/UnsignedInteger64.h>
#include <xdata/UnsignedLong.h>
#include <xdata/UnsignedShort.h>
#include <xdata/Vector.h>

#include <string>
#include <vector>

namespace gem::calibration {

const static std::map<std::string, calType> calTypeSelector {
    { "Physics", calType::PHYSICS },
    { "GBT Rx Phase Scan", calType::GBTRXPHASE },
    { "Latency Scan", calType::LATENCY },
    { "S-curve Scan", calType::SCURVE },
    { "S-bit ARM DAC Scan", calType::SBITARMDACSCAN },
    { "ARM DAC Scan", calType::ARMDACSCAN },
    { "Derive DAC Trim Registers", calType::TRIMDAC },
    { "DAC Scan on VFAT", calType::DACSCAN },
    { "Calibrate CFG_THR_ARM_DAC", calType::CALIBRATEARMDAC },
    { "Trigger cluster masking scan", calType::CLUSTERMASKSCAN },
    { "Calibration Pulse Scan", calType::CALPULSESCAN },
    { "Eye Opening Monitor Scan", calType::EOMSCAN },
};

class Calibration : public gem::utils::GEMApplication {

    friend class CalibrationWeb;

public:
    XDAQ_INSTANTIATOR();

    Calibration(xdaq::ApplicationStub* s);

    virtual ~Calibration();

    void applyAction(xgi::Input* in, xgi::Output* out);

    calType m_calType = calType::INVALID;

    /// @brief Launch the selected scan not using pyhisics data taking
    void runScan(xgi::Input* in, xgi::Output* out);

    ///  map to link a particular calibration routine to the parameters needed for it.
    ///  parameters can be filled with a form  or a radio selector
    std::map<calType, std::map<std::string, std::string /* uint32_t */>> m_scanParams {
        { calType::PHYSICS,
            {
                { "thresholdSourceType", "0" }, // By default use the thresholds in the VFAT configuration
                { "applyChannelMask", "1" }, // By default apply the channel masking from the VFAT configuration
                { "threshold", "1" },
            } },
        { calType::GBTRXPHASE,
            {
                { "doVFATPhaseScan", "1" },
                { "nTransactions", "50" },
                { "nTriggers", "1000000" },
            } },
        { calType::LATENCY,
            {
                { "cyclic", "0" },
                { "timeInterval", "1" },
                // { "l1aInterval", "250" },
                { "calDataFormat", "0" },
                { "pulseStretch", "0" },
                { "scanMin", "0" },
                { "scanMax", "300" },
                { "vfatChMin", "0" },
                { "vfatChMax", "1" },
                { "signalSourceType", "2" },
                { "calPulseAmplitude", "6" }, // According to VFAT 3 manual: in voltage mode low number correspond to high voltages, in current mode low number correspond to low currents
                { "calPulseDuration", "100" }, // According to VFAT 3 manual should be 25, 50, 75 or 100 nsec for current mode, half of the pulsating period in voltage mode
                { "calPulseFS", "0" }, // Scale factor for the current pulse (0 corresponds to 25%,1 to 50%, 2 to 75% ,3 to 100%)
                { "calPulsePhase", "0" }, // Timing phase of the claibration pulse in 3.125 ns steps
                { "pulseDelay", "50" },
                { "stepSize", "1" },
            } },
        { calType::SCURVE,
            {
                { "nTriggers", "100" },
                // {"trigType"  , "0"}, // TODO: TTC local should be only possible one
                { "thresholdSourceType", "1" },
                { "trimmingSourceType", "0" },
                { "l1aInterval", "250" },
                { "threshold", "100" },
                { "pulseDelay", "40" },
                //{ "calpulseDuration", "200" },
                { "latency", "33" },
                // { "vfatChMin", "0" },
                // { "vfatChMax", "127" },
                { "pulseStretch", "6" },
                { "scanMin", "0" },
                { "scanMax", "255" },
                { "stepSize", "1" },
                { "trimming", "63" },
            } },
        { calType::SBITARMDACSCAN,
            {
                { "comparatorMode", "0" },
                { "thresholdType", "0" },
                { "scanMin", "0" },
                { "scanMax", "255" },
                { "stepSize", "1" },
                { "doSingleChannel", "0" },
                { "vfatCh", "0" },
                { "timeInterval", "1" },
                { "iterNum", "1" },
                { "toggleRunMode", "1" },
            } },
        { calType::ARMDACSCAN,
            {
                { "nTriggers", "10000" },
                { "scanMin", "0" },
                { "scanMax", "255" },
                { "stepSize", "1" },
                { "l1aInterval", "1000" },
            } },
        { calType::TRIMDAC,
            {
                { "nSamples", "100" },
                // {"trigType"   , "0"}, // TODO: TTC local should be only possible one
                { "nSamples", "100" },
                { "l1aInterval", "250" },
                { "pulseDelay", "40" },
                { "latency", "33" },
                { "pulseStretch", "6" },
                { "trimValues", "-63,0,63" },
            } },
        { calType::DACSCAN,
            {
                { "adcExtType", "0" },
            } },
        { calType::CALIBRATEARMDAC,
            {
                { "nSamples", "100" },
                // {"trigType"  , "0"}, // TODO: TTC local should be only possible one
                { "l1aInterval", "250" },
                { "pulseDelay", "40" },
                { "latency", "33" },
                { "armDacPoins", "17,20,23,25,30,40,50,60,70,80,100,125,150,175" },
            } },
        { calType::CLUSTERMASKSCAN,
            {
                { "scanMin", "0" },
                { "scanMax", "31" },
                { "timeInterval", "1" },
                { "l1aInterval", "400" },
            } },
        { calType::CALPULSESCAN,
            {
                { "nTriggers", "250" },
                { "calPulseAmplitude", "0" },
                { "thresholdSourceType", "1" },
                { "threshold", "50" },
                { "l1aInterval", "250" },
                { "pulseDelay", "40" },
                { "latency", "33" },
                { "pulseStretch", "6" },
            } },
        { calType::EOMSCAN,
            {
                { "eomSamplesBX", "2048" },
                { "lpgbtEqAttnGain", "3" },
            } },
    };

    struct scanParamsRadioSelector {
        std::string label;
        std::vector<std::string> options;
    };

    ///  map of the parameters for which radio selector are used with relative viable options
    std::map<std::string, scanParamsRadioSelector> m_scanParamsRadioSelector {
        { "calPulseFS", { "Calibration current pulse scale factor", { "25%", "50%", "75%", "100%" } } },
        { "cyclic", { "Cyclic mode", { "False", "True" } } },
        { "trigType", { "TriggerType", { "Loopback", "TTC input", "Lemo/T3" } } },
        { "signalSourceType", { "Signal Source", { "Calibration Voltage Pulse", "Calibration Current Pulse", "Particle" } } },
        { "comparatorMode", { "Comparator Mode", { "From Config DB", "CFD", "ARM", "ZCC" } } },
        { "adcExtType", { "VFAT ADC reference", { "Internal", "External" } } },
        { "perChannelType", { "DAC scan per channel", { "False", "True" } } },
        { "thresholdSourceType", { "Threshold Source", { "From Config DB", "Same for all VFAT" } } },
        { "thresholdType", { "Threshold scanned", { "ARM", "ZCC" } } },
        { "trimmingSourceType", { "Trimming Source", { "From Config DB", "Same for all VFAT" } } },
        { "calDataFormat", { "Use cal data format", { "False", "True" } } },
        { "doSingleChannel", { "Use single VFAT channel", { "False", "True" } } },
        { "doVFATPhaseScan", { "Scan VFAT phases", { "False", "True" } } },
        { "toggleRunMode", { "Toggle Vfat Run Mode ", { "False", "True" } } },
        { "applyChannelMask", { "Apply Channel Mask", { "None", "From Config DB" } } },
        { "lpgbtEqAttnGain", { "LpGBT equalizer attenuation gain", { "1/3 (0x0)", "2/3 (0x1)", "2/3 (0x2)", "1/1 (0x3)" } } },
    };

    ///  map of the routine parameters and relative labels to appear in the interface
    std::map<std::string, std::string> m_scanParamsLabels {
        { "nSamples", "Number of samples" },
        { "nTransactions", "Number of slow-control transactions" },
        { "nTriggers", "Number of triggers" },
        { "l1aInterval", "L1A period (BX)" },
        { "pulseStretch", "Pulse stretch (int)" },
        { "scanMin", "Scan min" },
        { "scanMax", "Scan max" },
        { "vfatCh", "VFAT Ch" },
        { "vfatChMin", "VFAT Ch min" },
        { "vfatChMax", "VFAT Ch max" },
        { "threshold", "VFAT THR_ARM_DAC" },
        { "trimming", "VFAT trimming value" },
        // {"trigThrottle"  , "Trigger throttle (int)"},
        { "calPulseAmplitude", "Calibration pulse amplitude (CAL_DAC)" },
        { "calPulseDuration", "Calibration pulse duration (BX)" },
        { "calPulsePhase", "Calibration pulse timing phase (3.125 ns)" },
        { "pulseDelay", "Pulse delay (BX)" },
        { "latency", "Latency (BX)" }, // TODO::PulseDelay and Latency can be merged??
        { "trimValues", "Points in dac range" }, // TODO:need to be implemented properly in the back end in order to get a given number of points {-63,0,63}
        { "stepSize", "Step size (int)" },
        { "iterNum", "Iterations (int)" },
        { "timeInterval", "Time window (s)" },
        { "armDacPoins", "ARM DAC points" },
        { "eomSamplesBX", "Number of BX to gather samples from" },
    };

    std::map<std::string, uint32_t> m_amcOpticalLinks;

    dacScanType m_dacScanType;

    struct dacFeature {
        std::string label;
        uint16_t min;
        uint16_t max;
        bool scan;
    };

    ///  map of selectable DAC scan for the VFAT3 parameters and relative labels and range limits
    std::map<dacScanType, dacFeature> m_dacScanTypeParams {
        { dacScanType::CFG_CAL_DAC, { "CFG_CAL_DAC", 0, 255, false } },
        { dacScanType::CFG_BIAS_PRE_I_BIT, { "CFG_BIAS_PRE_I_BIT", 0, 255, false } },
        { dacScanType::CFG_BIAS_PRE_I_BLCC, { "CFG_BIAS_PRE_I_BLCC", 0, 63, false } },
        { dacScanType::CFG_BIAS_PRE_I_BSF, { "CFG_BIAS_PRE_I_BSF", 0, 63, false } },
        { dacScanType::CFG_BIAS_SH_I_BFCAS, { "CFG_BIAS_SH_I_BFCAS", 0, 255, false } },
        { dacScanType::CFG_BIAS_SH_I_BDIFF, { "CFG_BIAS_SH_I_BDIFF", 0, 255, false } },
        { dacScanType::CFG_BIAS_SD_I_BDIFF, { "CFG_BIAS_SD_I_BDIFF", 0, 255, false } },
        { dacScanType::CFG_BIAS_SD_I_BFCAS, { "CFG_BIAS_SD_I_BFCAS", 0, 255, false } },
        { dacScanType::CFG_BIAS_SD_I_BSF, { "CFG_BIAS_SD_I_BSF", 0, 63, false } },
        { dacScanType::CFG_BIAS_CFD_DAC_1, { "CFG_BIAS_CFD_DAC_1", 0, 63, false } },
        { dacScanType::CFG_BIAS_CFD_DAC_2, { "CFG_BIAS_CFD_DAC_2", 0, 63, false } },
        { dacScanType::CFG_HYST, { "CFG_HYST", 0, 63, false } },
        { dacScanType::CFG_THR_ARM_DAC, { "CFG_THR_ARM_DAC", 0, 255, false } },
        { dacScanType::CFG_THR_ZCC_DAC, { "CFG_THR_ZCC_DAC", 0, 255, false } },
        { dacScanType::CFG_BIAS_PRE_VREF, { "CFG_BIAS_PRE_VREF", 0, 255, false } },
        { dacScanType::CFG_VREF_ADC, { "CFG_VREF_ADC", 0, 3, false } }
    };

    void fillCalibConfigBag(xdata::Bag<ScanInfo>& myScanBag);

    void initializeAndFillOpticalLinksMap(std::map<std::string, xdata::Integer>* OpticalLinksMap);
    void fillBagFromOpticalLinksMap(std::unordered_map<std::string, xdata::Serializable*>* bag, std::map<std::string, xdata::Integer>* OpticalLinksMap);

private:
    xdata::Integer m_number_of_feds = 0;

    void sendSOAPMessageForCalibration();
    void sendSOAPMessageToLaunchScan();
};

} // namespace gem::calibration

#endif // GEM_CALIBRATION_CALIBRATION_H
