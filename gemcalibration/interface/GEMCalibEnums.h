/// @file

#ifndef GEM_CALIBRATION_CALIBENUMS_H
#define GEM_CALIBRATION_CALIBENUMS_H

namespace gem::calibration {

/// @brief
///
/// @details
///
/// @c calType is set from 0 to 15 for tracking data scan since the runtype is a 4-bit number:
/// * 0 being used for invalid events, e.g. while changing the scan point during a calibration run
/// * 1 used for physics runs
/// Scans with non-traking data should start from 16 in order.
enum class calType {
    INVALID = 0,
    PHYSICS = 1,
    TRACKING_SCANS = 2,
    LATENCY = 2,
    SCURVE = 3,
    ARMDACSCAN = 4,
    CALIBRATEARMDAC = 5,
    NON_TRACKING_SCANS = 16,
    SBITARMDACSCAN = 16,
    GBTRXPHASE = 17,
    DACSCAN = 18,
    TRIMDAC = 19,
    CLUSTERMASKSCAN = 20,
    CALPULSESCAN = 21,
    EOMSCAN = 22,
};

enum class dacScanType {
    CFG_CAL_DAC,
    CFG_BIAS_PRE_I_BIT,
    CFG_BIAS_PRE_I_BLCC,
    CFG_BIAS_PRE_I_BSF,
    CFG_BIAS_SH_I_BFCAS,
    CFG_BIAS_SH_I_BDIFF,
    CFG_BIAS_SD_I_BDIFF,
    CFG_BIAS_SD_I_BFCAS,
    CFG_BIAS_SD_I_BSF,
    CFG_BIAS_CFD_DAC_1,
    CFG_BIAS_CFD_DAC_2,
    CFG_HYST,
    CFG_THR_ARM_DAC,
    CFG_THR_ZCC_DAC,
    CFG_BIAS_PRE_VREF,
    CFG_VREF_ADC,
};

} // namespace gem::calibration

#endif // GEM_CALIBRATION_CALIBENUMS_H
