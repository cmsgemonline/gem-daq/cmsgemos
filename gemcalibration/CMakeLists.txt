cmsgemos_add_module(
    calibration
    SOURCES
    version.cpp
    Calibration.cpp
    CalibrationWeb.cpp
)

target_link_libraries(gemcalibration PUBLIC xDAQ::cgicc)
target_link_libraries(gemcalibration PUBLIC xDAQ::xcept)
target_link_libraries(gemcalibration PUBLIC xDAQ::xdata)
target_link_libraries(gemcalibration PUBLIC xDAQ::xdaq)
target_link_libraries(gemcalibration PUBLIC xDAQ::xgi)


target_link_libraries(gemcalibration PRIVATE gemutils)

target_link_libraries(gemcalibration PRIVATE xDAQ::config)
target_link_libraries(gemcalibration PRIVATE xDAQ::toolbox)
target_link_libraries(gemcalibration PRIVATE xDAQ::xoap)

target_link_libraries(gemcalibration PRIVATE Boost::boost) # for Boost.Algorithm
