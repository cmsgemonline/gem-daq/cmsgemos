/// @file

#include <gem/supervisor/GEMSupervisorWeb.h>

#include <gem/supervisor/GEMSupervisor.h>
#include <gem/supervisor/exception/Exception.h>

#include <cgicc/HTMLClasses.h>
#include <nlohmann/json.hpp>
#include <xcept/tools.h>
#include <xdata/InfoSpaceFactory.h>

#include <boost/algorithm/string.hpp>

// Additional cgicc HTML elements
namespace cgicc {
BOOLEAN_ELEMENT(section, "section");
}

gem::supervisor::GEMSupervisorWeb::GEMSupervisorWeb(gem::supervisor::GEMSupervisor* gemSupervisorApp)
    : gem::utils::GEMWebApplication(gemSupervisorApp)
    , p_gemSupervisor(gemSupervisorApp)
{
    level = 5;
    add_tab("Control Panel", &GEMSupervisorWeb::controlPanel);
}

gem::supervisor::GEMSupervisorWeb::~GEMSupervisorWeb()
{
}

void gem::supervisor::GEMSupervisorWeb::webDefault(xgi::Input* in, xgi::Output* out)
{
    if (p_gemFSMApp)
        CMSGEMOS_DEBUG("GEMSupervisorWeb::current supervisor state is"
            << dynamic_cast<gem::supervisor::GEMSupervisor*>(p_gemFSMApp)->getCurrentState());

    *out << "<script src=\"/cmsgemos/gemsupervisor/html/scripts/gemsupervisor.js\"></script>" << std::endl;

    GEMWebApplication::webDefault(in, out);
}

void gem::supervisor::GEMSupervisorWeb::controlPanel(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_DEBUG("GEMSupervisor::controlPanel");
    GEMWebApplication::controlPanel(in, out);

    *out << cgicc::br();

    displayManagedStateTable(in, out);
}

void gem::supervisor::GEMSupervisorWeb::displayManagedStateTable(xgi::Input* in, xgi::Output* out)
{
    *out << "<table class=\"xdaq-table\">" << std::endl;

    *out << cgicc::thead() << std::endl
         << cgicc::tr() << std::endl
         << cgicc::th() << "Application" << cgicc::th() << std::endl
         << cgicc::th() << "State" << cgicc::th() << std::endl
         << cgicc::th() << "Message" << cgicc::th() << std::endl
         << cgicc::tr() << std::endl
         << cgicc::thead() << std::endl;

    *out << "<tbody>" << std::endl;
    for (const auto& [descriptor, state] : p_gemSupervisor->m_globalState.application_states()) {
        const auto& classname = descriptor->getClassName();
        const auto& id = descriptor->getLocalId();
        const auto& urn = descriptor->getURN();
        const auto& state_name = GEMGlobalState::getStateName(state.state);
        const auto& state_message = state.stateMessage;

        *out << "<tr>" << std::endl;
        *out << "<td>" << std::endl
             << cgicc::h3()
             << classname << "-" << id
             << cgicc::h3()
             << "</td>" << std::endl;
        *out << "<td id=\""
             << "state-" << urn << "\">" << std::endl
             << cgicc::h3()
             << state_name
             << cgicc::h3()
             << "</td>" << std::endl;
        *out << "<td id=\""
             << "state-message-" << urn << "\">" << std::endl
             << cgicc::h3()
             << state_message
             << cgicc::h3()
             << "</td>" << std::endl;
        *out << "</tr>" << std::endl;
    }
    *out << "</tbody>" << std::endl;

    *out << "</table>" << std::endl;
}

void gem::supervisor::GEMSupervisorWeb::jsonUpdate(xgi::Input* in, xgi::Output* out)
{
    nlohmann::json jsonFile;

    for (const auto& [descriptor, state] : p_gemSupervisor->m_globalState.application_states()) {
        const auto& urn = descriptor->getURN();
        const auto& state_name = GEMGlobalState::getStateName(state.state);
        const auto& state_message = state.stateMessage;

        {
            nlohmann::json subentry;
            subentry["name"] = std::string { "state-" } + urn;
            subentry["value"] = state_name;
            jsonFile["states"].push_back(subentry);
        }

        {
            nlohmann::json subentry;
            subentry["name"] = std::string { "state-message-" } + urn;
            subentry["value"] = state_message;
            jsonFile["states"].push_back(subentry);
        }
    }

    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
    *out << jsonFile.dump(4) << std::endl;
}
