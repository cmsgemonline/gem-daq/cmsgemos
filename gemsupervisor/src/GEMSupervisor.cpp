/// @file

#include <gem/supervisor/GEMSupervisor.h>

#include <gem/calibration/GEMCalibEnums.h>
#include <gem/supervisor/GEMSupervisorWeb.h>
#include <gem/utils/exception/Exception.h>
#include <gem/utils/soap/GEMSOAPToolBox.h>

#include <toolbox/Runtime.h>
#include <toolbox/task/TimerFactory.h>

#include <boost/algorithm/string.hpp>

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <filesystem>
#include <iomanip>
#include <map>
#include <set>
#include <vector>

XDAQ_INSTANTIATOR_IMPL(gem::supervisor::GEMSupervisor);

void gem::supervisor::GEMSupervisor::TCDSConfig::registerFields(xdata::Bag<TCDSConfig>* bag)
{
    bag->addField("CPMHardwareConfig", &cpmHWConfig);
    bag->addField("FEDEnableMask", &fedEnableMask);
    bag->addField("HandleTCDS", &handleTCDS);
    bag->addField("ICIHardwareConfig", &iciHWConfig);
    bag->addField("LPMHardwareConfig", &lpmHWConfig);
    bag->addField("PIHardwareConfig", &piHWConfig);
    bag->addField("PISkipPLLReset", &piSkipPLLReset);
    bag->addField("UsePrimaryTCDS", &usePrimaryTCDS);
}

std::string gem::supervisor::GEMSupervisor::TCDSConfig::toString() const
{
    std::stringstream os;
    os << "cpmHWConfig:   " << cpmHWConfig.toString() << std::endl
       << "fedEnableMask: " << fedEnableMask.toString() << std::endl
       << "handleTCDS:    " << handleTCDS.toString() << std::endl
       << "iciHWConfig:   " << iciHWConfig.toString() << std::endl
       << "lpmHWConfig:   " << lpmHWConfig.toString() << std::endl
       << "piHWConfig:    " << piHWConfig.toString() << std::endl
       << "piSkipPLLReset:" << piSkipPLLReset.toString() << std::endl
       << "usePrimaryTCDS:" << usePrimaryTCDS.toString() << std::endl;
    return os.str();
}

gem::supervisor::GEMSupervisor::GEMSupervisor(xdaq::ApplicationStub* stub)
    : gem::utils::GEMFSMApplication(stub)
    , m_deviceLock(toolbox::BSem::FULL, true)
    , m_tcdsLock(toolbox::BSem::FULL, true)
    , m_globalState(this->getApplicationContext(), this)
    , m_scanParameter(0)
    , m_reportToRCMS(false)
    , m_timer(0)
    , m_gemRCMSNotifier(this->getApplicationLogger(),
          this->getApplicationDescriptor(),
          this->getApplicationContext())
{
    // Configuration parameters
    p_appInfoSpace->fireItemAvailable("TCDSConfig", &m_tcdsConfig);
    p_appInfoSpace->fireItemAvailable("UseFedKitReadout", &m_useFedKitReadout);
    p_appInfoSpace->fireItemAvailable("outputPath", &m_outputPath);

    p_appInfoSpace->fireItemAvailable("ReportStateToRCMS", &m_reportToRCMS);
    p_appInfoSpace->fireItemAvailable("rcmsStateListener", m_gemRCMSNotifier.getRcmsStateListenerParameter());
    m_gemRCMSNotifier.findRcmsStateListener();
    m_gemRCMSNotifier.subscribeToChangesInRcmsStateListener(p_appInfoSpace);

    // Web UI
    p_gemWebInterface = new gem::supervisor::GEMSupervisorWeb(this);

    // Create the timer counter
    const std::string class_name = this->getApplicationDescriptor()->getClassName();
    const uint32_t instance_number = this->getApplicationDescriptor()->getInstance();

    std::stringstream tmp_timer_name;
    tmp_timer_name << class_name << ":" << instance_number << ":GEMSupervisorTimer";
    m_timer = toolbox::task::getTimerFactory()->createTimer(tmp_timer_name.str());
}

gem::supervisor::GEMSupervisor::~GEMSupervisor()
{
    // make sure to empty the v_supervisedApps  vector and free the pointers
    v_supervisedApps.clear();
    m_tcdsLock.lock();
    v_leasedTCDSApps.clear();
    m_tcdsLock.unlock();
}

void gem::supervisor::GEMSupervisor::actionPerformed(xdata::Event& event)
{
    gem::utils::GEMApplication::actionPerformed(event);

    if (event.type() == "urn:xdaq-event:setDefaultValues") {
        m_handleTCDS = m_tcdsConfig.bag.handleTCDS.value_;
        // Replace the environment variables in the data path
        m_outputPath = toolbox::getRuntime()->expandPathName(std::string(m_outputPath.value_)).at(0);
    }
}

void gem::supervisor::GEMSupervisor::init()
{
    v_supervisedApps.clear();
    v_supervisedApps.reserve(0);

    // put a mutex on this
    m_tcdsLock.lock();
    CMSGEMOS_DEBUG("GEMSupervisor::init clearing TCDS leased applications list");
    v_leasedTCDSApps.clear();
    v_leasedTCDSApps.reserve(0);
    m_tcdsLock.unlock();
    // until here

    m_globalState.clear();

    CMSGEMOS_DEBUG("GEMSupervisor::init:: looping over " << p_appZone->getGroupNames().size() << " groups");
    std::set<const xdaq::ApplicationDescriptor*> used;
    std::set<std::string> groups = p_appZone->getGroupNames();
    for (auto i = groups.begin(); i != groups.end(); ++i) {
        CMSGEMOS_DEBUG("GEMSupervisor::init::xDAQ group: " << *i
                                                           << "getApplicationGroup() " << p_appZone->getApplicationGroup(*i)->getName());

        const xdaq::ApplicationGroup* ag = p_appZone->getApplicationGroup(*i);
        std::set<const xdaq::ApplicationDescriptor*> allApps = ag->getApplicationDescriptors();
        CMSGEMOS_DEBUG("GEMSupervisor::init::getApplicationDescriptors() " << allApps.size());
        for (auto j = allApps.begin(); j != allApps.end(); ++j) {
            std::string classname = (*j)->getClassName();
            CMSGEMOS_DEBUG("GEMSupervisor::init::xDAQ application descriptor " << *j << " " << classname << " we are " << p_appDescriptor);

            if (used.find(*j) != used.end())
                continue; // no duplicates
            if ((*j) == p_appDescriptor)
                continue; // don't fire the command into the GEMSupervisor again

            // maybe just write a function that populates some vectors
            // with the application classes that we want to supervise
            // avoids the problem of picking up all the xDAQ related processes
            // if (isGEMSupervised(*j))
            if (manageApplication(classname)) {
                CMSGEMOS_INFO("GEMSupervisor::init::pushing " << classname << "(" << *j << ") to list of supervised applications");
                v_supervisedApps.push_back(*j);
                m_globalState.addApplication(*j);
            }
            CMSGEMOS_DEBUG("done");
        } // done iterating over applications in group
        CMSGEMOS_DEBUG("GEMSupervisor::init::done iterating over applications in group");
    } // done iterating over groups in zone
    CMSGEMOS_DEBUG("GEMSupervisor::init::done iterating over groups in zone");

    CMSGEMOS_DEBUG("GEMSupervisor::init::starting the monitoring");

    // borrowed from hcalSupervisor
    if (m_reportToRCMS /*&& !m_hasDoneStandardInit*/) {
        m_gemRCMSNotifier.findRcmsStateListener();
        std::string classname = m_gemRCMSNotifier.getRcmsStateListenerParameter()->bag.classname.value_;
        int instance = m_gemRCMSNotifier.getRcmsStateListenerParameter()->bag.instance.value_;
        const std::string rcmsStateListenerUrl = getApplicationContext()->getDefaultZone()->getApplicationDescriptor(classname, instance)->getContextDescriptor()->getURL();
        CMSGEMOS_INFO("RCMSStateListener found with url: " << rcmsStateListenerUrl);
    }

    m_globalState.startTimer();
}

// state transitions
void gem::supervisor::GEMSupervisor::initializeAction()
{
    CMSGEMOS_INFO("GEMSupervisor::initializeAction start");
    CMSGEMOS_DEBUG("GEMSupervisor::initializeAction:: HandleTCDS is " << m_handleTCDS);

    // moved here from constructor, this is not what i want but it's how XDAQ works with setDefaultParameters,
    // as i need to know about handleTCDS before the init function...
    v_supervisedApps.clear();
    init();

    // while ((m_gemfsm.getCurrentState()) != m_gemfsm.getStateName(gem::utils::STATE_CONFIGURING)) {  // deal with possible race condition
    while (!(m_globalState.getStateName() == "Initial" && getCurrentState() == "Initializing")) {
        CMSGEMOS_INFO("GEMSupervisor::initializeAction global state not in " << gem::utils::STATE_INITIAL
                                                                             << " sleeping (" << m_globalState.getStateName() << ","
                                                                             << getCurrentState() << ")");
        usleep(100);
        m_globalState.update();
    }

    try {
        // do this only when RCMS is not present
        // for (auto i = v_supervisedApps.begin(); i != v_supervisedApps.end(); ++i) {
        auto initorder = getInitializationOrder();
        for (auto i = initorder.begin(); i != initorder.end(); ++i) {
            // if (!m_gemRCMSNotifier.getFoundRcmsStateListenerParameter()) {
            if (true) {
                CMSGEMOS_INFO("GEMSupervisor::initializeAction No RCMS state listener found, continuing to initialize children ");
                for (auto j = i->begin(); j != i->end(); ++j) {
                    if (((*j)->getClassName()).rfind("tcds::") != std::string::npos) {
                        CMSGEMOS_INFO("GEMSupervisor::initializeAction Halting " << (*j)->getClassName()
                                                                                 << " in case it is not in 'Halted'");
                        // need to ensure leases are properly respected
                        gem::utils::soap::sendCommand("Halt", p_appContext, p_appDescriptor, *j);
                    } else if (((*j)->getClassName()).rfind("Calibration") != std::string::npos) {
                        CMSGEMOS_INFO("GEMSupervisor::initializeAction Halting  " << (*j)->getClassName()
                                                                                  << " in case it is not in 'Halted'");

                    } else {
                        CMSGEMOS_INFO("GEMSupervisor::initializeAction Initializing " << (*j)->getClassName());
                        gem::utils::soap::sendCommand("Initialize", p_appContext, p_appDescriptor, *j);
                    }
                }
            }
            // check that group state of *i has moved to desired state before continuing
            do {
                usleep(10);
                m_globalState.update();
                CMSGEMOS_DEBUG("GEMSupervisor::initializeAction waiting for group to reach Halted: "
                    << m_globalState.compositeState(*i));
            } while (m_globalState.compositeState(*i) != gem::utils::STATE_HALTED);
        }
        // why is initializeAction treated differently than the other state transitions?
        // should make this uniform, or was it due to wanting to fail on DB errors?
    } catch (gem::supervisor::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::initializeAction unable to initialize " << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::SOAPException const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::initializeAction unable to initialize " << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::initializeAction unable to initialize " << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (xcept::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::initializeAction unable to initialize " << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (std::exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::initializeAction unable to initialize " << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (...) {
        std::stringstream msg;
        msg << "GEMSupervisor::initializeAction unable to initialize";
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    }

    // SHOULD ONLY REPORT "INITIALIZED" TO RCMS HERE
    m_globalState.update();
    CMSGEMOS_INFO("GEMSupervisor::initializeAction GlobalState = " << m_globalState.getStateName()
                                                                   << " with GlobalStateMessage = " << m_globalState.getStateMessage());
}

void gem::supervisor::GEMSupervisor::configureAction()
{
    CMSGEMOS_INFO("GEMSupervisor::configureAction start");

    while (!((m_globalState.getStateName() == "Halted" && getCurrentState() == "Configuring") ||
        // (m_globalState.getStateName() == "Paused"     && getCurrentState() == "Configuring") || // FIXME do we allow this???
        (m_globalState.getStateName() == "Configured" && getCurrentState() == "Configuring"))) {
        CMSGEMOS_INFO("GEMSupervisor::configureAction global state not in " << gem::utils::STATE_HALTED
                                                                            << " or " << gem::utils::STATE_CONFIGURED
                                                                            << " sleeping (" << m_globalState.getStateName() << ","
                                                                            << getCurrentState() << ")");
        usleep(10);
        m_globalState.update();
    }

    if (m_isLocalRunNumber)
        m_runNumber = getNewLocalRunNumber();

    try {
        for (auto i = v_supervisedApps.begin(); i != v_supervisedApps.end(); ++i) {
            sendRunNumber(*i);
        }

        const std::string command = "Configure";
        auto configorder = getConfigureOrder();
        for (auto i = configorder.begin(); i != configorder.end(); ++i) {
            for (auto j = i->begin(); j != i->end(); ++j) {
                CMSGEMOS_INFO("GEMSupervisor::configureAction Configuring " << (*j)->getClassName());
                m_globalState.setGlobalStateMessage("Configuring " + (*j)->getClassName());
                if (((*j)->getClassName()).rfind("tcds::") != std::string::npos) {
                    // if (tcdsState() == gem::utils::STATE_CONFIGURED)
                    //   command = "Reconfigure";
                    // xdata::Bag<xdata::Serializable> tcdsParams;
                    std::unordered_map<std::string, xdata::Serializable*> tcdsParams;
                    std::string content;
                    if (((*j)->getClassName()).rfind("ICI") != std::string::npos) {
                        std::ifstream ifs(m_tcdsConfig.bag.iciHWConfig.toString());
                        content.assign((std::istreambuf_iterator<char>(ifs)),
                            (std::istreambuf_iterator<char>()));
                        CMSGEMOS_INFO("GEMSupervisor::configureAction ICI HW config " << m_tcdsConfig.bag.iciHWConfig.toString()
                                                                                      << " is:" << std::endl
                                                                                      << content);
                    } else if (((*j)->getClassName()).rfind("PI") != std::string::npos) {
                        std::ifstream ifs(m_tcdsConfig.bag.piHWConfig.toString());
                        content.assign((std::istreambuf_iterator<char>(ifs)),
                            (std::istreambuf_iterator<char>()));
                        // tcdsParams.addField("usePrimaryTCDS",m_tcdsConfig.bag.usePrimaryTCDS);
                        // tcdsParams.addField("fedEnableMask",m_tcdsConfig.bag.fedEnableMask);
                        if (m_tcdsConfig.bag.piSkipPLLReset)
                            tcdsParams.insert(std::make_pair("skipPLLReset", &(m_tcdsConfig.bag.piSkipPLLReset)));
                        tcdsParams.insert(std::make_pair("usePrimaryTCDS", &(m_tcdsConfig.bag.usePrimaryTCDS)));
                        tcdsParams.insert(std::make_pair("fedEnableMask", &(m_tcdsConfig.bag.fedEnableMask)));
                    } else if (((*j)->getClassName()).rfind("LPM") != std::string::npos) {
                        std::ifstream ifs(m_tcdsConfig.bag.lpmHWConfig.toString());
                        content.assign((std::istreambuf_iterator<char>(ifs)),
                            (std::istreambuf_iterator<char>()));
                        // tcdsParams.addField("fedEnableMask",m_tcdsConfig.bag.fedEnableMask);
                        tcdsParams.insert(std::make_pair("fedEnableMask", &(m_tcdsConfig.bag.fedEnableMask)));
                    } else if (((*j)->getClassName()).rfind("CPM") != std::string::npos) {
                        std::ifstream ifs(m_tcdsConfig.bag.cpmHWConfig.toString());
                        content.assign((std::istreambuf_iterator<char>(ifs)),
                            (std::istreambuf_iterator<char>()));
                        // tcdsParams.addField("fedEnableMask",m_tcdsConfig.bag.fedEnableMask);
                        // tcdsParams.addField("noBeamActive", m_tcdsConfig.bag.fedEnableMask);
                        tcdsParams.insert(std::make_pair("fedEnableMask", &(m_tcdsConfig.bag.fedEnableMask)));
                        tcdsParams.insert(std::make_pair("noBeamActive", &(m_tcdsConfig.bag.fedEnableMask)));
                    }

                    xdata::String hwConfig(content);
                    // tcdsParams.addField("hardwareConfigurationString",content);
                    tcdsParams.insert(std::make_pair("hardwareConfigurationString", &(hwConfig)));
                    gem::utils::soap::sendCommandWithParameters(command, tcdsParams, p_appContext, p_appDescriptor, *j);

                    // put a mutex around this
                    m_tcdsLock.lock();
                    CMSGEMOS_DEBUG("GEMSupervisor::configureAction adding " << (*j)->getClassName() << " to TCDS leased applications list");
                    v_leasedTCDSApps.push_back(*j);
                    m_tcdsLock.unlock();
                    // until here
                } else
                    gem::utils::soap::sendCommand(command, p_appContext, p_appDescriptor, *j);
            }
            // check that group state of *i has moved to desired state before continuing
            do {
                usleep(10);
                m_globalState.update();
                CMSGEMOS_DEBUG("GEMSupervisor::configureAction waiting for group to reach Configured: "
                    << m_globalState.compositeState(*i));
            } while (m_globalState.compositeState(*i) != gem::utils::STATE_CONFIGURED);
        }
    } catch (gem::supervisor::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::configureAction unable to configure (gem::supervisor::exception) " << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::SOAPException const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::configureAction unable to configure (gem::utils::exception::SOAPException)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::configureAction unable to configure (gem::utils::exception)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (xcept::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::configureAction unable to configure (xcept)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (std::exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::configureAction unable to configure (std)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (...) {
        std::stringstream msg;
        msg << "GEMSupervisor::configureAction unable to configure (unknown exception)";
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    }

    // SHOULD ONLY REPORT "CONFIGURED" TO RCMS HERE
    m_globalState.update();
    CMSGEMOS_INFO("GEMSupervisor::configureAction GlobalState = " << m_globalState.getStateName()
                                                                  << " with GlobalStateMessage = " << m_globalState.getStateMessage());
}

void gem::supervisor::GEMSupervisor::startAction()
{
    CMSGEMOS_INFO("GEMSupervisor::startAction start");

    while (!(m_globalState.getStateName() == "Configured" && getCurrentState() == "Starting")) {
        CMSGEMOS_INFO("GEMSupervisor::startAction global state not in " << gem::utils::STATE_CONFIGURED
                                                                        << " sleeping (" << m_globalState.getStateName() << ","
                                                                        << getCurrentState() << ")");
        usleep(10);
        m_globalState.update();
    }

    if (m_isLocalRunNumber)
        m_runNumber = getNewLocalRunNumber();

    try {
        for (auto i = v_supervisedApps.begin(); i != v_supervisedApps.end(); ++i)
            sendRunNumber(*i);

        auto startorder = getEnableOrder();
        for (auto i = startorder.begin(); i != startorder.end(); ++i) {
            for (auto j = i->begin(); j != i->end(); ++j) {
                CMSGEMOS_INFO("GEMSupervisor::startAction: starting " << (*j)->getClassName());
                if (((*j)->getClassName()).rfind("tcds::") != std::string::npos) {
                    std::unordered_map<std::string, xdata::Serializable*> tcdsParams;
                    // Do not clutter the TCDS logs with local run numbers, use the 0 catch-all value instead
                    xdata::UnsignedInteger32 tcdsRunNumber(m_isLocalRunNumber ? 0 : m_runNumber.value_);
                    tcdsParams.insert(std::make_pair("runNumber", &tcdsRunNumber));
                    CMSGEMOS_INFO("GEMSupervisor::startAction: sending TCDS application " << (*j)->getClassName()
                                                                                          << " run number: " << m_runNumber.value_
                                                                                          << " as: " << tcdsRunNumber.value_);
                    gem::utils::soap::sendCommandWithParameters("Enable", tcdsParams, p_appContext, p_appDescriptor, *j);
                } else {
                    std::string command = "Start";
                    if (((*j)->getClassName()).rfind("ferol::") != std::string::npos)
                        command = "Enable";
                    else if (((*j)->getClassName()).rfind("evb::") != std::string::npos)
                        command = "Enable";

                    gem::utils::soap::sendCommand(command, p_appContext, p_appDescriptor, *j);
                }
            }
            // check that group state of *i has moved to desired state before continuing
            do {
                usleep(10);
                m_globalState.update();
                CMSGEMOS_DEBUG("GEMSupervisor::startAction: waiting for group to reach Running: "
                    << m_globalState.compositeState(*i));
            } while (m_globalState.compositeState(*i) != gem::utils::STATE_RUNNING);
        }
    } catch (gem::supervisor::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::startAction unable to start (gem::supervisor::exception) " << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::SOAPException const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::startAction unable to start (gem::utils::exception::SOAPException)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::startAction unable to start (gem::utils::exception)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (xcept::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::startAction unable to start (xcept)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (std::exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::startAction unable to start (std)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (...) {
        std::stringstream msg;
        msg << "GEMSupervisor::startAction unable to start (unknown exception)";
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    }

    // Latency scan specifics
    if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::LATENCY)) {
        CMSGEMOS_INFO("GEMSupervisor::startAction: specifics for latency scan");

        // Stopping timer if active timer
        try {
            m_timer->stop();
        } catch (toolbox::task::exception::NotActive const& ex) {
            CMSGEMOS_INFO("GEMSUpervisor::startAction: no timer stopped" << ex.what());
        }

        // Starting timer
        m_timer->start();
        toolbox::TimeVal time = toolbox::TimeVal::gettimeofday() + toolbox::TimeVal(m_scanInfo.bag.timeInterval.value_);
        m_timer->schedule(this, time, 0, "calibrationTimer");

        m_scanParameter = m_scanInfo.bag.scanMin.value_;
        CMSGEMOS_INFO("GEMSupervisor::startAction: latency = " << m_scanInfo.bag.scanMin.value_);
    }

    // SHOULD ONLY REPORT "RUNNING" TO RCMS HERE
    m_globalState.update();
    CMSGEMOS_INFO("GEMSupervisor::startAction: GlobalState = " << m_globalState.getStateName()
                                                               << " with GlobalStateMessage = " << m_globalState.getStateMessage());
}

void gem::supervisor::GEMSupervisor::pauseAction()
{
    CMSGEMOS_INFO("GEMSupervisor::pauseAction start");

    while (!(m_globalState.getStateName() == "Running" && getCurrentState() == "Pausing")) {
        CMSGEMOS_INFO("GEMSupervisor::pauseAction global state not in " << gem::utils::STATE_RUNNING
                                                                        << " sleeping (" << m_globalState.getStateName() << ","
                                                                        << getCurrentState() << ")");
        usleep(10);
        m_globalState.update();
    }

    try {
        auto disableorder = getDisableOrder();
        for (auto i = disableorder.begin(); i != disableorder.end(); ++i) {
            for (auto j = i->begin(); j != i->end(); ++j) {
                CMSGEMOS_INFO("GEMSupervisor::pauseAction Pausing " << (*j)->getClassName());
                gem::utils::soap::sendCommand("Pause", p_appContext, p_appDescriptor, *j);
            }
            // check that group state of *i has moved to desired state before continuing
            do {
                usleep(10);
                m_globalState.update();
                CMSGEMOS_DEBUG("GEMSupervisor::pauseAction waiting for group to reach Paused: "
                    << m_globalState.compositeState(*i));
            } while (m_globalState.compositeState(*i) != gem::utils::STATE_PAUSED);
        }
    } catch (gem::supervisor::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::pauseAction unable to pause (gem::supervisor::exception) " << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::SOAPException const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::pauseAction unable to pause (gem::utils::exception::SOAPException)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::pauseAction unable to pause (gem::utils::exception)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (xcept::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::pauseAction unable to pause (xcept)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (std::exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::pauseAction unable to pause (std)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (...) {
        std::stringstream msg;
        msg << "GEMSupervisor::pauseAction unable to pause (unknown exception)";
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    }

    // SHOULD ONLY REPORT "PAUSED" TO RCMS HERE
    m_globalState.update();
    CMSGEMOS_INFO("GEMSupervisor::pauseAction GlobalState = " << m_globalState.getStateName()
                                                              << " with GlobalStateMessage = " << m_globalState.getStateMessage());
}

void gem::supervisor::GEMSupervisor::resumeAction()
{
    CMSGEMOS_INFO("GEMSupervisor::resumeAction start");

    while (!(m_globalState.getStateName() == "Paused" && getCurrentState() == "Resuming")) {
        CMSGEMOS_INFO("GEMSupervisor::pauseAction global state not in " << gem::utils::STATE_PAUSED
                                                                        << " sleeping (" << m_globalState.getStateName() << ","
                                                                        << getCurrentState() << ")");
        usleep(10);
        m_globalState.update();
    }

    try {
        auto resumeorder = getEnableOrder();
        for (auto i = resumeorder.begin(); i != resumeorder.end(); ++i) {
            for (auto j = i->begin(); j != i->end(); ++j) {
                CMSGEMOS_INFO("GEMSupervisor::resumeAction Resuming " << (*j)->getClassName());
                gem::utils::soap::sendCommand("Resume", p_appContext, p_appDescriptor, *j);
            }
            // check that group state of *i has moved to desired state before continuing
            do {
                usleep(10);
                m_globalState.update();
                CMSGEMOS_DEBUG("GEMSupervisor::resumeAction waiting for group to reach Running: "
                    << m_globalState.compositeState(*i));
            } while (m_globalState.compositeState(*i) != gem::utils::STATE_RUNNING);
        }
    } catch (gem::supervisor::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::resumeAction unable to resume (gem::supervisor::exception) " << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::SOAPException const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::resumeAction unable to resume (gem::utils::exception::SOAPException)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::resumeAction unable to resume (gem::utils::exception)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (xcept::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::resumeAction unable to resume (xcept)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (std::exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::resumeAction unable to resume (std)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (...) {
        std::stringstream msg;
        msg << "GEMSupervisor::resumeAction unable to resume (unknown exception)";
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    }

    // SHOULD ONLY REPORT "RUNNING" TO RCMS HERE
    m_globalState.update();
    CMSGEMOS_INFO("GEMSupervisor::resumeAction GlobalState = " << m_globalState.getStateName()
                                                               << " with GlobalStateMessage = " << m_globalState.getStateMessage());
}

void gem::supervisor::GEMSupervisor::stopAction()
{
    CMSGEMOS_INFO("GEMSupervisor::stopAction start");
    while (!((m_globalState.getStateName() == "Running" && getCurrentState() == "Stopping") || (m_globalState.getStateName() == "Paused" && getCurrentState() == "Stopping"))) {
        CMSGEMOS_INFO("GEMSupervisor::pauseAction global state not in " << gem::utils::STATE_RUNNING
                                                                        << " or " << gem::utils::STATE_PAUSED
                                                                        << " sleeping (" << m_globalState.getStateName() << ","
                                                                        << getCurrentState() << ")");
        usleep(10);
        m_globalState.update();
    }

    // Stop the scan timer if we are running a scan
    if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::LATENCY))
        m_timer->stop();

    try {
        auto disableorder = getDisableOrder();
        for (auto i = disableorder.begin(); i != disableorder.end(); ++i) {
            for (auto j = i->begin(); j != i->end(); ++j) {
                CMSGEMOS_INFO("GEMSupervisor::stopAction Stopping " << (*j)->getClassName());
                gem::utils::soap::sendCommand("Stop", p_appContext, p_appDescriptor, *j);
            }
            // check that group state of *i has moved to desired state before continuing
            do {
                usleep(10);
                m_globalState.update();
                CMSGEMOS_DEBUG("GEMSupervisor::stopAction waiting for group to reach Configured: "
                    << m_globalState.compositeState(*i));
            } while (m_globalState.compositeState(*i) != gem::utils::STATE_CONFIGURED);
        }
    } catch (gem::supervisor::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::stopAction unable to stop (gem::supervisor::exception) " << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::SOAPException const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::stopAction unable to stop (gem::utils::exception::SOAPException)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::stopAction unable to stop (gem::utils::exception)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (xcept::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::stopAction unable to stop (xcept)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (std::exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::stopAction unable to stop (std)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (...) {
        std::stringstream msg;
        msg << "GEMSupervisor::stopAction unable to stop (unknown exception)";
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    }

    // SHOULD ONLY REPORT "CONFIGURED" TO RCMS HERE
    m_globalState.update();
    CMSGEMOS_INFO("GEMSupervisor::stopAction GlobalState = " << m_globalState.getStateName()
                                                             << " with GlobalStateMessage = " << m_globalState.getStateMessage());
}

void gem::supervisor::GEMSupervisor::haltAction()
{
    CMSGEMOS_INFO("GEMSupervisor::haltAction start");

    try {
        auto disableorder = getDisableOrder();
        for (auto i = disableorder.begin(); i != disableorder.end(); ++i) {
            for (auto j = i->begin(); j != i->end(); ++j) {
                CMSGEMOS_INFO("GEMSupervisor::haltAction Halting " << (*j)->getClassName());
                gem::utils::soap::sendCommand("Halt", p_appContext, p_appDescriptor, *j);
            }
            // check that group state of *i has moved to desired state before continuing
            do {
                usleep(10);
                m_globalState.update();
                CMSGEMOS_DEBUG("GEMSupervisor::haltAction waiting for group to reach Halted: "
                    << m_globalState.compositeState(*i));
            } while (m_globalState.compositeState(*i) != gem::utils::STATE_HALTED);
        }
    } catch (gem::supervisor::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::haltAction unable to halt (gem::supervisor::exception) " << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::SOAPException const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::haltAction unable to halt (gem::utils::exception::SOAPException)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (gem::utils::exception::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::haltAction unable to halt (gem::utils::exception)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (xcept::Exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::haltAction unable to halt (xcept)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (std::exception const& e) {
        std::stringstream msg;
        msg << "GEMSupervisor::haltAction unable to halt (std)" << e.what();
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    } catch (...) {
        std::stringstream msg;
        msg << "GEMSupervisor::haltAction unable to halt (unknown exception)";
        CMSGEMOS_ERROR(msg.str());
        fireEvent("Fail");
        m_globalState.update();
    }

    // SHOULD ONLY REPORT "HALTED" TO RCMS HERE
    m_globalState.update();
    CMSGEMOS_INFO("GEMSupervisor::haltAction GlobalState = " << m_globalState.getStateName()
                                                             << " with GlobalStateMessage = " << m_globalState.getStateMessage());
}

bool gem::supervisor::GEMSupervisor::isGEMApplication(const std::string& classname) const
{
    if (classname.find("gem::") != std::string::npos)
        return true;

    return false;
}

bool gem::supervisor::GEMSupervisor::manageApplication(const std::string& classname) const
{
    if (classname.find("gem::managers::") != std::string::npos)
        return true; // handle the GEM manager applications

    if (classname.find("gem::readout::") != std::string::npos)
        return true; // handle the GEM readout applications

    if (classname.find("tcds::") != std::string::npos && m_handleTCDS)
        return true; // handle the TCDS applications if requested

    if ((classname.find("ferol::") != std::string::npos || classname.find("evb::") != std::string::npos)
        && m_useFedKitReadout.value_)
        return true; // handle the DAQ applications if using uFedKit readout

    return false; // assume not ok
}

void gem::supervisor::GEMSupervisor::globalStateChanged(toolbox::fsm::State before, toolbox::fsm::State after)
{
    CMSGEMOS_INFO("GEMSupervisor::globalStateChanged(" << before << "," << after << ")");

    // Notify RCMS of a state change.
    m_stateName = GEMGlobalState::getStateName(after);

    // if state is terminal only?
    // ignore Initial? (only after after Reset?)
    if (std::string("UBHCEPF").rfind(after) != std::string::npos) {
        try {
            if (m_reportToRCMS)
                CMSGEMOS_INFO("GEMSupervisor::globalStateChanged::Notifying RCMS of state change: ("
                    << before << "," << after << "), "
                    << m_globalState.getStateMessage());
            m_gemRCMSNotifier.stateChanged(GEMGlobalState::getStateName(after), "GEM global state changed: " + (m_globalState.getStateMessage()));
        } catch (xcept::Exception& e) {
            CMSGEMOS_ERROR("GEMSupervisor::globalStateChanged::Failed to notify RCMS of state change: "
                << xcept::stdformat_exception_history(e));
            XCEPT_DECLARE_NESTED(gem::utils::exception::RCMSNotificationError, top,
                "Failed to notify RCMS of state change.", e);
            notifyQualified("error", top);
        }
    }

    // ensure that the supervisor FSM goes to error if the composite state is error
    // May want to also disable the updating of the state until a reset is issued...
    if (m_stateName == "Error") {
        fireEvent("Fail");
        m_globalState.update();
    }
}

void gem::supervisor::GEMSupervisor::sendRunNumber(const xdaq::ApplicationDescriptor* ad)
{
    const auto className = ad->getClassName();
    std::unordered_map<std::string, xdata::Serializable*> runNumberParams;

    // Always forward a run number
    xdata::UnsignedInteger32 runNumber(m_runNumber.value_);
    runNumberParams.insert(std::make_pair("runNumber", &runNumber));

    if (className.find("gem::") != std::string::npos) {
        runNumberParams.insert(std::make_pair("isLocalRunNumber", &m_isLocalRunNumber));
    } else if (m_isLocalRunNumber) {
        if (!m_useFedKitReadout || (className.find("ferol::") == std::string::npos && className.find("evb::") == std::string::npos)) {
            // Forward the run number as-is only to FedKit applications
            runNumber = 0;
        }
    }

    CMSGEMOS_INFO("GEMSupervisor::sendRunNumber: sending application " << className
                                                                       << " run number: " << m_runNumber.value_
                                                                       << " as: " << runNumber);

    gem::utils::soap::sendApplicationParameters(runNumberParams, p_appContext, p_appDescriptor, ad);
}

uint64_t gem::supervisor::GEMSupervisor::getNewLocalRunNumber()
{
    const std::time_t t_now = std::time(nullptr);
    struct tm now;
    if (!gmtime_r(&t_now, &now))
        throw std::runtime_error("Could not convert epoch time into calendar time");

    uint32_t localRunNumber = 2'000'000'000; // First era
    localRunNumber += ((now.tm_year - 100) * 365 + now.tm_yday - 1) * 100'000; // Epoch on 1 January 2000 00:00:00 UTC
    localRunNumber += now.tm_hour * 3600 + now.tm_min * 60 + now.tm_sec;

    for (;; ++localRunNumber) {
        if (std::filesystem::create_directories(m_outputPath.value_ + "/run" + runNumberToString(localRunNumber, true)))
            break;
    }

    if (!(localRunNumber >> 31))
        throw std::runtime_error("New locally assigned run number without local bit set");

    CMSGEMOS_INFO("GEMSupervisor::getNewLocalRunNumber: got a new run number " << runNumberToString(localRunNumber, true));

    return localRunNumber;
}

void gem::supervisor::GEMSupervisor::renewTCDSLease()
{
    if (m_handleTCDS) {
        gem::utils::LockGuard<gem::utils::Lock> guardedLock(m_tcdsLock);
        for (auto tcdsApp = v_leasedTCDSApps.begin(); tcdsApp != v_leasedTCDSApps.end(); ++tcdsApp) {
            CMSGEMOS_DEBUG("GEMSupervisor::renewTCDSLease renewing lease for " << (*tcdsApp)->getClassName());
            gem::utils::soap::sendCommand("RenewHardwareLease", p_appContext, p_appDescriptor, *tcdsApp);
        }
    }
}

void gem::supervisor::GEMSupervisor::timeExpired(toolbox::task::TimerEvent& event)
{
    if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::LATENCY))
        CMSGEMOS_INFO("GEMSupervisor::timeExpired: latency scan ongoing");
    else
        return;

    CMSGEMOS_INFO("GEMSupervisor::timeExpired: current scan value " << m_scanParameter);

    // Move to the next scan point
    m_scanParameter += m_scanInfo.bag.stepSize.value_;

    // Implement cyclic latency scan
    if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::LATENCY)
        && m_scanInfo.bag.cyclic
        && m_scanParameter > m_scanInfo.bag.scanMax.value_)
        m_scanParameter = m_scanInfo.bag.scanMin.value_;

    // The timer expires after the period indicated in the calibration parameter timeInterval
    // then the m_scanParameter needs to be updated.
    //
    // The AMC manager is responsible to update the frontend and the run parameters to the new
    // point of the scan. Trying to update past the scanMax value will only result in marking
    // the data as bad.
    const std::string command = "updateScanValueCalib";
    try {
        std::set<std::string> groups = p_appZone->getGroupNames();
        for (auto i = groups.begin(); i != groups.end(); ++i) {
            const xdaq::ApplicationGroup* ag = p_appZone->getApplicationGroup(*i);
            std::set<const xdaq::ApplicationDescriptor*> allApps = ag->getApplicationDescriptors();
            for (auto j = allApps.begin(); j != allApps.end(); ++j) {
                std::string classname = (*j)->getClassName();
                if (((*j)->getClassName()).rfind("AMCManager") != std::string::npos) {
                    const xdaq::ApplicationDescriptor* app = *j;

                    gem::utils::soap::sendCommand(command,
                        p_appContext, p_appDescriptor, app);
                }
            }
        }
    } catch (xcept::Exception& err) {
        std::string msgBase = toolbox::toString("Failed to receive SOAP reply for command '%s'",
            command.c_str());
        CMSGEMOS_ERROR(toolbox::toString("%s: %s.", msgBase.c_str(), xcept::stdformat_exception_history(err).c_str()));
        XCEPT_DECLARE_NESTED(gem::utils::exception::SoftwareProblem,
            top, toolbox::toString("%s.", msgBase.c_str()), err);
        this->notifyQualified("error", top);

        m_globalState.update();
        XCEPT_RETHROW(xoap::exception::Exception, msgBase, err);
    }

    if (m_scanParameter <= m_scanInfo.bag.scanMax.value_) {
        CMSGEMOS_INFO("GEMSupervisor::timeExpired: updated scan value " << m_scanParameter);

        // Scheduling just one timer at fixed rate does not work because of its implementation in the toolbox.
        // In fact if one tries to stop a timer,run singularly, with the current implementation ends up in a deadlock, unless some exceptions are thrown.
        // Work arounds to that are: scheduling more than one timer with fixed rate, or re-schedule one signle cycle timer, after the previous one has expired.
        try {
            toolbox::TimeVal time = toolbox::TimeVal::gettimeofday() + toolbox::TimeVal(m_scanInfo.bag.timeInterval.value_);
            m_timer->schedule(this, time, 0, "calibrationTimer");
        } catch (toolbox::task::exception::NotActive const& ex) {
            // ²t is possible that the timer was stopped during the execution of this function, ignore the error
            CMSGEMOS_WARN("GEMSUpervisor::timeExpired: timer stopped during upate routine - not rescheduling" << ex.what());
        }
    } else {
        CMSGEMOS_INFO("GEMSupervisor::timeExpiredCalib: scan finished at " << m_scanParameter);

        // For calibration with calpulse stop when calibration ends
        if (m_scanInfo.bag.signalSourceType.value_ < 1) {
            CMSGEMOS_INFO("GEMSupervisor::timeExpired: with calpulse, fire stop to the FSM");
            fireEvent("Stop");
        }
    }
}

/////////////////////////////////////////
//* Order of transition operations*//
/*
 - Initialize - Connect to hardware, set up some defaults?
    - TCDS applications DO NOTHING as per prescription, but maybe what we call Initialize is not what they call Initialize
    - GEM applications
    - Trigger applications
      - AMC13Manager

 - Configure (same as Initialize? maybe eventually remove Initialize?)
    -
*/
// Strongly "borrowed" from HCAL
class InitCompare {
public:
    bool operator()(const xdaq::ApplicationDescriptor* a, const xdaq::ApplicationDescriptor* b) const
    {
        return initPriority(a->getClassName()) > initPriority(b->getClassName());
    }

    // higher priority -- initialize first
    static int initPriority(const std::string& classname)
    {
        int priority = 50; // default

        if (classname == "tcds::lpm::LPMController")
            priority = 42;
        else if (classname == "tcds::ici::ICIController")
            priority = 41;
        else if (classname == "tcds::pi::PIController")
            priority = 40;
        else if (classname == "gem::managers::AMC13Manager")
            priority = 30;
        else if (classname == "gem::managers::BoardManager")
            priority = 21;
        else if (classname == "gem::managers::AMCManager")
            priority = 20;
        else if (classname == "gem::readout::AMC13Readout")
            priority = 10;
        else if (classname == "ferol::FerolController")
            priority = 3;
        else if (classname == "evb::EVM")
            priority = 2;
        else if (classname == "evb::RU")
            priority = 1;
        else if (classname == "evb::BU")
            priority = 0;

        return priority;
    }
};

std::vector<std::vector<const xdaq::ApplicationDescriptor*>> gem::supervisor::GEMSupervisor::getInitializationOrder()
{
    std::multimap<int, const xdaq::ApplicationDescriptor*, std::greater<int>> tool;
    for (auto i = v_supervisedApps.begin(); i != v_supervisedApps.end(); ++i) {
        // if ((*i)->getClassName().find("tcds") != std::string::npos) {
        //   CMSGEMOS_INFO("GEMSupervisor::getInitializationOrder::Skipping " << (*i)->getClassName()
        //        << " for initialization");
        //   continue;
        // }
        CMSGEMOS_INFO("GEMSupervisor::getInitializationOrder: application "
            << (*i)->getClassName() << " has priority "
            << InitCompare::initPriority((*i)->getClassName()));
        tool.insert(std::make_pair(InitCompare::initPriority((*i)->getClassName()), *i));
    }
    std::vector<std::vector<const xdaq::ApplicationDescriptor*>> retval;
    int level = -1;
    for (auto j = tool.begin(); j != tool.end(); ++j) {
        if (j->first < 0)
            continue;
        if (j->first != level) {
            retval.push_back(std::vector<const xdaq::ApplicationDescriptor*>());
            level = j->first;
        }
        retval.back().push_back(j->second);
    }
    return retval;
}

std::vector<std::vector<const xdaq::ApplicationDescriptor*>> gem::supervisor::GEMSupervisor::getConfigureOrder()
{
    std::multimap<int, const xdaq::ApplicationDescriptor*, std::greater<int>> tool;
    for (auto i = v_supervisedApps.begin(); i != v_supervisedApps.end(); ++i) {
        CMSGEMOS_INFO("GEMSupervisor::getConfigureOrder: application "
            << (*i)->getClassName() << " has priority "
            << InitCompare::initPriority((*i)->getClassName()));
        tool.insert(std::make_pair(InitCompare::initPriority((*i)->getClassName()), *i));
    }
    std::vector<std::vector<const xdaq::ApplicationDescriptor*>> retval;
    int level = -1;
    for (auto j = tool.begin(); j != tool.end(); ++j) {
        if (j->first < 0)
            continue;
        if (j->first != level) {
            retval.push_back(std::vector<const xdaq::ApplicationDescriptor*>());
            level = j->first;
        }
        retval.back().push_back(j->second);
    }
    return retval;
}

/*
    - Start
    - GEM applications first
    - Trigger applications
      - AMC13Manager
    - TCDS applications
      - CPMController/LPMController
      - PIController next
      - ICIController (APVEController) last
*/
class EnableCompare {
public:
    bool operator()(const xdaq::ApplicationDescriptor* a, const xdaq::ApplicationDescriptor* b) const
    {
        return enablePriority(a->getClassName()) > enablePriority(b->getClassName());
    }

    // higher priority -- enable first
    static int enablePriority(const std::string& classname)
    {
        int priority = 50;

        if (classname == "evb::EVM")
            priority = 53;
        else if (classname == "evb::RU")
            priority = 52;
        else if (classname == "evb::BU")
            priority = 51;
        else if (classname == "ferol::FerolController")
            priority = 50;
        else if (classname == "gem::readout::AMC13Readout")
            priority = 40;
        else if (classname == "gem::managers::AMCManager")
            priority = 31;
        else if (classname == "gem::managers::BoardManager")
            priority = 30;
        else if (classname == "gem::managers::AMC13Manager")
            priority = 20;
        else if (classname == "tcds::ici::ICIController")
            priority = 12;
        else if (classname == "tcds::pi::PIController")
            priority = 11;
        else if (classname == "tcds::lpm::LPMController")
            priority = 10;

        return priority;
    }
};

std::vector<std::vector<const xdaq::ApplicationDescriptor*>> gem::supervisor::GEMSupervisor::getEnableOrder()
{
    std::multimap<int, const xdaq::ApplicationDescriptor*, std::greater<int>> tool;
    for (auto i = v_supervisedApps.begin(); i != v_supervisedApps.end(); ++i) {
        CMSGEMOS_INFO("GEMSupervisor::getEnableOrder: application "
            << (*i)->getClassName() << " has priority "
            << EnableCompare::enablePriority((*i)->getClassName()));
        tool.insert(std::make_pair(EnableCompare::enablePriority((*i)->getClassName()), *i));
    }
    std::vector<std::vector<const xdaq::ApplicationDescriptor*>> retval;
    int level = -1;
    for (auto j = tool.begin(); j != tool.end(); ++j) {
        if (j->first < 0)
            continue;
        if (j->first != level) {
            retval.push_back(std::vector<const xdaq::ApplicationDescriptor*>());
            level = j->first;
        }
        retval.back().push_back(j->second);
    }
    return retval;
}

/*
 - Pause
    - TCDS applications
      - CPMController/LPMController
      - ICIController (APVEController) first
      - PIController next
    - Trigger applications
      - AMC13Manager
    - GEM applications

 - Stop (same order as Pause, except Pause TCDS first, then Stop TCDS at the end)
    - Stop TCDS applications? this removes clock?
      - CPMController/LPMController

 - Resume (same as Start)

 - Halt (same order as pause)
    - TCDS applications
      - ICIController (APVEController) first
      - PIController next
*/
// Strongly "borrowed" from HCAL
class DisableCompare {
public:
    bool operator()(const xdaq::ApplicationDescriptor* a, const xdaq::ApplicationDescriptor* b) const
    {
        return disablePriority(a->getClassName()) > disablePriority(b->getClassName());
    }

    // higher priority -- disable first
    static int disablePriority(const std::string& classname)
    {
        int priority = 50;

        if (classname == "tcds::lpm::LPMController")
            priority = 42;
        else if (classname == "tcds::ici::ICIController")
            priority = 41;
        else if (classname == "tcds::pi::PIController")
            priority = 40;
        else if (classname == "gem::managers::AMC13Manager")
            priority = 30;
        else if (classname == "gem::managers::BoardManager")
            priority = 21;
        else if (classname == "gem::managers::AMCManager")
            priority = 20;
        else if (classname == "gem::readout::AMC13Readout")
            priority = 10;
        else if (classname == "evb::EVM")
            priority = 3;
        else if (classname == "evb::BU")
            priority = 2;
        else if (classname == "evb::RU")
            priority = 1;
        else if (classname == "ferol::FerolController")
            priority = 0;

        return priority;
    }
};

std::vector<std::vector<const xdaq::ApplicationDescriptor*>> gem::supervisor::GEMSupervisor::getDisableOrder()
{
    std::multimap<int, const xdaq::ApplicationDescriptor*, std::greater<int>> tool;
    for (auto i = v_supervisedApps.begin(); i != v_supervisedApps.end(); ++i) {
        CMSGEMOS_INFO("GEMSupervisor::getDisableOrder: application "
            << (*i)->getClassName() << " has priority "
            << DisableCompare::disablePriority((*i)->getClassName()));

        tool.insert(std::make_pair(DisableCompare::disablePriority((*i)->getClassName()), *i));
    }
    std::vector<std::vector<const xdaq::ApplicationDescriptor*>> retval;
    int level = -1;
    for (auto j = tool.begin(); j != tool.end(); ++j) {
        if (j->first < 0)
            continue;
        if (j->first != level) {
            retval.push_back(std::vector<const xdaq::ApplicationDescriptor*>());
            level = j->first;
        }
        retval.back().push_back(j->second);
    }
    return retval;
}
