/// @file

#include <gem/supervisor/GEMGlobalState.h>

#include <gem/supervisor/GEMSupervisor.h>
#include <gem/utils/LockGuard.h>

#include <toolbox/task/Timer.h>
#include <toolbox/task/TimerFactory.h>

#include <fmt/format.h>

gem::supervisor::GEMApplicationState::GEMApplicationState()
{
    state = gem::utils::STATE_NULL;
    progress = 1.0;
    progressWeight = 1.0;
}

gem::supervisor::GEMGlobalState::GEMGlobalState(xdaq::ApplicationContext* context, GEMSupervisor* gemSupervisor)
    : // m_globalState(gem::utils::STATE_UNINIT),
    // p_gemSupervisor(std::make_shared<GEMSupervisor>(gemSupervisor)),
    p_timer(NULL)
    , p_gemSupervisor(gemSupervisor)
    , p_appContext(context)
    , p_srcApp(gemSupervisor->getApplicationDescriptor())
    , m_globalStateName("N/A")
    , m_globalStateMessage("N/A")
    , m_globalState(gem::utils::STATE_INITIAL)
    , m_forceGlobal(gem::utils::STATE_NULL)
    , m_gemLogger(gemSupervisor->getApplicationLogger())
    , m_mutex(toolbox::BSem::FULL, true)
{
    // default constructor
}

gem::supervisor::GEMGlobalState::~GEMGlobalState()
{
    // delete p_timer;
}

void gem::supervisor::GEMGlobalState::addApplication(const xdaq::ApplicationDescriptor* app)
{
    CMSGEMOS_DEBUG("GEMGlobalState::addApplication");
    gem::utils::LockGuard<gem::utils::Lock> guardedLock(m_mutex);

    ApplicationMap::iterator i = m_states.insert(std::pair<const xdaq::ApplicationDescriptor*, GEMApplicationState>(app, GEMApplicationState())).first;

    const std::string appClass = app->getClassName();
    const std::string appURN = "urn:xdaq-application:" + appClass;
    const bool isGEMApp = (appURN.find("gem::") != std::string::npos) ? true : false;

    i->second.updateMsg = gem::utils::soap::createStateRequestMessage("app", appURN, isGEMApp);
}

void gem::supervisor::GEMGlobalState::clear()
{
    CMSGEMOS_DEBUG("GEMGlobalState::clear");
    gem::utils::LockGuard<gem::utils::Lock> guardedLock(m_mutex);
}

void gem::supervisor::GEMGlobalState::update()
{
    CMSGEMOS_DEBUG("GEMGlobalState::update");
    gem::utils::LockGuard<gem::utils::Lock> guardedLock(m_mutex);
    for (auto i = m_states.begin(); i != m_states.end(); ++i) {
        updateApplication(i->first);
    }
    toolbox::fsm::State before = m_globalState;
    calculateGlobals();
    m_globalStateName = getStateName(m_globalState);
    CMSGEMOS_DEBUG("GEMGlobalState::update before=" << before << " after=" << m_globalState);
    if (before != m_globalState) {
        p_gemSupervisor->globalStateChanged(before, m_globalState);
        setGlobalStateMessage("Reached terminal state: " + std::to_string(m_globalState));
    }
}

void gem::supervisor::GEMGlobalState::startTimer()
{
    if (!p_timer) {
        // p_timer = std::make_shared<toolbox::task::Timer>(toolbox::task::TimerFactory::getInstance()->createTimer("GEMGlobalStateTimer"));
        p_timer = toolbox::task::TimerFactory::getInstance()->createTimer("GEMGlobalStateTimer");
        toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
        toolbox::TimeInterval delta(1, 0); // how often is enough/too often?
        p_timer->activate();
        p_timer->scheduleAtFixedRate(start, this, delta, 0, "GEMGlobalStateUpdate");
    }
}

void gem::supervisor::GEMGlobalState::timeExpired(toolbox::task::TimerEvent& event)
{
    CMSGEMOS_DEBUG("GEMGlobalState::timeExpired received event:" << event.type());
    p_gemSupervisor->renewTCDSLease();
    update();
}

void gem::supervisor::GEMGlobalState::calculateGlobals()
{
    CMSGEMOS_DEBUG("GEMGlobalState::calculateGlobalState");
    toolbox::fsm::State initialGlobalState = m_globalState;
    toolbox::fsm::State tmpGlobalState = gem::utils::STATE_NULL;

    std::stringstream statesString;
    // statesString << initialGlobalState << ":";

    m_globalState = gem::utils::STATE_NULL;
    m_globalStateMessage = "";
    for (auto appState = m_states.begin(); appState != m_states.end(); ++appState) {
        const std::string appClass = appState->first->getClassName();

        // "Special" treatment for non-GEM apps : no Initial state
        if (initialGlobalState == gem::utils::STATE_INITIAL || initialGlobalState == gem::utils::STATE_INITIALIZING || initialGlobalState == gem::utils::STATE_RESETTING) {
            if (appClass.find("gem::") == std::string::npos) {
                CMSGEMOS_DEBUG("GEMGlobalState::calculateGlobalState: ignoring " << appClass
                                                                                 << " in state '" << appState->second.state << "'"
                                                                                 << " for initial global state '" << initialGlobalState << "'");
                continue;
            }
        }

        CMSGEMOS_DEBUG("GEMGlobalState::calculateGlobalState:" << appClass << ":"
                                                               << appState->first->getInstance() << " has state message '"
                                                               << appState->second.stateMessage.data() << "' and state: '"
                                                               << appState->second.state << "'");

        if (!appState->second.stateMessage.empty()) {
            m_globalStateMessage += fmt::format(FMT_STRING("({:s}:{:03d}) : {:s} "),
                appClass,
                appState->first->getInstance(),
                appState->second.stateMessage.data());
        }

        int pg = getStatePriority(tmpGlobalState);
        int pa = getStatePriority(appState->second.state);
        if (pa < pg)
            tmpGlobalState = appState->second.state;

        statesString << appState->second.state;

        CMSGEMOS_DEBUG("GEMGlobalState::calculateGlobalState: Current global state is " << tmpGlobalState
                                                                                        << " and current statesString is " << statesString.str());
    }

    // take into account the FSM state of the supervisor
    statesString << p_gemSupervisor->getCurrentFSMState();

    // now get the actual global state based on the initial state, the state string, and the tmp global state
    toolbox::fsm::State intermediateGlobalState = getProperCompositeState(initialGlobalState, tmpGlobalState, statesString.str());
    if (intermediateGlobalState == gem::utils::STATE_NULL)
        m_globalState = tmpGlobalState;
    else
        m_globalState = intermediateGlobalState;

    // account for cases where the global state was forced
    if (m_forceGlobal != gem::utils::STATE_NULL) {
        m_globalState = m_forceGlobal;
    } else { // account for cases where the supervisor FSM reported a failure
        if (p_gemSupervisor->getCurrentFSMState() == gem::utils::STATE_FAILED
            || p_gemSupervisor->getCurrentFSMState() == 'f') {
            // || m_globalFailed) {
            m_globalState = gem::utils::STATE_FAILED;
        }
    }

    // statesString << ":" << m_globalState;
    CMSGEMOS_TRACE("GEMGlobalState::calculateGlobals statesString is '"
        << initialGlobalState << ":"
        << statesString.str().data() << ":"
        << tmpGlobalState << ":"
        << intermediateGlobalState << ":"
        << m_globalState << ":"
        << p_gemSupervisor->getCurrentFSMState()
        << "'");
}

toolbox::fsm::State gem::supervisor::GEMGlobalState::getProperCompositeState(toolbox::fsm::State const& initial,
    toolbox::fsm::State const& final,
    std::string const& states)
{
    // need the failed condition here...
    if ((states.rfind(gem::utils::STATE_FAILED) != std::string::npos) && (initial != (gem::utils::STATE_RESETTING))) {
        return gem::utils::STATE_FAILED;
    } else if (initial == gem::utils::STATE_INITIALIZING || initial == gem::utils::STATE_INITIAL) {
        if ((states.rfind(gem::utils::STATE_INITIAL) != std::string::npos) && (states.rfind(gem::utils::STATE_HALTED) != std::string::npos))
            return gem::utils::STATE_INITIALIZING;
    } else if (initial == gem::utils::STATE_RESETTING || initial == gem::utils::STATE_HALTED) {
        if ((states.rfind(gem::utils::STATE_INITIAL) != std::string::npos) && (states.rfind(gem::utils::STATE_HALTED) != std::string::npos))
            return gem::utils::STATE_RESETTING;
    } else if (initial == gem::utils::STATE_CONFIGURING || initial == gem::utils::STATE_HALTED) {
        if ((states.rfind(gem::utils::STATE_CONFIGURED) != std::string::npos) && (states.rfind(gem::utils::STATE_HALTED) != std::string::npos))
            return gem::utils::STATE_CONFIGURING;
    } else if (initial == gem::utils::STATE_HALTING || initial == gem::utils::STATE_CONFIGURED) {
        if ((states.rfind(gem::utils::STATE_CONFIGURED) != std::string::npos) && (states.rfind(gem::utils::STATE_HALTED) != std::string::npos))
            return gem::utils::STATE_HALTING;
    } else if (initial == gem::utils::STATE_RESETTING || initial == gem::utils::STATE_CONFIGURED) {
        if ((states.rfind(gem::utils::STATE_CONFIGURED) != std::string::npos) && (states.rfind(gem::utils::STATE_INITIAL) != std::string::npos))
            return gem::utils::STATE_RESETTING;
    } else if (initial == gem::utils::STATE_STARTING || initial == gem::utils::STATE_CONFIGURED) {
        if ((states.rfind(gem::utils::STATE_CONFIGURED) != std::string::npos) && (states.rfind(gem::utils::STATE_RUNNING) != std::string::npos))
            return gem::utils::STATE_STARTING;
    } else if (initial == gem::utils::STATE_STOPPING || initial == gem::utils::STATE_RUNNING) {
        if ((states.rfind(gem::utils::STATE_CONFIGURED) != std::string::npos) && (states.rfind(gem::utils::STATE_RUNNING) != std::string::npos))
            return gem::utils::STATE_STOPPING;
    } else if (initial == gem::utils::STATE_PAUSING || initial == gem::utils::STATE_RUNNING) {
        if ((states.rfind(gem::utils::STATE_PAUSED) != std::string::npos) && (states.rfind(gem::utils::STATE_RUNNING) != std::string::npos))
            return gem::utils::STATE_PAUSING;
    } else if (initial == gem::utils::STATE_HALTING || initial == gem::utils::STATE_RUNNING) {
        if ((states.rfind(gem::utils::STATE_HALTED) != std::string::npos) && (states.rfind(gem::utils::STATE_RUNNING) != std::string::npos))
            return gem::utils::STATE_HALTING;
    } else if (initial == gem::utils::STATE_RESETTING || initial == gem::utils::STATE_RUNNING) {
        if ((states.rfind(gem::utils::STATE_INITIAL) != std::string::npos) && (states.rfind(gem::utils::STATE_RUNNING) != std::string::npos))
            return gem::utils::STATE_RESETTING;
    } else if (initial == gem::utils::STATE_STOPPING || initial == gem::utils::STATE_PAUSED) {
        if ((states.rfind(gem::utils::STATE_CONFIGURED) != std::string::npos) && (states.rfind(gem::utils::STATE_PAUSED) != std::string::npos))
            return gem::utils::STATE_STOPPING;
    } else if (initial == gem::utils::STATE_RESUMING || initial == gem::utils::STATE_PAUSED) {
        if ((states.rfind(gem::utils::STATE_RUNNING) != std::string::npos) && (states.rfind(gem::utils::STATE_PAUSED) != std::string::npos))
            return gem::utils::STATE_RESUMING;
    } else if (initial == gem::utils::STATE_HALTING || initial == gem::utils::STATE_PAUSED) {
        if ((states.rfind(gem::utils::STATE_HALTED) != std::string::npos) && (states.rfind(gem::utils::STATE_PAUSED) != std::string::npos))
            return gem::utils::STATE_HALTING;
    } else if (initial == gem::utils::STATE_RESETTING || initial == gem::utils::STATE_PAUSED) {
        if ((states.rfind(gem::utils::STATE_INITIAL) != std::string::npos) && (states.rfind(gem::utils::STATE_PAUSED) != std::string::npos))
            return gem::utils::STATE_RESETTING;
    } else {
        return gem::utils::STATE_NULL;
    }
    return gem::utils::STATE_NULL;
}

void gem::supervisor::GEMGlobalState::updateApplication(const xdaq::ApplicationDescriptor* app)
{
    ApplicationMap::iterator i = m_states.find(app);
    xoap::MessageReference msg = xoap::createMessage(i->second.updateMsg), answer;
    std::string nstag = "gemapp";
    std::stringstream debugstream;
    msg->writeTo(debugstream);
    try {
        // what about sending a message to a different context?
        CMSGEMOS_TRACE("GEMGlobalState::updateApplication::p_appContext " << p_appContext->getSessionId());
        // << " p_srcAppContext " << p_srcApp->getApplicationContext()->getSessionId()
        // << " appContext "      << app->getApplicationContext()->getSessionId());
        CMSGEMOS_TRACE("GEMGlobalState::updateApplication::p_appContext " << std::endl
                                                                          << p_appContext->getContextDescriptor()->getURL() << std::endl
                                                                          << " p_srcAppContext " << p_srcApp->getContextDescriptor()->getURL() << std::endl
                                                                          << " appContext " << app->getContextDescriptor()->getURL());
        answer = p_appContext->postSOAP(msg, *p_srcApp, *app);
        // answer = static_cast<xdaq::Application>(app)->getApplicationContext()->postSOAP(msg, *p_srcApp, *app);
    } catch (xoap::exception::Exception const& e) {
        CMSGEMOS_WARN("GEMGlobalState::updateApplication caught exception communicating with " << app->getClassName() << ":" << app->getInstance()
                                                                                               << ". Applcation probably crashed, setting state to FAILED"
                                                                                               << " (xoap::exception::Exception)" << e.what());
        CMSGEMOS_INFO("GEMGlobalState::updateApplication tried sending SOAP [" << debugstream.str() << "]");
        i->second.state = gem::utils::STATE_FAILED;
        i->second.stateMessage = "Communication failure, assuming state is FAILED, may mean application/executive crash.";
        return;
    } catch (xdaq::exception::Exception const& e) {
        CMSGEMOS_WARN("GEMGlobalState::updateApplication caught exception communicating with " << app->getClassName() << ":" << app->getInstance()
                                                                                               << ". Applcation probably crashed, setting state to FAILED"
                                                                                               << " (xdaq::exception::Exception)" << e.what());
        CMSGEMOS_INFO("GEMGlobalState::updateApplication tried sending SOAP [" << debugstream.str() << "]");
        i->second.state = gem::utils::STATE_FAILED;
        i->second.stateMessage = "Communication failure, assuming state is FAILED, may mean application/executive crash.";
        return;
    } catch (xcept::Exception const& e) {
        CMSGEMOS_WARN("GEMGlobalState::updateApplication caught exception communicating with " << app->getClassName() << ":" << app->getInstance()
                                                                                               << ". Applcation probably crashed, setting state to FAILED"
                                                                                               << " (xcept::Exception)" << e.what());
        CMSGEMOS_INFO("GEMGlobalState::updateApplication tried sending SOAP [" << debugstream.str() << "]");
        i->second.state = gem::utils::STATE_FAILED;
        i->second.stateMessage = "Communication failure, assuming state is FAILED, may mean application/executive crash.";
        return;
    } catch (std::exception const& e) {
        CMSGEMOS_WARN("GEMGlobalState::updateApplication caught exception communicating with " << app->getClassName() << ":" << app->getInstance()
                                                                                               << ". Applcation probably crashed, setting state to FAILED"
                                                                                               << " (std::exception)" << e.what());
        CMSGEMOS_INFO("GEMGlobalState::updateApplication tried sending SOAP [" << debugstream.str() << "]");
        i->second.state = gem::utils::STATE_FAILED;
        i->second.stateMessage = "Communication failure, assuming state is FAILED, may mean application/executive crash.";
        return;
    } catch (...) {
        CMSGEMOS_WARN("GEMGlobalState::updateApplication caught exception communicating with " << app->getClassName() << ":" << app->getInstance()
                                                                                               << ". Applcation probably crashed, setting state to FAILED");
        CMSGEMOS_INFO("GEMGlobalState::updateApplication tried sending SOAP [" << debugstream.str() << "]");
        i->second.state = gem::utils::STATE_FAILED;
        i->second.stateMessage = "Communication failure, assuming state is FAILED, may mean application/executive crash.";
        return;
    }

    const std::string appClass = app->getClassName();
    const std::string appURN = "urn:xdaq-application:" + appClass;

    // Extract the state message for GEM applications
    if (appClass.find("gem::") != std::string::npos) {
        std::string responseName = "StateMessage";
        xoap::SOAPName stateReply(responseName, nstag, appURN);

        xoap::SOAPElement props = answer->getSOAPPart().getEnvelope().getBody().getChildElements()[0].getChildElements()[0];
        std::vector<xoap::SOAPElement> basic = props.getChildElements(stateReply);
        if (basic.size() == 1) {
            std::string stateMessage = basic[0].getValue();
            CMSGEMOS_DEBUG("GEMGlobalState::updateApplication " << app->getClassName() << ":" << static_cast<int>(app->getInstance())
                                                                << " returned state message " << stateMessage);
            i->second.stateMessage = stateMessage;
        }
    }

    // Extract the application state
    const std::string responseName = (appClass.find("gem::") != std::string::npos) ? "StateName" : "stateName";
    xoap::SOAPName stateReply(responseName, nstag, appURN);

    xoap::SOAPElement props = answer->getSOAPPart().getEnvelope().getBody().getChildElements()[0].getChildElements()[0];
    std::vector<xoap::SOAPElement> basic = props.getChildElements(stateReply);
    if (basic.size() == 1) {
        std::string stateString = basic[0].getValue();
        CMSGEMOS_DEBUG("GEMGlobalState::updateApplication " << app->getClassName() << ":" << static_cast<int>(app->getInstance())
                                                            << " returned state " << stateString);

        if (!strcasecmp(stateString.data(), "Uninitialized"))
            i->second.state = gem::utils::STATE_HALTED;
        else if (!strcasecmp(stateString.data(), "Halted"))
            i->second.state = gem::utils::STATE_HALTED;
        // Special case for the Ferol and EvB applications.
        // After startup, the state name is reported as "uninitialized" even
        // if the applications themselves are in the "Halted" state.
        // Note the lower case state name.
        else if (!strcasecmp(stateString.data(), "uninitialized"))
            i->second.state = gem::utils::STATE_HALTED;
        else if (!strcasecmp(stateString.data(), "Cold-Init"))
            i->second.state = gem::utils::STATE_COLD;
        else if (!strcasecmp(stateString.data(), "Initial"))
            i->second.state = gem::utils::STATE_INITIAL;
        else if (!strcasecmp(stateString.data(), "Configured"))
            i->second.state = gem::utils::STATE_CONFIGURED;
        else if (!strcasecmp(stateString.data(), "Ready"))
            i->second.state = gem::utils::STATE_CONFIGURED;
        else if (!strcasecmp(stateString.data(), "Active"))
            i->second.state = gem::utils::STATE_RUNNING;
        else if (!strcasecmp(stateString.data(), "Draining"))
            i->second.state = gem::utils::STATE_RUNNING;
        else if (!strcasecmp(stateString.data(), "Enabled"))
            i->second.state = gem::utils::STATE_RUNNING;
        else if (!strcasecmp(stateString.data(), "Running"))
            i->second.state = gem::utils::STATE_RUNNING;
        else if (!strcasecmp(stateString.data(), "Throttled"))
            i->second.state = gem::utils::STATE_RUNNING;
        else if (!strcasecmp(stateString.data(), "Paused"))
            i->second.state = gem::utils::STATE_PAUSED;
        else if (!strcasecmp(stateString.data(), "Suspended"))
            i->second.state = gem::utils::STATE_PAUSED;

        else if (!strcasecmp(stateString.data(), "Initializing"))
            i->second.state = gem::utils::STATE_INITIALIZING;
        else if (!strcasecmp(stateString.data(), "PreConfigure"))
            i->second.state = gem::utils::STATE_CONFIGURING;
        else if (!strcasecmp(stateString.data(), "Configuring"))
            i->second.state = gem::utils::STATE_CONFIGURING;
        else if (!strcasecmp(stateString.data(), "Halting"))
            i->second.state = gem::utils::STATE_HALTING;
        else if (!strcasecmp(stateString.data(), "Pausing"))
            i->second.state = gem::utils::STATE_PAUSING;
        else if (!strcasecmp(stateString.data(), "Stopping"))
            i->second.state = gem::utils::STATE_STOPPING;
        else if (!strcasecmp(stateString.data(), "Starting"))
            i->second.state = gem::utils::STATE_STARTING;
        else if (!strcasecmp(stateString.data(), "Resuming"))
            i->second.state = gem::utils::STATE_RESUMING;
        else if (!strcasecmp(stateString.data(), "Resetting"))
            i->second.state = gem::utils::STATE_RESETTING;
        else if (!strcasecmp(stateString.data(), "Fixing"))
            i->second.state = gem::utils::STATE_FIXING;
        else if (!strcasecmp(stateString.data(), "Blocked"))
            i->second.state = gem::utils::STATE_FAILED;
        else if (!strcasecmp(stateString.data(), "Error"))
            i->second.state = gem::utils::STATE_FAILED;
        else if (!strcasecmp(stateString.data(), "Failed"))
            i->second.state = gem::utils::STATE_FAILED;
        else if (!strcasecmp(stateString.data(), "MissingData"))
            i->second.state = gem::utils::STATE_FAILED;
        else if (!strcasecmp(stateString.data(), "SyncLoss"))
            i->second.state = gem::utils::STATE_FAILED;
        else
            CMSGEMOS_WARN("GEMGlobalState::updateApplication " << app->getClassName() << ":" << static_cast<int>(app->getInstance())
                                                               << " " << stateString);
    } else {
        std::string toolInput;
        xoap::dumpTree(msg->getSOAPPart().getEnvelope().getDOMNode(), toolInput);
        std::string tool;
        xoap::dumpTree(answer->getSOAPPart().getEnvelope().getDOMNode(), tool);

        if (answer->getSOAPPart().getEnvelope().getBody().hasFault()) {
            CMSGEMOS_ERROR("SOAP fault getting state: " << std::endl
                                                        << "SOAP request:" << std::endl
                                                        << toolInput);
            CMSGEMOS_ERROR("SOAP fault getting state: " << std::endl
                                                        << "SOAP reply:" << std::endl
                                                        << answer->getSOAPPart().getEnvelope().getBody().getFault().getFaultString()
                                                        << std::endl
                                                        << tool);
        }
        CMSGEMOS_DEBUG("GEMGlobalState::updateApplication " << app->getClassName() << ":" << static_cast<int>(app->getInstance())
                                                            << std::endl
                                                            << static_cast<int>(basic.size())
                                                            << std::endl
                                                            << tool);
    }
}

toolbox::fsm::State gem::supervisor::GEMGlobalState::compositeState(std::vector<const xdaq::ApplicationDescriptor*> const& apps)
{
    gem::utils::LockGuard<gem::utils::Lock> guardedLock(m_mutex);

    toolbox::fsm::State compState = gem::utils::STATE_NULL;
    for (auto i = apps.begin(); i != apps.end(); ++i) {
        toolbox::fsm::State appState = gem::utils::STATE_NULL;
        auto app = m_states.find(*i);
        if (app != m_states.end())
            appState = app->second.state;
        if (appState == gem::utils::STATE_UNINIT && compState == gem::utils::STATE_COLD)
            continue; // ignore this priority for the compositeState

        std::string classname = app->first->getClassName().data();
        CMSGEMOS_DEBUG("GEMGlobalState::compositeState: " << classname << " is in state '" << appState
                                                          << "' for composite state '" << compState << "'");

        if (getStatePriority(appState) < getStatePriority(compState))
            compState = appState;
    }
    return compState;
}

// static functions
std::string gem::supervisor::GEMGlobalState::getStateName(toolbox::fsm::State state)
{
    switch (state) {
    case (gem::utils::STATE_UNINIT):
        return "Uninitialized";
        break;
    case (gem::utils::STATE_COLD):
        return "Cold-Init";
        break;
    case (gem::utils::STATE_INITIAL):
        return "Initial";
        break;
    case (gem::utils::STATE_HALTED):
        return "Halted";
        break;
    case (gem::utils::STATE_CONFIGURED):
        return "Configured";
        break;
    case (gem::utils::STATE_RUNNING):
        return "Running";
        break;
    case (gem::utils::STATE_PAUSED):
        return "Paused";
        break;
    case (gem::utils::STATE_FAILED):
        return "Error";
        break;
    case (gem::utils::STATE_INITIALIZING):
        return "Initializing";
        break;
    case (gem::utils::STATE_CONFIGURING):
        return "Configuring";
        break;
    case (gem::utils::STATE_HALTING):
        return "Halting";
        break;
    case (gem::utils::STATE_PAUSING):
        return "Pausing";
        break;
    case (gem::utils::STATE_STOPPING):
        return "Stopping";
        break;
    case (gem::utils::STATE_STARTING):
        return "Starting";
        break;
    case (gem::utils::STATE_RESUMING):
        return "Resuming";
        break;
    case (gem::utils::STATE_RESETTING):
        return "Resetting";
        break;
    case (gem::utils::STATE_FIXING):
        return "Fixing";
        break;
    default:
        return toolbox::toString("Unknown : %c (%d)", state, static_cast<int>(state));
    }
}

int gem::supervisor::GEMGlobalState::getStatePriority(toolbox::fsm::State state)
{
    static const toolbox::fsm::State statePriority[] = {
        gem::utils::STATE_RESETTING,
        gem::utils::STATE_FAILED,
        gem::utils::STATE_COLD,
        gem::utils::STATE_UNINIT,
        gem::utils::STATE_INITIALIZING,
        gem::utils::STATE_HALTING,
        gem::utils::STATE_CONFIGURING,
        gem::utils::STATE_STARTING,
        gem::utils::STATE_STOPPING,
        gem::utils::STATE_PAUSING,
        gem::utils::STATE_RESUMING,
        gem::utils::STATE_INITIAL,
        gem::utils::STATE_HALTED,
        gem::utils::STATE_PAUSED,
        gem::utils::STATE_FIXING,
        gem::utils::STATE_CONFIGURED,
        gem::utils::STATE_RUNNING,
        gem::utils::STATE_NULL
    };

    int i = -1;
    for (i = 0; statePriority[i] != state && statePriority[i] != gem::utils::STATE_NULL; ++i) {
    }

    log4cplus::Logger m_gemLogger(log4cplus::Logger::getInstance("GEMGlobalState"));
    CMSGEMOS_DEBUG("GEMGlobalState::getStatePriority state " << state << " has priority " << i);

    return i;
};
