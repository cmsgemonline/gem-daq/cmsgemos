/// @file

#ifndef GEM_SUPERVISOR_GEMSUPERVISOR_H
#define GEM_SUPERVISOR_GEMSUPERVISOR_H

#include <gem/supervisor/GEMGlobalState.h>
#include <gem/supervisor/GEMSupervisorWeb.h>
#include <gem/supervisor/exception/Exception.h>
#include <gem/utils/GEMFSMApplication.h>
#include <gem/utils/Lock.h>
#include <gem/utils/LockGuard.h>

#include <toolbox/task/TimerListener.h>
#include <xdaq2rc/RcmsStateNotifier.h>

#include <string>
#include <vector>

namespace gem::supervisor {

class GEMSupervisorWeb;

class GEMSupervisor : public gem::utils::GEMFSMApplication, public toolbox::task::TimerListener {
    friend class GEMSupervisorWeb;

public:
    XDAQ_INSTANTIATOR();

    GEMSupervisor(xdaq::ApplicationStub* s);

    virtual ~GEMSupervisor();

    void init();

    void actionPerformed(xdata::Event& event) override;

protected:
    // State transitions
    void initializeAction() override;
    void configureAction() override;
    void startAction() override;
    void pauseAction() override;
    void resumeAction() override;
    void stopAction() override;
    void haltAction() override;

    std::vector<const xdaq::ApplicationDescriptor*> getSupervisedAppDescriptors()
    {
        return v_supervisedApps;
    };

    friend class gem::supervisor::GEMGlobalState;

    /* getCurrentState
         * @returns std::string name of the current global state
         *
        virtual std::string getCurrentState() {
          return m_stateName.toString();
          };*/

    void timeExpired(toolbox::task::TimerEvent& event) override;

private:
    /// @param classname is the class to check to see whether it is a GEMApplication inherited application
    /// @throws
    bool isGEMApplication(const std::string& classname) const;

    /// @param classname, checks whether or not GEMSupervisor should manage (send transitions) to this application
    /// @throws
    bool manageApplication(const std::string& classname) const;

    /// @brief Sets global supervisor state and optionally sends to RCMS
    ///
    /// @param before state before the update
    /// @param after state after the update
    void globalStateChanged(toolbox::fsm::State before, toolbox::fsm::State after);

    /// @brief Update the run number of the designated application
    ///
    /// @param @c ad is the application descriptor of the application to update
    void sendRunNumber(const xdaq::ApplicationDescriptor* ad);

    /// @brief Generates a new local run number
    ///
    /// @details The run number is structured as follow:
    ///          * Epoch is set on the 1 January 2000 00:00:00 UTC
    ///          * Bit 31 is used to signal a local run number, decreasing the
    ///            conflict probability with global run numbers
    ///          * In base 10 the run number is displayed as
    ///              `E.DDDD.SSSSS`
    ///            where - E is the era
    ///                  - DDDD the day within the era
    ///                  - SSSSS the second within the day
    ///          * Would a run number be duplicated, the next run number is
    ///            assigned (until an empty run number is found)
    uint64_t getNewLocalRunNumber();

    toolbox::task::Timer* m_timer;

    struct TCDSConfig {
        xdata::String cpmHWConfig = "";
        xdata::String fedEnableMask = "";
        xdata::Boolean handleTCDS = true;
        xdata::String iciHWConfig = "";
        xdata::String lpmHWConfig = "";
        xdata::String piHWConfig = "";
        xdata::Boolean piSkipPLLReset = false;
        xdata::Boolean usePrimaryTCDS = true;

        void registerFields(xdata::Bag<TCDSConfig>* bag);
        std::string toString() const;
    };

    /// @brief Renew the hardware lease on any TCDS applications
    void renewTCDSLease();

    mutable gem::utils::Lock m_deviceLock;
    mutable gem::utils::Lock m_tcdsLock;
    std::vector<const xdaq::ApplicationDescriptor*> v_supervisedApps;
    std::vector<const xdaq::ApplicationDescriptor*> v_leasedTCDSApps;

    const xdaq::ApplicationDescriptor* readoutApp;

    std::vector<std::vector<const xdaq::ApplicationDescriptor*>> getInitializationOrder();
    std::vector<std::vector<const xdaq::ApplicationDescriptor*>> getConfigureOrder();
    std::vector<std::vector<const xdaq::ApplicationDescriptor*>> getEnableOrder();
    std::vector<std::vector<const xdaq::ApplicationDescriptor*>> getDisableOrder();

    GEMGlobalState m_globalState;

    uint32_t m_scanParameter;

    bool m_handleTCDS;
    xdata::Bag<TCDSConfig> m_tcdsConfig;
    xdata::Boolean m_useFedKitReadout;
    xdata::Boolean m_reportToRCMS;
    xdata::String m_outputPath;

    xdaq2rc::RcmsStateNotifier m_gemRCMSNotifier;
};

} // namespace gem::supervisor

#endif // GEM_SUPERVISOR_GEMSUPERVISOR_H
