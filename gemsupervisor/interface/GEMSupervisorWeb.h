/// @file

#ifndef GEM_SUPERVISOR_GEMSUPERVISORWEB_H
#define GEM_SUPERVISOR_GEMSUPERVISORWEB_H

#include <gem/utils/GEMWebApplication.h>

#include <memory>

namespace gem::supervisor {

// Forward declaration
class GEMSupervisor;

class GEMSupervisorWeb : public gem::utils::GEMWebApplication {
public:
    GEMSupervisorWeb(GEMSupervisor* application);
    virtual ~GEMSupervisorWeb();

protected:
    void webDefault(xgi::Input* in, xgi::Output* out) override;
    void controlPanel(xgi::Input* in, xgi::Output* out);

    void displayManagedStateTable(xgi::Input* in, xgi::Output* out);

    void jsonUpdate(xgi::Input* in, xgi::Output* out) override;

private:
    GEMSupervisor* p_gemSupervisor = nullptr;
    size_t level;
};

} // namespace gem::supervisor

#endif // GEM_SUPERVISOR_GEMSUPERVISORWEB_H
