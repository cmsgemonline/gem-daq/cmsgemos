Name:    cmsgemos
Epoch:   1
Version: %{?_rpm_version}%{!?_rpm_version:0}
Release: 1%{?dist}
Summary: CMS GEM Online Software
License: Unknown
URL:     https://gitlab.cern.ch/cmsgemonline/gem-daq/%{name}
Source0: cmsgemos-%{_archive_version}.tar.gz

# Profile libraries are not auto-detected
Requires: cmsos-core-b2innub
Requires: cmsos-core-executive
Requires: cmsos-core-hyperdaq
Requires: cmsos-core-i2outils
Requires: cmsos-core-ptfifo
Requires: cmsos-core-pthttp
Requires: cmsos-worksuite-jobcontrol
Requires: cmsos-worksuite-xdaq2rc

# Used by the gem-start-xdaq debug mode
Requires: gdb

%description
Online Software for the CMS GEM detectors.

%package local-readout
Summary: CMS GEM Online Software - Local readout applications
Requires: %{name} = %{?epoch:%{epoch}:}%{version}-%{release}

%description local-readout
Local readout applications using DPDK for use in the CMS GEM Online Software.

%prep
%setup -q -n cmsgemos-%{_archive_version}

%build
%cmake -DBUILD_SHARED_LIBS:BOOL=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo -S . -B _build -G Ninja
%__cmake --build _build

%install
DESTDIR=%{buildroot} %__cmake --build _build --target install

# Copy services into systemd directory
mkdir -p %{buildroot}/usr/lib/systemd/system
for service in %{buildroot}/%{_datadir}/cmsgemos/*.service; do
    cp ${service} %{buildroot}/usr/lib/systemd/system
done

%check
# Run unit tests
%__cmake --build _build --target test

%files
%{_bindir}/*
%exclude %{_bindir}/gem-dpdk
%{_libdir}/*
%{_datadir}/cmsgemos
/usr/lib/systemd/system/*
%config %{_sysconfdir}/cmsgemos
%{_docdir}/cmsgemos
%docdir %{_docdir}/cmsgemos

%files local-readout
%attr(4755, root, root) %{_bindir}/gem-dpdk

%post

%preun

%changelog
