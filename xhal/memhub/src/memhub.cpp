/// @file

#include <xhal/memhub/memhub.h>

#include <fmt/format.h>

#include <errno.h>
#include <fcntl.h>
#include <mutex>
#include <pthread.h>
#include <setjmp.h>
#include <signal.h>
#include <stdexcept>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <unistd.h>

#define REG32(baseptr, offset) (*((volatile uint32_t*)(((uint8_t*)baseptr) + (offset))))

// Helper functions

/// @brief Retrieves two file descriptors from an open UNIX domain socket @c sockfd
///
/// The first file descriptor retrieved is stored in @c lockfd while the second one
/// is stored in @c memfd
static void recvfd(int sockfd, int* lockfd, int* memfd)
{
    int fds[2] = { -1, -1 };

    struct iovec iov[1];
    char iobuf;
    iov[0].iov_base = &iobuf;
    iov[0].iov_len = 1;

    union {
        char buf[CMSG_SPACE(sizeof(fds))];
        struct cmsghdr align; // Ensure the proper alignment
    } controlbuf;

    struct msghdr msg = { 0 };
    msg.msg_iov = iov;
    msg.msg_iovlen = 1;
    msg.msg_control = controlbuf.buf;
    msg.msg_controllen = sizeof(controlbuf.buf);

    if (recvmsg(sockfd, &msg, 0) < 0)
        throw std::runtime_error("Cannot receive file descriptors from the memhub server");

    for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg); cmsg != nullptr; cmsg = CMSG_NXTHDR(&msg, cmsg)) {
        if (cmsg->cmsg_level == SOL_SOCKET && cmsg->cmsg_type == SCM_RIGHTS) {
            memcpy(fds, CMSG_DATA(cmsg), sizeof(fds));
            *lockfd = fds[0];
            *memfd = fds[1];
            return;
        }
    }

    throw std::runtime_error("No file descriptor received in the message from the memhub server");
}

/// @brief Converts a libc error number into a describing string
static inline std::string get_errno_string(int errnum)
{
    char t_errbuf[512] = { 0 };
    const char* r_errbuf = strerror_r(errnum, t_errbuf, 511);
#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && !_GNU_SOURCE
    return t_errbuf;
#else
    return r_errbuf;
#endif
}

// memory_access_error

xhal::memhub::memory_access_error::memory_access_error(const uint32_t address, const direction_t direction, const std::string& message)
    : m_address(address)
    , m_direction(direction)
    , m_message(message)
{
    m_what = fmt::format(FMT_STRING("Cannot access {:#08x} [direction = {:s}, details = {:s}]"),
        m_address, to_string(m_direction), m_message);
}

const char* xhal::memhub::memory_access_error::to_string(const direction_t& direction)
{
    if (direction == direction_t::read)
        return "read";
    else if (direction == direction_t::write)
        return "write";
    else if (direction == direction_t::unknown)
        return "unknown";
    else
        return "invalid";
}

// base_memhub

xhal::memhub::base_memhub::base_memhub(const std::string& name, uint64_t map_base, uint32_t map_size)
    : m_mbase(map_base)
    , m_msize(map_size)
{
    // Retrieve memory descriptor from memhub
    struct sockaddr_un addr;
    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path + 1, name.data(), sizeof(addr.sun_path) - 2);

    int sockfd = socket(PF_UNIX, SOCK_STREAM, 0);
    if (sockfd < 0)
        throw std::runtime_error(fmt::format(FMT_STRING("Socket creation failed: {:s}"), get_errno_string(errno)));

    if (connect(sockfd, (struct sockaddr*)&addr, sizeof(struct sockaddr_un)) != 0) {
        close(sockfd);
        throw std::runtime_error(fmt::format(FMT_STRING("Unable to connect to memhub: {:s}"), get_errno_string(errno)));
    }

    int lockfd, memfd;
    try {
        recvfd(sockfd, &lockfd, &memfd);
    } catch (const std::exception& e) {
        close(sockfd);
        throw std::runtime_error(fmt::format(FMT_STRING("Unable to retrieve memory descriptor from memhub: {:s}"), e.what()));
    }
    close(sockfd);

    // Lock
    void* mutex_addr = mmap(nullptr, sizeof(pthread_mutex_t), PROT_READ | PROT_WRITE, MAP_SHARED, lockfd, 0);
    if (mutex_addr == MAP_FAILED) {
        close(lockfd);
        close(memfd);
        throw std::runtime_error("Impossible to map the lock in memory");
    }
    close(lockfd);
    m_mutex = static_cast<pthread_mutex_t*>(mutex_addr);

    // Memory-map the registers
    m_mmap = mmap(nullptr, m_msize, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, m_mbase);
    if (m_mmap == MAP_FAILED) {
        close(memfd);
        munmap(m_mutex, sizeof(pthread_mutex_t));
        throw std::runtime_error(fmt::format(FMT_STRING("Unable to memory-map the device memory: {:s}"), get_errno_string(errno)));
    }
    close(memfd);
}

xhal::memhub::base_memhub::~base_memhub()
{
    // Memory
    munmap(m_mmap, m_msize);

    // Lock
    munmap(m_mutex, sizeof(pthread_mutex_t));
}

void xhal::memhub::base_memhub::base_memhub::lock()
{
    if ((pthread_mutex_lock(m_mutex) == EOWNERDEAD) && pthread_mutex_consistent(m_mutex))
        throw std::runtime_error("Cannot make mutex consistent");
}

void xhal::memhub::base_memhub::base_memhub::unlock()
{
    pthread_mutex_unlock(m_mutex);
}

uint32_t xhal::memhub::base_memhub::read(const uint32_t address, const uint32_t mask)
{
    if (address > m_msize - 1)
        throw memory_access_error(address, memory_access_error::direction_t::read, fmt::format(FMT_STRING("address outside of range {:#08x}-{:#08x}"), m_mbase, m_mbase + m_msize - 1));

    if ((address & 0x3) != 0)
        throw memory_access_error(address, memory_access_error::direction_t::read, "address not 32-bits aligned");

    const auto shift = ::ffsl(mask);
    if (shift == 0)
        throw memory_access_error(address, memory_access_error::direction_t::read, "mask cannot be 0x0");

    std::lock_guard<decltype(*this)> lock(*this);

    uint32_t data = read_impl(address);

    // Apply the mask
    // Note that the shift variable is starting at 1 (!)
    data = (data & mask) >> (shift - 1);

    return data;
}

void xhal::memhub::base_memhub::write(const uint32_t address, uint32_t value, const uint32_t mask)
{
    if (address > m_msize - 1)
        throw memory_access_error(address, memory_access_error::direction_t::write, fmt::format(FMT_STRING("address outside of range {:#08x}-{:#08x}"), m_mbase, m_mbase + m_msize - 1));

    if ((address & 0x3) != 0)
        throw memory_access_error(address, memory_access_error::direction_t::write, "address not 32-bits aligned");

    const auto shift = ::ffsl(mask);
    if (shift == 0)
        throw memory_access_error(address, memory_access_error::direction_t::read, "mask cannot be 0x0");

    std::lock_guard<decltype(*this)> lock(*this);

    if (mask == 0xffffffff) {
        write_impl(address, value);
    } else {
        uint32_t data = read_impl(address);

        // Apply the mask
        // Note that the shift variable is starting at 1 (!)
        data = (data & ~mask);
        value = (value << (shift - 1)) & mask;
        data = data | value;

        write_impl(address, data);
    }
}

// register_memhub

static constexpr uint32_t transaction_status_init = 0xbefe0000;
static constexpr uint32_t transaction_status_allok = 0xbefe600d;
static constexpr uint32_t transaction_status_ipb_err = 0xbefebad0;
static constexpr uint32_t transaction_status_axi_timeout = 0xbefebad1;
static constexpr uint32_t transaction_status_axi_addr_err = 0xbefebad2;

std::string transaction_status_to_string(uint32_t status)
{
    switch (status) {
    case transaction_status_init:
        return "initial (was a slow-control transaction ever performed?)";
    case transaction_status_allok:
        return "all ok";
    case transaction_status_ipb_err:
        return "IPBus slave error";
    case transaction_status_axi_timeout:
        return "AXI timeout (IPBus slave did not answer in time)";
    case transaction_status_axi_addr_err:
        return "AXI address error (no known IPBus slave at this address)";
    default:
        return "unknown transaction status (is the back-end FPGA programmed?)";
    }
}

xhal::memhub::register_memhub::register_memhub(const std::string& name, const uint64_t map_base, const uint32_t map_size)
    : base_memhub(name, map_base, map_size)
    , transaction_status_address(map_size - 4 /* last 32-bits register */)
{
}

uint32_t xhal::memhub::register_memhub::read_impl(const uint32_t address)
{
    const uint32_t value = REG32(memory(), address);

    const uint32_t transaction_status = REG32(memory(), transaction_status_address);
    if (transaction_status != transaction_status_allok)
        throw memory_access_error(address, memory_access_error::direction_t::read, transaction_status_to_string(transaction_status));

    return value;
}

void xhal::memhub::register_memhub::write_impl(const uint32_t address, const uint32_t value)
{
    REG32(memory(), address) = value;

    const uint32_t transaction_status = REG32(memory(), transaction_status_address);
    if (transaction_status != transaction_status_allok)
        throw memory_access_error(address, memory_access_error::direction_t::write, transaction_status_to_string(transaction_status));
}

// sigbus_memhub

static sigjmp_buf sj_env; ///< Stack environment buffer. Used to recover upon SIGBUS errors during register accesses.

/// @brief SIGBUS signal handler jumping back to the last saved stack environment.
static void sigbushdl(int sig, siginfo_t* siginfo, void* ptr)
{
    siglongjmp(sj_env, 1);
}

xhal::memhub::sigbus_memhub::sigbus_memhub(const std::string& name, const uint64_t map_base, const uint32_t map_size)
    : base_memhub(name, map_base, map_size)
{
    struct sigaction act;
    memset(&act, 0, sizeof(act));
    act.sa_sigaction = &sigbushdl;
    act.sa_flags = SA_SIGINFO;
    if (sigaction(SIGBUS, &act, 0) < 0) {
        throw std::runtime_error(fmt::format(FMT_STRING("Unable to register SIGBUS signal handler: {:s}"), get_errno_string(errno)));
    }
}

uint32_t xhal::memhub::sigbus_memhub::read_impl(const uint32_t address)
{
    if (sigsetjmp(sj_env, 1))
        throw memory_access_error(address, memory_access_error::direction_t::read, "bus error");
    else
        return REG32(memory(), address);
}

void xhal::memhub::sigbus_memhub::write_impl(const uint32_t address, const uint32_t value)
{
    if (sigsetjmp(sj_env, 1))
        throw memory_access_error(address, memory_access_error::direction_t::write, "bus error");
    else
        REG32(memory(), address) = value;
}
