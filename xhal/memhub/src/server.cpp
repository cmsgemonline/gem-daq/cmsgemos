/// @file

#include <fmt/format.h>
#include <fmt/ostream.h>

#include <CLI/CLI.hpp>
#include <fcntl.h>
#include <iostream>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <unistd.h>

int main(int argc, const char* argv[])
{
    // Define the options
    std::string device_path;
    std::string device_name { "memhub-card0-fpga0" };

    // Parse the command line
    {
        CLI::App app { "memhub server application\n" };
        app.option_defaults()->always_capture_default();
        app.add_option("device-path", device_path, "Path to the device to send to client")->required();
        app.add_option("device-name", device_name, "Name of the socket/device on which to listen");
        CLI11_PARSE(app, argc, argv);
    }

    // Open memory device
    int memfd = -1;
    {
        memfd = open(device_path.data(), O_RDWR | O_SYNC);

        if (memfd < 0) {
            fmt::print(std::cout, FMT_STRING("Cannot open device: {:s}\n"), device_path);
            exit(1);
        }
    }

    // Create the lock
    int lockfd = -1;
    {
        // Open some shared memory
        int index = 0;
        std::string lock_name;
        do {
            lock_name = "memhub-" + std::to_string(index);
            lockfd = shm_open(lock_name.data(), O_RDWR | O_CREAT | O_EXCL, 0);
        } while ((lockfd < 0) && (errno == EEXIST) && (index < 10));

        if (lockfd < 0) {
            fmt::print(std::cerr, FMT_STRING("Impossible to create shared memory for the lock\n"));
            exit(1);
        }

        // We have the memory, doesn't have to be named anymore
        shm_unlink(lock_name.data());

        // Set the size of the object
        if (ftruncate(lockfd, sizeof(pthread_mutex_t))) {
            fmt::print(std::cerr, FMT_STRING("Cannot assign enough memory for the lock\n"));
            exit(1);
        }

        // Expose the object in the address space
        void* mutex_addr = mmap(nullptr, sizeof(pthread_mutex_t), PROT_READ | PROT_WRITE, MAP_SHARED, lockfd, 0);
        if (mutex_addr == MAP_FAILED) {
            fmt::print(std::cerr, FMT_STRING("Cannot map the lock in memory\n"));
            exit(1);
        }
        pthread_mutex_t* mutex = reinterpret_cast<pthread_mutex_t*>(mutex_addr);

        // Create the mutex
        pthread_mutexattr_t attr_mutex;
        pthread_mutexattr_init(&attr_mutex);
        pthread_mutexattr_setpshared(&attr_mutex, PTHREAD_PROCESS_SHARED);
        pthread_mutexattr_setrobust(&attr_mutex, PTHREAD_MUTEX_ROBUST);
        pthread_mutexattr_settype(&attr_mutex, PTHREAD_MUTEX_RECURSIVE);

        if (pthread_mutex_init(mutex, &attr_mutex)) {
            fmt::print(std::cerr, FMT_STRING("Impossible to create the mutex for the lock\n"));
            exit(1);
        }

        munmap(mutex, sizeof(pthread_mutex_t));
    }

    // Create socket
    int sockfd = -1;
    {
        struct sockaddr_un addr;
        memset(&addr, 0, sizeof(struct sockaddr_un));
        addr.sun_family = AF_UNIX;
        strncpy(addr.sun_path + 1, device_name.data(), sizeof(addr.sun_path) - 2);

        sockfd = socket(PF_UNIX, SOCK_STREAM, 0);
        if (bind(sockfd, (struct sockaddr*)&addr, sizeof(struct sockaddr_un)) != 0) {
            fmt::print(std::cerr, FMT_STRING("Cannot bind socket\n"));
            exit(1);
        }

        if (listen(sockfd, 5) != 0) {
            fmt::print(std::cerr, FMT_STRING("Cannot make socket listen\n"));
            exit(1);
        }
    }

    fmt::print(std::cout, FMT_STRING("Listenning on socket...\n"));

    for (;;) {
        int clientfd = accept(sockfd, nullptr, nullptr);

        // Send file descriptors to client
        {
            int fds[2] = { lockfd, memfd };

            struct iovec iov[1];
            char iobuf;
            iov[0].iov_base = &iobuf;
            iov[0].iov_len = 1;

            union {
                char buf[CMSG_SPACE(sizeof(fds))];
                struct cmsghdr align; // Ensure the proper alignment
            } controlbuf;

            struct msghdr msg = { 0 };
            msg.msg_iov = iov;
            msg.msg_iovlen = 1;
            msg.msg_control = controlbuf.buf;
            msg.msg_controllen = sizeof(controlbuf.buf);

            struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
            cmsg->cmsg_level = SOL_SOCKET;
            cmsg->cmsg_type = SCM_RIGHTS;
            cmsg->cmsg_len = CMSG_LEN(sizeof(fds));
            memcpy(CMSG_DATA(cmsg), fds, sizeof(fds));

            if (sendmsg(clientfd, &msg, 0) < 0)
                fmt::print(std::cout, FMT_STRING("Impossible send file descriptors to client\n"));
            else
                fmt::print(std::cout, FMT_STRING("File descriptors sent to client\n"));
        }

        close(clientfd);
    }
}
