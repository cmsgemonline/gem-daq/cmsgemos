/// @file

#ifndef XHAL_MEMHUB_MEMHUB_H
#define XHAL_MEMHUB_MEMHUB_H

#include <exception>
#include <pthread.h>
#include <stdint.h>
#include <string>

namespace xhal {
namespace memhub {

    /// @brief Exception representing an error accessing memory-mapped address
    class memory_access_error : public std::exception {
    public:
        /// @brief Representing the direction of a memory transaction
        enum class direction_t {
            read, ///< Read transaction
            unknown, ///< Undetermined transaction direction, useful as default
            write, ///< Write transaction
        };

        /// @brief Converts a direction into a human readable string
        static const char* to_string(const direction_t& direction);

        /// @brief Constructor
        ///
        /// @param @c address on which the transaction failed
        /// @param @c direction of the transaction
        /// @param @c message additional informational message
        explicit memory_access_error(uint32_t address, direction_t direction = direction_t::unknown, const std::string& message = "");

        /// @brief Returns the address for which the error occurred
        uint32_t address() const noexcept { return m_address; }

        /// @brief Returns @c true if the operation was a read access
        bool is_read() const noexcept { return m_direction == direction_t::read ? true : false; }

        /// @brief Returns @c true if the operation was a write access
        bool is_write() const noexcept { return m_direction == direction_t::write ? true : false; }

        /// @brief Description of the error
        const char* what() const noexcept override { return m_what.data(); }

    protected:
        uint32_t m_address = 0; ///< Address for which the access failed
        direction_t m_direction = direction_t::unknown; ///< Direction of the transaction
        std::string m_message = ""; ///< Original text message

        std::string m_what = ""; ///< Complete error message
    };

    /// @brief Provides access to memory-mapped registers
    ///
    /// This is an abstract class taking care of the higher level aspects, the
    /// interaction with the memhub server,... The register-access implementation
    /// and transaction error checks must be implemented in a derived class.
    class base_memhub {
    public:
        /// @brief Default constructor
        base_memhub(const std::string& name, uint64_t map_base, uint32_t map_size);

        /// @brief Forbid copy construction
        base_memhub(const base_memhub&) = delete;

        /// @brief Forbid copy assignment
        base_memhub& operator=(const base_memhub&) = delete;

        /// @brief Destructor
        virtual ~base_memhub();

        /// @brief Locks the device from other concurrent accesses
        void lock();

        /// @brief Unlocks the device from a previous lock
        void unlock();

        /// @brief Performs a masked register read
        ///
        /// @param @c address Address of the register
        /// @param @c mask Mask of the register
        ///
        /// @returns Value of the register
        uint32_t read(uint32_t address, uint32_t mask = 0xffffffff);

        /// @brief Performs a masked register write
        ///
        /// @param @c address Address of the register
        /// @param @c value Value to write to the register
        /// @param @c mask Mask of the register
        void write(uint32_t address, uint32_t value, uint32_t mask = 0xffffffff);

    protected:
        /// @brief Returns a pointer to the mapped memory
        void* memory() { return m_mmap; };

        /// @brief Low-level read implementation
        virtual uint32_t read_impl(uint32_t address) = 0;

        /// @brief Low-level write implementation
        virtual void write_impl(uint32_t address, uint32_t value) = 0;

    private:
        pthread_mutex_t* m_mutex = nullptr; ///< Shared robust mutex used to prevent concurrent register accesses
        uint64_t m_mbase = 0; ///< Base address of the mapped memory
        uint32_t m_msize = 0; ///< Size of the mapped memory
        void* m_mmap = nullptr; ///< Pointer to the mapped memory
    };

    /// @brief memhub implementation checking for transaction errors with a magic register
    ///
    /// After every register access, the last register in the mapped address space is read and
    /// its value checked to ensure no transaction error occurred during the last register
    /// access. In case of error, the specific kind of error is presented.
    class register_memhub : public base_memhub {
    public:
        register_memhub(const std::string& name, uint64_t map_base, uint32_t map_size);

    protected:
        uint32_t read_impl(uint32_t address) override;
        void write_impl(uint32_t address, uint32_t value) override;

        const uint32_t transaction_status_address = 0;
    };

    /// @brief memhub implementation checking for transaction errors with the SIGBUS signal
    class sigbus_memhub : public base_memhub {
    public:
        sigbus_memhub(const std::string& name, uint64_t map_base, uint32_t map_size);

    protected:
        uint32_t read_impl(uint32_t address) override;
        void write_impl(uint32_t address, uint32_t value) override;
    };

} // namespace xhal::memhub
} // namespace xhal

#endif // XHAL_MEMHUB_MEMHUB_H
