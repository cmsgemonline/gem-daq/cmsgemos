/// @file

#include <xhal/client/XHALInterface.h>

#include <regex>
#include <string>

xhal::client::XHALInterface::XHALInterface(const std::string& board_url, log4cplus::Logger& logger)
    : m_board_url(board_url)
    , m_logger(logger)
{
    // Split the URL into hostname and port
    std::regex re("([^:]+)(?::([0-9]+))?");
    std::smatch m;

    if (!std::regex_match(board_url, m, re))
        throw std::runtime_error("Invalid board URL: " + board_url);

    // First capture group -> hostname
    const std::string board_hostname = m[1].str();

    // Second capture group -> port
    short board_port = 9811;
    if (m[2].matched)
        board_port = std::stoi(m[2].str());

    try {
        m_rpc.connect(board_hostname, board_port);
        XHAL_INFO("Connected to " << m_board_url);
    } catch (wisc::RPCSvc::ConnectionFailedException& e) {
        XHAL_ERROR("Caught RPCErrorException: " << e.message.data());
        throw xhal::client::RPCException("RPC ConnectionFailedException: " + e.message);
    } catch (wisc::RPCSvc::RPCException& e) {
        XHAL_ERROR("Caught exception: " << e.message.data());
        throw xhal::client::RPCException("RPC exception: " + e.message);
    }
}

xhal::client::XHALInterface::~XHALInterface()
{
    try {
        m_rpc.disconnect();
        XHAL_INFO("Disconnected from " << m_board_url);
    } catch (wisc::RPCSvc::NotConnectedException& e) {
        XHAL_INFO("Caught RPCNotConnectedException: " << e.message.data());
    } catch (wisc::RPCSvc::RPCException& e) {
        XHAL_INFO("Caught exception: " << e.message.data());
    }
}
