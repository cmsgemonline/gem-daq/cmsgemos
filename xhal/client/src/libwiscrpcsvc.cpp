/// @file

#include <xhal/client/wiscrpcsvc.h>

#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// <zlib.h>
extern "C" {
extern unsigned long crc32(unsigned long crc, const unsigned char* buf, unsigned int len);
};
// </zlib.h>

using namespace wisc;

#define PROTOVER 3
#define MIN_PROTOVER 3

#undef DEBUG_LIBRPCSVC
#ifdef DEBUG_LIBRPCSVC
#include <stdio.h>
#define dprintf(...) printf(__VA_ARGS__)
#else
#define dprintf(...)
#endif

#define COMMAND_TIMEOUT 600 // Allow a 10 minute timeout for the response execution and first return

namespace /* anonymous */ {

ssize_t timeout_recv(int fd, void* buf, size_t count, int timeout_seconds = 30)
{
    fd_set fds;
    size_t bytesread = 0;

    while (bytesread < count) {
        FD_ZERO(&fds);
        FD_SET(fd, &fds);

        struct timeval tvtimeout;
        tvtimeout.tv_sec = timeout_seconds;
        tvtimeout.tv_usec = 0;

        int rv = select(fd + 1, &fds, NULL, NULL, &tvtimeout);

        if (rv < 0) { // Error
            if (errno == EINTR) // Interrupted by signal
                continue;
            return -1;
        } else if (rv == 0) // Timeout
            return bytesread;

        rv = recv(fd, reinterpret_cast<uint8_t*>(buf) + bytesread, count - bytesread, MSG_DONTWAIT);

        if (rv < 0) { // Error
            if (errno == EINTR || errno == EAGAIN || errno == EWOULDBLOCK) // Interrupted by signal or no data available
                continue;
            return bytesread;
        } else if (rv == 0) // EOF
            return bytesread; // Connection closed

        bytesread += rv;
    }

    return bytesread;
}

ssize_t timeout_send(int fd, const void* buf, size_t count, int timeout_seconds = 30)
{
    fd_set fds;
    size_t byteswritten = 0;

    while (byteswritten < count) {
        FD_ZERO(&fds);
        FD_SET(fd, &fds);

        struct timeval tvtimeout;
        tvtimeout.tv_sec = timeout_seconds;
        tvtimeout.tv_usec = 0;

        int rv = select(fd + 1, NULL, &fds, NULL, &tvtimeout);

        if (rv < 0) { // Error
            if (errno == EINTR) // Interrupted by signal
                continue;
            return -1;
        } else if (rv == 0) // Timeout
            return byteswritten;

        rv = send(fd, reinterpret_cast<const uint8_t*>(buf) + byteswritten, count - byteswritten, MSG_DONTWAIT);

        if (rv < 0) { // Error
            if (errno == EINTR || errno == EAGAIN || errno == EWOULDBLOCK) // Interrupted by signal or no data available
                continue;
            return byteswritten;
        } else if (rv == 0)
            return byteswritten; // Should not happen...

        byteswritten += rv;
    }

    return byteswritten;
}

std::string stdstrerror(int en)
{
    char err[512];
    return std::string(strerror_r(en, err, 512));
}

} // anonymous namespace

void RPCSvc::connect(const std::string& host, const uint16_t port)
{
    // Resolve host
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_flags = AI_NUMERICSERV; // Do not lookup port
    hints.ai_family = AF_INET; // Server is IPv4-only
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    struct addrinfo* results = nullptr;
    int error = getaddrinfo(host.data(), std::to_string(port).data(), &hints, &results);
    if (error != 0)
        throw ConnectionFailedException(std::string("Unable to resolve host: ") + gai_strerror(error));

    // Elect only the first lookup result as server information.
    //
    // While all addresses could be tried, the constraints imposed by the hints
    // should not lead to many options. Likely only multiple IPv4 addresses, if
    // present.
    struct addrinfo* serverinfo = results;

    this->fd = socket(serverinfo->ai_family, serverinfo->ai_socktype, serverinfo->ai_protocol);
    if (this->fd < 0) {
        freeaddrinfo(results);
        throw ConnectionFailedException(std::string("Unable to create socket: ") + stdstrerror(errno));
    }

    if (::connect(this->fd, serverinfo->ai_addr, serverinfo->ai_addrlen) < 0) {
        int err = errno;
        close(this->fd);
        this->fd = -1;
        freeaddrinfo(results);
        throw ConnectionFailedException(std::string("Unable to connect: ") + stdstrerror(err));
    }

    freeaddrinfo(results);

    uint32_t protover;
    if (timeout_recv(this->fd, &protover, 4) != 4) {
        int err = errno;
        close(this->fd);
        this->fd = -1;
        if (errno)
            throw ConnectionFailedException(std::string("Unable to read protocol version from server: ") + stdstrerror(err));
        else
            throw ConnectionFailedException(std::string("Unable to read protocol version from server: Connection closed."));
    }
    dprintf("Got ver %u\n", htonl(protover));
    if (ntohl(protover) < MIN_PROTOVER) {
        close(this->fd);
        this->fd = -1;
        throw ConnectionFailedException(std::string("Server protocol version too old"));
    }

    protover = htonl(PROTOVER);
    if (timeout_send(this->fd, &protover, 4) != 4) {
        int err = errno;
        close(this->fd);
        this->fd = -1;
        throw ConnectionFailedException(std::string("Unable to write protocol version to server: ") + stdstrerror(err));
    }
    dprintf("Sent ver %u\n", PROTOVER);
}

void RPCSvc::disconnect()
{
    close(this->fd);
    this->fd = -1;
}

std::string RPCSvc::call_method(const std::string& reqmsg)
{
    /* RPC Protocol Communications
     *
     * Initial Handshake:
     * Recv: uint32_t server_protocol_version; // network byte order
     * Hang up if incompatible.
     * Send: uint32_t client_protocol_version; // network byte order
     *
     * Runtime:
     * Send: uint32_t msglen_bytes; // network byte order
     * Send: uint32_t msg_crc32; // network byte order
     * Send: uint8_t[] serialized_rpcmsg;
     * Recv: uint32_t msglen_bytes; // network byte order
     * Recv: uint32_t msg_crc32; // network byte order
     * Recv: uint8_t[] serialized_rpcmsg;
     */

    if (this->fd < 0)
        throw NotConnectedException("Not connected to rpc service");

    const uint32_t reqlen = htonl(reqmsg.size());
    if (timeout_send(this->fd, &reqlen, 4) != 4) {
        close(this->fd);
        this->fd = -1;
        throw RPCException("Unable to write request length to server within allocated timeout");
    }
    dprintf("Wrote length %u\n", ntohl(reqlen));

    const uint32_t reqcrc = htonl(crc32(0, reinterpret_cast<const unsigned char*>(reqmsg.data()), reqmsg.size()));
    if (timeout_send(this->fd, &reqcrc, 4) != 4) {
        close(this->fd);
        this->fd = -1;
        throw RPCException("Unable to write request CRC32 to server within allocated time");
    }
    dprintf("Wrote crc32 0x%08x\n", ntohl(reqcrc));

    uint32_t bytes_written = 0;
    while (bytes_written < reqmsg.size()) {
        int rv = timeout_send(this->fd, reqmsg.data() + bytes_written, reqmsg.size() - bytes_written);
        if (rv < 0) {
            close(this->fd);
            this->fd = -1;
            throw RPCException("Error writing response message to server within allocated time");
        }
        bytes_written += rv;
    }
    dprintf("Wrote %u request bytes\n", bytes_written);

    uint32_t rsplen;
    if (timeout_recv(this->fd, &rsplen, 4, COMMAND_TIMEOUT) != 4) {
        close(this->fd);
        this->fd = -1;
        throw RPCException("Unable to read response length from server within allocated time");
    }
    rsplen = ntohl(rsplen);
    dprintf("Read rsplen %u\n", rsplen);

    uint32_t rspcrc;
    if (timeout_recv(this->fd, &rspcrc, 4) != 4) {
        close(this->fd);
        this->fd = -1;
        throw RPCException("Unable to read response CRC32 from server within allocated time");
    }
    rspcrc = ntohl(rspcrc);
    dprintf("Read rspcrc 0x%08x\n", rspcrc);

    uint32_t bytesread = 0;
    std::string rspmsg(rsplen, '\0');
    while (bytesread < rsplen) {
        int rv = timeout_recv(this->fd, rspmsg.data() + bytesread, rsplen - bytesread);
        if (rv < 0) {
            close(this->fd);
            this->fd = -1;
            throw RPCException("Error reading response message from server within allocated time");
        }
        bytesread += rv;
    }
    dprintf("Read %u response bytes\n", bytesread);

    if (rspcrc != crc32(0, reinterpret_cast<const unsigned char*>(rspmsg.data()), rsplen)) {
        close(this->fd);
        this->fd = -1;
        throw RPCException("Invalid request CRC32 from client");
    }

    return rspmsg;
}

RPCSvc::~RPCSvc()
{
    this->disconnect();
}
