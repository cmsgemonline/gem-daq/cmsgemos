/// @file

#ifndef XHAL_CLIENT_WISCRPCSVC_H
#define XHAL_CLIENT_WISCRPCSVC_H

#include <stdint.h>
#include <stdlib.h>
#include <string>

namespace wisc {

class RPCSvc {
protected:
    int fd;

public:
    /* Exceptions:
     *
     * RPCException                - parent class for all RPCSvc exceptions
     *   NotConnectedException     - the RPCSvc connection is not open
     *   ConnectionFailedException - unable to connect
     *   RPCErrorException         - a RPC error occured
     */
    class RPCException {
    public:
        std::string message;
        RPCException(std::string message)
            : message(message) {};
    };
#define __LIBRPCSVC_EXCEPTION(e, p) \
    class e : public p {            \
    public:                         \
        e(std::string message)      \
            : p(message) {};        \
    }
    __LIBRPCSVC_EXCEPTION(NotConnectedException, RPCException);
    __LIBRPCSVC_EXCEPTION(ConnectionFailedException, RPCException);
    __LIBRPCSVC_EXCEPTION(RPCErrorException, RPCException);
#undef __LIBRPCSVC_EXCEPTION

    RPCSvc()
        : fd(-1) {};
    void connect(const std::string& host, uint16_t port);
    void connect(const std::string& host) { this->connect(host, 9811); };
    void disconnect();
    std::string call_method(const std::string& reqmsg);
    ~RPCSvc();

private:
    RPCSvc(const RPCSvc& other) { abort(); }; // Unsupported.  Pass by reference.
    RPCSvc& operator=(const RPCSvc& other) { abort(); } // Unsupported.  Pass by reference.
};

} // namespace wisc

#endif // XHAL_CLIENT_WISCRPCSVC_H
