/// @file

#ifndef XHAL_CLIENT_XHALINTERFACE_H
#define XHAL_CLIENT_XHALINTERFACE_H

#include <xhal/client/call.h>
#include <xhal/client/wiscrpcsvc.h>

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <exception>
#include <mutex>
#include <string>

#define XHAL_TRACE(MSG) LOG4CPLUS_TRACE(m_logger, MSG)
#define XHAL_DEBUG(MSG) LOG4CPLUS_DEBUG(m_logger, MSG)
#define XHAL_INFO(MSG) LOG4CPLUS_INFO(m_logger, MSG)
#define XHAL_WARN(MSG) LOG4CPLUS_WARN(m_logger, MSG)
#define XHAL_ERROR(MSG) LOG4CPLUS_ERROR(m_logger, MSG)
#define XHAL_FATAL(MSG) LOG4CPLUS_FATAL(m_logger, MSG)

namespace xhal {
namespace client {

    class RPCException : public std::exception {
    public:
        RPCException() = delete;

        RPCException(std::string message)
            : msg(message)
        {
        }

        virtual ~RPCException() {}

        virtual const char* what() const noexcept(true) override
        {
            return msg.data();
        }

    protected:
        std::string msg;
    };

    /// @brief Provides interface to perform remote procedure calls
    class XHALInterface {
    public:
        /// @brief Constructor
        ///
        /// @param @c board_url URL of the backend board to connect to
        /// @param @c logger Logger to be used
        XHALInterface(const std::string& board_url, log4cplus::Logger& logger);

        virtual ~XHALInterface();

        /// @brief Call a remote RPC method
        ///
        /// The remote method called is defined by the template parameter
        /// @c Method. The arguments to give to the function are those from
        /// the @c Method::operator() signature and their types @b must be deducible.
        template <typename Method,
            typename... Args,
            typename std::enable_if<std::is_base_of<xhal::common::Method, Method>::value, int>::type = 0>
        common::helper::functor_return_t<Method> call(Args&&... args)
        {
            std::scoped_lock t_rpc_lock(m_rpc_mutex);
            return xhal::client::call<Method>(m_rpc, std::forward<Args>(args)...);
        }

    protected:
        log4cplus::Logger m_logger;
        std::string m_board_url;
        std::mutex m_rpc_mutex;
        wisc::RPCSvc m_rpc;
    };

}
}

#endif // XHAL_CLIENT_XHALINTERFACE_H
