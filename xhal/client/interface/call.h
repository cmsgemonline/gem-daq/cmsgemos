/// @file
/// @brief This file contains all the functions needed to call remotely a RPC method

#ifndef XHAL_CLIENT_CALL_H
#define XHAL_CLIENT_CALL_H

#include <xhal/client/wiscrpcsvc.h>
#include <xhal/common/common.h>
#include <xhal/common/compat.h>
#include <xhal/common/exceptions.h>
#include <xhal/common/helper.h>

#include <cereal/archives/portable_binary.hpp>

#include <sstream>
#include <typeinfo>

namespace xhal {
namespace client {

    /// @brief Remotely call a RPC method
    ///
    /// The remote method called is defined by the template parameter
    /// @c Method. The arguments to give to the function are those from
    /// the @c Method::operator() signature and their types @b must be deducible.
    template <typename Method,
        typename... Args,
        typename std::enable_if<std::is_base_of<xhal::common::Method, Method>::value, int>::type = 0>
    common::helper::functor_return_t<Method> call(wisc::RPCSvc& connection, Args&&... args);

    /// @brief Thrown by @ref call when an exception is thrown on the remote host.
    class RemoteException : public std::runtime_error {
        std::string m_type;

        /// @brief @ref call is the only function that can throw this exception.
        template <typename Method,
            typename... Args,
            typename std::enable_if<std::is_base_of<xhal::common::Method, Method>::value, int>::type>
        friend common::helper::functor_return_t<Method> call(wisc::RPCSvc& connection,
            Args&&... args);

        /// @brief Constructor.
        ///
        /// @param @c type The remote exception type.
        /// @param @c message The remote exception user message.
        explicit RemoteException(const std::string& type, const std::string& message)
            : std::runtime_error(makeUserMessage(type, message))
            , m_type(type)
        {
        }

        /// @brief Returns a user-friendly error message from remote exception parameters.
        ///
        /// @param @c type The remote exception type.
        /// @param @c message The remote exception user message.
        std::string makeUserMessage(const std::string& type, const std::string& message)
        {
            std::string userMessage = "remote error: ";
            if (!type.empty())
                userMessage += type + ": ";
            userMessage += message;
            return userMessage;
        }

    public:
        /// @brief Returns @c true if the type of the exception is available.
        bool hasType() const { return !m_type.empty(); }

        /// @brief Returns the exception type name if available, an empty string otherwise.
        std::string type() const { return m_type; }
    };

    /// @brief Thrown by @c call when there is a problem calling the remote method.
    ///
    /// This can either be because of an error in the underlying messaging layer or because the
    /// message is improperly formatted.
    class MessageException : public std::runtime_error {
        /// @brief @ref call is the only function that can throw this exception.
        template <typename Method,
            typename... Args,
            typename std::enable_if<std::is_base_of<xhal::common::Method, Method>::value, int>::type>
        friend common::helper::functor_return_t<Method> call(wisc::RPCSvc& connection,
            Args&&... args);

        /// @brief Constructor.
        explicit MessageException(const std::string& message)
            : std::runtime_error(message)
        {
        }

    public:
        // Use what()
    };

    /* Implementation */
    template <typename Method,
        typename... Args,
        typename std::enable_if<std::is_base_of<xhal::common::Method, Method>::value, int>::type>
    common::helper::functor_return_t<Method> call(wisc::RPCSvc& connection, Args&&... args)
    {
        try {
            // Encode the request
            std::ostringstream request;
            {
                cereal::PortableBinaryOutputArchive request_archive(request);
                request_archive(common::MessageType::call);

                // The RPC method name is taken from the typeid(). This is
                // implementation dependent but both gcc and clang follow the
                // same convention
                request_archive(std::string { typeid(Method).name() });

                // Type conversion from args to serializable types must be
                // performed in the same statement in order to make use of
                // lifetime extension
                request_archive(common::helper::get_forward_as_tuple<Method>()(args...));
            }

            // Remote call
            const std::string response_data = connection.call_method(request.str());

            // Decode the response
            std::istringstream response(response_data);
            {
                cereal::PortableBinaryInputArchive response_archive(response);

                // Was the call succesful or not?
                common::MessageType messageType;
                response_archive(messageType);

                if (messageType == common::MessageType::reply) {
                    // Use a void_holder wrapper to handle the cases for which the RPC method "returns" a @c void
                    common::compat::void_holder<common::helper::functor_return_t<Method>> return_v;
                    response_archive(return_v);
                    return return_v.get();
                } else if (messageType == common::MessageType::exception) {
                    std::string exceptionType, exceptionMessage;
                    response_archive(exceptionType, exceptionMessage);
                    throw RemoteException(exceptionType, exceptionMessage);
                }

                // Hmm... We should never reach here in normal operations...
                throw MessageException("Unexpected xHAL RPC message type: " + std::to_string(uint16_t(messageType)));
            }
        } catch (const wisc::RPCSvc::RPCException& e) {
            throw MessageException(e.message);
        }
    }

}
}

#endif // XHAL_CLIENT_CALL_H
