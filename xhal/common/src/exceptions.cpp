/// @file

#include <xhal/common/exceptions.h>

#include "cxxabi.h" // C++ Itanium ABI

std::string xhal::common::helper::getExceptionMessage(const std::exception& e)
{
    return e.what();
}

std::string xhal::common::helper::getExceptionType()
{
    std::string userName;

    // Fetch the type of the current exception
    const std::type_info* exceptionType = abi::__cxa_current_exception_type();
    if (exceptionType != nullptr) {
        // Try to demangle it
        char* demangled = abi::__cxa_demangle(exceptionType->name(),
            nullptr, nullptr, nullptr);
        if (demangled != nullptr) {
            userName = demangled;
            std::free(demangled);
        } else {
            // Could not demangle, use raw name
            userName = exceptionType->name();
        }
    }

    return userName;
}
