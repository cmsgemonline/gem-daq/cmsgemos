/// @file
/// @brief This file contains helpers for exception handling.

#ifndef XHAL_COMMON_EXCEPTIONS_H
#define XHAL_COMMON_EXCEPTIONS_H

#include <string>

namespace xhal {
namespace common {
    namespace helper {

        /// @brief Retrieves a user-friendly message for the given exception.
        ///
        /// Specialization for @c std::exception.
        std::string getExceptionMessage(const std::exception& e);

        /// @brief Retrieve the type of the current exception.
        ///
        /// @internal This function makes use of low-level functions of the Itanium C++ ABI to
        ///           retrieve the type name of the current exception.
        std::string getExceptionType();

    } // namespace xhal::common::helper
} // namespace xhal::common
} // namespace xhal

#endif // XHAL_COMMON_EXCEPTIONS_H
