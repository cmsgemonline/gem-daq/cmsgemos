/// @file
/// @brief Contains the classes required for defining remotely callable RPC methods

#ifndef XHAL_COMMON_COMMON_H
#define XHAL_COMMON_COMMON_H

#include <cereal/types/array.hpp>
#include <cereal/types/common.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/optional.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/tuple.hpp>
#include <cereal/types/utility.hpp>
#include <cereal/types/variant.hpp>
#include <cereal/types/vector.hpp>

namespace xhal {
namespace common {

    /// @brief List of all supported message types
    ///
    /// @detail The numerical value holds the message type ID to be sent on the
    ///         link
    enum struct MessageType : uint8_t {
        /// @brief Remotely call an RPC method
        ///
        /// @details The message consists of:
        ///            1. The RPC method name
        ///            2. The method arguments from left to right
        call = 0x0,

        /// @brief Reply succesfully to an RPC method call
        ///
        /// #details The message consists of:
        ///            1. The method return value
        reply = 0x1,

        /// @brief Reply with an exception to an RPC method call
        ///
        /// @details The message consists of:
        ///            * The exception type (demangled whenever possible; can be empty)
        ///            * The exception message
        exception = 0x2,
    };

    /// @brief Class whose all remotely callable RPC method must inherit from
    ///
    /// The required inheritance is used as a compile time check so a developer
    /// cannot remotely call a local function by mistake.
    struct Method {
        /// @brief The operator call must be define <b>once and only once</b> per
        /// RPC method.
        ///
        /// This templated operator declaration is only shown as an example and
        /// emphasizes the need of defining it in child classes.
        ///
        /// @warnng The call operator @b must be defined as @c const.
        template <typename R, typename... Args>
        R operator()(Args...) const;
    };

} // namespace xhal::common
} // namespace xhal

#endif // XHAL_COMMON_COMMON_H
