/// @file

#include <xhal/server/ModuleManager.h>

#include <xhal/common/common.h>
#include <xhal/common/exceptions.h>

#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/string.hpp>

#include <dirent.h>
#include <dlfcn.h>
#include <errno.h>
#include <sstream>
#include <sys/types.h>

namespace /* anonymous */ {
namespace helper {

    /// @brief Handles an exception, setting the error key on the response.
    ///
    /// In case a second exception occurs when setting the error key, @c std::terminate is called.
    template <typename Exception>
    void serializeException(const Exception& e, cereal::PortableBinaryOutputArchive& response) noexcept
    {
        // Log exception here?
        response(xhal::common::MessageType::exception);
        response(xhal::common::helper::getExceptionType());
        response(xhal::common::helper::getExceptionMessage(e));
    }

    /// @brief Handles an unknown exception, setting the error key on the response.
    ///
    /// In case an exception occurs when setting the error key, @c std::terminate is called.
    static void serializeException(cereal::PortableBinaryOutputArchive& response) noexcept
    {
        // Log exception here?
        response(xhal::common::MessageType::exception);
        response(xhal::common::helper::getExceptionType());
        response(std::string { "unknown exception type" });
    }

} // namespace helper
} // anonymous namespace

bool ModuleManager::load_module(std::string module_name)
{
    LogManager::ScopeService scopesvc(LOGGER, "ModuleManager");

    void* mod = dlopen(module_name.data(), RTLD_NOW | RTLD_LOCAL);
    if (!mod) {
        LOGGER->log_message(LogManager::ERROR, stdsprintf("Unable to dlopen %s: %s", module_name.data(), dlerror()));
        return false;
    }

    if (this->modulestate[module_name] == 1) {
        LOGGER->log_message(LogManager::INFO, stdsprintf("Module %s is already loaded", module_name.data()));
        return true;
    }

    void* initfunc = dlsym(mod, "module_init");
    if (!initfunc) {
        LOGGER->log_message(LogManager::ERROR, stdsprintf("Unable to locate initialization function in %s: %s", module_name.data(), dlerror()));
        return false;
    }

    LOGGER->log_message(LogManager::DEBUG, stdsprintf("Initializing loaded module %s", module_name.data()));
    LOGGER->push_active_service(module_name);
    reinterpret_cast<void (*)(ModuleManager*)>(initfunc)(this);
    LOGGER->pop_active_service(module_name);
    this->modulestate[module_name] = 1;
    LOGGER->log_message(LogManager::DEBUG, stdsprintf("Completed module initialization for %s", module_name.c_str()));

    return true;
}

void ModuleManager::register_method(const std::string& name, rpc_method_t func)
{
    this->methods.insert(
        std::pair<std::string, ModuleMethod>(name, ModuleMethod(func, this->modload_activity_color)));
    LOGGER->log_message(LogManager::DEBUG, stdsprintf("Registered method %s", name.data()));
}

void ModuleManager::invoke_method(const std::string& request_data, std::string& response_data) noexcept
{
    std::ostringstream response;

    try {
        std::istringstream request(request_data);

        cereal::PortableBinaryInputArchive request_archive(request);

        // Ensure we need to call an RPC method
        xhal::common::MessageType messageType;
        request_archive(messageType);
        if (messageType != xhal::common::MessageType::call)
            throw std::runtime_error("Expected a \"call\" message type (" + std::to_string(uint16_t(xhal::common::MessageType::call)) + "), got " + std::to_string(uint16_t(messageType)));

        // Find the RPC method
        std::string method;
        request_archive(method);
        std::map<std::string, ModuleMethod>::iterator mmi = this->methods.find(method);
        if (mmi == this->methods.end()) {
            LOGGER->log_message(LogManager::ERROR, std::string("Unknown method called: ") + method);
            throw std::runtime_error("Unknown method called: " + method);
        }
        LogManager::ScopeService scopesvc(LOGGER, method, mmi->second.activity_color);
        LOGGER->indicate_activity();

        // Prepare a reply message and call the RPC method
        cereal::PortableBinaryOutputArchive response_archive(response);
        response_archive(xhal::common::MessageType::reply);
        mmi->second.method(request_archive, response_archive);
    } catch (const std::exception& e) {
        // Clear the reply message in case it would be partially filled
        response.str({});
        response.clear();
        cereal::PortableBinaryOutputArchive response_archive(response);
        helper::serializeException(e, response_archive);
    } catch (...) {
        // Clear the reply message in case it would be partially filled
        response.str({});
        response.clear();
        cereal::PortableBinaryOutputArchive response_archive(response);
        helper::serializeException(response_archive);
    }

    response_data = response.str();
}
