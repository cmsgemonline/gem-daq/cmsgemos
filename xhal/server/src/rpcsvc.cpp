/// @file

#include <CLI/CLI.hpp>

#include <dirent.h>
#include <errno.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define MAX_CLIENTS 50

//#define dprintf(...) printf(__VA_ARGS__)
#define dprintf(...)

int run_client(int clientfd); // run_client.cpp

void handle_client_connection(int clientfd, struct sockaddr_in& client_addr, socklen_t client_addr_len, int& numchildren);

void do_fork();

int main(int argc, char* argv[])
{
    // Define the options
    bool foreground = false;
    short port = 9811;

    // Parse the command line
    {
        CLI::App app { "RPC server application\n" };
        app.option_defaults()->always_capture_default();
        app.add_option("-p,--port", port, "Port on which to listen");
        app.add_flag("-f,--foreground", foreground, "Not not daemonize the server")->multi_option_policy(CLI::MultiOptionPolicy::Throw);
        CLI11_PARSE(app, argc, argv);
    }

    int listenfd;
    struct sockaddr_in serv_addr;

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    if (listenfd < 0) {
        printf("Unable to open socket: %d (%s)\n", errno, strerror(errno));
        return 1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(port);

    int optval = 1;
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int));

    if (bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("Unable to bind address: %d (%s)\n", errno, strerror(errno));
        return 1;
    }

    dprintf("Listening...\n");
    listen(listenfd, 5);

    if (!foreground)
        do_fork();

    int numchildren = 0;
    while (1) {
        int waitstat;
        while (waitpid(0, &waitstat, WNOHANG) > 0) {
            numchildren--;
        }

        struct timeval tvtimeout;
        tvtimeout.tv_sec = 0;
        tvtimeout.tv_usec = 50000;

        fd_set rfd;
        FD_ZERO(&rfd);
        FD_SET(listenfd, &rfd);
        int rv = select(listenfd + 1, &rfd, NULL, NULL, &tvtimeout);
        if (rv == 0 || (rv < 0 && errno == EINTR))
            continue;
        if (rv < 0)
            return 1;

        struct sockaddr_in client_addr;
        socklen_t client_addr_len = sizeof(client_addr);
        int clientfd = accept(listenfd, reinterpret_cast<struct sockaddr*>(&client_addr), &client_addr_len);
        if (clientfd >= 0)
            handle_client_connection(clientfd, client_addr, client_addr_len, numchildren);
    }
    close(listenfd);
    return 0;
}

void handle_client_connection(int clientfd, struct sockaddr_in& client_addr, socklen_t client_addr_len, int& numchildren)
{
    dprintf("Handling connection #%u.\n", numchildren);

    if (numchildren >= MAX_CLIENTS) {
        close(clientfd);
        dprintf("Dropped client #%u: max client connections reached, not forking.\n", numchildren);
        return;
    }

    dprintf("Accepted client #%u, forking...\n", numchildren);

    int optval = 3; // tcp_keepalive_probes (max unacknowledged)
    if (setsockopt(clientfd, IPPROTO_TCP, TCP_KEEPCNT, &optval, sizeof(optval)) < 0) {
        dprintf("Failed to set TCP_KEEPCNT.  Dropping new client\n");
        close(clientfd);
        return;
    }

    optval = 60; // tcp_keepalive_time (idle time before connection's first probe)
    if (setsockopt(clientfd, IPPROTO_TCP, TCP_KEEPIDLE, &optval, sizeof(optval)) < 0) {
        dprintf("Failed to set TCP_KEEPIDLE.  Dropping new client\n");
        close(clientfd);
        return;
    }

    optval = 60; // tcp_keepalive_intvl (time between probes, data or not)
    if (setsockopt(clientfd, IPPROTO_TCP, TCP_KEEPINTVL, &optval, sizeof(optval)) < 0) {
        dprintf("Failed to set TCP_KEEPINTVL.  Dropping new client\n");
        close(clientfd);
        return;
    }

    optval = 1; // enable tcp keepalive
    if (setsockopt(clientfd, SOL_SOCKET, SO_KEEPALIVE, &optval, sizeof(optval)) < 0) {
        dprintf("Failed to set SO_KEEPALIVE.  Dropping new client\n");
        close(clientfd);
        return;
    }

    int pid = fork();
    if (pid == 0) {
        run_client(clientfd); // process exiting handles fd cleanup.
        exit(0);
    } else if (pid > 0)
        numchildren++;
    close(clientfd); // parent has no use for this.
}

void do_fork()
{
    if (fork())
        exit(0);
    setsid();
    if (fork())
        exit(0);

    // Close all standard file descriptors
    // This avoid unexpected output or termination by signal
    close(0);
    close(1);
    close(2);
}
