/// @file

#define PROTOVER 3
#define MIN_PROTOVER 3
#define MODULE_NAME "libgemhardwarerpc.so"
#define LOG_PATH_FORMAT "syslog"
// #define LOG_PATH_FORMAT "/var/log/rpcsvc/rpcsvc.%s.%u.log"
#define LOG_OUTPUT_LEVEL LogManager::INFO

/* 8 megabyte max message.
 * The Zynq only has so much RAM, and we serve many clients.
 * Let me know if you need this larger for any reason.
 */
#define MAX_MSGLEN (1024 * 1024 * 8)

#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <sys/select.h>
#include <sys/socket.h>
#include <unistd.h>
// <zlib.h>
extern "C" {
extern unsigned long crc32(unsigned long crc, const unsigned char* buf, unsigned int len);
};
// </zlib.h>

#include <xhal/server/ModuleManager.h>

// #define ERROR_DIE(...) return 1

//#define dprintf(...) printf(__VA_ARGS__)
#define dprintf(...)
#define ERROR_DIE(...)                                                   \
    do {                                                                 \
        LOGGER->log_message(LogManager::ERROR, stdsprintf(__VA_ARGS__)); \
        return 1;                                                        \
    } while (0)

std::string get_ip4_string(int socket)
{
    struct sockaddr_in remote_address;
    socklen_t remote_address_len = sizeof(remote_address);
    if (getpeername(socket, reinterpret_cast<sockaddr*>(&remote_address), &remote_address_len) < 0)
        return "";
    char buf[INET_ADDRSTRLEN];
    memset(buf, 0, INET_ADDRSTRLEN);
    inet_ntop(AF_INET, &remote_address.sin_addr, buf, INET_ADDRSTRLEN);
    return std::string(buf);
}

int run_client(int clientfd)
{
    /* RPC Protocol Communications
     *
     * Initial Handshake:
     * Send: uint32_t server_protocol_version; // network byte order
     * Recv: uint32_t client_protocol_version; // network byte order
     * Hang up if incompatible.
     *
     * Runtime:
     * Recv: uint32_t msglen_bytes; // network byte order
     * Recv: uint32_t msg_crc32; // network byte order
     * Recv: uint8_t serialized_rpcmsg;
     * Send: uint32_t msglen_bytes; // network byte order
     * Send: uint32_t msg_crc32; // network byte order
     * Send: uint8_t serialized_rpcmsg;
     */

    // Create our own logger for this client.  We leak this.  It will be cleaned up on close.
    LOGGER = new LogManager(LOG_PATH_FORMAT, LOG_OUTPUT_LEVEL);
    LOGGER->push_active_service("rpcsvc");

    LOGGER->log_message(LogManager::NOTICE, stdsprintf("Client connected from %s", get_ip4_string(clientfd).c_str()));

    ModuleManager modmgr;
    if (!modmgr.load_module(MODULE_NAME))
        ERROR_DIE("Unable to load the RPC module");

    uint32_t protover = htonl(PROTOVER);
    if (write(clientfd, &protover, 4) != 4)
        ERROR_DIE("Unable to write protocol version to client");
    LOGGER->log_message(LogManager::DEBUG, stdsprintf("Wrote protocol version to client: %u", PROTOVER));
    if (recv(clientfd, &protover, 4, MSG_WAITALL) != 4)
        ERROR_DIE("Unable to read protocol version from client");
    LOGGER->log_message(LogManager::DEBUG, stdsprintf("Read protocol version from client: %u", ntohl(protover)));
    if (ntohl(protover) < MIN_PROTOVER)
        ERROR_DIE("Client is too old");

    while (1) {
        uint32_t reqlen;
        int rv = recv(clientfd, &reqlen, 4, MSG_WAITALL);
        if (rv == 0) {
            LOGGER->log_message(LogManager::NOTICE, "Client disconnected cleanly");
            return 0;
        }
        if (rv != 4)
            ERROR_DIE("Unable to read request length from client");
        reqlen = ntohl(reqlen);
        if (reqlen > MAX_MSGLEN)
            ERROR_DIE("Client request too long: %u bytes", reqlen);

        uint32_t reqcrc;
        if (recv(clientfd, &reqcrc, 4, MSG_WAITALL) != 4)
            ERROR_DIE("Unable to read request CRC32 from client");
        reqcrc = ntohl(reqcrc);

        uint32_t bytesread = 0;
        std::string reqmsg(reqlen, '\0');
        while (bytesread < reqlen) {
            int rv = read(clientfd, reqmsg.data() + bytesread, reqlen - bytesread);
            if (rv <= 0)
                ERROR_DIE("Error reading request message from client");
            bytesread += rv;
        }

        if (reqcrc != crc32(0, reinterpret_cast<const unsigned char*>(reqmsg.data()), reqlen))
            ERROR_DIE("Invalid request CRC32 from client");

        std::string rspmsg;
        modmgr.invoke_method(reqmsg, rspmsg);

        const uint32_t rsplen = htonl(rspmsg.size());
        if (write(clientfd, &rsplen, 4) != 4)
            ERROR_DIE("Unable to write request length to client");

        const uint32_t rspcrc = htonl(crc32(0, reinterpret_cast<const unsigned char*>(rspmsg.data()), rspmsg.size()));
        if (write(clientfd, &rspcrc, 4) != 4)
            ERROR_DIE("Unable to write request CRC32 to client");

        uint32_t bytes_written = 0;
        while (bytes_written < rspmsg.size()) {
            int rv = write(clientfd, rspmsg.data() + bytes_written, rspmsg.size() - bytes_written);
            if (rv < 0)
                ERROR_DIE("Error writing response message to client");
            bytes_written += rv;
        }
    }
}
