/// @file

#ifndef XHAL_SERVER_MODULEMANAGER_H
#define XHAL_SERVER_MODULEMANAGER_H

#include <xhal/server/LogManager.h>

#include <cereal/archives/portable_binary.hpp>

#include <map>
#include <string>

class ModuleManager {
public:
    typedef void (*rpc_method_t)(cereal::PortableBinaryInputArchive& request_archive, cereal::PortableBinaryOutputArchive& response_archive);

protected:
    class ModuleMethod {
    public:
        rpc_method_t method;
        int activity_color;
        ModuleMethod(rpc_method_t method, int activity_color)
            : method(method)
            , activity_color(activity_color) {};
    };
    std::map<std::string, ModuleMethod> methods;
    std::map<std::string, int> modulestate;
    int modload_activity_color;

public:
    ModuleManager() {};
    bool load_module(std::string module_name);
    void register_method(const std::string& name, rpc_method_t func);
    void invoke_method(const std::string& request_data, std::string& response_data) noexcept;
};

#endif // XHAL_SERVER_MODULEMANAGER_H
