/// @file
/// @brief This file contains all the functions needed to register a remotely callable RPC method

#ifndef XHAL_COMMON_REGISTER_H
#define XHAL_COMMON_REGISTER_H

#include <xhal/common/common.h>
#include <xhal/common/compat.h>
#include <xhal/common/helper.h>
#include <xhal/server/ModuleManager.h> // Only present in the CTP7 modules

#include <cereal/archives/portable_binary.hpp>

#include <typeinfo>

namespace xhal {
namespace server {

    /// @brief Locally invoke an RPC method
    ///
    /// This function is the wrapper called for every remote function call. It
    /// deserializes the @b arguments from the input @c cereal archive, calls
    /// the local functor, and then serializes the return value to the output
    /// @c cereal archive.
    ///
    /// Exception handling is left to the caller.
    template <typename Method,
        typename std::enable_if<std::is_base_of<xhal::common::Method, Method>::value, int>::type = 0>
    void invoke(cereal::PortableBinaryInputArchive& request_archive, cereal::PortableBinaryOutputArchive& response_archive)
    {
        // Remove the cv-qualifiers and references since we need
        // a copy of the object
        common::helper::functor_decay_args_t<Method> args;
        request_archive(args);

        // Call the Method functor with the arguments received from
        // the RPC message
        auto result = common::compat::tuple_apply<common::helper::functor_return_t<Method>>(Method {}, args);

        // Serialize the reply
        response_archive(result);
    }

    /// @brief Register a RPC method into the @c ModuleManager
    ///
    /// This helper function register a RPC method with the right parameters
    /// so it can be remotely called.
    template <typename Method>
    void registerMethod(ModuleManager* modmgr)
    {
        // The RPC method name is taken from the typeid(). This is
        // implementation dependent but both gcc and clang follow the
        // same convention
        return modmgr->register_method(typeid(Method).name(), invoke<Method>);
    }

}
}

#endif // XHAL_COMMON_REGISTER_H
