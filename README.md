# GEM Online Software

Please start with reading the [Contributor's Guide](CONTRIBUTING.md)
If you're super eager to play with the software, check the [setup](DEVELOPERS.md#setup) instructions.

Enjoy!

## Firmware compatibility

This software version has been tested and validated for the following firmware
releases:

* ME0 backend: `r1553.241113233050+gita68fc79f-local-resets-qc7-flavors`
* GEx/1 backend: `r1553.241113233050+gita68fc79f-local-resets-qc7-flavors`
* ME0 OptoHybrid: `230404163016+git4d2d692b-ohv2-prod`
* GEx/1 OptoHybrid: `r1538.240424213759+git55a3a222-relax-iserdes-crc-trig-sem-fix`

Using older firmware releases is not recommended since the software is
continuously adapted to the new firmware behavior and improved using newly
available firmware features. Conversely, using newer firmware releases can lead
to incompatibilities due to changing firmware behavior. Use with care.

## What this repository contains

* The `cmsgemos` source code
  * Whose builds are published on [the cmsgemos website](https://cmsgemos.web.cern.ch/software/cmsgemos/)
* The `cmsgemos` documentation, including the conventions, API, installation instruction, and basic usage instructions
  * The most recent and up-to-date version of the documentation is published on [the cmsgemos website](https://cmsgemos.web.cern.ch/software/cmsgemos/doc/)
* A minimal set of examples how to get started with the developments

## What this repository does not contain

* An introduction to the GEM DAQ & electronics and the user documentation for managed setups at CERN, stored in [gem-daq-doc](https://gitlab.cern.ch/cmsgemonline/gem-ops/gem-daq-doc)
  * Its most recent and up-to-date version is published on [the cmsgemonline website](https://cmsgemonline.web.cern.ch/doc/latest/)
* The GEM DAQ firmware sources, developed in [0xBEFE](https://gitlab.cern.ch/emu/0xbefe/)
  * The artifacts to be used with `cmsgemos` are however published on [the cmsgemos website](https://cmsgemos.web.cern.ch/firmware/)
* The sources of the various Linux images, tools, and additional components
  * The artifacts are published on [the cmsgemos website](https://cmsgemos.web.cern.ch/software/extras/)
* The configurations for the managed setups at CERN, stored in [cmsgemos-configs](https://gitlab.cern.ch/cmsgemonline/gem-daq/cmsgemos-configs)
