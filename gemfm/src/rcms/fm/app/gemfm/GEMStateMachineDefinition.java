/**
 * @author Andrea Petrucci, Alexander Oh, Michele Gulmini, Jose Ruiz, Jared Sturdy
 */

package rcms.fm.app.gemfm;

import rcms.fm.fw.parameter.CommandParameter;
import rcms.fm.fw.parameter.ParameterException;
import rcms.fm.fw.parameter.ParameterSet;
import rcms.fm.fw.parameter.type.BooleanT;
import rcms.fm.fw.parameter.type.IntegerT;
import rcms.fm.fw.parameter.type.LongT;
import rcms.fm.fw.parameter.type.StringT;
import rcms.fm.fw.parameter.type.VectorT;
import rcms.fm.fw.user.UserStateMachineDefinition;
import rcms.statemachine.definition.State;
import rcms.statemachine.definition.StateMachineDefinitionException;

/**
 * GEM Level-1 Function Manager state machine
 *
 * The actual definition of the State Machine must be put in the "init" method.
 */
public class GEMStateMachineDefinition extends UserStateMachineDefinition {

    public GEMStateMachineDefinition(boolean lifetimeOnly)
        throws StateMachineDefinitionException
    {
        if (!lifetimeOnly)
            fullStateMachineDefinition();
        else
            lifetimeOnlyStateMachineDefinition();
    }

    protected void lifetimeOnlyStateMachineDefinition()
        throws StateMachineDefinitionException
    {
        // States
        addState(GEMStates.INITIAL);
        addState(GEMStates.INITIALIZING);
        addState(GEMStates.ENABLED);
        addState(GEMStates.ERROR);

        // Initial state
        setInitialState(GEMStates.INITIAL);

        // Commands
        addInput(GEMInputs.INITIALIZE); // External command
        addInput(GEMInputs.SETENABLED); // Internal command for asynchronous behaviour
        addInput(GEMInputs.SETERROR);
        GEMInputs.SETENABLED.setVisualizable(false);
        GEMInputs.SETERROR.setVisualizable(false);

        // State transitions
        addTransition(GEMInputs.INITIALIZE, GEMStates.INITIAL, GEMStates.INITIALIZING); // External command
        addTransition(GEMInputs.SETENABLED, GEMStates.INITIALIZING, GEMStates.ENABLED); // Internal command
        addTransition(GEMInputs.SETERROR, State.ANYSTATE, GEMStates.ERROR); // Internal command
    }

    protected void fullStateMachineDefinition()
        throws StateMachineDefinitionException
    {
        /*
         * States
         */
        addState(GEMStates.INITIAL);
        addState(GEMStates.HALTED);
        addState(GEMStates.CONFIGURED);
        addState(GEMStates.PAUSED);
        addState(GEMStates.ERROR);

        addState(GEMStates.INITIALIZING);
        addState(GEMStates.RESETTING);
        addState(GEMStates.RECOVERING);
        addState(GEMStates.COLDRESETTING);
        addState(GEMStates.CONFIGURING);
        addState(GEMStates.HALTING);
        addState(GEMStates.STARTING);
        addState(GEMStates.STOPPING);
        addState(GEMStates.PAUSING);
        addState(GEMStates.RESUMING);

        addState(GEMStates.RUNNING);
        addState(GEMStates.RUNNINGDEGRADED);
        addState(GEMStates.RUNNINGSOFTERRORDETECTED);

        addState(GEMStates.FIXINGSOFTERROR);

        addState(GEMStates.TTSTEST_MODE);

        addState(GEMStates.PREPARING_TTSTEST_MODE);
        addState(GEMStates.TESTING_TTS);

        /*
         * Initial state
         */
        setInitialState(GEMStates.INITIAL);

        /*
         * Commands
         */
        addInput(GEMInputs.INITIALIZE);
        addInput(GEMInputs.RESET);
        addInput(GEMInputs.RECOVER);
        addInput(GEMInputs.COLDRESET);
        addInput(GEMInputs.CONFIGURE);
        addInput(GEMInputs.HALT);
        addInput(GEMInputs.START);
        addInput(GEMInputs.STOP);
        addInput(GEMInputs.PAUSE);
        addInput(GEMInputs.RESUME);
        addInput(GEMInputs.FIXSOFTERROR);
        addInput(GEMInputs.PREPARE_TTSTEST_MODE);
        addInput(GEMInputs.TEST_TTS);

        // Internal and invisible commands needed for fully asynchronous behaviour
        addInput(GEMInputs.SETHALTED);
        addInput(GEMInputs.SETCONFIGURED);
        addInput(GEMInputs.SETPAUSED);
        addInput(GEMInputs.SETERROR);
        addInput(GEMInputs.SETRUNNING);
        addInput(GEMInputs.SETRUNNINGDEGRADED);
        addInput(GEMInputs.SETRUNNINGSOFTERRORDETECTED);
        addInput(GEMInputs.SETTTSTEST_MODE);

        GEMInputs.SETHALTED.setVisualizable(false);
        GEMInputs.SETCONFIGURED.setVisualizable(false);
        GEMInputs.SETPAUSED.setVisualizable(false);
        GEMInputs.SETERROR.setVisualizable(false);
        GEMInputs.SETRUNNING.setVisualizable(false);
        GEMInputs.SETRUNNINGDEGRADED.setVisualizable(false);
        GEMInputs.SETRUNNINGSOFTERRORDETECTED.setVisualizable(false);
        GEMInputs.SETTTSTEST_MODE.setVisualizable(false);

        /*
         * Define parameters for the Initialize command in the the GUI
         */
        CommandParameter<StringT> initializeGlobalConfigurationKey = new CommandParameter<StringT>(GEMParameters.GLOBAL_CONF_KEY, new StringT(""));

        ParameterSet<CommandParameter> initializeParameters = new ParameterSet<CommandParameter>();
        try {
            initializeParameters.add(initializeGlobalConfigurationKey);
        } catch (ParameterException nothing) {
            throw new StateMachineDefinitionException("Could not add to initializeParameters. Duplicate Parameter?", nothing);
        }

        GEMInputs.INITIALIZE.setParameters(initializeParameters);

        /*
         * Define parameters for the Configure command in the GUI
         */
        CommandParameter<IntegerT> configureRunNumber = new CommandParameter<IntegerT>(GEMParameters.RUN_NUMBER, new IntegerT(0));
        CommandParameter<StringT> configureRunKey = new CommandParameter<StringT>(GEMParameters.RUN_KEY, new StringT(""));
        CommandParameter<StringT> configureFedEnableMask = new CommandParameter<StringT>(GEMParameters.FED_ENABLE_MASK, new StringT("%"));
        CommandParameter<BooleanT> configureUsePrimaryTCDS = new CommandParameter<BooleanT>(GEMParameters.USE_PRIMARY_TCDS, new BooleanT(true));

        ParameterSet<CommandParameter> configureParameters = new ParameterSet<CommandParameter>();
        try {
            configureParameters.add(configureRunNumber);
            configureParameters.add(configureRunKey);
            configureParameters.add(configureFedEnableMask);
            configureParameters.add(configureUsePrimaryTCDS);
        } catch (ParameterException nothing) {
            throw new StateMachineDefinitionException("Could not add to configureParameters. Duplicate Parameter?", nothing);
        }

        GEMInputs.CONFIGURE.setParameters(configureParameters);

        /*
         * Define parameters for the Start command in the GUI
         */
        CommandParameter<IntegerT> startRunNumber = new CommandParameter<IntegerT>(GEMParameters.RUN_NUMBER, new IntegerT(0));

        ParameterSet<CommandParameter> startParameters = new ParameterSet<CommandParameter>();
        try {
            startParameters.add(startRunNumber);
        } catch (ParameterException nothing) {
            throw new StateMachineDefinitionException("Could not add to startParameters. Duplicate Parameter?", nothing);
        }

        GEMInputs.START.setParameters(startParameters);

        /*
         * Define parameters for the FixSoftError command in the GUI
         */
        CommandParameter<LongT> fixSoftErrortriggerNumberAtPause = new CommandParameter<LongT>(GEMParameters.TRIGGER_NUMBER_AT_PAUSE, new LongT(0));

        ParameterSet<CommandParameter> fixSoftErrorParameters = new ParameterSet<CommandParameter>();
        try {
            fixSoftErrorParameters.add(fixSoftErrortriggerNumberAtPause);
        } catch (ParameterException nothing) {
            throw new StateMachineDefinitionException("Could not add to fixSoftErrorParameters. Duplicate Parameter?", nothing);
        }

        GEMInputs.FIXSOFTERROR.setParameters(fixSoftErrorParameters);

        /*
         * Define parameters for the TestTTS command in the GUI
         */
        CommandParameter<IntegerT> testTTSFedID = new CommandParameter<IntegerT>(GEMParameters.TTS_TEST_FED_ID, new IntegerT(0));
        CommandParameter<StringT> testTTSType = new CommandParameter<StringT>(GEMParameters.TTS_TEST_TYPE, new StringT(""));
        CommandParameter<StringT> testTTSPattern = new CommandParameter<StringT>(GEMParameters.TTS_TEST_PATTERN, new StringT(""));
        CommandParameter<IntegerT> testTTSCycles = new CommandParameter<IntegerT>(GEMParameters.TTS_TEST_CYCLES, new IntegerT(0));

        ParameterSet<CommandParameter> testTTSParameters = new ParameterSet<CommandParameter>();
        try {
            testTTSParameters.add(testTTSFedID);
            testTTSParameters.add(testTTSType);
            testTTSParameters.add(testTTSPattern);
            testTTSParameters.add(testTTSCycles);
        } catch (ParameterException nothing) {
            throw new StateMachineDefinitionException("Could not add to testTTSParameters. Duplicate Parameter?", nothing);
        }

        GEMInputs.TEST_TTS.setParameters(testTTSParameters);

        /*
         * State transitions from external commands
         */

        // Initialize
        addTransition(GEMInputs.INITIALIZE, GEMStates.INITIAL, GEMStates.INITIALIZING);

        // Reset
        addTransition(GEMInputs.RESET, GEMStates.HALTED, GEMStates.RESETTING);
        addTransition(GEMInputs.RESET, GEMStates.CONFIGURED, GEMStates.RESETTING);
        addTransition(GEMInputs.RESET, GEMStates.PAUSED, GEMStates.RESETTING);
        addTransition(GEMInputs.RESET, GEMStates.ERROR, GEMStates.RESETTING);
        addTransition(GEMInputs.RESET, GEMStates.RUNNING, GEMStates.RESETTING);
        addTransition(GEMInputs.RESET, GEMStates.RUNNINGDEGRADED, GEMStates.RESETTING);
        addTransition(GEMInputs.RESET, GEMStates.RUNNINGSOFTERRORDETECTED, GEMStates.RESETTING);
        addTransition(GEMInputs.RESET, GEMStates.TTSTEST_MODE, GEMStates.RESETTING);

        // Recover
        addTransition(GEMInputs.RECOVER, GEMStates.ERROR, GEMStates.RECOVERING);

        // ColdReset
        addTransition(GEMInputs.COLDRESET, GEMStates.HALTED, GEMStates.COLDRESETTING);

        // Configure
        addTransition(GEMInputs.CONFIGURE, GEMStates.HALTED, GEMStates.CONFIGURING);

        // Halt
        addTransition(GEMInputs.HALT, GEMStates.CONFIGURED, GEMStates.HALTING);
        addTransition(GEMInputs.HALT, GEMStates.PAUSED, GEMStates.HALTING);
        addTransition(GEMInputs.HALT, GEMStates.RUNNING, GEMStates.HALTING);
        addTransition(GEMInputs.HALT, GEMStates.RUNNINGDEGRADED, GEMStates.HALTING);
        addTransition(GEMInputs.HALT, GEMStates.RUNNINGSOFTERRORDETECTED, GEMStates.HALTING);
        addTransition(GEMInputs.HALT, GEMStates.TTSTEST_MODE, GEMStates.HALTING);

        // Start
        addTransition(GEMInputs.START, GEMStates.CONFIGURED, GEMStates.STARTING);

        // Stop
        addTransition(GEMInputs.STOP, GEMStates.PAUSED, GEMStates.STOPPING);
        addTransition(GEMInputs.STOP, GEMStates.RUNNING, GEMStates.STOPPING);
        addTransition(GEMInputs.STOP, GEMStates.RUNNINGDEGRADED, GEMStates.STOPPING);
        addTransition(GEMInputs.STOP, GEMStates.RUNNINGSOFTERRORDETECTED, GEMStates.STOPPING);

        // Pause
        addTransition(GEMInputs.PAUSE, GEMStates.RUNNING, GEMStates.PAUSING);
        addTransition(GEMInputs.PAUSE, GEMStates.RUNNINGDEGRADED, GEMStates.PAUSING);
        addTransition(GEMInputs.PAUSE, GEMStates.RUNNINGSOFTERRORDETECTED, GEMStates.PAUSING);

        // Resume
        addTransition(GEMInputs.RESUME, GEMStates.PAUSED, GEMStates.RESUMING);

        // FixSoftError
        addTransition(GEMInputs.FIXSOFTERROR, GEMStates.RUNNING, GEMStates.FIXINGSOFTERROR);
        addTransition(GEMInputs.FIXSOFTERROR, GEMStates.RUNNINGDEGRADED, GEMStates.FIXINGSOFTERROR);
        addTransition(GEMInputs.FIXSOFTERROR, GEMStates.RUNNINGSOFTERRORDETECTED, GEMStates.FIXINGSOFTERROR);

        // PreparingTTSTestMode
        addTransition(GEMInputs.PREPARE_TTSTEST_MODE, GEMStates.HALTED, GEMStates.PREPARING_TTSTEST_MODE);

        // TestTTS
        addTransition(GEMInputs.TEST_TTS, GEMStates.TTSTEST_MODE, GEMStates.TESTING_TTS);

        /*
         * State transitions from internal commands
         */

        // SetHalted
        addTransition(GEMInputs.SETHALTED, GEMStates.INITIALIZING, GEMStates.HALTED);
        addTransition(GEMInputs.SETHALTED, GEMStates.RESETTING, GEMStates.HALTED);
        addTransition(GEMInputs.SETHALTED, GEMStates.RECOVERING, GEMStates.HALTED);
        addTransition(GEMInputs.SETHALTED, GEMStates.COLDRESETTING, GEMStates.HALTED);
        addTransition(GEMInputs.SETHALTED, GEMStates.HALTING, GEMStates.HALTED);

        // SetConfigured
        addTransition(GEMInputs.SETCONFIGURED, GEMStates.RECOVERING, GEMStates.CONFIGURED);
        addTransition(GEMInputs.SETCONFIGURED, GEMStates.CONFIGURING, GEMStates.CONFIGURED);
        addTransition(GEMInputs.SETCONFIGURED, GEMStates.STOPPING, GEMStates.CONFIGURED);

        // SetPaused
        addTransition(GEMInputs.SETPAUSED, GEMStates.RUNNING, GEMStates.PAUSED);
        addTransition(GEMInputs.SETPAUSED, GEMStates.RUNNINGDEGRADED, GEMStates.PAUSED);
        addTransition(GEMInputs.SETPAUSED, GEMStates.RUNNINGSOFTERRORDETECTED, GEMStates.PAUSED);

        // SetError
        addTransition(GEMInputs.SETERROR, State.ANYSTATE, GEMStates.ERROR);

        // SetRunning
        addTransition(GEMInputs.SETRUNNING, GEMStates.STARTING, GEMStates.RUNNING);
        addTransition(GEMInputs.SETRUNNING, GEMStates.RESUMING, GEMStates.RUNNING);
        addTransition(GEMInputs.SETRUNNING, GEMStates.RUNNINGDEGRADED, GEMStates.RUNNING);
        addTransition(GEMInputs.SETRUNNING, GEMStates.FIXINGSOFTERROR, GEMStates.RUNNING);

        // SetRunningDegraded
        addTransition(GEMInputs.SETRUNNINGDEGRADED, GEMStates.STARTING, GEMStates.RUNNINGDEGRADED);
        addTransition(GEMInputs.SETRUNNINGDEGRADED, GEMStates.RESUMING, GEMStates.RUNNINGDEGRADED);
        addTransition(GEMInputs.SETRUNNINGDEGRADED, GEMStates.RUNNING, GEMStates.RUNNINGDEGRADED);
        addTransition(GEMInputs.SETRUNNINGDEGRADED, GEMStates.FIXINGSOFTERROR, GEMStates.RUNNINGDEGRADED);

        // SetRunningSoftErrorDetected
        addTransition(GEMInputs.SETRUNNINGSOFTERRORDETECTED, GEMStates.STARTING, GEMStates.RUNNINGSOFTERRORDETECTED);
        addTransition(GEMInputs.SETRUNNINGSOFTERRORDETECTED, GEMStates.RESUMING, GEMStates.RUNNINGSOFTERRORDETECTED);
        addTransition(GEMInputs.SETRUNNINGSOFTERRORDETECTED, GEMStates.RUNNING, GEMStates.RUNNINGSOFTERRORDETECTED);
        addTransition(GEMInputs.SETRUNNINGSOFTERRORDETECTED, GEMStates.RUNNINGDEGRADED, GEMStates.RUNNINGSOFTERRORDETECTED);

        // SetTTSTestMode
        addTransition(GEMInputs.SETTTSTEST_MODE, GEMStates.PREPARING_TTSTEST_MODE, GEMStates.TTSTEST_MODE);
        addTransition(GEMInputs.SETTTSTEST_MODE, GEMStates.TESTING_TTS, GEMStates.TTSTEST_MODE);
    }
}
