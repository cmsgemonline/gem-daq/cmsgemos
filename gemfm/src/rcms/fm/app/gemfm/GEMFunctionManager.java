/*
 * @author Andrea Petrucci, Alexander Oh, Michele Gulmini, Jose Ruiz, Jared Sturdy
 */

package rcms.fm.app.gemfm;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import net.hep.cms.xdaqctl.XDAQException;
import net.hep.cms.xdaqctl.XDAQMessageException;
import net.hep.cms.xdaqctl.XDAQTimeoutException;
import rcms.errorFormat.CMS.CMSError;
import rcms.fm.fw.EventHandlerException;
import rcms.fm.fw.parameter.CommandParameter;
import rcms.fm.fw.parameter.FunctionManagerParameter;
import rcms.fm.fw.parameter.Parameter;
import rcms.fm.fw.parameter.ParameterSet;
import rcms.fm.fw.parameter.type.BooleanT;
import rcms.fm.fw.parameter.type.DoubleT;
import rcms.fm.fw.parameter.type.IntegerT;
import rcms.fm.fw.parameter.type.StringT;
import rcms.fm.fw.parameter.type.VectorT;
import rcms.fm.fw.user.UserActionException;
import rcms.fm.fw.user.UserFunctionManager;
import rcms.fm.resource.CommandException;
import rcms.fm.resource.QualifiedGroup;
import rcms.fm.resource.QualifiedResource;
import rcms.fm.resource.QualifiedResourceContainer;
import rcms.fm.resource.QualifiedResourceContainerException;
import rcms.fm.resource.StateVector;
import rcms.fm.resource.StateVectorCalculation;
import rcms.fm.resource.qualifiedresource.FunctionManager;
import rcms.fm.resource.qualifiedresource.JobControl;
import rcms.fm.resource.qualifiedresource.XdaqApplication;
import rcms.fm.resource.qualifiedresource.XdaqApplicationContainer;
import rcms.fm.resource.qualifiedresource.XdaqExecutive;
import rcms.fm.resource.qualifiedresource.XdaqServiceApplication;
import rcms.resourceservice.db.resource.Resource;
import rcms.resourceservice.db.resource.config.ConfigProperty;
import rcms.resourceservice.db.resource.fm.FunctionManagerResource;
import rcms.resourceservice.db.resource.xdaq.XdaqApplicationResource;
import rcms.resourceservice.db.resource.xdaq.XdaqExecutiveResource;
import rcms.stateFormat.StateNotification;
import rcms.statemachine.definition.Input;
import rcms.statemachine.definition.State;
import rcms.statemachine.definition.StateMachineDefinitionException;
import rcms.util.logger.RCMSLogger;
import rcms.util.logsession.LogSessionConnector;
import rcms.util.logsession.LogSessionException;
import rcms.utilities.fm.task.SimpleTask;
import rcms.utilities.fm.task.Task;
import rcms.utilities.fm.task.TaskSequence;
import rcms.utilities.runinfo.RunInfo;
import rcms.utilities.runinfo.RunInfoConnectorIF;
import rcms.utilities.runinfo.RunInfoException;
import rcms.utilities.runinfo.RunNumberData;
import rcms.utilities.runinfo.RunSequenceNumber;
import rcms.xdaqctl.XDAQMessage;
import rcms.xdaqctl.XDAQParameter;

/**
 * GEM Level-1 Function Manager
 */
public class GEMFunctionManager extends UserFunctionManager {

    /**
     * <code>RCMSLogger</code>: RCMS log4j Logger
     */
    static RCMSLogger logger = new RCMSLogger(GEMFunctionManager.class);

    /**
     * <code>m_gemQC</code>: QualifiedGroup of the GEM FM
     */
    private QualifiedGroup m_gemQG = null;

    /**
     * <code>logSessionConnector</code>: Connector for logsession DB
     */
    public LogSessionConnector logSessionConnector;

    /**
     * <code>RunSequenceName</code>: Run sequence name, for attaining a run sequence number
     */
    public String RunSequenceName = "GEM test";

    /**
     * define GEM specific application containers
     */
    public XdaqApplicationContainer c_gemSupervisors = null; ///<

    /**
     * <code>svCalc</code>: Composite state calculator
     */
    public StateVectorCalculation m_svCalc = null;

    /**
     * <code>m_calcState</code>: Calculated State.
     */
    public State m_calcState = null;

    /**
     * <code>degraded</code>: FM is in degraded state
     */
    boolean degraded = false;

    /**
     * <code>softErrorDetected</code>: FM has detected softError
     */
    boolean softErrorDetected = false;

    // set from the controlled EventHandler
    protected GEMEventHandler theEventHandler = null;
    protected GEMErrorHandler theErrorHandler = null;

    // GEM RunInfo namespace, the FM name will be added in the createAction() method
    public String GEM_NS = "CMS.GEM";

    // string containing details on the setup from where this FM was started (copied from HCAL)
    public String m_rcmsStateListenerURL = "";
    public String RunSetupDetails = "empty";
    public String m_FMfullpath = "empty";
    public String m_FMname = "empty";
    public String m_FMurl = "empty";
    public String m_FMuri = "empty";
    public String m_FMrole = "empty";
    public String m_FMpartition = "empty";
    public String m_utcFMtimeofstart = "empty";
    public Date m_FMtimeofstart;

    /**
     * Default constructor
     */
    public GEMFunctionManager()
    {
        // Make the parameters available
        parameterSet = GEMParameters.LVL_ONE_PARAMETER_SET;
    }

    /**
     * Late initialization
     */
    public void init()
        throws StateMachineDefinitionException,
               rcms.fm.fw.EventHandlerException
    {
        String msgPrefix = "[GEMFM::" + m_FMname + "] GEMFunctionManager::init(): ";

        // Check whether a full FM or lifetime only FM is requested
        boolean lifetimeOnly = false;
        String lifetimeOnlyProperty = getFunctionManagerProperty("lifetimeOnly");
        if (lifetimeOnlyProperty != null && lifetimeOnlyProperty.equals("true"))
            lifetimeOnly = true;

        logger.info(msgPrefix + "Will create an FSM with lifetimeOnly = " + String.valueOf(lifetimeOnly));

        // Set first of all the State Machine Definition
        logger.info(msgPrefix + "Setting the state machine definition");
        setStateMachineDefinition(new GEMStateMachineDefinition(lifetimeOnly));

        // Add event handler
        logger.info(msgPrefix + "Adding the GEMEventHandler");
        theEventHandler = new GEMEventHandler(lifetimeOnly);
        addEventHandler(theEventHandler);

        // Add error handler
        logger.info(msgPrefix + "Adding the GEMErrorHandler");
        theErrorHandler = new GEMErrorHandler();
        addEventHandler(theErrorHandler);
    }

    /**
     * Create the function manager
     *
     * This method is called by the framework when the Function Manager is created.
     *
     * @see rcms.statemachine.user.UserStateMachine#createAction()
     */
    public void createAction(ParameterSet<CommandParameter> pars)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::<not init>] GEMFunctionManager::createAction(): ";

        logger.info(msgPrefix + "Creating function manager");

        m_gemQG = qualifiedGroup;

        // Retrieve the configuration for this Function Manager from the Group
        FunctionManagerResource fmConf = ((FunctionManagerResource)m_gemQG.getGroup().getThisResource());

        m_FMfullpath = fmConf.getDirectory().getFullPath().toString();
        m_FMname = fmConf.getName();
        m_FMurl = fmConf.getSourceURL().toString();
        m_FMuri = fmConf.getURI().toString();
        m_FMrole = fmConf.getRole();
        m_FMtimeofstart = new Date();
        DateFormat dateFormatter = new SimpleDateFormat("M/d/yy hh:mm:ss a z");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        ;
        m_utcFMtimeofstart = dateFormatter.format(m_FMtimeofstart);

        msgPrefix = "[GEMFM::" + m_FMname + "] GEMFunctionManager::createAction(): ";

        // Set StateListener URL
        try {
            URL fmURL = new URL(m_FMurl);
            String rcmsStateListenerHost = fmURL.getHost();
            int rcmsStateListenerPort = fmURL.getPort() + 1;
            String rcmsStateListenerProtocol = fmURL.getProtocol();
            m_rcmsStateListenerURL = rcmsStateListenerProtocol + "://" + rcmsStateListenerHost + ":" + rcmsStateListenerPort + "/rcms";
        } catch (MalformedURLException e) {
            goToError("Caught MalformedURLException");
        }

        // Get log session connector
        logSessionConnector = getLogSessionConnector();

        logger.info(msgPrefix + "Done");
    }

    /**
     * Destroy the function manager
     *
     * @see rcms.statemachine.user.UserStateMachine#destroyAction()
     */
    public void destroyAction()
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_FMname + "] GEMFunctionManager::destroyAction(): ";

        logger.debug(msgPrefix + "Destroying function manager");

        // Kill all xDAQ executives
        destroyXDAQ();

        // Try to close an open session ID only if we are in local run mode i.e. not in MiniDAQ and Global runs
        String runType = ((StringT)getParameterSet().get(GEMParameters.GEM_RUN_TYPE).getValue()).getString();
        if (runType.equals("local")) {
            closeSessionId();
        }

        logger.info(msgPrefix + "Done");
    }

    /**
     * Create a new RCMS session
     */
    protected Integer getSessionId()
    {
        String msgPrefix = "[GEMFM::" + m_FMname + "] GEMFunctionManager::getSessionId(): ";

        int sessionID = 0;
        if (logSessionConnector != null) {
            try {
                String user = getQualifiedGroup().getGroup().getDirectory().getUser();
                String description = getQualifiedGroup().getGroup().getDirectory().getFullPath();
                sessionID = logSessionConnector.createSession(user, description);
                logger.info(msgPrefix + "New session ID = " + sessionID);
            } catch (LogSessionException e1) {
                logger.warn(msgPrefix + "Could not get a session ID, using default = " + sessionID + ".\n"
                    + "Exception: " + e1);
            }
        } else {
            logger.warn(msgPrefix + "logSessionConnector = " + logSessionConnector + ", using default = " + sessionID);
        }

        return sessionID;
    }

    /**
     * Close the current RCMS session
     */
    protected void closeSessionId()
    {
        String msgPrefix = "[GEMFM::" + m_FMname + "] GEMFunctionManager::closeSessionId(): ";

        if (logSessionConnector != null) {
            int sessionID = 0;
            try {
                sessionID = ((IntegerT)getParameterSet().get(GEMParameters.SID).getValue()).getInteger();
            } catch (Exception e) {
                logger.warn(msgPrefix + "Could not get sessionID for closing session, not closing session.\n"
                    + "Exception: " + e);
            }
            try {
                logSessionConnector.closeSession(sessionID);
                logger.info(msgPrefix + "Session closed: " + sessionID);
            } catch (LogSessionException e) {
                logger.warn(msgPrefix + "Could not close session, but sessionID was requested and used.\n"
                    + "Exception: " + e);
            }
        } else {
            logger.warn(msgPrefix + "logSessionConnector = " + logSessionConnector + ", skipping session closure");
        }
    }

    /**
     * Get new official run and sequence numbers from the RunInfo database
     */
    protected RunNumberData getOfficialRunNumber()
    {
        String msgPrefix = "[GEMFM::" + m_FMname + "] GEMFunctionManager::getOfficialRunNumber(): ";

        // Get SID
        Integer sid = ((IntegerT)getParameterSet().get(GEMParameters.SID).getValue()).getInteger();

        // check availability of runInfo DB
        RunInfoConnectorIF ric = getRunInfoConnector();
        if (ric == null) {
            logger.error(msgPrefix + "RunInfoConnector is empty. Is there a RunInfo DB or is it down?");

            return new RunNumberData(new Integer(sid), new Integer(0), getOwner(), Calendar.getInstance().getTime());
        } else {
            RunSequenceNumber rsn = new RunSequenceNumber(ric, getOwner(), RunSequenceName);
            RunNumberData rnd = rsn.createRunSequenceNumber(sid);

            // Associate run number with session ID
            RunInfo runInfo = new RunInfo(ric, sid, rnd.getRunNumber());
            runInfo.setNameSpace(GEM_NS);
            try {
                runInfo.publish(new Parameter<StringT>(new String("GEMFM_VERSION"), new StringT(getBuildVersion())));
            } catch (RunInfoException e) {
                logger.error(msgPrefix + "Cannot pusblish in RunInfo DB: " + e);
            }

            logger.info(msgPrefix + "Received run number: " + rnd.getRunNumber()
                + " and sequence number: " + rnd.getSequenceNumber()
                + " for session ID: " + rnd.getSessionIdentifier());

            return rnd;
        }
    }

    public static String getBuildVersion()
    {
        return GEMFunctionManager.class.getPackage().getImplementationVersion();
    }

    /**
     * Whether the system is running in degraded mode or not
     */
    public boolean isDegraded()
    {
        return degraded;
    }

    /**
     * Whether the system has a soft error or not
     */
    public boolean hasSoftError()
    {
        return softErrorDetected;
    }

    /**
     * Set the system in degraded running mode
     */
    public void setDegraded(boolean degraded)
    {
        this.degraded = degraded;
    }

    /**
     * Set the system in soft error running mode
     */
    public void setSoftErrorDetected(boolean softErrorDetected)
    {
        this.softErrorDetected = softErrorDetected;
    }

    /**
     * Report error
     */
    protected void sendCMSError(String errMessage)
    {
        String msgPrefix = "[GEMFM::" + m_FMname + "] GEMFunctionManager::sendCMSError(): ";

        // create a new error notification msg
        CMSError error = getErrorFactory().getCMSError();
        error.setDateTime(new Date().toString());
        error.setMessage(errMessage);

        // update error msg parameter for GUI
        getParameterSet().get(GEMParameters.ERROR_MSG).setValue(new StringT(errMessage));

        // send error
        try {
            getParentErrorNotifier().sendError(error);
        } catch (Exception e) {
            logger.warn(msgPrefix + "" + getClass().toString() + ": Failed to send error message " + errMessage);
        }
    }

    /**
     * Set the current action
     */
    public void setAction(String actionMessage)
    {
        getParameterSet().put(new FunctionManagerParameter<StringT>("ACTION_MSG", new StringT(actionMessage)));
    }

    /**
     * Set the error message
     */
    public void setError(String errorMessage)
    {
        getParameterSet().put(new FunctionManagerParameter<StringT>("ERROR_MSG", new StringT(errorMessage)));
    }

    /**
     * Go to the error state, with exception
     */
    public void goToError(String errorMessage, Exception e)
    {
        errorMessage += "\nMessage from the caught exception is: " + e.getMessage();
        goToError(errorMessage);
    }

    /**
     * Go to the error state, without exception
     */
    public void goToError(String errorMessage)
    {
        // Log the error
        logger.error(errorMessage);
        sendCMSError(errorMessage);

        // Present message to the user
        getParameterSet().put(new FunctionManagerParameter<StringT>("STATE", new StringT("Error")));
        setAction("In Error!");
        setError(errorMessage);

        // Move to the Error state
        Input errInput = new Input(GEMInputs.SETERROR);
        errInput.setReason(errorMessage);
        fireEvent(errInput);
    }

    /**
     * Get all XDAQ executives and kill them
     */
    protected void destroyXDAQ()
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_FMname + "] GEMFunctionManager::destroyXDAQ(): ";

        logger.info(msgPrefix + "Destroying the Qualified Group");
        try {
            getQualifiedGroup().destroy();
        } catch (Exception e) {
            goToError("Could not destroy the Qualified Group", e);
        }

        // Reset the QualifiedGroup so that the next time an init is sent all resources will be initialized again
        logger.info(msgPrefix + "Resetting the QualifiedGroup");
        if (m_gemQG != null) {
            m_gemQG.reset();
        }

        logger.info(msgPrefix + "Done");
    }

    /*
     * (non-Javadoc)
     *
     * @see rcms.statemachine.user.IUserStateMachine#getUpdatedState()
     */
    public State getUpdatedState()
    {
        String msgPrefix = "[GEMFM::" + m_FMname + "] GEMFunctionManager::getUpdatedState(): ";

        try {
            // This method is called by the framework when a Function Manager is
            // created (after createAction).
            // It can be used for updating the state of the Function Manager
            // according to the resources' state.
            //
            // In this example it is used also to retrieve the updated State of all
            // resources
            // and recalculate the State Machine state.

            if (m_gemQG == null || !m_gemQG.isInitialized()) {
                // if the QualifiedGroup is not initialized just return the FSM
                // state.
                return this.getState();
            }

            // inquiry the state of the resources
            // m_gemQG.getStates();
            // recalculate the state
            if (m_svCalc != null) {
                m_calcState = m_svCalc.getState();
            }
        } catch (Exception e) {
            logger.error(msgPrefix + "Caught exception: " + e);
        }

        return m_calcState;
    }

    /**
     * Retrieves the function manager property named @c name from the ressource DB, if available
     */
    public String getFunctionManagerProperty(String name)
    {
        String value = null;

        List<ConfigProperty> properties = getGroup().getThisResource().getProperties();

        if (properties != null) {
            for (ConfigProperty property : properties) {
                if (property.getName().equals(name)) {
                    value = property.getValue();
                    break;
                }
            }
        }

        return value;
    }

    /**
     * Calculation of State derived from the State of the controlled resources
     */
    public void defineConditionState()
    {
        String msgPrefix = "[GEMFM::" + m_FMname + "] GEMFunctionManager::defineConditionState(): ";

        // Initial state
        StateVector initialConds = new StateVector();
        initialConds.registerConditionState(c_gemSupervisors, GEMStates.INITIAL);
        initialConds.setResultState(GEMStates.INITIAL);

        // Halted state
        StateVector haltedConds = new StateVector();
        haltedConds.registerConditionState(c_gemSupervisors, GEMStates.HALTED);
        haltedConds.setResultState(GEMStates.HALTED);

        // Error state
        StateVector errorConds = new StateVector();
        errorConds.registerConditionState(c_gemSupervisors, GEMStates.ERROR);
        errorConds.setResultState(GEMStates.ERROR);

        // Configured state
        StateVector configuredConds = new StateVector();
        configuredConds.registerConditionState(c_gemSupervisors, GEMStates.CONFIGURED);
        configuredConds.setResultState(GEMStates.CONFIGURED);

        // Paused state
        StateVector pausedConds = new StateVector();
        pausedConds.registerConditionState(c_gemSupervisors, GEMStates.PAUSED);
        pausedConds.setResultState(GEMStates.PAUSED);

        // Running state
        StateVector runningConds = new StateVector();
        runningConds.registerConditionState(c_gemSupervisors, GEMStates.RUNNING);
        runningConds.setResultState(GEMStates.RUNNING);

        // // RunningDegraded state
        // StateVector runningdegradedConds = new StateVector();
        // runningdegradedConds.registerConditionState(c_gemSupervisors,  GEMStates.RUNNINGDEGRADED);
        // runningdegradedConds.setResultState(GEMStates.RUNNINGDEGRADED);

        // // RunningSoftErrorDetected state
        // StateVector runningsofterrordetectedConds = new StateVector();
        // runningsofterrordetectedConds.registerConditionState(c_gemSupervisors,  GEMStates.RUNNINGSOFTERRORDETECTED);
        // runningsofterrordetectedConds.setResultState(GEMStates.RUNNINGSOFTERRORDETECTED);

        // Add the conditions and all the resources belonging to this group to the object that calculates the state
        Set resourceGroup = m_gemQG.getQualifiedResourceGroup();
        m_svCalc = new StateVectorCalculation(resourceGroup);
        m_svCalc.add(initialConds);
        m_svCalc.add(haltedConds);
        m_svCalc.add(configuredConds);
        m_svCalc.add(runningConds);
        m_svCalc.add(errorConds);
    }
}
