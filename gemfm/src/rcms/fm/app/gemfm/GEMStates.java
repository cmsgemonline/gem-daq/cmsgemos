/**
 * @author Andrea Petrucci, Alexander Oh, Michele Gulmini, Hannes Sakulin, Jose Ruiz, Jared Sturdy
 */

package rcms.fm.app.gemfm;

import rcms.statemachine.definition.State;

/**
 * GEM Level-1 Function Manager states
 */
public final class GEMStates {

    // Steady states
    public static final State INITIAL = new State("Initial");

    public static final State HALTED = new State("Halted");

    public static final State ENABLED = new State("Enabled"); // Lifetime only

    public static final State CONFIGURED = new State("Configured");

    public static final State PAUSED = new State("Paused");

    public static final State ERROR = new State("Error");

    // Transitional states
    public static final State INITIALIZING = new State("Initializing");

    public static final State RESETTING = new State("Resetting");

    public static final State RECOVERING = new State("Recovering");

    public static final State COLDRESETTING = new State("ColdResetting");

    public static final State CONFIGURING = new State("Configuring");
    public static final State HALTING = new State("Halting");

    public static final State STARTING = new State("Starting");
    public static final State STOPPING = new State("Stopping");

    public static final State PAUSING = new State("Pausing");
    public static final State RESUMING = new State("Resuming");

    // Steady running states
    public static final State RUNNING = new State("Running");
    public static final State RUNNINGDEGRADED = new State("RunningDegraded");
    public static final State RUNNINGSOFTERRORDETECTED = new State("RunningSoftErrorDetected");

    // Transitional running states
    public static final State FIXINGSOFTERROR = new State("FixingSoftError");

    // Steady special states
    public static final State TTSTEST_MODE = new State("TTSTestMode");

    // Transitional special states
    public static final State PREPARING_TTSTEST_MODE = new State("PreparingTTSTestMode");

    public static final State TESTING_TTS = new State("TestingTTS");
}
