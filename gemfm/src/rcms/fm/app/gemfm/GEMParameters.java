/**
 * @author Andrea Petrucci, Alexander Oh, Michele Gulmini, Hannes Sakulin, Jose Ruiz, Jared Sturdy
 */

package rcms.fm.app.gemfm;

import rcms.fm.fw.parameter.CommandParameter;
import rcms.fm.fw.parameter.CommandParameter.Required;
import rcms.fm.fw.parameter.FunctionManagerParameter;
import rcms.fm.fw.parameter.FunctionManagerParameter.Exported;
import rcms.fm.fw.parameter.ParameterSet;
import rcms.fm.fw.parameter.type.BooleanT;
import rcms.fm.fw.parameter.type.DoubleT;
import rcms.fm.fw.parameter.type.IntegerT;
import rcms.fm.fw.parameter.type.StringT;

/**
 * GEM Level-1 Function Manager parameters
 *
 * Standard parameter definitions for Level 1 Function Manager
 *
 * STATE:      State name the function manager is currently in
 * ACTION_MSG: Short description of current activity, if any
 * ERROR_MSG:  In case of an error contains a description of the error
 * COMPLETION: Completion of an activity can be signaled through this numerical value 0 < PROGRESS_BAR < 1
 *
 * For more details => https://twiki.cern.ch/twiki/bin/view/CMS/StdFMParameters
 */
public class GEMParameters {

    public static final String STATE = "STATE";

    public static final String ACTION_MSG = "ACTION_MSG";
    public static final String ERROR_MSG = "ERROR_MSG";
    public static final String COMPLETION = "COMPLETION";

    public static final String GEM_RUN_TYPE = "GEM_RUN_TYPE";

    // Command parameters for Initialize
    public static final String SID = "SID";
    public static final String GLOBAL_CONF_KEY = "GLOBAL_CONF_KEY";

    // Command parameters for Configure
    public static final String RUN_NUMBER = "RUN_NUMBER"; // Also for Start
    public static final String RUN_KEY = "RUN_KEY";
    public static final String FED_ENABLE_MASK = "FED_ENABLE_MASK";
    public static final String USE_PRIMARY_TCDS = "USE_PRIMARY_TCDS";

    // Command parameters for FixSoftError
    public static final String TRIGGER_NUMBER_AT_PAUSE = "TRIGGER_NUMBER_AT_PAUSE";

    // Command parameters for TestTTS
    public static final String TTS_TEST_FED_ID = "TTS_TEST_FED_ID";
    public static final String TTS_TEST_TYPE = "TTS_TEST_TYPE";
    public static final String TTS_TEST_PATTERN = "TTS_TEST_PATTERN";
    public static final String TTS_TEST_CYCLES = "TTS_TEST_CYCLES";

    // To be exported after Initialize
    public static final String INITIALIZED_WITH_SID = "INITIALIZED_WITH_SID";
    public static final String INITIALIZED_WITH_GLOBAL_CONF_KEY = "INITIALIZED_WITH_GLOBAL_CONF_KEY";

    // To be exported after Configure
    public static final String CONFIGURED_WITH_RUN_NUMBER = "CONFIGURED_WITH_RUN_NUMBER";
    public static final String CONFIGURED_WITH_RUN_KEY = "CONFIGURED_WITH_RUN_KEY";
    public static final String CONFIGURED_WITH_FED_ENABLE_MASK = "CONFIGURED_WITH_FED_ENABLE_MASK";

    // To be exported after Start
    public static final String STARTED_WITH_RUN_NUMBER = "STARTED_WITH_RUN_NUMBER";

    // Standard Level-1 parameter set
    public static final ParameterSet<FunctionManagerParameter> LVL_ONE_PARAMETER_SET = new ParameterSet<FunctionManagerParameter>();

    static
    {

        /*
         * State of the Function Manager is currently in
         */
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(STATE, new StringT("Initial"),
            Exported.READONLY));

        /*
         * Parameters for monitoring
         */
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(ACTION_MSG, new StringT("Created! GEM says Hello World!"),
            Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(ERROR_MSG, new StringT(""),
            Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<DoubleT>(COMPLETION, new DoubleT(0),
            Exported.READONLY));

        /*
         * Configuration parameters
         */
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(GEM_RUN_TYPE, new StringT(""),
            Exported.READONLY));

        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<IntegerT>(SID, new IntegerT(0),
            Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(GLOBAL_CONF_KEY, new StringT(""),
            Exported.READONLY));

        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<IntegerT>(RUN_NUMBER, new IntegerT(0),
            Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(RUN_KEY, new StringT(""),
            Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(FED_ENABLE_MASK, new StringT("%"),
            Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<BooleanT>(USE_PRIMARY_TCDS, new BooleanT(true),
            Exported.READONLY));

        /*
         * Exported parameters for cross-checks
         */
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<IntegerT>(INITIALIZED_WITH_SID, new IntegerT(0),
            Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(INITIALIZED_WITH_GLOBAL_CONF_KEY, new StringT(""),
            Exported.READONLY));

        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<IntegerT>(CONFIGURED_WITH_RUN_NUMBER, new IntegerT(0),
            Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(CONFIGURED_WITH_RUN_KEY, new StringT(""),
            Exported.READONLY));
        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<StringT>(CONFIGURED_WITH_FED_ENABLE_MASK, new StringT(""),
            Exported.READONLY));

        LVL_ONE_PARAMETER_SET.put(new FunctionManagerParameter<IntegerT>(STARTED_WITH_RUN_NUMBER, new IntegerT(0),
            Exported.READONLY));
    }
}
