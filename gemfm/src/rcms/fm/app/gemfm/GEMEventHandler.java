/*
 * @author Andrea Petrucci, Alexander Oh, Michele Gulmini, Jose Ruiz, Jared Sturdy
 */

package rcms.fm.app.gemfm;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Double;
import java.lang.Integer;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import net.hep.cms.xdaqctl.XDAQException;
import net.hep.cms.xdaqctl.XDAQMessageException;
import net.hep.cms.xdaqctl.XDAQTimeoutException;
import rcms.errorFormat.CMS.CMSError;
import rcms.fm.fw.StateEnteredEvent;
import rcms.fm.fw.parameter.CommandParameter;
import rcms.fm.fw.parameter.FunctionManagerParameter;
import rcms.fm.fw.parameter.ParameterSet;
import rcms.fm.fw.parameter.type.BooleanT;
import rcms.fm.fw.parameter.type.DoubleT;
import rcms.fm.fw.parameter.type.IntegerT;
import rcms.fm.fw.parameter.type.LongT;
import rcms.fm.fw.parameter.type.StringT;
import rcms.fm.fw.parameter.type.VectorT;
import rcms.fm.fw.user.UserActionException;
import rcms.fm.fw.user.UserStateNotificationHandler;
import rcms.fm.resource.QualifiedGroup;
import rcms.fm.resource.QualifiedResource;
import rcms.fm.resource.QualifiedResourceContainer;
import rcms.fm.resource.QualifiedResourceContainerException;
import rcms.fm.resource.qualifiedresource.FunctionManager;
import rcms.fm.resource.qualifiedresource.XdaqApplication;
import rcms.fm.resource.qualifiedresource.XdaqApplicationContainer;
import rcms.fm.resource.qualifiedresource.XdaqExecutive;
import rcms.fm.resource.qualifiedresource.XdaqExecutiveConfiguration;
import rcms.resourceservice.db.resource.fm.FunctionManagerResource;
import rcms.stateFormat.StateNotification;
import rcms.statemachine.definition.Input;
import rcms.statemachine.definition.State;
import rcms.statemachine.definition.StateMachineDefinitionException;
import rcms.util.logger.RCMSLogger;
import rcms.util.logsession.LogSessionConnector;
import rcms.util.logsession.LogSessionException;
import rcms.utilities.fm.task.SimpleTask;
import rcms.utilities.fm.task.Task;
import rcms.utilities.fm.task.TaskSequence;
import rcms.utilities.runinfo.RunNumberData;
import rcms.xdaqctl.XDAQMessage;
import rcms.xdaqctl.XDAQParameter;

/**
 * Event handler for the GEM Level-1 Function Manager
 */
public class GEMEventHandler extends UserStateNotificationHandler {

    /**
     * RCMS log4j logger
     */
    static RCMSLogger logger = new RCMSLogger(GEMEventHandler.class);

    /**
     * GEM FunctionManager
     */
    private GEMFunctionManager m_gemFM = null;

    /**
     * ParameterSet of the GEM FM
     */
    private ParameterSet<FunctionManagerParameter> m_gemPSet = null;

    /**
     * QualifiedGroup of the GEM FM
     */
    private QualifiedGroup m_gemQG = null;

    private TaskSequence m_taskSequence = null;

    private boolean m_lifetimeOnly = false;

    /**
     * Default constructor
     */
    public GEMEventHandler(boolean lifetimeOnly)
        throws rcms.fm.fw.EventHandlerException
    {
        // String msgPrefix = "[GEMFM:: " + ((GEMFunctionManager)getUserFunctionManager()).m_FMname
        String msgPrefix = "[GEMFM:: (pre-init)"
            + "] GEMEventHandler::GEMEventHandler(): ";

        m_lifetimeOnly = lifetimeOnly;

        // this handler inherits UserStateNotificationHandler
        // so it is already registered for StateNotification events
        logger.info(msgPrefix + "Starting");

        // Let's register also the StateEnteredEvent triggered when the FSM enters in a new state.
        logger.info(msgPrefix + "Subscribing StateEnteredEvent events");
        subscribeForEvents(StateEnteredEvent.class);

        logger.info(msgPrefix + "Adding action callbacks");

        // No action on GEMStates.INITIAL
        addAction(GEMStates.HALTED, "onSteadyState");
        addAction(GEMStates.ENABLED, "onSteadyState");
        addAction(GEMStates.CONFIGURED, "onSteadyState");
        addAction(GEMStates.PAUSED, "onSteadyState");
        addAction(GEMStates.ERROR, "onSteadyState");

        addAction(GEMStates.INITIALIZING, "onInitializing");
        addAction(GEMStates.RESETTING, "onResetting");
        addAction(GEMStates.RECOVERING, "onRecovering");
        addAction(GEMStates.COLDRESETTING, "onColdResetting");
        addAction(GEMStates.CONFIGURING, "onConfiguring");
        addAction(GEMStates.HALTING, "onHalting");
        addAction(GEMStates.STARTING, "onStarting");
        addAction(GEMStates.STOPPING, "onStopping");
        addAction(GEMStates.PAUSING, "onPausing");
        addAction(GEMStates.RESUMING, "onResuming");

        addAction(GEMStates.RUNNING, "onRunning");
        addAction(GEMStates.RUNNINGDEGRADED, "onRunningDegraded");
        addAction(GEMStates.RUNNINGSOFTERRORDETECTED, "onRunningSoftErrorDetected");

        addAction(GEMStates.FIXINGSOFTERROR, "onFixingSoftError");

        addAction(GEMStates.TTSTEST_MODE, "onSteadyState");

        addAction(GEMStates.PREPARING_TTSTEST_MODE, "onPreparingTTSTestMode");
        addAction(GEMStates.TESTING_TTS, "onTestingTTS");

        logger.info(msgPrefix + "Done");
    }

    /**
     * Late initialization
     */
    public void init()
        throws rcms.fm.fw.EventHandlerException
    {
        m_gemFM = (GEMFunctionManager)getUserFunctionManager();

        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::init(): ";

        m_gemQG = m_gemFM.getQualifiedGroup();
        m_gemPSet = (ParameterSet<FunctionManagerParameter>)m_gemFM.getParameterSet();

        logger.debug(msgPrefix + "Done");
    }

    /**
     * Generic actions to be performed in steady states
     */
    public void onSteadyState(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onSteadyState(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            if (m_gemFM.getState() == GEMStates.ENABLED)
                return; // This is a lifetime only FM state

            logger.info(msgPrefix + "Received state notification");

            m_gemFM.goToError("Received spontaneous state transition: " + printStateChangedNotice((StateNotification)obj));

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            String currentState = m_gemFM.getState().toString();
            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT(currentState)));

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     * Actions to be performed in the Initializing state
     */
    public void onInitializing(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onInitializing(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            processNotice((StateNotification)obj);

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("Initializing")));

            /*
             * Get parameters from the command
             */
            m_gemFM.setAction("Retrieving parameters from command");

            Integer sid = 0;
            String globalConfKey = "";
            String runType = "";

            try {
                ParameterSet<CommandParameter> inputParSet = m_gemFM.getLastInput().getParameterSet();
                logger.info(msgPrefix + "Input parameters = " + inputParSet.getNames());

                if (inputParSet.size() == 0 || inputParSet.get(GEMParameters.SID) == null) {
                    // No parameters from the L0 FM, we are in local
                    runType = "local";

                    // Create a new session
                    sid = m_gemFM.getSessionId();
                } else {
                    // Got parameters from the L0 FM, we are in global
                    runType = "global";

                    // Extract parameters from the command
                    sid = ((IntegerT)inputParSet.get(GEMParameters.SID).getValue()).getInteger();
                    globalConfKey = ((StringT)inputParSet.get(GEMParameters.GLOBAL_CONF_KEY).getValue()).getString();
                }
            } catch (Exception e) {
                // Go to error, we require parameters
                m_gemFM.goToError("Error reading parameters of Initialize command", e);
                return;
            }

            // Fill the function manager parameter set
            m_gemPSet.put(new FunctionManagerParameter<IntegerT>(GEMParameters.SID, new IntegerT(sid)));
            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.GLOBAL_CONF_KEY, new StringT(globalConfKey)));
            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.GEM_RUN_TYPE, new StringT(runType)));

            m_gemPSet.put(new FunctionManagerParameter<IntegerT>(GEMParameters.INITIALIZED_WITH_SID, new IntegerT(sid)));
            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.INITIALIZED_WITH_GLOBAL_CONF_KEY, new StringT(globalConfKey)));

            // Add the SID as a registry entry to the QualifiedGroup
            // The registry entry is merely a dictionary used to store information about the QG
            // The SID is used to spawn new xDAQ executives
            if (m_gemQG.getRegistryEntry(GEMParameters.SID) == null) {
                m_gemQG.putRegistryEntry(GEMParameters.SID, sid.toString());
            }

            logger.info(msgPrefix + "SID of QualifiedGroup is " + m_gemQG.getRegistryEntry(GEMParameters.SID));

            /*
             * Initialize the Qualified Group
             */
            m_gemFM.setAction("Initializing xDAQ applications");

            try {
                logger.info(msgPrefix + "Initializing Qualified Group\n" + m_gemQG.print());
                m_gemQG.init();
            } catch (Exception e) {
                m_gemFM.goToError("Could not initialize xDAQ applications", e);
                return;
            }

            logger.info(msgPrefix + "Qualified Group initialized");

            // Move to "Enabled" when the FM is lifetime only
            if (m_lifetimeOnly) {
                m_gemFM.setAction("Preparing tasks");
                TaskSequence enableTaskSeq = new TaskSequence(GEMStates.INITIALIZING, GEMInputs.SETENABLED);
                executeTaskSequence(enableTaskSeq);
                logger.info(msgPrefix + "Done");
                return;
            }

            /*
             * Look for GEM supervisors
             */
            m_gemFM.setAction("Retrieving GEM supervisors");

            List<QualifiedResource> xdaqAppList = m_gemQG.seekQualifiedResourcesOfType(new XdaqApplication());
            XdaqApplicationContainer xdaqApps = new XdaqApplicationContainer(xdaqAppList);
            m_gemFM.c_gemSupervisors = new XdaqApplicationContainer(xdaqApps.getApplicationsOfClass("gem::supervisor::GEMSupervisor"));

            if (m_gemFM.c_gemSupervisors.isEmpty()) {
                logger.warn(msgPrefix + "No GEM supervisor found!");
            } else {
                logger.info(msgPrefix + "GEM supervisor found! Welcome to GEM xDAQ control :)");

                logger.info(msgPrefix + "Checking for asynchronous SOAP communication with GEMSupervisor");
                for (QualifiedResource qr : m_gemFM.c_gemSupervisors.getApplications()) {
                    String doWeHaveAsyncgemSupervisor = "false";
                    try {
                        XDAQParameter pam = ((XdaqApplication)qr).getXDAQParameter();
                        pam.select(new String[] { "ReportStateToRCMS" });
                        pam.get();
                        doWeHaveAsyncgemSupervisor = pam.getValue("ReportStateToRCMS");
                        logger.info(msgPrefix + "ReportStateToRCMS results is: " + doWeHaveAsyncgemSupervisor);
                    } catch (XDAQTimeoutException e) {
                        String msg = "Timeout when checking the asynchronous SOAP capabilities of " + qr.getURI()
                            + "\nPerhaps the GEM supervisor application is dead?";
                        m_gemFM.goToError(msg, e);
                    } catch (XDAQException e) {
                        String msg = "Error when checking the asynchronous SOAP capabilities of " + qr.getURI();
                        m_gemFM.goToError(msg, e);
                    }

                    if (!doWeHaveAsyncgemSupervisor.equals("true"))
                        m_gemFM.goToError("GEM supervisor configured without asynchronous SOAP capabilites: " + qr.getURI());
                }
            }

            logger.info(msgPrefix + "GEM supervisors ready");

            /*
             * Prepare task sequence
             */
            m_gemFM.setAction("Preparing tasks");

            TaskSequence initTaskSeq = new TaskSequence(GEMStates.INITIALIZING, GEMInputs.SETHALTED);

            if (m_gemFM.c_gemSupervisors != null && !m_gemFM.c_gemSupervisors.isEmpty()) {
                Input initInputGEMSuper = new Input(GEMInputs.INITIALIZE.toString());
                ParameterSet<CommandParameter> pSet = new ParameterSet<CommandParameter>();
                initInputGEMSuper.setParameters(pSet);
                SimpleTask gemSuperInitTask = new SimpleTask(m_gemFM.c_gemSupervisors, initInputGEMSuper, GEMStates.INITIALIZING, GEMStates.HALTED, "Initializing GEM supervisors");
                initTaskSeq.addLast(gemSuperInitTask);
            }

            executeTaskSequence(initTaskSeq);

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     * Actions to be performed in the Resetting state
     */
    public void onResetting(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onResetting(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            processNotice((StateNotification)obj);

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("Resetting")));

            /*
             * Not implemented
             */
            m_gemFM.goToError("Reset action not implemented");

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     * Actions to be performed in the Recovering state
     */
    public void onRecovering(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onRecovering(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            processNotice((StateNotification)obj);

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("Recovering")));

            /*
             * Not implemented
             */
            m_gemFM.goToError("Recover action not implemented");

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     * Actions to be performed in the ColdResetting state
     */
    public void onColdResetting(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onColdResetting(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            processNotice((StateNotification)obj);

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("ColdResetting")));

            /*
             * Not implemented, go back straight to Halted
             */
            m_gemFM.fireEvent(GEMInputs.SETHALTED);

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     * Actions to be performed in the Configuring state
     */
    public void onConfiguring(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onConfiguring(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            processNotice((StateNotification)obj);

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("Configuring")));

            /*
             * Get parameters from the command
             */
            m_gemFM.setAction("Retrieving parameters from command");

            Integer runNumber = 0;
            String runKey = "";
            String fedEnableMask = "%";
            Boolean usePrimaryTCDS = true;

            String runType = ((StringT)m_gemPSet.get(GEMParameters.GEM_RUN_TYPE).getValue()).getString();
            if (!runType.equals("local")) {
                try {
                    ParameterSet<CommandParameter> inputParSet = m_gemFM.getLastInput().getParameterSet();
                    logger.info(msgPrefix + "Input parameters = " + inputParSet.getNames());

                    runNumber = ((IntegerT)inputParSet.get(GEMParameters.RUN_NUMBER).getValue()).getInteger();
                    runKey = ((StringT)inputParSet.get(GEMParameters.RUN_KEY).getValue()).getString();
                    fedEnableMask = ((StringT)inputParSet.get(GEMParameters.FED_ENABLE_MASK).getValue()).getString();
                    usePrimaryTCDS = ((BooleanT)inputParSet.get(GEMParameters.USE_PRIMARY_TCDS).getValue()).getBoolean();
                } catch (Exception e) {
                    // Go to error, we require parameters
                    m_gemFM.goToError("Error reading parameters of Configure command", e);
                    return;
                }
            }

            // Fill the function manager parameter set
            // We do NOT set RUN_NUMBER since it is irrelevant for GEM applications until Start
            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.RUN_KEY, new StringT(runKey)));
            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.FED_ENABLE_MASK, new StringT(fedEnableMask)));
            m_gemPSet.put(new FunctionManagerParameter<BooleanT>(GEMParameters.USE_PRIMARY_TCDS, new BooleanT(usePrimaryTCDS)));

            m_gemPSet.put(new FunctionManagerParameter<IntegerT>(GEMParameters.CONFIGURED_WITH_RUN_NUMBER, new IntegerT(runNumber)));
            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.CONFIGURED_WITH_RUN_KEY, new StringT(runKey)));
            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.CONFIGURED_WITH_FED_ENABLE_MASK, new StringT(fedEnableMask)));

            /*
             * Prepare task sequence
             */
            TaskSequence confTaskSeq = new TaskSequence(GEMStates.CONFIGURING, GEMInputs.SETCONFIGURED);

            if (m_gemFM.c_gemSupervisors != null && !m_gemFM.c_gemSupervisors.isEmpty()) {
                // Send configuration parameters
                m_gemFM.setAction("Sending configuration parameters to the GEM supervisors");

                for (QualifiedResource qr : m_gemFM.c_gemSupervisors.getApplications()) {
                    try {
                        XDAQParameter pam = ((XdaqApplication)qr).getXDAQParameter();
                        pam.setValue("isLocalRunNumber", "false");
                        pam.setValue("FEDEnableMask", fedEnableMask);
                        pam.setValue("UsePrimaryTCDS", usePrimaryTCDS.toString());
                        logger.info(msgPrefix + "configuring the supervior for global run numbers");
                        logger.info(msgPrefix + "sending FEDEnableMask to the supervisor " + fedEnableMask);
                        logger.info(msgPrefix + "sending UsePrimaryTCDS to the supervisor " + usePrimaryTCDS);
                        pam.send();
                    } catch (XDAQTimeoutException e) {
                        m_gemFM.goToError("Error! Timeout when trying to send the parameters to the GEM supervisor\n.Perhaps this application is dead!?", e);
                        return;
                    } catch (Exception e) {
                        m_gemFM.goToError("Error! XDAQException when trying to send the paramters to the GEM supervisor", e);
                        return;
                    }
                }

                // Prepare the command
                m_gemFM.setAction("Preparing tasks");

                Input confInputGEMSuper = new Input(GEMInputs.CONFIGURE.toString());
                ParameterSet<CommandParameter> pSet = new ParameterSet<CommandParameter>();
                confInputGEMSuper.setParameters(pSet);
                SimpleTask gemSuperConfTask = new SimpleTask(m_gemFM.c_gemSupervisors, confInputGEMSuper, GEMStates.CONFIGURING, GEMStates.CONFIGURED, "Configuring GEM supervisors");
                confTaskSeq.addLast(gemSuperConfTask);
            }

            executeTaskSequence(confTaskSeq);

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     * Actions to be performed in the Halting state
     */
    public void onHalting(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onHalting(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            processNotice((StateNotification)obj);

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("Halting")));

            /*
             * Prepare task sequence
             */
            m_gemFM.setAction("Preparing tasks");

            TaskSequence haltTaskSeq = new TaskSequence(GEMStates.HALTING, GEMInputs.SETHALTED);

            if (m_gemFM.c_gemSupervisors != null && !m_gemFM.c_gemSupervisors.isEmpty()) {
                Input haltInputGEMSuper = new Input(GEMInputs.HALT.toString());
                ParameterSet<CommandParameter> pSet = new ParameterSet<CommandParameter>();
                haltInputGEMSuper.setParameters(pSet);
                SimpleTask gemSuperHaltTask = new SimpleTask(m_gemFM.c_gemSupervisors, haltInputGEMSuper, GEMStates.HALTING, GEMStates.HALTED, "Halting GEM supervisors");
                haltTaskSeq.addLast(gemSuperHaltTask);
            }

            executeTaskSequence(haltTaskSeq);

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     * Actions to be performed in the Starting state
     */
    public void onStarting(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onStarting(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            processNotice((StateNotification)obj);

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("Starting")));

            /*
             * Get parameters from the command
             */
            m_gemFM.setAction("Retrieving parameters from command");

            Integer runNumber = 0;

            String runType = ((StringT)m_gemPSet.get(GEMParameters.GEM_RUN_TYPE).getValue()).getString();
            if (!runType.equals("local")) {
                try {
                    ParameterSet<CommandParameter> inputParSet = m_gemFM.getLastInput().getParameterSet();

                    runNumber = ((IntegerT)inputParSet.get(GEMParameters.RUN_NUMBER).getValue()).getInteger();
                } catch (Exception e) {
                    // Go to error, we require parameters
                    m_gemFM.goToError("Error reading parameters of Start command", e);
                    return;
                }
            } else {
                if (m_gemFM.getRunInfoConnector() != null) {
                    RunNumberData rnd = m_gemFM.getOfficialRunNumber();
                    runNumber = rnd.getRunNumber();
                } else {
                    logger.error(msgPrefix + "Official RunNumber requested, but cannot establish RunInfo Connection. Is there a RunInfo DB? or is RunInfo DB down?");
                    logger.info(msgPrefix + "Going to use run number =" + runNumber);
                }
            }

            // Fill the function manager parameter set
            m_gemPSet.put(new FunctionManagerParameter<IntegerT>(GEMParameters.RUN_NUMBER, new IntegerT(runNumber)));

            m_gemPSet.put(new FunctionManagerParameter<IntegerT>(GEMParameters.STARTED_WITH_RUN_NUMBER, new IntegerT(runNumber)));

            logger.info(msgPrefix + "runNumber is " + runNumber);

            /*
             * Prepare task sequence
             */
            TaskSequence startTaskSeq = new TaskSequence(GEMStates.STARTING, GEMInputs.SETRUNNING);

            if (m_gemFM.c_gemSupervisors != null && !m_gemFM.c_gemSupervisors.isEmpty()) {
                // Send configuration parameters
                m_gemFM.setAction("Sending configuration parameters to the GEM supervisors");

                for (QualifiedResource qr : m_gemFM.c_gemSupervisors.getApplications()) {
                    try {
                        XDAQParameter pam = ((XdaqApplication)qr).getXDAQParameter();
                        pam.setValue("runNumber", runNumber.toString());
                        logger.info(msgPrefix + "sending global run number to the supervisor: " + runNumber);
                        pam.send();
                    } catch (XDAQTimeoutException e) {
                        m_gemFM.goToError("Error! Timeout when trying to send the parameters to the GEM supervisor\n.Perhaps this application is dead!?", e);
                        return;
                    } catch (Exception e) {
                        m_gemFM.goToError("Error! XDAQException when trying to send the parameters to the GEM supervisor", e);
                        return;
                    }
                }

                // Prepare the command
                m_gemFM.setAction("Preparing tasks");

                Input startInputGEMSuper = new Input(GEMInputs.START.toString());
                ParameterSet<CommandParameter> pSet = new ParameterSet<CommandParameter>();
                startInputGEMSuper.setParameters(pSet);
                SimpleTask gemSuperStartTask = new SimpleTask(m_gemFM.c_gemSupervisors, startInputGEMSuper, GEMStates.STARTING, GEMStates.RUNNING, "Starting GEM supervisors");
                startTaskSeq.addLast(gemSuperStartTask);
            }

            executeTaskSequence(startTaskSeq);

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     * Actions to be performed in the Stopping state
     */
    public void onStopping(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onStopping(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            processNotice((StateNotification)obj);

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("Stopping")));

            /*
             * Prepare task sequence
             */
            m_gemFM.setAction("Preparing tasks");

            TaskSequence stopTaskSeq = new TaskSequence(GEMStates.STOPPING, GEMInputs.SETCONFIGURED);

            if (m_gemFM.c_gemSupervisors != null && !m_gemFM.c_gemSupervisors.isEmpty()) {
                Input stopInputGEMSuper = new Input(GEMInputs.STOP.toString());
                ParameterSet<CommandParameter> pSet = new ParameterSet<CommandParameter>();
                stopInputGEMSuper.setParameters(pSet);
                SimpleTask gemSuperStopTask = new SimpleTask(m_gemFM.c_gemSupervisors, stopInputGEMSuper, GEMStates.STOPPING, GEMStates.CONFIGURED, "Stopping GEM supervisors");
                stopTaskSeq.addLast(gemSuperStopTask);
            }

            executeTaskSequence(stopTaskSeq);

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     * Actions to be performed in the Pausing state
     */
    public void onPausing(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onPausing(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            processNotice((StateNotification)obj);

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("Pausing")));

            /*
             * Prepare task sequence
             */
            m_gemFM.setAction("Preparing tasks");

            TaskSequence pauseTaskSeq = new TaskSequence(GEMStates.PAUSING, GEMInputs.SETPAUSED);

            if (m_gemFM.c_gemSupervisors != null && !m_gemFM.c_gemSupervisors.isEmpty()) {
                Input pauseInputGEMSuper = new Input(GEMInputs.PAUSE.toString());
                ParameterSet<CommandParameter> pSet = new ParameterSet<CommandParameter>();
                pauseInputGEMSuper.setParameters(pSet);
                SimpleTask gemSuperPauseTask = new SimpleTask(m_gemFM.c_gemSupervisors, pauseInputGEMSuper, GEMStates.PAUSING, GEMStates.PAUSED, "Pausing GEM supervisors");
                pauseTaskSeq.addLast(gemSuperPauseTask);
            }

            executeTaskSequence(pauseTaskSeq);

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     * Actions to be performed in the Resuming state
     */
    public void onResuming(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onResuming(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            processNotice((StateNotification)obj);

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("Resuming")));

            /*
             * Prepare task sequence
             */
            m_gemFM.setAction("Preparing tasks");

            TaskSequence resumeTaskSeq = new TaskSequence(GEMStates.RESUMING, GEMInputs.SETRUNNING);

            if (m_gemFM.c_gemSupervisors != null && !m_gemFM.c_gemSupervisors.isEmpty()) {
                Input resumeInputGEMSuper = new Input(GEMInputs.RESUME.toString());
                ParameterSet<CommandParameter> pSet = new ParameterSet<CommandParameter>();
                resumeInputGEMSuper.setParameters(pSet);
                SimpleTask gemSuperResumeTask = new SimpleTask(m_gemFM.c_gemSupervisors, resumeInputGEMSuper, GEMStates.RESUMING, GEMStates.RUNNING, "Resuming GEM supervisors");
                resumeTaskSeq.addLast(gemSuperResumeTask);
            }

            executeTaskSequence(resumeTaskSeq);

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     * Actions to be performed in the Running state
     */
    public void onRunning(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onRunning(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            m_gemFM.goToError("Received spontaneous state transition: " + printStateChangedNotice((StateNotification)obj));

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("Running")));
            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.ACTION_MSG, new StringT("Running!")));

            // Reset the running status
            m_gemFM.setDegraded(false);
            m_gemFM.setSoftErrorDetected(false);
        }
    }

    /**
     * Actions to be performed in the RunningDegraded state
     */
    public void onRunningDegraded(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onRunningDegraded(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            m_gemFM.goToError("Received spontaneous state transition: " + printStateChangedNotice((StateNotification)obj));

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            // Set action
            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("RunningDegraded")));
            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.ACTION_MSG, new StringT("Running degraded... :'(")));

            // Set the Function Manager status
            m_gemFM.setDegraded(true);
            m_gemFM.setSoftErrorDetected(false);
        }
    }

    /**
     * Actions to be performed in the RunningSoftErrorDetected state
     */
    public void onRunningSoftErrorDetected(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onRunningSoftErrorDetected(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            m_gemFM.goToError("Received spontaneous state transition: " + printStateChangedNotice((StateNotification)obj));

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            // Set action
            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("RunningSoftErrorDetected")));
            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.ACTION_MSG, new StringT("Running with soft error... :/")));

            // Set the Function Manager status
            // Do NOT touch degraded
            m_gemFM.setSoftErrorDetected(false);
        }
    }

    /**
     * Actions to be performed in the FixingSoftError state
     */
    public void onFixingSoftError(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onFixingSoftError(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            processNotice((StateNotification)obj);

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("FixingSoftError")));

            /*
             * Get parameters from the command
             */
            m_gemFM.setAction("Retrieving parameters from command");

            Long triggerNumberAtPause = null;

            try {
                ParameterSet<CommandParameter> inputParSet = m_gemFM.getLastInput().getParameterSet();
                logger.info(msgPrefix + "Input parameters = " + inputParSet.getNames());

                triggerNumberAtPause = ((LongT)inputParSet.get(GEMParameters.TRIGGER_NUMBER_AT_PAUSE).getValue()).getLong();
            } catch (Exception e) {
                // Go to error, we require parameters
                m_gemFM.goToError("Error reading parameters of FixSoftError command", e);
                return;
            }

            /*
             * To be implemented
             */

            /*
             * The soft error should be fixed now
             */
            m_gemFM.setSoftErrorDetected(false);

            if (m_gemFM.hasSoftError()) {
                // If the soft error cannot be fixed, the FM should go to ERROR
                m_gemFM.goToError("Error, could not fix the soft error");
                return;
            } else {
                // If the soft error was fixed, return to Running or RunningDegraded
                m_gemFM.fireEvent(m_gemFM.isDegraded() ? GEMInputs.SETRUNNINGDEGRADED : GEMInputs.SETRUNNING);
            }

            m_gemFM.setAction("");

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     * Actions to be performed in the PreparingTTSTestMode state
     */
    public void onPreparingTTSTestMode(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onPreparingTTSTestMode(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            processNotice((StateNotification)obj);

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("PrepararingTTSTestMode")));

            /*
             * Feature to be implemented
             */
            m_gemFM.fireEvent(GEMInputs.SETTTSTEST_MODE);

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     * Actions to be performed in the TestingTTS state
     */
    public void onTestingTTS(Object obj)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::onTestingTTS(): ";

        if (obj instanceof StateNotification) {
            // Triggered when state notification received from child resource

            logger.info(msgPrefix + "Received state notification");

            processNotice((StateNotification)obj);

            return;
        } else if (obj instanceof StateEnteredEvent) {
            // Triggered when state entered

            logger.info(msgPrefix + "State entered");

            m_gemPSet.put(new FunctionManagerParameter<StringT>(GEMParameters.STATE, new StringT("TestingTTS")));

            /*
             * Get parameters from the command
             */
            m_gemFM.setAction("Retrieving parameters from command");

            Integer fedID = -1;
            String type = "";
            String pattern = "";
            Integer cycles = -1;

            try {
                ParameterSet<CommandParameter> inputParSet = m_gemFM.getLastInput().getParameterSet();
                logger.info(msgPrefix + "Input parameters = " + inputParSet.getNames());

                fedID = ((IntegerT)inputParSet.get(GEMParameters.TTS_TEST_FED_ID).getValue()).getInteger();
                type = ((StringT)inputParSet.get(GEMParameters.TTS_TEST_TYPE).getValue()).getString();
                pattern = ((StringT)inputParSet.get(GEMParameters.TTS_TEST_PATTERN).getValue()).getString();
                cycles = ((IntegerT)inputParSet.get(GEMParameters.TTS_TEST_CYCLES).getValue()).getInteger();
            } catch (Exception e) {
                // Go to error, we require parameters
                m_gemFM.goToError("Error reading parameters of TestTTS command", e);
                return;
            }

            logger.info("Using parameters: fedID=" + fedID + " type=" + type + " pattern=" + pattern + " cycles=" + cycles);

            /*
             * Feature to be implemented
             */
            m_gemFM.setAction("");
            m_gemFM.fireEvent(GEMInputs.SETHALTED);

            logger.info(msgPrefix + "Done");
        }
    }

    /**
     *
     */
    private void executeTaskSequence(TaskSequence taskSequence)
        throws UserActionException
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::executeTaskSequence(): ";

        if (m_taskSequence != null) {
            String msg = "Error: trying to execute a new TaskSequence while another one is in progress.";
            logger.error(msgPrefix + msg);
            throw new UserActionException(msg);
        }

        m_taskSequence = taskSequence;

        processTaskSequence();
    }

    /**
     *
     */
    private void completeTransition()
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::completeTransition(): ";

        m_gemFM.setAction("Transition completed");

        logger.info(msgPrefix + "Fire TaskSequence CompletionEvent " + m_taskSequence.getCompletionEvent().toString());
        m_gemFM.fireEvent(m_taskSequence.getCompletionEvent());

        m_taskSequence = null;
    }

    /**
     *
     */
    private void processNotice(StateNotification stateNotification)
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::processNotice(): ";

        // prepare state notification
        String toState = stateNotification.getToState();
        QualifiedResource qr = findQRFromSN(stateNotification);

        if (toState == null) {
            logger.warn(msgPrefix + "Received StateNotification with toState==null.");
            return;
        }

        if (toState.equals(GEMStates.ERROR.toString())) {
            // fire event to go to error
            m_gemFM.goToError("Received " + toState + " notification from " + qr.getURI() + "\n"
                + "Reason given: " + stateNotification.getReason());
            return;
        }

        if (m_taskSequence == null) {
            String msg = "Received an unexpected StateNotification while taskSequence is null\n" + printStateChangedNotice(stateNotification);
            logger.warn(msgPrefix + msg);
            return;
        }

        processTaskSequence();
    }

    /**
     *
     */
    private void processTaskSequence()
    {
        String msgPrefix = "[GEMFM::" + m_gemFM.m_FMname + "] GEMEventHandler::processTaskSequence(): ";

        try {
            // Make sure that TaskSequence belongs to the state we are in
            if (!m_taskSequence.getState().equals(m_gemFM.getState())) {
                String errmsg = "Current TaskSequence does not belong to this state \n "
                    + "Function Manager state = " + m_gemFM.getState()
                    + " while TaskSequence is for state = " + m_taskSequence.getState();
                throw new UserActionException(errmsg);
            }

            // Execute the next step, if any, of the TaskSequence
            m_taskSequence.startExecution();
            m_gemFM.setAction(m_taskSequence.getDescription());

            // Check if we are done with the TaskSequence
            if (m_taskSequence.isCompleted()) {
                completeTransition();
            }
        } catch (Exception e) {
            m_gemFM.goToError("Caught UserActionException during task sequence processing", e);
        }
    }

    /*
     *
     */
    private QualifiedResource findQRFromSN(StateNotification sn)
    {
        try {
            return m_gemFM.getQualifiedGroup().seekQualifiedResourceOfURI(new URI(sn.getIdentifier()));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
     *
     */
    private String printStateChangedNotice(StateNotification notice)
    {
        return "StateUpdate "
            + "\n Destination = " + notice.getDestination()
            + "\n Identifier  = " + notice.getIdentifier()
            + "\n fromState   = " + notice.getFromState()
            + "\n reason      = " + notice.getReason()
            + "\n toState     = " + notice.getToState();
    }
}
