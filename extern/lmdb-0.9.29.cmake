cmake_minimum_required(VERSION 3.11 FATAL_ERROR)

project(lmdb)

find_package(Threads REQUIRED)

add_library(lmdb
    mdb.c
    midl.c
)
target_include_directories(lmdb PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")
target_link_libraries(lmdb PRIVATE Threads::Threads)

# Intended to be linked into shared libraries
set_property(TARGET lmdb PROPERTY POSITION_INDEPENDENT_CODE ON)

# Even if compiled within the main project, alias the library in its expected namespace
add_library(lmdb::lmdb ALIAS lmdb)
