/// @file

#ifndef GEM_CORE_DATAFRAME_H
#define GEM_CORE_DATAFRAME_H

#include <gem/core/algorithms.h>

#include <fmt/format.h>

#include <algorithm>
#include <array>
#include <charconv>
#include <istream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

namespace gem::core {

/// @brief
class CSVParsingError : public std::runtime_error {
public:
    CSVParsingError(const std::string& what)
        : runtime_error(what)
    {
    }
};

/// @brief Simple type-safe DataFrame-like type
template <typename... Ts>
using DataFrame = std::vector<std::tuple<Ts...>>;

namespace helper {
    /// @brief Convert a single element from the @c valueStr string to the type @c T
    template <typename T>
    T convertRowElement(const std::string& valueStr)
    {
        T value;

        if constexpr (std::is_same_v<T, std::string>) {
            value = valueStr; // Do not convert strings
        } else {
            auto [ptr, ec] = std::from_chars(valueStr.data(), valueStr.data() + valueStr.size(), value);

            if (ec == std::errc()) {
                /* no error, nothing to do */
            } else if (ec == std::errc::result_out_of_range) {
                throw CSVParsingError(fmt::format(FMT_STRING("Value out-of-range '{:s}'"), valueStr));
            } else {
                throw CSVParsingError(fmt::format(FMT_STRING("Could not convert '{:s}'"), valueStr));
            }

            if (ptr != valueStr.data() + valueStr.size())
                throw CSVParsingError(fmt::format(FMT_STRING("Element partially converted '{:s}'"), valueStr));
        }

        return value;
    }

    /// @brief Convert the elements from the @c arguments vector to an @c std::tuple<Ts...>
    template <typename... Ts, size_t... Is>
    auto convertRowImpl(std::index_sequence<Is...>, const std::vector<std::string>& arguments)
    {
        return std::make_tuple(convertRowElement<Ts>(arguments[Is])...);
    }

    /// @brief Convenience helper to use @c convertRowImpl
    template <typename... Ts>
    auto convertRow(const std::vector<std::string>& arguments)
    {
        return convertRowImpl<Ts...>(std::make_index_sequence<sizeof...(Ts)> {}, arguments);
    }
} // namespace gem::core::helper

/// @brief Load a @c DataFrame from a CSV-like document
template <typename... Ts>
DataFrame<Ts...> loadDataFrameFromCSV(
    std::istream& input,
    const decltype(std::declval<Ts>, std::string())&... columNames,
    const char delimiter = ';')
{
    std::string row;

    // Parse the header row
    std::array<size_t, sizeof...(Ts)> columnIndexes {};

    std::getline(input, row);
    const auto inputColumnNames = split(row, delimiter);

    size_t columnIndex { 0 };
    for (const auto& columName : { columNames... }) {
        const auto intputColumnIndex = std::find(inputColumnNames.begin(), inputColumnNames.end(), columName);
        if (intputColumnIndex == inputColumnNames.end())
            throw CSVParsingError(fmt::format(FMT_STRING("Column '{:s}' not found in input CSV document"), columName));

        columnIndexes.at(columnIndex) = std::distance(inputColumnNames.begin(), intputColumnIndex);
        columnIndex++;
    }

    // Parse the data rows
    DataFrame<Ts...> df {};

    size_t rowNumber = 2; // Accounting for the header row
    while (std::getline(input, row)) {
        const auto inputStringElements = split(row, delimiter);

        if (inputColumnNames.size() != inputStringElements.size())
            throw CSVParsingError(fmt::format(FMT_STRING("Incorrect number of columns in row {:d}: '{:s}'"), rowNumber, row));

        std::vector<std::string> rowStringElements(columnIndexes.size());
        for (size_t columnIndex = 0; columnIndex < columnIndexes.size(); ++columnIndex)
            rowStringElements[columnIndex] = inputStringElements[columnIndexes[columnIndex]];

        const auto rowElements = helper::convertRow<Ts...>(rowStringElements);

        df.emplace_back(std::move(rowElements));
    }

    return df;
}
} // namespace gem::core

#endif // GEM_CORE_DATAFRAME_H
