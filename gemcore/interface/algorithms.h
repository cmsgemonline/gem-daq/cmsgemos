/// @file

#ifndef GEM_CORE_ALGORITHMS_H
#define GEM_CORE_ALGORITHMS_H

#include <optional>
#include <string>
#include <vector>

namespace gem::core {

/// @brief Searches for the ideal GBT phase from a GBT phase scan
///
/// This function searches, in order, the following patterns, with a decreasing valid window size (default from 7 to 5)
/// 1. 1,0..0,1
/// 2. 1,0..0
/// 3.   0..0,1
///
/// @param @c phases results of the GBT phase scan (true/0 means a valid phase, false/1 means an invalid phase)
/// @param @c valid_window_length the length of the valid window
///
/// @return The value of the optimal phase, if the computation was possible. 15 is used to indicate that no good phase exists.
std::optional<size_t> find_gbt_phase(const std::vector<bool>& phases, size_t valid_window_length = 7);

/// @brief Tokenizes the string @c str based on the delimiter @c @delimiter
std::vector<std::string> split(const std::string& str, char delimiter);

} // namespace gem::core

#endif // GEM_CORE_ALGORITHMS_H
