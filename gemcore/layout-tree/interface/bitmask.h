#ifndef GEM_CORE_LAYOUT_TREE_BITMASK_H
#define GEM_CORE_LAYOUT_TREE_BITMASK_H

/// @file
/// @brief Implements conversion from a list of children to bitmasks

#include "gem/core/layout-tree/child_of.h"

#include <boost/range.hpp>

#include <climits>
#include <sstream>
#include <stdexcept>
#include <type_traits>

namespace gem::core::layout_tree {

/// @brief Helper classes
namespace helper {
    /// @brief Used for @ref as_bitmask
    template <class T>
    class as_bitmask_tag {
        // Empty
    };
} // namespace helper

/// @brief Range filter that converts a list of children into a bitmask
///
/// This filter can be applied to the ranges returned by
/// parent_mixin::children() and parent_mixin::recursive_children() to convert
/// a list of @ref multiple_child_of to a bitmask. It it most useful when all
/// nodes are of the same type, as achieves with @ref filter_nodes. The basic
/// syntax is:
///
/// ~~~{.cpp}
/// parent->children() | filter_nodes<child_node_type> | as_bitmask<integral_type>
/// ~~~
///
/// The resulting expression is the integral type specified in the first
/// template argument, @c T. If the child number cannot be represented within
/// the bitmask range, a @c std::out_of_range exception is thrown.
///
/// @relates helper::as_bitmask_tag
template <class T, std::enable_if_t<std::is_integral_v<T>, int> = 0>
constexpr helper::as_bitmask_tag<T> as_bitmask;

/// @brief Applies @ref as_bitmask to a range of @ref multiple_child_of
/// @relates helper::as_bitmask_tag
template <class Range,
    class T,
    std::enable_if_t<is_multiple_child_node<typename boost::range_value<Range>::type>, int> = 0>
auto operator|(const Range& range, helper::as_bitmask_tag<T>)
{
    T bitmask { 0 };
    for (const auto& child : range) {
        // Ensure child number fits within bitmask
        if (child.number() >= sizeof(T) * CHAR_BIT) {
            std::stringstream ss;
            ss << "Child number cannot be represented within bitmask"
               << " (got child number "
               << child.number()
               << ", maximum bitmask number "
               << sizeof(T) * CHAR_BIT - 1
               << ")";
            throw std::out_of_range(ss.str());
        }

        bitmask |= (static_cast<T>(1) << child.number());
    }
    return bitmask;
}

} // namespace gem::core::layout_tree

#endif // GEM_CORE_LAYOUT_TREE_BITMASK_H
