/// @file
/// @brief Tests for the @c DataFrame class

#include <gem/core/dataframe.h>

#include <sstream>
#include <tuple>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE core_classes
#include <boost/test/unit_test.hpp>

namespace gem::core {

BOOST_AUTO_TEST_SUITE(dataframe)

BOOST_AUTO_TEST_CASE(load_from_csv)
{
    using ss = std::stringstream;

    // Valid document
    {
        auto document = ss(R"--(fed;slot;oh;vfat;channel;status
333;0;42;0;64;ok
333;0;42;1;64;noisy
333;0;42;2;64;dead
444;2;0;8;0;ok
444;2;0;8;1;ok
)--");
        const auto df = loadDataFrameFromCSV<std::string, uint32_t, uint8_t, uint8_t, uint8_t, uint8_t>(
            document, "status", "fed", "slot", "oh", "vfat", "channel");
        const bool ok = (df.at(2) == std::make_tuple("dead", 333, 0, 42, 2, 64));
        BOOST_CHECK(ok);
    }

    auto exceptTestFunction = [](std::stringstream& document) {
        return loadDataFrameFromCSV<int, char, unsigned char>(document, "int", "char", "uchar");
    };

    // Missing column (uchar)
    {
        auto document = ss(R"--(int;char;ushort
-1024;-7;512
)--");
        BOOST_CHECK_THROW(exceptTestFunction(document), CSVParsingError);
    }

    // Invalid number of columns (line 3)
    {
        auto document = ss(R"--(int;char;uchar
-1024;-7;128
-1024;128
)--");
        BOOST_CHECK_THROW(exceptTestFunction(document), CSVParsingError);
    }

    // Invalid conversion (ten is not an uchar)
    {
        auto document = ss(R"--(int;char;uchar
-1024;-7;ten
)--");
        BOOST_CHECK_THROW(exceptTestFunction(document), CSVParsingError);
    }

    // Out of range (512 above uchar MAX)
    {
        auto document = ss(R"--(int;char;uchar
-1024;-7;512
)--");
        BOOST_CHECK_THROW(exceptTestFunction(document), CSVParsingError);
    }

    // Partial conversion ('-7 ' != '-7')
    {
        auto document = ss(R"--(int;char;uchar
-1024;-7 ;512
)--");
        BOOST_CHECK_THROW(exceptTestFunction(document), CSVParsingError);
    }
}

BOOST_AUTO_TEST_SUITE_END()

} // namespace gem::core
