/// @file
/// @brief Tests for the core algorithms

#include <gem/core/algorithms.h>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE core_classes
#include <boost/test/unit_test.hpp>

namespace gem::core {

BOOST_AUTO_TEST_SUITE(algorithms)

BOOST_AUTO_TEST_CASE(func_find_gbt_phase)
{
    std::vector<bool> test_vector(15);
    BOOST_CHECK_EQUAL(find_gbt_phase(test_vector).has_value(), false);

    test_vector = { 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 };
    BOOST_CHECK_EQUAL(find_gbt_phase(test_vector).value(), 6);
    test_vector = { 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    BOOST_CHECK_EQUAL(find_gbt_phase(test_vector).value(), 6);
    test_vector = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 };
    BOOST_CHECK_EQUAL(find_gbt_phase(test_vector).value(), 6);

    test_vector = { 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 };
    BOOST_CHECK((find_gbt_phase(test_vector) == 6) || (find_gbt_phase(test_vector) == 7));

    test_vector = { 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0 };
    BOOST_CHECK((find_gbt_phase(test_vector) == 2) || (find_gbt_phase(test_vector) == 3));
    test_vector = { 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 };
    BOOST_CHECK((find_gbt_phase(test_vector) == 12) || (find_gbt_phase(test_vector) == 13));

    test_vector = { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0 };
    BOOST_CHECK_EQUAL(find_gbt_phase(test_vector).value(), 8);

    test_vector = { 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0 };
    BOOST_CHECK_EQUAL(find_gbt_phase(test_vector).value(), 15);
}

BOOST_AUTO_TEST_CASE(func_split)
{
    // A few test patterns...
    std::vector<std::tuple<std::string, char, std::vector<std::string>>> patterns = {
        { "foo", ';', { "foo" } },
        { "foo;bar", ';', { "foo", "bar" } },
        { "foo|bar", '|', { "foo", "bar" } },
        { ";foo", ';', { "", "foo" } },
        { "bar;", ';', { "bar", "" } },
        { "foo;;bar", ';', { "foo", "", "bar" } },
        { "foo||bar", '|', { "foo", "", "bar" } },
        { ";", ';', { "", "" } },
        { "", ';', { "" } },
        { "", '|', { "" } },
    };

    // Check computations vs expectations
    for (const auto& [input, delimiter, output] : patterns) {
        const auto& values = split(input, delimiter);
        BOOST_CHECK_EQUAL_COLLECTIONS(values.begin(), values.end(),
            output.begin(), output.end());
    }
}

BOOST_AUTO_TEST_SUITE_END()

} // namespace gem::core
