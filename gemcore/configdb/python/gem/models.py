from sqlalchemy import Column, ForeignKey, BigInteger, Integer, String, Table, UniqueConstraint
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.schema import MetaData


CONVENTION = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}

Base = declarative_base(metadata=MetaData(naming_convention=CONVENTION))


class Configuration(Base):
    """Configuration objects identify sets of settings that provide complete detector configurations."""

    __tablename__ = "configuration"

    id = Column(Integer, primary_key=True)

    # Helper functions
    def as_dict(self):
        return {cr.component: cr.as_dict() for cr in self.contents}


class Key(Base):
    """Keys are evolving labels, similar to git branches."""

    __tablename__ = "key"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    description = Column(String, nullable=False)


class KeyVersion(Base):
    """Each version of a key points to a specific Configuration."""

    __tablename__ = "key_version"

    id = Column(Integer, primary_key=True)

    # Implement a N-to-M mapping between Key and Configuration
    key = Column(Integer, ForeignKey("key.id"), nullable=False)
    config = Column(Integer, ForeignKey("configuration.id"), nullable=False)

    # Sequence number within the key
    version = Column(Integer, nullable=False)

    # Optional metadata
    message = Column(String)


class Register(Base):
    """Assigns an id to register names to avoid storing them as strings."""

    __tablename__ = "register"

    id = Column(Integer, primary_key=True)
    name = Column(String, index=True, nullable=False, unique=True)


configuration_to_component_record = Table(
    "configuration_to_component_record",
    Base.metadata,
    Column("configuration", Integer, ForeignKey("configuration.id"), primary_key=True),
    Column("component_record", Integer, ForeignKey("component_record.id"), primary_key=True),
)


class ComponentRecord(Base):
    """Groups all Record objects part of a Configuration for a given component."""

    __tablename__ = "component_record"

    id = Column(Integer, primary_key=True)

    # Configuration associated with this record
    configs = relationship(
        "Configuration", backref="contents", secondary=configuration_to_component_record
    )

    # Component configured by this record
    component = Column(String, nullable=False)  # To be updated

    # Helper functions
    def as_dict(self):
        return {record.register.name: record.value for record in self.records}


class Record(Base):
    """The value of one register for one component."""

    __tablename__ = "record"

    id = Column(BigInteger, primary_key=True)

    # Component record that we belong to
    parent_id = Column(Integer, ForeignKey("component_record.id"), nullable=False)
    parent = relationship("ComponentRecord", backref="records")

    # Register that we configure
    register_id = Column(Integer, ForeignKey("register.id"), nullable=False)
    register = relationship("Register")  # No backref: would be too slow

    # Value
    value = Column(BigInteger, nullable=False)

    # Invariants
    __table_args__ = (UniqueConstraint("parent_id", "register_id"),)
