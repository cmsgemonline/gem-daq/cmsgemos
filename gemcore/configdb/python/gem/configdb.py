"""The main module for the configuration database."""

import argparse
import getpass
import glob
import importlib.resources
import itertools as it
import logging
import os
import random
import sys

import sqlalchemy as sql
from sqlalchemy.orm import Session
from sqlalchemy.dialects.postgresql import insert as pg_insert

from . import models


# If True, print SQL statements.
DEBUG = False


def find_db():
    """Guesses the database URL. If $GEM_CONFIG_DB is set, it is used. Otherwise, the URL
    `postgresql://<user>:<user>@localhost/<user>` is returned."""

    if "GEM_CONFIG_DB" in os.environ:
        return os.environ["GEM_CONFIG_DB"]
    else:
        user = getpass.getuser()
        return f"postgresql://{user}:{user}@localhost/{user}"


def open_db():
    """Creates a database engine and connects."""
    engine = sql.create_engine(find_db(), echo=DEBUG)
    conn = engine.connect()
    return engine, conn


def _alembic(args):
    """Manage the database schema (shortcut to run alembic with the right arguments)"""

    # We need to add arguments to tell alembic where to find its configuration file
    with importlib.resources.path(__package__, "alembic.ini") as path:
        from alembic.config import main as alembic_main

        alembic_main(argv=["-c", str(path)] + args)


def _create_key(args):
    """Creates a new key"""

    parser = argparse.ArgumentParser()
    parser.add_argument("name")
    parser.add_argument("description")
    args = parser.parse_args(args)

    engine, _ = open_db()
    with Session(engine) as session:
        key = models.Key()
        key.name = args.name
        key.description = args.description
        session.add(key)
        session.commit()


def _list_keys(args):
    """Lists available keys"""

    engine, _ = open_db()
    with Session(engine) as session:
        for key in session.query(models.Key).all():
            print("{}\t{}".format(key.name, key.description))


def _history(args):
    """Prints the history of a key"""

    parser = argparse.ArgumentParser()
    parser.add_argument("key")
    args = parser.parse_args(args)

    engine, _ = open_db()
    with Session(engine) as session:
        got_something = False
        for version in (
            session.query(models.KeyVersion)
            .join(models.Key)
            .where(models.Key.name == args.key)
            .order_by(models.KeyVersion.version.desc())
        ):
            got_something = True
            if version.message:
                print("{:3d} -> {:4d}: {}".format(version.version, version.config, version.message))
            else:
                print("{:3d} -> {:4d}".format(version.version, version.config))
        if not got_something:
            # Print something even if the key has no version
            print("<No version>")


def _new_version(args):
    """Creates a new version of a key"""

    parser = argparse.ArgumentParser()
    parser.add_argument("key")
    parser.add_argument("config", metavar="configuration-id")
    parser.add_argument("-m,--message", dest="message")
    args = parser.parse_args(args)

    engine, _ = open_db()
    with Session(engine) as session:
        # A scalar subquery for the ID of the key, with a CTE to prettify the generated SQL
        key_id = sql.select(
            session.query(models.Key.id).where(models.Key.name == args.key).cte("cte_key_id").c.id
        ).scalar_subquery()

        # A subquery for the current version (not a scalar yet)
        current_version = sql.select(
            session.query(models.KeyVersion.version)
            .where(models.KeyVersion.key == models.Key.id)
            .where(models.Key.name == args.key)
            .order_by(models.KeyVersion.version.desc())
            .limit(1)
            .cte("cte_current")
            .c.version
        )

        # Generate SQL for: version = 1 + current_version if current_version else 0
        version = sql.case(
            (current_version.exists(), 1 + current_version.scalar_subquery()), else_=0
        )

        # Create the new version
        new = models.KeyVersion()
        new.key = key_id
        new.config = args.config
        new.version = version
        new.message = args.message
        session.add(new)
        session.commit()

        # Print the new version number
        print(new.version)


def _create_config(args):
    """Creates a new configuration"""

    engine, _ = open_db()
    with Session(engine) as session:
        c = models.Configuration()
        session.add(c)
        session.commit()

        print(c.id)


def _configuration_as_dict(session, id):
    """Retrieves a configuration from the database and turns it into nested dictionaries"""
    stmt = sql.select(models.Configuration).filter_by(id=id)
    (configuration,) = session.execute(stmt).one()
    return configuration.as_dict()


def _dict_from_files(path):
    """Reads configuration files in a folder and turns them into nested dictionaries"""
    data = {}
    for cfg in glob.iglob(os.path.join(path, "*.cfg")):
        component = os.path.basename(cfg)[:-4]  # Drop .cfg extension
        contents = {}
        with open(cfg, "rt") as f:
            for line in f:
                # Drop comments
                line = line.split("#")[0]
                line = line.strip()
                if line == "":
                    continue
                # Get name and value
                key, value = line.split()
                contents[key] = int(value)

        data[component] = contents
    return data


def _import_data(args):
    """Imports data to the database"""

    parser = argparse.ArgumentParser(
        description="Scans a folder for .cfg files and imports them to the database"
    )
    parser.add_argument("path", help="Folder from which to read data")
    parser.add_argument(
        "-r,--relative-to",
        dest="relative_to",
        type=int,
        help="Id of the configuration relative to which deduplication should take place",
    )
    args = parser.parse_args(args)

    # Read all data files
    data = _dict_from_files(args.path)

    # List all registers needed by this batch
    registers = set()
    for contents in data.values():
        registers |= contents.keys()

    engine, _ = open_db()
    with Session(engine) as session:
        # O- Deduplicate
        if args.relative_to is not None:
            # Get the reference configuration
            stmt = sql.select(models.Configuration).filter_by(id=args.relative_to)
            (reference,) = session.execute(stmt).one()

            # Check every component to see if some are equal to the reference
            deduplicated = 0
            for cr in reference.contents:
                if cr.component in data and data[cr.component] == cr.as_dict():
                    data[cr.component] = cr  # Will reuse the record
                    deduplicated += 1
            if deduplicated == len(data):
                # No change with respect to the reference... this is potentially an operator error
                logging.warning("No difference with respect to the reference configuration")
                print(args.relative_to)
                return

            logging.info(
                "Deduplication: %d/%d components reused (%.0f%%)",
                deduplicated,
                len(data),
                deduplicated / len(data) * 100,
            )

        # 1- Upsert register names
        stmt = (
            pg_insert(models.Register)
            .values([{"name": key} for key in registers])
            .on_conflict_do_nothing()  # NOTE on_conflict_do_nothing is specific to Postgres
        )
        session.execute(stmt)

        # 2- Get the mapping between register names and ids (once: less DB work and rows are immutable)
        stmt = [session.query(models.Register).filter_by(name=name) for name in registers]
        stmt = sql.union_all(*stmt)
        stmt = sql.select(models.Register).from_statement(stmt)
        # Changes the meaning of 'registers' -- we won't need the old one
        registers = {reg.name: reg for reg in session.execute(stmt).scalars()}

        # 3- Create a configuration and attach the data
        config = models.Configuration()
        session.add(config)

        components = {}
        for name, values in data.items():  # Iterate over components
            if isinstance(values, models.ComponentRecord):
                # Reusing an existing record from another configuration
                # See deduplication logic above
                config.contents.append(values)
                session.add(values)
                continue

            cr = models.ComponentRecord()
            cr.component = name
            config.contents.append(cr)
            components[name] = cr
            session.add(cr)

            for regname, value in values.items():  # Iterate over registers
                record = models.Record()
                record.parent = cr
                record.register = registers[regname]
                record.value = value
                session.add(record)

        # 4- Save everything
        session.commit()

        # Print the configuration ID so it can be associated with a key
        print(config.id)


def _export_data(args):
    """Exports data to configuration files"""

    parser = argparse.ArgumentParser()
    parser.add_argument("configuration", help="The id of the configuration to export")
    parser.add_argument("path", help="Where to put the files")
    args = parser.parse_args(args)

    os.makedirs(args.path)

    engine, _ = open_db()
    with Session(engine) as session:
        data = _configuration_as_dict(session, args.configuration)
        for component, contents in data.items():
            path = os.path.join(args.path, component + ".cfg")
            with open(path, "wt") as f:
                f.write(f"# File exported from configuration {args.configuration}\n")
                f.write(f"# Applies to component {component}\n")
                f.write("\n")
                for regname, value in contents.items():
                    f.write(f"{regname} {value}\n")


def _generate_test_data(args):
    """Generates some data for testing"""

    parser = argparse.ArgumentParser()
    parser.add_argument("path")
    parser.add_argument(
        "--random-fraction",
        dest="random",
        type=float,
        default=0.1,
        help="Fraction of files with a random value (to test deduplication)",
    )
    args = parser.parse_args(args)

    folder = os.path.join(args.path, "vfat")
    os.makedirs(folder)
    for loc in it.product(range(2), range(12), range(12), range(24)):
        with open(
            os.path.join(folder, "config-fed{}-slot{}-oh{}-vfat{}.cfg".format(*loc)), "wt"
        ) as f:
            f.write("# Auto-generated test data\n")
            f.write("# DO NOT USE in production\n")
            f.write("\n")
            for i in range(234):
                f.write("REG{} {} # dummy comment\n".format(i, i + loc[2] + loc[1] * loc[0]))
            if random.random() < args.random:
                f.write("RANDOM_REG {}".format(random.randint(-10000, 10000)))


def main():
    """Entry point used by gem-configdb."""

    parser = argparse.ArgumentParser(
        description="Tool to alembic the GEM configuration database.",
        epilog="Set the database address using the $GEM_CONFIG_DB environment variable.",
    )
    parser.add_argument("-d,--debug", dest="debug", action="store_true")
    subparsers = parser.add_subparsers(title="subcommands")

    commands = {
        "create-key": _create_key,
        "history": _history,
        "list-keys": _list_keys,
        "create-configuration": _create_config,
        "new-version": _new_version,
        "alembic": _alembic,
        "import-data": _import_data,
        "export-data": _export_data,
        "generate-test-data": _generate_test_data,
    }

    for command, function in commands.items():
        subcommand = subparsers.add_parser(
            command,
            help=function.__doc__,
            add_help=False,
        )
        subcommand.set_defaults(sub=command, func=function)

    known_args, args = parser.parse_known_args()

    global DEBUG
    DEBUG = known_args.debug

    if not hasattr(known_args, "sub"):
        # No command provided
        parser.print_usage()
        sys.exit(1)

    sys.argv[0] += f" {known_args.sub}"  # So the subcommand can use argparse as normal
    known_args.func(args)
