/// @file

#include <gem/core/algorithms.h>

#include <algorithm>
#include <numeric>

std::optional<size_t> gem::core::find_gbt_phase(const std::vector<bool>& phases, size_t valid_window_length)
{
    // If all phases are good(ie, 0), return std::nullopt
    if (!std::accumulate(phases.begin(), phases.end(), 0))
        return {};
    // If the window of good phases is less then 5, the VFAT does not have good communication and we return a phase of 15
    if (valid_window_length < 5)
        return 15;

    // First define a window of N+2 good phases of 1,0..0,1
    std::vector<int> window_good_phases(valid_window_length + 2);
    // Make the first and the last element 1
    window_good_phases[0] = 1;
    window_good_phases[valid_window_length + 1] = 1;

    // Search if there is such window of good phases in the phases vector
    auto it = std::search(phases.begin(), phases.end(), window_good_phases.begin(), window_good_phases.end());
    if (it != phases.end()) {
        auto good_phase = std::distance(phases.begin(), it) + (valid_window_length / 2 + 1);
        return good_phase;
    }

    // Try with a smaller window of N+1 good phases of 1,0..0
    window_good_phases.pop_back();
    it = std::search(phases.begin(), phases.end(), window_good_phases.begin(), window_good_phases.end());
    if (it != phases.end()) {
        auto good_phase = std::distance(phases.begin(), it) + (valid_window_length / 2 + 1);
        return good_phase;
    }

    // Try with a window of N+1 good phases of 0..0,1
    window_good_phases[0] = 0;
    window_good_phases[valid_window_length] = 1;
    it = std::search(phases.begin(), phases.end(), window_good_phases.begin(), window_good_phases.end());
    if (it != phases.end()) {
        auto good_phase = std::distance(phases.begin(), it) + (valid_window_length / 2);
        return good_phase;
    }

    // None of the cases worked, so try a smaller good phases window
    return find_gbt_phase(phases, valid_window_length - 1);
}

std::vector<std::string> gem::core::split(const std::string& str, const char delimiter)
{
    std::vector<std::string> tokens;
    size_t start = 0, end = 0;
    do {
        start = end;
        end = str.find(delimiter, start);
        tokens.push_back(str.substr(start, end - start));
    } while (end++ != std::string::npos);
    return tokens;
}
