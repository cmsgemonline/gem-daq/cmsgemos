/// @file

#include <gem/core/unpacker/OptoHybrid.h>

#include <fmt/format.h>

namespace gem::core::unpacker {

void OptoHybrid::readChamberHeader(uint64_t word)
{
    m_CalibChan = 0x7f & (word >> 40);
    m_InputID = 0x1f & (word >> 35);
    m_VfWdCnt = 0xfff & (word >> 23);
    m_EvtF = 0x1 & (word >> 22);
    m_InF = 0x1 & (word >> 21);
    m_EvtSzOFW = 0x1 & (word >> 19);
    m_EvtNF = 0x1 & (word >> 18);
    m_InNF = 0x1 & (word >> 17);
    m_InvBC = 0x1 & (word >> 16);
    m_EvtSzW = 0x1 & (word >> 15);
    m_InvCRC = 0x1 & (word >> 14);
    m_OOScAvV = 0x1 & (word >> 13);
    m_OOScVvV = 0x1 & (word >> 12);
    m_BxmAvV = 0x1 & (word >> 11);
    m_BxmVvV = 0x1 & (word >> 10);
    m_OOScAvO = 0x1 & (word >> 9);
    if (m_version == 0) {
        m_l1aF = 0x1 & (word >> 20);
        m_l1aNF = 0x1 & (word >> 16);
    } else {
        m_l1aF = 0;
        m_l1aNF = 0;
    }
}

void OptoHybrid::readChamberTrailer(uint64_t word)
{
    if (m_version == 0) {
        m_VfWdCntTr = 0xfff & (word >> 36);
        m_InUfw = 0x1 & (word >> 33);
        m_ZSMask = 0;
        m_VFATMask = 0;
    } else {
        m_VfWdCntTr = 0xfff & (word >> 52);
        m_InUfw = 0x1 & (word >> 51);
        m_ZSMask = 0xff'ffff & (word >> 24);
        m_VFATMask = 0xff'ffff & word;
    }
}

std::ostream& operator<<(std::ostream& os, const OptoHybrid& oh)
{
    os << fmt::format(FMT_STRING("Input ID: {}\n"), oh.m_InputID)
       << fmt::format(FMT_STRING("Calibration channel: {}\n"), oh.m_CalibChan)
       << fmt::format(FMT_STRING("VFAT word count: {}\n"), oh.m_VfWdCnt)
       << fmt::format(FMT_STRING("Event FIFO full: {}\n"), oh.m_EvtF)
       << fmt::format(FMT_STRING("Event FIFO near full: {}\n"), oh.m_EvtNF)
       << fmt::format(FMT_STRING("Input FIFO full: {}\n"), oh.m_InF)
       << fmt::format(FMT_STRING("Input FIFO near full: {}\n"), oh.m_InNF)
       << fmt::format(FMT_STRING("Input FIFO underflow: {}\n"), oh.m_InUfw)
       << fmt::format(FMT_STRING("Event size overflow: {}\n"), oh.m_EvtSzOFW)
       << fmt::format(FMT_STRING("Event size warning: {}\n"), oh.m_EvtSzW)
       << fmt::format(FMT_STRING("Invalid BC: {}\n"), oh.m_InvBC)
       << fmt::format(FMT_STRING("Invalid CRC: {}\n"), oh.m_InvCRC)
       << fmt::format(FMT_STRING("OOS AMC-vs-OH: {}\n"), oh.m_OOScAvO)
       << fmt::format(FMT_STRING("OOS AMC-vs-VFAT: {}\n"), oh.m_OOScAvV)
       << fmt::format(FMT_STRING("OOS VFAT-vs-VFAT: {}\n"), oh.m_OOScVvV)
       << fmt::format(FMT_STRING("Bxm AMC-vs-VFAT: {}\n"), oh.m_BxmAvV)
       << fmt::format(FMT_STRING("Bxm VFAT-vs-VFAT: {}\n"), oh.m_BxmVvV)
       << fmt::format(FMT_STRING("VFAT mask: {:#x}\n"), oh.m_VFATMask)
       << fmt::format(FMT_STRING("Zero-suppressed mask: {:#x}\n"), oh.m_ZSMask);
    return os;
}

} // namespace gem::core::unpacker
