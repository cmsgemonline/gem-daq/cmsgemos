/// @file

#include <gem/core/dataframe.h>
#include <gem/core/unpacker/AMC13Event.h>
#include <gem/core/unpacker/AMCEvent.h>
#include <gem/core/unpacker/raw_file_reader.h>

#include <CLI/CLI.hpp>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <fmt/format.h>

#include <array>
#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>

namespace unpacker = gem::core::unpacker;

constexpr size_t MAX_OH = 24; ///< Maximum number of OptoHybrids per GEMPayload supported by the data format
constexpr size_t MAX_VFAT = 24; ///< Maximum number of VFAT per OptoHybrid supported by the data format
constexpr size_t VFAT_CHANNELS = 128; ///< Number of VFAT channels

/// @brief Simple 1D histogram with @c N bins
template <size_t N>
using histogram_1d_t = std::array<uint64_t, N>;

/// @brief Simple 2D histogram with @c NxM bins
template <size_t N, size_t M>
using histogram_2d_t = std::array<std::array<uint64_t, M>, N>;

/// @brief Save @c histogram in the current @c TDirectory under @c name
///
/// See ROOT documentation for more details about the @c name, @c title, @c
/// xlow, and @c xup parameters.
template <size_t N>
void fillHistogram(const histogram_1d_t<N>& histogram, const std::string& name, const std::string& title, double xlow = -0.5, double xup = N - 0.5)
{
    // Owned by gDirectory
    auto rootHistogram = new TH1D(name.data(), title.data(), N, xlow, xup);

    for (size_t i = 0; i < N; i++) {
        auto bin = rootHistogram->GetBin(i + 1);
        rootHistogram->SetBinContent(bin, histogram.at(i));
    }
}

/// @brief Save @c histogram in the current @c TDirectory under @c name
///
/// See ROOT documentation for more details about the @c name, @c title, @c
/// xlow, @c xup, @c ylow, and @c yup parameters.
template <size_t N, size_t M>
void fillHistogram(const histogram_2d_t<N, M>& histogram, const std::string& name, const std::string& title, double xlow = -0.5, double xup = N - 0.5, double ylow = -0.5, double yup = M - 0.5)
{
    // Owned by gDirectory
    auto rootHistogram = new TH2D(name.data(), title.data(), N, xlow, xup, M, ylow, yup);

    for (size_t i = 0; i < N; i++) {
        for (size_t j = 0; j < M; j++) {
            auto bin = rootHistogram->GetBin(i + 1, j + 1);
            rootHistogram->SetBinContent(bin, histogram.at(i).at(j));
        }
    }
}

/// @brief Represents the detector geometry
///
/// @note Defaults to the null/identity geometry (e.g. all channels unmasked)
class DetectorGeometry {
public:
    struct ChannelMask {
        uint64_t lsMask = 0xffffffffffffffff;
        uint64_t msMask = 0xffffffffffffffff;
    };

    /// @brief Load the VFAT channels mask from @c filename
    ///
    /// The input file is expected to be a CSV-like file with ';' separators
    /// and the fed, slot, oh, vfat, and noisy-channel columns.
    void loadChannelMask(const std::string& filename)
    {
        std::ifstream infile(filename);
        if (!infile.is_open())
            throw std::runtime_error("Could not open: " + filename);

        const auto df = gem::core::loadDataFrameFromCSV<uint32_t, uint8_t, uint8_t, uint8_t, uint8_t>(infile, "fed", "slot", "oh", "vfat", "noisy-channel");

        for (const auto& row : df) {
            const uint32_t fed = std::get<0>(row);
            const uint8_t slot = std::get<1>(row);
            const uint8_t oh = std::get<2>(row);
            const uint8_t vfat = std::get<3>(row);
            const uint8_t channel = std::get<4>(row);

            const uint64_t fedslot = (static_cast<uint64_t>(slot) << 32) | fed;
            auto& mask = m_masks[fedslot].at(oh).at(vfat);
            if (channel < 64)
                mask.lsMask &= ~(1ULL << channel);
            else
                mask.msMask &= ~(1ULL << (channel - 64));
        }
    }

    /// @brief Get the channel mask corresponding to @c fed, @c slot, @c oh, and @c vfat
    ChannelMask getChannelMask(const uint32_t fed, const uint8_t slot, const uint8_t oh, const uint8_t vfat)
    {
        // Cache the FED-Slot histograms holders array in order to avoid
        // unnecessary std::unordered_map lookups
        const uint64_t fedslot = (static_cast<uint64_t>(slot) << 32) | fed;
        if (fedslot != m_cachedFEDSlot) {
            m_cachedFEDSlot = fedslot;
            m_cachedFEDSlotMasks = &m_masks[fedslot];
        }

        return m_cachedFEDSlotMasks->at(oh).at(vfat);
    }

protected:
    std::unordered_map<uint64_t, std::array<std::array<ChannelMask, MAX_VFAT>, MAX_OH>> m_masks;

    uint64_t m_cachedFEDSlot = 0; ///< FED-Slot corresponding to the cached masks
    typename decltype(m_masks)::mapped_type* m_cachedFEDSlotMasks = nullptr; ///< Cached masks
};

/// @brief Generic histogrammer interface
class Histogrammer {
public:
    /// @brief Constructor
    explicit Histogrammer(/* const */ DetectorGeometry& detectorGeometry)
        : m_detectorGeometry(detectorGeometry)
    {
    }

    virtual ~Histogrammer() {}

    /// @brief Process the event pointed by @c event
    virtual void processEvent(unpacker::AMCEvent* event) = 0;

    /// @brief Save all histograms into @c outputFile
    virtual void saveAllHistograms(TFile& outputFile) = 0;

protected:
    /* const */ DetectorGeometry& m_detectorGeometry;
};

/// @brief Generic per-VFAT histogrammer
///
/// @tparam THist Per-VFAT histograms holder
template <typename THist>
class VFATHistogrammer : public Histogrammer {
public:
    VFATHistogrammer(/* const */ DetectorGeometry& detectorGeometry)
        : Histogrammer(detectorGeometry)
    {
    }

    virtual ~VFATHistogrammer() {}

    void saveAllHistograms(TFile& outputFile) final
    {
        for (const auto& fedSlotHistograms : m_histograms) {
            const auto fed = fedSlotHistograms.first & 0xffffffff;
            const auto slot = (fedSlotHistograms.first >> 32) & 0xff;

            for (size_t ioh = 0; ioh < MAX_OH; ++ioh) {
                for (size_t ivfat = 0; ivfat < MAX_VFAT; ++ivfat) {
                    auto& histograms = fedSlotHistograms.second.at(ioh).at(ivfat);
                    if (!histograms)
                        continue;

                    const auto dirName = fmt::format(FMT_STRING("fed{}-slot{}-oh{}-vfat{}"), fed, slot, ioh, ivfat);
                    gDirectory->mkdir(dirName.data());
                    gDirectory->cd(dirName.data());
                    saveHistograms(*histograms);
                    gDirectory->cd("..");
                }
            }
        }
    }

protected:
    /// @brief Get the histograms holder corresponding to @c fed, @c slot, @c oh, and @c vfat
    ///
    /// @note Histograms are created and value-initiliazed on first use.
    THist& getHistograms(const uint32_t fed, const uint8_t slot, const uint8_t oh, const uint8_t vfat)
    {
        // Cache the FED-Slot histograms holders array in order to avoid
        // unnecessary std::unordered_map lookups
        const uint64_t fedslot = (static_cast<uint64_t>(slot) << 32) | fed;
        if (fedslot != m_cachedFEDSlot) {
            m_cachedFEDSlot = fedslot;
            m_cachedFEDSlotHistograms = &m_histograms[fedslot];
        }

        auto& histograms = m_cachedFEDSlotHistograms->at(oh).at(vfat);
        if (!histograms)
            histograms.reset(new THist {});
        return *histograms;
    }

    /// @brief Save all histograms from the @c histogram holder
    virtual void saveHistograms(THist& histogram) = 0;

private:
    std::unordered_map<uint64_t, std::array<std::array<std::unique_ptr<THist>, MAX_VFAT>, MAX_OH>> m_histograms; ///< Per-VFAT histogram holder (index by FED-Slot)

    uint64_t m_cachedFEDSlot = 0; ///< FED-Slot corresponding to the cached histogram holders
    typename decltype(m_histograms)::mapped_type* m_cachedFEDSlotHistograms = nullptr; ///< Cached histogram holders
};

/// @brief Histograms holder for occupancy analysis
struct OccupancyHistograms {
    histogram_1d_t<VFAT_CHANNELS> nHitvsChan {}; // (channel)
};

/// @brief Histograms producer for occupancy analysis
class OccupancyHistogrammer : public VFATHistogrammer<OccupancyHistograms> {
public:
    OccupancyHistogrammer(/* const */ DetectorGeometry& detectorGeometry)
        : VFATHistogrammer(detectorGeometry)
    {
    }

    virtual ~OccupancyHistogrammer() {}

    void processEvent(unpacker::AMCEvent* event) override
    {
        const auto& payload = event->payload();
        if (payload.runType() != 1 && payload.runType() != 2 && payload.runType() != 4)
            return; // Keep only Physics, Latency scan, and Threshold events

        for (const auto& oh : payload.optohybrids()) {
            for (const auto& vfat : oh.vfats()) {
                auto& histograms = getHistograms(event->softSrcID(), event->softSlot(), oh.InputId(), vfat.pos());

                for (uint8_t ichannel = 0; ichannel < VFAT_CHANNELS; ++ichannel) {
                    bool hit { false };
                    if (ichannel < 64)
                        hit = (vfat.lsData() >> ichannel) & 0x1;
                    else
                        hit = (vfat.msData() >> (ichannel - 64)) & 0x1;

                    if (hit)
                        histograms.nHitvsChan.at(ichannel) += 1;
                }
            }
        }
    }

protected:
    void saveHistograms(OccupancyHistograms& histograms) override
    {
        fillHistogram(histograms.nHitvsChan, "nHitvsChan", "Number of hits vs VFAT channel; VFAT channel number; Number of hits");
    }
};

/// @brief Histograms holder for the S-curve scan
struct SCurveHistograms {
    histogram_2d_t<256, VFAT_CHANNELS> nEventvsCalDAC2D {}; // (calDac, channel)
    histogram_2d_t<256, VFAT_CHANNELS> chanHitvsCalDAC2D {}; // (calDac, channel)
};

/// @brief Histograms producer for the S-curve scan
class SCurveHistogrammer : public VFATHistogrammer<SCurveHistograms> {
public:
    SCurveHistogrammer(/* const */ DetectorGeometry& detectorGeometry)
        : VFATHistogrammer(detectorGeometry)
    {
    }

    virtual ~SCurveHistogrammer() {}

    void processEvent(unpacker::AMCEvent* event) override
    {
        const auto& payload = event->payload();
        if (payload.runType() != 3)
            return; // Keep the only S-curve events

        const uint8_t calDac = payload.runParams() & 0xff;

        for (const auto& oh : payload.optohybrids()) {
            const uint8_t calibChan = oh.CalibChan();

            // Zero-suppressed VFATs do not appear in the oh.vfats()
            // collection, but did participate to the event. Fill the
            // nEventvsLatency histograms for those first.
            const auto zsMask = oh.ZSMask();
            for (uint8_t ivfat = 0; ivfat < MAX_VFAT; ++ivfat) {
                if ((zsMask >> ivfat) & 0x1)
                    getHistograms(event->softSrcID(), event->softSlot(), oh.InputId(), ivfat).nEventvsCalDAC2D.at(calDac).at(calibChan) += 1;
            }

            for (const auto& vfat : oh.vfats()) {
                auto& histograms = getHistograms(event->softSrcID(), event->softSlot(), oh.InputId(), vfat.pos());

                bool hit { false };
                if (calibChan < 64)
                    hit = (vfat.lsData() >> calibChan) & 0x1;
                else
                    hit = (vfat.msData() >> (calibChan - 64)) & 0x1;

                histograms.nEventvsCalDAC2D.at(calDac).at(calibChan) += 1;
                if (hit)
                    histograms.chanHitvsCalDAC2D.at(calDac).at(calibChan) += 1;
            }
        }
    }

protected:
    void saveHistograms(SCurveHistograms& histograms) override
    {
        fillHistogram(histograms.nEventvsCalDAC2D, "nEventvsCalDAC2D", "Channel eventmap vs CAL_DAC; CAL_DAC (DAC); VFAT channel; Number of events");
        fillHistogram(histograms.chanHitvsCalDAC2D, "chanHitvsCalDAC2D", "Channel hitmap vs CAL_DAC; CAL_DAC (DAC); VFAT channel; Number of hits");
    }
};

/// @brief Histograms holder the for threshold scan
struct ThresholdScanHistograms {
    histogram_2d_t<256, VFAT_CHANNELS> chanHitvsDACVal2D {}; // (dacVal, channel)
};

/// @brief Histograms producer for the threshold scan
class ThresholdScanHistogrammer : public VFATHistogrammer<ThresholdScanHistograms> {
public:
    ThresholdScanHistogrammer(/* const */ DetectorGeometry& detectorGeometry)
        : VFATHistogrammer(detectorGeometry)
    {
    }

    virtual ~ThresholdScanHistogrammer() {}

    void processEvent(unpacker::AMCEvent* event) override
    {
        const auto& payload = event->payload();
        if (payload.runType() != 4)
            return; // Keep only the Threshold events

        const uint8_t dacVal = payload.runParams() & 0xff;

        for (const auto& oh : payload.optohybrids()) {
            for (const auto& vfat : oh.vfats()) {
                auto& histograms = getHistograms(event->softSrcID(), event->softSlot(), oh.InputId(), vfat.pos());

                for (uint8_t ichannel = 0; ichannel < VFAT_CHANNELS; ++ichannel) {
                    bool hit { false };
                    if (ichannel < 64)
                        hit = (vfat.lsData() >> ichannel) & 0x1;
                    else
                        hit = (vfat.msData() >> (ichannel - 64)) & 0x1;

                    if (hit)
                        histograms.chanHitvsDACVal2D.at(dacVal).at(ichannel) += 1;
                }
            }
        }
    }

protected:
    void saveHistograms(ThresholdScanHistograms& histograms) override
    {
        fillHistogram(histograms.chanHitvsDACVal2D, "chanHitvsDACVal2D", "Channel hitmap vs ARM DAC value; ARM threshold (DAC); VFAT channel; Number of hits");
    }
};

/// @brief Histograms holder the for latency scan
struct LatencyHistograms {
    histogram_1d_t<1024> nEventvsLatency {}; // (latency)
    histogram_1d_t<1024> oneHitvsLatency {}; // (latency)
    histogram_1d_t<1024> nHitvsLatency {}; // (latency)
    histogram_2d_t<1024, VFAT_CHANNELS> chanHitvsLatency2D {}; // (latency, channel)
    histogram_2d_t<1024, VFAT_CHANNELS + 1> hitMultvsLatency2D {}; // (latency, channel)
};

/// @brief Histograms producer for the latency scan
class LatencyHistogrammer : public VFATHistogrammer<LatencyHistograms> {
public:
    LatencyHistogrammer(/* const */ DetectorGeometry& detectorGeometry)
        : VFATHistogrammer(detectorGeometry)
    {
    }

    virtual ~LatencyHistogrammer() {}

    void processEvent(unpacker::AMCEvent* event) override
    {
        const auto& payload = event->payload();
        if (payload.runType() != 2)
            return; // Keep only the latency events

        const uint16_t latency = payload.runParams() & 0x3ff;

        for (const auto& oh : payload.optohybrids()) {
            // Zero-suppressed VFATs do not appear in the oh.vfats()
            // collection, but did participate to the event. Fill the
            // nEventvsLatency histograms for those first.
            const auto zsMask = oh.ZSMask();
            for (uint8_t ivfat = 0; ivfat < MAX_VFAT; ++ivfat) {
                if ((zsMask >> ivfat) & 0x1)
                    getHistograms(event->softSrcID(), event->softSlot(), oh.InputId(), ivfat).nEventvsLatency.at(latency) += 1;
            }

            for (const auto& vfat : oh.vfats()) {
                const auto channelMask = m_detectorGeometry.getChannelMask(event->softSrcID(), event->softSlot(), oh.InputId(), vfat.pos());
                const auto lsData = vfat.lsData() & channelMask.lsMask;
                const auto msData = vfat.msData() & channelMask.msMask;

                auto& histograms = getHistograms(event->softSrcID(), event->softSlot(), oh.InputId(), vfat.pos());
                histograms.nEventvsLatency.at(latency) += 1;

                bool anyChannel { false };
                uint8_t numberHits { 0 };

                for (uint8_t ichannel = 0; ichannel < VFAT_CHANNELS; ++ichannel) {
                    bool hit { false };
                    if (ichannel < 64)
                        hit = (lsData >> ichannel) & 0x1;
                    else
                        hit = (msData >> (ichannel - 64)) & 0x1;

                    if (hit) {
                        anyChannel = true;
                        numberHits++;
                        histograms.nHitvsLatency.at(latency) += 1;
                        histograms.chanHitvsLatency2D.at(latency).at(ichannel) += 1;
                    }
                }

                if (anyChannel)
                    histograms.oneHitvsLatency.at(latency) += 1;

                histograms.hitMultvsLatency2D.at(latency).at(numberHits) += 1;
            }
        }
    }

protected:
    void saveHistograms(LatencyHistograms& histograms) override
    {
        fillHistogram(histograms.nEventvsLatency, "nEventvsLatency", "Number of valid events recorded vs Latency; Latency (BX); Number of valid events");
        fillHistogram(histograms.oneHitvsLatency, "oneHitvsLatency", "At least one hit vs Latency; Latency (BX); Number of events with at least one hit");
        fillHistogram(histograms.nHitvsLatency, "nHitvsLatency", "Number of hits vs Latency; Latency (BX); Number of hits");
        fillHistogram(histograms.chanHitvsLatency2D, "chanHitvsLatency2D", "Channel hitmap vs Latency; Latency (BX); VFAT channel; Number of hits");
        fillHistogram(histograms.hitMultvsLatency2D, "hitMultvsLatency2D", "Hit multiplicity vs Latency; Latency (BX); Hit multiplicty per event");
    }
};

int main(int argc, char** argv)
{
    // Argument parsing
    std::string outputPath;
    std::string inputPath;
    std::string eventType;
    std::string analysisType;
    uint64_t firstEvent = 0, lastEvent = -1;
    std::string channelMaskPath;

    CLI::App app { "GEM RAW event to histogram converter tool\n" };
    app.add_option("output", outputPath, "Output file")->required();
    app.add_option("input", inputPath, "Input file")->required();
    app.add_option("-t,--type", eventType, "Event type")
        ->check(CLI::IsMember({ "amc", "amc13" }))
        ->required();
    app.add_option("-a,--analysis", analysisType, "Analysis type")
        ->check(CLI::IsMember({ "occupancy", "scurve", "threshold-scan", "latency" }))
        ->required();
    app.add_option("-f,--first-event", firstEvent, "First event to unpack (starts at 0)");
    app.add_option("-l,--last-event", lastEvent, "Last event to unpack");
    app.add_option("--mask", channelMaskPath, "(Geometry) Channels mask filepath");
    CLI11_PARSE(app, argc, argv)

    // Input file
    std::cout << "Working on file: " << inputPath << std::endl;
    unpacker::raw_file_reader inputFile(inputPath);

    std::unique_ptr<unpacker::AMCEvent> amcEvent;
    std::unique_ptr<unpacker::AMC13Event> amc13Event;

    if (eventType == "amc")
        amcEvent = std::make_unique<unpacker::AMCEvent>();
    else if (eventType == "amc13")
        amc13Event = std::make_unique<unpacker::AMC13Event>();
    else
        throw std::runtime_error("Unknown event type");

    // Geometry
    DetectorGeometry detectorGeometry {};
    if (!channelMaskPath.empty())
        detectorGeometry.loadChannelMask(channelMaskPath);

    // Analysis type
    std::unique_ptr<Histogrammer> histogrammer;

    if (analysisType == "occupancy")
        histogrammer = std::make_unique<OccupancyHistogrammer>(detectorGeometry);
    else if (analysisType == "scurve")
        histogrammer = std::make_unique<SCurveHistogrammer>(detectorGeometry);
    else if (analysisType == "threshold-scan")
        histogrammer = std::make_unique<ThresholdScanHistogrammer>(detectorGeometry);
    else if (analysisType == "latency")
        histogrammer = std::make_unique<LatencyHistogrammer>(detectorGeometry);
    else
        throw std::runtime_error("Unknown analysis type");

    // Processing loop...
    //
    // FIXME Generalize the EventBase class and its usage
    uint64_t eventNumber = 0;
    while (const auto rawEvent = inputFile.event()) {
        if (eventNumber % 100000 == 0)
            fmt::print("{}\n", eventNumber);

        if (eventNumber < firstEvent) {
            ++eventNumber;
            continue;
        }
        if (eventNumber > lastEvent)
            break;

        if (amcEvent) {
            amcEvent->read(rawEvent->data.data());
            histogrammer->processEvent(amcEvent.get());
        } else if (amc13Event) {
            amc13Event->read(rawEvent->data.data());
            for (const auto& child : amc13Event->children())
                histogrammer->processEvent(dynamic_cast<unpacker::AMCEvent*>(child.get()));
        } else {
            throw std::runtime_error("No event parser");
        }

        ++eventNumber;
    }

    // Output file
    TFile outputFile(outputPath.data(), "RECREATE");
    histogrammer->saveAllHistograms(outputFile);
    outputFile.Write();
    outputFile.Close();

    return 0;
}
