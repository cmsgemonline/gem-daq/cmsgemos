/// @file

#include <gem/core/unpacker/VFAT.h>

#include <fmt/format.h>

namespace gem::core::unpacker {

void VFAT::readFirstWord(uint64_t word)
{
    m_pos = 0x1f & (word >> 56);
    m_invBC = 0x1 & (word >> 49);
    m_invCRC = 0x1 & (word >> 48);
    m_header = 0xff & (word >> 40);
    m_ec = 0xff & (word >> 32);
    m_bc = 0xffff & (word >> 16);
    m_msData = (0xffff & word) << 48;
}

void VFAT::readSecondWord(uint64_t word)
{
    m_msData |= 0xffff'ffff'ffff & (word >> 16);
    m_lsData = (word & 0xffff) << 48;
}

void VFAT::readThirdWord(uint64_t word)
{
    m_lsData |= 0xffff'ffff'ffff & (word >> 16);
    m_crc = 0xffff & word;
}

std::ostream& operator<<(std::ostream& os, const VFAT& vfat)
{
    os << fmt::format(FMT_STRING("Position: {}\n"), vfat.m_pos)
       << fmt::format(FMT_STRING("BC Error: {}\n"), vfat.m_invBC)
       << fmt::format(FMT_STRING("CRC Error: {}\n"), vfat.m_invCRC)
       << fmt::format(FMT_STRING("Header: {:#x}\n"), vfat.m_header)
       << fmt::format(FMT_STRING("BC: {:#x}\n"), vfat.m_bc)
       << fmt::format(FMT_STRING("EC: {}\n"), vfat.m_ec)
       << fmt::format(FMT_STRING("Data ( 0- 63): {:#b}\n"), vfat.m_lsData)
       << fmt::format(FMT_STRING("Data (64-127): {:#b}\n"), vfat.m_msData)
       << fmt::format(FMT_STRING("CRC: {:#x}\n"), vfat.m_crc);
    return os;
}

} // namespace gem::core::unpacker
