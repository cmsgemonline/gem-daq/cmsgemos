/// @file

#include <gem/core/unpacker/GEMPayload.h>

#include <fmt/format.h>

#include <cstdlib>

namespace gem::core::unpacker {

void GEMPayload::readGEMheader(uint64_t word)
{
    m_davList = 0xff'ffff & (word >> 40);
    m_fakeMultiBX = 0xf & (word >> 16);
    m_davCnt = 0x1f & (word >> 11);
    m_fv = 0x7 & (word >> 8);
    m_vfType = 0xf & (word >> 4);
    m_tts = 0xf & word;
}

void GEMPayload::readGEMtrailer(uint64_t word)
{
    m_linkTO = 0xff'ffff & (word >> 40);
    m_bp = 0x1 & (word >> 7);
    m_ml = 0x1 & (word >> 6);
    m_cl = 0x1 & (word >> 5);
    m_dr = 0x1 & (word >> 4);
    m_bcl = 0x1 & (word >> 3);
    m_fakeL1A = 0x1 & (word >> 2);
    static bool force_v302a = (std::getenv("GEM_UNPACKER_FORCE_V302A") != nullptr);
    if (m_fv > 0 || force_v302a) {
        m_runType = 0xf & (word >> 32);
        m_runParams = 0xff'ffff & (word >> 8);
        m_l1aF = 0x1 & (word >> 1);
        m_l1aNF = 0x1 & word;
    }
}

bool GEMPayload::l1aF() const
{
    if (m_fv > 0) {
        return m_l1aF;
    } else {
        bool l1aF = 0;
        for (const auto& oh : m_optohybrids)
            l1aF |= oh.L1aF();
        return l1aF;
    }
}

bool GEMPayload::l1aNF() const
{
    if (m_fv > 0) {
        return m_l1aNF;
    } else {
        bool l1aNF = 0;
        for (const auto& oh : m_optohybrids)
            l1aNF |= oh.L1aNF();
        return l1aNF;
    }
}

std::ostream& operator<<(std::ostream& os, const GEMPayload& pl)
{
    os << fmt::format(FMT_STRING("Format version: {}\n"), pl.m_fv)
       << fmt::format(FMT_STRING("VFAT payload type: {}\n"), pl.m_vfType)
       << fmt::format(FMT_STRING("DAV count: {}\n"), pl.m_davCnt)
       << fmt::format(FMT_STRING("DAV list: {:#x}\n"), pl.m_davList)
       << fmt::format(FMT_STRING("Timeout list: {:#x}\n"), pl.m_linkTO)
       << fmt::format(FMT_STRING("TTS: {:#x}\n"), pl.m_tts)
       << fmt::format(FMT_STRING("Run Type: {}\n"), pl.m_runType)
       << fmt::format(FMT_STRING("Run Params: {:#x}\n"), pl.m_runParams)
       << fmt::format(FMT_STRING("Fake multi-BX: {}\n"), pl.m_fakeMultiBX)
       << fmt::format(FMT_STRING("Fake L1A: {}\n"), pl.m_fakeL1A)
       << fmt::format(FMT_STRING("Back-pressure: {}\n"), pl.m_l1aF)
       << fmt::format(FMT_STRING("BC0 locked: {}\n"), pl.m_bcl)
       << fmt::format(FMT_STRING("L1aF: {}\n"), pl.m_l1aF)
       << fmt::format(FMT_STRING("L1aNF: {}\n"), pl.m_l1aNF);
    return os;
}

} // namespace gem::core::unpacker
