/// @file

#include <gem/core/unpacker/raw_file_reader.h>

#include <string.h>
#include <string>

/// @brief Converts a libc error number into a describing string
static std::string get_errno_string(int errnum)
{
    char t_errbuf[512] = { 0 };
    const char* r_errbuf = strerror_r(errnum, t_errbuf, 511);
#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && !_GNU_SOURCE
    return t_errbuf;
#else
    return r_errbuf;
#endif
}

gem::core::unpacker::raw_file_reader::raw_file_reader(const std::string& filename)
{
    m_file = std::fopen(filename.data(), "rb");
    if (m_file == nullptr)
        throw raw_file_error("Error while opening RAW file: " + get_errno_string(errno));

    // Read the first 4 bytes to detect ZSTD compression
    uint32_t magic;
    if (read_file(&magic, 4) != 4)
        throw raw_file_error("File format not supported (too short)");
    rewind(m_file);

    // Prepare the decompression context if the file is ZSTD compressed
    if (magic == ZSTD_MAGICNUMBER) {
        m_is_zstd = true;
        m_context = ZSTD_createDCtx();
        if (m_context == nullptr)
            throw raw_file_error("Could not create a ZSTD decompression context");

        // Initialize ZSTD input buffer
        m_zstd_input_buffer.size = m_zstd_input_buffer.pos = ZSTD_DStreamInSize();
        m_input_buffer = new char[m_zstd_input_buffer.size];
        m_zstd_input_buffer.src = m_input_buffer;
    }
}

gem::core::unpacker::raw_file_reader::~raw_file_reader()
{
    if (m_is_zstd) {
        delete[] m_input_buffer;
        ZSTD_freeDCtx(m_context);
    }
    fclose(m_file);
}

std::unique_ptr<gem::core::unpacker::raw_event> gem::core::unpacker::raw_file_reader::event()
{
    auto event = std::make_unique<raw_event>();

    // Get header
    const size_t rv = read(&event->header, sizeof(event->header));
    if (rv == 0) // This is normal termination
        return {};
    if (rv != sizeof(event->header))
        throw raw_file_error("Reached premature end-of-file (is the file complete?)");

    // Get data
    event->data.resize(event->header.event_size / sizeof(decltype(event->data)::value_type));
    if (read(event->data.data(), event->header.event_size) != event->header.event_size)
        throw raw_file_error("Reached premature end-of-file (is the file complete?)");

    return event;
}

size_t gem::core::unpacker::raw_file_reader::read_file(void* data, const size_t size)
{
    const size_t rv = std::fread(data, 1, size, m_file);

    if (rv == size) // All OK
        return size;
    else if (std::ferror(m_file)) // Error
        throw raw_file_error("Error while RAW reading file: " + get_errno_string(errno));
    else // EOF
        return rv;
}

size_t gem::core::unpacker::raw_file_reader::read_zstd(void* data, size_t size)
{
    ZSTD_outBuffer zstd_output_buffer = { data, size, 0 };

    for (;;) {
        // If the intput buffer is exhausted, read a new chunk
        if (m_zstd_input_buffer.pos == m_zstd_input_buffer.size) {
            const size_t rv = read_file(m_input_buffer, m_zstd_input_buffer.size);

            // Reset the buffer pointers
            m_zstd_input_buffer.pos = 0;
            m_zstd_input_buffer.size = rv;
        }

        // We completed an ZSTD frame and have nothing left to read
        //
        // Note it MUST be checked before calling ZSTD_decompressStream. If not
        // the return value will ask for more bytes and the error/termination
        // handling will be made more difficult.
        if (m_last_zstd_rv == 0 && m_zstd_input_buffer.size == 0)
            return 0;

        // Decompress data from the input buffer
        m_last_zstd_rv = ZSTD_decompressStream(m_context, &zstd_output_buffer, &m_zstd_input_buffer);
        if (ZSTD_isError(m_last_zstd_rv))
            throw raw_file_error(std::string("Error occured during ZSTD decompression: ") + ZSTD_getErrorName(m_last_zstd_rv));

        // We got the number of byte we need
        if (zstd_output_buffer.pos == zstd_output_buffer.size)
            break;

        // We reached the EOF and the frame is fully flushed, stop here
        if (m_last_zstd_rv == 0 && m_zstd_input_buffer.size == 0)
            return zstd_output_buffer.pos;

        // We didn't reach the end of the ZSTD frame but cannot pull more data from the file, this is an error
        if (m_last_zstd_rv > 0 && m_zstd_input_buffer.size == 0)
            throw raw_file_error("ZSTD frame not flushed, but not data left in file");
    }

    return size;
}

size_t gem::core::unpacker::raw_file_reader::read(void* data, const size_t size)
{
    if (m_is_zstd)
        return read_zstd(data, size);
    else
        return read_file(data, size);
}
