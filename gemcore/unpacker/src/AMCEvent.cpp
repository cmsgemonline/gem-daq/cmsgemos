/// @file

#include <gem/core/unpacker/AMCEvent.h>

#include <fmt/format.h>

namespace gem::core::unpacker {

size_t AMCEvent::read(const uint64_t* data)
{
    m_payload.clearOptoHybrids();

    const uint64_t* const begin = data;

    readAMCheader1(*data++);
    readAMCheader2(*data++);

    m_payload.readGEMheader(*data++);

    for (uint8_t j = 0; j < m_payload.davCnt(); ++j) {
        OptoHybrid& optohybrid = m_payload.addOptoHybrid();
        optohybrid.setVersion(m_fv);
        optohybrid.readChamberHeader(*data++);
        for (uint16_t k = 0; k < optohybrid.vfWdCnt() / 3; ++k) {
            // This is an Input FIFO error
            // Skip the VFAT block filling payload
            if (*data == 0xffffffffffff0000) {
                data += 3;
                continue;
            }
            VFAT& vfat = optohybrid.addVFAT();
            vfat.readFirstWord(*data++);
            vfat.readSecondWord(*data++);
            vfat.readThirdWord(*data++);
        }
        optohybrid.readChamberTrailer(*data++);
    }

    m_payload.readGEMtrailer(*data++);

    readAMCtrailer(*data++);

    return data - begin;
}

void AMCEvent::readAMCheader1(uint64_t word)
{
    m_slot = 0xf & (word >> 56);
    m_ec = 0xff'ffff & (word >> 32);
    m_bc = 0xfff & (word >> 20);
}

void AMCEvent::readAMCheader2(uint64_t word)
{
    m_fv = 0xf & (word >> 60);
    m_softSlot = 0xf & (word >> 12);
    m_softSrcID = 0xfff & word;
    if (m_fv == 0) {
        m_oc = 0xffff & (word >> 16);
        m_payload.setRunType((0xf & (word >> 56)));
        m_payload.setRunParams((0xff'ffff & (word >> 32)));
    } else {
        m_oc = 0xffff'ffff & (word >> 16);
    }
}

void AMCEvent::readAMCtrailer(uint64_t word)
{
    m_crc = 0xffff'ffff & (word >> 32);
    m_ecT = 0xff & (word >> 24);
    m_evtLen = 0xf'ffff & word;
}

std::ostream& AMCEvent::print(std::ostream& os) const
{
    os << fmt::format(FMT_STRING("Format version: {}\n"), m_fv)
       << fmt::format(FMT_STRING("Slot number: {}\n"), m_slot)
       << fmt::format(FMT_STRING("FED ID: {}\n"), m_softSrcID)
       << fmt::format(FMT_STRING("AMC ID: {}\n"), m_softSlot)
       << fmt::format(FMT_STRING("EC: {}\n"), m_ec)
       << fmt::format(FMT_STRING("OC: {}\n"), m_oc)
       << fmt::format(FMT_STRING("BC: {:#x}\n"), m_bc)
       << fmt::format(FMT_STRING("Event length: {}\n"), m_evtLen);
    return os;
}

} // namespace gem::core::unpacker
