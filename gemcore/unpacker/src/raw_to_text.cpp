/// @file

#include <gem/core/unpacker/AMC13Event.h>
#include <gem/core/unpacker/AMCEvent.h>
#include <gem/core/unpacker/GEMPayload.h>
#include <gem/core/unpacker/OptoHybrid.h>
#include <gem/core/unpacker/VFAT.h>
#include <gem/core/unpacker/raw_file_reader.h>

#include <CLI/CLI.hpp>
#include <fmt/color.h>
#include <fmt/format.h>
#include <fmt/ostream.h>

#include <fstream>
#include <iostream>

namespace unpacker = gem::core::unpacker;

// clang-format off
template <> struct fmt::formatter<unpacker::AMC13Event> : ostream_formatter { };
template <> struct fmt::formatter<unpacker::AMCEvent> : ostream_formatter { };
template <> struct fmt::formatter<unpacker::GEMPayload> : ostream_formatter { };
template <> struct fmt::formatter<unpacker::OptoHybrid> : ostream_formatter { };
template <> struct fmt::formatter<unpacker::VFAT> : ostream_formatter { };
// clang-format on

/// @brief Filtering @c streambuf indenting each new line
///
/// Heavily inspired by https://stackoverflow.com/a/9600752
class IndentOStream : public std::streambuf {
public:
    explicit IndentOStream(std::streambuf* dest, int indent = 4)
        : m_dest(dest)
        , m_isAtStartOfLine(true)
        , m_indent(indent, ' ')
        , m_owner(nullptr)
    {
    }
    explicit IndentOStream(std::ostream& dest, int indent = 4)
        : m_dest(dest.rdbuf())
        , m_isAtStartOfLine(true)
        , m_indent(indent, ' ')
        , m_owner(&dest)
    {
        m_owner->rdbuf(this);
    }
    virtual ~IndentOStream()
    {
        if (m_owner != nullptr) {
            m_owner->rdbuf(m_dest);
        }
    }

protected:
    virtual int overflow(int ch)
    {
        if (m_isAtStartOfLine && ch != '\n') {
            m_dest->sputn(m_indent.data(), m_indent.size());
        }
        m_isAtStartOfLine = ch == '\n';
        return m_dest->sputc(ch);
    }

private:
    std::streambuf* m_dest;
    bool m_isAtStartOfLine;
    std::string m_indent;
    std::ostream* m_owner;
};

template <typename T>
void frame(const T& value, const int size = 30)
{
    fmt::print(std::cout, "{}\n", std::string(size, '-'));
    fmt::print(std::cout, "{}\n", value);
    fmt::print(std::cout, "{}\n", std::string(size, '-'));
}

void print(const unpacker::AMCEvent& event)
{
    frame("AMC Event");
    fmt::print(std::cout, "{}", event);

    const auto& payload = event.payload();
    frame("GEM Payload");
    fmt::print(std::cout, "{}", payload);

    const auto& ohs = payload.optohybrids();
    for (size_t j = 0; j < ohs.size(); ++j) {
        IndentOStream indent1(std::cout);
        frame(fmt::format("OptoHybrid Payload #{}", j));
        fmt::print(std::cout, "{}", ohs.at(j));

        const auto& vfats = ohs.at(j).vfats();
        for (size_t k = 0; k < vfats.size(); ++k) {
            IndentOStream indent2(std::cout);
            frame(fmt::format("VFAT Block #{}", k));
            fmt::print(std::cout, "{}", vfats.at(k));
        }
    }
}

void print(const unpacker::AMC13Event& event)
{
    frame("AMC13 Event");
    fmt::print(std::cout, "{}", event);

    size_t number_of_amc = event.numberOfChildren();
    for (size_t i = 0; i < number_of_amc; ++i) {
        const auto& amc = *dynamic_cast<unpacker::AMCEvent*>(event.child(i));
        print(amc);
    }
}

int main(int argc, char** argv)
{
    std::string input_filename;
    std::string event_type;
    uint64_t first_event = 0, last_event = -1;

    CLI::App app { "GEM RAW events to text converter" };
    app.add_option("-i,--input", input_filename, "Input file")->required();
    app.add_option("-t,--type", event_type, "Event type")
        ->check(CLI::IsMember({ "amc", "amc13" }))
        ->required();
    app.add_option("-f,--first-event", first_event, "First event to unpack (starts at 0)");
    app.add_option("-l,--last-event", last_event, "Last event to unpack");
    CLI11_PARSE(app, argc, argv)

    fmt::print(fg(fmt::terminal_color::blue), "Working on file {} with event type {}\n", input_filename, event_type);

    unpacker::raw_file_reader raw_file(input_filename);

    std::unique_ptr<unpacker::EventBase> event;
    if (event_type == "amc")
        event = std::make_unique<unpacker::AMCEvent>();
    else if (event_type == "amc13")
        event = std::make_unique<unpacker::AMC13Event>();
    else {
        std::cerr << "Event type '" << event_type << "' not supported" << std::endl;
        return 1;
    }

    uint64_t event_number = 0;
    while (const auto raw_event = raw_file.event()) {
        if (event_number < first_event) {
            ++event_number;
            continue;
        }
        if (event_number > last_event)
            break;

        event->read(raw_event->data.data());

        frame(fmt::styled(fmt::format(FMT_STRING("Event #{}"), event_number), fg(fmt::terminal_color::blue)));

        if (event_type == "amc") {
            print(*dynamic_cast<unpacker::AMCEvent*>(event.get()));
        } else {
            print(*dynamic_cast<unpacker::AMC13Event*>(event.get()));
        }

        ++event_number;
    }
    return 0;
}
