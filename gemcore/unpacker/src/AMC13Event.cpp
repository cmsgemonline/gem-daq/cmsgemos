/// @file

#include <gem/core/unpacker/AMC13Event.h>

#include <fmt/format.h>

#include <memory>

namespace gem::core::unpacker {

size_t AMC13Event::read(const uint64_t* data)
{
    clearChildren();

    const uint64_t* const begin = data;

    readCDFheader(*data++);
    readAMC13header(*data++);

    for (uint16_t i = 0; i < m_namc; ++i) {
        AMCHeader amch = {
            uint32_t(0xffffff & ((*data) >> 32)),
            uint8_t(0xff & ((*data) >> 20)),
            uint8_t(0xf & ((*data) >> 16)),
            uint16_t(0xffff & (*data))
        };
        m_amcHeader.emplace_back(amch);
        data++;
    }

    for (uint16_t i = 0; i < m_namc; ++i) {
        auto amc = std::make_unique<AMCEvent>();
        data += amc->read(data);
        addChild(std::move(amc));
    }

    readAMC13trailer(*data++);
    readCDFtrailer(*data++);

    return data - begin;
}

void AMC13Event::readCDFheader(uint64_t word)
{
    m_boe = 0xf & (word >> 60);
    m_evtType = 0xf & (word >> 56);
    m_ec_cdf = 0xff'ffff & (word >> 32);
    m_bc_cdf = 0xfff & (word >> 20);
    m_srcID = 0xfff & (word >> 8);
    m_fv_cdf = 0xf & (word >> 4);
    m_H = 0x1 & (word >> 3);
}

void AMC13Event::readAMC13header(uint64_t word)
{
    m_fv = 0xf & (word >> 60);
    m_calType = 0xf & (word >> 56);
    m_namc = 0xf & (word >> 52);
    m_oc = 0xffffffff & (word >> 4);
}

void AMC13Event::readAMC13trailer(uint64_t word)
{
    m_crc32 = 0xffff'ffff & (word >> 32);
    m_blkSeqNo = 0xff & (word >> 20);
    m_ec = 0xff & (word >> 12);
    m_bc = 0xfff & word;
}

void AMC13Event::readCDFtrailer(uint64_t word)
{
    m_eoe = 0xf & (word >> 60);
    m_evtLen = 0xff'ffff & (word >> 32);
    m_crc16 = 0xffff & (word >> 16);
    m_C = 0x1 & (word >> 15);
    m_F = 0x1 & (word >> 14);
    m_evtStatus = 0xf & (word >> 8);
    m_tts = 0x7 & (word >> 4);
    m_T = 0x1 & (word >> 3);
    m_R = 0x1 & (word >> 2);
}

std::ostream& AMC13Event::print(std::ostream& os) const
{
    os << fmt::format(FMT_STRING("FED ID: {}\n"), m_srcID)
       << fmt::format(FMT_STRING("EC: {}\n"), m_ec_cdf)
       << fmt::format(FMT_STRING("OC: {}\n"), m_oc)
       << fmt::format(FMT_STRING("BC: {:#x}\n"), m_bc_cdf)
       << fmt::format(FMT_STRING("Event size: {}\n"), m_evtLen)
       << fmt::format(FMT_STRING("Number of AMCs: {}\n"), m_namc)
       << fmt::format(FMT_STRING("AMC block headers:\n"));

    for (uint16_t i = 0; i < m_namc; ++i) {
        os << fmt::format(FMT_STRING("  Slot: {}\n"), m_amcHeader.at(i).slot)
           << fmt::format(FMT_STRING("  Size: {}\n"), m_amcHeader.at(i).size);
    }

    return os;
}

} // namespace gem::core::unpacker
