cmsgemos_add_module(
    unpacker
    SOURCES
    AMC13Event.cpp
    AMCEvent.cpp
    GEMPayload.cpp
    OptoHybrid.cpp
    raw_file_reader.cpp
    VFAT.cpp
)

target_link_libraries(gemcore_unpacker PUBLIC PkgConfig::libzstd)
target_link_libraries(gemcore_unpacker PRIVATE fmt::fmt)

add_executable(gem-raw-to-text src/raw_to_text.cpp)
target_link_libraries(gem-raw-to-text PRIVATE gemcore_unpacker)
target_link_libraries(gem-raw-to-text PRIVATE CLI11::CLI11 fmt::fmt)
install(TARGETS gem-raw-to-text RUNTIME DESTINATION "${CMAKE_INSTALL_FULL_BINDIR}")

add_executable(gem-raw-to-histogram src/raw_to_histogram.cpp)
target_link_libraries(gem-raw-to-histogram PRIVATE gemcore)
target_link_libraries(gem-raw-to-histogram PRIVATE gemcore_unpacker)
target_link_libraries(gem-raw-to-histogram PRIVATE ROOT::Hist ROOT::RIO)
target_link_libraries(gem-raw-to-histogram PRIVATE CLI11::CLI11 fmt::fmt)
install(TARGETS gem-raw-to-histogram RUNTIME DESTINATION "${CMAKE_INSTALL_FULL_BINDIR}")
