/// @file

#ifndef GEM_CORE_UNPACKER_VFAT_H
#define GEM_CORE_UNPACKER_VFAT_H

#include <cstdint>
#include <ostream>

namespace gem::core::unpacker {

/// @brief Class used to manage the VFAT data
class VFAT {

    friend std::ostream& operator<<(std::ostream& os, const VFAT& vfat);

public:
    /// @brief Default constructor
    VFAT() {}

    /// @brief Destructor
    ~VFAT() {}

    /// @brief Read the first word from the VFAT block
    void readFirstWord(uint64_t word);

    /// @brief Read the second word from the VFAT block
    void readSecondWord(uint64_t word);

    /// @brief Read the third word from the VFAT block
    void readThirdWord(uint64_t word);

    uint16_t pos() const { return m_pos; }
    bool invBC() const { return m_invBC; }
    bool invCRC() const { return m_invCRC; }
    uint8_t header() const { return m_header; }
    uint8_t ec() const { return m_ec; }
    uint16_t bc() const { return m_bc; }
    uint64_t lsData() const { return m_lsData; }
    uint64_t msData() const { return m_msData; }
    uint16_t crc() const { return m_crc; }

private:
    uint8_t m_pos; ///< VFAT position within the OptoHybrid
    bool m_invBC; ///< Invalid VFAT BC (@c false if the BC is out-of-range, @c true otherwise)
    bool m_invCRC; ///< Invalid VFAT CRC (@c false if the CRC check passed, @c true if the CRC check failed)
    uint8_t m_header; ///< VFAT header
    uint8_t m_ec; ///< Event counter
    uint16_t m_bc; ///< Bunch counter
    uint64_t m_lsData; ///< Channels from 0 to 63
    uint64_t m_msData; ///< Channels from 64 to 127
    uint16_t m_crc; ///< VFAT block CRC
};

} // namespace gem::core::unpacker

#endif // GEM_CORE_UNPACKER_VFAT_H
