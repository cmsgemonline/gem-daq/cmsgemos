/// @file

#ifndef GEM_CORE_UNPACKER_GEMPAYLOAD_H
#define GEM_CORE_UNPACKER_GEMPAYLOAD_H

#include <gem/core/unpacker/OptoHybrid.h>

#include <cstdint>
#include <ostream>
#include <vector>

namespace gem::core::unpacker {

/// @brief Class used to manage the GEMPayload data
class GEMPayload {

    friend std::ostream& operator<<(std::ostream& os, const GEMPayload& pl);

public:
    /// @brief Default constructor
    GEMPayload() {}

    /// @brief Destructor
    ~GEMPayload() {}

    /// @brief Create and add a new child OptoHybrid
    OptoHybrid& addOptoHybrid() { return m_optohybrids.emplace_back(); }

    /// @brief Clear all child OptoHybrids
    void clearOptoHybrids() { m_optohybrids.clear(); }

    /// @brief Return a vector containing all child OptoHybrids
    const std::vector<OptoHybrid>& optohybrids() const { return m_optohybrids; }

    /// @brief Set the event run type (for v301b)
    void setRunType(uint8_t runType) { m_runType = runType; }

    /// @brief Get the event run params (for v301b)
    void setRunParams(uint32_t runParams) { m_runParams = runParams; }

    /// @brief Reads the GEM payload header
    void readGEMheader(uint64_t word);

    /// @brief Reads the GEM payload trailer
    void readGEMtrailer(uint64_t word);

    uint32_t davList() const { return m_davList; }
    uint8_t fakeMultiBX() const { return m_fakeMultiBX; }
    uint8_t davCnt() const { return m_davCnt; }
    uint8_t fv() const { return m_fv; }
    uint8_t vfType() const { return m_vfType; }
    uint8_t tts() const { return m_tts; }

    uint32_t linkTO() const { return m_linkTO; }
    uint8_t runType() const { return m_runType; }
    uint32_t runParams() const { return m_runParams; }
    bool bp() const { return m_bp; }
    bool ml() const { return m_ml; }
    bool cl() const { return m_cl; }
    bool dr() const { return m_dr; }
    bool bcl() const { return m_bcl; }
    bool fakeL1A() const { return m_fakeL1A; }
    bool l1aF() const;
    bool l1aNF() const;

private:
    std::vector<OptoHybrid> m_optohybrids;

    uint32_t m_davList; ///< Bitmask indicating which OptoHybrids have data
    uint8_t m_fakeMultiBX; ///< Number of additional L1A sent in fake multi-BX readout mode
    uint8_t m_davCnt; ///< Number of OptoHybrids that have data
    uint8_t m_fv;
    uint8_t m_vfType; ///< VFAT paylod type
    uint8_t m_tts;

    uint32_t m_linkTO;
    uint8_t m_runType;
    uint32_t m_runParams;
    bool m_bp; ///< Back-pressure
    bool m_ml; ///< MMCM locked
    bool m_cl; ///< DAQ clock locked
    bool m_dr; ///< DAQ ready
    bool m_bcl; ///< BC0 locked
    bool m_fakeL1A; ///< Whether this is a "true" or a "fake" L1A
    bool m_l1aF; ///< L1A FIFO full
    bool m_l1aNF; ///< L1A FIFO near full
};

} // namespace gem::core::unpacker

#endif // GEM_CORE_UNPACKER_GEMPAYLOAD_H
