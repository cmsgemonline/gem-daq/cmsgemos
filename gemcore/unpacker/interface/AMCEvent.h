/// @file

#ifndef GEM_CORE_UNPACKER_AMCEVENT_H
#define GEM_CORE_UNPACKER_AMCEVENT_H

#include <gem/core/unpacker/EventBase.h>

#include <cstdint>
#include <ostream>

namespace gem::core::unpacker {

/// @brief Class used to manage the AMC event data
class AMCEvent : public EventBase {

    friend std::ostream& operator<<(std::ostream& os, const AMCEvent& ev)
    {
        return ev.print(os);
    }

public:
    /// @brief Deafult Constructor
    AMCEvent() {}

    /// @brief Destructor
    ~AMCEvent() {}

    /// @brief Unpack the AMC event data
    ///
    /// @param @c data Array containing the AMC event data
    size_t read(const uint64_t* data) override;

    std::ostream& print(std::ostream& os) const override;

    /// @brief Read the first AMC header
    void readAMCheader1(uint64_t word);

    /// @brief Read the second AMC header
    void readAMCheader2(uint64_t word);

    /// @brief Read the AMC trailer
    void readAMCtrailer(uint64_t word);

    uint16_t slot() const { return m_slot; }
    uint64_t ec() const { return m_ec; }
    uint16_t bc() const { return m_bc; }

    uint16_t fv() const { return m_fv; }
    uint32_t oc() const { return m_oc; }
    uint16_t softSlot() const { return m_softSlot; }
    uint16_t softSrcID() const { return m_softSrcID; }
    uint16_t boardID() const { return ((m_softSlot << 12) | m_softSrcID); } // v301

    uint32_t crc() const { return m_crc; }
    uint32_t evtLen() const { return m_evtLen; }
    uint16_t fedID() const { return m_softSrcID; }

private:
    uint8_t m_slot;
    uint32_t m_ec;
    uint16_t m_bc;

    uint8_t m_fv;
    uint32_t m_oc;
    uint8_t m_softSlot;
    uint16_t m_softSrcID;

    uint32_t m_crc;
    uint8_t m_ecT;
    uint32_t m_evtLen;
};

} // namespace gem::core::unpacker

#endif // GEM_CORE_UNPACKER_AMCEVENT_H
