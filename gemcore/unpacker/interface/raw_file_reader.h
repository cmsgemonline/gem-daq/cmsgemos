/// @file

#ifndef GEM_CORE_UNPACKER_RAW_FILE_READER_H
#define GEM_CORE_UNPACKER_RAW_FILE_READER_H

#include <zstd.h>

#include <cstdio>
#include <memory>
#include <string>
#include <vector>

namespace gem::core::unpacker {

/// @brief Structure holding a RAW event
struct raw_event {
    struct {
        uint16_t version;
        uint16_t flags;
        uint32_t run_number;
        uint32_t lumisection;
        uint32_t event_number;
        uint32_t event_size; ///< Event RAW data size in bytes
        uint32_t crc32c;
    } header; ///< RAW file event header
    std::vector<uint64_t> data; ///< Event RAW data
};

/// @brief Exception used to signal errors related to RAW file handling
class raw_file_error : public std::exception {
public:
    /// @brief Constructor
    ///
    /// @param @c what descriptive error message
    explicit raw_file_error(const std::string& what)
        : m_what(what)
    {
    }

    /// @brief Description of the error
    const char* what() const noexcept override { return m_what.data(); }

protected:
    std::string m_what = ""; ///< Error message
};

/// @brief Class used to read events from a RAW file
///
/// The class is able to automatically detect whether the file is uncompressed
/// or ZSTD compress based on the ZSTD frame magic number.
class raw_file_reader {
public:
    /// @brief Constructor
    ///
    /// @param @c filename File to read events from
    raw_file_reader(const std::string& filename);

    /// @brief Destructor
    ~raw_file_reader();

    /// @brief Fetch one event from file
    ///
    /// @return A @c std::unique_ptr to the event. An empty pointer means end-of-file.
    std::unique_ptr<raw_event> event();

protected:
    /// @brief Read uncompressed data directly from file
    ///
    /// @param @c data Buffer in which to write the data
    /// @param @c size Number of bytes to read
    ///
    /// @return The number of bytes actually read (any value different than @c size means end-of-file)
    size_t read_file(void* data, size_t size);

    /// @brief Read and decompressed on the fly ZSTD data from file
    ///
    /// @param @c data Buffer in which to write the data
    /// @param @c size Number of bytes to read
    ///
    /// @return The number of bytes actually read (any value different than @c size means end-of-file)
    size_t read_zstd(void* data, size_t size);

    /// @brief Read data from file, decompressing it if needed
    ///
    /// @param @c data Buffer in which to write the data
    /// @param @c size Number of bytes to read
    ///
    /// @return The number of bytes actually read (any value different than @c size means end-of-file)
    size_t read(void* data, size_t size);

protected:
    std::FILE* m_file = nullptr;
    bool m_is_zstd = false;

    // For ZSTD decompression
    ZSTD_DCtx* m_context = nullptr;
    size_t m_last_zstd_rv = 0;
    char* m_input_buffer = nullptr;
    ZSTD_inBuffer m_zstd_input_buffer;
};

} // namespace gem::core::unpacker

#endif // GEM_CORE_UNPACKER_RAW_FILE_READER_H
