/// @file

#ifndef GEM_CORE_UNPACKER_AMC13EVENT_H
#define GEM_CORE_UNPACKER_AMC13EVENT_H

#include <gem/core/unpacker/AMCEvent.h>

#include <cstdint>
#include <vector>

namespace gem::core::unpacker {

/// @brief Class used to manage the AMC13 event data
class AMC13Event : public EventBase {

    friend std::ostream& operator<<(std::ostream& os, const AMC13Event& ev)
    {
        return ev.print(os);
    }

public:
    struct AMCHeader {
        uint32_t size;
        uint8_t blkSeqNo;
        uint8_t slot;
        uint16_t boardID;
    };

public:
    /// @brief Default constructor
    AMC13Event() {}

    /// @brief Destructor
    ~AMC13Event() {}

    /// @brief Unpack the AMC13 event data
    ///
    /// @param @c data Array containing the AMC13 event data
    size_t read(const uint64_t* data) override;

    std::ostream& print(std::ostream& os) const override;

    /// @brief Read the CDF header
    void readCDFheader(uint64_t);

    /// @brief Read the AMC13 header
    void readAMC13header(uint64_t);

    /// @brief Read the AMC13 trailer
    void readAMC13trailer(uint64_t);

    /// @brief Read the CDF trailer
    void readCDFtrailer(uint64_t);

    uint16_t boe() const { return m_boe; }
    uint16_t evtType() const { return m_evtType; }
    uint32_t ec_cdf() const { return m_ec_cdf; }
    uint16_t bc_cdf() const { return m_bc_cdf; }
    uint32_t srcID() const { return m_srcID; }
    uint16_t fv_cdf() const { return m_fv_cdf; }

    uint16_t eoe() const { return m_eoe; }
    uint16_t evtLen() const { return m_evtLen; }
    uint16_t crc16() const { return m_crc16; }
    uint16_t evtStatus() const { return m_evtStatus; }
    uint16_t tts() const { return m_tts; }

    uint16_t fv() const { return m_fv; }
    uint16_t calType() const { return m_calType; }
    uint16_t namc() const { return m_namc; }
    uint32_t oc() const { return m_oc; }

    uint32_t crc32() const { return m_crc32; }
    uint16_t blkSeqNo() const { return m_blkSeqNo; }
    uint32_t ec() const { return m_ec; }
    uint16_t bc() const { return m_bc; }

private:
    std::vector<AMCHeader> m_amcHeader;

    uint8_t m_boe; ///< BEO (0x5)
    uint8_t m_evtType; ///< Event type
    uint32_t m_ec_cdf; ///< Event counter
    uint16_t m_bc_cdf; ///< Bunch counter
    uint16_t m_srcID; ///< FED ID
    uint8_t m_fv_cdf; ///< CDF format version
    bool m_H; ///< Indicates that another header word is following

    uint8_t m_eoe;
    uint32_t m_evtLen;
    uint16_t m_crc16;
    bool m_C; ///< Indicates that the FRL/FEROL has detected a transmission error over the S-link cable
    bool m_F; ///< Indicates that the FED ID given by the FED is not the one expected by the FRL/FEROL
    uint8_t m_evtStatus;
    uint8_t m_tts;
    bool m_T; ///< Indicates that another trailer word is following
    bool m_R; ///< Indicates that the CRC value has been modified by the S-link sender card

    uint8_t m_fv; ///< Format version
    uint8_t m_calType; ///< Calibration type
    uint8_t m_namc; ///< Number of AMCs
    uint32_t m_oc; ///< Orbit counter

    uint32_t m_crc32;
    uint8_t m_blkSeqNo; ///< Block number
    uint8_t m_ec; ///< Event counter
    uint16_t m_bc; ///< Bunch counter
};

} // namespace gem::core::unpacker

#endif // GEM_CORE_UNPACKER_AMC13EVENT_H
