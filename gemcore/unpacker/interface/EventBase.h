/// @file

#ifndef GEM_CORE_UNPACKER_EVENTBASE_H
#define GEM_CORE_UNPACKER_EVENTBASE_H

#include <gem/core/unpacker/GEMPayload.h>

#include <cstdint>
#include <memory>
#include <ostream>

namespace gem::core::unpacker {

/// @brief Event Data Class interface
class EventBase {

    friend std::ostream& operator<<(std::ostream& os, const EventBase& ev)
    {
        return ev.print(os);
    }

public:
    /// @brief Default constructor
    EventBase() {}

    /// @brief Simple constructor
    EventBase(uint32_t runNumber, uint32_t evtNumber, uint32_t evtSize)
        : m_runNumber(runNumber)
        , m_evtNumber(evtNumber)
        , m_evtSize(evtSize)
    {
    }

    /// @brief Destructor
    ~EventBase() {}

    /// @brief Unpack the event data
    ///
    /// @param @c data Array containing the event data
    virtual size_t read(const uint64_t* data) = 0;

    /// @brief Print the event
    virtual std::ostream& print(std::ostream& os) const = 0;

    const GEMPayload& payload() const { return m_payload; }

    void addChild(std::unique_ptr<EventBase>&& child)
    {
        m_children.push_back(std::move(child));
    }

    void clearChildren()
    {
        m_children.clear();
    }

    size_t numberOfChildren() const
    {
        return m_children.size();
    }

    EventBase* child(size_t i) const
    {
        EventBase* c = (i < numberOfChildren()) ? m_children[i].get() : nullptr;
        return c;
    }

    const auto& children() const
    {
        return m_children;
    }

    uint32_t runNumber() const
    {
        return m_runNumber;
    }
    uint32_t evtNumber() const
    {
        return m_evtNumber;
    }
    uint32_t evtSize() const
    {
        return m_evtSize;
    }

    void setRunNumber(uint32_t runNumber)
    {
        m_runNumber = runNumber;
    }
    void setEvtNumber(uint32_t evtNumber)
    {
        m_evtNumber = evtNumber;
    }
    void setEvtSize(uint32_t evtSize)
    {
        m_evtSize = evtSize;
    }

protected:
    uint32_t m_runNumber = 0;
    uint32_t m_evtNumber = 0;
    uint32_t m_evtSize = 0;
    GEMPayload m_payload;
    std::vector<std::unique_ptr<EventBase>> m_children;
};

} // namespace gem::core::unpacker

#endif // GEM_CORE_UNPACKER_EVENTBASE_H
