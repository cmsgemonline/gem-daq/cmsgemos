/// @file

#ifndef GEM_CORE_UNPACKER_OPTOHYBRID_H
#define GEM_CORE_UNPACKER_OPTOHYBRID_H

#include <gem/core/unpacker/VFAT.h>

#include <cstdint>
#include <ostream>
#include <vector>

namespace gem::core::unpacker {

/// @brief Class used to manage the OptoHybrid data
class OptoHybrid {

    friend std::ostream& operator<<(std::ostream& os, const OptoHybrid& oh);

public:
    /// @brief Default constructor
    OptoHybrid() {};

    /// @brief Destructor
    ~OptoHybrid() {}

    /// @brief Create and add a new child VFAT
    VFAT& addVFAT() { return m_vfats.emplace_back(); }

    /// @brief Clear all child VFATs
    void clearVFATs() { m_vfats.clear(); }

    /// @brief Return a vector containing all child VFATs
    const std::vector<VFAT>& vfats() const { return m_vfats; }

    /// @brief Set GEM payload version
    void setVersion(uint16_t version) { m_version = version; }

    /// @brief Get the GEM payload version
    uint8_t version() const { return m_version; }

    /// @brief Read the chamber header
    void readChamberHeader(uint64_t word);

    /// @brief Read the chamber trailer
    void readChamberTrailer(uint64_t word);

    uint8_t CalibChan() const { return m_CalibChan; }
    uint8_t InputId() const { return m_InputID; }
    uint16_t vfWdCnt() const { return m_VfWdCnt; }
    bool EvtF() const { return m_EvtF; }
    bool InF() const { return m_InF; }
    bool EvtSzOFW() const { return m_EvtSzOFW; }
    bool EvtNF() const { return m_EvtNF; }
    bool InNF() const { return m_InNF; }
    bool InvBC() const { return m_InvBC; }
    bool EvtSzW() const { return m_EvtSzW; }
    bool InvCRC() const { return m_InvCRC; }
    bool OSCcAvV() const { return m_OOScAvV; }
    bool OOscVvV() const { return m_OOScVvV; }
    bool BxmAvV() const { return m_BxmAvV; }
    bool BxmVvV() const { return m_BxmVvV; }
    bool OOScAvO() const { return m_OOScAvO; }

    bool L1aF() const { return m_l1aF; }
    bool L1aNF() const { return m_l1aNF; }

    uint16_t VftWdCntTr() const { return m_VfWdCntTr; }
    uint8_t InUfw() const { return m_InUfw; }
    uint32_t ZSMask() const { return m_ZSMask; }
    uint32_t VFATMask() const { return m_VFATMask; }

private:
    std::vector<VFAT> m_vfats;

    uint16_t m_version;

    uint8_t m_CalibChan;
    uint8_t m_InputID;
    uint16_t m_VfWdCnt;
    bool m_EvtF;
    bool m_InF;
    bool m_EvtSzOFW;
    bool m_EvtNF;
    bool m_InNF;
    bool m_InvBC;
    bool m_EvtSzW;
    bool m_InvCRC;
    bool m_OOScAvV;
    bool m_OOScVvV;
    bool m_BxmAvV;
    bool m_BxmVvV;
    bool m_OOScAvO;

    // v301
    bool m_l1aF;
    bool m_l1aNF;

    uint16_t m_VfWdCntTr;
    bool m_InUfw;
    uint32_t m_ZSMask; ///< Bitmask indicating if the corresponding VFAT block has been zero suppressed
    uint32_t m_VFATMask; ///< Bitmask indicating if the corresponding VFAT is enabled in the DAQ readout
};

} // namespace gem::core::unpacker

#endif // GEM_CORE_UNPACKER_OPTOHYBRID_H
