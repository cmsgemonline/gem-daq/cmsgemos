/// @file

#ifndef GEM_CORE_DIM_DCS_H
#define GEM_CORE_DIM_DCS_H

#include <dim/dic.hxx>
#include <dim/dis.hxx>

#include <algorithm>
#include <future>
#include <limits>
#include <mutex>
#include <string>
#include <type_traits>
#include <utility>

namespace gem::core::dim {

/// @brief Stores the value considered as invalid in a DIM service
template <typename T>
constexpr T invalid_dim_v = -(1 << std::numeric_limits<float>::digits);

/// @brief Structure made to store the high voltage information applied by the DCS
///
/// @details Is a @c I:7 DIM type
struct hv2_dim_broker {
    enum class hv_status_t : int {
        OFF = 0,
        ON = 1,
        STANDBY = 2,
        PROTECT_STANDBY = 3,
        RAMPING_UP = 4,
        RAMPING_DOWN = 5,
        NOT_OK = 6,
        WARNING = 7,
        TRIPPED = 8,
        ERROR = 9,
    } status[7];
};

/// @brief Structure made to store the GE1/1 front-end low voltages
///
/// @details Is a @c F:5 DIM type
struct lv1_dim_broker {
    float values[5];

    lv1_dim_broker()
    {
        std::fill(std::begin(this->values), std::end(this->values), invalid_dim_v<float>);
    }
};

static_assert(std::is_standard_layout_v<lv1_dim_broker>); // Check C compatibility

/// @brief Contains the mapping between field meanings and the @c lv1_dim_broker array indexes
struct lv1_dim_indexes {
    static constexpr size_t voltage_2p5 = 0;
    static constexpr size_t voltage_1p5 = 1;
    static constexpr size_t voltage_1p2_fpga_mgt = 2;
    static constexpr size_t voltage_1p0_fpga_mgt = 3;
    static constexpr size_t voltage_1p0_fpga_core = 4;
};

/// @brief Structure made to store the low voltage information applied by the DCS
///
/// @details Is a @c F:2;I:1 DIM type
struct lv2_dim_broker {
    float current_mon = 0;
    float voltage_mon = 0;
    float voltage_con = 0;
    enum class lv_status_t : int {
        OFF = 0,
        ON = 1,
        RAMPING_UP = 2,
        RAMPING_DOWN = 4,
        OVERCURRENT = 8,
        OVERVOLAGE = 16,
        UNDERVOLTAGE = 32,
        EXTERNAL_TRIP = 64,
        MAX_VOLTAGE = 128,
        EXTERNAL_DISABLE = 256,
        INTERNAL_TRIP = 512,
        CALIBRATION_ERROR = 1024,
        UNPLUGGED = 2048,
    } status;
};

/// @brief Structure made to store the GE1/1 front-end temperatures
///
/// @details Is a @c F:30 DIM type
struct temp1_dim_broker {
    float values[30];

    temp1_dim_broker()
    {
        std::fill(std::begin(this->values), std::end(this->values), invalid_dim_v<float>);
    }
};

static_assert(std::is_standard_layout_v<temp1_dim_broker>); // Check C compatibility

/// @brief Contains the mapping between fields meaning and the @c temp1_dim_broker array indexes
struct temp1_dim_indexes {
    static constexpr size_t sca_core = 0;
    static constexpr size_t gbt0_pt = 1;
    static constexpr size_t vttx_gem_pt = 2;
    static constexpr size_t vttx_csc_pt = 3;
    static constexpr size_t fpga_pt = 4;
    static constexpr size_t fpga_core = 5;
    static constexpr size_t vfat_pt[24] = { 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 };
};

/// @brief Send a command to the DCS
///
/// @details
///
/// An RPC command will be sent to the DIM service named @c COMMAND1-<wire_name>.
/// The expected return format is @c [OK|NOK]|<message>. In case of failure, an
/// exception is embedded into the returned future. In case of success, the message
/// is set as value in the future.
///
/// @param @c wire_name of the OptoHybrid targeted by the command
/// @param @c command to send to the DCS
/// @param @c timeout for the operation to complete
///
/// @return An @c std::future containing the result of the command.
std::future<std::string> send_dcs_command(const std::string& wire_name, const std::string& command, const int timeout);

/// @brief This class manages a DIM service export to the DCS
template <typename T>
class dcs_service_export {
public:
    /// @brief Constructor
    ///
    /// @param @c wire_name of the OptoHybrid for which the values are published
    dcs_service_export(const std::string& wire_name);

    /// @brief Destructor
    virtual ~dcs_service_export() = default;

    /// @brief No copy policy
    dcs_service_export(const dcs_service_export&) = delete;

    /// @brief No copy policy
    dcs_service_export& operator=(const dcs_service_export&) = delete;

    /// @brief No move policy
    dcs_service_export(dcs_service_export&&) = delete;

    /// @brief No move policy
    dcs_service_export& operator=(dcs_service_export&&) = delete;

    /// @brief Updates the DCS values published in the DIM service
    ///
    /// @param @c value DIM broker structure containing the values to be published
    void update(const T& value);

    /// @brief Gets the values currently published in the DIM service
    const T& get() const;

private:
    T m_dim_value;
    std::unique_ptr<DimService> m_dim_service;
};

/// @brief This class manages the import from a DIM service exposed by the DCS
template <typename T>
class dcs_service_import {

    /// @brief Wrapper class used to decouple @c dcs_service_import from DIM
    class dim_info_wrapper : public DimStampedInfo {
    public:
        /// @brief Constructor
        ///
        /// See the DIM C++ API documentation for more details on the
        /// parameters.
        dim_info_wrapper(char* name, int time, void* nolink, int nolinksize, dcs_service_import* parent);

        /// @brief Callback function used by DIM
        void infoHandler() override;

    private:
        dcs_service_import* m_parent = nullptr;
    };

public:
    /// @brief Constructor
    ///
    /// @param @c wire_name of the OptoHybrid for which the values are imported
    dcs_service_import(const std::string& wire_name);

    /// @brief Destructor
    virtual ~dcs_service_import() = default;

    /// @brief No copy policy
    dcs_service_import(const dcs_service_import&) = delete;

    /// @brief No copy policy
    dcs_service_import& operator=(const dcs_service_import&) = delete;

    /// @brief No move policy
    dcs_service_import(dcs_service_import&&) = delete;

    /// @brief No move policy
    dcs_service_import& operator=(dcs_service_import&&) = delete;

    /// @brief Retrieves the value currently published
    ///
    /// @return An @c std::pair whose first component is the value last update
    ///         timestamp and second component is the values encoded in a DIM
    ///         broker structure
    std::pair<int, T> get() const;

private:
    /// @brief Callback function updating the locally cached value
    void update(int timestamp, const T& value);

    std::unique_ptr<dim_info_wrapper> m_dim_info;

    std::mutex m_mutex;
    int m_timestamp;
    T m_dim_value {};

    // Cannot use static constexpr T because the DIM API non-constness
    T m_dim_value_nolink {}; ///< Default value when the DIM service is down
};

} // namespace gem::core::dim

#endif // GEM_CORE_DIM_DCS_H
