/// @file

#include <gem/core/dim/dcs.h>

#include <gem/core/algorithms.h>

#include <future>
#include <map>
#include <memory>
#include <string>
#include <string_view>
#include <thread>
#include <vector>

namespace /* anonymous */ {

template <typename T>
struct dim_broker_export_properties {
};

template <typename T>
struct dim_broker_import_properties {
};

} // anonymous namespace

std::future<std::string> gem::core::dim::send_dcs_command(const std::string& wire_name, const std::string& command, const int timeout)
{
    std::promise<std::string> promise;
    std::future<std::string> future = promise.get_future();

    std::thread([=](std::promise<std::string> promise) {
        // DIM wants non-const pointers...
        /* const */ std::string t_service_name = "COMMAND1-" + wire_name;
        /* const */ std::string t_command = command;
        /* const */ std::string t_default_reply = "NOK|DIM service error";

        // Perform the RPC call
        DimRpcInfo rpc(t_service_name.data(), timeout, t_default_reply.data());
        rpc.setData(t_command.data());
        const std::string reply = rpc.getString();

        // Parse the reply following the expected format of: <status-code>|<message>
        // where <status-code> is OK in case of success, NOK in case of failure
        try {
            const auto tokens = split(reply, '|');

            if (tokens.size() != 2)
                throw std::runtime_error("unexpected COMMAND1 reply format");

            if ((tokens.at(0) != "OK") && (tokens.at(0) != "NOK"))
                throw std::runtime_error("unknown COMMAND1 reply status");

            if (tokens.at(0) != "OK")
                throw std::runtime_error(tokens.at(1));

            promise.set_value(tokens.at(1));
        } catch (const std::exception& e) {
            promise.set_exception(std::current_exception());
        }
    },
        std::move(promise))
        .detach();

    return std::move(future);
}

template <typename T>
gem::core::dim::dcs_service_export<T>::dcs_service_export(const std::string& wire_name)
{
    /* const */ auto t_service_name = std::string { dim_broker_export_properties<T>::service_name } + wire_name;
    /* const */ std::string t_dim_type { dim_broker_export_properties<T>::dim_type };

    m_dim_service = std::make_unique<DimService>(t_service_name.data(), t_dim_type.data(), &m_dim_value, sizeof(m_dim_value));
};

template <typename T>
void gem::core::dim::dcs_service_export<T>::update(const T& value)
{
    m_dim_value = value;
    m_dim_service->updateService();
}

template <typename T>
const T& gem::core::dim::dcs_service_export<T>::get() const
{
    return m_dim_value;
}

template <typename T>
gem::core::dim::dcs_service_import<T>::dim_info_wrapper::dim_info_wrapper(char* name, const int time, void* nolink, const int nolinksize, gem::core::dim::dcs_service_import<T>* parent)
    : DimStampedInfo(name, time, nolink, nolinksize)
    , m_parent(parent)
{
}

template <typename T>
void gem::core::dim::dcs_service_import<T>::dim_info_wrapper::infoHandler()
{
    m_parent->update(getTimestamp(), *static_cast<T*>(getData()));
}

template <typename T>
gem::core::dim::dcs_service_import<T>::dcs_service_import(const std::string& wire_name)
{
    /* const */ auto t_service_name = std::string { dim_broker_import_properties<T>::service_name } + wire_name;
    const int t_dim_time { dim_broker_import_properties<T>::time };

    m_dim_info = std::make_unique<dim_info_wrapper>(t_service_name.data(), t_dim_time, &m_dim_value_nolink, sizeof(m_dim_value_nolink), this);
};

template <typename T>
std::pair<int, T> gem::core::dim::dcs_service_import<T>::get() const
{
    std::scoped_lock m_lock;
    return { m_timestamp, m_dim_value };
}

template <typename T>
void gem::core::dim::dcs_service_import<T>::update(const int timestamp, const T& value)
{
    std::scoped_lock m_lock;
    m_timestamp = timestamp;
    m_dim_value = value;
}

#define GEM_CORE_DIM_EXPORTABLE(broker_c_type, broker_dim_type, dim_service_prefix)     \
    namespace /* anonymous */ {                                                         \
        template <>                                                                     \
        struct dim_broker_export_properties<broker_c_type> {                            \
            inline constexpr static std::string_view service_name = dim_service_prefix; \
            inline constexpr static std::string_view dim_type = broker_dim_type;        \
        };                                                                              \
    }                                                                                   \
                                                                                        \
    template class gem::core::dim::dcs_service_export<broker_c_type>;

GEM_CORE_DIM_EXPORTABLE(gem::core::dim::lv1_dim_broker, "F:5", "LV1-")
GEM_CORE_DIM_EXPORTABLE(gem::core::dim::temp1_dim_broker, "F:30", "TEMP1-")

#define GEM_CORE_DIM_IMPORTABLE(broker_c_type, dim_service_prefix, dim_time)            \
    namespace /* anonymous */ {                                                         \
        template <>                                                                     \
        struct dim_broker_import_properties<broker_c_type> {                            \
            inline constexpr static std::string_view service_name = dim_service_prefix; \
            inline constexpr static int time = dim_time;                                \
        };                                                                              \
    }                                                                                   \
                                                                                        \
    template class gem::core::dim::dcs_service_import<broker_c_type>;

GEM_CORE_DIM_IMPORTABLE(gem::core::dim::hv2_dim_broker, "HV2-", 30)
GEM_CORE_DIM_IMPORTABLE(gem::core::dim::lv2_dim_broker, "LV2-", 30)
