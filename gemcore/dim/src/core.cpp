/// @file

#include <dim/dic.hxx>
#include <dim/dim_common.h>
#include <dim/dis.hxx>
#include <fmt/format.h>
#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <string>
#include <unistd.h>

namespace /* anonymous */ {

/// @brief Class used to log all the messages from the DIM client
class dim_client : public DimErrorHandler {
public:
    dim_client()
    {
        m_logger = log4cplus::Logger::getInstance("dim-client");
        DimClient::addErrorHandler(this);

        dim_init(); // avoid race condition in multithreaded environment
    }

protected:
    void errorHandler(int severity, int code, char* msg) override
    {
        // We only log the errors
        // The proper handling is done in the calling code
        if (severity == DIM_FATAL || severity == DIM_ERROR)
            LOG4CPLUS_ERROR(m_logger, msg);
        else if (severity == DIM_WARNING)
            LOG4CPLUS_WARN(m_logger, msg);
        else if (severity == DIM_INFO)
            LOG4CPLUS_INFO(m_logger, msg);
    }

    log4cplus::Logger m_logger;
};

static dim_client dim_client; ///< Unique instance of the DIM client manager

/// @brief Class used to start a DIM server and handle the error messages
class dim_server : public DimErrorHandler, public DimExitHandler {
public:
    dim_server()
    {
        const std::string name = fmt::format(FMT_STRING("cmsgemos-{:08x}-{:d}"), static_cast<uint32_t>(gethostid()), getpid());

        m_logger = log4cplus::Logger::getInstance("dim-server");

        DimServer::addErrorHandler(this);
        DimServer::addExitHandler(this);
        DimServer::start(name.data()); // calls dim_init()
    }

protected:
    void errorHandler(int severity, int code, char* msg) override
    {
        // We only log the errors
        // The proper handling is done in the calling code
        if (severity == DIM_FATAL || severity == DIM_ERROR)
            LOG4CPLUS_ERROR(m_logger, msg);
        else if (severity == DIM_WARNING)
            LOG4CPLUS_WARN(m_logger, msg);
        else if (severity == DIM_INFO)
            LOG4CPLUS_INFO(m_logger, msg);
    }

    void exitHandler(int code) override
    {
        // Log the exit request but stay alive, we are not a pure DIM server
        LOG4CPLUS_ERROR(m_logger, "DIM server would exit with error - ignoring...");
    }

    log4cplus::Logger m_logger;
};

static dim_server dim_server; ///< Unique instance of the DIM server

} // anonymous namespace
