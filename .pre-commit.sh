#!/bin/bash

## Any process can use the following directory, cleaned up at exit
tmp_dir=$(mktemp -d)

function cleanup {
    rm -rf ${tmp_dir}
}
trap cleanup EXIT

## Check source files formating with clang-format
staged_file=${tmp_dir}/staged_file
formatted_file=${tmp_dir}/formatted_file

format_patch=${tmp_dir}/cpp_format.patch
format_patch_color=${tmp_dir}/cpp_format_color.patch

for file in $(git diff --cached --diff-filter=ACMR --name-only | grep -E '\.(c|cpp|h|java)$')
do
    git show :${file} > ${staged_file}
    clang-format -style=file -assume-filename=${file} < ${staged_file} > ${formatted_file}

    # Patch to be applied
    git diff -u --no-index -- ${staged_file} ${formatted_file} \
        | sed -e "1,2d" -e "3s|--- .*$|--- a/${file}|" -e "4s|+++ .*$|+++ b/${file}|" \
        >> ${format_patch}

    # Colored patch for user display
    git diff -u --no-index --color=always -- ${staged_file} ${formatted_file} \
        | sed -e "1,2d" -e "3s|--- .*$|--- a/${file}|" -e "4s|+++ .*$|+++ b/${file}|" \
        >> ${format_patch_color}
done

if [[ -s ${format_patch} ]]
then
    echo "Files in this commit do not comply with the clang-format rules. :("

    cat ${format_patch_color}

    exec < /dev/tty
    read -p "Do you want to apply the suggested patch? (Y/n) " -r
    REPLY=${REPLY:-Y}

    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        echo "Not committing the changes, please fix the formatting."
        exit 1
    else
        echo "Applying the changes and continuing."
        git apply --cached ${format_patch}
    fi
fi

## Check for conflict markers and whitespace errors, i.e.
##  * trailing whitespace
##  * space after tab in initial indent
##  * blank lines at the end of a file
git -c core.whitespace= diff --cached --check || exit 1
