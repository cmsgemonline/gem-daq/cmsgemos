/// @file

#include <gem/managers/AMC13Manager.h>

#include <gem/calibration/GEMCalibEnums.h>
#include <gem/managers/AMC13ManagerWeb.h>
#include <gem/managers/exception/Exception.h>
#include <gem/utils/exception/Exception.h>
#include <gem/utils/soap/GEMSOAPToolBox.h>

#include <amc13/AMC13.hh>
#include <uhal/log/exception.hpp>
#include <uhal/log/log.hpp>

XDAQ_INSTANTIATOR_IMPL(gem::managers::AMC13Manager);

void gem::managers::AMC13Manager::BGOInfo::registerFields(xdata::Bag<BGOInfo>* bag)
{
    bag->addField("BGOChannel", &channel);
    bag->addField("BGObx", &bx);
    bag->addField("BGOcmd", &cmd);
    bag->addField("BGOlong", &isLong);
    bag->addField("BGOprescale", &prescale);
    bag->addField("BGOrepeat", &repeat);
}

std::string gem::managers::AMC13Manager::BGOInfo::toString() const
{
    std::stringstream os;
    os << "channel: " << channel.toString() << std::endl
       << "bx: " << bx.toString() << std::endl
       << "cmd: " << cmd.toString() << std::endl
       << "isLong: " << isLong.toString() << std::endl
       << "prescale: " << prescale.toString() << std::endl
       << "repeat: " << repeat.toString() << std::endl;
    return os.str();
};

void gem::managers::AMC13Manager::L1AInfo::registerFields(xdata::Bag<L1AInfo>* bag)
{
    bag->addField("EnableLEMO", &enableLEMO);
    bag->addField("EnableLocalL1A", &enableLocalL1A);
    bag->addField("InternalPeriodicPeriod", &internalPeriodicPeriod);
    bag->addField("L1Aburst", &l1Aburst);
    bag->addField("L1Amode", &l1Amode);
    bag->addField("L1Arules", &l1Arules);
    bag->addField("sendL1ATriburst", &sendl1ATriburst);
    bag->addField("startL1ATricont", &startl1ATricont);
}

std::string gem::managers::AMC13Manager::L1AInfo::toString() const
{
    std::stringstream os;
    os << "enableLEMO: " << enableLEMO.toString() << std::endl
       << "enableLocalL1A: " << enableLocalL1A.toString() << std::endl
       << "internalPeriodicPeriod: " << internalPeriodicPeriod.toString() << std::endl
       << "l1Aburst: " << l1Aburst.toString() << std::endl
       << "l1Amode: " << l1Amode.toString() << std::endl
       << "l1Arules: " << l1Arules.toString() << std::endl
       << "sendl1ATriburst: " << sendl1ATriburst.toString() << std::endl
       << "startl1ATricont: " << startl1ATricont.toString() << std::endl;
    return os.str();
};

void gem::managers::AMC13Manager::TTCInfo::registerFields(xdata::Bag<TTCInfo>* bag)
{
    bag->addField("OC0_CMD", &oc0Command);
    bag->addField("OC0_MASK", &oc0Mask);
    bag->addField("RESYNC_CMD", &resyncCommand);
    bag->addField("RESYNC_MASK", &resyncMask);
}

std::string gem::managers::AMC13Manager::TTCInfo::toString() const
{
    std::stringstream os;
    os << "oc0Command: " << std::hex << oc0Command.value_ << std::endl
       << "oc0Mask: " << std::hex << oc0Mask.value_ << std::endl
       << "resyncCommand: " << std::hex << resyncCommand.value_ << std::endl
       << "resyncMask: " << std::hex << resyncMask.value_ << std::endl;
    return os.str();
}

void gem::managers::AMC13Manager::AMC13Info::registerFields(xdata::Bag<AMC13Info>* bag)
{
    bag->addField("AMC13TTCConfig", &amc13TTCConfig);
    bag->addField("AMCIgnoreTTSList", &amcIgnoreTTSList);
    bag->addField("AMCInputEnableList", &amcInputEnableList);
    bag->addField("BCOffset", &bcOffset);
    bag->addField("BGOConfig", &bgoConfig);
    bag->addField("EnableDAQ", &enableDAQ);
    bag->addField("EnableDAQLink", &enableDAQLink);
    bag->addField("EnableFakeData", &enableFakeData);
    bag->addField("EnableLocalTTC", &enableLocalTTC);
    bag->addField("LocalTriggerConfig", &localTriggerConfig);
    bag->addField("MonitorBackPressure", &monBackPressure);
    bag->addField("PrescaleFactor", &prescaleFactor);
    bag->addField("SFPMask", &sfpMask);
    bag->addField("SkipPLLReset", &skipPLLReset);
    bag->addField("SlotMask", &slotMask);
}

std::string gem::managers::AMC13Manager::AMC13Info::toString() const
{
    std::stringstream os;
    os << "amc13TTCConfig: " << amc13TTCConfig.bag.toString() << std::endl
       << "amcIgnoreTTSList: " << amcIgnoreTTSList.toString() << std::endl
       << "amcInputEnableList: " << amcInputEnableList.toString() << std::endl
       << "bcOffset: " << bcOffset.toString() << std::endl

       << "bgoConfig: " << std::endl;
    for (auto bgo = bgoConfig.begin(); bgo != bgoConfig.end(); ++bgo)
        os << "    " << bgo->bag.toString() << std::endl;

    os << "enabeDAQ: " << enableDAQ.toString() << std::endl
       << "enableDAQLink: " << enableDAQLink.toString() << std::endl
       << "enableFakeData: " << enableFakeData.toString() << std::endl
       << "enableLocalTTC: " << enableLocalTTC.toString() << std::endl
       << "localTriggerConfig: " << localTriggerConfig.bag.toString() << std::endl
       << "monBackPressure: " << monBackPressure.toString() << std::endl
       << "prescaleFactor: " << prescaleFactor.toString() << std::endl
       << "sfpMask: 0x" << std::hex << std::setw(8) << std::setfill('0') << sfpMask.value_ << std::endl
       << "skipPLLReset: " << skipPLLReset.toString() << std::endl
       << "slotMask: 0x" << std::hex << std::setw(8) << std::setfill('0') << slotMask.value_ << std::endl;
    return os.str();
};

gem::managers::AMC13Manager::AMC13Manager(xdaq::ApplicationStub* stub)
    : gem::utils::GEMFSMApplication(stub)
    , m_amc13Lock(toolbox::BSem::FULL, true)
{
    // Configuration parameters
    p_appInfoSpace->fireItemAvailable("fedID", &m_fed_id);
    p_appInfoSpace->fireItemAvailable("amc13ConfigParams", &m_amc13Params);

    m_bgoConfig.setSize(4);

    // Web UI
    editApplicationDescriptor()->setAttribute("icon", "/cmsgemos/gemmanagers/html/images/amc13/AMC13Manager.png");

    p_gemWebInterface = new gem::managers::AMC13ManagerWeb(this);

    // SOAP API
    xoap::deferredbind(this, this, &gem::managers::AMC13Manager::sendTriggerBurst, "sendtriggerburst", XDAQ_NS_URI);
    xoap::deferredbind(this, this, &gem::managers::AMC13Manager::enableTriggers, "enableTriggers", XDAQ_NS_URI);
    xoap::deferredbind(this, this, &gem::managers::AMC13Manager::disableTriggers, "disableTriggers", XDAQ_NS_URI);

    uhal::setLogLevelTo(uhal::Error);
}

gem::managers::AMC13Manager::~AMC13Manager()
{
    CMSGEMOS_DEBUG("AMC13Manager::dtor called");
}

void gem::managers::AMC13Manager::actionPerformed(xdata::Event& event)
{
    gem::utils::GEMApplication::actionPerformed(event);

    if (event.type() == "urn:xdaq-event:setDefaultValues") {
        // Update configuration variables
        m_amcInputEnableList = m_amc13Params.bag.amcInputEnableList.value_;
        m_amcIgnoreTTSList = m_amc13Params.bag.amcIgnoreTTSList.value_;
        m_enableDAQ = m_amc13Params.bag.enableDAQ.value_;
        m_enableDAQLink = m_amc13Params.bag.enableDAQLink.value_;
        m_enableFakeData = m_amc13Params.bag.enableFakeData.value_;
        m_monBackPressEnable = m_amc13Params.bag.monBackPressure.value_;
        m_enableLocalTTC = m_amc13Params.bag.enableLocalTTC.value_;
        m_skipPLLReset = m_amc13Params.bag.skipPLLReset.value_;
        m_amc13TTCConfig = m_amc13Params.bag.amc13TTCConfig;

        m_localTriggerConfig = m_amc13Params.bag.localTriggerConfig;
        m_enableLocalL1A = m_localTriggerConfig.bag.enableLocalL1A.value_;
        m_internalPeriodicPeriod = m_localTriggerConfig.bag.internalPeriodicPeriod.value_;
        m_L1Amode = m_localTriggerConfig.bag.l1Amode.value_;
        m_L1Arules = m_localTriggerConfig.bag.l1Arules.value_;
        m_L1Aburst = m_localTriggerConfig.bag.l1Aburst.value_;
        m_sendL1ATriburst = m_localTriggerConfig.bag.sendl1ATriburst.value_;
        m_startL1ATricont = m_localTriggerConfig.bag.startl1ATricont.value_;
        m_enableLEMO = m_localTriggerConfig.bag.enableLEMO.value_;

        for (auto bconf = m_amc13Params.bag.bgoConfig.begin(); bconf != m_amc13Params.bag.bgoConfig.end(); ++bconf)
            if (bconf->bag.channel > -1)
                m_bgoConfig.at(bconf->bag.channel) = *bconf;

        if (m_bgoConfig.size() > 0) {
            m_bgoChannel = 0;
            m_bgoCMD = m_bgoConfig.at(0).bag.cmd.value_;
            m_bgoBX = m_bgoConfig.at(0).bag.bx.value_;
            m_bgoPrescale = m_bgoConfig.at(0).bag.prescale.value_;
            m_bgoRepeat = m_bgoConfig.at(0).bag.repeat.value_;
            m_bgoIsLong = m_bgoConfig.at(0).bag.isLong.value_;
        }

        m_prescaleFactor = m_amc13Params.bag.prescaleFactor.value_;
        m_bcOffset = m_amc13Params.bag.bcOffset.value_;
        m_fedID = m_fed_id.value_;
        m_sfpMask = m_amc13Params.bag.sfpMask.value_;
        m_slotMask = m_amc13Params.bag.slotMask.value_;
    }
}

// state transitions
void gem::managers::AMC13Manager::initializeAction()
{
    CMSGEMOS_INFO("AMC13Manager::initializeAction m_amc13Params is:" << std::endl
                                                                     << m_amc13Params.bag.toString());

    try {
        gem::utils::LockGuard<gem::utils::Lock> guardedLock(m_amc13Lock);
        const auto layout_tree_address = core::layout_tree::address<core::layout_tree::amc13_node> { m_fed_id.value_ };
        const auto t1_uri = (layout_tree() % layout_tree_address).t1_uri();
        const auto t2_uri = (layout_tree() % layout_tree_address).t2_uri();
        p_amc13 = std::make_shared<::amc13::AMC13>(t1_uri, "${AMC13_ADDRESS_TABLE_PATH}/AMC13XG_T1.xml", t2_uri, "${AMC13_ADDRESS_TABLE_PATH}/AMC13XG_T2.xml");
    } catch (::amc13::Exception::exBase const& e) {
        std::stringstream msg;
        msg << "AMC13Manager::AMC13::AMC13() failed, caught amc13::Exception: " << e.what();
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::managers::exception::HardwareProblem, msg.str());
    } catch (uhal::exception::exception const& e) {
        std::stringstream msg;
        msg << "AMC13Manager::AMC13::AMC13() failed, caught uhal::exception: " << e.what();
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::managers::exception::HardwareProblem, msg.str());
    } catch (std::exception const& e) {
        std::stringstream msg;
        msg << "AMC13Manager::AMC13::AMC13() failed, caught std::exception: " << e.what();
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::managers::exception::HardwareProblem, msg.str());
    } catch (...) {
        std::stringstream msg;
        msg << "AMC13Manager::AMC13::AMC13() failed (unknown exception)";
        CMSGEMOS_ERROR(msg.str());
        XCEPT_RAISE(gem::managers::exception::HardwareProblem, msg.str());
    }

    CMSGEMOS_DEBUG("AMC13Manager::initializeAction finished with AMC13::AMC13()");

    try {
        gem::utils::LockGuard<gem::utils::Lock> guardedLock(m_amc13Lock);
        p_amc13->reset(::amc13::AMC13::T2);

        // enable specified AMCs
        m_slotMask = p_amc13->parseInputEnableList(m_amcInputEnableList, true);
        p_amc13->AMCInputEnable(m_slotMask);

        // Re-map AMC13 Resync and OCR to match TCDS and CTP7 expectation
        // NOT FINAL FIXME
        p_amc13->write(::amc13::AMC13::T1, "CONF.TTC.RESYNC.COMMAND", m_amc13TTCConfig.bag.resyncCommand.value_);
        p_amc13->write(::amc13::AMC13::T1, "CONF.TTC.RESYNC.MASK", m_amc13TTCConfig.bag.resyncMask.value_);
        p_amc13->write(::amc13::AMC13::T1, "CONF.TTC.OCR_COMMAND", m_amc13TTCConfig.bag.oc0Command.value_);
        p_amc13->write(::amc13::AMC13::T1, "CONF.TTC.OCR_MASK", m_amc13TTCConfig.bag.oc0Mask.value_);

        p_amc13->enableAllTTC();
    } catch (uhal::exception::exception const& e) {
        CMSGEMOS_ERROR("AMC13Manager::AMC13::AMC13() failed, caught uhal::exception " << e.what());
        XCEPT_RAISE(gem::managers::exception::HardwareProblem, std::string("Problem during preinit : ") + e.what());
    } catch (std::exception const& e) {
        CMSGEMOS_ERROR("AMC13Manager::AMC13::AMC13() failed, caught std::exception " << e.what());
        XCEPT_RAISE(gem::managers::exception::HardwareProblem, std::string("Problem during preinit : ") + e.what());
    }

    if (!p_amc13)
        return;

    // have to set up the initialization of the AMC13 for the desired running situation
    // possibilities are TTC/TCDS mode, DAQ link, local trigger scheme
    // lock the access
    gem::utils::LockGuard<gem::utils::Lock> guardedLock(m_amc13Lock);

    // enable daq link (if SFP mask is non-zero
    CMSGEMOS_DEBUG("AMC13Manager::initializeAction Enabling DAQLink with settings: fake data:" << m_enableFakeData
                                                                                               << ", sfpMask:" << m_sfpMask);

    p_amc13->fakeDataEnable(m_enableFakeData);
    p_amc13->daqLinkEnable(m_enableDAQLink);
    p_amc13->daqIgnore(!m_enableDAQ);

    // enable SFP outputs based on mask configuration
    p_amc13->sfpOutputEnable(m_sfpMask);

    // ignore AMC tts state per mask
    CMSGEMOS_INFO("AMC13Manager::initializeAction m_amcIgnoreTTSList " << m_amcIgnoreTTSList);
    m_ignoreAMCTTS = p_amc13->parseInputEnableList(m_amcIgnoreTTSList, true);
    CMSGEMOS_INFO("AMC13Manager::initializeAction m_amcIgnoreTTSList " << m_amcIgnoreTTSList
                                                                       << " parsed as m_ignoreAMCTTS: " << std::hex << m_ignoreAMCTTS << std::dec);
    if (m_ignoreAMCTTS) {
        p_amc13->ttsDisableMask(m_ignoreAMCTTS);
    } else {
        p_amc13->ttsDisableMask(0x0);
    }

    // Use local TTC signal if config says to
    p_amc13->localTtcSignalEnable(m_enableLocalTTC);

    // Use local trigger generator if config says to
    p_amc13->configureLocalL1A(m_enableLocalL1A, m_L1Amode, m_L1Aburst, m_internalPeriodicPeriod, m_L1Arules);
    // probably shouldn't enable until we're running? CHECKME
    // p_amc13->enableLocalL1A(m_enableLocalL1A);
    p_amc13->write(::amc13::AMC13::T1, "CONF.TTC.T3_TRIG", 0);

    // need to ensure that all BGO channels are disabled
    for (int bchan = 0; bchan < 4; ++bchan)
        p_amc13->disableBGO(bchan);

    // Enable Monitor Buffer Backpressure if config doc says so
    p_amc13->monBufBackPressEnable(m_monBackPressEnable);

    // m_dtc->configurePrescale(1,m_preScaleFactNumOfZeros);
    p_amc13->configurePrescale(0, m_prescaleFactor);

    // set the FED id
    p_amc13->setFEDid(m_fedID);

    // reset the PLL on the T1
    if (!m_skipPLLReset)
        p_amc13->write(::amc13::AMC13::T1, 0x0, 0x8);
    usleep(200);

    // reset the T1
    p_amc13->reset(::amc13::AMC13::T1);

    // reset the T1 counters
    p_amc13->resetCounters();

    // unlock the access
    CMSGEMOS_INFO("AMC13Manager::initializeAction end");
}

void gem::managers::AMC13Manager::configureAction()
{
    // Set the offset between the AMC13 BX counter and AMC BX counter
    // Starts at 0 on the AMC13, and at 1 on the AMC
    p_amc13->write(::amc13::AMC13::T1, "CONF.BCN_OFFSET", 1);

    /* REDUNDANT?
  if (m_enableLocalL1A) {
    m_L1Aburst = m_localTriggerConfig.bag.l1Aburst.value_;
    p_amc13->configureLocalL1A(m_enableLocalL1A, m_L1Amode, m_L1Aburst, m_internalPeriodicPeriod, m_L1Arules);
  } else {
    p_amc13->configureLocalL1A(m_enableLocalL1A, m_L1Amode, m_L1Aburst, m_internalPeriodicPeriod, m_L1Arules);
  }
  */
    // if ((m_scanInfo.bag.scanType.value_ == 2 || m_scanInfo.bag.scanType.value_ == 3) && m_scanInfo.bag.trigType.value_< 1) {
    // m_enableLocalTTC = true;
    //} // TODO: fix the use of local TTC and its configuration : atm done in the xml; should it be passed trough the calibration suite?
    m_L1Aburst = m_localTriggerConfig.bag.l1Aburst.value_;
    p_amc13->configureLocalL1A(m_enableLocalL1A, m_L1Amode, m_L1Aburst, m_internalPeriodicPeriod, m_L1Arules);

    if (m_enableLocalTTC) {
        CMSGEMOS_DEBUG("AMC13Manager::configureAction configuring BGO channels "
            << m_bgoConfig.size());
        for (auto bchan = m_bgoConfig.begin(); bchan != m_bgoConfig.end(); ++bchan) {
            CMSGEMOS_DEBUG("AMC13Manager::configureAction channel "
                << bchan->bag.channel.value_);
            if (bchan->bag.channel.value_ > -1) {
                if (bchan->bag.isLong.value_)
                    p_amc13->configureBGOLong(bchan->bag.channel.value_, bchan->bag.cmd.value_, bchan->bag.bx.value_,
                        bchan->bag.prescale.value_, bchan->bag.repeat.value_);
                else
                    p_amc13->configureBGOShort(bchan->bag.channel.value_, bchan->bag.cmd.value_, bchan->bag.bx.value_,
                        bchan->bag.prescale.value_, bchan->bag.repeat.value_);

                p_amc13->getBGOConfig(bchan->bag.channel.value_);
            }
        }
    }

    if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::LATENCY) || m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::SCURVE)) {
        m_scanParameter = m_scanInfo.bag.scanMin.value_;
    }
    // set the settings from the config options
    // usleep(10); // just for testing the timing of different applications
    CMSGEMOS_INFO("AMC13Manager::configureAction end");
}

void gem::managers::AMC13Manager::startAction()
{
    CMSGEMOS_DEBUG("AMC13Manager::Entering AMC13Manager::startAction()");
    // gem::utils::GEMFSMApplication::enable();
    gem::utils::LockGuard<gem::utils::Lock> guardedLock(m_amc13Lock);

    // reset the T1?
    p_amc13->reset(::amc13::AMC13::T1);

    // reset the PLL on the T1
    // if (!m_skipPLLReset)
    //   p_amc13->write(::amc13::AMC13::T1, 0x0, 0x1);
    usleep(10);

    p_amc13->resetCounters();
    m_updatedL1ACount = p_amc13->read(::amc13::AMC13::T1, "STATUS.GENERAL.L1A_COUNT_LO");

    p_amc13->startRun();

    if (m_enableLocalTTC) {
        for (auto bchan = m_bgoConfig.begin(); bchan != m_bgoConfig.end(); ++bchan)
            if (bchan->bag.channel.value_ > -1) {
                CMSGEMOS_INFO("AMC13Manager::startAction enabling BGO channel " << bchan->bag.channel.value_);
                if (bchan->bag.repeat.value_)
                    p_amc13->enableBGORepeat(bchan->bag.channel.value_);
                else
                    p_amc13->enableBGOSingle(bchan->bag.channel.value_);
            }
        p_amc13->sendBGO();
    }

    if (m_enableLocalL1A) {
        if (m_enableLEMO) {
            CMSGEMOS_DEBUG("AMC13Manager::startAction enabling LEMO trigger " << m_enableLEMO);
            p_amc13->write(::amc13::AMC13::T1, "CONF.TTC.T3_TRIG", 0x1);
        } else {
            p_amc13->startContinuousL1A();
        }
    } else {
        /*
    // HACK
    // when using TTC triggers, they should be enabled upstream of the AMC13 with start
    p_amc13->configureLocalL1A(m_enableLocalL1A, m_L1Amode, m_L1Aburst, m_internalPeriodicPeriod, m_L1Arules);
    */
    }

    CMSGEMOS_INFO("AMC13Manager::startAction end");
}

void gem::managers::AMC13Manager::pauseAction()
{
    // what does pause mean here?
    // if local triggers are enabled, do we have a separate trigger application?
    // we can just disable them here maybe?

    if (m_enableLocalL1A) {
        if (m_enableLEMO)
            p_amc13->write(::amc13::AMC13::T1, "CONF.TTC.T3_TRIG", 0);
        else
            // what if using both local triggers and LEMO triggers?
            p_amc13->stopContinuousL1A();
    } else {
        /*
    // HACK
    // when using external triggers, they should be stopped upstream of the AMC13 with a pause
    p_amc13->configureLocalL1A(true, m_L1Amode, m_L1Aburst, m_internalPeriodicPeriod, m_L1Arules);
    p_amc13->enableLocalL1A(true);  // is this fine to switch to local L1A as a way to fake turning off upstream?
    */
    }

    if (m_enableLocalTTC)
        for (auto bchan = m_bgoConfig.begin(); bchan != m_bgoConfig.end(); ++bchan)
            if (bchan->bag.channel.value_ > -1) {
                CMSGEMOS_DEBUG("AMC13Manager::pauseAction disabling BGO channel " << bchan->bag.channel.value_);
                p_amc13->disableBGO(bchan->bag.channel.value_);
            }

    // need to ensure that all BGO channels are disabled, rather than just the ones in the config
    for (int bchan = 0; bchan < 4; ++bchan)
        p_amc13->disableBGO(bchan);

    CMSGEMOS_INFO("AMC13Manager::pauseAction end");
}

void gem::managers::AMC13Manager::resumeAction()
{
    // undo the actions taken in pauseAction
    if (m_enableLocalTTC) {
        bool sendLocalBGO = false;
        for (auto bchan = m_bgoConfig.begin(); bchan != m_bgoConfig.end(); ++bchan)
            if (bchan->bag.channel.value_ > -1) {
                sendLocalBGO = true;
                CMSGEMOS_DEBUG("AMC13Manager::resumeAction enabling BGO channel " << bchan->bag.channel.value_);
                if (bchan->bag.repeat.value_)
                    p_amc13->enableBGORepeat(bchan->bag.channel.value_);
                else
                    p_amc13->enableBGOSingle(bchan->bag.channel.value_);
            }

        if (sendLocalBGO)
            p_amc13->sendBGO();
    }

    if (m_enableLocalL1A) {
        p_amc13->configureLocalL1A(m_enableLocalL1A, m_L1Amode, m_L1Aburst, m_internalPeriodicPeriod, m_L1Arules);
        p_amc13->enableLocalL1A(m_enableLocalL1A);

        if (m_enableLEMO)
            p_amc13->write(::amc13::AMC13::T1, "CONF.TTC.T3_TRIG", 1);
        else
            p_amc13->startContinuousL1A(); // only if we want to send triggers continuously
    } else {
        /*
    // HACK
    // when using external triggers, they should be enabled upstream of the AMC13 with a resume
    p_amc13->configureLocalL1A(m_enableLocalL1A, m_L1Amode, m_L1Aburst, m_internalPeriodicPeriod, m_L1Arules);
    */
    }

    CMSGEMOS_INFO("AMC13Manager::resumeAction end");
}

void gem::managers::AMC13Manager::stopAction()
{
    CMSGEMOS_DEBUG("AMC13Manager::Entering AMC13Manager::stopAction()");
    // gem::utils::GEMFSMApplication::disable();
    gem::utils::LockGuard<gem::utils::Lock> guardedLock(m_amc13Lock);

    if (m_enableLocalL1A) {
        // p_amc13->enableLocalL1A(false);

        if (m_enableLEMO)
            p_amc13->write(::amc13::AMC13::T1, "CONF.TTC.T3_TRIG", 0);
        else
            p_amc13->stopContinuousL1A();
    } else {
        /*
    // HACK
    // when using external triggers, they should be stopped upstream of the AMC13 with a stop
    p_amc13->configureLocalL1A(m_enableLocalL1A, m_L1Amode, m_L1Aburst, m_internalPeriodicPeriod, m_L1Arules);
    p_amc13->enableLocalL1A(true);
    */
    }

    if (m_enableLocalTTC)
        for (auto bchan = m_bgoConfig.begin(); bchan != m_bgoConfig.end(); ++bchan)
            if (bchan->bag.channel.value_ > -1) {
                CMSGEMOS_DEBUG("AMC13Manager::stopAction disabling BGO channel " << bchan->bag.channel.value_);
                p_amc13->disableBGO(bchan->bag.channel.value_);
            }

    // need to ensure that all BGO channels are disabled, rather than just the ones in the config
    for (int bchan = 0; bchan < 4; ++bchan)
        p_amc13->disableBGO(bchan);

    // usleep(10);
    p_amc13->endRun();
    CMSGEMOS_INFO("AMC13Manager::stopAction end");
}

void gem::managers::AMC13Manager::haltAction()
{
    // what is necessary for a halt on the AMC13?
    usleep(10); // just for testing the timing of different applications
    CMSGEMOS_INFO("AMC13Manager::haltAction end");
}

xoap::MessageReference gem::managers::AMC13Manager::sendTriggerBurst(xoap::MessageReference msg)
{
    // set to send a burst of trigger
    CMSGEMOS_INFO("Entering AMC13Manager::sendTriggerBurst()");

    if (msg.isNull()) {
        XCEPT_RAISE(xoap::exception::Exception, "Null message received!");
    }

    std::string commandName = "sendTriggerBurst";

    try {
        if (m_enableLocalL1A && m_sendL1ATriburst) { // CLEANME need to remove
            // p_amc13->localTtcgSignalEnable(m_enableLocalL1A);
            // p_amc13->enableLocalL1A(m_enableLocalL1A);
            p_amc13->sendL1ABurst();
        }
    } catch (xoap::exception::Exception const& err) {
        std::string msgBase = toolbox::toString("Unable to extract command from SOAP message");
        std::string faultString = toolbox::toString("%s failed", commandName.c_str());
        std::string faultCode = "Client";
        std::string detail = toolbox::toString("%s: %s.",
            msgBase.c_str(),
            err.message().c_str());
        std::string faultActor = this->getFullURL();
        xoap::MessageReference reply = gem::utils::soap::makeSOAPFaultReply(faultString, faultCode, detail, faultActor);
        return reply;
    }
    try {
        CMSGEMOS_INFO("AMC13Manager::sendTriggerBurst command " << commandName << " succeeded ");
        return gem::utils::soap::makeSOAPReply(commandName, "SentTriggers");
    } catch (xcept::Exception& err) {
        std::string msgBase = toolbox::toString("Failed to create SOAP reply for command '%s'",
            commandName.c_str());
        CMSGEMOS_ERROR(toolbox::toString("%s: %s.", msgBase.c_str(), xcept::stdformat_exception_history(err).c_str()));
        XCEPT_DECLARE_NESTED(gem::utils::exception::SoftwareProblem,
            top, toolbox::toString("%s.", msgBase.c_str()), err);
        this->notifyQualified("error", top);

        XCEPT_RETHROW(xoap::exception::Exception, msgBase, err);
    }
    XCEPT_RAISE(xoap::exception::Exception, "command not found");
}

xoap::MessageReference gem::managers::AMC13Manager::enableTriggers(xoap::MessageReference msg)
{
    CMSGEMOS_DEBUG("AMC13Manager::enableTriggers");
    // gem::utils::GEMFSMApplication::disable();

    std::string commandName = "enableTriggers";

    if (!p_amc13) {
        std::string msgBase = toolbox::toString("Failed to create SOAP reply for command '%s', AMC13 not yet connected",
            commandName.c_str());
        CMSGEMOS_ERROR(toolbox::toString("%s", msgBase.c_str()));
        return gem::utils::soap::makeSOAPReply(commandName, "Failed");
    }

    if (!m_startL1ATricont) { // CLEANME need to remove
        p_amc13->enableLocalL1A(m_enableLocalL1A);
        if (m_enableLEMO)
            p_amc13->write(::amc13::AMC13::T1, "CONF.TTC.T3_TRIG", 1);
    }

    if (m_enableLocalL1A && m_startL1ATricont) { // CLEANME need to remove
        p_amc13->startContinuousL1A();
    }

    try {
        CMSGEMOS_INFO("AMC13Manager::enableTriggers command " << commandName << " succeeded ");
        return gem::utils::soap::makeSOAPReply(commandName, "SentTriggers");
    } catch (xcept::Exception& e) {
        const std::string errmsg = "Failed to create SOAP reply for command '" + commandName + "'";
        CMSGEMOS_ERROR(errmsg << ": " << xcept::stdformat_exception_history(e));
        XCEPT_DECLARE_NESTED(gem::utils::exception::SoftwareProblem, top, errmsg, e);
        this->notifyQualified("error", top);

        XCEPT_RETHROW(xoap::exception::Exception, errmsg, e);
    }
    XCEPT_RAISE(xoap::exception::Exception, "command not found");
}

xoap::MessageReference gem::managers::AMC13Manager::disableTriggers(xoap::MessageReference msg)
{
    CMSGEMOS_DEBUG("AMC13Manager::disableTriggers");
    // gem::utils::GEMFSMApplication::disable();
    std::string commandName = "disableTriggers";

    if (!p_amc13) {
        std::string msgBase = toolbox::toString("Failed to create SOAP reply for command '%s', AMC13 not yet connected",
            commandName.c_str());
        CMSGEMOS_ERROR(toolbox::toString("%s", msgBase.c_str()));
        return gem::utils::soap::makeSOAPReply(commandName, "Failed");
    }

    if (!m_startL1ATricont) { // CLEANME need to remove
        p_amc13->enableLocalL1A(!m_enableLocalL1A);
        if (m_enableLEMO)
            p_amc13->write(::amc13::AMC13::T1, "CONF.TTC.T3_TRIG", 0);
    }

    if (m_enableLocalL1A && m_startL1ATricont) { // CLEANME need to remove
        p_amc13->stopContinuousL1A();
    }

    try {
        CMSGEMOS_INFO("AMC13Manager::disableTriggers " << commandName << " succeeded ");
        return gem::utils::soap::makeSOAPReply(commandName, "SentTriggers");
    } catch (xcept::Exception& e) {
        const std::string errmsg = "Failed to create SOAP reply for command '" + commandName + "'";
        CMSGEMOS_ERROR(errmsg << ": " << xcept::stdformat_exception_history(e));
        XCEPT_DECLARE_NESTED(gem::utils::exception::SoftwareProblem, top, errmsg, e);
        this->notifyQualified("error", top);

        XCEPT_RETHROW(xoap::exception::Exception, errmsg, e);
    }
    XCEPT_RAISE(xoap::exception::Exception, "command not found");
}
