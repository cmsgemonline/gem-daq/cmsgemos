/// @file

#include <gem/managers/BoardManagerWeb.h>

#include <gem/managers/BoardManager.h>

gem::managers::BoardManagerWeb::BoardManagerWeb(gem::managers::BoardManager* application)
    : gem::utils::GEMWebApplication(application)
{
    add_tab("Control Panel", &BoardManagerWeb::controlPanel);
}

gem::managers::BoardManagerWeb::~BoardManagerWeb()
{
}
