/// @file

#ifndef DOXYGEN_IGNORE_THIS

#include "config/PackageInfo.h"

namespace gem {
namespace managers {

    const std::string project = "cmsgemos";
    const std::string package = "gem::managers";
    const std::string versions = PACKAGE_VERSION_STRING(0, 0, 0);
    const std::string summary = "";
    const std::string description = "";
    const std::string authors = "";
    const std::string link = "";

    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() {}
    std::set<std::string> getPackageDependencies() { return {}; }

}
}

GETPACKAGEINFO(gem::managers);

#endif // DOXYGEN_IGNORE_THIS
