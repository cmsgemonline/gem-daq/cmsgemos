/// @file

#include <gem/managers/BoardManager.h>

#include <gem/hardware/board.h>
#include <gem/hardware/utils.h>
#include <gem/managers/BoardManagerWeb.h>
#include <xhal/client/XHALInterface.h>

XDAQ_INSTANTIATOR_IMPL(gem::managers::BoardManager);

gem::managers::BoardManager::BoardManager(xdaq::ApplicationStub* stub)
    : gem::utils::GEMFSMApplication(stub)
{
    // Hardware description
    p_appInfoSpace->fireItemAvailable("connectionURL", &m_connection_url);

    // Web UI
    p_gemWebInterface = new gem::managers::BoardManagerWeb(this);
}

gem::managers::BoardManager::~BoardManager()
{
}

// State transitions
void gem::managers::BoardManager::initializeAction()
{
    CMSGEMOS_INFO("BoardManager::initializeAction begin");

    CMSGEMOS_INFO("BoardManager::initializeAction end");
}

void gem::managers::BoardManager::configureAction()
{
    CMSGEMOS_INFO("BoardManager::configureAction begin");

    CMSGEMOS_INFO("BoardManager::configureAction: connect to the backend board");
    m_connection = std::make_shared<xhal::client::XHALInterface>(m_connection_url.value_, m_gemLogger);

    CMSGEMOS_INFO("AMCManager::configureAction: recover the backend board");
    m_connection->call<gem::hardware::board::recover>();
    m_connection->call<gem::hardware::board::reset>();

    CMSGEMOS_INFO("AMCManager::configureAction: configure the backend board");
    m_connection->call<gem::hardware::board::configure>();

    CMSGEMOS_INFO("BoardManager::configureAction end");
}

void gem::managers::BoardManager::startAction()
{
    CMSGEMOS_INFO("BoardManager::startAction begin");

    // Reset the TTC counters for the multiple AMC run case
    m_connection->call<gem::hardware::utils::writeRemoteReg>("BEFE.SYSTEM.CTRL.TTC_RESET", 1);

    m_connection->call<gem::hardware::utils::writeRemoteReg>("BEFE.SYSTEM.CTRL.EXT_TRIG_ENABLE", 1);

    CMSGEMOS_INFO("BoardManager::startAction end");
}

void gem::managers::BoardManager::pauseAction()
{
    CMSGEMOS_INFO("BoardManager::pauseAction begin");

    m_connection->call<gem::hardware::utils::writeRemoteReg>("BEFE.SYSTEM.CTRL.EXT_TRIG_ENABLE", 0);

    CMSGEMOS_INFO("BoardManager::pauseAction end");
}

void gem::managers::BoardManager::resumeAction()
{
    CMSGEMOS_INFO("BoardManager::resumeAction begin");

    m_connection->call<gem::hardware::utils::writeRemoteReg>("BEFE.SYSTEM.CTRL.EXT_TRIG_ENABLE", 1);

    CMSGEMOS_INFO("BoardManager::resumeAction end");
}

void gem::managers::BoardManager::stopAction()
{
    CMSGEMOS_INFO("BoardManager::stopAction begin");

    m_connection->call<gem::hardware::utils::writeRemoteReg>("BEFE.SYSTEM.CTRL.EXT_TRIG_ENABLE", 0);

    CMSGEMOS_INFO("BoardManager::stopAction end");
}

void gem::managers::BoardManager::haltAction()
{
    CMSGEMOS_INFO("BoardManager::haltAction begin");

    m_connection.reset();

    CMSGEMOS_INFO("BoardManager::haltAction end");
}
