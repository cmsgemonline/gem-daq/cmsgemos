/// @file

#include <gem/managers/AMC13ManagerWeb.h>

#include <gem/managers/AMC13Manager.h>
#include <gem/managers/exception/Exception.h>

#include <cgicc/HTMLClasses.h>
#include <xcept/tools.h>

#include <amc13/AMC13.hh>
#include <amc13/Status.hh>

// Additional cgicc HTML elements
namespace cgicc {
BOOLEAN_ELEMENT(section, "section");
}

gem::managers::AMC13ManagerWeb::AMC13ManagerWeb(gem::managers::AMC13Manager* application)
    : gem::utils::GEMWebApplication(application)
{
    add_tab("Control Panel", &AMC13ManagerWeb::controlPanel);
}

gem::managers::AMC13ManagerWeb::~AMC13ManagerWeb()
{
}
