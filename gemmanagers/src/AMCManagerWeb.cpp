/// @file

#include <gem/managers/AMCManagerWeb.h>

#include <gem/managers/AMCManager.h>

#include <xcept/tools.h>

#include <memory>

gem::managers::AMCManagerWeb::AMCManagerWeb(gem::managers::AMCManager* application)
    : gem::utils::GEMWebApplication(application)
{
    add_tab("Control Panel", &AMCManagerWeb::controlPanel);
    add_tab("Expert Page", &AMCManagerWeb::expertPage);
}

gem::managers::AMCManagerWeb::~AMCManagerWeb()
{
}

void gem::managers::AMCManagerWeb::webDefault(xgi::Input* in, xgi::Output* out)
{
    *out << "<script src=\"/cmsgemos/gemmanagers/html/scripts/amc/amc.js\"></script>" << std::endl;
    *out << "<link href=\"/cmsgemos/gemmanagers/html/css/optohybrid/optohybrid.css\" rel=\"stylesheet\">" << std::endl;
    *out << "<script src=\"/cmsgemos/gemmanagers/html/scripts/optohybrid/optohybrid.js\"></script>" << std::endl;

    GEMWebApplication::webDefault(in, out);
}

void gem::managers::AMCManagerWeb::expertPage(xgi::Input* in, xgi::Output* out)
{
    *out << R"--(
    <script type="text/javascript">
        function configureOptoHybridFPGA()
        {
            $.post(window.location.pathname + "/configureOptoHybridFPGA", $("#configureOptoHybridFPGA").serialize())
                .done(function(data) {
                    alert("Done!\n" + data["message"]);
                })
                .fail(function(xhr) {
                    alert("Operation failed!\n" + xhr.responseJSON["message"]);
                });
        }
    </script>
    <form id=configureOptoHybridFPGA style="display: inline;">
        <label for="oh-mask">OptoHybrid mask</label>
        <input id="oh-mask" name="oh-mask" type="text">
        <button type="button" onclick="configureOptoHybridFPGA()">Configure OptoHybrid FPGA</button>
    </form>
    )--" << std::endl;
}
