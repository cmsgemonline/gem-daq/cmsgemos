/// @file

#include <gem/managers/AMCManager.h>

#include <gem/calibration/GEMCalibEnums.h>
#include <gem/core/algorithms.h>
#include <gem/core/dim/dcs.h>
#include <gem/core/layout-tree/bitmask.h>
#include <gem/hardware/amc.h>
#include <gem/hardware/amc/daq.h>
#include <gem/hardware/amc/trigger.h>
#include <gem/hardware/amc/ttc.h>
#include <gem/hardware/blaster.h>
#include <gem/hardware/calibration.h>
#include <gem/hardware/expert.h>
#include <gem/hardware/gbt.h>
#include <gem/hardware/optohybrid.h>
#include <gem/hardware/utils.h>
#include <gem/hardware/vfat3.h>
#include <gem/managers/AMCManagerWeb.h>
#include <gem/managers/exception/Exception.h>

#include <xhal/client/XHALInterface.h>

#include <nlohmann/json.hpp>
#include <toolbox/Runtime.h>
#include <toolbox/task/TimerFactory.h>
#include <xgi/Utils.h>

#include <fmt/format.h>

#include <filesystem>
#include <iterator>
#include <string>
#include <thread>

namespace ltree = gem::core::layout_tree;

namespace /* anonymous */ {
constexpr size_t scan_point_update_attempts = 10;

constexpr std::string_view clusterMask_rate_scan_header = "fed;slot;oh;delay;cluster-rate-unmasked;cluster-rate-masked;trigger-rate;l1a-period";
constexpr auto clusterMask_rate_scan_format = FMT_STRING("{:d};{:d};{:d};{:d};{:d};{:d};{:d};{:d}");

constexpr std::string_view sbit_rate_scan_header = "iteration;fed;slot;oh;vfat;dac-value;rate";
constexpr auto sbit_rate_scan_format = FMT_STRING("{:d};{:d};{:d};{:d};{:d};{:d};{:d}");

constexpr std::string_view dac_scan_header = "dac-name;fed;slot;oh;vfat;dac-value;adc-value;adc-std";
constexpr auto dac_scan_format = FMT_STRING("{:s};{:d};{:d};{:d};{:d};{:d};{:f};{:f}");

constexpr std::string_view gbt_phase_scan_header = "fed;slot;oh;vfat;phase;errors";
constexpr auto gbt_phase_scan_format = FMT_STRING("{:d};{:d};{:d};{:d};{:d};{:d}");

constexpr std::string_view gbt_phase_result_header = "fed;slot;oh;vfat;phase";
constexpr auto gbt_phase_result_format = FMT_STRING("{:d};{:d};{:d};{:d};{:d}");

constexpr std::string_view calpulse_scan_header = "fed;slot;oh;vfat;channel;hits;triggers";
constexpr auto calpulse_scan_format = FMT_STRING("{:d};{:d};{:d};{:d};{:d};{:d};{:d}");

constexpr std::string_view eom_scan_header = "fed;slot;oh;gbt;x;y;counts;samples-40m";
constexpr auto eom_scan_format = FMT_STRING("{:d};{:d};{:d};{:d};{:d};{:d};{:d};{:d}");
} // anonymous namespace

/* We need to import std::optional<T> variables into the detector identifier
 * JSON objects. Make them importable in nlohmann::json via ADL serialization.
 */
namespace nlohmann { // update to NLOHMANN_JSON_NAMESPACE_BEGIN when available
template <typename T>
struct adl_serializer<std::optional<T>> {
    static void to_json(json& j, const std::optional<T>& v)
    {
        if (v.has_value())
            j = *v;
        else
            j = nullptr;
    }
};
} // update to NLOHMANN_JSON_NAMESPACE_END when available

XDAQ_INSTANTIATOR_IMPL(gem::managers::AMCManager);

gem::managers::AMCManager::AMCManager(xdaq::ApplicationStub* stub)
    : gem::utils::GEMFSMApplication(stub)
{
    // Configuration parameters
    p_appInfoSpace->fireItemAvailable("fedID", &m_fed_id);
    p_appInfoSpace->fireItemAvailable("slot", &m_slot);
    p_appInfoSpace->fireItemAvailable("automaskerMaskingPeriod", &m_automasker_masking_period);
    p_appInfoSpace->fireItemAvailable("automaskerSEURecoveryPeriod", &m_automasker_seu_recovery_period);
    p_appInfoSpace->fireItemAvailable("enableDAQLink", &m_enable_daqlink);
    p_appInfoSpace->fireItemAvailable("enableLocalReadout", &m_enable_local_readout);
    p_appInfoSpace->fireItemAvailable("enableTTCGenerator", &m_enable_ttc_generator);
    p_appInfoSpace->fireItemAvailable("enableZS", &m_enableZS);
    p_appInfoSpace->fireItemAvailable("powerCycleTimeout", &m_power_cycle_timeout);
    p_appInfoSpace->fireItemAvailable("powerOnTimeout", &m_power_on_timeout);
    p_appInfoSpace->fireItemAvailable("outputPath", &m_outputPath);

    // Web UI
    p_gemWebInterface = new gem::managers::AMCManagerWeb(this);

    // SOAP API
    xoap::deferredbind(this, this, &gem::managers::AMCManager::updateScanValueCalib, "updateScanValueCalib", XDAQ_NS_URI);
    xoap::deferredbind(this, this, &gem::managers::AMCManager::launchScan, "launchScan", XDAQ_NS_URI);

    // JSON API
    xgi::deferredbind(this, this, &gem::managers::AMCManager::calibrationStateUpdate, "calibrationStateUpdate");
    xgi::deferredbind(this, this, &gem::managers::AMCManager::configureOptoHybridFPGA, "configureOptoHybridFPGA");
    xgi::deferredbind(this, this, &gem::managers::AMCManager::readDetectorIdentifiers, "readDetectorIdentifiers");

    // Periodic masking timer
    const std::string t_timer_name = fmt::format(FMT_STRING("{}:{}:automasker"), m_xmlClass, m_instance);
    m_automasker_timer = toolbox::task::getTimerFactory()->createTimer(t_timer_name);
}

gem::managers::AMCManager::~AMCManager()
{
}

void gem::managers::AMCManager::actionPerformed(xdata::Event& event)
{
    gem::utils::GEMApplication::actionPerformed(event);

    if (event.type() == "urn:xdaq-event:setDefaultValues") {
        // Replace the environment variables in the output data path
        m_outputPath = toolbox::getRuntime()->expandPathName(std::string(m_outputPath.value_)).at(0);
    }
}

// State transitions
void gem::managers::AMCManager::initializeAction()
{
    CMSGEMOS_INFO("AMCManager::initializeAction begin");

    CMSGEMOS_INFO("AMCManager::initializeAction end");
}

void gem::managers::AMCManager::configureAction()
{
    CMSGEMOS_INFO("AMCManager::configureAction begin");

    const auto& backend_board_node = layout_tree() % ltree::address<ltree::backend_board_node> { m_fed_id.value_, m_slot.value_ };
    m_initialOptohybridMask = backend_board_node.recursive_children() | ltree::filter_nodes<ltree::link_node> | ltree::as_bitmask<decltype(m_initialOptohybridMask)>;

    CMSGEMOS_INFO("AMCManager::configureAction: connect to the AMC");
    const auto hostname = backend_board_node.hostname();
    m_connection = std::make_shared<xhal::client::XHALInterface>(hostname, m_gemLogger);
    m_connection_automasker = std::make_shared<xhal::client::XHALInterface>(hostname, m_gemLogger);

    CMSGEMOS_INFO("AMCManager::configureAction: power-on the front-end");
    std::vector<std::pair<std::string, std::future<std::string>>> power_on_futures;
    if (m_power_on_timeout.value_ != 0) {
        for (const auto& link : backend_board_node.recursive_children() | ltree::filter_nodes<ltree::link_node>) {
            const std::string display_name = link.display_name();
            const std::string wire_name = link.wire_name();
            power_on_futures.push_back(std::make_pair(display_name, core::dim::send_dcs_command(wire_name, "ON", m_power_on_timeout.value_)));
        }
    }

    CMSGEMOS_INFO("AMCManager::configureAction: recover the AMC");
    m_connection->call<gem::hardware::amc::recover>();
    m_connection->call<gem::hardware::amc::reset>();

    CMSGEMOS_INFO("AMCManager::configureAction: configure the AMC");
    m_connection->call<gem::hardware::amc::configure>();

    CMSGEMOS_INFO("AMCManager::configureAction: configure the TTC");

    m_ttc_generator_enabled = m_enable_ttc_generator.value_;
    if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::SCURVE) || m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::ARMDACSCAN))
        m_ttc_generator_enabled = true;

    m_connection->call<gem::hardware::amc::ttc::enableTTCGenerator>(m_ttc_generator_enabled);
    m_connection->call<gem::hardware::amc::ttc::setL1AEnable>(false);

    // Reset the TTC module once configured... with care!
    // Some counters are left at a non-zero value so they must be reset manually
    m_connection->call<gem::hardware::amc::ttc::ttcModuleReset>();
    m_connection->call<gem::hardware::amc::ttc::ttcCounterReset>();

    CMSGEMOS_INFO("AMCManager::configureAction: configure the DAQ");

    auto enable_daqlink = m_enable_daqlink.value_;
    auto local_readout_prescale = m_enable_local_readout.value_ ? hardware::amc::daq::ENABLE_LOCAL_READOUT : hardware::amc::daq::DISABLE_LOCAL_READOUT;
    if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::SCURVE) || m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::ARMDACSCAN)) {
        enable_daqlink = false;
        local_readout_prescale = 1;
    }

    m_connection->call<gem::hardware::amc::daq::configureFedID>(m_fed_id.value_, m_slot.value_);
    m_connection->call<gem::hardware::amc::daq::configureDAQModule>(enable_daqlink, local_readout_prescale, 0x0 /* disable all OH atm */, m_enableZS.value_);

    // Run type specifics
    m_scannedValue = m_scanInfo.bag.scanMin.value_;

    if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::PHYSICS)) {
        const uint32_t runParams = ((m_slot.value_ << 12) & 0xf) | (m_fed_id.value_ & 0xffff);
        m_connection->call<gem::hardware::amc::daq::setRunParameters>(runParams);
    } else if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::LATENCY)) {
        const uint32_t runParams = ((m_scanInfo.bag.signalSourceType.value_ & 0x2) << 21) | ((m_scanInfo.bag.calPulseAmplitude.value_ & 0xff) << 13) | ((m_scanInfo.bag.pulseStretch.value_ & 0x7) << 10) | (m_scannedValue & 0x3ff);
        //                           isExtTrig(1bit); isCurrentPulse(1bit); CFG_CAL_DAC(8bits); pulseStretch(3bits); latency(10bits);
        m_connection->call<gem::hardware::amc::daq::setRunParameters>(runParams);

        // Setting calibration data format as requested
        m_connection->call<gem::hardware::amc::daq::configureAMCCalDataFormat>(m_scanInfo.bag.calDataFormat, 0 /* VFAT channel */);

        if (m_scanInfo.bag.signalSourceType.value_ < 2) { // use calibration pulse from the TTC internally generated in the AMC
            m_connection->call<gem::hardware::utils::writeRemoteReg>("BEFE.GEM.TTC.CTRL.L1A_DELAY", m_scanInfo.bag.pulseDelay.value_); // send the L1A N BX after the calibration pulse
            m_connection->call<gem::hardware::utils::writeRemoteReg>("BEFE.GEM.TTC.CTRL.CALIBRATION_MODE", 0x1);
        } else { // use signal from the detector (or external calibration pulses)
            m_connection->call<gem::hardware::utils::writeRemoteReg>("BEFE.GEM.TTC.CTRL.L1A_DELAY", 0x0);
            m_connection->call<gem::hardware::utils::writeRemoteReg>("BEFE.GEM.TTC.CTRL.CALIBRATION_MODE", 0x0);
        }
    }

    m_connection->call<gem::hardware::amc::daq::setRunType>(m_scanInfo.bag.scanType.value_);

    CMSGEMOS_INFO("AMCManager::configureAction: check for unlocked links");

    // Check the outcome of the power on command
    for (auto& [display_name, future] : power_on_futures) {
        try {
            const auto rv = future.get();
            CMSGEMOS_INFO("AMCManager::configureAction: powered on successfully '" << display_name << "' with '" << rv << "'");
        } catch (const std::exception& e) {
            CMSGEMOS_WARN("AMCManager::configureAction: failed to power on '" << display_name << "' with '" << e.what() << "'");
        } catch (...) {
            CMSGEMOS_WARN("AMCManager::configureAction: failed to power on '" << display_name << "'");
        }
    }

    // Wait for the GBT timeout (~2 seconds) to kick-in if needed
    std::this_thread::sleep_for(std::chrono::milliseconds(2100));

    // Power-cycle unlocked links
    power_cycle();

    // Frontend configuration
    CMSGEMOS_INFO("AMCManager::configureAction: configure the frontend");
    m_optohybridMask = m_connection->call<gem::hardware::blaster::execute>(m_initialOptohybridMask);

    // Additional configuration for the calibration scans
    for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
        if (!((m_optohybridMask >> link) & 0x1))
            continue;

        // Retrieve the original (i.e. pre-configured one) VFAT mask
        // Impossibility to configure further a VFAT will result in disabling it for the data taking
        uint32_t vfatMask = m_connection->call<gem::hardware::vfat3::getVFATMask>(link);

        if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::LATENCY)) {
            CMSGEMOS_INFO("AMCManager::configureAction: beginning specifics for latency scan");

            vfatMask &= m_connection->call<gem::hardware::vfat3::broadcastWrite>(link, vfatMask, "CFG_LATENCY", m_scanInfo.bag.scanMin.value_);
            vfatMask &= m_connection->call<gem::hardware::vfat3::broadcastWrite>(link, vfatMask, "CFG_PULSE_STRETCH", m_scanInfo.bag.pulseStretch.value_);

            if (m_scanInfo.bag.signalSourceType.value_ < 2) { // Calibration pulses
                // Configure the calibration module for all VFAT
                gem::hardware::vfat3::calPulseConfig t_calpulse_config = {
                    // Note that a low CFG_CAL_DAC corresponds to a high injected charge in voltage mode, and to low injected currents in current mode
                    .cal_dac = uint8_t(m_scanInfo.bag.calPulseAmplitude.value_),
                    .duration = uint16_t(m_scanInfo.bag.calPulseDuration.value_),
                    .scale_factor = uint8_t(m_scanInfo.bag.calPulseFS.value_), // Used only in current mode
                };
                // Use voltage pulses (CAL_MODE == 0x1), or current pulses (CAL_MODE == 0x2),
                if (m_scanInfo.bag.signalSourceType.value_ == 0) { // Voltage mode
                    t_calpulse_config.mode = hardware::vfat3::VFATCalibrationMode::VOLTAGE;
                } else { // Current mode
                    t_calpulse_config.mode = hardware::vfat3::VFATCalibrationMode::CURRENT;
                }

                vfatMask &= m_connection->call<gem::hardware::vfat3::confCalPulse>(link, vfatMask, t_calpulse_config);

                // Disable the calibration pulses for all channels, but channel 0, on all VFAT
                vfatMask &= m_connection->call<gem::hardware::vfat3::enableChannelCalPulse>(link, vfatMask, gem::hardware::vfat3::VFAT_ALL_CHANNELS, false);
                vfatMask &= m_connection->call<gem::hardware::vfat3::enableChannelCalPulse>(link, vfatMask, 0, true);
            } else { // Physical particle signal
                // Disable the global and per-channel calibration circuits
                vfatMask &= m_connection->call<gem::hardware::vfat3::confCalPulse>(link, vfatMask, hardware::vfat3::calPulseConfig());
                vfatMask &= m_connection->call<gem::hardware::vfat3::enableChannelCalPulse>(link, vfatMask, gem::hardware::vfat3::VFAT_ALL_CHANNELS, false);
            }
        } else if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::PHYSICS)) {
            CMSGEMOS_INFO("AMCManager::configureAction: beginning specifics for Physics");
            if (m_scanInfo.bag.thresholdSourceType.value_ == 1) { // Use same configurable threshold for all VFATs
                vfatMask &= m_connection->call<gem::hardware::vfat3::broadcastWrite>(link, vfatMask, "CFG_THR_ARM_DAC", m_scanInfo.bag.threshold.value_);
                vfatMask &= m_connection->call<gem::hardware::vfat3::broadcastWrite>(link, vfatMask, "CFG_EN_HYST", 0);
            }
            if (m_scanInfo.bag.applyChannelMask.value_ == false)
                vfatMask &= m_connection->call<gem::hardware::vfat3::enableAllChannels>(link, vfatMask);
        } else if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::SBITARMDACSCAN)) {
            // Set the comparator mode: 0 is taken from the VFAT configuration files, 1 is CFD, 2 ARM, 3 ZCC

            switch (m_scanInfo.bag.comparatorMode.value_) {
            case 0:
                // Do nothing: the configuration is taken from the VFAT conf DB
                break;
            case 1:
                vfatMask &= m_connection->call<gem::hardware::vfat3::broadcastWrite>(link, vfatMask, "CFG_SEL_COMP_MODE", 0);
                vfatMask &= m_connection->call<gem::hardware::vfat3::broadcastWrite>(link, vfatMask, "CFG_FORCE_EN_ZCC", 0);
                break;
            case 2:
                vfatMask &= m_connection->call<gem::hardware::vfat3::broadcastWrite>(link, vfatMask, "CFG_SEL_COMP_MODE", 1);
                vfatMask &= m_connection->call<gem::hardware::vfat3::broadcastWrite>(link, vfatMask, "CFG_FORCE_EN_ZCC", 0);
                break;
            case 3:
                vfatMask &= m_connection->call<gem::hardware::vfat3::broadcastWrite>(link, vfatMask, "CFG_SEL_COMP_MODE", 2);
                vfatMask &= m_connection->call<gem::hardware::vfat3::broadcastWrite>(link, vfatMask, "CFG_FORCE_EN_ZCC", 1);
                break;
            default:
                throw std::runtime_error("AMCManager::configureAction: S-bit rate vs Threshold scan - thresholdType not recognized");
            }
        }

        // Push the new VFAT mask to the BE
        m_connection->call<gem::hardware::vfat3::setVFATMask>(link, vfatMask);

        // Update the VFAT mask local copy
        m_vfatMask.at(link) = vfatMask;
    }

    // The active OptoHybrids are now known
    m_connection->call<gem::hardware::amc::daq::setDAQLinkInputMask>(m_optohybridMask);
    CMSGEMOS_INFO(fmt::format(FMT_STRING("AMCManager::configureAction: configured OptoHybrid mask {:#x}"), m_optohybridMask));

    // Reset the DAQ counters after shaking the front-end
    m_connection->call<gem::hardware::amc::daq::resetDAQ>(false);

    CMSGEMOS_INFO("AMCManager::configureAction: prepare the trigger links");
    const uint32_t triggerLinksMask = m_connection->call<gem::hardware::amc::trigger::getValidLinks>(m_optohybridMask, true /* reset non-working links */);
    m_connection->call<gem::hardware::amc::trigger::enableLinksToEMTF>(triggerLinksMask);
    CMSGEMOS_INFO(fmt::format(FMT_STRING("AMCManager::configureAction: enabled trigger links for {:#x}"), triggerLinksMask));

    CMSGEMOS_INFO("AMCManager::configureAction: dump the VFAT configuration");
    dumpConfigInfoVFAT();

    // Summary for the user
    m_stateMessage = fmt::format(FMT_STRING("Configured OptoHybrid mask {:#x} (initial mask {:#x})"), m_optohybridMask, m_initialOptohybridMask);

    CMSGEMOS_INFO("AMCManager::configureAction end");
}

void gem::managers::AMCManager::startAction()
{
    CMSGEMOS_INFO("AMCManager::startAction begin");

    // Reset the event building until the next ReSync, proof that our front-end has been synchronized
    m_connection->call<gem::hardware::amc::daq::resetDAQ>(true);
    m_connection->call<gem::hardware::amc::daq::enableDAQ>(true);

    if (m_ttc_generator_enabled) {
        // Reset the OC, BC, and EC
        m_connection->call<gem::hardware::amc::ttc::ttcModuleReset>();
        m_connection->call<gem::hardware::amc::ttc::sendReSyncSequence>();
    } else {
        m_connection->call<gem::hardware::amc::ttc::setL1AEnable>(true);
        // FIXME: Remove when the AMC13 in loopback mode will send a ReSync
        m_connection->call<gem::hardware::amc::ttc::sendReSyncSequence>();
    }

    m_connection->call<gem::hardware::amc::ttc::ttcCounterReset>();

    // Start the automasker
    if (m_automasker_timer->isActive())
        m_automasker_timer->stop();
    m_automasker_timer->start();

    if (m_automasker_masking_period.value_ != 0) {
        toolbox::TimeInterval interval(m_automasker_masking_period.value_ / 1000.);
        toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
        m_automasker_timer->scheduleAtFixedRate(start, this, interval, 0, "masking");
    }

    if (m_automasker_seu_recovery_period.value_ != 0) {
        toolbox::TimeInterval interval(m_automasker_seu_recovery_period.value_ / 1000.);
        toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
        m_automasker_timer->scheduleAtFixedRate(start, this, interval, 0, "seu-recovery");
    }

    CMSGEMOS_INFO("AMCManager::saving VFAT configuration");
    saveConfigInfoVFAT();

    CMSGEMOS_INFO("AMCManager::startAction end");
}

void gem::managers::AMCManager::pauseAction()
{
    CMSGEMOS_INFO("AMCManager::pauseAction begin");

    CMSGEMOS_INFO("AMCManager::pauseAction end");
}

void gem::managers::AMCManager::resumeAction()
{
    CMSGEMOS_INFO("AMCManager::resumeAction begin");

    CMSGEMOS_INFO("AMCManager::resumeAction end");
}

void gem::managers::AMCManager::stopAction()
{
    CMSGEMOS_INFO("AMCManager::stopAction begin");

    if (m_automasker_timer->isActive())
        m_automasker_timer->stop();

    m_connection->call<gem::hardware::amc::ttc::setL1AEnable>(false);
    m_connection->call<gem::hardware::amc::daq::enableDAQ>(false);

    CMSGEMOS_INFO("AMCManager::stopAction end");
}

void gem::managers::AMCManager::haltAction()
{
    CMSGEMOS_INFO("AMCManager::haltAction begin");

    CMSGEMOS_INFO("AMCManager::haltAction end");
}

void gem::managers::AMCManager::power_cycle()
{
    // If the automatic power-cycle is enabled
    if (m_power_cycle_timeout.value_ != 0) {
        const uint32_t workingOptoHybridMask = m_connection->call<gem::hardware::gbt::getGBTStatus>(false /* instantaneous only */) & m_initialOptohybridMask;
        uint32_t unlockedOptoHybridMask = workingOptoHybridMask ^ m_initialOptohybridMask;

        std::vector<std::pair<std::string, std::future<std::string>>> futures;
        for (size_t ohN = 0; unlockedOptoHybridMask != 0; ++ohN, unlockedOptoHybridMask >>= 1)
            if (unlockedOptoHybridMask & 0x1) {
                const auto layout_tree_address = ltree::address<ltree::link_node> { m_fed_id.value_, m_slot.value_, ohN };
                std::string display_name, wire_name;

                try {
                    display_name = (layout_tree() % layout_tree_address).display_name();
                    wire_name = (layout_tree() % layout_tree_address).wire_name();
                } catch (...) {
                    CMSGEMOS_WARN("AMCManager::power_cycle: '" << layout_tree_address << "' not found in the layour tree, skipping automatic power cycle");
                    continue;
                }

                futures.push_back(std::make_pair(display_name, core::dim::send_dcs_command(wire_name, "POWERCYCLE", m_power_cycle_timeout.value_)));
            }

        for (auto& [display_name, future] : futures) {
            try {
                CMSGEMOS_INFO("AMCManager::power_cycle: power cycle successful for '" << display_name << "' with '" << future.get() << "'");
            } catch (const std::exception& e) {
                CMSGEMOS_WARN("AMCManager::power_cycle: failed to power cycle '" << display_name << "' with '" << e.what() << "'");
            } catch (...) {
                CMSGEMOS_WARN("AMCManager::power_cycle: failed to power cycle '" << display_name << "'");
            }
        }
    }

    // Present the unlocked chambers to the user anyway
    const uint32_t workingOptoHybridMask = m_connection->call<gem::hardware::gbt::getGBTStatus>(false /* instantaneous only */) & m_initialOptohybridMask;
    uint32_t unlockedOptoHybridMask = workingOptoHybridMask ^ m_initialOptohybridMask;

    std::vector<std::string> unlockedOptoHybrids;
    for (size_t ohN = 0; unlockedOptoHybridMask != 0; ++ohN, unlockedOptoHybridMask >>= 1)
        if (unlockedOptoHybridMask & 0x1) {
            const auto layout_tree_address = ltree::address<ltree::link_node> { m_fed_id.value_, m_slot.value_, ohN };
            try {
                const auto display_name = (layout_tree() % layout_tree_address).display_name();
                unlockedOptoHybrids.push_back(display_name);
            } catch (...) {
                std::stringstream ss;
                ss << layout_tree_address;
                unlockedOptoHybrids.push_back(ss.str());
            }
        }

    if (unlockedOptoHybrids.size()) {
        CMSGEMOS_INFO(fmt::format(FMT_STRING("AMCManager::power_cycle: the following OptoHybrids are not fully locked and might require a power-cycle : '{}'"), fmt::join(unlockedOptoHybrids, "', '")));
    } else {
        CMSGEMOS_INFO("AMCManager::power_cycle: all OptoHybrids fully locked");
    }
}

void gem::managers::AMCManager::timeExpired(toolbox::task::TimerEvent& event)
{
    if (event.getTimerTask()->name == "seu-recovery") {
        const auto nRecoveredOH = m_connection_automasker->call<gem::hardware::expert::recoverSEU>(m_optohybridMask);
        if (!nRecoveredOH)
            return; // No OH recovered from SEU, do not proceeed further

        m_stateMessage = fmt::format(FMT_STRING("recovered {:d} OH from SEU"), nRecoveredOH);
        CMSGEMOS_WARN("AMCManager: " << m_stateMessage.value_);

        // Run the VFAT masking after a successful SEU recovery in case GBT have been lost in the process
    }

    // Mask unstable VFAT
    const auto number_masked_vfat = m_connection_automasker->call<gem::hardware::expert::maskUnstableVFAT>(m_optohybridMask);
    if (number_masked_vfat) {
        m_stateMessage = fmt::format(FMT_STRING("masked {:d} unstable VFAT"), number_masked_vfat);
        CMSGEMOS_WARN("AMCManager: " << m_stateMessage.value_);
    }

    // Synchronize the software masks with the firmware ones
    // This includes mask (re)set both in software and firmware
    for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link)
        if (((m_optohybridMask >> link) & 0x1))
            m_vfatMask.at(link) = m_connection_automasker->call<gem::hardware::vfat3::getVFATMask>(link);
}

xoap::MessageReference gem::managers::AMCManager::updateScanValueCalib(xoap::MessageReference msg)
{
    CMSGEMOS_DEBUG("AMCManager::updateScanValueCalib");

    const std::string commandName = "updateScanValueCalib";

    try {
        // CG: Do we need it ?
        // LP: I think it might prevent to move forward at high rate... :/
        while (!m_connection->call<gem::hardware::amc::daq::l1aFIFOIsEmpty>()) {
            CMSGEMOS_DEBUG("AMCManager::updateScanValueCalib:  waiting for AMC" << m_slot.value_ << " to finish building events");
            usleep(10);
        }

        CMSGEMOS_INFO("AMCManager::updateScanValueCalib: current scan value " << m_scannedValue);

        // First, set the run type to "invalid" while changing the front-end configuration
        m_connection->call<gem::hardware::amc::daq::setRunType>(static_cast<int>(gem::calibration::calType::INVALID));

        // Move to the next scan point
        m_scannedValue += m_scanInfo.bag.stepSize.value_;

        // Implement cyclic latency scan
        if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::LATENCY)
            && m_scanInfo.bag.cyclic
            && m_scannedValue > m_scanInfo.bag.scanMax.value_)
            m_scannedValue = m_scanInfo.bag.scanMin.value_;

        // Stay in "invalid" run type once the scan is over
        if (m_scannedValue > m_scanInfo.bag.scanMax.value_)
            return gem::utils::soap::makeSOAPReply(commandName, "Done");

        // Update the front-end configuration
        for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
            if (!((m_optohybridMask >> link) & 0x1))
                continue;

            const uint32_t vfatMask = m_vfatMask.at(link);

            // We know that VFAT communication is unstable in presence of L1A (firmware bug?)
            // Need to implement some re-try logic
            size_t attempts = 0;
            uint32_t success_mask = 0;
            do {
                if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::LATENCY)) {
                    CMSGEMOS_DEBUG("AMCManager::updateScanValueCalib: latency Scan on OptoHybrid " << link << " - slot " << m_slot.value_ << " - latency " << m_scannedValue);
                    success_mask |= m_connection->call<gem::hardware::vfat3::broadcastWrite>(link, vfatMask, "CFG_LATENCY", m_scannedValue);
                }
            } while (((success_mask & vfatMask) != vfatMask) && (++attempts < scan_point_update_attempts));

            // Mask the VFAT that could not be configured
            // Software mask update will be taken care of by the next update iteration
            if ((success_mask & vfatMask) != vfatMask) {
                CMSGEMOS_WARN(fmt::format(FMT_STRING("AMCManager::updateScanValueCalib: failed to update latency parameter for VFAT {:#08x} on OptoHybrid {:d} - disabling"), ~success_mask & vfatMask, link));
                m_connection->call<gem::hardware::vfat3::updateVFATMask>(link, success_mask & vfatMask);
            }
        }

        // Update the run parameters
        if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::LATENCY)) {
            // Updating the runparam as descrbed here: https://github.com/cms-gem-daq-project/cmsgemos/pull/302
            const uint32_t runParams = ((m_scanInfo.bag.signalSourceType.value_ & 0x2) << 21) | ((m_scanInfo.bag.calPulseAmplitude.value_ & 0xff) << 13) | ((m_scanInfo.bag.pulseStretch.value_ & 0x7) << 10) | (m_scannedValue & 0x3ff);
            //                           isExtTrig(1bit); isCurrentPulse(1bit); CFG_CAL_DAC(8bits); pulseStretch(3bits); latency(10bits);

            m_connection->call<gem::hardware::amc::daq::setRunParameters>(runParams);
        }

        // All done! Restore the run type to the current scan type
        m_connection->call<gem::hardware::amc::daq::setRunType>(m_scanInfo.bag.scanType.value_);

        CMSGEMOS_INFO("AMCManager::updateScanValueCalib: updated scan value " << m_scannedValue);

        return gem::utils::soap::makeSOAPReply(commandName, "Done");
    } catch (xcept::Exception& err) {
        std::stringstream msg;
        msg << "AMCManager::updateScanValueCalib: Scan failed with the exception: "
            << xcept::stdformat_exception_history(err).c_str();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
    } catch (std::exception& e) {
        std::stringstream msg;
        msg << "AMCManager::updateScanValueCalib: Scan failed with the exception: " << e.what();
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
    } catch (...) {
        std::stringstream msg;
        msg << "AMCManager::updateScanValueCalib: Scan failed with an unknown exception.";
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
    }

    CMSGEMOS_DEBUG("AMCManager::updateScanValueCalib end");

    return gem::utils::soap::makeSOAPReply(commandName, "NotDone");
}

xoap::MessageReference gem::managers::AMCManager::launchScan(xoap::MessageReference msg)
{
    const std::string commandName = "launchScan";

    if (!isStartAllowed()) {
        std::stringstream msg;
        msg << "AMCManager::launchScan: Scan could not be started (did you re-configure the system?)";
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
        return gem::utils::soap::makeSOAPReply(commandName, "Scan not launched");
    }

    // Create thread for the scan to run in the background
    std::thread t([&]() {
        m_stateMessage = "Launching scan";
        m_calibrationState = true;
        m_calibrationType = m_scanInfo.bag.scanType.value_;

        try {
            // Create output directory
            const std::string output_pathname = fmt::format(FMT_STRING("{:s}/run{:s}"), m_outputPath.value_, runNumberToString());
            std::filesystem::create_directories(output_pathname);
            CMSGEMOS_INFO("AMCManager::launchScan: saving scan output in " + output_pathname);

            if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::SBITARMDACSCAN)) {
                CMSGEMOS_INFO("AMCManager::launchScan: launching S-bit rate vs Threshold scan");

                // Save scan configuration
                {
                    std::ofstream log(output_pathname + "/sbit-rate-scan-config.log");
                    log << m_scanInfo.bag.toString();
                }

                // Create the output txt file for scan result
                const std::string t_output_filename = fmt::format(FMT_STRING("{:s}/fed{:02d}-slot{:02d}.txt"), output_pathname, m_fed_id.value_, m_slot.value_);
                std::ofstream out_file(t_output_filename);
                out_file << sbit_rate_scan_header << std::endl;

                // Set the register that needs to be scanned: if the thresholdType is 0 is the THR_ARM_DAC, if 1 is CFG_THR_ZCC_DAC
                std::string register_thr = "CFG_THR_ARM_DAC";
                if (m_scanInfo.bag.thresholdType.value_ == 1)
                    register_thr = "CFG_THR_ZCC_DAC";

                // If single channel is selected, use the selected channel for the scan
                uint8_t channel_used = gem::hardware::vfat3::VFAT_ALL_CHANNELS; // Default use all VFAT channels
                if (m_scanInfo.bag.doSingleChannel) {
                    if (m_scanInfo.bag.vfatCh.value_ >= gem::hardware::vfat3::VFAT_ALL_CHANNELS)
                        throw std::runtime_error("AMCManager::launchScan: S-bit rate vs Threshold scan: Invalid vfatCh value chosen. The number should be in the range [0; 127]");
                    channel_used = m_scanInfo.bag.vfatCh.value_;
                }

                // Loop over iterations number
                for (int iter = 0; iter < m_scanInfo.bag.iterNum.value_; iter++) {
                    CMSGEMOS_INFO(fmt::format(FMT_STRING("AMCManager::launchScan: scanning S-bit rate vs Threshold ({} out of {})"), iter + 1, m_scanInfo.bag.iterNum.value_));
                    m_stateMessage = fmt::format(FMT_STRING("Scanning S-bit rate vs Threshold ({}/{})"), iter + 1, m_scanInfo.bag.iterNum.value_);

                    // Call RPC function for the scan
                    const auto t_result = m_connection->call<gem::hardware::calibration::sbitRateScanParallel>(m_optohybridMask, channel_used, m_scanInfo.bag.scanMin.value_, m_scanInfo.bag.scanMax.value_, m_scanInfo.bag.stepSize.value_, register_thr, m_scanInfo.bag.timeInterval.value_, m_scanInfo.bag.toggleRunMode);

                    for (const auto& [oh_index, oh_data] : t_result)
                        for (const auto& [vfat_index, vfat_data] : oh_data)
                            for (const auto& [dac_value, rate] : vfat_data)
                                out_file << fmt::format(sbit_rate_scan_format, iter, m_fed_id.value_, m_slot.value_, oh_index, vfat_index, dac_value, rate) << std::endl;

                    // Store results on-disk after each iteration
                    out_file << std::flush;
                }

                CMSGEMOS_INFO("AMCManager::launchScan: S-bit rate vs Threshold scan done");
            } else if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::DACSCAN)) {
                CMSGEMOS_INFO("AMCManager::launchScan: launching DAC scan");

                // Save scan configuration
                {
                    std::ofstream log(output_pathname + "/dac-scan-config.log");
                    log << m_scanInfo.bag.toString();
                }

                // Create the output txt file for scan result
                const std::string t_output_filename = fmt::format(FMT_STRING("{:s}/fed{:02d}-slot{:02d}.txt"), output_pathname, m_fed_id.value_, m_slot.value_);
                std::ofstream out_file(t_output_filename);
                out_file << dac_scan_header << std::endl;

                // Loop over the DAC and OH
                for (const auto& scanDAC : m_scanInfo.bag.dacScanInfo) {
                    for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
                        if (!((m_optohybridMask >> link) & 0x1))
                            continue;

                        CMSGEMOS_INFO(fmt::format(FMT_STRING("AMCManager::launchScan: scanning DAC circuit {} from {} to {} for OptoHybrid {}"), scanDAC.bag.name.value_, scanDAC.bag.min.value_, scanDAC.bag.max.value_, link));
                        m_stateMessage = fmt::format(FMT_STRING("Scanning DAC circuit {} for OptoHybrid {}"), scanDAC.bag.name.value_, link);

                        const auto t_result = m_connection->call<gem::hardware::calibration::dacScan>(link, m_vfatMask.at(link), scanDAC.bag.name.value_, 1, m_scanInfo.bag.adcExtType.value_);
                        for (const auto& [vfat_index, vfat_data] : t_result)
                            for (const auto& [dac_value, adc_value] : vfat_data)
                                out_file << fmt::format(dac_scan_format, scanDAC.bag.name.value_, m_fed_id.value_, m_slot.value_, link, vfat_index, dac_value, adc_value.first, adc_value.second) << std::endl;

                        // Store results on-disk after each DAC scan
                        out_file << std::flush;
                    }
                }

                CMSGEMOS_INFO("AMCManager::launchScan: DAC scan done");
            } else if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::GBTRXPHASE)) {
                CMSGEMOS_INFO("AMCManager::launchScan: launching GBT phase scan");

                // Save scan configuration
                {
                    std::ofstream log(output_pathname + "/gbt-phase-scan-config.log");
                    log << m_scanInfo.bag.toString();
                }

                // Create the output txt file for scan result
                const std::string t_raw_output_filename = fmt::format(FMT_STRING("{:s}/fed{:02d}-slot{:02d}.log"), output_pathname, m_fed_id.value_, m_slot.value_);
                std::ofstream raw_out_file(t_raw_output_filename);
                raw_out_file << gbt_phase_scan_header << std::endl;

                const std::string t_output_filename = fmt::format(FMT_STRING("{:s}/fed{:02d}-slot{:02d}.cfg"), output_pathname, m_fed_id.value_, m_slot.value_);
                std::ofstream out_file(t_output_filename);
                out_file << gbt_phase_result_header << std::endl;

                // This does not run in parallel over the OH... :(
                for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
                    if (!((m_initialOptohybridMask >> link) & 0x1))
                        continue;

                    CMSGEMOS_INFO(fmt::format(FMT_STRING("AMCManager::launchScan: scanning GBT phases on OptoHybrid {}"), link));
                    m_stateMessage = fmt::format(FMT_STRING("Scanning GBT phases on OptoHybrid {}"), link);

                    // Scan the phase
                    auto t_result = m_connection->call<gem::hardware::gbt::scanGBTPhases>(link, m_scanInfo.bag.nTransactions.value_, m_scanInfo.bag.nTriggers.value_);

                    for (size_t vfatN = 0; vfatN < t_result.size(); ++vfatN) {
                        // Store the raw data
                        for (size_t phase = 0; phase < t_result.at(vfatN).size(); ++phase)
                            raw_out_file << fmt::format(gbt_phase_scan_format, m_fed_id.value_, m_slot.value_, link, vfatN, phase, t_result.at(vfatN).at(phase)) << std::endl;

                        // Analyze the result
                        std::vector<bool> reduced_phases_vector(t_result.at(vfatN).begin(), t_result.at(vfatN).end());
                        auto optimal_phase = core::find_gbt_phase(reduced_phases_vector);

                        // We have to find the best phase in a lookup table when all phases are reported as good
                        if (!optimal_phase) {
                            const auto layout_tree_address = ltree::address<ltree::link_node> { m_fed_id.value_, m_slot.value_, link };
                            const auto& gem_type = (layout_tree() % layout_tree_address).gem_type();
                            if (auto it { hardware::gbt::default_phases.find(gem_type) }; it != std::end(hardware::gbt::default_phases)) {
                                optimal_phase = it->second.at(vfatN);
                            } else {
                                optimal_phase = hardware::gbt::PHASE_DEFAULT_UNKNOWN_TYPE;
                            }
                        }

                        // Store the analyzed output
                        out_file << fmt::format(gbt_phase_result_format, m_fed_id.value_, m_slot.value_, link, vfatN, *optimal_phase) << std::endl;
                    }

                    // Store results on-disk after each GBT phase scan
                    raw_out_file << std::flush;
                    out_file << std::flush;
                }

                CMSGEMOS_INFO("AMCManager::launchScan: GBT phase scan done");
            } else if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::SCURVE)) {
                CMSGEMOS_INFO("AMCManager::launchScan: launching S-curve scan");
                m_stateMessage = "Running S-curve scan";

                // Save scan configuration
                {
                    std::ofstream log(output_pathname + "/scurve-config.log");
                    log << m_scanInfo.bag.toString();
                }

                m_connection->call<gem::hardware::amc::daq::setRunType>(m_scanInfo.bag.scanType.value_);

                // Reset the event building until the next ReSync, proof that our front-end has been synchronized
                m_connection->call<gem::hardware::amc::daq::resetDAQ>(true);
                m_connection->call<gem::hardware::amc::daq::enableDAQ>(true);

                // Reset the OC, BC, and EC
                m_connection->call<gem::hardware::amc::ttc::ttcModuleReset>();
                m_connection->call<gem::hardware::amc::ttc::sendSingleReSync>();

                m_connection->call<gem::hardware::amc::ttc::ttcCounterReset>();

                // Trigger the scan routine on the back-end board
                m_connection->call<gem::hardware::calibration::scurveScan>(m_optohybridMask, m_scanInfo.bag.scanMin.value_, m_scanInfo.bag.scanMax.value_, m_scanInfo.bag.stepSize.value_, m_scanInfo.bag.nTriggers.value_, m_scanInfo.bag.l1aInterval.value_, m_scanInfo.bag.pulseDelay.value_, m_scanInfo.bag.latency.value_, m_scanInfo.bag.pulseStretch.value_, m_scanInfo.bag.thresholdSourceType.value_, m_scanInfo.bag.threshold.value_, m_scanInfo.bag.trimmingSourceType.value_, m_scanInfo.bag.trimming.value_);

                CMSGEMOS_INFO("AMCManager::launchScan: S-curve scan done");
            } else if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::CLUSTERMASKSCAN)) {
                CMSGEMOS_INFO("AMCManager::launchScan: launching trigger cluster mask scan");
                m_stateMessage = "Running trigger cluster mask scan";

                // L1A shouldn't be active to start with

                // Save scan configuration
                {
                    std::ofstream log(output_pathname + "/cluster-mask-scan-config.log");
                    log << m_scanInfo.bag.toString();
                }

                // Create the output txt file for scan result
                const std::string t_output_filename = fmt::format(FMT_STRING("{:s}/fed{:02d}-slot{:02d}.txt"), output_pathname, m_fed_id.value_, m_slot.value_);
                std::ofstream out_file(t_output_filename);
                out_file << clusterMask_rate_scan_header << std::endl;

                const auto t_result = m_connection->call<gem::hardware::calibration::clusterMaskRateScanParallel>(m_optohybridMask, m_scanInfo.bag.scanMin.value_, m_scanInfo.bag.scanMax.value_, m_scanInfo.bag.timeInterval.value_, m_scanInfo.bag.l1aInterval.value_);

                for (const auto& [oh_index, clusterMask_data] : t_result)
                    for (const auto& clusterMask_rate : clusterMask_data)
                        out_file << fmt::format(clusterMask_rate_scan_format, m_fed_id.value_, m_slot.value_, oh_index, clusterMask_rate.delay, clusterMask_rate.cluster_rate_unmasked, clusterMask_rate.cluster_rate_masked, clusterMask_rate.trigger_rate, clusterMask_rate.l1a_period) << std::endl;

                out_file << std::flush;
                CMSGEMOS_INFO("AMCManager::launchScan: trigger cluster mask scan done");
            } else if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::CALPULSESCAN)) {
                CMSGEMOS_INFO("AMCManager::launchScan: launching calibration pulse scan");

                // Save scan configuration
                {
                    std::ofstream log(output_pathname + "/cal-pulse-scan-config.log");
                    log << m_scanInfo.bag.toString();
                }

                // Create the output txt file for scan result
                const std::string t_output_filename = fmt::format(FMT_STRING("{:s}/fed{:02d}-slot{:02d}.txt"), output_pathname, m_fed_id.value_, m_slot.value_);
                std::ofstream out_file(t_output_filename);
                out_file << calpulse_scan_header << std::endl;

                // Loop over the OptoHybrids
                for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
                    if (!((m_optohybridMask >> link) & 0x1))
                        continue;

                    CMSGEMOS_INFO(fmt::format(FMT_STRING("AMCManager::launchScan: scanning OptoHybrid {} with calibration pulses"), link));
                    m_stateMessage = fmt::format(FMT_STRING("Scanning OptoHybrid {} with calibration pulses"), link);

                    const auto t_result = m_connection->call<gem::hardware::calibration::calPulseScan>(link, m_scanInfo.bag.nTriggers.value_, m_scanInfo.bag.calPulseAmplitude.value_, m_scanInfo.bag.thresholdSourceType.value_, m_scanInfo.bag.threshold.value_, m_scanInfo.bag.l1aInterval.value_, m_scanInfo.bag.pulseDelay.value_, m_scanInfo.bag.latency.value_, m_scanInfo.bag.pulseStretch.value_);

                    for (const auto& [vfat_index, vfat_data] : t_result)
                        for (size_t channel = 0; channel < vfat_data.size(); ++channel)
                            out_file << fmt::format(calpulse_scan_format, m_fed_id.value_, m_slot.value_, link, vfat_index, channel, vfat_data[channel], m_scanInfo.bag.nTriggers.value_) << std::endl;

                    // Store results on-disk after each OptoHybrid
                    out_file << std::flush;
                }

                CMSGEMOS_INFO("AMCManager::launchScan: calibration pulse scan done");
            } else if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::ARMDACSCAN)) {
                CMSGEMOS_INFO("AMCManager::launchScan: launching threshold ARM DAC scan");
                m_stateMessage = "Running threshold ARM DAC scan";

                // Save scan configuration
                {
                    std::ofstream log(output_pathname + "/threshold-scan-config.log");
                    log << m_scanInfo.bag.toString();
                }

                m_connection->call<gem::hardware::amc::daq::setRunType>(m_scanInfo.bag.scanType.value_);

                // Reset the event building until the next ReSync, proof that our front-end has been synchronized
                m_connection->call<gem::hardware::amc::daq::resetDAQ>(true);
                m_connection->call<gem::hardware::amc::daq::enableDAQ>(true);

                // Reset the OC, BC, and EC
                m_connection->call<gem::hardware::amc::ttc::ttcModuleReset>();
                m_connection->call<gem::hardware::amc::ttc::sendSingleReSync>();

                m_connection->call<gem::hardware::amc::ttc::ttcCounterReset>();

                // Trigger the scan routine on the back-end board
                m_connection->call<gem::hardware::calibration::thresholdScan>(m_optohybridMask, m_scanInfo.bag.scanMin.value_, m_scanInfo.bag.scanMax.value_, m_scanInfo.bag.stepSize.value_, m_scanInfo.bag.nTriggers.value_, m_scanInfo.bag.l1aInterval.value_);

                CMSGEMOS_INFO("AMCManager::launchScan: threshold ARM DAC scan done");
            } else if (m_scanInfo.bag.scanType.value_ == static_cast<int>(gem::calibration::calType::EOMSCAN)) {
                CMSGEMOS_INFO("AMCManager::launchScan: launching eye opening monitor scan");

                // Save scan configuration
                {
                    std::ofstream log(output_pathname + "/eye-opening-monitor-scan-config.log");
                    log << m_scanInfo.bag.toString();
                }

                // Create the output txt file for scan result
                const std::string t_output_filename = fmt::format(FMT_STRING("{:s}/fed{:02d}-slot{:02d}.txt"), output_pathname, m_fed_id.value_, m_slot.value_);
                std::ofstream out_file(t_output_filename);
                out_file << eom_scan_header << std::endl;

                // Loop over the OH
                for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
                    if (!((m_optohybridMask >> link) & 0x1))
                        continue;

                    CMSGEMOS_INFO(fmt::format(FMT_STRING("AMCManager::launchScan: scanning eye opening monitor OptoHybrid {}"), link));
                    m_stateMessage = fmt::format(FMT_STRING("Scanning eye opening monitor on OptoHybrid {}"), link);

                    const auto t_result = m_connection->call<gem::hardware::calibration::eyeOpeningMonitorScan>(link, m_scanInfo.bag.eomSamplesBX.value_, m_scanInfo.bag.lpgbtEqAttnGain.value_);

                    for (const auto& [gbt_index, gbt_data] : t_result)
                        for (size_t x_axis = 0; x_axis < 64; x_axis++)
                            for (size_t y_axis = 0; y_axis < 31; y_axis++)
                                out_file << fmt::format(eom_scan_format, m_fed_id.value_, m_slot.value_, link, gbt_index, x_axis, y_axis, gbt_data[x_axis][y_axis].first, gbt_data[x_axis][y_axis].second) << std::endl;

                    // Store results on-disk after each OH
                    out_file << std::flush;
                }

                CMSGEMOS_INFO("AMCManager::launchScan: eye opening monitor scan done");
            }
        } catch (std::exception& e) {
            std::stringstream msg;
            msg << "AMCManager::launchScan: Scan failed with the exception: " << e.what();
            CMSGEMOS_ERROR(msg.str());
            m_stateMessage = msg.str();
            fireEvent("Fail");
        } catch (...) {
            std::stringstream msg;
            msg << "AMCManager::launchScan: Scan failed with an unknown exception.";
            CMSGEMOS_ERROR(msg.str());
            m_stateMessage = msg.str();
            fireEvent("Fail");
        }

        m_calibrationState = false;
        m_stateMessage = "Scan done!";
    });

    // Detach from the thread so that the object can be safely destroyed
    t.detach();

    return gem::utils::soap::makeSOAPReply(commandName, "Scan launched");
}

void gem::managers::AMCManager::dumpConfigInfoVFAT()
{
    for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
        if (!((m_optohybridMask >> link) & 0x1))
            continue;

        auto configLat = m_connection->call<gem::hardware::vfat3::broadcastRead>(link, m_vfatMask.at(link), "CFG_LATENCY");
        auto configThr = m_connection->call<gem::hardware::vfat3::broadcastRead>(link, m_vfatMask.at(link), "CFG_THR_ARM_DAC");

        for (size_t i = 0; i < configLat.size(); i++) {
            if (!((m_vfatMask.at(link) >> i) & 0x1))
                continue; // Not saving info for VFAT that are not in the current mask (i.e. not included in the run)

            // If a VFAT had communication issues while retrieving the configuration
            // parameters but is not masked, its base node will exist in the JSON object
            // and only *successfully* retrieved parameters will be stored. Note that if
            // no parameter is successfully retrieved, it will lead to an empty object.
            auto& vfatNode = m_jsonVFAT["fed"][fmt::format(FMT_STRING("{:d}"), m_fed_id.value_)]["slot"][fmt::format(FMT_STRING("{:d}"), m_slot.value_)]["link"][fmt::format(FMT_STRING("{:d}"), link)]["vfat"][fmt::format(FMT_STRING("{:d}"), i)];
            vfatNode = nlohmann::json::object();
            if (configLat[i])
                vfatNode["LATENCY"] = *configLat[i];
            if (configThr[i])
                vfatNode["THRESHOLD_DAC"] = *configThr[i];
        }

        CMSGEMOS_DEBUG(fmt::format(FMT_STRING("AMCManager::dumpConfigInfoVFAT: dump json {:s}"), m_jsonVFAT.dump()));
    }
}

void gem::managers::AMCManager::saveConfigInfoVFAT()
{
    try {
        // Create directory and output JSON file for the VFAT configuration dump
        const std::string output_pathname = fmt::format(FMT_STRING("{:s}/run{:s}"), m_outputPath.value_, runNumberToString());
        std::filesystem::create_directories(output_pathname);
        const std::string output_filename = fmt::format(FMT_STRING("fed{:02d}-amc{:02d}-config.json"), m_fed_id.value_, m_slot.value_);
        std::ofstream out_file(output_pathname + "/" + output_filename);
        out_file << m_jsonVFAT.dump(4) << std::endl;
        CMSGEMOS_INFO(fmt::format(FMT_STRING("AMCManager::saveConfigInfoVFAT: created file {:s}"), output_filename));
    } catch (...) {
        std::stringstream msg;
        msg << "AMCManager::saveConfigInfoVFAT: failed to save json file with VFAT config.";
        CMSGEMOS_ERROR(msg.str());
        m_stateMessage = msg.str();
        fireEvent("Fail");
    }
}

void gem::managers::AMCManager::calibrationStateUpdate(xgi::Input* in, xgi::Output* out)
{
    nlohmann::json jsonFile;

    jsonFile["calibrationState"] = m_calibrationState.load();
    jsonFile["calibrationType"] = m_calibrationType.load();

    *out << jsonFile.dump(4) << std::endl;
}

void gem::managers::AMCManager::configureOptoHybridFPGA(xgi::Input* in, xgi::Output* out)
{
    cgicc::Cgicc cgi(in);

    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");

    const auto t_optohybridMask = cgi.getElement("oh-mask");
    if (t_optohybridMask == cgi.getElements().end()) {
        out->getHTTPResponseHeader().getStatusCode(400);
        out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(400));
        *out << R"--({"message": "Mask not provided"})--" << std::endl;
        return;
    }

    uint32_t optohybridMask = 0;
    try {
        optohybridMask = std::stoul(t_optohybridMask->getValue(), nullptr, 0 /* auto-detect base*/);
    } catch (...) {
        out->getHTTPResponseHeader().getStatusCode(400);
        out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(400));
        *out << R"--({"message": "Failed to parse mask"})--" << std::endl;
        return;
    }

    if (!m_connection_automasker) {
        out->getHTTPResponseHeader().getStatusCode(500);
        out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(500));
        *out << R"--({"message": "Not connected to backend board"})--" << std::endl;
        return;
    }

    const uint32_t successMask = m_connection_automasker->call<gem::hardware::blaster::configureOptoHybridFPGA>(optohybridMask);

    *out << fmt::format(FMT_STRING(R"--({{"message": "Successfully configured OH FPGA {:#x}"}})--"), successMask) << std::endl;
}

void gem::managers::AMCManager::readDetectorIdentifiers(xgi::Input* in, xgi::Output* out)
{
    cgicc::Cgicc cgi(in);

    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");

    const auto t_optohybridMask = cgi.getElement("oh-mask");
    if (t_optohybridMask == cgi.getElements().end()) {
        out->getHTTPResponseHeader().getStatusCode(400);
        out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(400));
        *out << R"--({"message": "Mask not provided"})--" << std::endl;
        return;
    }

    uint32_t optohybridMask = 0;
    try {
        optohybridMask = std::stoul(t_optohybridMask->getValue(), nullptr, 0 /* auto-detect base*/);
    } catch (...) {
        out->getHTTPResponseHeader().getStatusCode(400);
        out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(400));
        *out << R"--({"message": "Failed to parse mask"})--" << std::endl;
        return;
    }

    if (!m_connection) {
        out->getHTTPResponseHeader().getStatusCode(500);
        out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(500));
        *out << R"--({"message": "Not connected to backend board"})--" << std::endl;
        return;
    }

    nlohmann::json output;

    const auto fpgaSerialNumbers = m_connection->call<gem::hardware::oh::getOHfpgaDNA>(optohybridMask);

    for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
        if (!((optohybridMask >> link) & 0x1))
            continue;

        const auto gbtSerialNumbers = m_connection->call<gem::hardware::gbt::readGBTSerialNumber>(link);
        const auto vfatSerialNumbers = m_connection->call<gem::hardware::vfat3::getVFAT3ChipIDs>(link, -1, false);

        output[std::to_string(link)] = {
            { "fpga", fpgaSerialNumbers.at(link) },
            { "gbt", gbtSerialNumbers },
            { "vfat", vfatSerialNumbers }
        };
    }

    *out << output.dump() << std::endl;
}
