/// @file

#ifndef GEM_MANAGERS_AMCMANAGER_H
#define GEM_MANAGERS_AMCMANAGER_H

#include <gem/utils/GEMFSMApplication.h>

#include <nlohmann/json.hpp>
#include <toolbox/task/TimerListener.h>
#include <xdata/Boolean.h>
#include <xdata/UnsignedInteger32.h>

#include <atomic>
#include <memory>

namespace xhal::client {
class XHALInterface;
}

namespace toolbox::task {
class Timer;
}

using xhal_interface_shared_ptr = std::shared_ptr<xhal::client::XHALInterface>;

namespace gem::managers {

class AMCManagerWeb;

class AMCManager : public gem::utils::GEMFSMApplication,
                   protected toolbox::task::TimerListener {
    friend class AMCManagerWeb;

public:
    XDAQ_INSTANTIATOR();

    AMCManager(xdaq::ApplicationStub* s);
    virtual ~AMCManager();

protected:
    void actionPerformed(xdata::Event& event) override;

    // State transitions
    void initializeAction() override;
    void configureAction() override;
    void startAction() override;
    void pauseAction() override;
    void resumeAction() override;
    void stopAction() override;
    void haltAction() override;

    /// @brief Power cycle the unlocked OptoHybrids that should be included in the run
    ///
    /// @warning Only displays an error message at the moment
    void power_cycle();

    /// @brief Automasker timer
    toolbox::task::Timer* m_automasker_timer = nullptr;

    /// @brief Periodic masking callback function
    void timeExpired(toolbox::task::TimerEvent& event) override;

    /// @brief Return JSON-formatted calibration state variable
    void calibrationStateUpdate(xgi::Input* in, xgi::Output* out);

    /// @brief Configures manually the OptoHybrid FPGA given in the "oh-mask" parameter
    void configureOptoHybridFPGA(xgi::Input* in, xgi::Output* out);

    /// @brief Read all identifiers (e.g. GBTx/LpGBT serial numbers, VFAT chip ID,...) for the detectors given in the "oh-mask" parameter
    ///
    /// If successful, returns an object similar to the example below. The @c
    /// null value is used to report unavailable or non-existent components.
    ///
    /// ~~~
    /// {
    ///   "0": {
    ///     "fpga": null,
    ///     "gbt": [null, 42, 100],
    ///     "vfat": [12, 24, 48, 96, ..., null]
    ///   },
    ///   ...
    /// ]
    /// ~~~
    void readDetectorIdentifiers(xgi::Input* in, xgi::Output* out);

private:
    // Calibration scan path
    xdata::String m_outputPath = ""; ///< Specifies the path for the output files of the non-traking data calibration scans

    // Hardware description
    xdata::UnsignedInteger32 m_fed_id = -1; ///< Specifies the FED ID of which the AMC is part of
    xdata::UnsignedInteger32 m_slot = -1; ///< Specifies the slot in which the AMC is located
    std::uint32_t m_initialOptohybridMask = 0x0; ///< OptoHybrids to be used in the run (loaded from layout tree)

    // Configuration
    xdata::UnsignedInteger32 m_automasker_masking_period = 1000; ///< Period at which the automasker VFAT masking is being triggered, in ms (0 means disabled)
    xdata::UnsignedInteger32 m_automasker_seu_recovery_period = 0; ///< Period at which the automasker SEU recovery being triggered, in ms (0 means disabled)
    xdata::Boolean m_enable_daqlink = true; ///< Enable the DAQLink by default in normal runs
    xdata::Boolean m_enable_local_readout = true; ///< Enable the local readout by default in normal runs
    xdata::Boolean m_enable_ttc_generator = false; ///< Enable the TTC generator by default in normal runs
    xdata::Boolean m_enableZS = false; ///< Enable the zero-suppression
    xdata::UnsignedInteger32 m_power_cycle_timeout = 0; ///< Timeout for the automatic power cycle (0 means disabled)
    xdata::UnsignedInteger32 m_power_on_timeout = 0; ///< Timeout for the automatic power on (0 means disabled)

    xhal_interface_shared_ptr m_connection; ///< Connection to the backend board
    xhal_interface_shared_ptr m_connection_automasker; ///< Connection to the backend board dedicated to the automasker
    std::uint32_t m_optohybridMask = 0x0; ///< OptoHybrids used in the run (is a subset of m_initialOptohybridMask)
    std::array<uint32_t, MAX_OPTOHYBRIDS_PER_AMC> m_vfatMask; ///< Automatically determined VFAT mask
    bool m_ttc_generator_enabled = false; ///< Whether or not the TTC generator should be enabled in the current run

    // Calibration scans
    uint32_t m_scannedValue; ///< Special variable for scans

    xoap::MessageReference updateScanValueCalib(xoap::MessageReference msg);
    xoap::MessageReference launchScan(xoap::MessageReference msg);

    nlohmann::json m_jsonVFAT; ///< Object used to store the VFAT configuration (updated whenever @c dumpConfigInfoVFAT is called)

    /// @brief Reads VFAT registers from the front-end and updates the @c m_jsonVFAT JSON object
    ///
    /// Typically called at the end of the configuration.
    void dumpConfigInfoVFAT();

    /// @brief Saves the @c m_jsonVFAT JSON object to disk
    ///
    /// Typically called at the beginning of the run.
    void saveConfigInfoVFAT();

    std::atomic<bool> m_calibrationState { false }; ///< True if calibration is ongoing, false otherwise
    std::atomic<uint32_t> m_calibrationType { 0 }; ///< Identifier for ongoing calibration scan
};

} // namespace gem::managers

#endif // GEM_MANAGERS_AMCMANAGER_H
