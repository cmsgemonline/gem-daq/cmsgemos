/// @file

#ifndef GEM_MANAGERS_AMC13MANAGERWEB_H
#define GEM_MANAGERS_AMC13MANAGERWEB_H

#include <gem/utils/GEMWebApplication.h>

#include <memory>

namespace gem::managers {

// Forward declaration
class AMC13Manager;

class AMC13ManagerWeb : public gem::utils::GEMWebApplication {
    friend class AMC13Manager;

public:
    AMC13ManagerWeb(AMC13Manager* application);
    virtual ~AMC13ManagerWeb();
};

} // namespace gem::managers

#endif // GEM_MANAGERS_AMC13MANAGERWEB_H
