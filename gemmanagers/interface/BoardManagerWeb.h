/// @file

#ifndef GEM_MANAGERS_BOARDMANAGERWEB_H
#define GEM_MANAGERS_BOARDMANAGERWEB_H

#include <gem/utils/GEMWebApplication.h>

namespace gem::managers {

class BoardManager;

class BoardManagerWeb : public gem::utils::GEMWebApplication {
    friend class BoardManager;

public:
    BoardManagerWeb(BoardManager* application);
    virtual ~BoardManagerWeb();
};

} // namespace gem::managers

#endif // GEM_MANAGERS_BOARDMANAGERWEB_H
