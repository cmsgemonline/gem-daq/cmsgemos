/// @file

#ifndef GEM_MANAGERS_AMCMANAGERWEB_H
#define GEM_MANAGERS_AMCMANAGERWEB_H

#include <gem/utils/GEMWebApplication.h>

namespace gem::managers {

class AMCManager;

class AMCManagerWeb : public gem::utils::GEMWebApplication {
    friend class AMCManager;

public:
    AMCManagerWeb(AMCManager* application);
    virtual ~AMCManagerWeb();

protected:
    void webDefault(xgi::Input* in, xgi::Output* out) override;
    void expertPage(xgi::Input* in, xgi::Output* out);
};

} // namespace gem::managers

#endif // GEM_MANAGERS_AMCMANAGERWEB_H
