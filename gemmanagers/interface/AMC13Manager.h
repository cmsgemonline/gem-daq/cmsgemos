/// @file

#ifndef GEM_MANAGERS_AMC13MANAGER_H
#define GEM_MANAGERS_AMC13MANAGER_H

#include <gem/utils/GEMFSMApplication.h>

#include <iomanip>

namespace amc13 {
class AMC13;
class Status;
}

namespace gem::managers {

typedef std::shared_ptr<::amc13::AMC13> amc13_ptr;

class AMC13ManagerWeb;

class AMC13Manager : public gem::utils::GEMFSMApplication {
    friend class AMC13ManagerWeb;

public:
    XDAQ_INSTANTIATOR();

    AMC13Manager(xdaq::ApplicationStub* s);

    virtual ~AMC13Manager();

protected:
    void actionPerformed(xdata::Event& event) override;

    amc13_ptr getAMC13Device() const { return p_amc13; };

    // State transitions
    void initializeAction() override;
    void configureAction() override;
    void startAction() override;
    void pauseAction() override;
    void resumeAction() override;
    void stopAction() override;
    void haltAction() override;

    xoap::MessageReference sendTriggerBurst(xoap::MessageReference mns);
    xoap::MessageReference enableTriggers(xoap::MessageReference mns);
    xoap::MessageReference disableTriggers(xoap::MessageReference mns);

    struct BGOInfo {
        xdata::Integer channel = -1;
        xdata::UnsignedInteger32 cmd = 0x0;
        xdata::UnsignedInteger32 bx = 0x0;
        xdata::Boolean isLong = false;
        xdata::UnsignedInteger32 prescale = 0x0;
        xdata::Boolean repeat = false;

        void registerFields(xdata::Bag<BGOInfo>* bag);
        std::string toString() const;
    };

    struct L1AInfo {
        xdata::Boolean enableLEMO = false;
        xdata::Boolean enableLocalL1A = false;
        xdata::UnsignedInteger32 internalPeriodicPeriod = 1;
        xdata::UnsignedInteger32 l1Aburst = 1;
        xdata::Integer l1Amode = 0;
        xdata::Integer l1Arules = 0;
        xdata::Boolean sendl1ATriburst = false;
        xdata::Boolean startl1ATricont = false;

        void registerFields(xdata::Bag<L1AInfo>* bag);
        std::string toString() const;
    };

    struct TTCInfo {
        xdata::UnsignedInteger32 oc0Command = 0x0;
        xdata::UnsignedInteger32 oc0Mask = 0x0;
        xdata::UnsignedInteger32 resyncCommand = 0x0;
        xdata::UnsignedInteger32 resyncMask = 0x0;

        void registerFields(xdata::Bag<TTCInfo>* bag);
        std::string toString() const;
    };

    struct AMC13Info {
        xdata::Bag<TTCInfo> amc13TTCConfig;
        xdata::String amcInputEnableList = "";
        xdata::String amcIgnoreTTSList = "";
        xdata::Integer bcOffset = 0;
        xdata::Vector<xdata::Bag<BGOInfo>> bgoConfig = { /* empty */ }; // Up to 4
        xdata::Boolean enableDAQ = true;
        xdata::Boolean enableDAQLink = false;
        xdata::Boolean enableFakeData = false;
        xdata::Boolean enableLocalTTC = false;
        xdata::Bag<L1AInfo> localTriggerConfig;
        xdata::Boolean monBackPressure = false;
        xdata::Integer prescaleFactor = 0;
        xdata::UnsignedInteger32 sfpMask = 0;
        xdata::Boolean skipPLLReset = true;
        xdata::UnsignedInteger32 slotMask = 0;

        void registerFields(xdata::Bag<AMC13Info>* bag);
        std::string toString() const;
    };

private:
    mutable gem::utils::Lock m_amc13Lock;

    amc13_ptr p_amc13;

    // Hardware description
    xdata::UnsignedInteger32 m_fed_id = -1; ///< Specifies the FED ID of which the AMC13 is part of

    xdata::Bag<AMC13Info> m_amc13Params;
    xdata::Vector<xdata::Bag<BGOInfo>> m_bgoConfig;
    xdata::Bag<L1AInfo> m_localTriggerConfig;
    xdata::Bag<TTCInfo> m_amc13TTCConfig;

    // seems that we've duplicated the members of the m_amc13Params as class variables themselves
    // what is the reason for this?  is it necessary/better to have these variables?
    std::string m_connectionFile, m_cardName, m_amcInputEnableList, m_slotEnableList, m_amcIgnoreTTSList;
    bool m_enableDAQ, m_enableDAQLink, m_enableFakeData;
    bool m_monBackPressEnable, m_megaMonitorScale;
    bool m_enableLocalTTC, m_skipPLLReset, m_enableLocalL1A,
        m_sendL1ATriburst, m_startL1ATricont, // need to remove
        m_bgoRepeat, m_bgoIsLong, m_enableLEMO;
    int m_localTriggerMode, m_localTriggerPeriod, m_localTriggerRate, m_L1Amode, m_L1Arules;
    int m_prescaleFactor, m_bcOffset, m_bgoChannel;
    uint8_t m_bgoCMD;
    uint16_t m_bgoBX, m_bgoPrescale;
    uint32_t m_ignoreAMCTTS, m_fedID, m_sfpMask, m_slotMask,
        m_internalPeriodicPeriod, m_L1Aburst;
    uint64_t m_updatedL1ACount = 0;
    ////counters
    uint32_t m_scanParameter;
};

} // namespace gem::managers

#endif // GEM_MANAGERS_AMC13MANAGER_H
