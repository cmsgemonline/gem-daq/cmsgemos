/// @file

#ifndef GEM_MANAGERS_BOARDMANAGER_H
#define GEM_MANAGERS_BOARDMANAGER_H

#include <gem/utils/GEMFSMApplication.h>

#include <xdata/String.h>

#include <memory>

namespace xhal::client {
class XHALInterface;
}

using xhal_interface_shared_ptr = std::shared_ptr<xhal::client::XHALInterface>;

namespace gem::managers {

class BoardManagerWeb;

class BoardManager : public gem::utils::GEMFSMApplication {
    friend class BoardManagerWeb;

public:
    XDAQ_INSTANTIATOR();

    BoardManager(xdaq::ApplicationStub* s);
    virtual ~BoardManager();

protected:
    // State transitions
    void initializeAction() override;
    void configureAction() override;
    void startAction() override;
    void pauseAction() override;
    void resumeAction() override;
    void stopAction() override;
    void haltAction() override;

private:
    // Hardware description
    xdata::String m_connection_url = ""; ///< Defines the URL of the RPC server to which to connect

    xhal_interface_shared_ptr m_connection; ///< Connection to the backend board
};

} // namespace gem::managers

#endif // GEM_MANAGERS_BOARDMANAGER_H
