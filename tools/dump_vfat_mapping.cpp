/// @file

#include <gem/core/layout-tree/addressing.h>
#include <gem/core/layout-tree/nodes.h>

#include <CLI/CLI.hpp>
#include <fmt/format.h>
#include <fmt/ostream.h>

#include <fstream>
#include <iostream>

constexpr std::string_view vfat_mapping_header = "fed;slot;oh;vfat;chip-id";
constexpr auto vfat_mapping_format = FMT_STRING("{:d};{:d};{:d};{:d};{:s}");

namespace ltree = gem::core::layout_tree;

int main(int argc, const char* argv[])
{
    // Define the options
    std::string output_filename;

    // Parse the command line
    CLI::App app { "Layout tree YAML file to .dat file export tool\n" };
    app.option_defaults()->always_capture_default();
    app.add_option("output-filename", output_filename, "Path to the output file")->required();
    CLI11_PARSE(app, argc, argv);

    // Load the tree
    const auto hardware_layout = ltree::load();

    // Open output file
    std::ofstream output_file(output_filename);
    output_file << vfat_mapping_header << std::endl;

    // Dump the chipID mapping for all VFAT
    for (const auto& vfat : hardware_layout->recursive_children() | ltree::filter_nodes<ltree::vfat_node>) {
        const auto address = ltree::address(&vfat);
        const auto fed_id = address.component<ltree::fed_node>();
        const auto slot = address.component<ltree::backend_board_node>();
        const auto link = address.component<ltree::link_node>();
        const auto vfatN = address.component<ltree::vfat_node>();

        const auto chip_id = vfat.uid();

        output_file << fmt::format(vfat_mapping_format, fed_id, slot, link, vfatN, chip_id) << std::endl;
    }

    return 0;
}
