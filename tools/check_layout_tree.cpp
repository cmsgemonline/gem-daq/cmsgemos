/// @file

#include <gem/core/layout-tree/nodes.h>
#include <gem/hardware/amc.h>
#include <gem/hardware/gbt.h>
#include <gem/hardware/optohybrid.h>
#include <gem/hardware/vfat3.h>
#include <xhal/client/XHALInterface.h>

#include <log4cplus/configurator.h>
#include <log4cplus/initializer.h>
#include <log4cplus/logger.h>

#include <memory>
#include <string>

namespace ltree = gem::core::layout_tree;

int main(int argc, const char* argv[])
{
    log4cplus::Initializer gemLog_initializer;
    auto logger = log4cplus::Logger::getInstance("default");
    log4cplus::BasicConfigurator config;
    config.configure();

    // Load the tree
    const auto hardware_layout = ltree::load();

    // Loop over "links"
    for (const auto& amc : hardware_layout->recursive_children() | ltree::filter_nodes<ltree::backend_board_node>) {
        const auto& amc_name = amc.display_name();
        const auto& amc_uid = amc.uid();
        const auto& amc_hostname = amc.hostname();

        std::unique_ptr<xhal::client::XHALInterface> connection;
        try {
            connection = std::make_unique<xhal::client::XHALInterface>(amc_hostname, logger);
        } catch (...) {
            LOG4CPLUS_WARN(logger, "Impossible to connect to AMC: " << amc_hostname);
            continue;
        }

        const std::string amc_name_rpc = connection->call<gem::hardware::amc::getFacts>().hostname;

        if (amc_name_rpc != amc_uid) {
            LOG4CPLUS_ERROR(logger, "Mismatched AMC UID: (AMC UID - AMC UID RPC) " << amc_uid << " - " << amc_name_rpc);
        }

        for (const auto& link : amc.recursive_children() | ltree::filter_nodes<ltree::link_node>) {
            const auto& link_uid = link.uid();
            const auto& link_display = link.display_name();
            const auto& link_number = link.number();

            // FPGA DNA
            for (const auto& fpga : link.recursive_children() | ltree::filter_nodes<ltree::optohybrid_fpga_node>) {
                const auto fpga_uid = fpga.uid();
                const auto fpga_uid_rpc = connection->call<gem::hardware::oh::getOHfpgaDNA>(1 << link_number).at(link_number);

                if (!fpga_uid_rpc) {
                    LOG4CPLUS_WARN(logger, "Impossible to check FPGA DNA for " << link_display << " (UID: " << link_uid << ")");
                    continue;
                }

                if (fpga_uid != std::to_string(*fpga_uid_rpc))
                    LOG4CPLUS_ERROR(logger, "On " << link_display << " (UID: " << link_uid << "), mistmatched FPGA DNA: (FPGA DNA - FPGA DNA RPC) " << fpga_uid << " - " << *fpga_uid_rpc);
            }

            // GBT serial number
            const auto gbt_uid_rpc_vec = connection->call<gem::hardware::gbt::readGBTSerialNumber>(link_number);

            for (const auto& gbt : link.recursive_children() | ltree::filter_nodes<ltree::gbt_node>) {
                const auto gbt_uid = gbt.uid();
                const auto gbt_number = gbt.number();

                if (!gbt_uid_rpc_vec.at(gbt_number)) {
                    LOG4CPLUS_INFO(logger, "Impossible to check GBT serial number for " << link_display << " (UID:  " << link_uid << ") - GBT number: " << gbt_number);
                    continue;
                }

                if (gbt_uid != std::to_string(*gbt_uid_rpc_vec.at(gbt_number)))
                    LOG4CPLUS_ERROR(logger, "On " << link_display << " (UID: " << link_uid << "), mistmatched GBT serial number: (GBT number - GBT serial number - GBT serial number RPC) " << gbt_number << " - " << gbt_uid << " - " << *gbt_uid_rpc_vec.at(gbt_number));
            }

            // VFAT chip ID
            //
            // Checking chip IDs from front-end just for sync'ed VFATs
            const auto vfat_mask = connection->call<gem::hardware::vfat3::getSyncedVFAT>(link_number);
            const auto vfat_uid_rpc_vec = connection->call<gem::hardware::vfat3::getVFAT3ChipIDs>(link_number, vfat_mask, false);

            for (const auto& vfat : link.recursive_children() | ltree::filter_nodes<ltree::vfat_node>) {
                const auto vfat_uid = vfat.uid();
                const auto vfat_number = vfat.number();

                if (!vfat_uid_rpc_vec.at(vfat_number)) {
                    LOG4CPLUS_INFO(logger, "Impossible to check VFAT chip ID for " << link_display << " (UID:  " << link_uid << ") - VFAT number: " << vfat_number);
                    continue;
                }

                if (vfat_uid != std::to_string(*vfat_uid_rpc_vec.at(vfat_number)))
                    LOG4CPLUS_ERROR(logger, "On " << link_display << " (UID: " << link_uid << "), mistmatched VFAT chip ID: (VFAT number - VFAT chip ID - VFAT chip ID RPC) " << vfat_number << " - " << vfat_uid << " - " << *vfat_uid_rpc_vec.at(vfat_number));
            }
        }
    }

    return 0;
}
